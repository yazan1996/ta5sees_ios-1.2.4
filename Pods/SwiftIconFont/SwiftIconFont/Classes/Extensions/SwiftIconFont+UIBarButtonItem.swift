//
//  SwiftIconFont+UIBarButtonItem.swift
//  SwiftIconFont
//
//  Created by Sedat Gökbek ÇİFTÇİ on 13.10.2017.
//  Copyright © 2017 Sedat Gökbek ÇİFTÇİ. All rights reserved.
//

import UIKit

public extension UIBarButtonItem {
    func icon(from font: Fonts, code: String, ofSize size: CGFloat){
        var textAttributes: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key.font: UIFont.icon(from: font, ofSize: size)]
        let currentTextAttributes: [String: AnyObject]? = self.titleTextAttributes(for: UIControl.State()) as? [String : AnyObject]
        
        if currentTextAttributes != nil {
            for (rawKey, value) in currentTextAttributes! {
                let key = NSAttributedString.Key.init(rawKey)
                if key != NSAttributedString.Key.font {
                    textAttributes[key] = value
                }
            }
        }
        self.setTitleTextAttributes(textAttributes, for: UIControl.State.normal)
        self.setTitleTextAttributes(textAttributes, for: UIControl.State.highlighted)
        
        self.title = String.getIcon(from: font, code: code)
    }
}
