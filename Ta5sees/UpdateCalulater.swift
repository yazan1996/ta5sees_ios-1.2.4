//
//  UpdateCalulater.swift
//  Ta5sees
//
//  Created by Admin on 9/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import FirebaseDatabase
import FirebaseCore
import FirebaseFirestore
import Firebase
import UIKit
import SwiftyJSON

class UpdateCalulater {
    
    var tee:Float!
    var energyTotal = 0
    let bmi:Float!
    var user_item:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!
    let ageChild:Float!
    var obPresnterAccount:PresnterAccount!
    var flag:String!
    
    init(item:tblUserInfo,ob:PresnterAccount,user_ProgressHistory:tblUserProgressHistory) {
        user_item = item
        self.user_ProgressHistory = user_ProgressHistory
        obPresnterAccount = ob
        ageChild = calclateAge(str: user_ProgressHistory.birthday)

        let r = Double(user_ProgressHistory.height)!/100.0
        bmi = Float(Double(user_ProgressHistory.weight)!/(r * r))

        if user_item.grown == "2" {
            calculateNormalDietChild()

            print("Child")
        }else{
            calculateNormalDietAdult()
            print("Adult")
        }
    }
    
    init(item:tblUserInfo,flag:String,user_ProgressHistory:tblUserProgressHistory) {
        user_item = item
        
        self.user_ProgressHistory = user_ProgressHistory

        
        self.flag = flag
        ageChild = calclateAge(str: user_ProgressHistory.birthday)
        
        let r = Double(user_ProgressHistory.height)!/100.0
        bmi = Float(Double(user_ProgressHistory.weight)!/(r * r))
        
        if user_item.grown == "2" {
            calculateNormalDietChild()
            
            print("Child")
        }else{
            calculateNormalDietAdult()
            print("Adult")
        }
    }
    
    func calculateNormalDietAdult(){
        var bmr:Float
        if user_ProgressHistory.refGenderID == "1" {
            
            bmr = Float(24.0 * Double(user_ProgressHistory.weight)!)
        }  else{
            bmr = Float(0.9 * 24.0 * Double(user_ProgressHistory.weight)!)
        }
        
        let pa:Float = Float(getRatePA(id:Int(user_ProgressHistory.refLayaqaCondID)!)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        
        energyTotal = processDietOrderForTEEAdulte(tee:tee, incrementKACL: Float(user_ProgressHistory!.gramsToLose)!)

        
        setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
        setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
        setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(user_ProgressHistory.refPlanMasterID)!)
        
    }
    
    func calculateNormalDietChild(){

        
        if ageChild >= 1.0 && ageChild <= 3.0{
            print("****0")
            let sub1 = (89.0 * Float(user_ProgressHistory.weight)! - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= 4.0 && ageChild <= 8.0 && user_ProgressHistory.refGenderID == "1" {
            print("****1")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= 4.0 && ageChild <= 8.0 && ageChild <= tblChildAges.range2EndTo(id:2)  && user_ProgressHistory.refGenderID == "2" {
            print("****2")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= 9  && ageChild <= 16 && user_ProgressHistory.refGenderID == "1" {
            print("****3")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
            
        }else if ageChild >= 9  && ageChild <= 16 && ageChild <= tblChildAges.range3EndTo(id:3)  && user_ProgressHistory.refGenderID == "2"{
            print("****4")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }else {
            print("****5")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }
        
        
        energyTotal = processDietOrderForTEEChild(tee:tee)
        
        
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(user_ProgressHistory.refPlanMasterID)!)
        
        
        
    }
    
    
    
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let age = ageChild
        let sum1 = v2 * age!
        let sum2 = (v3 * Float(user_ProgressHistory.weight)!)
        let sum5 = ((Float(user_ProgressHistory.height)!/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(user_ProgressHistory.refLayaqaCondID)! * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        print(sum4)
        return sum4
    }
    
    
    
    
    func processDietOrderForTEEAdulte(tee:Float,incrementKACL:Float)->Int{
        if user_ProgressHistory.refPlanMasterID == "2"  {
            return Int(round(tee - incrementKACL))
        }else if user_ProgressHistory.refPlanMasterID == "1"   {
            return Int(round(tee + incrementKACL))
        }else if user_ProgressHistory.refPlanMasterID == "3"   {
            return Int(round(tee))
        }
       
        return 1
    }
    
    
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = ageChild
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: Int(round(oldyear!)), bmi: bmi ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value <= 25 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
       
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = user_ProgressHistory.refGenderID
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if user_item.grown == "1"  {
            return Float(ob.PAadult)
        } else{
            if gender == "1"  {
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
        }
    }
    
    
    func getservMenuPlan(id:Int){
        var ob = tblMenuPlanExchangeSystem()
        if user_item.grown == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }

        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }else{
            setupRealm().beginWrite()
        }
        print("new carbo \(getDataFromSheardPreferanceString(key: "carbo"))")
        let pro = String(Int(round(getDataFromSheardPreferanceFloat(key: "newpro"))))
        let fat = String(Int(round(getDataFromSheardPreferanceFloat(key: "newfat"))))
        let carb = String(Int(round(getDataFromSheardPreferanceFloat(key: "newcarbo"))))
        let energy = String(Int(round(getDataFromSheardPreferanceFloat(key: "newenergy"))))

        print("new carbo \(getDataFromSheardPreferanceString(key: "newcarbo"))")
        tblUserTEEDistribution.setUserTeeDistrbution(userid: user_ProgressHistory.refUserID
        , carbo: carb, protien: pro, fat: fat, caloris: energy, date: getDateOnly(), dateAsInt: getDateOnlyAsIntType()) { (resp,err) in
            print("tblUserTEEDistribution  save : \(resp!)")
        }
                
        tblDailyWajbeh.setUserTrackerDaily(userid: user_ProgressHistory.refUserID
                                           , carbo: Int(carb)!, protien: Int(pro)!, fat: Int(fat)!, caloris: Int(energy)!,burned: 0,eaten: 0, left:0,date:getDateOnly()) { (resp,err) in
            print("tblDailyWajbeh  save : \(resp!)")
        }
        
        tblUserInfo.updateTargetWeightUser(totalCalories: getDataFromSheardPreferanceString(key: "newenergy")) { (re, er) in
            print("update caloris user : \(re)")
        }

        if flag == "1" {
            tblUserHistory.setInsertNewWeight(iduser: user_ProgressHistory.refUserID,h:user_ProgressHistory.height,w: user_ProgressHistory.weight,date: getDateOnly())
        }
        
//        self.saveExchangeSystem(id:user_item.id)
        if flag != "1" && flag != "2" {
        obPresnterAccount.hidDialog()
        }
    }
    
    
//    func saveExchangeSystem(id:String){
//        let item = setupRealm().objects(tblExchangeSystem.self).filter("id = %@", idUserExchangeSystem!).first!
//        try! setupRealm().write {
//            item.setValue(id, forKeyPath: "refUserID")
//        }
//    }
   

        
    
    func getTeeDistribution(energy:Int){
        
        var ref = Int(user_ProgressHistory.refPlanMasterID)
                
        if getDataFromSheardPreferanceString(key: "cusinene") == "8" {
            ref = 8
        }
        var ob = tblTEEDistribution()
        if user_item.grown == "1" {
            ob.getTeeDistrbution(refID: ref!,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{
            ob.getTeeDistrbution(refID: ref!,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
    }
    
    
    
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)

        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        sheardPreferanceWrite(fat: Float(fat),pro: Float((pro)),carbo: Float(carbo),energy: Float(energy))
    }
}



