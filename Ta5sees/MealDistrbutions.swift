//
//  MealDistrbutions.swift
//  Ta5sees
//
//  Created by Admin on 3/1/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import UIKit
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseAuth

class MealDistrbutions: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ModelMEalDistrbution.sharedManager.list.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealsDistrbution", for: indexPath) as! MealDistbutionCell
        
        cell.index = indexPath
        cell.delegate = self
        cell.cell = cell
        cell.timeFood = ModelMEalDistrbution.sharedManager.list[indexPath.row].time_food
        if  ModelMEalDistrbution.sharedManager.list[indexPath.row].selected { cell.setColor(color1:colorBasicApp,color2:.black,imageSmall:"clockWajbeh",imageBig: "clockmealDistrbution",isEnable:true,imgCateg: "breakfast")
        }else{
            cell.setColor(color1:colorGray,color2:colorGray,imageSmall:"clockDisable",imageBig: "clockWajbeh",isEnable:false,imgCateg: "breakFastDisable")
        }
        cell.setMainWajbeh(flag:ModelMEalDistrbution.sharedManager.list[indexPath.row].checkAsMain)
        
        cell.wajbehName.text = ModelMEalDistrbution.sharedManager.list[indexPath.row].name
        cell.setCustomTimr(time: ModelMEalDistrbution.sharedManager.list[indexPath.row].time_food)
        
        cell.imgWajbeh.image =  UIImage(named:ModelMEalDistrbution.sharedManager.list[indexPath.row].image)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    @IBOutlet weak var tblview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // setupCellConfiguration()
        tblview.delegate=self
        tblview.dataSource=self
      
        
    }
   
    @IBOutlet weak var btnNextOutlet: UIBarButtonItem!
    
    @IBAction func btnNext(_ sender: Any) {
        
        let resultsMain =  ModelMEalDistrbution.sharedManager.list.filter { $0.checkAsMain == true}
        print("resultsMain \(resultsMain)")
        let results =  ModelMEalDistrbution.sharedManager.list.filter { $0.selected == false && $0.name != "سناك ١" && $0.name != "سناك ٢"}
        if  results.count == 3 {
            alertDialog(mes: "يجب اختيار وجبات")
        }else if ModelMEalDistrbution.sharedManager.list[1].selected == false && ModelMEalDistrbution.sharedManager.list[1].name == "سناك ١" &&  ModelMEalDistrbution.sharedManager.list[3].name == "سناك ٢" &&  ModelMEalDistrbution.sharedManager.list[3].selected == false {
            alertDialog(mes: "يجب اختيار سناك واحد على الاقل")
        }else if resultsMain.count == 5 {
            alertDialog(mes: "يجب اختيار وجبة رئيسية")
        }else{
            self.performSegue(withIdentifier: "goToToturial", sender: self)
        }
        
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //
    //        return 80
    //    }
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateTable(){
        DispatchQueue.main.async {
            self.tblview.reloadData()
        }
    }
    
    
    func alertDialog(mes:String){
        let alert = UIAlertController(title: "تنبيه", message: mes, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "إخفاء", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = Bundle.main.loadNibNamed("HeaderMealDistbutionCell", owner: self, options: nil)?.first as! HeaderMealDistbutionCell
        
        return viewHeader
    }
    
    //    func updateCell(cell:MealDistbutionCell){
    //        if let indexPath = tblview.indexPath(for: cell) {
    //            DispatchQueue.main.async {
    //                self.tblview.reloadRows(at: [indexPath], with: .fade)
    //
    //            }
    //        }
    //    }
    
 
    
}

struct ObjectMealDistrbution: Equatable, Hashable {
    static var id:Int!
    static  var meals:tblMealDistribution!
    static func filterMealDistrbution()->tblMealDistribution{
        let obMD = setupRealm().objects(tblMealDistribution.self).filter("Breakfast == %@ AND Snack1 == %@ AND Lunch == %@ AND Snack2 == %@ AND Dinner == %@ AND isBreakfastMain == %@ AND isLunchMain == %@ AND isDinnerMain == %@", NSNumber(value:ModelMEalDistrbution.sharedManager.list[0].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[1].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[2].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[3].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[4].selected),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[0].checkAsMain),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[2].checkAsMain),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[4].checkAsMain)).first
        
        setDataInSheardPreferance(value: "\(obMD!.id)", key: "mealDistrbution")
        
        
        return obMD!
    }
}

struct ObjectMealDistrbutionFixLunch: Equatable, Hashable {
    static var id:Int!
    static  var meals:tblMealDistribution!
    static func filterMealDistrbution()->tblMealDistribution{
        let obMD = setupRealm().objects(tblMealDistribution.self).filter("Breakfast == %@ AND Snack1 == %@ AND Lunch == %@ AND Snack2 == %@ AND Dinner == %@ AND isBreakfastMain == %@ AND isLunchMain == %@ AND isDinnerMain == %@", NSNumber(value:ModelMEalDistrbution.sharedManager.list[0].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[1].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[2].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[3].selected),NSNumber(value:ModelMEalDistrbution.sharedManager.list[4].selected),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[0].checkAsMain),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[2].checkAsMain),NSNumber(value:!ModelMEalDistrbution.sharedManager.list[4].checkAsMain)).first
        
        setDataInSheardPreferance(value: "\(obMD!.id)", key: "mealDistrbution")
        
        
        return obMD!
    }
}
class ModelMEalDistrbution {
  
//    static var sharedInstance = ModelMEalDistrbution()

    var name:String!
    var selected:Bool!
    var checkAsMain:Bool!
    var time_food:String!
    var time_food_date_str:String!
    var image:String!
    var refOrederby:String!
    var id:String!
    var list = [ModelMEalDistrbution]()
    init() {}
    init(name: String,selected: Bool,checkAsMain: Bool,time_food: String,image:String,refOrederby: String,id: String,time_food_date_str:String){
        self.name = name
        self.selected = selected
        self.checkAsMain = checkAsMain
        self.time_food = time_food
        self.image = image
        self.refOrederby = refOrederby
        self.id = id
        self.time_food_date_str = time_food_date_str
    }

     static var sharedInstance: ModelMEalDistrbution?
       class var sharedManager : ModelMEalDistrbution {
           guard let sharedInstance = self.sharedInstance else {
               let sharedInstance = ModelMEalDistrbution()
               self.sharedInstance = sharedInstance
               return sharedInstance
           }
           return sharedInstance
       }
       class func destroySharedManager() {
           sharedInstance = nil
       }
//    func dispose()
//       {
//        ActivationModel = nil
//           print("Disposed Singleton instance")
//       }

    func generteList(){
        list.removeAll()
        ActivationModel.sharedInstance.arr_mealDes.removeAll()
        
        for id in ActivationModel.sharedInstance.refWajbehNotMain.removeDuplicates() { // not main waj
            ActivationModel.sharedInstance.arr_mealDes.append(ModelMEalDistrbution.sharedManager.getItem(id:id, isMain: true, selected: true))
        }
        for id in ActivationModel.sharedInstance.refTasbera.removeDuplicates() { //not main tasbera
            ActivationModel.sharedInstance.arr_mealDes.append(ModelMEalDistrbution.sharedManager.getItem(id:id, isMain: true, selected: true))
        }

        ActivationModel.sharedInstance.arr_mealDes.append(ModelMEalDistrbution.sharedManager.getItem(id:ActivationModel.sharedInstance.refWajbehMain, isMain: false, selected: true)) //main waj
        
        ModelMEalDistrbution.sharedManager.list = ActivationModel.sharedInstance.arr_mealDes.sorted(by: { $0.refOrederby < $1.refOrederby }).unique{$0.name ?? ""}
        
        for i in 1...5 {
            _ =  ActivationModel.sharedInstance.arr_mealDes.filter { (item) -> Bool in
                if item.id != "\(i)" {
                    ActivationModel.sharedInstance.arr_mealDes.append(ModelMEalDistrbution.sharedManager.getItem(id:"\(i)", isMain: true, selected: false))
                    return true
                }
                
                return false
            }
        }

        ActivationModel.sharedInstance.arr_mealDes =  ActivationModel.sharedInstance.arr_mealDes.sorted(by: { $0.refOrederby < $1.refOrederby }).unique{$0.name ?? ""}

        
   }
     func getItem(id:String,isMain:Bool,selected:Bool)->ModelMEalDistrbution{
        switch id {
        case "1":
            return ModelMEalDistrbution(name: "وجبة فطور",selected: selected,checkAsMain: isMain,time_food: "9:00 AM",image:"breakfast",refOrederby: "1",id: id, time_food_date_str: "9:00 AM")
        case "2":
            return  ModelMEalDistrbution(name: "وجبة غداء",selected: selected,checkAsMain: isMain,time_food: "4:00 PM",image:"launch",refOrederby: "3",id: id, time_food_date_str: "4:00 PM")
        case "3":
            return ModelMEalDistrbution(name: "وجبة عشاء",selected: selected,checkAsMain: isMain,time_food: "9:00 PM",image:"dinner",refOrederby: "5",id: id, time_food_date_str: "9:00 PM")
        case "4":
            return ModelMEalDistrbution(name: "تصبيرة صباحي",selected: selected,checkAsMain: isMain,time_food: "12:00 PM",image:"apple",refOrederby: "2",id: id, time_food_date_str: "12:00 PM")
        case "5":
            return ModelMEalDistrbution(name: "تصبيرة مسائي",selected: selected,checkAsMain: isMain,time_food: "7:00 PM",image:"tsbera",refOrederby: "4",id: id, time_food_date_str: "7:00 PM")
        default:
            return ModelMEalDistrbution(name: "تصبيرة مسائي",selected: selected,checkAsMain: isMain,time_food: "7:00 PM",image:"tsbera",refOrederby: id,id: id, time_food_date_str: "7:00 PM")
        }
    }
}



extension MealDistrbutions:updateList {
    func updateWajbeh(index:IndexPath,flag:Bool) {
        //        ModelMEalDistrbution.sharedInstance.list[index.row].selected = flag
        for (indexItem,_) in  ModelMEalDistrbution.sharedManager.list.enumerated() {
            if index.row == indexItem {
                if ModelMEalDistrbution.sharedInstance!.list[indexItem].checkAsMain != false {
                    ModelMEalDistrbution.sharedInstance!.list[indexItem].selected = flag
                }
            }
        }
        
        updateTable()
    }
    
    
    func updateImageWajbeh(index:IndexPath,img:String) {
        ModelMEalDistrbution.sharedManager.list[index.row].image = img
        updateTable()
    }
    
    func updateMain(index:IndexPath,flag:Bool,cell:MealDistbutionCell) {
        ModelMEalDistrbution.sharedManager.list[index.row].checkAsMain = flag
        for (indexItem,_) in  ModelMEalDistrbution.sharedManager.list.enumerated() {
            if index.row == indexItem {
                ModelMEalDistrbution.sharedInstance!.list[indexItem].checkAsMain = flag
                if ModelMEalDistrbution.sharedInstance!.list[indexItem].selected == false {
                    ModelMEalDistrbution.sharedInstance!.list[indexItem].selected = true
                }
            }else{
                ModelMEalDistrbution.sharedInstance!.list[indexItem].checkAsMain = true
            }
        }
        updateTable()
    }
    
    
    func updateTime(index:IndexPath,str:String,cell:MealDistbutionCell,textDate:String) {
        ModelMEalDistrbution.sharedManager.list[index.row].time_food = str
        ModelMEalDistrbution.sharedManager.list[index.row].time_food_date_str = textDate
        updateTable()
    }
}


extension Array {
    func unique<T:Hashable>(map: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(map(value)) {
                set.insert(map(value))
                arrayOrdered.append(value)
            }
        }

        return arrayOrdered
    }
}
