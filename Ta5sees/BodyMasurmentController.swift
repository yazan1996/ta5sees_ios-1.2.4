//
//  BodyMasurmentController.swift
//  Ta5sees
//
//  Created by Admin on 9/28/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
class BodyMasurmentController: UIViewController {
    
    
    
    var user_ProgressHistory:tblUserProgressHistory!
    
    @IBAction func dissmis(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var txtweight: HoshiTextField!
    @IBOutlet weak var txtHeight: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtweight.clearsOnBeginEditing = true
        txtHeight.clearsOnBeginEditing = true
        
        
        
        
        getInfoHistoryProgressUser { [self] (user, err) in
            self.user_ProgressHistory = user
            print("user_ProgressHistory",user)
//            self.txtweight.text = user_ProgressHistory.weight
            setWeight(weight:user_ProgressHistory.weight)
            setHight(hight:user_ProgressHistory.height)
//            self.txtHeight.text =
        }
        
    }
    func setWeight(weight:String){
        if Double(weight)!.rounded(.up) == Double(weight)!.rounded(.down){
            txtweight.text = "\(Int(Double(weight)!.rounded(.down)))"
        }else{
            txtweight.text = weight

        }
    }
    
    func setHight(hight:String){
        if Double(hight)!.rounded(.up) == Double(hight)!.rounded(.down){
            txtHeight.text = "\(Int(Double(hight)!.rounded(.down)))"
        }else{
            txtHeight.text = hight

        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }
    
    
    @IBAction func btnSave(_ sender: Any) {
        if FormatterTextField(str: txtweight).text! != user_ProgressHistory.weight || FormatterTextField(str: txtHeight).text! != user_ProgressHistory.height {
            if txtHeight.text!.isEmpty {
                showToast(message: "يجب ادخال الطول", view: view, place: 1)
            }else  if txtweight.text!.isEmpty {
                showToast(message: "يجب ادخال الوزن", view: view, place: 1)
            }else {
                //                CheckInternet.checkIntenet { (bool) in
                //                    if !bool {
                //                        SVProgressHUD.dismiss()
                //                        showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
                //                        return
                //                    }
                //                }
                tblUserProgressHistory.updateWeightHeightUser(weight:"\(Double(FormatterTextField(str: txtweight).text!)!)", height: FormatterTextField(str: txtHeight).text!, date: getDateOnly() ) { [self] (res, error) in
                    if res == "success" {
                        tblUserHistory.setInsertNewWeight(iduser: getDataFromSheardPreferanceString(key: "userID"), h: "nil",w: "\(Double(FormatterTextField(str: txtweight).text!)!)",date: getDateOnly())
                        //                        _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,flag:"1", user_ProgressHistory: getUserInfoProgressHistory())
                        //                                                tblUserInfo.getMealDistrbutionUser()
                        //                                                tblUserInfo.updateMealPlanner { (s, e) in
                        SVProgressHUD.dismiss {
                            showToast(message: "تم تحديث قياسات الجسم", view: self.view,place:0)
                            self.dismiss(animated: true, completion: nil)
                        }
                        //                                                }
                    }else if res == "false" {
                        SVProgressHUD.dismiss {
                            //                            showToast(message: "نعتذر لم يتم تحديث قياسات الجسم", view: self.view,place:0)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }else{
                        showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
                        
                    }
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
            
        }
    }
}
