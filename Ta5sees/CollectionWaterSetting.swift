//
//  CollectionWaterSetting.swift
//  Ta5sees
//
//  Created by Admin on 2/7/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import FLAnimatedImage

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return modelData.count
        
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:TrackerWaterCell=collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TrackerWaterCell
        cell.laImage.delegate = self
        cell.tag = indexPath.row
        cell.laImage.image = nil
        
      
        if indexPath.row == 0 && modelData[indexPath.row].select == false && modelData[indexPath.row].flag == false {
            cell.laImage.image = UIImage(named: "addWater")! // plus
        }
        else if indexPath.row > 0 && modelData[indexPath.row].select == false && modelData[indexPath.row].flag == false {
            cell.laImage.image = UIImage(named: "empty_small")!
            
        }else if modelData[indexPath.row].select == true {
            if modelData[indexPath.row].flag == true {
                collectionView.isUserInteractionEnabled = false
                if let path =  Bundle.main.path(forResource: "small-gif", ofType: "gif") {
                    if let data = NSData(contentsOfFile: path) {
                        let gif = FLAnimatedImage(animatedGIFData: data as Data)
                        cell.laImage.animatedImage = gif
                        cell.laImage.loopCompletionBlock = {_ in
                            cell.laImage.animationDuration = 1.5
                            cell.laImage.stopAnimating()
                            cell.laImage.image = UIImage(named: "full_small")!
                            collectionView.isUserInteractionEnabled = true
                            if indexPath.row == self.modelData.count-1 && self.indexTrue == self.modelData.count {
                                if self.modelData[indexPath.row].flag == true {
//                                    ShowTost(title:"", message: "")
                                }
                            }
                            self.modelData[indexPath.row].flag = false
                            
                        }
                    }
                }
                
            }else{
                
                cell.laImage.image = UIImage(named: "full_small")!
                
            }
        }
        if indexTrue != nil{
            if indexTrue == cell.tag {
                cell.laImage.image = UIImage(named: "addWater")! // plus
            }
        }

        if  indexPath.row == 0 {
            location.append(cell.frame)
            imagTut.insert(cell, at: 0)
            print(cell.frame)
        }else  if  indexPath.row == 1 {
            location.append(cell.frame)
            imagTut.insert(cell, at: 1)
            print(cell.frame)
        }else  if  indexPath.row == 2 {
            location.append(cell.frame)
            imagTut.insert(cell, at: 2)
            print(cell.frame)
        }else  if  indexPath.row == 3 {
            location.append(btnBreakOut.frame) // index 34unused
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if modelData[indexPath.row].select == false && modelData[indexPath.row].flag == false {
            setAll(indexs:indexPath.row)
            print("case 1")
        }else  if modelData[indexPath.row].select == true && modelData[indexPath.row].flag == true  {
            remove(indexs:indexPath.row)
            print("case 2")
            
        }else if modelData[indexPath.row].select == true && modelData[indexPath.row].flag == false  {
            remove(indexs:indexPath.row)
            print("case 3")
        }
        
        collectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
    func remove(indexs:Int){
        indexTrue = indexs
        var index = indexs
        print("leng : \(modelData.count) \(indexs)" )
        while index < modelData.count{
            modelData[index].select = false
            modelData[index].flag = false
            index = index+1
        }
        pd?.saveAmountWaterDrink(x: indexTrue)
    }
    func setAll(indexs:Int){
        //        collectionviewoutl.isUserInteractionEnabled = false
        var index = 0
        if indexTrue != nil {
            index = indexTrue
        }
        while index < indexs+1{
            modelData[index].select = true
            modelData[index].flag = true
            index = index+1
            indexTrue = index
        }
        pd?.saveAmountWaterDrink(x: indexTrue)
    }
    
    func selectPrgramticlly(index:Int){
        setDataInSheardPreferance(value: "1", key: "tutWaterRun")
        let indexPathForFirstRow = IndexPath(row: index, section: 0)
        self.collectionView(collectionviewoutl, didSelectItemAt: indexPathForFirstRow)
    }
    func removePrgramticlly(){
        indexTrue = 0
        var index = 0
        while index < modelData.count{
            modelData[index].select = false
            modelData[index].flag = false
            index = index+1
        }
        collectionviewoutl.reloadData()
    }
   
}
