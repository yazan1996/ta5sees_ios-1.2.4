//
//  loginFacebook.swift
//  Ta5sees
//
//  Created by Admin on 3/15/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import FBSDKLoginKit
import FirebaseAuth
import Firebase

class loginFacebook {
    let fbLogin : LoginManager = LoginManager()
    var delege:Any!
    var view:viewFaceBookLogin!
    var flag:Int!
//    var obAPI = APiSubscribeUser()
    
    
    init(with view:viewFaceBookLogin,delege:Any,flag:Int){
        self.view=view
        self.delege=delege
        self.flag=flag
    }
    
    func faceBookConnection(){
        fbLogin.logIn(permissions: ["email"], from: delege as? UIViewController) { (result, err) in
            self.view.HideProgress()
            if(err != nil){
                self.view.errorConnection(str:"\(String(describing: err))") //"لم تتم العملية بنجاح")
            }else
                if (result?.isCancelled)!{
                    self.view.HideProgress()
                }else {
                    self.view.showProgress()
                    self.dataGettingFromFB(self.fbLogin,v: (result?.token?.tokenString)!)
            }
        }
    }
    
    
    func dataGettingFromFB(_ fbLogin:LoginManager,v:String){
        let credential = FacebookAuthProvider.credential(withAccessToken: v)
        
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if error == nil {
//                if getDataFromSheardPreferanceString(key: "loadWajbehSenf") == "0" {
//                    DispatchQueue.global(qos: .utility).async {
//                        LoadData.FetchWajbehSenf()
//                    }
//                }
                
                GraphRequest(graphPath: "/me", parameters: ["fields":"id, email, first_name, last_name"]).start { [self] (connection, result, err) in
                    if (err != nil){
                        print("failed to start graph request:",err as Any)
                        (self.delege as AnyObject).view.removeFromSuperview()
                        self.view.errorConnection(str:"\(String(describing: err))")//"لم تتم العملية بنجاح")
                        
                    }else {
                        let fbDetails = result as! NSDictionary

                        var email : String? = fbDetails["email"] as? String
                        if(email == nil){
                            email = ""
                        }
                        
                        let firstName = fbDetails["first_name"] as! String
                        let id = fbDetails["id"] as! String
                        let lastName = fbDetails["last_name"] as! String
                        //let phone = fbDetails["phone"] as! String
                        
                        //                if self.flag == 0 { // signup
                        //                    self.saveData(name:firstName+lastName,id_Facebook:id)
                        //                }else{
                        //// signin
                        self.view.getIDUseer(id: id, name: firstName+" "+lastName,loginType: "2")
                        //                }
                        
                    }
                    //                fbLogin.logOut()
                }
            }else{
                self.view.HideProgress()
                self.view.errorConnection(str:"\(String(describing: error))")//"لم تتم العملية بنجاح")
            }
            
        }
        
    }
    
    
    func saveData(name:String,id_Facebook:String){
        setDataInSheardPreferance(value: name, key: "name")
        setDataInSheardPreferance(value: id_Facebook, key: "mobNum")
        setDataInSheardPreferance(value: "2", key: "loginType")
        //        APILogin.sheard.checkUserFacebookIDFound(idFacebook:id_Facebook) { (flag) in
        //            if flag == 4 {
        //                self.view.HideProgress()
        //                self.view.successConnectionFacebook()
        //
        //            }else if flag == 1 {
        //                self.view.HideProgress()
        //                self.view.errorConnection(str: "المستخدم مشترك مسبقا")
        //
        //            }
        //        }
        
    }
}


protocol viewFaceBookLogin {
    func errorConnection(str:String)
    func successConnectionFacebook()
    func showProgress()
    func HideProgress()
    func getIDUseer(id:String,name:String,loginType:String)

}


/*
 func dataGettingFromFB(_ fbLogin:LoginManager){
     GraphRequest(graphPath: "/me", parameters: ["fields":"id, email, first_name, last_name"]).start { (connection, result, err) in
         
         if (err != nil){
             print("failed to start graph request:",err as Any)
             (self.delege as AnyObject).view.removeFromSuperview()
             self.view.errorConnection(str:"\(err)")//"لم تتم العملية بنجاح")
             
         }else {
             
             let fbDetails = result as! NSDictionary
             
             var email : String? = fbDetails["email"] as? String
             if(email == nil){
                 email = ""
             }
             
             let firstName = fbDetails["first_name"] as! String
             let id = fbDetails["id"] as! String
             let lastName = fbDetails["last_name"] as! String
             //let phone = fbDetails["phone"] as! String
             if self.flag == 0 { // signup
                 self.saveData(name:firstName+lastName,id_Facebook:id)
             }else{
                 //// signin
                 self.view.getIDUseer(id: id)
             }
             
         }
         //                fbLogin.logOut()
     }
 }
 */
