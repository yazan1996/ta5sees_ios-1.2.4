//
//  DietsVC.swift
//  Ta5sees
//
//  Created by Admin on 3/17/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//


import UIKit
import Firebase
import FirebaseAnalytics

class DietsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIViewControllerTransitioningDelegate {
    
    
    @objc func clickSubscribeNow(_ sender:UIButton){
        showList = true
        createEvent(key: "18", date: getDateTime())
        tblview.reloadData()
        viewWillAppear(true)
    }
    func showButton(){
        let btn =  CollectionViewHelper.BTNEmptyMessage(message: "تفعيل إشتراك النظام الغذائي", viewController: tblview)
        btn.addTarget(self, action: #selector(clickSubscribeNow(_:)), for: .touchUpInside)
        btn.tag = 1001
        showList = false
    }
    var showList = false
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !showList {
            if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 2 {
                textTitle.isHidden = true
                showButton()
                return 0
                
            }else if getUserInfo().subscribeType == 0 {
                
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                    textTitle.isHidden = true
                    showButton()
                    return 0
                }
            }else if getUserInfo().subscribeType != 1 || getUserInfo().subscribeType != 4 {
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                    textTitle.isHidden = true
                    showButton()
                    return 0
                }
            }else{
                removeFromSuperviews()
                textTitle.isHidden = false
            }
        }else{
            textTitle.isHidden = false
            removeFromSuperviews()
        }
        
        return items.count
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DietsVCCell",for: indexPath) as? DietsVCCell else {
            return DietsVCCell()
        }
        let item = items[indexPath.row]
        
        if item.id == "7" || item.id == "8" || item.id == "9"{
            cell.txt.text = items[indexPath.row].discription
        }else{
            cell.txt.text = "حمية من بيتك (\(items[indexPath.row].discription))"
        }
        getData(from: URL(string:item.imgPath)!) { data, response, error in
            guard let data = data, error == nil else {
                DispatchQueue.main.async() {
                setGradientBackgroundLable(txt: cell.txt,delegate:self)
                }
                return
            }
            print(response?.suggestedFilename ?? URL(string:item.imgPath)!.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                if let imgEdite = UIImage(data: data){
                    cell.txt.backgroundColor = UIColor(patternImage:imgEdite)
                }else {
                    setGradientBackgroundLable(txt: cell.txt,delegate:self)
                }
        }
        }
        cell.txt.textAlignment = .center
        cell.txt.font = UIFont(name: "GE Dinar One",size: 20)
        cell.txt.layer.cornerRadius = 9
        cell.txt.backgroundColor = UIColor.white.withAlphaComponent(0)
        return cell
    }
    
    var items = [tblCusisneType]()
    var section = [Section]()
    var obselectionDelegate:selectionDelegate!
    var optionsSelected = ""
    var flag = "13" // flag 1 >> cusine / flag 2 >> allergy
    var index:Int!
    var seletAll = 0
    var delegatViewController:ViewController!
    @IBOutlet var textTitle:UILabel!
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async() { [self] in
            tblview.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        removeFromSuperviews()
    
    }
    
    func removeFromSuperviews(){
        for subview in tblview.subviews {
            if subview is UIButton {
                if subview.tag == 1001 {
                    subview.removeFromSuperview()
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async { [self] in
            items = tblCusisneType.getCusineActive().sorted(by: {$0.orderNumber < $1.orderNumber})
            
            print(items)
        }
        setUpCollectionView()
        
    }
    
    func setUpCollectionView() {
        /// 1
        
        
        /// 2
        tblview.delegate = self
        tblview.dataSource = self
        
        /// 3
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        /// 4
        layout.minimumLineSpacing = 8
        /// 5
        layout.minimumInteritemSpacing = 4
        
        /// 6
        tblview
            .setCollectionViewLayout(layout, animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        
        
        
        tblview.reloadItems(at: [indexPath])
        
        
        print("##1 ",item.id)
        if getUserInfo().subscribeType == -1 {
            showAlert(str: "ستقوم بتفعيل ٧ ايام مجانية للخدمة", view: self,id: item.id)
        }else{
            showAlert(str: "مازلت في الفترة التجريبية،ستقوم بتحديث نظامك الغذائي", view: self,id: item.id)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CVSubcribe" {
            if let dis=segue.destination as?  AdvViewControllers{
                if  let data=sender as? [Bool] {
                    dis.subscribeDirect = data[0]
                    dis.hideFreeTrialBTN = data[1]
                }
            }
        }
    }
    
    func showAlert(str:String,view:UIViewController,id:String){
        isPaymentRun() { [self]  str in
            print("isPaymentRun",str)
            if str == "1" || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 1 || getUserInfo().subscribeType ==  4 {
                ActivationModel.sharedInstance.refCusine = id
                let detailView = self.storyboard!.instantiateViewController(withIdentifier: "MainWajbhVC") as! MainWajbhVC
                detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                detailView.transitioningDelegate = self
                createEvent(key: "19", date: getDateTime())
                self.present(detailView, animated: true, completion: nil)
            }else{
                showToast(message: "ياتيكم قريبا", view: self.view, place: 0)
            }
        }
        
    }
    
    @IBOutlet weak var tblview: UICollectionView!
}



extension DietsVC: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        /// 2
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
    
    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthPerItem:CGFloat!
        /// 4
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// 5
        if indexPath.row == 2 {
            widthPerItem = collectionView.frame.width / 1 - lay.minimumInteritemSpacing
        }else{
            widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        }
        /// 6
        return CGSize(width: widthPerItem - 8, height: 100)
    }
}

extension DietsVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
