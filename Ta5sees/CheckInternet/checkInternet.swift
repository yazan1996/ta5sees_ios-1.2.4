//
//  checkInternet.swift
//  Ta5sees
//
//  Created by Admin on 4/5/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import SystemConfiguration
import Reachability

class checkInternet {
//    static var sheard = checkInternet()
    let reachability = try! Reachability()

    func setupCheckInternet(){
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
     @objc func reachabilityChanged(note: Notification) {

          let reachability = note.object as! Reachability

          switch reachability.connection {
          case .wifi:
              print("Reachable via WiFi")
    //        alert(mes: "Reachable via WiFi", selfUI: self)
          case .cellular:
              print("Reachable via Cellular")
    //        alert(mes: "Reachable via Cellular", selfUI: self)

          case .unavailable:
            print("Network not reachable")
    //        alert(mes: "لا يوجد اتصال بالانترنت", selfUI: self)
            ShowTostِError(title: "تنبيه", message: "لا يوجد اتصال بالانترنت")
//            showToast(message: "لا يوجد اتصال بالانترنت", view: self.view)

          case .none:
            break
            }
        //        reachability.stopNotifier()
        //        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
            }
}
/*    private let reactablility = SCNetworkReachabilityCreateWithName(nil, "www.google.com")

//    func checkReactablility(){
//        var flag = SCNetworkReachabilityFlags()
//        SCNetworkReachabilityGetFlags(self.reactablility!, &flag)
//        if (isNetworkReachable(with:flag)){
//            print(flag)
//            if flag.contains(.isWWAN) {
//                print("via moble")
//                return
//            }
//            print("via wifi")
//
//        }else if !isNetworkReachable(with:flag){
//            print("not connection")
//            print(flag)
//            return
//        }
//    }
//    func isNetworkReachable(with flag:SCNetworkReachabilityFlags)->Bool{
//        let isReachable = flag.contains(.reachable)
//        let needsConnection = flag.contains(.connectionRequired)
//        let canConnectionAuto = flag.contains(.connectionOnDemand) || flag.contains(.connectionOnTraffic)
//        let canConnectionWithoutUserIntraction = canConnectionAuto && !flag.contains(.interventionRequired) || flag.contains(.connectionOnTraffic)
//
//        return isReachable && (!needsConnection || canConnectionWithoutUserIntraction)
//
//
 //    }*/
