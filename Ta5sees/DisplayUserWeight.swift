//
//  DisplayUserWeight.swift
//  Ta5sees
//
//  Created by Admin on 1/23/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class DisplayUserWeight {

    var updateweight:UIStepperController!

    init(view:ViewController,updateweight:UIStepperController) {
        self.updateweight = updateweight
        updateweight.delegate = view // Assign instance delegate to UIStepperControllerDelegate
        updateweight.isFloat = true
        updateweight.isMinus = true
        updateweight.textColor(color: UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0))
        self.updateweight.borderColor(color: UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0))
        updateweight.incrementBy(number: 0.1)
        updateweight.leftButtonBackgroundColor(color: UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0))
        updateweight.rightButtonBackgroundColor(color:UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0))
        updateweight.leftButtonForegroundColor(color: .white)
        updateweight.rightButtonForegroundColor(color: .white)
    }
    
    func setCurrentWeightValue(currentWeight:Double){
        updateweight.count = CGFloat(currentWeight)
    }


}
