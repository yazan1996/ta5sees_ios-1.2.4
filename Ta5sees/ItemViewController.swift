//
//  ItemViewController.swift
//  ViewPager-Swift
//
//  Created by Nishan on 2/9/16.
//  Copyright © 2016 Nishan. All rights reserved.
//

import UIKit
import Firebase

class ItemViewController: UIViewController ,UIViewControllerTransitioningDelegate  {

    override func loadView() {
        
        let newView = UIView()
        newView.backgroundColor = UIColor.white
        
        view = newView
    }

    var itemText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let itemLabel = UILabel()
//        itemLabel.translatesAutoresizingMaskIntoConstraints = false
//        view.addSubview(itemLabel)
//
//        itemLabel.textAlignment = .center
//        itemLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
//        itemLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//
//        itemLabel.text = itemText
        
        dateLabel.text = txtDateDay//getDateOnly()
        tblView.delegate = self
        tblView.dataSource = self

        tblView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
       
//       if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() <= Double(getUserInfo().expirDateSubscribtion)! {
//           print("txtDateDay \(txtDateDay)")
//           tblWajbatUserMealPlanner.getAllData(date: txtDateDay) { [self] (arrWajbatUserMealPlanner) in
//               _arrWajbat.append(contentsOf: arrWajbatUserMealPlanner)
//               _arrWajbat = _arrWajbat.sorted(by:{ $0.refOrderID < $1.refOrderID })
//               tblView.reloadData()
//           }
//       }else{
           if getUserInfo().subscribeType == 0 {
               showAlert(str: " تم الانتهاء من الفترة التجريبية ،هل تريد الاشتراك الآن ؟", view: self)
           }else if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 2{
               showAlert(str: "انت غير مشترك بالخدمة،هل تريد الاشتراك الآن ؟", view: self)
           }
       }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblView.reloadData()
        print("txtDateDay",txtDateDay,_arrWajbat)
       
        // Access the navigation controller if you want from child
        if let parentPageViewController = self.parent {
            parentPageViewController.parent?.navigationItem.title = itemText
        }
    }
    
    
    var dateLabel: UILabel = UILabel()
     
    internal var _title : NSString = "Page Zero"
    internal var _setupSubViews:Bool = false
    internal var _arrWajbat = [tblWajbatUserMealPlanner]()
    var MainView:ViewController!
//    var id_items = "1"
    var idWajbeh = ""
    var txtDateDay = ""
    var delegateMain:ViewController!
    var pg:protocolWajbehInfoTracker?
    init(title : NSString) {
        _title = title

        super.init(nibName: nil, bundle: nil)
    }
    init() {
         super.init(nibName: nil, bundle: nil)
     }
    init(arr : [tblWajbatUserMealPlanner],MainView:ViewController,txtDateDay:String,pg:protocolWajbehInfoTracker,delegateMain:ViewController) {
 //        _arrWajbat = arr
        self.pg = pg
         self.MainView = MainView
         self.txtDateDay = txtDateDay
        self.delegateMain = delegateMain
        
         super.init(nibName: nil, bundle: nil)
     }
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         fatalError("init(coder:) has not been implemented")
     }
     var tblView = UITableView(frame: CGRect(x: 0,y: 0,width: 300,height: 300))
     

     
     func getDateOnly()->String{
         
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd"
         let currentDate = Date()
         return dateFormatter.string(from: currentDate)
     }
     func setupTableView() {
         self.view.addSubview(tblView)
         tblView.translatesAutoresizingMaskIntoConstraints = false
         tblView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor,constant: 10).isActive = true
         tblView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
         tblView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         tblView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     func setupLabel() {
         self.view.addSubview(dateLabel)
         dateLabel.textAlignment = .center
         dateLabel.translatesAutoresizingMaskIntoConstraints = false
         dateLabel.topAnchor.constraint(equalTo: view.topAnchor,constant: 10).isActive = true
         dateLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
 //        dateLabel.bottomAnchor.constraint(equalTo: tblView.topAnchor).isActive = true
         dateLabel.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     override func viewWillLayoutSubviews() {
         setupLabel()
         setupTableView()
         //self.presentLabel.center = self.view.center
     }
}

extension ItemViewController:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if _arrWajbat.isEmpty {
           TableViewHelper.EmptyMessage(message: "لا يوجد لديك نظام غذائي،يرجى الاشتراك الان", viewController: tblView)
           return 0
       }else {
       return _arrWajbat.count
       }
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = _arrWajbat[indexPath.row]
        let cell = Bundle.main.loadNibNamed("CustomCell", owner: self, options: nil)?.first as! CustomCell
        cell.lblName.text = item.packgeName
        cell.lblCaloris.text = " \(item.packagesCalories) سعرة "
        cell.lblGram.text = " \(item.packagesWeights) غم "
//        cell.view = self
        cell.item = item
        cell.index = indexPath.row
        if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
            cell.isFavrite = true
            cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
        }else{
            cell.isFavrite = false
            cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
            cell.imgFav.alpha = 0.2
        }
        cell.lbltet.text = setTilePage(id:tblPackeg.getIDCategory(id: Int(item.refPackgeID)!,id_Cate:Int(item.refPackgeCateItme)!))
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

       let indexPathForFirstRow = IndexPath(row: indexPath.row, section: 0)
       let currentCell = tableView.cellForRow(at: indexPathForFirstRow)! as! CustomCell
       UIView.animate(withDuration: 0.1, delay: 0.01, options: .allowAnimatedContent) {
//
           currentCell.contentView.backgroundColor = .gray.withAlphaComponent(0)
       } completion: {  bool in
           currentCell.contentView.backgroundColor = UIColor(hexString: "#F2F2F7")
       }
       DisplayWajbh(index:indexPath.row)

    }
   func DisplayWajbh(index:Int){
       let item = _arrWajbat[index]

       if item.refPackgeCateItme == "1" {
           createEvent(key: "61", date: getDateTime())
       }else if item.refPackgeCateItme == "2" {
           createEvent(key: "63", date: getDateTime())
       }else if item.refPackgeCateItme == "3" {
           createEvent(key: "65", date: getDateTime())
       }else if item.refPackgeCateItme == "4" {
           createEvent(key: "67", date: getDateTime())
       }else if item.refPackgeCateItme == "5" {
           createEvent(key: "69", date: getDateTime())
       }
       
       let detailView = MainView.storyboard!.instantiateViewController(withIdentifier: "showWajbehMealPlanner") as! showWajbehMealPlanner
       detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
       detailView.transitioningDelegate = self
       detailView.date = item.date
       detailView.asMealPlanner = true
       detailView.idPackge = Int(item.refPackgeID)
       detailView.id_items = item.refPackgeCateItme
       detailView.flg_category_mealPlanner = Int(item.refPackgeCateItme)!
       detailView.pg = pg
       if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
           detailView.isFavrite = true
       }else{
           detailView.isFavrite = false
       }
       detailView.delegateMain = self.delegateMain
       detailView.itemMealPlanner = item
       detailView.flagFromMealPlanner = true
       tblUserWajbehEaten.checkFoundItemIsNotProposal(id:Int(item.refPackgeID)!, wajbehInfo:item.refPackgeCateItme, date: item.date, isProposal: "1", completion: { (id,bool) in
         print("bool \(bool)")
           detailView.idItemsEaten = id
           if bool == true {
               detailView.isEaten = true
           }
           else{
               detailView.isEaten = false
           }
       })
       present(detailView, animated: true, completion: nil)
   }
   
 
    func setTilePage(id:Int)->String{
        
        switch id {
        case 1:
            return "فطور"
        case 2:
            return "غداء"
        case 3:
            return "عشاء"
        case 4:
            return "تصبيرة صباحي"
        case 5:
            return "تصبيرة مسائي"
        default:
            print("not found !!")
        }
        return ""
    }
   

}


extension ItemViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}
