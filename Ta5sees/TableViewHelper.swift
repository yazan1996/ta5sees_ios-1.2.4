//
//  TableViewHelper.swift
//  Ta5sees
//
//  Created by Admin on 2/3/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class TableViewHelper {
    
    static func EmptyMessage(message:String, viewController:UITableView) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: viewController.bounds.size.width, height: viewController.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name:"GE Dinar One", size: 30)
        messageLabel.sizeToFit()
        
        viewController.backgroundView = messageLabel;
        viewController.separatorStyle = .none;
    }
    
    
    static func BTNEmptyMessage(message:String, viewController:UITableView)->UIButton {
        let rect = CGRect(origin: CGPoint(x: 0,y :viewController.frame.height*0.5), size: CGSize(width: viewController.bounds.size.width, height: 44))
        let btn = UIButton(frame: rect)
        btn.setTitle(message, for: .normal)
        btn.titleLabel?.textColor = .white
        btn.backgroundColor = UIColor(red: 106/255, green: 197/255, blue: 182/255,alpha: 1.0)
        btn.titleLabel?.textAlignment = .center;
        btn.titleLabel?.font = UIFont(name:"GE Dinar One", size: 20)!
        btn.titleLabel?.sizeToFit()
        btn.isHighlighted = true
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.layer.cornerRadius = btn.frame.height*0.5
        viewController.separatorStyle = .none;

        viewController.addSubview(btn)
        
        return btn
    }
    
}


class CollectionViewHelper {
    
    static func EmptyMessage(message:String, viewController:UICollectionView) {
        print("message",message)
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: viewController.bounds.size.width, height: viewController.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name:"GE Dinar One", size: 30)
        messageLabel.sizeToFit()
        viewController.backgroundView = messageLabel;
    }
    static func BTNEmptyMessage(message:String, viewController:UICollectionView)->UIButton {
        let rect = CGRect(origin: CGPoint(x: 0,y :viewController.frame.height*0.5), size: CGSize(width: viewController.bounds.size.width, height: 44))
        let btn = UIButton(frame: rect)
        btn.setTitle(message, for: .normal)
        btn.titleLabel?.textColor = .white
        btn.backgroundColor = UIColor(red: 106/255, green: 197/255, blue: 182/255,alpha: 1.0)
        btn.titleLabel?.textAlignment = .center;
        btn.titleLabel?.font = UIFont(name:"GE Dinar One", size: 20)!
        btn.titleLabel?.sizeToFit()
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.layer.cornerRadius = btn.frame.height*0.5
        viewController.addSubview(btn)

        return btn
    }
    
}
