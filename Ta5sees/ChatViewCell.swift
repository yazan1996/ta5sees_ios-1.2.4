//
//  ChatViewCell.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import SVProgressHUD

class ChatViewCell: UITableViewCell,AVAudioPlayerDelegate {
    var audioPlayer:AVAudioPlayer!
    var delegate:AudioDelegate?
    var url:String?
    var isPlayedAudio = false
    
    @IBOutlet weak var txtText: UILabel!
    @IBOutlet weak var btnAudioSender: UIButton!
    @IBOutlet weak var checkDielverd: UIImageView!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var txtrecive: LableColor!
  
    @IBOutlet weak var checkDeilverdAudio: UIImageView!
    func setText(msg:Message){
        txtText.text="\(msg.text!)"
    }
    @IBAction func btnReciverAudio(_ sender: Any) {
        SVProgressHUD.show()
        downloadFileFromURL(url:NSURL(string: url!)!, btn: btnAudioSender)
    }
    @IBAction func btnAudioRecord(_ sender: Any) {
        SVProgressHUD.show()

        if ((url?.isEmpty) == nil){
           return
        }else{
        downloadFileFromURL(url:NSURL(string: url!)!, btn: btnAudio )
        }
    }
    
    func downloadFileFromURL(url:NSURL,btn:UIButton){
        DispatchQueue.global().async {
            let task = URLSession.shared.downloadTask(with: url as URL) { localURL, urlResponse, error in
                do {
                    if localURL != nil {
                        self.play(url: localURL! as NSURL,btn:btn)
                    }else {
                        print("no Internet or something error")
                    }
                }
            }
            
            
            task.resume()
        }
        
        
    }
       
  
       
    func play(url:NSURL,btn:UIButton) {
       
           do {

               SVProgressHUD.dismiss()
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
//            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)

            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
               self.audioPlayer = try AVAudioPlayer(contentsOf: url as URL, fileTypeHint: AVFileType.m4a.rawValue)

               audioPlayer.prepareToPlay()
               audioPlayer.delegate = self
               audioPlayer.play()
               if isPlayedAudio {
                      audioPlayer.pause()
                      isPlayedAudio = false
                DispatchQueue.main.async {
                    btn.setImage(UIImage(named:"playAudio"), for: .normal)
                }
                  } else {
                      audioPlayer.play()
                DispatchQueue.main.async {
                    
                    btn.setImage(UIImage(named:"puaseAudio"), for: .normal)
            
                    print(self.audioPlayer!.duration)
                    _ = Timer.scheduledTimer(withTimeInterval:self.audioPlayer!.duration, repeats: false, block: { [self] timer in
                        self.isPlayedAudio = false
                        audioPlayer.stop()
                        try? AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)

                        btn.setImage(UIImage(named:"playAudio"), for: .normal)

                    })

                }
               }
           } catch {
               print("AVAudioPlayer init failed")
           }
       }

    func setTextWithImage(msg:Message){
        txtText.text="\(msg.text!)"
    }

}
protocol AudioDelegate {
    func playAudio(url: NSURL)
}


extension ChatViewCell {
    func setConvertTextToLink(input:String)->String{
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print("url input \(url)")
            return String(url) as String
        }
        return ""
    }

    @objc func tapped(sender : MyTapGesture) {

        var url_str = sender.title
        if !url_str.contains("https://") && !url_str.contains("http://"){
             url_str = "https://\(sender.title)"
        }
        if !verifyUrl(urlString: url_str) {
            print(" verifyUrl = ",false)
            return
        }
        let url = URL(string: url_str)

        if var comps = URLComponents(url: url!, resolvingAgainstBaseURL: false) {
            if comps.scheme == nil {
                comps.scheme = "http"
            }
            if let validUrl = comps.url {
                UIApplication.shared.open(validUrl, options: [:], completionHandler: nil)
            }
        }
    }
    func verifyUrl (urlString: String?) -> Bool {
        
       if let urlString = urlString {
           if let url = NSURL(string: urlString) {
               return UIApplication.shared.canOpenURL(url as URL)
           }
       }
       return false
   }
    func createLinke(txtText:UILabel,oneMessage:Message){
        
        if let i = imagCahce.object(forKey: txtText.attributedText! as NSAttributedString) as? NSAttributedString {
            DispatchQueue.main.async { 
                txtText.attributedText = i
            }
            return
        }
        
        if setConvertTextToLink(input:oneMessage.text!) != "" {

            let tap = MyTapGesture(target: self, action:  #selector(tapped(sender:)))
            tap.title = setConvertTextToLink(input:oneMessage.text!)
            txtText.isUserInteractionEnabled = true
            txtText.addGestureRecognizer(tap)
            txtText.textColor = .blue
            let textWithoutLink = oneMessage.text!.replacingOccurrences(of: setConvertTextToLink(input:oneMessage.text!), with: "")
            
            let attributedString1 = NSMutableAttributedString.init(string: oneMessage.text!)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: (oneMessage.text! as NSString).range(of:textWithoutLink))

            attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:17)!, range: (oneMessage.text! as NSString).range(of: setConvertTextToLink(input:oneMessage.text!)))
            txtText.attributedText = attributedString1
            imagCahce.setObject(txtText, forKey: txtText.attributedText!)
            

    
        }else{
            txtText.isUserInteractionEnabled = false
            txtText.textColor = .white
            txtText.text = oneMessage.text
        }
    }
}
