//
//  WajbehInfoCell.swift
//  Ta5sees
//
//  Created by Admin on 2/5/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class WajbehInfoCell: UITableViewCell {

    var idItem:Int!
    var view:ViewController!
    var id_category:String!

    @IBOutlet weak var kacl_item: UILabel!
    @IBOutlet weak var name_item: UILabel!
    @IBOutlet weak var main_view: UIView!
    var isWajbeh: String!

    func getNameItem(id:Int,isWajbeh:String)->String{
        if isWajbeh == "0"{
            let ob:tblPackeg? = setupRealm().objects(tblPackeg.self).filter("id == %@",id).first
            return ob?.name ?? "nil"
        }else if isWajbeh == "3" {
            let ob:tblTamreenType? = setupRealm().objects(tblTamreenType.self).filter("id == %@",id).first!
            return ob?.discription ?? "nil"
        }else{
            let ob:tblSenf? = setupRealm().objects(tblSenf.self).filter("id == %@",id).first!
            return ob?.trackerName ?? "nil"
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        kacl_item.translatesAutoresizingMaskIntoConstraints = false
        name_item.translatesAutoresizingMaskIntoConstraints = false
        let tap = UITapGestureRecognizer(target: self, action: #selector((self.Tapped(_:))))
         tap.numberOfTapsRequired=2
         tap.delaysTouchesEnded = true
         let tapLong = UILongPressGestureRecognizer(target: self, action: #selector((self.Tapped(_:))))
         tapLong.minimumPressDuration = 0.3
         tapLong.delaysTouchesEnded = true
         
         //        tapLong.numberOfTapsRequired=2
         main_view.addGestureRecognizer(tapLong)
         main_view.addGestureRecognizer(tap)
         tap.require(toFail: tapLong)
    }
    @objc func Tapped(_ gestureRecognizer: UITapGestureRecognizer){
           if gestureRecognizer.numberOfTapsRequired == 2 {
            deleteItem(view:view,index:idItem)
           }else{
               if gestureRecognizer.state != UIGestureRecognizer.State.ended  {
                   
               }else{
                showAlertWithThreeButton(view:view)
               }
               
           }
       }
    func showAlertWithThreeButton(view:ViewController) {
        var message:String!
        if id_category != "6"{
            message = "هل انت متاكد من حذف : \(getNameItem(id:view.getNameItemBreakfast(index:idItem, id: id_category),isWajbeh:isWajbeh)) ؟"
        }else {
             message = "هل انت متاكد من حذف التمرين؟"
        }
        let alert = UIAlertController(title: "حذف الوجبة", message:message, preferredStyle: .alert)

           alert.addAction(UIAlertAction(title: "إالغاء", style: .default, handler: { (_) in
               print("You've pressed cancel")
           }))

           alert.addAction(UIAlertAction(title: "حذف", style: .destructive, handler: { (_) in
            self.deleteItem(view:view,index:self.idItem)
           }))
           view.present(alert, animated: true, completion: nil)
       }
    func deleteItem(view:ViewController,index:Int){
        if id_category == "1" {
            view.deletBreakfast(index:self.idItem)
        }else  if id_category == "2" {
            view.deletLunsh(index:self.idItem)
        }else  if id_category == "3" {
            view.deletDinner(index:self.idItem)
        }else  if id_category == "4" {
            view.deletTabsera1(index:self.idItem)
        }else  if id_category == "5" {
            view.deletTabsera2(index:self.idItem)
        }else  if id_category == "6" {
            view.deletTamreen(index:self.idItem)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
protocol ButtonClicableHeader {
    func tap(ob:tblUserWajbehForWeek)
}

