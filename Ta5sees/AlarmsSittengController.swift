//
//  AlarmsSittengController.swift
//  Ta5sees
//
//  Created by Admin on 9/28/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SwiftyJSON

class AlarmsSittengController: UIViewController,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var lblWeight: UILabel!
    @IBOutlet weak var lblTamreen: UILabel!
    @IBOutlet weak var lblWater: UILabel!
    @IBAction func dissmis(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var btnTamreenOutlet: UIButton!
    lazy var obpresnterAccount = PresnterAccount(with:self)
    
    @IBOutlet weak var switchWajbatOutlet: UISwitch!
    @IBOutlet weak var switchWeightOutlet: UISwitch!
    @IBOutlet weak var btnWeightOutlet: UIButton!
    @IBOutlet weak var btnWaterOutlet: UIButton!
    @IBOutlet weak var switchTamreenOutlet: UISwitch!
    @IBOutlet weak var switchWaterOutlet: UISwitch!
    @IBOutlet weak var stackAlarmWajbh: UIStackView!
    var datePicker: UIDatePicker = UIDatePicker()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        checkCenterNotificationWajbat { [self] (bool) in
            if bool {
                DispatchQueue.main.async {
                    switchWajbatOutlet.isOn = true
                }
            }else{
                DispatchQueue.main.async {
                    switchWajbatOutlet.isOn = false
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnWaterOutlet.setTitle(tblAlermICateItems.getalertNotification(cateID: "7"), for: .normal)
        btnWeightOutlet.setTitle(tblAlermICateItems.getalertNotification(cateID: "8"), for: .normal)
        btnTamreenOutlet.setTitle(tblAlermICateItems.getalertNotification(cateID: "6"), for: .normal)
        stackAlarmWajbh.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        
        if tblAlermICateItems.getalertNotificationActive(cateID:"7") == "0" {
            switchWaterOutlet.isOn = true
        }else{
            switchWaterOutlet.isOn = false
            
        }
        
        if tblAlermICateItems.getalertNotificationActive(cateID:"6") == "0" {
            switchTamreenOutlet.isOn = true
        }else{
            switchTamreenOutlet.isOn = false
            
        }
        if tblAlermICateItems.getalertNotificationActive(cateID:"8") == "0" {
            switchWeightOutlet.isOn = true
        }else{
            switchWeightOutlet.isOn = false
        }
        
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        showDialogCateInfo()
        switchWajbatOutlet.isOn = true
    }
    
    @IBAction func switchWajbat(_ sender: UISwitch) {
        if (sender.isOn == true){
            print("###")
            showDialogCateInfo()
        }
    }
    @IBAction func btnAlarmWeight(_ sender: Any) {
        TimePicker(id_Notification: "8",time: tblAlermICateItems.getalertNotificationForText(cateID: "8"))
    }
    @IBAction func btnAlarmTamreen(_ sender: Any) {
        TimePicker(id_Notification: "6",time: tblAlermICateItems.getalertNotificationForText(cateID: "6"))
    }
    @IBAction func btnAlarmWater(_ sender: Any) {
        TimePicker(id_Notification: "7",time: tblAlermICateItems.getalertNotificationForText(cateID: "7"))
    }
    @IBAction func switchAlarmWater(_ sender: UISwitch) {
        if (sender.isOn == true){
            let notfyInfo:tblAlermICateItems = tblAlermICateItems.getNotfyInfo(cateItem:"7")
            print("on")
            craeteNOtfy(hours:notfyInfo.alermHour,min:notfyInfo.alermMinuet,id:"7")
            tblAlermICateItems.updateAlertNotification(cateItem:"7",hour:Int(notfyInfo.alermHour),min:Int(notfyInfo.alermMinuet))
        }
        else{
            print("off")
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["7"])
            tblAlermICateItems.updateAlertNotificationActive(cateItem: "7")
        }
        deleteCenterNotification()
    }
    @IBAction func switchAlarmWeight(_ sender: UISwitch) {
        if (sender.isOn == true){
            let notfyInfo:tblAlermICateItems = tblAlermICateItems.getNotfyInfo(cateItem:"8")
            print("on")
            craeteNOtfy(hours:notfyInfo.alermHour,min:notfyInfo.alermMinuet,id:"8")
            tblAlermICateItems.updateAlertNotification(cateItem:"8",hour:Int(notfyInfo.alermHour),min:Int(notfyInfo.alermMinuet))
        }else{
            print("off")
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["8"])
            tblAlermICateItems.updateAlertNotificationActive(cateItem: "8")
        }
        deleteCenterNotification()
    }
    
    @IBAction func switchAlarmTamreen(_ sender: UISwitch) {
        if (sender.isOn == true){
            let notfyInfo:tblAlermICateItems = tblAlermICateItems.getNotfyInfo(cateItem:"6")
            print("on")
            craeteNOtfy(hours:notfyInfo.alermHour,min:notfyInfo.alermMinuet,id:"6")
            tblAlermICateItems.updateAlertNotification(cateItem:"6",hour:Int(notfyInfo.alermHour),min:Int(notfyInfo.alermMinuet))
        }
        else{
            print("off")
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["6"])
            tblAlermICateItems.updateAlertNotificationActive(cateItem: "6")
        }
        deleteCenterNotification()
    }
    
    
    func craeteNOtfy(hours:Int,min:Int,id:String){
        var titleNotfy = ""
        if id == "7" {
            titleNotfy =  getTitleNotify(keyRemote:"waterRemindersTitles",keyValue:"waterTitle",sheardKey:"waterTitleCounter")
            showNotify(id:id,body:titleNotfy,hour:hours,minut:min)
            
        }else if id == "6"{
            titleNotfy =  getTitleNotify(keyRemote:"exerciseRemindersTitles",keyValue:"exerciseTitle",sheardKey:"exerciseTitleCounter")
            showNotify(id:id,body:titleNotfy,hour:hours,minut:min)
        }else if id == "8"{
            titleNotfy =  getTitleNotify(keyRemote:"weightRemindersTitles",keyValue:"weightTitle",sheardKey:"weightTitleCounter")
            showNotify(id:id,body:titleNotfy,hour:hours,minut:min)
        }
    }
    
    //
    //    func getTitleNotify(keyRemote:String,keyValue:String,sheardKey:String)->String{
    //        var titleNotfy = ""
    //        var count = getDataFromSheardPreferanceInt(key: sheardKey)
    //
    //        if let recommends = JSON(remoteConfig[keyRemote].jsonValue!).array {
    //            titleNotfy = recommends[count][keyValue].stringValue
    //        }
    //
    //        count+=1
    //        if count > 4{
    //            count = 0
    //        }
    //        setDataInSheardPreferanceInt(value:count, key: sheardKey)
    //        return titleNotfy
    //    }
    //    func showAlertDateTime(id_notification:String) {
    //
    //        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertController") as! CustomAlertController
    //        popoverContent.id_Notification = id_notification
    //        popoverContent.viewAccountPresnter =  obpresnterAccount
    //        let nav = UINavigationController(rootViewController: popoverContent)
    //        nav.modalPresentationStyle = UIModalPresentationStyle.popover
    //        let popover = nav.popoverPresentationController
    //        popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
    //        popover!.delegate = self
    //        popover!.sourceView = self.view
    //        popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)
    //        popoverContent.popoverPresentationController?.passthroughViews?.removeAll()
    //        self.present(nav, animated: true, completion: nil)
    //
    //    }
    
    var pd:protocolGeneral?
    
    func showAlertDateTime(id_notification:String){
        
        let childVC = storyboard!.instantiateViewController(withIdentifier: "CustomAlertController") as! CustomAlertController
        childVC.pd = self
        childVC.id_Notification = id_notification
        childVC.isFlagController = "1"
        let segue = BottomCardSegue(identifier: nil, source: self, destination: childVC)
        segue.customHright = 200
        prepare(for: segue, sender: id_notification)
        view.isUserInteractionEnabled = false
        segue.perform()
        
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
        
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    
    func TimePicker(id_Notification:String,time:String) {
        DPPickerManager.shared.showPicker(title: "الوقت", time: time, flag: 1, idTime: id_Notification, picker: { (picker) in
            //            picker.date = Date()
            picker.datePickerMode = .time
            picker.datePickerMode = UIDatePicker.Mode.time
            picker.locale = NSLocale(localeIdentifier: "da_DK") as Locale
        }) { (date, cancel) in
            if !cancel {
                self.selectDate(date: date!, id_Notification: id_Notification)
                //
            }
        }
    }
    
    func selectDate(date:Date,id_Notification:String){
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        var isActive = "1" // 1-> not active
        print("hour",hour,"minute",minute)
        
        var hourAS12 = -1
        var isMorning = ""
        var minuet = ""
        
        if hour > 12 {
            isMorning = "PM"
            hourAS12 = hour - 12
        }else{
            isMorning = "AM"
            hourAS12 = hour
        }
        if minute == 0 {
            minuet = "00"
        }else {
            minuet = String(minute)
        }
        if tblAlermICateItems.getalertNotificationActive(cateID:id_Notification) == "0" {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id_Notification])
            if id_Notification == "6" {
                craeteNOtfy(hours:hour,min:minute, id: id_Notification)
            }else  if id_Notification == "7" {
                craeteNOtfy(hours:hour,min:minute, id: id_Notification)
            }else  if id_Notification == "8" {
                craeteNOtfy(hours:hour,min:minute, id: id_Notification)
            }
            isActive = "0"
        }
        if id_Notification == "7" {
            btnWaterOutlet.setTitle("\(hourAS12):\(minuet) \(isMorning)", for: .normal)
        }else if id_Notification == "6" {
            btnTamreenOutlet.setTitle("\(hourAS12):\(minuet) \(isMorning)", for: .normal)
        }else if id_Notification == "8" {
            btnWeightOutlet.setTitle("\(hourAS12):\(minuet) \(isMorning)", for: .normal)
        }else{
            print("no select date")
        }
        
        tblAlermICateItems.updateAlertNotification1(cateItem:id_Notification,hour:hour,min:minute,isActive:isActive)
        
        print(hour)
        print(minute)
        
        
    }
}

extension AlarmsSittengController {
    func showDialogCateInfo(){
        
        let mealDistbution = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).last
        let alert = UIAlertController(title: "اختر نوع الوجبة اليومية", message: nil , preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
        let breakfast = UIAlertAction(title:  "الافطار", style: .default, handler: { (action) -> Void in
            if mealDistbution?.Breakfast == 0 {
                showToast(message: "لا يمكنك اضافة تنبيه بناءا على اختيارك الوجبات الخاصه بك", view: self.view, place: 1)
                return
            }
            self.showAlertDateTime(id_notification: "1")
            
        })
        
        let tasbera1 = UIAlertAction(title:  "تصبيرة  صباحي", style: .default, handler: { (action) -> Void in
            if mealDistbution?.Breakfast == 0 {
                showToast(message: "لا يمكنك اضافة تنبيه بناءا على اختيارك الوجبات الخاصه بك", view: self.view, place: 1)
                return
            }
            self.showAlertDateTime(id_notification: "4")
            
        })
        
        let launsh = UIAlertAction(title:  "الغداء", style: .default, handler: { (action) -> Void in
            if mealDistbution?.Breakfast == 0 {
                showToast(message: "لا يمكنك اضافة تنبيه بناءا على اختيارك الوجبات الخاصه بك", view: self.view, place: 1)
                return
            }
            self.showAlertDateTime(id_notification: "2")
            
        })
        
        
        let tasbera2 = UIAlertAction(title:  "تصبيرة مسائي", style: .default, handler: { (action) -> Void in
            if mealDistbution?.Breakfast == 0 {
                showToast(message: "لا يمكنك اضافة تنبيه بناءا على اختيارك الوجبات الخاصه بك", view: self.view, place: 1)
                return
            }
            self.showAlertDateTime(id_notification: "5")
            
        })
        let dinner = UIAlertAction(title:  "العشاء", style: .default, handler: { (action) -> Void in
            if mealDistbution?.Breakfast == 0 {
                showToast(message: "لا يمكنك اضافة تنبيه بناءا على اختيارك الوجبات الخاصه بك", view: self.view, place: 1)
                return
            }
            self.showAlertDateTime(id_notification: "3")
            
        })
        
        
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: {(action) -> Void in
        }))
        
        let imgbreakfast = resizeImage(image: UIImage(named: "breakfast.png")!, targetSize: CGSize(width: 25.0, height: 25.0))
        breakfast.setValue(imgbreakfast.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(breakfast)
        
        let imgTasbera1 = resizeImage(image: UIImage(named: "apple.png")!, targetSize: CGSize(width: 25.0, height: 25.0))
        tasbera1.setValue(imgTasbera1.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(tasbera1)
        
        let imgLaunsh = resizeImage(image: UIImage(named: "launch.png")!, targetSize: CGSize(width: 25.0, height: 25.0))
        launsh.setValue(imgLaunsh.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(launsh)
        
        let imgTasbera2 = resizeImage(image: UIImage(named: "tsbera.png")!, targetSize: CGSize(width: 25.0, height: 25.0))
        tasbera2.setValue(imgTasbera2.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(tasbera2)
        
        let imgDinner = resizeImage(image: UIImage(named: "dinner.png")!, targetSize: CGSize(width: 25.0, height: 25.0))
        dinner.setValue(imgDinner.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), forKey: "image")
        alert.addAction(dinner)
        
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    
}
extension AlarmsSittengController:viewAccount{
    func setNewTarget(val: String) {
        
    }
    
    func setAlpha(flag:Bool) {
        
        checkCenterNotificationWajbat { [self] (bool) in
            if bool {
                DispatchQueue.main.async {
                    print("#########")
                    switchWajbatOutlet.isOn = true
                }
            }else{
                DispatchQueue.main.async {
                    print("#########")
                    switchWajbatOutlet.isOn = false
                }
            }
        }
        showDialogCateInfo()
    }
    
    
}

