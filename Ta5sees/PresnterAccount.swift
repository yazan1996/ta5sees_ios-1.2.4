//
//  presnterAccount.swift
//  Ta5sees
//
//  Created by Admin on 9/22/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import SVProgressHUD


class PresnterAccount {
    
    var view:viewAccount!
    
    init(with view:viewAccount) {
        self.view = view
    }
    func setAlphaView(flag:Bool){
        view.setAlpha(flag: flag)
    }
    func setNewTargets(val:String){
        view.setNewTarget(val:val)
       }
   
    func showDialog(){
        SVProgressHUD.show()
//        SVProgressHUD.showInfo(withStatus: "جاري تحدث البيانات")
    }
    func hidDialog(){
        SVProgressHUD.dismiss()
    }
}


protocol viewAccount:class {
    func setAlpha(flag:Bool)
    func setNewTarget(val:String)
 
}
