//
//  HomeShoppingController.swift
//  Ta5sees
//
//  Created by Admin on 7/28/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import CarbonKit
import SVProgressHUD
//Ta5sees/Ta5sees-Bridging-Header.h


class HomeShoppingController : UIViewController, CarbonTabSwipeNavigationDelegate {
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 1 {
            let page = storyboard.instantiateViewController(withIdentifier: "FridgeController") as! FridgeList
            page.count = count
            return page
        }
        let page = storyboard.instantiateViewController(withIdentifier: "TableShoppiongController") as! ShoppingList
        page.count = count

        return page
    }
    
    var count = 7
    var flagSetImageAction:String!
    let color: UIColor = UIColor(red: 94.0 / 255, green: 204.0 / 255, blue: 156.0 / 255, alpha: 1)
    let colorSwip: UIColor = UIColor(red: 156.0 / 255, green: 171.0 / 255, blue: 179.0 / 255, alpha: 1)
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!

    override func viewDidLoad(){
        super.viewDidLoad()
        
        //self.title = "المطبخ"
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: ["قائمة التسوق","مطبخي"], delegate: self)
        carbonTabSwipeNavigation.setTabExtraWidth(2)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(200, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(200, forSegmentAt: 1)
        carbonTabSwipeNavigation.setNormalColor(color,font: UIFont(name: "GE Dinar One", size: 15)!)
        carbonTabSwipeNavigation.setSelectedColor(colorSwip, font: UIFont(name: "GE Dinar One", size: 16)!)
        //
     
        
     
        self.style()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
//        if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 2{
//            DispatchQueue.main.asyncAfter(deadline: .now()) { [self] in // Change `2.0` to the desired number of seconds.
//
//                //        navigationController!.view.frame = CGRect(x: 0, y: 80, width: view.frame.width, height: view.frame.height-80)
//                navigationController!.view.frame.origin.y = 50
//                navigationController!.view.frame.size.height-=50
//                let v2 = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
//                v2.backgroundColor = UIColor.white
//
//                let button = UIButton(type: .system)
//                button.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
//                button.setTitle(NSLocalizedString("Button", comment: "Button"), for: .normal)
//                button.backgroundColor = .green
////                button.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
//                v2.addSubview(button)
//                window.addSubview(v2)
//            }
//        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func btnFilter(_ sender: Any) {
           filterDialog()
    }
  
    
    func style() {
//        self.navigationController!.navigationBar.isTranslucent = false
//        self.navigationController!.navigationBar.tintColor = UIColor.white
//        self.navigationController!.navigationBar.barTintColor = color
//        self.navigationController!.navigationBar.barStyle = .blackTranslucent
    }
    
    func filterDialog(){
        let alert = UIAlertController(title: "", message: nil , preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
      
 
        let btn0 = UIAlertAction(title: "مكونات لاسبوع", style: .default, handler: { [self] _ in
            flagSetImageAction = "0"
            filterDataList(count:7)
        })
        btn0.accessibilityIdentifier = "0"
        setImageAction(alert:alert,alertAction:btn0)
        
        let btn1 = UIAlertAction(title: "مكونات لاسبوع ٢", style: .default, handler: { [self] _ in
            flagSetImageAction = "1"
            filterDataList(count:14)
        })
        btn1.accessibilityIdentifier = "1"
        setImageAction(alert:alert,alertAction:btn1)
        let btn2 = UIAlertAction(title: "مكونات لاسبوع ٣", style: .default, handler: { [self] _ in
            flagSetImageAction = "2"
            filterDataList(count:21)
        })
        btn2.accessibilityIdentifier = "2"
        setImageAction(alert:alert,alertAction:btn2)
        let btn3 = UIAlertAction(title: "مكونات لاسبوع ٤", style: .default, handler: { [self] _ in
            flagSetImageAction = "3"
            filterDataList(count:31)
        })
        btn3.accessibilityIdentifier = "3"
        setImageAction(alert:alert,alertAction:btn3)
        
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
            
        }))
       

        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func setImageAction(alert:UIAlertController,alertAction:UIAlertAction){
        if alertAction.accessibilityIdentifier == flagSetImageAction {
            let img = resizeImage(image: #imageLiteral(resourceName: "notDeliverd"), targetSize: CGSize(width: 20.0, height: 20.0))

        alertAction.setValue(img, forKey: "image")
        }
        
            alert.addAction(alertAction)
        
    }
    
    func filterDataList(count:Int){
        self.count = count
        carbonTabSwipeNavigation.currentTabIndex = 0
        _ = carbonTabSwipeNavigation(carbonTabSwipeNavigation, viewControllerAt: 0)
        
        _ = carbonTabSwipeNavigation(carbonTabSwipeNavigation, viewControllerAt: 1)
        viewDidLoad()
        if carbonTabSwipeNavigation.currentTabIndex == 0 {
          
            viewDidLoad()
        }else {
          
            carbonTabSwipeNavigation.currentTabIndex = 1
        }
    }
 
    
    
}
