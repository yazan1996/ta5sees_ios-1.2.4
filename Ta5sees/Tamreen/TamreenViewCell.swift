//
//  TamreenViewCell.swift
//  Ta5sees
//
//  Created by Admin on 9/13/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class TamreenViewCell: UITableViewCell , UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return arr.count
    }
    
   
   
    @IBOutlet weak var colletionsview: UICollectionView!
    
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell:TamreenCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TamreenCollectionCell", for: indexPath) as! TamreenCollectionCell
        setRadusLabel(lbl:cell.lblname)
        cell.semanticContentAttribute = .forceRightToLeft
//        cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        cell.lblname.text = arr[indexPath.row].discription
//        cell.lblname.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        if indexPath.row == 0 {
            view.btnInVisable.frame.size.width = cell.lblname.frame.width
            view.btnInVisable.frame.size.height = cell.lblname.frame.height + 5
            view.btnInVisable.frame.origin.y = view.frameCell.origin.y+7
////            view.btnInVisable.frame = convertLocationItems(location:cell.frame)
////            view.btnInVisable.frame.size.width = 200
////            view.frameCellcollection.width = 150
////            view.btnInVisable.frame.size.height = 50
////            view.btnInVisable.frame.size.height = 50
////            view.btnInVisable.frame.size.width = cell.frame.width
////            view.btnInVisable.frame.origin.y =  convertLocationItems(location:cell.frame).origin.y
//            view.frameCellcollection = convertLocationItems(location:cell.frame,cell:cell)
//            view.frameCellcollection.origin.y =  convertLocationItems(location:cell.frame,cell:cell).origin.y-10
        }
        return cell
    }
    
    func convertLocationItems(location:CGRect,cell:TamreenCollectionCell)->CGRect{
        
        let l12 = colletionsview.convert(location, to: view.tblView)
//        let l3 = colletionsview.convert(l12, to: view.cellTamren)
//        let l4 = view.cellTamren.convert(l3, to: view.tblView)
        
       return view.tblView.convert(l12, to: view.view.superview)
 
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("### \(indexPath.row)")
        view.goToPage(id:arr[indexPath.row].id,title:arr[indexPath.row].discription!)
    }
    
    func setRadusLabel(lbl:UILabel){
          lbl.layer.cornerRadius = 15
//          lbl.font = UIFont(name: "GE Dinar One", size: 17)
      }
      
    var arr=[tblTamreenType]()
    var view:TamreenTypeListController!
    func setArray(ar:[tblTamreenType]){
        arr = ar
        colletionsview.reloadData()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        colletionsview.delegate = self
        colletionsview.dataSource = self
//        colletionsview.semanticContentAttribute = .forceLeftToRight
//        colletionsview.transform = CGAffineTransform(scaleX: -1, y: 1)
        colletionsview.reloadData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}



protocol viewTamreen {
    func goToPage(id:Int,title:String)
}

