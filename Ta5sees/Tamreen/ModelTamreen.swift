//
//  ModelTamreen.swift
//  Ta5sees
//
//  Created by Admin on 9/13/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation


class ModelTamreen {
    var view:tamreenViews
    var model = [modelTamreen]()

    init(with view: tamreenViews) {
        self.view = view
    }
    
    
    func getListTamreen(){
        tblTamreenActivity.getTamreenName { (arr) in
            
            for i in arr {
                tblTamreenType.getTamreenTypeData(id: String(i.id)) { (arrTamreenType) in
                    let item = modelTamreen(TamreenMain: i.discription!,
                        id:i.id, sub: arrTamreenType)
                    self.model.append(item)
                }
            }
            self.view.setTamreenData(list:self.model)

        }
    }
    func getTamreenDetails(){
         tblTamreenActivity.getTamreenName { (arr) in
             
             for i in arr {
                 tblTamreenType.getTamreenTypeData(id: String(i.id)) { (arrTamreenType) in
                     let item = modelTamreen(TamreenMain: i.discription!,
                         id:i.id, sub: arrTamreenType)
                     self.model.append(item)
                 }
             }
             self.view.setTamreenData(list:self.model)

         }
     }
    
    
}


class ModelTamreenDeteals {
    var view:viewSetUnitTamreen
    var model = [modelTamreen]()

    init(with view: viewSetUnitTamreen) {
        self.view = view
    }
    
    
    func getTamreenDetails(id:Int,unit:Int,time:Int){
        let weightPerson = Double(tblUserHistory.getLastWeight()) ?? 1.0
        var  weight = Int(round((weightPerson*2.2)/10.0))
        if weight <= 0{
            weight = 1
        }
            if weight < 11 {
            weight = 11
            }else if weight < 25 {
                if weight % 2 == 0{
                    weight+=1
                }
            }else if weight > 25 {
                weight = 25
        }
        tblTamreenWeights.getItem(id: id,weight :weight ) { (item) in
            self.view.setData(ob:item, unit: unit,time:time )
        }
     }
    
    
}


protocol tamreenViews:class {
    func setTamreenData(list:[modelTamreen])



}
protocol viewSetUnitTamreen {
    
    func setUnit(unit:Int)
    func setData(ob:tblTamreenWeights,unit:Int,time:Int)
}
