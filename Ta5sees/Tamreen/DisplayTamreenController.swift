//
//  DisplayTamreenController.swift
//  Ta5sees
//
//  Created by Admin on 9/14/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class DisplayTamreenController: UIViewController {
    var ratio = 1.0
    var idTamreen:Int!
    var tmreenItem:tblTamreenWeights!
    var titlePage = ""
    lazy var obModelTamreenDeteals = ModelTamreenDeteals(with: self)
    var date = ""
    var refprotocolGeneralTamreen:protocolGeneral?
    @IBOutlet weak var btnAddOutlet: ButtonColorRaduis!
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var stackButton: UIStackView!
    @IBOutlet weak var btnUnitOutlet: UIButton!
    @IBOutlet weak var dropDown: HADropDown!
    var timePlayOutlet = "-1"

//    @IBOutlet weak var timePlayOutlet: UITextField!
    @IBOutlet weak var txtCAlories: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDown.delegate = self
        dropDown.items = ["1","5","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90","95","100","105","110","115","120","125","130","135","140","145","150","155","160","165","170","175","180","185","190","195","200","205","210","215","220","225","230","235","240","245","250","255","260","265","270","275","280","285","290","295","300"]
        navigationBar.topItem!.title = titlePage
//        timePlayOutlet.clearsOnBeginEditing = true
        obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:1, time: 1)
        btnUnitOutlet.backgroundColor = .clear
        btnUnitOutlet.layer.cornerRadius = 5
        btnUnitOutlet.layer.borderWidth = 0.5
        btnUnitOutlet.layer.borderColor = colorGray.cgColor
        btnAddOutlet.titleLabel?.font = UIFont(name: "GE Dinar One", size: 17)

    }
    @objc func setAutText(sender: Any){
        dropDown.r()
    }
    func convertLocationItems(location:CGRect)->CGRect{
        let l1 = stackButton.convert(location, to: subview)
        return subview.convert(l1, to: self.view)
    }
  
    func showTutorialAddTamreen() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)

        let focusRect1 = convertLocationItems(location:btnUnitOutlet.frame)

        let message1 =  "إختر وحدة الوقت"
        let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame1 = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect1.maxY + 30, width: 72, height: 72)
        var tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame1)
        tut1.isArrowHidden = true

        let tutorials = [tut1]
         tutorialVC .tutorials = tutorials
         tutorialVC .DisplayTamreen = self
         tutorialVC .showAlertUnit = true
//         tutorialVC.isFlagTamreen = true
         tutorialVC .showInViewController(self)
    }
   
    func showTutorialAddTamreenShowAlert(location:CGRect) {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        let focusRect1 = location
        let focusRect2 = convertLocationItems(location:dropDown.frame)
        var f = focusRect2
        f.size.height = 40
        f.origin.y = f.origin.y+f.height
        let focusRect4 = f//convertLocationItems(location:dropDown.frame)
        let focusRect3 = subview.convert(btnAddOutlet.frame, to: self.view)

        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame1 = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect1.maxY + 30, width: 72, height: 72)
        let icon3Frame2 = CGRect(x: focusRect2.origin.x, y: focusRect2.maxY + 30, width: 72, height: 72)
        let icon3Frame3 = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect3.maxY + 30, width: 72, height: 72)
        
        let message1 =   "إختر وحدة الوقت"
        let message2 =  "قم بتحديد مدة اللعب"
        let message3 =  "اضف التمرين"
        let message4 =   "قم بتحديد مدة اللعب"

        let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
        
        var tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame1)
        tut1.isArrowHidden = true
        
        var tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message2, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame2)
              tut2.isArrowHidden = true
        
        var tut3 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect3, text: message3, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame3)
        var tut4 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect4, text: message4, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame3)
        tut4.isArrowHidden = true

        tut3.isArrowHidden = true
        
        let tutorials = [tut1,tut2,tut3]
         tutorialVC .tutorials = tutorials
         tutorialVC .DisplayTamreen = self
         tutorialVC .showAlertUnit = false
        tutorialVC.presentedUIAlertController = true
        tutorialVC.presentedUIAlertControllerUnit = true
         tutorialVC.isFlagTamreen = true
         tutorialVC .showInViewController(self)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") == "1" {
            view.superview?.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) { [self] in
                showTutorialAddTamreen()
            }
        }
    }
    @IBOutlet weak var navigationBar: NavigationViewController!
    @IBAction func btnDissems(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnUnitTime(_ sender: Any) {
        showSimpleActionSheet()
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        if timePlayOutlet == "-1" {
            showToast(message: "يجب تحديد المدة", view: view, place: 1)
        }else{
        tblUserTamreen.setTamreenBurnHistory(iduser: getDataFromSheardPreferanceString(key: "userID"), idTamreen:String(idTamreen), periodTime: timePlayOutlet, date:date, calorisBurn:txtCAlories.text.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)){ (res, error) in
            let response:String = res as! String
            if response == "success"{
                tblDailyWajbeh.updateDataDaily(date: self.date,calorisBurned: Int(round(Double(self.txtCAlories.text.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))!)), flag: "burned"){ [self] (res, error) in
                    createEvent(key: "11",date: getDateTime())
                    if res as? String == "true"{
                        self.refprotocolGeneralTamreen?.TrackerTamreen()
                        showToast(message: "تم إضافة التمرين", view: self.view,place:0)
                        self.dismiss(animated: true, completion: nil)
                        print("Save Tamreen Burned : \(res!)")
                    }else{
                        self.refprotocolGeneralTamreen?.TrackerTamreen()
                        showToast(message: "تم إضافة التمرين", view: self.view,place:0)
                        self.dismiss(animated: true, completion: nil)
                        print("Save Tamreen Burned : \(res!)")
                    }
                }
            }
        }
        }
    }
    @IBAction func time_play(_ sender: Any) {
                let value = timePlayOutlet
            var unit = 1
            if btnUnitOutlet.titleLabel!.text == "ساعة" {
                unit = 60
            }else {
                unit = 1
            }
        if  timePlayOutlet != "-1" {
            obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:Int(value) ?? 1)
            ratio = Double(value)!
            }else {
             obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:1)
                ratio = 1.0
            }
    }
    

      
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          self.view.endEditing(true)
          return true
      }

    
  
}

extension DisplayTamreenController:viewSetUnitTamreen{
    func setData(ob: tblTamreenWeights,unit:Int,time:Int) {
        tmreenItem = ob
        if unit == 60 {
            btnUnitOutlet.setTitle("ساعة", for: .normal)
        }else if unit == 1 {
            btnUnitOutlet.setTitle("دقيقة", for: .normal)
        }
        let textCal =  "سعرات حرارية   \(Int(calculate(value:Double(tmreenItem!.calories), unit: unit, time: time)))"
        let attributedString1 = NSMutableAttributedString.init(string: textCal)
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:40)!, range: (textCal as NSString).range(of: "\(Int(calculate(value:Double(tmreenItem!.calories), unit: unit, time: time)))"))
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:20)!, range: (textCal as NSString).range(of: "سعرات حرارية"))
        
        txtCAlories.attributedText = attributedString1
        
    }
    
    
    func calculate(value:Double,unit:Int,time:Int)->Int{
        if unit == 1 {
        return Int(round(value/60.0)*Double(time))
        }else {
        return Int(round(value/60.0)*Double(time)*Double(unit))

        }
    }
    func setUnit(unit:Int) {
        
        if unit == 60 {
            btnUnitOutlet.setTitle("ساعة", for: .normal)
        }else if unit == 1 {
            btnUnitOutlet.setTitle("دقيقة", for: .normal)
        }
        var unit = 1
                   if btnUnitOutlet.titleLabel!.text == "ساعة" {
                       unit = 60
                   }else {
                       unit = 1
                   }
        if timePlayOutlet != "-1" {
            let value = timePlayOutlet
            obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:Int(value)!)

            ratio = Double(value)!
            
        }else {
       obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:1)

            ratio = 1.0
        }
    }
    



}
extension DisplayTamreenController {
    
    func showSimpleActionSheet() {
        
        let alert = UIAlertController(title: "حدد وحدة الوقت", message: nil , preferredStyle: .alert)

     
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title:"دقيقة", style: .default, handler: { (_) in
            self.setUnit(unit:1)
        }))
        
        alert.addAction(UIAlertAction(title:"ساعة", style: .default, handler: { (_) in
            self.setUnit(unit:60)
        }))
        

        
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: { [self] in
            if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") == "1" {
            var f = alert.view.frame
            f.size.height = 30
            f.origin.y = f.origin.y + 70
            f.origin.x = f.origin.x + 2
            f.size.width = f.size.width - 5
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) { [self] in
            showTutorialAddTamreenShowAlert(location:f)
            
            }
            }
        })
    }
    
    
}

extension DisplayTamreenController: HADropDownDelegate {
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(dropDown.items[index])")
        timePlayOutlet = dropDown.items[index]
        let value = timePlayOutlet
        var unit = 1
        if btnUnitOutlet.titleLabel!.text == "ساعة" {
            unit = 60
        }else {
            unit = 1
        }
        if  timePlayOutlet != "-1" {
            obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:Int(value) ?? 1)
            ratio = Double(value)!
        }else {
            obModelTamreenDeteals.getTamreenDetails(id:idTamreen,unit:unit,time:1)
            ratio = 1.0
        }
    }
    func didShow(dropDown: HADropDown) {
        
    }
    override class func didChangeValue(forKey key: String) {
    }
}
extension DisplayTamreenController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
