//
//  TamreenTypeListController.swift
//  Ta5sees
//
//  Created by Admin on 9/13/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class TamreenTypeListController: UIViewController ,UISearchBarDelegate{
    
    @IBOutlet weak var tblview: SelfSizedTableView!
    
    @IBOutlet weak var btnInVisable: UIButton!
    @IBOutlet weak var tblView: SelfSizedTableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var se:UISegmentedControl!
    var frameCell:CGRect!
    var frameCellcollection:CGRect!
    var arrayTamreen=[modelTamreen]()
    var refprotocolGeneralTamreen:protocolGeneral?
    var date = ""
    var titlePage = "تمرين"
    lazy var obModelTamreen = ModelTamreen(with: self)

    func disableController(){
        view.isUserInteractionEnabled = false
    }
    func enableController(){
        view.isUserInteractionEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tblview.delegate = self
        tblview.dataSource = self
        searchBar.delegate = self
        
        obModelTamreen.getListTamreen()
        if #available(iOS 13.0, *) {
        searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
        searchBar.searchTextField.textAlignment = .right
        }
    }
  
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        arrayTamreen = searchText.isEmpty ? arrayTamreen : arrayTamreen.filter { (item: modelTamreen) -> Bool in
            
            return item.TamreenMain.lowercased().contains(searchText.lowercased())
            
            
        }
        
        
        tblview.reloadData()
        
    }
    
    @IBAction func dismess(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DisplayTamreenController" {
            if let dis=segue.destination as?  DisplayTamreenController{
                if  let data=sender as? Int {
                    dis.idTamreen = data
                    dis.date = date
                    dis.titlePage = titlePage
                    dis.refprotocolGeneralTamreen=refprotocolGeneralTamreen
                    
                }
            }
        }
    }
    
}



extension TamreenTypeListController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TamreenViewCell = tblview.dequeueReusableCell(withIdentifier: "TamreenViewCell", for: indexPath) as! TamreenViewCell
        
        cell.view = self
        cell.arr = arrayTamreen[indexPath.section].sub
        
            cell.colletionsview.reloadData()
        
       
        if indexPath.row == 0 {
            frameCell = tblView.convert(cell.frame, to: self.view)
        }
        return cell
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        arrayTamreen.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  arrayTamreen[section].TamreenMain
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            
            headerView.contentView.backgroundColor = .white
            headerView.backgroundView?.backgroundColor = .white
            headerView.backgroundColor = .white
            headerView.textLabel!.backgroundColor = .white
            headerView.textLabel!.textColor = colorGray
            headerView.textLabel!.font = UIFont(name: "GE Dinar One", size: 22)
            headerView.textLabel!.textAlignment = .right
        }
        
        
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") == "1" {
            view.superview?.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) { [self] in
                self.showTutorial(flag: 1)
                self.perform(#selector(setAutText),with:0,afterDelay: 0.2)
            }
        }
        
    }
    @objc func setAutText(sender: Any){
        let word = "الركض"
        var index = sender as! Int

        let arr = Array(word)
        if index < arr.count {
        UIView.animate(withDuration: 0.2, animations: { [self] in
            searchBar.textField?.text!.append(arr[index])
            index = index+1
        }, completion: { [self]
            (value: Bool) in
            self.searchBar(self.searchBar, textDidChange: searchBar.text!)
            self.perform(#selector(setAutText),with:index,afterDelay: 0.2)
            
        })
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.40) { [self] in
            showTutorial(flag:2)
            }
        }
    }

    func showTutorial(flag:Int) {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        var tutorials: [KJTutorial] = []
        if flag == 1 {
            // tut1
            let focusRect1 =  tblView.convert(searchBar.frame, to: self.view)
            let message1 = "ابحث عن تمرين"
            let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
            let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect1.maxY + 30, width: 72, height: 72)
            let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
            var tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame)
            tut1.isArrowHidden = true
            tutorialVC .addTutTamreen1 = true
             tutorials = [tut1]
        }else if flag == 2 {

            let focusRect2 = btnInVisable.frame//btnInVisable.convert( btnInVisable.frame, to: self.view.superview)
            let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
            let icon3Frame = CGRect(x: self.btnInVisable.frame.origin.x, y: focusRect2.maxY + 12, width: 72, height: 72)
            let message2 = "اختر تمرين"
            let message1Center1 = CGPoint(x: view.bounds.width/2, y: focusRect2.maxY + 100)
            let tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message2, textPosition: message1Center1, icon: icon3, iconFrame: icon3Frame)
             tutorialVC .addTutTamreen = true
            tutorialVC .addTutTamreen1 = false
             tutorials = [tut2]
        }

         tutorialVC.tutorials = tutorials
        tutorialVC.TamreenTypeListCV = self
         tutorialVC.showInViewController(self)
    }
}


extension TamreenTypeListController : tamreenViews {
    func setTamreenData(list: [modelTamreen]) {
        arrayTamreen = list
        tblview.reloadData()
    }
}

extension TamreenTypeListController : viewTamreen {
    func goToPage(id:Int,title:String) {
        print("DisplayTamreenController10")
        createEvent(key: "10",date: getDateTime())
        titlePage = title
        self.performSegue(withIdentifier: "DisplayTamreenController", sender: id)
        
    }
}


struct modelTamreen {
    let TamreenMain: String
    let id: Int
    let sub:[tblTamreenType]
}

extension TamreenTypeListController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
