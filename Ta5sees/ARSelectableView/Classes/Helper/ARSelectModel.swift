//
//  ARSelectModel.swift
//  ARSelectableView
//
//  Created by Rohit Makwana on 05/10/20.
//  Copyright © 2020 Rohit Makwana. All rights reserved.
//

import UIKit

public class ARSelectModel {

    var title: String
    var id: String
    var isSelected: Bool
    var selectionType: ARSelectionType?
    var width: CGFloat = 0.0

    init(title: String, id:String,isSelected:Bool) {
        
        self.isSelected = isSelected
        self.id = id
        self.title = title
        self.width = UILabel().sizeForLabel(text: title, font: ARSelectableCell.titleFont).width
    }
    
    init(title: String, isSelected: Bool = false) {
        if ActivationModel.sharedInstance.refWajbehMain == "2" {
            
        }
        if title == "الغداء" {
            self.isSelected = true
        }else{
            self.isSelected = isSelected
        }
        self.title = title
        self.id = ""
        self.width = UILabel().sizeForLabel(text: title, font: ARSelectableCell.titleFont).width
    }
    
}
