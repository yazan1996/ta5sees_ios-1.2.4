//
//  LastOpenAppController.swift
//  Ta5sees
//
//  Created by Admin on 11/3/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
class LastOpenAppController: UIViewController {

    @IBOutlet weak var dietOrderValue: UISlider!
    @IBOutlet weak var target_wieght: HoshiTextField!
    @IBOutlet weak var wieght: HoshiTextField!
    @IBOutlet weak var txtNumberWeek: UILabel!
    @IBOutlet weak var lblPeriodTarget: UILabel!
    @IBOutlet weak var txtQuantitiEachWeek: UILabel!
    @IBOutlet weak var lblLayaCondition: UILabel!

    @IBOutlet weak var stackTarget: UIStackView!
    @IBOutlet weak var stackSeekBar: UIStackView!
    let color_Inactive =  UIColor(hex: 0x9CABB3)
    var incrementKACL:Double = 0.0
    var week:Double = 0.0
    var userInfo:tblUserInfo!
    var userProgressHistory:tblUserProgressHistory!
    var obselectionDelegate:selectionDelegate!
    var idFetnessRate:Int!
    let color_green = UIColor(red: 123/255, green: 222/255, blue: 179/255, alpha: 1.0)


    override func viewDidLoad() {
        super.viewDidLoad()
        getInfoUser { (item, error) in
            self.userInfo = item
          
        }
        
        getInfoHistoryProgressUser { (item, error) in
            self.userProgressHistory = item
        }
        
        wieght.clearsOnBeginEditing = true
        target_wieght.clearsOnBeginEditing = true
        
        clicableLabel(lbl:lblLayaCondition,tag:3)
        obselectionDelegate = self

        if userInfo.refPlanMasterID  == "1" {
            txtQuantitiEachWeek.text = "زيادة : ٠ غم لكل اسبوع"
            
        }else{
            txtQuantitiEachWeek.text = "تخسيس : ٠ غم لكل اسبوع"
            
        }
        
        if  self.userInfo.refPlanMasterID == "3" || self.userInfo.grown == "2" {
            target_wieght.isHidden = true
            let firstView1 =  self.stackSeekBar.arrangedSubviews[0]
            firstView1.isHidden = true
            
            let firstView2 =  self.stackTarget.arrangedSubviews[1]
            firstView2.isHidden = true
        }
        
        
        if userInfo.refPlanMasterID != "3" ||  getUserInfo().grown != "2" {
            calculateWeek()
        }
        

        
        wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        
        if userInfo.refPlanMasterID != "3"  || getUserInfo().grown != "2"{
          
        target_wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        target_wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
       
        }
    }
        
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }
  
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         //  flag 1 >>> cusin
         // flag 2 >>> alleagy
         //flag 3 >> layaqa
         if let dis=segue.destination as? MultiSeletetionView {
             if  let dictionary=sender as? [String:String] {
                 dis.title_Navigation = dictionary["title"]
                 dis.obselectionDelegate = self
                 dis.optionsSelected = dictionary["options"]
                 dis.flag = dictionary["flag"]
             }
         }
     }
    func clicableLabel(lbl:UILabel,tag:Int){
           lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
           lbl.tag = tag
           lbl.isUserInteractionEnabled = true
       }
    
    @objc func textFieldDidChange(textField: HoshiTextField) {

        let text =  FormatterHoshiTextField(str:textField).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        if text == "" { return }
       if textField.tag == 3 {
        
            target_wieght.text = ""
            if Double(text)! >= 30.0 && Double(text)! <= 299.0{
                customError(textFeild:textField,color:color_green,placeHolder:"الوزن")
                textField.text = text + " كغم"
                if  getUserInfo().grown != "2" && getUserInfo().refPlanMasterID  != "3"{
                    target_wieght.becomeFirstResponder()
                    calculateWeek()
                }else{
                    view.endEditing(true)
                }
            }else{
                customError(textFeild:textField,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }
            
        } else if textField.tag == 4 {
           
            
            let text_wieght = FormatterHoshiTextField(str:wieght!).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            if text_wieght == "" { return}
            if textField.text == "" { return}
            
            if  userInfo.refPlanMasterID == "1" {
                if  getUserInfo().grown != "2"{

                    if Double(textField.text!)! > Double(text_wieght)! && Double(textField.text!)! <= 299.0{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                 
                }else{
                    
                    customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من  \(Double(text_wieght)!+1.0)و ٢٩٩")
                    
                }
                    dietOrderValue.setValue(10.0, animated: true)
                    calculateWeek()
                }else{
                    view.endEditing(true)
                             }
                
            }else if userInfo.refPlanMasterID == "2" {
            if  getUserInfo().grown != "2"{
                if Double(textField.text!)! >= 30.0 && Double(textField.text!)! < Double(text_wieght)!{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                               }else{
                        customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من ٣٠  \(Double(text_wieght)!-1.0)و")
                }
                dietOrderValue.setValue(10.0, animated: true)
                calculateWeek()
            }else{
                view.endEditing(true)
                
            }
            }
            
            
            
            return
        }
    }
    @objc func textFieldDidBeign(textField: HoshiTextField) {
    
            let result =  textField.text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            textField.text = result
            
        
    }
    @IBAction func btnSave(_ sender: Any){
        
        let val_wieght = FormatterHoshiTextField(str:wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        var val_target_wieght:String?  = FormatterHoshiTextField(str:target_wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        if lblLayaCondition.text! == "اختر معدل اللياقة"{
            showAlert(str:"يجب اختيار معدل اللياقة",view:self)
            SVProgressHUD.dismiss()
            return
        }
        if getUserInfo().grown == "2" {
            if val_target_wieght == "" || val_target_wieght!.isEmpty{
                val_target_wieght = "0.0"
            }
             if val_wieght == ""  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
            }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }else{
//             processDietOrderForTEEChild()
            }
        }
        
        if val_target_wieght == "" || val_target_wieght!.isEmpty || val_target_wieght == "0"{
            val_target_wieght = "0.0"
        }
      
         if val_wieght == ""  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
        }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
        }else if val_target_wieght == "0.0" && userInfo.refPlanMasterID != "3"   && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ادخال الهدف")
        }else if userInfo.refPlanMasterID == "1" && Double(val_target_wieght ?? "0.0")! < 300.0 && Double(val_target_wieght ?? "0.0")! < Double(val_wieght)!  && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder: "يجب ان يكون الهدف بين \(Int(round(Double(val_wieght)!+1.0))) و ٢٩٩")
        }else if userInfo.refPlanMasterID == "2" && Double(val_target_wieght ?? "0.0")! >= 30.0 && Double(val_target_wieght ?? "0.0")! > Double(val_wieght)! && getUserInfo().grown == "1" {
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ان يكون الهدف بين ٣٠ و \(Int(round(Double(val_wieght)!-1.0)))")
        }else if week == 0.0 && getUserInfo().grown != "2" && userInfo.refPlanMasterID != "3"{
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
        }else{
            CheckInternet.checkIntenet { (bool) in
                if !bool {
                    SVProgressHUD.dismiss()
                    showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
                    return
                }
            }
          print("9")
            let layaq = String(setupRealm().objects(tblLayaqaCond.self).filter("id == %@",idFetnessRate!).first!.id)
            tblUserProgressHistory.updateDatathUser(userID: self.userInfo.id, refLayaqaCondID: layaq, target: val_target_wieght!, weight: val_wieght,weeksNeeded:String(Int(round(week))),gramsToLose:String(incrementKACL)) { [self] (res, err) in
                print("res",res)
                
                if res == "false" {
                    showToast(message: "نعتذر لم يتم تحديث المعلومات ", view: self.view,place:0)
                    return
                }
                
                tblUserInfo.updateDatathUser(userID: self.userInfo.id, refLayaqaCondID: layaq, target: val_target_wieght!, weight: val_wieght,weeksNeeded:String(Int(round(week))),gramsToLose:String(incrementKACL)) { (res, err) in
                    if res == "success" {
                        showToast(message: "تم تحديث المعلومات", view: self.view,place:0)
                        _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,flag:"2", user_ProgressHistory: getUserInfoProgressHistory())
                        //                    AppDelegate.appDelegate.checkGenerateSubcribe()
                        self.dismiss(animated: true, completion: nil)
                        setDataInSheardPreferance(value: getDateOnly(), key: "latestDateOpenApp")
                    }else if res == "false"{
                        showToast(message: "نعتذر لم يتم تحديث المعلومات ", view: self.view,place:0)
                    }else{
                        showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
                    }
                }
            }
        }
    }
    
    

    @IBAction func seekBarAction(_ sender: Any) {
      
        if wieght.text == "" {
            customError(textFeild: wieght, color: UIColor.red, placeHolder: "يجب ادخال وزنك")
            return
        }else if target_wieght.text == "" {
            customError(textFeild: target_wieght, color: UIColor.red, placeHolder: "يجب ادخال هدف وزنك")
            
            return
        }else{

            calculateWeek()
  

        if dietOrderValue.value == 0.0 {
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
            txtNumberWeek.text = "(0) اسابيع"
            if getUserInfo().refPlanMasterID == "1" {
                txtQuantitiEachWeek.text = "زيادة : ٠ غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : ٠ غم لكل اسبوع"
                
            }

        }else{
            lblPeriodTarget.text = "قم  بتحديد فترة الوصول الى هدفك"
            lblPeriodTarget.textColor = color_Inactive
            
            txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
            if getUserInfo().refPlanMasterID == "1" {
                txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }
        }


    }
    }
    func calculateWeek(){
        let progress:Double = round(Double(dietOrderValue.value))
        print(progress)
        let wekcoloreis:Double = (0.2 + (progress * 0.06))

        week =  abs(Double(((FormatterHoshiTextField(str:wieght!).text! as NSString).doubleValue
            - ( FormatterHoshiTextField(str:target_wieght!).text! as NSString).doubleValue)) / wekcoloreis)
        txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
        incrementKACL = wekcoloreis * 1000.0
        if getUserInfo().refPlanMasterID == "1" {
            txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }else{
            txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }
        ActivationModel.sharedInstance.incrementKACL = String(Int(round(incrementKACL)))
        ActivationModel.sharedInstance.week = String(Int(round(week)))


    }
      
      func customError(textFeild:HoshiTextField,color:UIColor,placeHolder:String){
            textFeild.isError( numberOfShakes: 2.5, revert: true)
            textFeild.placeholder = placeHolder
            textFeild.placeholderColor = color
        }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
      
             self.performSegue(withIdentifier: "selectLayaq", sender: ["title": "اختر معدل  اللياقة", "options": lblLayaCondition.text,"flag":"3"])
        
    }
}
extension LastOpenAppController:selectionDelegate {
    
    func setPragnentMonths(str: String, id: String) {
           
       }
    func setMidctionDiabetsTypeOptionTwo(str: String, type: String) {
        
    }
    
    func setDiabetsType(str: String, typeDiabites: String) {
        
    }
    func setMidctionDiabetsTypeOptionOne(str: String, type: String) {
          
      }
    
    func setSeletedCusine(str:String,strID:String){
   
    }
    
    func setSeletedallaegy(str:String,strID:String){
     
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        if !str.isEmpty {
            lblLayaCondition.text = str
            saveLayaqa(strId:strID)
            idFetnessRate = Int(strID)
        }else{
            lblLayaCondition.text = "اختر معدل اللياقة"
            saveLayaqa(strId:strID)
            idFetnessRate = 0
        }
    }
}
