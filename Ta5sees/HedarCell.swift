//
//  HedarCell.swift
//  Ta5sees
//
//  Created by Admin on 2/6/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class HedarCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lineHeader: UILabel!
    @IBOutlet weak var viewContentHeader: UIView!
    @IBOutlet weak var headerImage: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
