//
//  UpdateInfoUserVC.swift
//  Ta5sees
//
//  Created by Admin on 3/22/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD
import TextFieldEffects
import Firebase
import FirebaseAnalytics

class UpdateInfoUserVC: UIViewController,UIViewControllerTransitioningDelegate {
    var idFetnessRate = 0
    @IBOutlet weak var lblLayaCondition: UILabel!
    @IBOutlet weak var wieght: HoshiTextField!
    @IBOutlet weak var target_wieght: HoshiTextField!
    @IBOutlet weak var stackTarget: UIStackView!
    @IBOutlet weak var stackSeekBar: UIStackView!
    @IBOutlet weak var lblPeriodTarget: UILabel!
    @IBOutlet weak var dietOrderValue: UISlider!
    @IBOutlet weak var txtQuantitiEachWeek: UILabel!
    @IBOutlet weak var txtNumberWeek: UILabel!
    var incrementKACL:Double = 0.0
    var week:Double = 0.0
    let color_Inactive =  UIColor(hex: 0x9CABB3)
    let color_green = UIColor(red: 123/255, green: 222/255, blue: 179/255, alpha: 1.0)
    var ageChild:Float = 0.0

    var dietType = ActivationModel.sharedInstance.dietType
    var user_ProgressHistory:tblUserProgressHistory!

    @IBAction func btnNext(_ sender: Any) {
        let val_wieght = FormatterHoshiTextField(str:wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        var val_target_wieght:String?  = FormatterHoshiTextField(str:target_wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        if lblLayaCondition.text! == "اختر معدل اللياقة"{
            showAlert(str:"يجب اختيار معدل اللياقة",view:self)
            SVProgressHUD.dismiss()
            return
        }
//        getUserInfoProgressHistory
        if getUserInfo().grown == "2" {
            if val_target_wieght == "" || val_target_wieght!.isEmpty{
                val_target_wieght = "0.0"
            }
             if val_wieght == ""  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
            }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }else{
                ActivationModel.sharedInstance.weight = val_wieght
                ActivationModel.sharedInstance.target_weight = val_target_wieght!
                ActivationModel.sharedInstance.ageChild =  String(round(ageChild))
                ActivationModel.sharedInstance.BMI = ((FormatterHoshiTextField(str:wieght).text! as NSString).doubleValue)/((Double(getUserInfoProgressHistory().height)!/100.0) * (Double(getUserInfoProgressHistory().height)!/100.0))
             processDietOrderForTEEChild()
            }
        }
        if val_target_wieght == "" || val_target_wieght!.isEmpty || val_target_wieght == "0"{
            val_target_wieght = "0.0"
        }
         if val_wieght == ""  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
        }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
        }else if val_target_wieght == "0.0" && dietType != "3"   && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ادخال الهدف")
        }else if dietType == "1" && Double(val_target_wieght ?? "0.0")! < 300.0 && Double(val_target_wieght ?? "0.0")! < Double(val_wieght)!  && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder: "يجب ان يكون الهدف بين \(Int(round(Double(val_wieght)!+1.0))) و ٢٩٩")
        }else if dietType == "2" && Double(val_target_wieght ?? "0.0")! >= 30.0 && Double(val_target_wieght ?? "0.0")! > Double(val_wieght)! && getUserInfo().grown == "1" {
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ان يكون الهدف بين ٣٠ و \(Int(round(Double(val_wieght)!-1.0)))")
        }else if week == 0.0 && getUserInfo().grown != "2" && dietType != "3"{
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
        }else{
            ActivationModel.sharedInstance.weight = val_wieght
            ActivationModel.sharedInstance.target_weight = val_target_wieght!
            ActivationModel.sharedInstance.BMI = ((FormatterHoshiTextField(str:wieght).text! as NSString).doubleValue)/((Double(getUserInfoProgressHistory().height)!/100.0) * (Double(getUserInfoProgressHistory().height)!/100.0))
            if ActivationModel.sharedInstance.refCusine == "7" || ActivationModel.sharedInstance.refCusine == "8" {
                ActivationModel.sharedInstance.xtraCusine = [ActivationModel.sharedInstance.refCusine]
                let detailView = self.storyboard!.instantiateViewController(withIdentifier: "ContainerVC") as! ContainerVC
                detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                detailView.transitioningDelegate = self
                self.present(detailView, animated: true, completion: nil)
            }else{
                let detailView =  self.storyboard!.instantiateViewController(withIdentifier: "CuisineVC") as! CuisineVC
                detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                detailView.transitioningDelegate = self
                createEvent(key: "26",date: getDateTime())
                self.present(detailView, animated: true, completion: nil)
            }
            
        }
      
//
    }
    func processDietOrderForTEEChild(){
           let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
           let bmi = ActivationModel.sharedInstance.BMI
           var ob = tblCDCGrowthChild()
          print("oldyear \(oldyear)")
         tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: Float(bmi) ) { (respose, error) in
               ob = respose as! tblCDCGrowthChild
           }
           
           if ob.value >= 25 && ob.value <= 75 {
            ActivationModel.sharedInstance.dietType = "3"
           }else if ob.value <= 25 {
            ActivationModel.sharedInstance.dietType = "1"
           }else if ob.value > 75 {
            ActivationModel.sharedInstance.dietType = "2"
           }
        
       }
    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if ActivationModel.sharedInstance.dietType == "3"  || getUserInfo().grown == "2"{
            let firstView1 =  self.stackSeekBar.arrangedSubviews[0]
            firstView1.isHidden = true
            let firstView2 =  self.stackTarget.arrangedSubviews[1]
            firstView2.isHidden = true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //  flag 1 >>> cusin
        // flag 2 >>> alleagy
        //flag 3 >> layaqa
       if let dis=segue.destination as? MultiSeletetionView {
            if  let dictionary=sender as? [String:String] {
                dis.title_Navigation = dictionary["title"]
                dis.obselectionDelegate = self
                dis.optionsSelected = dictionary["options"]
                dis.flag = dictionary["flag"]
            }
        }
        
    }
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    func setWeight(weight:String){
        if Double(weight)!.rounded(.up) == Double(weight)!.rounded(.down){
            wieght.text = "\(Int(Double(weight)!.rounded(.down)))" + " كغم"
        }else{
            wieght.text = weight + " كغم"

        }
    }
    func setTatgetWeight(weight:String){
        if Double(weight)!.rounded(.up) == Double(weight)!.rounded(.down){
            target_wieght.text = "\(Int(Double(weight)!.rounded(.down)))" + " كغم"
        }else{
            target_wieght.text = weight + " كغم"

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        wieght.clearsOnBeginEditing = true
        target_wieght.clearsOnBeginEditing = true
        
        dietOrderValue.setValue(10.0, animated: true)

        if !tblLayaqaCond.getItem(refID: Int(getUserInfoProgressHistory().refLayaqaCondID)!).isEmpty {
            lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
            lblLayaCondition.text = tblLayaqaCond.getItem(refID: Int(getUserInfoProgressHistory().refLayaqaCondID)!)
        }
   
        clicableLabel(lbl:lblLayaCondition,tag:3)
        
        setWeight(weight:getUserInfoProgressHistory().weight)
        setTatgetWeight(weight:getUserInfoProgressHistory().target)

//        wieght.text = getUserInfoProgressHistory().weight + " كغم"
        ActivationModel.sharedInstance.refLayaqa = getUserInfoProgressHistory().refLayaqaCondID
//        target_wieght.text = getUserInfoProgressHistory().target + " كغم"
        self.HideKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        if dietType != "3"  || getUserInfo().grown != "2"{
          
        target_wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        target_wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        }
        
        if dietType != "3" ||  getUserInfo().grown != "2" {
            calculateWeek()
        }
        var val_target_wieght:String?  = FormatterHoshiTextField(str:target_wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)

        let val_wieght = FormatterHoshiTextField(str:wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        if getUserInfo().grown == "2" {
            if val_target_wieght == "" || val_target_wieght!.isEmpty{
                val_target_wieght = "0.0"
            }
        }

        if val_target_wieght == "" || val_target_wieght!.isEmpty || val_target_wieght == "0"{
            val_target_wieght = "0.0"
        }
        
        if val_target_wieght == "0.0" && dietType != "3"   && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ادخال الهدف")
        }else if dietType == "1" && Double(val_target_wieght ?? "0.0")! < 300.0 && Double(val_target_wieght ?? "0.0")! < Double(val_wieght)!  && getUserInfo().grown == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder: "يجب ان يكون الهدف بين \(Int(round(Double(val_wieght)!+1.0))) و ٢٩٩")
        }else if dietType == "2" && Double(val_target_wieght ?? "0.0")! >= 30.0 && Double(val_target_wieght ?? "0.0")! > Double(val_wieght)! && getUserInfo().grown == "1" {
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ان يكون الهدف بين ٣٠ و \(Int(round(Double(val_wieght)!-1.0)))")
        }
    }
    
    func clicableLabel(lbl:UILabel,tag:Int){
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lbl.tag = tag
        lbl.isUserInteractionEnabled = true
    }
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
       self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "اختر معدل  اللياقة", "options": lblLayaCondition.text,"flag":"3"])
    }
    
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func seekBarAction(_ sender: Any) {
      
        if wieght.text == "" {
            customError(textFeild: wieght, color: UIColor.red, placeHolder: "يجب ادخال وزنك")
            return
        }else if target_wieght.text == "" {
            customError(textFeild: target_wieght, color: UIColor.red, placeHolder: "يجب ادخال هدف وزنك")
            
            return
        }else{

            calculateWeek()
  

        if dietOrderValue.value == 0.0 {
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
            txtNumberWeek.text = "(0) اسابيع"
            if dietType == "1" {
                txtQuantitiEachWeek.text = "زيادة : ٠ غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : ٠ غم لكل اسبوع"
                
            }

        }else{
            lblPeriodTarget.text = "قم  بتحديد فترة الوصول الى هدفك"
            lblPeriodTarget.textColor = color_Inactive
            
            txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
            if dietType == "1" {
                txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }
        }


    }
    }
    func calculateWeek(){
        let progress:Double = round(Double(dietOrderValue.value))
        print(progress)
        let wekcoloreis:Double = (0.2 + (progress * 0.06))

        week =  abs(Double(((FormatterHoshiTextField(str:wieght!).text! as NSString).doubleValue
            - ( FormatterHoshiTextField(str:target_wieght!).text! as NSString).doubleValue)) / wekcoloreis)
        txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
        incrementKACL = wekcoloreis * 1000.0
        if dietType == "1" {
            txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }else{
            txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }
        ActivationModel.sharedInstance.incrementKACL = String(Int(round(incrementKACL)))
        ActivationModel.sharedInstance.week = String(Int(round(week)))


    }
    @objc func textFieldDidChange(textField: HoshiTextField) {

        let text =  FormatterHoshiTextField(str:textField).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        if text == "" { return }
       if textField.tag == 3 {
        
            target_wieght.text = ""
            if Double(text)! >= 30.0 && Double(text)! <= 299.0{
                customError(textFeild:textField,color:color_green,placeHolder:"الوزن")
                textField.text = text + " كغم"
                if  getUserInfo().grown != "2" && dietType != "3"{
                    target_wieght.becomeFirstResponder()
                    calculateWeek()
                }else{
                    view.endEditing(true)
                }
            }else{
                customError(textFeild:textField,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }
            
        } else if textField.tag == 4 {
           
            
            let text_wieght = FormatterHoshiTextField(str:wieght!).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            if text_wieght == "" { return}
            if textField.text == "" { return}
            
            if  dietType == "1" {
                if  getUserInfo().grown != "2"{

                    if Double(textField.text!)! > Double(text_wieght)! && Double(textField.text!)! <= 299.0{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                 
                }else{
                    
                    customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من  \(Double(text_wieght)!+1.0)و ٢٩٩")
                    
                }
                    dietOrderValue.setValue(10.0, animated: true)
                    calculateWeek()
                }else{
                    view.endEditing(true)
                             }
                
            }else if dietType == "2" {
            if  getUserInfo().grown != "2"{
                if Double(textField.text!)! >= 30.0 && Double(textField.text!)! < Double(text_wieght)!{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                               }else{
                        customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من ٣٠  \(Double(text_wieght)!-1.0)و")
                }
                dietOrderValue.setValue(10.0, animated: true)
                calculateWeek()
            }else{
                view.endEditing(true)
                
            }
            }
            
            
            
            return
        }
    }
    @objc func textFieldDidBeign(textField: HoshiTextField) {
      

            let result =  textField.text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            textField.text = result
            
        
    }
}
extension UpdateInfoUserVC:selectionDelegate {
    
    func setPragnentMonths(str: String, id: String) {
        
    }
    func setMidctionDiabetsTypeOptionTwo(str: String, type: String) {
        
    }
    func setDiabetsType(str: String, typeDiabites: String) {
        
    }
    func setMidctionDiabetsTypeOptionOne(str: String, type: String) {
        
    }
    func setSeletedCusine(str:String,strID:String){
    }
    
    func setSeletedallaegy(str:String,strID:String){
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        if !str.isEmpty {
            lblLayaCondition.text = str
            saveLayaqa(strId:strID)
            idFetnessRate = Int(strID)!
            ActivationModel.sharedInstance.refLayaqa = strID
            lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblLayaCondition.font = UIFont(name: "GE Dinar One",size: 17)
            lblLayaCondition.text = "اختر معدل اللياقة"
            saveLayaqa(strId:strID)
            idFetnessRate = 0
            
        }
    }

}
extension UpdateInfoUserVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
