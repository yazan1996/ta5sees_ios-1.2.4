//
//  ChangeWajbehWeekCell.swift
//  Ta5sees
//
//  Created by Admin on 10/2/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit



protocol ButtonClicable1 : class  {
    func didSelect(_ cell: UICollectionViewCell ,_ button: UIButton)
}

class ChangeWajbehWeekCell: UICollectionViewCell {
    var delegate:ButtonClicable1?
    var index:IndexPath?

    @IBAction func btnClilckChange(_ sender: UIButton) {
        delegate?.didSelect(self, sender)
        effectViewButtom(sender:btnChange)
    }
    @IBOutlet weak var view: CapsuleView!
    @IBOutlet weak var txtdate: UILabel!
    @IBOutlet weak var imgWajbeh: UIImageView!
    
    @IBOutlet weak var btnChange: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.isUserInteractionEnabled = false
    }
    
    
    
    func SetImage(url:String,laImage:UIImageView){
        laImage.layer.borderWidth = 1
        laImage.layer.masksToBounds = false
        laImage.layer.borderColor = UIColor.black.cgColor
        laImage.layer.cornerRadius = laImage.frame.height/2
        
        laImage.clipsToBounds = true
        //paralel process
        DispatchQueue.global().async {
            
            
            do{
                let AppURL=URL(string:url)
                let data = try Data(contentsOf: AppURL!)
                
                // access to UI
                DispatchQueue.main.sync {
                    laImage.image = UIImage(data: data)
                }
            }
            catch {
                print("cannot load from server")
            }
            
        }
    }
    
}
