//
//  DisplayItemsViewCell.swift
//  Ta5sees
//
//  Created by Admin on 9/6/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class DisplayItemsViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    @IBOutlet weak var textName: UITextView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
