//
//  CircleProgress.swift
//  Recipient
//
//  Created by Zoe Liu on 2019/8/16.
//  Copyright © 2019 Everbridge, Inc. All rights reserved.
//

import UIKit

@IBDesignable class CircleProgress: UIControl {
    @IBInspectable var mainColor: UIColor = UIColor.white {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable var forgroundColor: UIColor = defaultInnerColor {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBInspectable var colortext: UIColor = UIColor.blue {
           didSet {
               self.setNeedsDisplay()
           }
       }
    @IBInspectable var progress: CGFloat = 0.20 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    lazy var backLayer: CAShapeLayer? = {
        let layer = CAShapeLayer()
        layer.strokeColor = mainColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = 2.0
        return layer
    }()
    

    lazy var foreLayer: CAShapeLayer? = {
        let layer = CAShapeLayer()
        layer.strokeColor = forgroundColor.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = 2.0
        layer.lineCap = .round
        return layer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.layer.addSublayer(self.backLayer!)
        self.layer.addSublayer(self.foreLayer!)
//        self.layer.addSublayer(self.text)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = .clear
        self.layer.addSublayer(self.backLayer!)
        self.layer.addSublayer(self.foreLayer!)

        self.mainColor = .white
        self.forgroundColor = defaultInnerColor
        self.progress = 0.3
    }
    
    override func awakeFromNib() {
    self.mainColor = .white
        self.forgroundColor = defaultInnerColor

        self.progress = 0.3
    }
    
    override func draw(_ rect: CGRect) {
        self.backLayer?.frame = self.bounds.inset(by: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
        self.foreLayer?.frame = self.bounds.inset(by: UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1))
        self.backLayer?.strokeColor = mainColor.cgColor
        self.foreLayer?.strokeColor = forgroundColor.cgColor
        
        let circle = UIBezierPath.init(ovalIn: self.backLayer!.bounds)
        self.backLayer?.path = circle.cgPath
//        text.frame.origin.x =  self.backLayer!.bounds.origin.y
//
//        text.frame.origin.y =  self.backLayer!.bounds.origin.y
              
        let center = CGPoint.init(x: self.foreLayer!.frame.size.width / 2,
                                  y: self.foreLayer!.frame.size.height / 2)
        let start = 0 - CGFloat(Double.pi / 2)
        let end = CGFloat(Double.pi) * 2 * progress - CGFloat(Double.pi / 2)
        let arc = UIBezierPath.init(arcCenter: center,
                                    radius: self.foreLayer!.frame.size.width / 2,
                                    startAngle: start, endAngle: end, clockwise: true)
        self.foreLayer!.path = arc.cgPath
    }
}

extension CircleProgress {
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
}









//    var alertController: UIAlertController?
//    var alertTimer: Timer?
//      var remainingTime = 0
//      var baseMessage: String?


//    override func viewDidAppear(_ animated: Bool) {
//          super.viewDidAppear(animated)
//
//      }

//
//
//
//    func showAlertMsg(_ title: String, message: String, time: Int) {
//
//        guard (self.alertController == nil) else {
//            print("Alert already displayed")
//            return
//        }
//
//        self.baseMessage = message
//        self.remainingTime = time
//
//        self.alertController = UIAlertController(title: title, message: self.baseMessage, preferredStyle: .alert)
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//            print("Alert was cancelled")
//            self.alertController=nil;
//            self.alertTimer?.invalidate()
//            self.alertTimer=nil
//        }
//
//        self.alertController!.addAction(cancelAction)
//
//        self.alertTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countDown), userInfo: nil, repeats: true)
//
//        self.present(self.alertController!, animated: true, completion: nil)
//    }
//
//    @objc func countDown() {
//
//        self.remainingTime -= 1
//        if (self.remainingTime < 0) {
//            self.alertTimer?.invalidate()
//            self.alertTimer = nil
//            self.alertController!.dismiss(animated: true, completion: {
//                self.alertController = nil
//            })
//        } else {
//            self.alertController!.message = self.alertMessage()
//        }
//
//    }
//
//    func alertMessage() -> String {
//        var message=""
//        if let baseMessage=self.baseMessage {
//            message=baseMessage+" "
//        }
//        return(message+"\(self.remainingTime)")
//    }
