//
//  DesignableView.swift
//  Gradient
//
//  Created by Payal Gupta on 12/05/18.
//  Copyright © 2018 Payal Gupta. All rights reserved.
//

import UIKit

@IBDesignable class ButtonColorRaduis: UIButton {
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map {$0.cgColor}
        layer.borderWidth = 0.5
        layer.borderColor =  UIColor(red: 182/255, green: 251/255, blue: 211/255, alpha: 1.0).cgColor //94-204-156
        layer.cornerRadius = bounds.size.height * 0.5
        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
}
