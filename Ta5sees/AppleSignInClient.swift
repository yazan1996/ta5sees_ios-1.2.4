//
//  AppleSignInClient.swift
//  AppleSignIn
//
//  Created by Harendra Sharma on 26/01/20.
//  Copyright © 2020 Harendra Sharma. All rights reserved.
//


import AuthenticationServices
import SVProgressHUD
import Firebase
import FirebaseAuth
 
@available(iOS 13.0, *)
class AppleSignInClient: NSObject {
    
    let view:PresenterAppleID!
   
    init(with view :PresenterAppleID) {
        self.view = view
    }
    var completionHandler: (_ fullname: String?, _ email: String?, _ token: String?) -> Void = { _, _, _ in }
    var currentNonce:String!

    @objc func handleAppleIdRequest(block: @escaping (_ fullname: String?, _ email: String?, _ token: String?) -> Void) {
        completionHandler = block
       
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
//            self.currentNonce = randomNonceString()
            // Set the SHA256 hashed nonce to ASAuthorizationAppleIDRequest
//             request.nonce = currentNonce.sha512
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()

    }

    func getCredentialState(userID: String) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: userID) { credentialState, _ in
            switch credentialState {
            case .authorized:
                print("userID *** \(userID)")
                // The Apple ID credential is valid.
                break
            case .revoked:
                SVProgressHUD.dismiss()
                // The Apple ID credential is revoked.
                break
            case .notFound:
                SVProgressHUD.dismiss()
                // No credential was found, so show the sign-in UI.
                break
            default:
                SVProgressHUD.dismiss()
                break
            }
        }
        print("userID *** nil")

    }
    
//    private func randomNonceString(length: Int = 32) -> String {
//        precondition(length > 0)
//        let charset: Array<Character> =
//            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
//        var result = ""
//        var remainingLength = length
//
//        while remainingLength > 0 {
//            let randoms: [UInt8] = (0 ..< 16).map { _ in
//                var random: UInt8 = 0
//                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
//                if errorCode != errSecSuccess {
//                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
//                }
//                return random
//            }
//
//            randoms.forEach { random in
//                if remainingLength == 0 {
//                    return
//                }
//
//                if random < charset.count {
//                    result.append(charset[Int(random)])
//                    remainingLength -= 1
//                }
//            }
//        }
//
//        return result
//    }

}

 
@available(iOS 13.0, *)
extension AppleSignInClient: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        SVProgressHUD.show()
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = "appleIDCredential.email"
        
            guard let appleIDToken = appleIDCredential.identityToken else {
                  print("Unable to fetch identity token")
                  return
                }
            guard String(data: appleIDToken, encoding: .utf8) != nil else {
                   print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                   return
                 }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                   print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                   return
                 }
                 // Initialize a Firebase credential.
                 let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                           idToken: idTokenString,
                                                           rawNonce: .none)
            
            
            Auth.auth().signIn(with: credential) { [self] (authResult, error) in
                if (error == nil) {
                    print( userIdentifier.filter({ "0"..."9" ~= $0 || $0.isLetter }))//.filter({ "0"..."9" ~= $0 || $0.isLetter })
                    setDataInSheardPreferance(value: userIdentifier, key: "appleID")
                    setDataInSheardPreferance(value: userIdentifier, key: "mobNum")
                    //000993.a988622e50f544edbb74f3629ce448ee.1102
                    //000993.a988622e50f544edbb74f3629ce448ee.1102
                    //000028.ffd93fe6d5164e0d9c17de1958372d13.0826
                    print("user1 \(getDataFromSheardPreferanceString(key: "mobNum"))")
                    print("user \(getDataFromSheardPreferanceString(key: "appleID"))")
                    if  "\(String(describing: fullName!))".isEmpty  || "\(String(describing: fullName!))" == ""{
                        setDataInSheardPreferance(value: "", key: "name")
                    }else{
                        setDataInSheardPreferance(value: "\(fullName!.givenName!) \(fullName!.familyName!)", key: "name")
                    }
                    if let identityTokenData = appleIDCredential.identityToken,
                        let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                        print("Identity Token \(identityTokenString)")
                        completionHandler(fullName?.givenName, email, identityTokenString)
                    } else {
                        completionHandler(fullName?.givenName, email, nil)
                    }

                    getCredentialState(userID: userIdentifier)
                }else{
                    SVProgressHUD.dismiss()
                    print("errors \(error!.localizedDescription)")
                     
                }
                   }
        
        }
    
        }
    
       
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
            // Handle error.
            print("error in sign in with apple: \(error.localizedDescription)")
        }

    }
  
    




protocol PresenterAppleID:AnyObject {
    func goToHomePage()
    func goToGenderTypePage()
}
//extension String {
//
//    public var sha512: String {
//        let data = self.data(using: .utf8) ?? Data()
//        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
//        data.withUnsafeBytes {
//            _ = CC_SHA512($0.baseAddress, CC_LONG(data.count), &digest)
//        }
//        return digest.map({ String(format: "%02hhx", $0) }).joined(separator: "")
//    }
//}
