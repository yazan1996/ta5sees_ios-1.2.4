//
//  FridgeList.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 16/04/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//


import UIKit
import RealmSwift
import RxSwift
import RxCocoa

private let reuseIdentifier = "Cell"

class FridgeList: UICollectionViewController {

    @IBOutlet var tbl: UICollectionView!
    var x:[Int]! = []
    
    let disposeBag = DisposeBag()
    var items : Observable<[shoppingModel]>!
    lazy var obShoppingListPresnter = ShoppingListPresnter(with: self)
    var initialDataAry=[shoppingModel]()
    var dataAry = [shoppingModel]()
    let textLabel = UILabel()
    var count:Int!
    
    func setUpCollectionView() {
         /// 1
        

         /// 2
        tbl.delegate = self
        tbl.dataSource = self
        tbl.semanticContentAttribute = .forceLeftToRight
         /// 3
         let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical
         /// 4
        layout.minimumLineSpacing = 8
         /// 5
        layout.minimumInteritemSpacing = 4

         /// 6
        tbl
               .setCollectionViewLayout(layout, animated: true)
       }
    override func viewDidLoad() {
        super.viewDidLoad()

//        tbl.register(FridgeHeaderCell.self,
//               forHead erFooterViewReuseIdentifier: "sectionHeader")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        setUpCollectionView()
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        tbl.rx.modelSelected(shoppingModel.self).subscribe(onNext: {
            item in //selec all row
            if let selectedRowIndexPath = self.tbl.indexPathsForSelectedItems {
                self.tbl.deselectItem(at: selectedRowIndexPath.first!, animated: true)
            }
        }).disposed(by: disposeBag)
        
        
        tbl.rx.itemSelected.subscribe(onNext : {
            [weak self] indexPath in
            if (self?.tbl.cellForItem(at: indexPath) as? ShoppingListCell) != nil {
                
                if let cell = self?.tbl.cellForItem(at: indexPath) as? ShoppingListCell {
                    let item: shoppingModel = try! self!.tbl.rx.model(at: indexPath)
                    if item.isCheck == 0 {
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:1) { (response) in
                            item.isCheck = 1
                            cell.btn.setImage(UIImage(named: "green_ic_checked"), for: .normal)
                            
                        }
                    }else{
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:0) { (response) in
                            item.isCheck = 0
                            cell.btn.setImage(UIImage(named: "green_ic_unchecked"), for: .normal)
                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                        self!.viewDidAppear(true)
                    }
                 
                }
            }
        }).disposed(by: disposeBag)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

       

        obShoppingListPresnter.getAllIngridintForPurchased(rang:count)
        if dataAry.isEmpty {
            CollectionViewHelper.EmptyMessage(message: "مطبخي فارغ", viewController: tbl)
        }else {
            CollectionViewHelper.EmptyMessage(message: "", viewController: tbl)
            
        }
        items = Observable.just(dataAry)
        bindData()
  
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    private func bindData() {
        tbl.dataSource = nil
        items.bind(to: tbl.rx.items(cellIdentifier: "cellShopping")) { (row, newitem, cell) in
            if let cellToUse = cell as? ShoppingListCell {
                cellToUse.item = newitem
                cellToUse.delegate = self
            }
        }.disposed(by: disposeBag)
    }
    
    
    
    private func prependData(dataToPrepend : [shoppingModel]) {
        let newObserver = Observable.just(dataToPrepend)
        items = Observable.combineLatest(items, newObserver) {
            $1+$0
        }
        bindData()
    }
    
    private func appendData(dataToAppend : [shoppingModel]) {
        let newObserver = Observable.just(dataToAppend)
        items = Observable.combineLatest(items, newObserver) {
            $0+$1
        }
        bindData()
    }
    
    func reloadData(count : Int) {
        if self.count != count{
            obShoppingListPresnter.getAllIngridintForPurchased(rang:count)
            items = Observable.just(dataAry)
            bindData()
        }
    }
    
    
    
    static var indexID:Int!
    
   
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }


        // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }



    

}
extension FridgeList: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        /// 2
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }

    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// 4
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// 5
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        /// 6
        
        return CGSize(width: widthPerItem - 8, height: 80)
    }
}
extension FridgeList:ShoppingListView {
    func setListIngridint(list: [shoppingModel]) {
        dataAry.removeAll()
        items = nil
        dataAry = list//.removeDuplicates()
    }
}
