//
//  tblSenfAllergy.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblSenfAllergy:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refSenfID = -2
    @objc dynamic var refAllergyInfoID = -2
    //    let lines = LinkingObjects(fromType: tblSenfAllergy.self, property: "id")
    override static func primaryKey() -> String? {
        return "id"
    }
    

    func readJson(){
        let rj = ReadJSON()
        rj.readJson(tableName: "senf/tblSenfAllergy") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
                      thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblSenfAllergy = tblSenfAllergy()
                        data.id = item["id"].intValue
                        data.refSenfID = item["refSenfID"].intValue
                        data.refAllergyInfoID = item["refAllergyInfoID"].intValue
                        

                      setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                            }
                        }
                    }
                }
            }

        }
    }
    
    

