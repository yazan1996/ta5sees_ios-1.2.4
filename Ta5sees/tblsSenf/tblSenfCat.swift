//
//  tblSenfCat.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblSenfCat:Object {
    
    @objc dynamic var id = -1
    @objc dynamic var discription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func getdataItem(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblSenfCat.self).filter("id == %@",refID).first,nil)
    }

    
    static func removeWajbeh(id:Int){
           let item = setupRealm().objects(tblSenfCat.self).filter("id == %@",id)
           try! setupRealm().write {
               setupRealm().delete(item)
           }
       }
    static func updateItemListner(item:[String : Any]){
          
          let data:tblSenfCat = tblSenfCat()
          data.id = item["id"] as? Int ?? 0
          data.discription = item["description"] as? String ?? "null"
          
          try! setupRealm().write {
              setupRealm().add(data, update: Realm.UpdatePolicy.modified)
              
          }
          
      }
    static func getCountItems()->Int{
               
               return setupRealm().objects(tblSenfCat.self).count
           }
    static func readListnerItem(count:UInt,completion: @escaping (Bool) -> Void){
        
        let rj = ReadJSON()
        rj.AddLisenerItem(tableName: "senf/tblSenfCat",count:count) {(response, Error) in
//            let thread =  DispatchQueue.global(qos: .utility)
//            thread.async {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let obj = tblSenfCat(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                            setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                        }
//                    }
                    completion(true)
                }
                
            }
        }
    }
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblSenfCat]()
        rj.readJson(tableName: "senf/tblSenfCat") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblSenfCat(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                        list.append(obj)
                      
                    }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                    }
                }
            }
        }
        
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblSenfCat]()
        
        if let recommends = JSON(response).array {
            for item in recommends {
                let obj = tblSenfCat(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                list.append(obj)
            }
            try! setupRealm().write {
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

        }
    }
}


