//
//  tblSenfRestrictedPlans.swift
//  Ta5sees
//
//  Created by Admin on 6/24/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblSenfRestrictedPlans:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refSenfID = -2
    @objc dynamic var refPlanID = -2
//    let lines = LinkingObjects(fromType: tblWajbeh.self, property: "reftblWajbehRestrictedPlans")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func KeyName() -> String
    {
        return primaryKey()!
}
}
