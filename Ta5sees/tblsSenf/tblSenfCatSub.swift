//
//  tblSenfCatSub.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblSenfCatSub:Object {
    
    
    
    
    
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    @objc dynamic var refSenfCatID = ""
    @objc dynamic var refAppFoodCategory = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func getdataItem(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblSenfCatSub.self).filter("id == %@",String(refID)).first,nil)
    }
    
  
    
    
    static func removeWajbeh(id:Int){
             let item = setupRealm().objects(tblSenfCatSub.self).filter("id == %@",id)
             try! setupRealm().write {
                 setupRealm().delete(item)
             }
         }
      static func updateItemListner(item:[String : Any]){
            
            let data:tblSenfCatSub = tblSenfCatSub()
            data.id =  String(item["id"] as? Int ?? 0)
            data.discription = item["description"] as? String ?? "null"
            data.refSenfCatID = String(item["refSenfCatID"] as? Int ?? 0)
        if item["refAppFoodCategoryID"] as? Int == nil {
                data.refAppFoodCategory = "null"
            }else{
                data.refAppFoodCategory = String(item["refAppFoodCategoryID"] as? Int ?? 0)
              }
                                    
            try! setupRealm().write {
                setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                
            }
            
        }
    
    static func getCountItems()->Int{
          
          return setupRealm().objects(tblSenfCatSub.self).count
      }
    static func readListnerItem(count:UInt,completion: @escaping (Bool) -> Void){
        
        let rj = ReadJSON()
        var list = [tblSenfCatSub]()
        rj.AddLisenerItem(tableName: "senf/tblSenfCatSub",count:count) {(response, Error) in
//            let thread =  DispatchQueue.global(qos: .utility)
//            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblSenfCatSub = tblSenfCatSub()
                        data.id = String(item["id"].intValue)
                        data.discription = item["description"].string!
                        data.refSenfCatID = String(item["refSenfCatID"].intValue)
                        if item["refAppFoodCategoryID"].string == nil {
                            data.refAppFoodCategory = "null"
                        }else{
                            data.refAppFoodCategory = String(item["refAppFoodCategoryID"].intValue)
                        }
                        
                        list.append(data)

                    }
                  
                }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    completion(true)
//                }

                }
                
            }
        }
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
       
        rj.readJson(tableName: "senf/tblSenfCatSub") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblSenfCatSub = tblSenfCatSub()
                        data.id = String(item["id"].intValue)
                        data.discription = item["description"].string!
                        data.refSenfCatID = String(item["refSenfCatID"].intValue)
                        if item["refAppFoodCategoryID"].string == nil {
                            data.refAppFoodCategory = "null"
                        }else{
                            data.refAppFoodCategory = String(item["refAppFoodCategoryID"].intValue)
                        }
                        
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                    completion(true)
                }
            }
            
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblSenfCatSub]()
        if let recommends = JSON(response).array {
            for item in recommends {
                let data:tblSenfCatSub = tblSenfCatSub()
                data.id = String(item["id"].intValue)
                data.discription = item["description"].string!
                data.refSenfCatID = String(item["refSenfCatID"].intValue)
                if item["refAppFoodCategoryID"].string == nil {
                    data.refAppFoodCategory = "null"
                }else{
                    data.refAppFoodCategory = String(item["refAppFoodCategoryID"].intValue)
                }
                list.append(data)
                
            }
            try! setupRealm().write {
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

        }
    }

    
}

