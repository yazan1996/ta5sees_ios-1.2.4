//
//  tblSenf.swift
//  Ta5sees
//
//  Created by Admin on 6/24/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import RxSwift
class tblSenf:Object {
    
    @objc dynamic var id:Int = -4
    @objc dynamic var discription:String? = "" // delete
    @objc dynamic var wajbehProteinTotal:String? = ""
    @objc dynamic var wajbehFatTotal:String? = ""
    @objc dynamic var wajbehCarbTotal:String? = ""
    @objc dynamic var wajbehCaloriesTotal:String? = ""
    @objc dynamic var Cholestrol:String? = ""
    @objc dynamic var Sodium:String? = ""
    @objc dynamic var Purine:String? = ""
    @objc dynamic var Phosphor:String? = ""
    @objc dynamic var Potassium:String? = ""
    
    // deleted
    @objc dynamic var refDefaultHesaID = -4
    @objc dynamic var refRenalHesaID = -4
    @objc dynamic var refChild1to3HesaID = -4
    @objc dynamic var refChild4to6HesaID = -4
    @objc dynamic var refChild7to10HesaID = -4
    //
    
    @objc dynamic var HesaUnit = ""
    @objc dynamic var DefaultHesaID = ""
    @objc dynamic var RenalHesaID = ""
    @objc dynamic var Child1to3HesaID = ""
    @objc dynamic var Child4to6HesaID = ""
    @objc dynamic var Child7to10HesaID = ""
    
    @objc dynamic var isAppearToUser:String? = ""
    @objc dynamic var imgPath:String? = ""
    @objc dynamic var refSourceID = -4
    @objc dynamic var refSenfCatSubID = -4
    @objc dynamic var refSenfCatID = -4
    @objc dynamic var rrtblSenfCat:tblSenfCat?
    @objc dynamic var refMainHesaID = 0
    @objc dynamic var allergy = 0
    @objc dynamic var appCategory = 0
    @objc dynamic var fdcId = 0
    @objc dynamic var glycemicIndex = 0
    @objc dynamic var glycemicLoad = 0
    @objc dynamic var restrictedPlans = 0
    @objc dynamic var searchKeyword = ""
    @objc dynamic var serveType = ""
    @objc dynamic var servingTypeWeight = ""
    @objc dynamic var suitableForDiabetics = "" // delete
    @objc dynamic var trackerName = ""
    @objc dynamic var englishName = ""
    @objc dynamic var isSuitableForDiabetics = ""
    @objc dynamic var ingredientName = ""

    
//    @objc dynamic var reftblMainHesa:tblMainHesa?
//
//    @objc dynamic var reftblHesaValuesrefChild4to6HesaID:tblHesaValues?
//    @objc dynamic var reftblHesaValuesrefChild1to3HesaID:tblHesaValues?
//    @objc dynamic var reftblHesaValuesrefChild7to10HesaID:tblHesaValues?
//    @objc dynamic var reftblHesaValuesrefRenalHesaID:tblHesaValues?
//    @objc dynamic var reftblHesaValuesrefDefaultHesaID:tblHesaValues?
    
    static let sheard = tblSenf()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    static func removeWajbeh(id:Int){
        let item = setupRealm().objects(tblSenf.self).filter("id == %@",id)
        if !item.isEmpty {
        try! setupRealm().write {
            setupRealm().delete(item)
        }
        }
    }
    
    
    static func getdataItem(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblSenf.self).filter("id == %@",refID).first,nil)
    }
    static func getdataJoin(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblSenf.self).filter("refSenfCatID == %@",refID),nil)
    }
    static func getdataJoin2(refID:Int,refID2:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblSenf.self).filter("refSenfCatID == %@ AND refSenfCatSubID == %@",refID,refID2),nil)
    }
    
    func getSenf(refID:Int,callBack:(String)->Void){
        let obSenf:tblSenf? = setupRealm().objects(tblSenf.self).filter("id == %@",refID).first
        callBack(obSenf?.discription ?? "")
        
    }
    static  func getSenfIngriRuls(refIDs:Int,callBack:([tblSenf])->Void){
        let arrSenf = setupRealm().objects(tblSenf.self).filter("id == %@",refIDs)
        callBack(Array(arrSenf))
    }
    static func getAllSenf(responsEHendler:@escaping (Any?,Error?)->Void){
        autoreleasepool{
        responsEHendler(setupRealm().objects(tblSenf.self).sorted(byKeyPath: "id", ascending: true),nil)
    }
    }
   
    

    

    
  
    
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
            let thread =  DispatchQueue.global(qos: .utility)
            thread.async { [self] in
                if let recommends = JSON(response).array {

                        
                    let main_part = recommends.splitted()
                    let part1 =  main_part.0.splitted()
                    let part2 =  main_part.1.splitted()
                    let subPart1 = part1.0
                    let subPart12 = part1.1
                    let subPart2 = part2.0
                    let subPart21 = part2.1
                    insertData(recommends:subPart1)
                    insertData(recommends:subPart12)
                    insertData(recommends:subPart2)
                    insertData(recommends:subPart21)
                    completion(true)
                }
            }
        }
    



  
    
    
    static var arraylist:[tblSenf] = []
    static var resultelist:Results<tblSenf>!
    class func searchModelArray() -> [SearchItem]{
        autoreleasepool{
  
        var modelAry = [SearchItem]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblSenf.getAllSenf() {(response, Error) in
            resultelist = (response) as? Results<tblSenf>
            arraylist = Array(resultelist)
            for item in arraylist {
                modelAry.append(SearchItem(iteme_name: item.trackerName,item_id: String(item.id), item_flag: "senf", caolris: item.wajbehCaloriesTotal!,iteme_search: item.searchKeyword))
            }
        }
        
        return modelAry
        }
    }
    
    
    func insertData(recommends:Array<JSON>){
        var list = [tblSenf]()
        print("insert new Data senf")
        for item in recommends {
            
            let data:tblSenf = tblSenf()
            data.HesaUnit = item["HesaUnit"].stringValue
            data.isSuitableForDiabetics = item["isSuitableForDiabetics"].stringValue
            data.englishName = item["englishName"].stringValue
            data.id = item["id"].intValue
            data.discription = item["description"].string
            data.refSenfCatID = item["refSenfCatID"].intValue
                        data.refSenfCatSubID = item["refSenfCatSubID"].intValue
            data.refSourceID = item["refSourceID"].intValue //refSourceID
            data.imgPath = String(item["imgPath"].intValue)
            data.isAppearToUser = String(item["isAppearToUser"].intValue)//isAppearToUser
            data.Child7to10HesaID = item["refChild7to10HesaID"].stringValue
            data.wajbehFatTotal = String(item["wajbehFatTotal"].intValue)
            data.wajbehCarbTotal = String(item["wajbehCarbTotal"].intValue)
            data.wajbehProteinTotal = String(item["wajbehProteinTotal"].intValue)
            data.Child1to3HesaID = String(item["refChild1to3HesaID"].doubleValue)
//                        data.refDefaultHesaID = item["defaultHesaId"].intValue
            data.Child4to6HesaID = String(item["refChild4to6HesaID"].doubleValue)
            data.RenalHesaID = item["refRenalHesaID"].stringValue
            data.imgPath = String(item["imgPath"].intValue)
            
            data.Potassium = String(item["Potassium"].intValue)
            
            data.Phosphor = String(item["Phosphor"].intValue)
            data.Purine! = String(item["Purine"].intValue)
            
            data.Sodium = String(item["Sodium"].intValue)
            data.Cholestrol = String(item["Cholestrol"].intValue)
            
            
            
            data.wajbehCaloriesTotal = String(item["wajbehCaloriesTotal"].intValue)
            data.refMainHesaID = item["mainHesaId"].intValue
            
            data.allergy = item["allergy"].intValue
//                        data.appCategory = item["appCategory"].intValue
//                        data.fdcId = item["fdcId"].intValue
            data.glycemicLoad = item["GlycemicLoad"].intValue//GlycemicLoad
            data.glycemicIndex = item["GlycemicIndex"].intValue
            data.restrictedPlans = item["restrictedPlans"].intValue
            data.searchKeyword = item["searchKeyword"].stringValue
            data.serveType = item["serveType"].stringValue
            data.servingTypeWeight = item["servingTypeWeight"].stringValue
//            data.suitableForDiabetics = item["suitableForDiabetics"].stringValue
            data.trackerName = item["trackerName"].stringValue
            data.ingredientName = item["ingredientName"].stringValue

            
            
            if let ob = setupRealm().objects(tblSenfCat.self).filter("id = %@",item["refSenfCatID"].intValue).first {
                data.rrtblSenfCat = ob
            }
            
            
            list.append(data)
             
                }

            if setupRealm().isInWriteTransaction == true {
                setupRealm().add(list,update: Realm.UpdatePolicy.modified)
            }else {
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                }
    }
}
}
    

/*//
 //  tblSenf.swift
 //  Ta5sees
 //
 //  Created by Admin on 6/24/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //
 
 import Foundation
 import RealmSwift
 import SwiftyJSON
 import RxSwift
 class tblSenf:Object {
 
 
 @objc dynamic var id:Int = -4
 @objc dynamic var discription:String? = ""
 @objc dynamic var wajbehProteinTotal:String? = ""
 @objc dynamic var wajbehFatTotal:String? = ""
 @objc dynamic var wajbehCarbTotal:String? = ""
 @objc dynamic var wajbehCaloriesTotal:String? = ""
 @objc dynamic var Cholestrol:String? = ""
 @objc dynamic var Sodium:String? = ""
 @objc dynamic var Purine:String? = ""
 @objc dynamic var Phosphor:String? = ""
 @objc dynamic var Potassium:String? = ""
 @objc dynamic var Gluten:String? = ""
 @objc dynamic var bloodSugerIndex = ""
 @objc dynamic var refDefaultHesaID = -4
 @objc dynamic var refRenalHesaID = -4
 @objc dynamic var refChild1to3HesaID = -4
 @objc dynamic var refChild4to6HesaID = -4
 @objc dynamic var refChild7to10HesaID = -4
 @objc dynamic var isAppearToUser:String? = ""
 @objc dynamic var imgPath:String? = ""
 @objc dynamic var refSourceID = -4
 @objc dynamic var refSenfCatSubID = -4
 @objc dynamic var refSenfCatID = -4
 @objc dynamic var rrtblSenfCat:tblSenfCat?
 @objc dynamic var refMainHesaID = 0
 @objc dynamic var reftblMainHesa:tblMainHesa?
 
 @objc dynamic var reftblHesaValuesrefChild4to6HesaID:tblHesaValues?
 @objc dynamic var reftblHesaValuesrefChild1to3HesaID:tblHesaValues?
 @objc dynamic var reftblHesaValuesrefChild7to10HesaID:tblHesaValues?
 @objc dynamic var reftblHesaValuesrefRenalHesaID:tblHesaValues?
 @objc dynamic var reftblHesaValuesrefDefaultHesaID:tblHesaValues?
 
 static let sheard = tblSenf()
 
 override static func primaryKey() -> String? {
 return "id"
 }
 static func KeyName() -> String
 {
 return primaryKey()!
 }
 
 
 static func getdataItem(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
 let realm1 = try! Realm()
 responsEHendler(realm1.objects(tblSenf.self).filter("id == %@",refID).first,nil)
 }
 static func getdataJoin(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
 let realm1 = try! Realm()
 responsEHendler(realm1.objects(tblSenf.self).filter("refSenfCatID == %@",refID),nil)
 }
 static func getdataJoin2(refID:Int,refID2:Int,responsEHendler:@escaping (Any?,Error?)->Void){
 let realm1 = try! Realm()
 responsEHendler(realm1.objects(tblSenf.self).filter("refSenfCatID == %@ AND refSenfCatSubID == %@",refID,refID2),nil)
 }
 
 func getSenf(refID:Int,callBack:(String)->Void){
 let realm1 = try! Realm()
 let obSenf = realm1.objects(tblSenf.self).filter("id == %@",refID).first
 callBack(obSenf!.discription!)
 }
 
 static func getAllSenf(responsEHendler:@escaping (Any?,Error?)->Void){
 let realm1 = try! Realm()
 responsEHendler(realm1.objects(tblSenf.self),nil)
 }
 func setUpRealm()->Realm{
 return try! Realm()
 }
 func readJson(completion: @escaping (Bool) -> Void) {
 let rj = ReadJSON()
 rj.readJson(tableName: "senf/tblSenf") {(response, Error) in
 let thread =  DispatchQueue.global(qos: .userInitiated)
 thread.sync {
 if let recommends = JSON(response!).array {
 for item in recommends {
 
 let data:tblSenf = tblSenf()
 
 data.id = item["id"].intValue
 data.discription = item["description"].string
 data.refSenfCatID = item["refSenfCatID"].intValue
 data.refSenfCatSubID = item["refSenfCatSubID"].intValue
 data.refSourceID = item["refSourceID"].intValue
 data.imgPath = String(item["imgPath"].intValue)
 data.isAppearToUser = String(item["isAppearToUser"].intValue)
 data.refChild7to10HesaID = item["refChild7to10HesaID"].intValue
 data.refChild4to6HesaID = item["refChild4to6HesaID"].intValue
 
 data.wajbehFatTotal = String(item["wajbehFatTotal"].intValue)
 data.wajbehCarbTotal = String(item["wajbehCarbTotal"].intValue)
 data.wajbehProteinTotal = String(item["wajbehProteinTotal"].intValue)
 data.refChild1to3HesaID = item["refChild1to3HesaID"].intValue
 data.refDefaultHesaID = item["refDefaultHesaID"].intValue
 data.refChild4to6HesaID = item["refChild4to6HesaID"].intValue
 data.refChild7to10HesaID = item["refChild7to10HesaID"].intValue
 data.refRenalHesaID = item["refRenalHesaID"].intValue
 data.refDefaultHesaID = item["refDefaultHesaID"].intValue
 data.imgPath = String(item["imgPath"].intValue)
 data.bloodSugerIndex = String(item["bloodSugerIndex"].intValue)
 data.Gluten = String(item["Gluten"].intValue)
 data.Potassium = String(item["Potassium"].intValue)
 
 data.Phosphor = String(item["Phosphor"].intValue)
 data.Purine! = String(item["Purine"].intValue)
 
 data.Sodium = String(item["Sodium"].intValue)
 data.Cholestrol = String(item["Cholestrol"].intValue)
 
 
 data.wajbehFatTotal = String(item["wajbehFatTotal"].intValue)
 data.wajbehCarbTotal = String(item["wajbehCarbTotal"].intValue)
 data.wajbehProteinTotal = String(item["wajbehProteinTotal"].intValue)
 data.wajbehCaloriesTotal = String(item["wajbehCaloriesTotal"].intValue)
 data.refMainHesaID = item["refMainHesaID"].intValue
 
 
 
 
 if let ob = self.setUpRealm().objects(tblSenfCat.self).filter("id = %@",item["refSenfCatID"].intValue).first {
 data.rrtblSenfCat = ob
 }
 
 if let ob2 = self.setUpRealm().objects(tblMainHesa.self).filter("id = %@",item["refMainHesaID"].intValue).first {
 data.reftblMainHesa = ob2
 }
 
 DispatchQueue.main.async {
 
 if let ob3 = self.setUpRealm().objects(tblHesaValues.self).filter("id = %@",item["refChild4to6HesaID"].intValue).first {
 data.reftblHesaValuesrefChild4to6HesaID = ob3
 }
 }
 DispatchQueue.main.async {
 
 if let ob3 = self.setUpRealm().objects(tblHesaValues.self).filter("id = %@",item["refChild1to3HesaID"].intValue).first {
 data.reftblHesaValuesrefChild1to3HesaID = ob3
 }
 }
 DispatchQueue.main.async {
 if let ob3 = self.setUpRealm().objects(tblHesaValues.self).filter("id = %@",item["refChild7to10HesaID"].intValue).first {
 data.reftblHesaValuesrefChild7to10HesaID = ob3
 }
 }
 DispatchQueue.main.async {
 if let ob3 = self.setUpRealm().objects(tblHesaValues.self).filter("id = %@",item["refRenalHesaID"].intValue).first {
 data.reftblHesaValuesrefRenalHesaID = ob3
 }
 }
 DispatchQueue.main.async {
 if let ob3 = self.setUpRealm().objects(tblHesaValues.self).filter("id = %@",item["refDefaultHesaID"].intValue).first {
 data.reftblHesaValuesrefDefaultHesaID = ob3
 }
 }
 
 //                    let itemWajbehIngredietns = realm.objects(tblWajbehIngredietns.self).filter("refSenfID = %@",item["id"].intValue)
 //
 //                    for item3 in itemWajbehIngredietns {
 //                        data.reftblWajbehIngredietns.append(item3)
 //                    }
 
 
 DispatchQueue.main.async {
 if self.setUpRealm().isInWriteTransaction == true {
 self.setUpRealm().add(data)
 }else {
 try! self.setUpRealm().write {
 
 self.setUpRealm().add(data, update: Realm.UpdatePolicy.modified)
 }
 }
 }
 }
 }
 DispatchQueue.main.async {
 
 completion(true)
 }
 }
 }
 
 }
 
 
 static var arraylist:[tblSenf] = []
 static var resultelist:Results<tblSenf>!
 class func searchModelArray() -> [tblSenf]{
 var modelAry = [tblSenf]()
 modelAry.removeAll()
 arraylist.removeAll()
 tblSenf.getAllSenf() {(response, Error) in
 resultelist = (response) as? Results<tblSenf>
 arraylist = Array(resultelist)
 }
 
 return arraylist
 }
 }
 
 */
extension Array {
    func splitted() -> ([Element], [Element]) {
        let half = count / 2 + count % 2
        let head = self[0..<half]
        let tail = self[half..<count]

        return (Array(head), Array(tail))
    }
}
