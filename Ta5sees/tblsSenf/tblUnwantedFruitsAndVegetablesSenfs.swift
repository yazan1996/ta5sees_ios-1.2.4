//
//  UnwantedFruitsAndVegetablesSenfs.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 01/04/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUnwantedFruitsAndVegetablesSenfs:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refSenfID = -2
    @objc dynamic var refUnwantedFruitsAndVegetablesID = -2

    override static func primaryKey() -> String? {
        return "id"
    }
    

    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblUnwantedFruitsAndVegetablesSenfs]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    
                    let data:tblUnwantedFruitsAndVegetablesSenfs = tblUnwantedFruitsAndVegetablesSenfs()
                    data.id = item["id"].intValue
                    data.refUnwantedFruitsAndVegetablesID = item["refUnwantedFruitsAndVegetablesID"].intValue
                    data.refSenfID = item["refSenfID"].intValue
                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
    }
    
    

