//
//  tblSenfItems.swift
//  Ta5sees
//
//  Created by Admin on 4/16/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import SwiftyJSON

class  tblSenfItems:Object {

     @objc dynamic var id = ""
     @objc dynamic var refSenfCatID = ""
      @objc dynamic var refSenfCatSubID = ""
      @objc dynamic var SIProteinComp = ""
     @objc dynamic var SIPCarbComp = ""
    @objc dynamic var SIPFatComp = ""
      @objc dynamic var refHesaID = ""
     @objc dynamic var refSIhesaID = ""
    @objc dynamic var SITotalCalories = ""

    override static func primaryKey() -> String? {
        return "id"
    }

       func readJson(){
           let rj = ReadJSON()
           rj.readJson(tableName: "senf/tblSenfItems") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
                         thread.sync {
               if let recommends = JSON(response!).array {
                try! setupRealm().write {
                   for item in recommends {
                    let data:tblSenfItems = tblSenfItems()
                    data.id = String(item["id"].intValue)
                    data.refSenfCatID = String(item["refSenfCatID"].intValue)
                    data.refSenfCatSubID = String(item["refSenfCatSubID"].intValue)
                    data.SIProteinComp = String(item["SIProteinComp"].intValue)
                    data.SIPCarbComp = String(item["SICarbComp"].intValue)
                    data.SIPFatComp = String(item["SIFatComp"].intValue)
                    data.refHesaID = String(item["refHesaID"].intValue)
                    data.refSIhesaID = String(item["refSIhesaID"].intValue)
                    data.SITotalCalories = String(item["SITotalCalories"].intValue)
                         setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                                
                                   }
                               }
                }
            }

        }
    }
   
   
}


