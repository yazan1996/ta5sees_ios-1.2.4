//
//  MealDistbutionCell.swift
//  Ta5sees
//
//  Created by Admin on 3/1/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
//import SimpleCheckbox
class MealDistbutionCell: UITableViewCell,UITextFieldDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        time_food_Picker.delegate = self
//        datePicker.datePickerMode = .time
//        let pickerToolbar =  UIToolbar(frame:CGRect(x:0, y:0, width:100, height:100))
//        time_food_Picker.inputAccessoryView = pickerToolbar
//        time_food_Picker.inputView = datePicker
//        obcustomDatePicker.createUIToolBar(pickerToolbar: pickerToolbar)

     
        viewClock.backgroundColor = .white
        viewClock.addshadow(top: false, left: false, bottom: false, right: true, shadowRadius: 1.0)
//        let tap = UITapGestureRecognizer(target: self, action: #selector((self.Tapped(_:))))
//        tap.numberOfTapsRequired=1
//        tap.delaysTouchesEnded = true
//        let tapLong = UILongPressGestureRecognizer(target: self, action: #selector((self.Tapped(_:))))
//        tapLong.minimumPressDuration = 0.3
//        tapLong.delaysTouchesEnded = true

//        tapLong.numberOfTapsRequired=2
//        main_view.addGestureRecognizer(tapLong)
//        main_view.addGestureRecognizer(tap)
//        tap.require(toFail: tapLong)
        
        let showPicker = UITapGestureRecognizer(target: self, action: #selector((self.TimePicker(_:))))
               showPicker.numberOfTapsRequired=1
        viewClock.addGestureRecognizer(showPicker)

        isMain.roundCorners(corners: [.topRight, .bottomRight], radius: 5.0)

    
    }
    func checkTime(cutentDate:Date)->Bool{
        let arrDate = tblAlermICateItems.getalertNotificationAsTime(id: id_Notification)
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.string(from:cutentDate)
        let seletedDate = dateFormatter.date(from: date)
        
        if id_Notification == "1" {
            for item in arrDate {
                if seletedDate!.timeIntervalSince1970 > item.time{
                    return false
                }
            }
            return true
            
        }else  if id_Notification == "2" {
            for item in arrDate {
                if item.id == "3" || item.id == "5" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "2" ||  item.id == "1"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }else  if id_Notification == "3" {
            for item in arrDate {
                if seletedDate!.timeIntervalSince1970 < item.time{
                    return false
                }
            }
            return true
        }else  if id_Notification == "4" {
            for item in arrDate {
                if item.id == "3" ||  item.id == "2" || item.id == "5" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "1"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }else  if id_Notification == "5" {
            for item in arrDate {
                if  item.id == "3" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "1" || item.id == "4" ||  item.id == "2"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }
        return true
    }
    
    @objc func TimePicker(_ gestureRecognizer: UITapGestureRecognizer) {
        DPPickerManager.shared.showPicker(title: "الوقت", time: "", flag: 0, idTime: "-2", picker: { (picker) in
                  picker.date = Date()
                  picker.datePickerMode = .time
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "h:mm a"
            let date = dateFormatter.date(from:self.timeFood)
            picker.date = date!
            
        }) { [self] (date, cancel) in
                  if !cancel {
                    if !checkTime(cutentDate:date!) {
                        alert(mes:"يجب ان يكون وقت الوجبة اقل من وقت الوجبات الاخرى" , selfUI: view)
                        return
                    }
                    let formatter = DateFormatter()
                    formatter.timeStyle = .short
                    let time = formatter.string(from:date!)
                    self.setCustomTimr(time:time)
                    self.delegate.updateTime(index: self.index, str:self.txtClock.text! ,cell:self.cell,textDate:time)
//
                }
              }
    }
    
//    @objc func Tapped(_ gestureRecognizer: UITapGestureRecognizer){
//        if gestureRecognizer.numberOfTapsRequired == 1 {
//            cell.unSelected(flag:ModelMEalDistrbution.list[index.row].selected)
//            print("single tap \(ModelMEalDistrbution.list[index.row])")
//
//        }else{
//            if gestureRecognizer.state != UIGestureRecognizer.State.ended  {
//                print(index.row)
//
//            }else{
//                if index.row != 1 && index.row != 3 {
//                    print("double tap \(ModelMEalDistrbution.list[index.row])")
//                    cell.unSelectMain(flag:ModelMEalDistrbution.list[index.row].checkAsMain)
//                }else{
//                    print("disable double tap")
//                }
//            }
//
//        }
//    }
//

       
    
//   let datePicker = UIDatePicker()
//    var pickerToolbar: UIToolbar?
    var index:IndexPath!
    var delegate:updateList!
    var cell:MealDistbutionCell!
    var delegatMain:MealDistrbutions!
    var id_Notification:String!
    var view:MealDistrbutionVC!

    var listImageEnable = ["breakfast","apple","launch","tsbera","dinner"]
    var listImageDisable = ["breakFastDisable","appleDisable","launch","orangeDisable","dinner"]
    var timeFood:String!
//    lazy var obcustomDatePicker = customDatePicker(with: self)
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBOutlet weak var main_view: UIView!
    @IBOutlet weak var txtSpicificTime: UILabel!
    @IBOutlet weak var stackIsMain: UIStackView!
    @IBOutlet weak var isMain: UIView!
//    @IBOutlet weak var time_food_Picker: UITextField!
    @IBOutlet weak var imgClock2: UIImageView!
    @IBOutlet weak var imgClock: UIImageView!
    @IBOutlet weak var txtClock: UILabel!
    @IBOutlet weak var wajbehName: UILabel!
    @IBOutlet weak var viewClock: UIView!
    @IBOutlet weak var imgWajbeh: UIImageView!
    

    
    var constraintVariable: NSLayoutConstraint!


   
    func unSelectMain(flag:Bool){
        let firstView = stackIsMain.arrangedSubviews[1]
        firstView.isHidden = flag
        setConstrain(flag:flag)
        self.delegate.updateMain(index: self.index, flag: !flag,cell:self.cell)
    }
    
    func setMainWajbeh(flag:Bool){
        let firstView = stackIsMain.arrangedSubviews[1]
        firstView.isHidden = flag
        setConstrain(flag:flag)
    }
    
    func setConstrain(flag:Bool){
        UIView.animate(withDuration: 0.1, animations:{
             for constraint in self.main_view.constraints {
                 if constraint.identifier == "tralling" {
                     if flag == false {
                         constraint.constant = 0
                     }else{
                         constraint.constant = 16
                     }
                     break
                 }
             }
             self.main_view.layoutIfNeeded()
         })
    }
    func unSelected(flag:Bool){
        self.delegate.updateWajbeh(index: index, flag: !flag)
    }
    func setColor(color1:UIColor,color2:UIColor,imageSmall:String,imageBig:String,isEnable:Bool,imgCateg:String){
        wajbehName.textColor = color1
        txtClock.textColor = color2
        txtSpicificTime.textColor = color1
        imgClock.image = UIImage(named : imageSmall)
        imgClock2.image = UIImage(named : imageBig)
//        time_food_Picker.isEnabled = isEnable
        if isEnable {
            delegate.updateImageWajbeh(index: index, img: listImageEnable[index.row])

        }else{
            delegate.updateImageWajbeh(index: index, img: listImageDisable[index.row])

        }
    }
    

    
    func setCustomTimr(time:String){
        let index2 = time.range(of: " ", options: .backwards)?.lowerBound
        let substring3 = index2.map(time.substring(to:))
        if time.contains("AM") ||  time.contains("am") || time.contains("صباحا"){
            txtClock.text = "\(substring3!) صباحا"
        }else {
            txtClock.text = "\(substring3!) مساءا"
        }
    
    }
    
    
}

//extension MealDistbutionCell:DatePickerDelegate{
//    func cancelBtnClicked() {
//        time_food_Picker?.resignFirstResponder()
//    }
//
//    func doneBtnClicked() {
//        txtClock?.resignFirstResponder()
//        let formatter = DateFormatter()
//        formatter.timeStyle = .short
//        let time = formatter.string(from: datePicker.date)
//        self.delegate.updateTime(index: self.index, str: setCustomTimr(time:time),cell:self.cell)
//    }
//}


protocol updateList {
    func updateWajbeh(index:IndexPath,flag:Bool)
    
    func updateMain(index:IndexPath,flag:Bool,cell:MealDistbutionCell)
    
    func updateImageWajbeh(index:IndexPath,img:String)
    func updateTime(index:IndexPath,str:String,cell:MealDistbutionCell,textDate:String)
}

extension UIView {
func addshadow(top: Bool,
               left: Bool,
               bottom: Bool,
               right: Bool,
               shadowRadius: CGFloat = 2.0) {

    self.layer.masksToBounds = false
    self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    self.layer.shadowRadius = shadowRadius
    self.layer.shadowOpacity = 1.0

    let path = UIBezierPath()
    var x: CGFloat = 0
    var y: CGFloat = 0
    var viewWidth = self.frame.width
    var viewHeight = self.frame.height

    // here x, y, viewWidth, and viewHeight can be changed in
    // order to play around with the shadow paths.
    if (!top) {
        y+=(shadowRadius+1)
    }
    if (!bottom) {
        viewHeight-=(shadowRadius+1)
    }
    if (!left) {
        x+=(shadowRadius+1)
    }
    if (!right) {
        viewWidth-=(shadowRadius+1)
    }
    // selecting top most point
    path.move(to: CGPoint(x: x, y: y))
    // Move to the Bottom Left Corner, this will cover left edges
    /*
     |☐
     */
    path.addLine(to: CGPoint(x: x, y: viewHeight))
    // Move to the Bottom Right Corner, this will cover bottom edge
    /*
     ☐
     -
     */
    path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
    // Move to the Top Right Corner, this will cover right edge
    /*
     ☐|
     */
    path.addLine(to: CGPoint(x: viewWidth, y: y))
    // Move back to the initial point, this will cover the top edge
    /*
     _
     ☐
     */
    path.close()
    self.layer.shadowPath = path.cgPath
}
}
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
//    func setupCheckBox(flag:Bool,view:UIView){
//        let squareBox = Checkbox(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//        squareBox.tintColor = .black
//        squareBox.borderStyle = .square
//        squareBox.checkmarkStyle = .tick
//        squareBox.uncheckedBorderColor = .lightGray
//        squareBox.borderLineWidth = 2
//        squareBox.checkedBorderColor = colorGray
//        squareBox.checkmarkSize = 0.5
//        squareBox.backgroundColor = colorGray
//        squareBox.checkboxFillColor = .white
//        squareBox.checkmarkColor = colorGray
//        squareBox.isChecked = flag
//        squareBox.valueChanged = { (value) in
//            print("squarebox value change: \(value)")
////            self.delegate.updateWajbeh(index: self.index, flag: value,cell:self.cell)
//        }
//        view.addSubview(squareBox)
//    }
//
//    func setupRadiokBox(flag:Bool,view:UIView){
//        let circleBox = Checkbox(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
//        circleBox.borderStyle = .circle
//        circleBox.checkmarkStyle = .circle
//        circleBox.borderLineWidth = 2
//        circleBox.uncheckedBorderColor = .lightGray
//        circleBox.checkedBorderColor = colorGray
//        circleBox.checkmarkSize = 0.5
//        circleBox.backgroundColor = colorGray
//        circleBox.checkboxFillColor = .white
//        circleBox.checkmarkColor = colorGray
//        circleBox.isChecked = flag
//        circleBox.valueChanged = { (value) in
//            print("squarebox value change: \(value)")
//            self.delegate.updateMain(index: self.index, flag: true,cell:self.cell)
//
//        }
//        // circleBox.addTarget(self, action: #selector(circleBoxValueChanged(sender:)), for: .valueChanged)
//        view.addSubview(circleBox)
//    }
