//
//  CollectionViewController44.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 15/04/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa
import SVProgressHUD
private let reuseIdentifier = "Cell"

class ShoppingList: UICollectionViewController {

    @IBOutlet var tbl: UICollectionView!
    var x:[Int]! = []
    
    let disposeBag = DisposeBag()
    var items : Observable<[shoppingModel]>!
    lazy var obShoppingListPresnter = ShoppingListPresnter(with: self)
    var initialDataAry=[shoppingModel]()
    var dataAry = [shoppingModel]()
    let textLabel = UILabel()
    var count:Int!
    
    func setUpCollectionView() {
         /// 1

         /// 2
        tbl.delegate = self
        tbl.dataSource = self
        tbl.semanticContentAttribute = .forceLeftToRight
         /// 3
         let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical
         /// 4
        layout.minimumLineSpacing = 8
         /// 5
        layout.minimumInteritemSpacing = 4

         /// 6
        tbl
               .setCollectionViewLayout(layout, animated: true)
       }
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpCollectionView()

        
        
//        tbl.register(TablePlannerHeader.self,
//                     forHeaderFooterViewReuseIdentifier: "sectionHeader")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        tbl.rx.modelSelected(shoppingModel.self).subscribe(onNext: {
            item in //selec all row
            if let selectedRowIndexPath = self.tbl.indexPathsForSelectedItems {
                self.tbl.deselectItem(at: selectedRowIndexPath.first!, animated: true)
            }
        }).disposed(by: disposeBag)
        
        
        tbl.rx.itemSelected.subscribe(onNext : {
            [weak self] indexPath in
            if (self?.tbl.cellForItem(at: indexPath) as? ShoppingListCell) != nil {
                
                if let cell = self?.tbl.cellForItem(at: indexPath) as? ShoppingListCell {
                    let item: shoppingModel = try! self!.tbl.rx.model(at: indexPath)
                    if item.isCheck == 0 {
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:1) {  (response) in
                            item.isCheck = 1
                            cell.btn.setImage(UIImage(named: "green_ic_checked"), for: .normal)
                        }
                    }else{
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:0) { (response) in
                            item.isCheck = 0
                            cell.btn.setImage(UIImage(named: "green_ic_unchecked"), for: .normal)
                           
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                        self?.setupShowData()
                    }
                }
            }
        }).disposed(by: disposeBag)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
      
   
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 4{
//            print("H1")
//            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
//                print("H2")
//                SVProgressHUD.dismiss()
//                setupShowData()
//                return
//            }else{
//                DispatchQueue.global(qos: .background).async { [self] in
//                    autoreleasepool {
//                        if setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND date >= %@ AND date <= %@",getUserInfo().id,getUserInfo().startSubMilli,getUserInfo().endSubMilli).isEmpty {
//                            print("H5")
//                            SVProgressHUD.show(withStatus: "جاري تحضير سلة التسوق")
//                            DispatchQueue.main.async { [self] in
//                                self.view.isUserInteractionEnabled = false
//                                self.tabBarController?.view.isUserInteractionEnabled = false
//                            }
//                            tblPackeg.createItemShoppingList() { err in
//                                SVProgressHUD.dismiss{
//                                    print("H6")
//                                    DispatchQueue.main.async { [self] in
//                                        self.view.isUserInteractionEnabled = true
//                                        self.tabBarController?.view.isUserInteractionEnabled = true
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    DispatchQueue.main.async { [self] in
//                        // reload your collection view here:
//                        print("H4")
//                        setupShowData()
//
//                    }
//                }
//            }
//
//        }else{
//            print("H3")
            setupShowData()
//        }
        
   
        
    }
    
    func setupShowData(){
//        for i in Array(setupRealm().objects(tblShoppingList.self)) {
//            print("%%%%",i)
//        }
        obShoppingListPresnter.getAllIngridintForWeek(count:count)
        if dataAry.isEmpty {
            CollectionViewHelper.EmptyMessage(message: "سلة التسوق فارغة", viewController: tbl)
        }else {
            CollectionViewHelper.EmptyMessage(message: "", viewController: tbl)
            
        }
        items = Observable.just(dataAry)
        bindData()
    }
    private func bindData() {
        tbl.dataSource = nil
        items.bind(to: tbl.rx.items(cellIdentifier: "cellShopping")) { row, newitem, cell in
            if let cellToUse = cell as? ShoppingListCell {
                cellToUse.item = newitem
                cellToUse.delegateShopping = self
            }

        }.disposed(by: disposeBag)
    }
    
    
    
    private func prependData(dataToPrepend : [shoppingModel]) {
        let newObserver = Observable.just(dataToPrepend)
        items = Observable.combineLatest(items, newObserver) {
            $1+$0
        }
        bindData()
    }
    
    private func appendData(dataToAppend : [shoppingModel]) {
        let newObserver = Observable.just(dataToAppend)
        items = Observable.combineLatest(items, newObserver) {
            $0+$1
        }
        bindData()
    }
    
    func reloadData(count : Int) {
        if self.count != count{
            obShoppingListPresnter.getAllIngridintForWeek(count:count)
            items = Observable.just(dataAry)
            bindData()
        }
    }
    
    
    
    static var indexID:Int!
    
   
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }


        // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }

    
    /*
     override func tableView(_ tableView: UITableView,
                             viewForHeaderInSection section: Int) -> UIView? {
         let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                                                                 "sectionHeader") as! TablePlannerHeader
         view.title.text = "فلتر"
         view.viewCV = self
         view.image.image = UIImage(named: "filter-menu")
         
         return view
     }
     
     override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return 70
     }
     */


    

}
extension ShoppingList: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        /// 2
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }

    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// 4
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// 5
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        /// 6
        return CGSize(width: widthPerItem - 8, height: 80)
    }
}
extension ShoppingList:ShoppingListView {
    func setListIngridint(list: [shoppingModel]) {
        dataAry.removeAll()
        items = nil
//        print("list \(list.count)")
        dataAry = list//.removeDuplicates()
    }
}
