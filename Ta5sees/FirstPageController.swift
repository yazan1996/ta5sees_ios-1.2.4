//
//  FirstPageController.swift
//  Ta5sees
//
//  Created by Admin on 8/23/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
//import SwiftGifOrigin
import SwiftyGif
import FirebaseAuth
import Firebase
import SwiftyJSON
import FlagPhoneNumber
//import KSFacebookButton
import TwitterKit
import Realm
import RealmSwift
import Alamofire
import SwiftyJSON
import SVProgressHUD
//import RNCryptor
import CommonCrypto
import FLAnimatedImage
import FirebaseFirestore
import FirebaseCore
import FirebaseDatabase
import AuthenticationServices
import FirebaseCrashlytics
import FirebaseUI
import ZIPFoundation
import SVProgressHUD
////
import Foundation
import ZIPFoundation
import SwiftyJSON
class FirstPageController:  UIViewController,UIViewControllerTransitioningDelegate ,UITextFieldDelegate,viewFaceBookLogin,viewTwitterLogin,FUIAuthDelegate {
    
    @IBOutlet weak var btnFacebookOutlet: btnFiacebook!
    func getIDUseerTwitter(id: String,name:String,loginType:String){
        checkUserTwitter(UserID:id,name:name,loginType:loginType)
    }
    @IBOutlet weak var loginProviderStackView: UIStackView!
    func errorConnectionTwitter(str: String) {
        alert(mes: str, selfUI: self)
        
    }
    
    func successConnectionTwitter() {
        
    }
    
    @IBOutlet weak var stackButton: UIStackView!
    var obAPI = APiSubscribeUser()
    let onAPiSubscribeUser = APiSubscribeUser()
    @IBAction func btnAppleID(_ sender: Any) {
        createEvent(key: "45", date: getDateTime())
        if #available(iOS 13.0, *) {
            appleProvider.handleAppleIdRequest(block: { fullName, email, token in
                // receive data in login class.
                
                if getDataFromSheardPreferanceString(key: "mobNum") != "0" || !getDataFromSheardPreferanceString(key: "mobNum").isEmpty {
                    
                    self.checkUserAppleID(UserID:getDataFromSheardPreferanceString(key: "mobNum"),name:getDataFromSheardPreferanceString(key: "name"))
                }else{
                    showToast(message: "نعتذر حدث مشلكة", view: self.view,place:0)
                }
            })
        } else {
            // Fallback on earlier versions
        }
        
        
        
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let user = authDataResult?.user {
            print("user \(user.uid) \(user.email!)")
        }
    }
    func getIDUseer(id: String,name:String,loginType:String) {
        checkUserFacebook(UserID:Int(id)!,name:name,loginType:loginType)
    }
    
    func HideProgress() {
        SVProgressHUD.dismiss()
        
    }
    func errorConnection(str:String) {
        alert(mes: str, selfUI: self)
    }
    
    func successConnectionFacebook() {
        //        self.performSegue(withIdentifier: "toHomePage", sender: self)
    }
    
    func showProgress() {
        SVProgressHUD.show()
    }
    
    lazy var obloginFacebook = loginFacebook(with:self, delege: self,flag:1)
    lazy var obLoginTwitter = LoginTwitter(with:self, delege: self,flag:1,delegte: self)
    
    
    let encryptionKEY = "enUserKey"
    let logoAnimationView = LogoAnimationView()
    @available(iOS 13.0, *)
    lazy var appleProvider = AppleSignInClient(with: self)
    
    let image=FLAnimatedImageView()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 13, *) {
            // iOS 11 (or newer) ObjC code
            NSLog("ios above 13")
        } else {
            let item =  stackButton.arrangedSubviews[2]
            item.isHidden = true
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.isUserInteractionEnabled = false
        image.frame.size.width = view.frame.size.width
        image.frame.size.height = view.frame.size.height
        view.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        image.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        image.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width).isActive = true
        image.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.height).isActive = true
        
        if let path =  Bundle.main.path(forResource: "LogoAnimation", ofType: "gif") {
            if let data = NSData(contentsOfFile: path) {
                let gif = FLAnimatedImage(animatedGIFData: data as Data)
                self.image.animatedImage = gif
                self.image.loopCompletionBlock = {_ in
                    self.image.stopAnimating()
                    
                    if UserDefaults.standard.integer(forKey: "login") == 1 {
                        
                        self.goToMain()
                    }else {
                        self.image.isHidden = true
                        self.view.isUserInteractionEnabled = true
                    }
                    
                    
                }
            }
        }
        
    }
    
    @IBAction func btnFacebookLogin(_ sender: Any) {
        createEvent(key: "35", date: getDateTime())
        obloginFacebook.faceBookConnection()
        
    }
    
    @IBAction func btnTwitterLogin(_ sender: Any) {
        createEvent(key: "40", date: getDateTime())
        obLoginTwitter.LoginTwitter(){bool in
            print(bool)
        }
    }
    @IBAction func btnGoogleLogin(_ sender: Any) {
        //        checkUser(UserID:0)
        
    }
    @IBAction func btnMSISDNLogin(_ sender: Any) {
        createEvent(key: "50", date: getDateTime())
        Crashlytics.crashlytics().log("view error")
        
        checkUserMSISDN()
    }
    
    
    public func checkUserFacebook(UserID:Int,name:String,loginType:String){
        
        tblUserInfo.checkFoundIDUserFacebook(iduser: String(UserID)) { (bool) in
            if bool == true {
                SVProgressHUD.dismiss()
                if UserDefaults.standard.integer(forKey: "login") == 0 {
                    UserDefaults.standard.set(1, forKey: "login")
                    setDataInSheardPreferance(value: String(UserID), key: "userID")
                    print("user login app ")
                }
                
                tblAlermICateItems.setUserAlarm()
                onAPiSubscribeUser.updateSomDataUser(id:String(UserID), arr: ["deviceType": "2","deviceToken": deviceTokens ?? "","appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                self.performSegue(withIdentifier: "MovetoHomePage", sender: self)
            }else {
                
                checkdataFromFirestore(id:String(UserID), name: name, loginType: loginType)
                
                
            }
        }
    }
    public func checkUserTwitter(UserID:String,name:String,loginType:String){
        
        tblUserInfo.checkFoundIDUserTwitterID(iduser: UserID) { (bool) in
            if bool == true {
                SVProgressHUD.dismiss()
                if UserDefaults.standard.integer(forKey: "login") == 0 {
                    UserDefaults.standard.set(1, forKey: "login")
                    setDataInSheardPreferance(value: UserID, key: "userID")
                    print("user login app ")
                }
                tblAlermICateItems.setUserAlarm()
                onAPiSubscribeUser.updateSomDataUser(id:UserID, arr: ["deviceType": "2","deviceToken": deviceTokens!,"appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                self.performSegue(withIdentifier: "MovetoHomePage", sender: self)
            }else {
                checkdataFromFirestore(id:UserID, name: name, loginType: loginType)
                
                
            }
        }
    }
    
    public func checkUserAppleID(UserID:String,name:String){
        
        tblUserInfo.checkFoundIDUserAppleID(iduser: UserID) { (bool) in
            if bool == true {
                SVProgressHUD.dismiss()
                if UserDefaults.standard.integer(forKey: "login") == 0 {
                    UserDefaults.standard.set(1, forKey: "login")
                    setDataInSheardPreferance(value: UserID, key: "userID")
                    print("user login app ")
                }
                tblAlermICateItems.setUserAlarm()
                onAPiSubscribeUser.updateSomDataUser(id:UserID, arr: ["deviceType": "2","deviceToken": deviceTokens!,"appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                self.performSegue(withIdentifier: "MovetoHomePage", sender: self)
            }else {
                setDataInSheardPreferance(value: "4", key: "loginType")
                print("UserID \(UserID)")
                checkdataFromFirestore(id: UserID, name: name, loginType: "4")
                
            }
        }
    }
    
    
    func checkdataFromFirestore(id:String,name:String,loginType:String){
        print("idTW",id)
        let isExist = AppDelegate.db.collection("Users").document(id)
        let syncConc = DispatchQueue(label:"con",attributes:.concurrent)
        let progressHistory = AppDelegate.db.collection("UserProgressHistory").document(id).collection(id)
        
        syncConc.sync {
            isExist.getDocument { [self] (document, error) in
                if let document = document, document.exists {
                    if  document.data()!.isEmpty{
                        goToGenderCV(id: id, name: name, loginType: loginType)
                    }else{
                        tblUserInfo.saveUserInfoData(doc: document) { (tblUserInfo) in
                            print("save user success")
                            progressHistory.getDocuments { querySnapshot, err in
                                tblUserProgressHistory.saveUserProgressHistoryInfoData(docs: querySnapshot!.documents) { string in
                                    if setupRealm().isInWriteTransaction {
                                        setupRealm().cancelWrite()
                                    }
                                   
                                    onAPiSubscribeUser.updateSomDataUser(id:id, arr: ["deviceType": "2","deviceToken": deviceTokens ?? "","appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                                    setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
                                    setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
                                    setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
                                    setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                                    setDataInSheardPreferance(value: "1", key: "isTutorial")
                                    setDataInSheardPreferance(value: "1", key: "isTutorial-water")
                                    _ = TryCalaulate(doc:document)
                                    if UserDefaults.standard.integer(forKey: "login") == 0 {
                                        UserDefaults.standard.set(1, forKey: "login")
                                        print("user login app ")
                                        SVProgressHUD.dismiss {
                                            setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                                            self.performSegue(withIdentifier: "MovetoHomePage", sender: self)
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    
                }else{
                    
                    goToGenderCV(id: id, name: name, loginType: loginType)
                }
            }
        }
        
    }
    
    func formatDate(date:String)->String{
        var date_copy = ""
        if date == "" {
            date_copy = getDateOnly()
            return date_copy
        }else{
            date_copy = date
        }
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd - MM - yyyy"
        let d = dateFormatter1.date(from: date_copy)
        let newFormate = dateFormatter.string(from: d!)
        return newFormate
        
    }
    func goToGenderCV(id:String,name:String,loginType:String){
//        dispatchGroup.enter()
        setDataInSheardPreferanceInt(value: 0, key: "counterRetryLoad")
        setDataInSheardPreferance(value: id, key: "userID")
        Downloader.loadData_firebase(flag:1) { (bool) in
          
            if bool {
//                if getDataFromSheardPreferanceString(key:"loadWajbehSenf") == "0" {
                    print("loadWajbehSenf == 0")

                    DispatchQueue.main.async {
                        UIApplication.shared.isIdleTimerDisabled = false
                    }
//                    dispatchGroup.leave()
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "dissmesDialog"),object: nil))
                    
                    //                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName1"), object: self, userInfo:nil)
//                }
                setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
                print("loadData_firebase \(bool)")
            }else{
                print("loadData_firebase false")
            }
            return
            
        }
        setDataInSheardPreferance(value: name, key: "name")
        setDataInSheardPreferance(value: id, key: "mobNum")
        setDataInSheardPreferance(value: loginType, key: "loginType")
        setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
        setDataInSheardPreferance(value: "0", key: "isTutorial-tamreen")
        setDataInSheardPreferance(value: "0", key: "isTutorial-isBeign")
        setDataInSheardPreferance(value: "0", key: "isTutorial-nutration")
        setDataInSheardPreferance(value: "0", key: "isTutorial-water")
        setDataInSheardPreferance(value: "0", key: "isTutorial")
        //        obAPI.saveUserFirestore(id:id)
        SVProgressHUD.dismiss()
        self.delegetGenderType()
    }
    func saveExchangeSystem(id:String){
        let item = setupRealm().objects(tblExchangeSystem.self).filter("id = %@", idUserExchangeSystem!).first!
        try! setupRealm().write {
            item.setValue(id, forKeyPath: "refUserID")
        }
    }
 
    public func checkUserMSISDN(){
        
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "newLoginActivity") as! newLoginActivity
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func goToMain(){
        print("isDownloadAPI",getDataFromSheardPreferanceString(key: "isDownloadAPI"))
        if getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "1" {
            setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
            setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
            setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
            setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
            setDataInSheardPreferance(value: "1", key: "isTutorial-water")
            setDataInSheardPreferance(value: "1", key: "isTutorial")
        }else if getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "0" {
            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
            setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
            setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
            setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
            setDataInSheardPreferance(value: "1", key: "isTutorial-water")
            setDataInSheardPreferance(value: "1", key: "isTutorial")
        }

        
        
        
        let mainStoryboard:UIStoryboard=UIStoryboard(name:"Main", bundle:nil)
        let yourVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain")
        //
        self.present(yourVC, animated: true, completion: nil)
        //        let appDelegate = UIApplication.shared.delegate as!AppDelegate
        //              appDelegate.window = UIWindow(frame:UIScreen.main.bounds)
        //              let mainStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        //              let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain") as! UITabBarController
        //              tabbarVC.selectedViewController = tabbarVC.viewControllers![0]
        //
        //              appDelegate.window?.rootViewController = tabbarVC
        //              appDelegate.window?.makeKeyAndVisible()
    }
   
}
 
extension FirstPageController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
    }
}

 
extension FirstPageController:PresenterAppleID {
    func goToHomePage() {
        self.performSegue(withIdentifier: "MovetoHomePage", sender: self)
        
    }
    
    func goToGenderTypePage() {
        delegetGenderType()
    }
}
 
extension FirstPageController {
    func delegetGenderType(){
        setDataInSheardPreferance(value: "1", key: "finishRegistration")
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "GenderType") as! GenderType
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
                
        self.present(detailView, animated: true, completion: nil)
    }
}
//

class newQier {
    
    
    var s:[String:Any]
    init(c:QueryDocumentSnapshot!) {
        let i = c.data()["id"] as? String
        let index = i!.index(i!.startIndex, offsetBy: 6)
        let mySubstring = i!.prefix(upTo: index) // Hello
        
        let index12 = i!.index(i!.endIndex, offsetBy: -4)
        let mySubstring12 = i!.suffix( from: index12) // Hello
        
        let replaced = i!.replacingOccurrences(of: mySubstring, with: "\(mySubstring).")
        
        let r = replaced.replacingOccurrences(of: mySubstring12, with: ".\(mySubstring12)")
        print(" r \(r)")
        
        
        
        
        s = [
            "id" :r ,
            "firstName" :  c.data()["firstName"] as! String,
            "facebookID" :  c.data()["facebookID"] as? String ?? NSNull(),
            "refGenderID" : c.data()["refGenderID"] as! Int ,
            "height" :  c.data()["height"] as! Double ,
            "weight" :  c.data()["weight"] as! Double ,
            "birthday" : c.data()["birthday"] as! String,
            "refLayaqaCondID" :c.data()["refLayaqaCondID"] as! Int ,
            "reftblCountryID" :  c.data()["reftblCountryID"] as! Int,
            "mobNum" :  c.data()["mobNum"] as? String ?? NSNull(),
            "password" :  c.data()["password"] as? String ?? NSNull(),
            "insertDateTime" : c.data()["insertDateTime"] as! String ,
            "refPlanMasterID" :  c.data()["refPlanMasterID"] as! Int ,
            "mealDistributionId" :  c.data()["mealDistributionId"] as! Int ,
            "target" : c.data()["target"] as! Double ,
            "refCuiseseID" :  c.data()["refCuiseseID"] as! String ,
            "gramsToLose" :  c.data()["gramsToLose"] as! Double ,
            "grown" :  c.data()["grown"] as! String ,
            "loginType" : c.data()["loginType"] as! Int ,
            "weeksNeeded" :  c.data()["weeksNeeded"] as! Int,
            "twitterID" :  c.data()["twitterID"] as? String ?? NSNull(),
            "totalCalories" :  c.data()["totalCalories"] as! Double ,
            "refAllergyInfoID" :  c.data()["refAllergyInfoID"] as! String ,
            "appleID" : r,
            "mealPlan" : c.data()["mealPlan"] as! [Any]
        ]
        
        //        if r == "000993.a988622e50f544edbb74f3629ce448ee.1102" {
        //        db.collection("Users").document(r).setData(s) { err in
        //                    if let err = err {
        //                        print("Error writing document: \(err)")
        //                    } else {
        //
        //                        print("YESYES")
        //                    }
        //                }
        
        //    }
    }
    
}



class arr {
    var id:Int!
    var item:String!
}


//        print("*******")
//        UIView.animate(withDuration: 2, animations:{
//            print("*******$$$$$")
//        /Users/admin/Desktop/ta5sees_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp  /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p ios /Users⁩/admin⁩/Library⁩/Developer⁩/Xcode⁩/Archives⁩/2020-10-25⁩/Ta5sees 10-25-20, 12.16 PM.xcarchive⁩/dSYMs

//find dSYM_directory -name "*.dSYM" | xargs -I \{\} $PODS_ROOT/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p platform \{\}
//‎⁨Users⁩/admin⁩/Library⁩/Developer⁩/Xcode⁩/Archives⁩/ ⁨2020-10-26⁩/dSYMs

//        /Users/admin/Desktop/ta5sees_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp  /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p ios /Users⁩/admin⁩/Library⁩/Developer⁩/Xcode⁩/Archives⁩/2020-10-26⁩/dSYMs


/*
 sh "${SRCROOT}/Pods/TestFairy/upload-dsym.sh" 14be9a61ca1ec126993ec8727ade1175905fe328
 "${PODS_ROOT}/FirebaseCrashlytics/run"
 find dSYM_directory -name "*.dSYM" | xargs -I \{\} $PODS_ROOT/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p platform \{\}
 
 
 */




/*
 
 /*
 "find ${DWARF_DSYM_FOLDER_PATH}" -name "*.dSYM" | xargs -I \{\} "$PODS_ROOT/FirebaseCrashlytics/upload-symbols" -gsp "$PODS_ROOT/../GoogleService-Info.plist" -p ios \{\}
 /Users/admin/Desktop/ta5sees_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p ios /Users/admin/Library/Developer/Xcode/Archives/2020-10-27/Ta5sees\ 10-27-20\,\ 3.50\ PM.xcarchive/dSYMs/Ta5sees.app.dSYM
 */
 
 //   /Users/admin/Desktop/ta5sees_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p ios /Users/admin/Library/Developer/Xcode/Archives/2020-10-26/Ta5sees\ 10-26-20\,\ 4.27\ PM.xcarchive/dSYMs/Ta5sees.app.dSYM
 
 
 
 new command *******
 
 /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Pods/FirebaseCrashlytics/upload-symbols -gsp  /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Ta5sees/GoogleService-Info.plist -p ios /Users/telecomenterprise/Library/Developer/Xcode/Archives/2021-06-01/Ta5sees\ 01-06-2021\,\ 6.43\ AM.xcarchive/dSYMs
 
 
 /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Ta5sees/GoogleService-Info.plist -p ios /Users/telecomenterprise/Downloads/appDsyms
 
 /*
 
 /Users/admin/Desktop/ta5sees_ios/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios/Ta5sees/GoogleService-Info.plist -p ios /Users/admin/Library/Developer/Xcode/Archives/2020-10-26/Ta5sees\ 10-26-20\,\ 4.27\ PM.xcarchive/dSYMs/Ta5sees.app.dSYM
 */
 //    static func readLocalFile(name: Data) -> Data? {
 //        do {
 //                let decodedData = try JSONDecoder().decode(DemoData.self,
 //                                                           from: jsonData)
 //
 //                print("Title: ", decodedData.title)
 //                print("Description: ", decodedData.description/UserProgressHistory)
 //                print("===================================")
 //            } catch {
 //                print("decode error")
 //            }
 //   }
 */

// --> code firebase crashes
//        Crashlytics.crashlytics().log("view error")
//        fatalError()
/*
 /Users/admin/Movies/ta5sees_ios_new/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Movies/ta5sees_ios_new/Ta5sees/GoogleService-Info-4.plist -p ios /Users/admin/Library/Developer/Xcode/Archives/2020-12-08/Ta5sees\ 12-8-20\,\ 6.52\ PM.xcarchive/dSYMs
 */
extension FirstPageController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}

