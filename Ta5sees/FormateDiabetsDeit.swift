//
//  regesterStep10.swift
//  Ta5sees
//
//  Created by Admin on 8/28/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown
import TextFieldEffects
class FormateDiabetsDeit: UIViewController,UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var txtAddTypeInsulinOutlet: UIButton!
    @IBOutlet weak var imageDiet: UIImageView!
    @IBOutlet weak var titlePage: UILabel!
    
    @IBOutlet weak var txtinsolin2: HoshiTextField!
    @IBOutlet weak var txtinsolen1: HoshiTextField!
    @IBOutlet weak var pregnant_months: UILabel!
    @IBOutlet weak var midication_type_option_one: UILabel!
    
    @IBOutlet weak var midication_type_option_two: UILabel!
    @IBOutlet weak var FBG: HoshiTextField!
    @IBOutlet weak var isHiddinSeekBar: UIStackView!
    @IBOutlet weak var typeDiabets: UILabel!
    override func viewDidLoad(){
        super.viewDidLoad()
        setFontText(text:titlePage,size:30)
        titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        imageDiet.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
        self.HideKeyboard()
        clicableLabel(lbl:typeDiabets,tag:1)
        clicableLabel(lbl:midication_type_option_one,tag:2)
        clicableLabel(lbl:midication_type_option_two,tag:3)
        clicableLabel(lbl:pregnant_months,tag:4)
        
    }
    func clicableLabel(lbl:UILabel,tag:Int){
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lbl.tag = tag
        lbl.isUserInteractionEnabled = true
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 1 {
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "نوع السكري", "options": typeDiabets.text,"flag":"4"])
        }else if tappedImageView.tag == 2 {
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "نوع الانسولين", "options": typeDiabets.text,"flag":"5"])
        }else if tappedImageView.tag == 3 {
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "نوع الانسولين", "options": typeDiabets.text,"flag":"6"])
        }else if tappedImageView.tag == 4 {
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "مرحلة الحمل", "options": typeDiabets.text,"flag":"7"])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //  flag 4 >>> typeDiabets
        //  flag 5 >>> typeMediction
        //  flag 6 >>> typeMedictiontwo
        if let dis=segue.destination as? MultiSeletetionView {
            if  let dictionary=sender as? [String:String] {
                dis.title_Navigation = dictionary["title"]
                dis.obselectionDelegate = self
                dis.optionsSelected = dictionary["options"]
                dis.flag = dictionary["flag"]
            }
        }
    }
    @IBAction func btnShowAddInsolin(_ sender: Any) {
        let firstView8 = isHiddinSeekBar.arrangedSubviews[7]
        let firstView9 = isHiddinSeekBar.arrangedSubviews[8]
        if firstView8.isHidden == true {
            UIView.animate(withDuration: 0.5) {
                firstView8.isHidden = false
                firstView9.isHidden = false
                self.txtAddTypeInsulinOutlet.setTitle("إضافة نوع اخر - إخفاء",for: .normal)
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                firstView8.isHidden = true
                firstView9.isHidden = true
                 self.txtAddTypeInsulinOutlet.setTitle("إضافة نوع اخر",for: .normal)
            }
            
            
        }
    }
    
    func HideSeekbar(flag:Bool,flag2:Bool,flag3:Bool,flag4:Bool,flag5:Bool,flag6:Bool){
        let firstView3 = isHiddinSeekBar.arrangedSubviews[1]
        firstView3.isHidden = flag
        let firstView1 = isHiddinSeekBar.arrangedSubviews[2]
        firstView1.isHidden = flag2
        let firstView4 = isHiddinSeekBar.arrangedSubviews[3]
        firstView4.isHidden = flag3
        let firstView41 = isHiddinSeekBar.arrangedSubviews[4]
        firstView41.isHidden = flag4
        let firstView11 = isHiddinSeekBar.arrangedSubviews[5]
        firstView11.isHidden = flag5
        let firstView114 = isHiddinSeekBar.arrangedSubviews[6]
        firstView114.isHidden = flag6
        
        self.HideKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    var refIDiet = getDataFromSheardPreferanceString(key: "dietType")
    //func setBackground(){
    //    imgBackground(imgname: "curve")
    // }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //setBackground()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            if getDataFromSheardPreferanceString(key: "gender") == "1" {
                HideSeekbar(flag:false,flag2:false, flag3: false,flag4:true,flag5:false, flag6: false)
                
                
                
                
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
                
            }else{
                HideSeekbar(flag:false,flag2:false, flag3: false,flag4:false,flag5:false, flag6: false)
                
                // here
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
            }
        }else{ //child
            if getDataFromSheardPreferanceString(key: "gender") == "1" {
                HideSeekbar(flag:true,flag2:false, flag3: false,flag4:true,flag5:false, flag6: false)
                
                
                
                
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
                
            }else{
                HideSeekbar(flag:true,flag2:false, flag3: false,flag4:false,flag5:false, flag6: false)
                
                
                //here
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
            }
        }
        
        
    }
    /*func imgBackground (imgname:String) {
     
     let Diabetes = UIImageView(frame: CGRect(x: 224, y: 68, width: 100, height: 100))
     Diabetes.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
     self.subview.addSubview(Diabetes)
     
     let spicialDiet = UILabel(frame: CGRect(x: 8, y: 13, width: 224, height: 96))
     spicialDiet.textAlignment = .center
     spicialDiet.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
     spicialDiet.textColor = UIColor(red: 255, green: 255, blue: 255)
     spicialDiet.font =  UIFont(name: "GE Dinar One", size: 25.0)!
     self.subview.addSubview(spicialDiet)
     }*/
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    @IBOutlet weak var subview: UIView!
    
    @IBAction func btnNext(_ sender: Any) {
        
        if isHiddinSeekBar.arrangedSubviews[1].isHidden == false  && typeDiabets.text! == "اختر نوع السكري" {
            showAlert(str:"يجب اختيار نوع السكري")
        }else if isHiddinSeekBar.arrangedSubviews[2].isHidden == false  && midication_type_option_one.text! == "اختر نوع الانسولين"  {
            showAlert(str:"يجب اختيار نوع الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[4].isHidden == false  && pregnant_months.text! == "اختر فترة الحمل"{
            showAlert(str:"يجب اختيار فترة الحمل")
        }else if isHiddinSeekBar.arrangedSubviews[3].isHidden == false  && Formatter(str:txtinsolen1).text!.isEmpty {
            showAlert(str:"يجب ادخال كمية الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[5].isHidden == false  && Formatter(str:FBG).text!.isEmpty {
            showAlert(str:"يجب ادخال كمية الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[8].isHidden == false  && midication_type_option_two.text! == "اختر نوع الانسولين"  {
            showAlert(str:"يجب اختيار نوع الانسولين ")
        }else if isHiddinSeekBar.arrangedSubviews[7].isHidden == false  && Formatter(str:txtinsolin2).text!.isEmpty {
            showAlert(str:"يجب ادخال نسبة الانسولين")
        }else{
            if txtinsolin2.text!.isEmpty {
                txtinsolin2.text! = "0"
            }
            setDataInSheardPreferance(value: FBG.text!, key: "FBG")
            setDataInSheardPreferance(value: txtinsolen1.text!, key: "insolin1")
            setDataInSheardPreferance(value: txtinsolin2.text!, key: "insolin2")
            
            let detailView = storyboard!.instantiateViewController(withIdentifier: "newRegesterStep9") as! FormatDiabetsDiet2
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            detailView.transitioningDelegate = self
            present(detailView, animated: true, completion: nil)
            self.performSegue(withIdentifier: "step12", sender: nil)
        }
        //
    }
    func Formatter(str:HoshiTextField)->HoshiTextField{
        let NumberStr: String = str.text!
        let Formatter = NumberFormatter()
        Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        if let final = Formatter.number(from: NumberStr) {
            str.text = final.stringValue
        }
        return str
    }
    
    func showAlert(str:String){
        let alert = UIAlertController(title: "خطأ", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        subview.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        subview.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.subview.frame.origin.y == 0 {
                self.subview.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.subview.frame.origin.y != 0 {
            self.subview.frame.origin.y = 0
        }
    }
    
    func savetypeDiabets(strId:String){
        setDataInSheardPreferance(value:  strId, key: "PregancyMonth")
        
    }
}



extension FormateDiabetsDeit:selectionDelegate {
    
    func setPragnentMonths(str: String, id: String) {
        print(str)
        print(id)
        if !str.isEmpty {
            pregnant_months.text = str
            savetypeDiabets(strId: id)
        }else{
            pregnant_months.text = "اختر فترة الحمل"
            savetypeDiabets(strId:id)
        }
    }
    
    func setDiabetsType(str: String, typeDiabites: String) {
        if !str.isEmpty {
            typeDiabets.text = str
            savetypeDiabets(strId: typeDiabites)
        }else{
            typeDiabets.text = "اختر نوع السكري"
            savetypeDiabets(strId:typeDiabites)
        }
    }
    
    func setMidctionDiabetsTypeOptionOne(str: String,type: String) {
        
        if !str.isEmpty {
            midication_type_option_one.text = str
            savetMidctionDiabetsTypeOptionOne(str: type)
        }else{
            midication_type_option_one.text = "اختر نوع الانسولين"
            savetMidctionDiabetsTypeOptionOne(str:type)
        }
    }
    
    func setMidctionDiabetsTypeOptionTwo(str: String,type: String) {
        print(str)
        print(type)
        if !str.isEmpty {
            midication_type_option_two.text = str
            savetMidctionDiabetsTypeOptionTwo(str: type)
        }else{
            midication_type_option_two.text = "اختر نوع الانسولين"
            savetMidctionDiabetsTypeOptionTwo(str:type)
        }
    }
    func setSeletedCusine(str:String,strID:String){
    }
    
    func setSeletedallaegy(str:String,strID:String){
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        
    }
}
