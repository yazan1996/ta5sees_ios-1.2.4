//
//  viewDelegateMessage.swift
//  Ta5sees
//
//  Created by Admin on 2/20/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseDatabase
import FirebaseFunctions
import SVProgressHUD
class viewPresnterMessage {
    var obMessage:Message!
    var view:viewDelegteMessages!
    var viewCv:UIView!
    var obUser:tblUserInfo!
    var user_id:String!
    lazy var functions = Functions.functions()

    init(with view:viewDelegteMessages,viewCv:UIView) {
        self.view = view
        self.viewCv = viewCv
    }
    func setChat(completion: @escaping (Bool) -> Void){
        ref.child("chats").child("\(self.user_id!)").child("done").setValue(false){
           (error:Error?, ref:DatabaseReference) in
           if let error = error {
             print("Data could not be saved: \(error).")
             completion(false)
           } else {
             print("Data saved successfully!")
             completion(true)
           }
         }
         completion(false)
     }
    
    func sentMessage(msg:[String:Any],completion: @escaping (String)->Void){
        //        DispatchQueue.global().async {
        
        
        setChat { (bool) in
            if bool {
                let FirebaseMessage=self.setUpDatabaseReference().child("messages").child("\(self.user_id!)").childByAutoId()
                FirebaseMessage.setValue(msg) { [self]
                    (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        print("Data could not be saved: \(error).")
                        self.addToList(msg:msg)
                        //                completion("")
                    }else  {
                        print("Data saved successfully! \(ref.key!)")
                        print("MSG \(msg["textMessage"] as! String)")
                        pushNotfy(msg:msg["textMessage"] as! String, firstName: self.obUser.firstName)
//                        FirebaseMessage.removeAllObservers()
                        completion(ref.key!)
                        
                        //                self.view.cleareTextxMess age()
                    }
                }
            }else{
                print("not sent chat")
            }
        }
        
    }
    
    func pushNotfy(msg:String,firstName:String){
        functions.httpsCallable("sendNotification").call(["body": msg,"topic":"cHtH0j3SWFRxrR7KbLWINrl92fU2","title":firstName,"currentUserID":getDataFromSheardPreferanceString(key: "userID")]) { (result, error) in
            if let error = error as NSError? {
                print("failed notification")
                if error.domain == FunctionsErrorDomain {
                    _ = FunctionsErrorCode(rawValue: error.code)
                    _ = error.localizedDescription
                    _ = error.userInfo[FunctionsErrorDetailsKey]
                }
            }
            //            if let text = (result?.data as? [String: Any])?["text"] as? String {
            //                print("result text notification \(text)")
            //            }
            print("result text notification")
        }
    }
//    }
    
    
   
    func generateURlAudio(url:URL){
        let date = getDateOnlyAsMillSeconde()
        let urlLocal = url.absoluteString
        let msg=[
            "seen":"لم يتم رؤيته",
            "receiverId": "cHtH0j3SWFRxrR7KbLWINrl92fU2",
            "senderId":"\(self.user_id!)",
            "textMessage":"تسجيل صوتي",
            "date": date,
            "voiceUrl":url
            ] as [String:Any]
        self.addToList(msg:msg)
        CheckInternet.checkIntenet { [self] (bool) in
            if !bool {
                SVProgressHUD.dismiss()
                showToast(message: "لا يوجد اتصال بالانترنت", view: view as! UIView, place: 0)
                return
            }
        }
        self.generateURlAudioStorageFirebase(url: url) { (url) in
            self.sentMessageAudio(url: url,date:Int(date),urlLocal:urlLocal)
        }
    }
    
    func generateURlAudioStorageFirebase(url:URL,completion: @escaping (URL)->Void){
        let fileName = NSUUID().uuidString// + ".m4a"
        let riversRef =  Storage.storage().reference().child(fileName)
        
//        DispatchQueue.global().async {
        let metadata = StorageMetadata()
        metadata.contentType = "audio/mpeg"
            _ = riversRef.putFile(from: url, metadata: metadata) { metadata, error in
                guard let metadata = metadata else {
                    return
                }
                _ = metadata.size
//                metadata.contentType = "audio/mpeg"

                riversRef.downloadURL { (url, error) in
                    if error == nil{
//                        DispatchQueue.main.sync {
                            completion(url!)

//                        }
//                    }
                }
            }
        }
        
    }
    
    func sentMessageAudio(url:URL,date:Int,urlLocal:String){
        let urlString:String = url.absoluteString
        let msg=[
            "seen":"لم يتم رؤيته",
            "receiverId": "cHtH0j3SWFRxrR7KbLWINrl92fU2",
            "senderId":"\(user_id!)",
            "textMessage":"تسجيل صوتي",
            "date": date,
            "voiceUrl":urlString
            ] as [String:Any]
        
        sentMessage(msg:msg) { (str) in
            let message = Message(msgData: msg as [String : AnyObject],id_msg:"0",firstName:self.obUser.firstName,id_user:self.user_id)
            self.view.removeItemAudio(ob: message,url:urlLocal)
        }


        }
        
    
   
    
    func checkInternet(msg:[String:Any],firstname:String,id:String){
        self.addToList(msg:msg)
        
        CheckInternet.checkIntenet { [self] (bool) in
            if !bool {
                SVProgressHUD.dismiss()
                showToast(message: "لا يوجد اتصال بالانترنت", view: viewCv, place: 0)
                return
            }
        }
        
        self.sentMessage(msg: msg) { (strKey) in
            let message = Message(msgData: msg as [String : AnyObject],id_msg:strKey,firstName:firstname,id_user:id)
            
            self.view.removeItem(ob: message)
        }
        
    }
    func addToList(msg:[String:Any]){
//
        view.setListMessage(obMessage: Message(text: "\(msg["textMessage"]!)", PostDate:Int(msg["date"]! as! Int64),mark: "\(msg["seen"]!)",senderID: "\(msg["senderId"]!)", audio: "\(msg["voiceUrl"]!)",flageInternet:"0",receiverId: "cHtH0j3SWFRxrR7KbLWINrl92fU2", urlImage: "not found"))

        self.view.cleareTextxMessage()
        
    }
    
    func setUpDatabaseReference() -> DatabaseReference{
        return  Database.database().reference()
    }
    
    func getUserData(){
        tblUserInfo.getdataUser { (user) in
            self.obUser = user
            if obUser.loginType == "4" {
                user_id = obUser.id.replacingOccurrences(of: ".", with: "_")
            }else{
                user_id = obUser.id
            }
            self.view.setUserData(ob:user)
        }
    }
    
    
}


protocol viewDelegteMessages {
    func setListMessage(obMessage:Message!)
    func updateMessage(index:Int)
    func cleareTextxMessage()
    func setUserData(ob:tblUserInfo)
    func removeItem(ob:Message!)
    func removeItemAudio(ob:Message!,url:String)
}


