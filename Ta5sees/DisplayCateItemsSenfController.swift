//
//  DisplayCateItemsSenfController.swift
//  Ta5sees
//
//  Created by Admin on 9/9/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
//import GTProgressBar
import RealmSwift
import SVProgressHUD
import Firebase
import FirebaseAnalytics

class DisplayCateItemsSenfController: UIViewController {
    @IBOutlet weak var name_senf: UILabel!
    @IBOutlet weak var btnUnitOutlet: UIButton!
    @IBOutlet weak var lblcaloris:UITextView!
    @IBOutlet weak var fat: UIImageView!
    @IBOutlet weak var protein: UIImageView!
    @IBOutlet weak var carbo: UIImageView!
    @IBOutlet weak var dropDown: HADropDown!
    var valueWeightOutlet = "-1"
    lazy var obADDManualSenf = ADDManualSenfItem(with: self)
    var pg:protocolWajbehInfoTracker?
    var ratio = 1.0
    var unit:Int!
    lazy var obPresenter = Presenter(with: self)

    lazy var obAddEatenItem = AddEatenItem(with: self, pd: pg!)
    
    var senf_item:tblSenf!
    var idPackge:Int!
    var id_items:String!
    var date:String!
    var isFavrite:Bool!
    var isEaten:Bool!
    var idItemsEaten:Int!

    @IBAction func buDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var btnFavuriteOutlet: UIButton!
    
    @IBAction func btnFavurite(_ sender: Any) {
        if isFavrite == false {
            let obFavurite = tblItemsFavurite()
            obFavurite.id = obFavurite.IncrementaID()
            obFavurite.refUserId = getDataFromSheardPreferanceString(key: "userID")
            obFavurite.flag = "1"
            obFavurite.refItem = String(idPackge)
            
            try! setupRealm().write {
                setupRealm().add(obFavurite, update: Realm.UpdatePolicy.modified)
                btnFavuriteOutlet.setImage(UIImage(named : "heartred"), for: .normal)
                showToast(message: "تم إضافة الصنف الى المفضلة", view: self.view,place:0)
            }
            isFavrite = true
        }else{
            tblItemsFavurite.deleteItem(id: String(idPackge),flag:"1")
            btnFavuriteOutlet.setImage(UIImage(named : "heart"), for: .normal)
            showToast(message: "تم حذف الصنف من المفضلة", view: self.view,place:0)
            isFavrite = false
        }
    }
    @IBAction func btnDelete(_ sender: Any) {
        obPresenter.getWajbeh(id: idItemsEaten, date: date, caloris: -1, isProposal: ["0"]) { (res, err) in
            try! setupRealm().write{
                setupRealm().delete((res as! tblUserWajbehEaten?)! )
                self.btnDeleteOutlet.isHidden = true
                showToast(message:"تم حذف الصنف", view: self.view,place:0)
                self.dismiss(animated: true, completion: nil)

            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDown.delegate = self
        dropDown.items = ["1","2","3","4","5","6","7","8","9","10","20","50","100","150","200","250","300","400","500","600","650"]
        proccessSearchLimit(id_items: id_items)
        tblRecentUsed.processInsert(id:String(idPackge),flag:"1")
        obADDManualSenf.getNewSenf(idPackge: idPackge)
        lblcaloris.isUserInteractionEnabled = true
        lblcaloris.isEditable = false
        btnUnitOutlet.backgroundColor = .clear
        btnUnitOutlet.layer.cornerRadius = 5
        btnUnitOutlet.layer.borderWidth = 0.5
        btnUnitOutlet.layer.borderColor = colorGray.cgColor
//        valueWeightOutlet.clearsOnBeginEditing = true

        let icon = UIImage(named: "right-arrow")
        btnUnitOutlet.setImage(icon, for: .normal)
        btnUnitOutlet.imageView?.contentMode = .scaleAspectFit
        btnUnitOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        btnUnitOutlet.titleEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10);

        btnUnitOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    }
    
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBAction func btnUnitQuantity(_ sender: Any) {
        showSimpleActionSheet(hesa:Int(round(Double(senf_item.servingTypeWeight)!)), mainhesa:obADDManualSenf.getMainHesa(refMainHesa:Int( senf_item.HesaUnit)!))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.HideKeyboard()
        if isFavrite == true {
            btnFavuriteOutlet.setImage(UIImage(named : "heartred"), for: .normal)
        }
        if  isEaten == false {
            btnDeleteOutlet.isHidden = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @IBAction func btnValueChange(_ sender: Any) {
        if valueWeightOutlet != "-1" {
            let value = valueWeightOutlet
        obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: Int(value)!)
        ratio = Double(value)!
        }else {
            obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: 1)
            ratio = 1.0
        }
    }
    
    func addItems(){
        let ob = tblUserWajbehEaten()
        //        let value:Int? = Int(lblcaloris.text.westernArabicNumeralsOnly)
        ob.id = ob.IncrementaID()
        ob.userID = getDataFromSheardPreferanceString(key: "userID")
        ob.refItemID = String(senf_item.id)
        ob.isWajbeh = "1"
        ob.wajbehInfiItem = id_items
        ob.date = date
        ob.isProposal = "0"
        ob.kCal = "\(lblcaloris.text.westernArabicNumeralsOnly)"// "\(calculate(unit:unit,quatnity:value!,value:senf_item!.servingTypeWeight))"
        //lblcaloris.text.westernArabicNumeralsOnly
        ob.ratio = valueWeightOutlet
        obAddEatenItem.addItems(ob: ob, date: date, calorisBurned: Int(round(Double(ob.kCal)!))) { [self] (error) in
            createEvent(key: "4", date: getDateTime())
            showToast(message : "تم إضافة الصنف ",view:self.view,place:0)
        }
    }
    
    
    @IBAction func btnAdd(_ sender: Any) {
        if valueWeightOutlet != "-1"{
            if id_items != "-1" {
                addItems()
            }else{
                showDialogCateInfo()
            }
        }else{
            showToast(message: "يجب تحديد الكمية", view: view,place:1)
        }
    }
    
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

extension DisplayCateItemsSenfController:viewAddEatenItem{
    func finshPage() {
        dismiss(animated: true,completion: nil)
        
    }
    
}

extension DisplayCateItemsSenfController:viewSetUnit{
 
    func setUnit(title:Int) {}
    func setUnit(title:String) {
        
            btnUnitOutlet.setTitle(title, for: .normal)
    
        
        if valueWeightOutlet != "-1"{
            let value = valueWeightOutlet
        obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: Int(value)!)
        ratio = Double(value)!
            
        }else {
            obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: 1)
            ratio = 1.0
        }
    }
    
    
    
    
}
extension DisplayCateItemsSenfController:viewSetCateInfo{
    func setIDCateInfo(title: String) {
        id_items=title
        addItems()
//        showToast(message : "تم إضافة الصنف ",view:view)
    }
}

extension DisplayCateItemsSenfController:setSenfDailyView{
    func setMainHESA(item: String,unit:String) {
        if !item.contains("غم") {
            btnUnitOutlet.setTitle("(\(Int(round(Double(unit)!))) غم) \(item)", for: .normal)
        }else{
            btnUnitOutlet.setTitle("\(item) \(unit)", for: .normal)
        }
    }
    
    func setDataSenf(wahbeh: tblSenf, quatnity: Int, unit: Int) {
        self.unit = unit
        senf_item = wahbeh
        name_senf.text = senf_item?.trackerName
//        btnUnitOutlet.setTitle("\(String(unit)) غم", for: .normal)
        
        
        if let image = createFinalImageText(str:(String (calculate(unit:unit,quatnity:quatnity,value:(senf_item?.wajbehFatTotal!)!))), view: view, flgBackground: 1) {
            self.protein.image = image
        }
        
        if let image = createFinalImageText(str:(String (calculate(unit:unit,quatnity:quatnity,value:(senf_item?.wajbehProteinTotal!)!))), view: view, flgBackground: 1) {
            self.fat.image = image
        }
        if let image = createFinalImageText(str:(String (calculate(unit:unit,quatnity:quatnity,value:(senf_item?.wajbehCarbTotal!)!))), view: view, flgBackground: 1) {
            self.carbo.image = image
        }
        
        
        let textCal =  "سعرات حرارية   \(Int(calculate(unit:unit,quatnity:quatnity,value:(senf_item?.wajbehCaloriesTotal!)!)))"
        let attributedString1 = NSMutableAttributedString.init(string: textCal)
        //        let range1 = (textCal as NSString).range(of: String(num))
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:40)!, range: (textCal as NSString).range(of: "\(Int(calculate(unit:unit,quatnity:quatnity,value:(senf_item?.wajbehCaloriesTotal!)!)))"))
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:20)!, range: (textCal as NSString).range(of: "سعرات حرارية"))
        
        lblcaloris.attributedText = attributedString1
        
    }
    
    
    
    func calculate(unit:Int,quatnity:Int,value:String)->Int{
        var servingTypeWeight = Double(senf_item.servingTypeWeight)!
        if servingTypeWeight == 0.0 {
            servingTypeWeight = 1.0
        }
        if unit == 1 {
            print("S2")
            let value1:Double = (((Double((value))! * 100.0 ) / servingTypeWeight))
            let value2:Double = value1 / 100.0
            let value3 = value2 * Double(quatnity)
            print(value3)
            print(value2)
            print(value1)
            
            return Int(round(value3))
        }
        print("S3")
        return Int(round(Double(value)! * Double(quatnity)))
    }
}

extension DisplayCateItemsSenfController {
    
    func showSimpleActionSheet(hesa:Int,mainhesa:String)  {
        var str = ""
        if !mainhesa.contains("غم") {
            str = "( \(hesa)غم) \(mainhesa)"
        }else{
            str = "\(hesa) \(mainhesa)"
        }
        
        let alert = UIAlertController(title: "حدد الكمية", message: nil , preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title:str, style: .default, handler: { (_) in
            self.setUnit(title:str)
        }))
        
        alert.addAction(UIAlertAction(title: "١ غم", style: .default, handler: { (_) in
            self.setUnit(title:"1 غم")
        }))
        
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    
}
extension DisplayCateItemsSenfController {
    func showDialogCateInfo(){
        let alert = UIAlertController(title: "اختر نوع الوجبة اليومية", message: nil , preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title:"الافطار", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "1")
        }))
        
        alert.addAction(UIAlertAction(title:  "تصبيرة  صباحي", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "4")
        }))
        
        alert.addAction(UIAlertAction(title: "الغداء", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "2")
        }))
        
        alert.addAction(UIAlertAction(title: "تصبيرة مسائي", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "5")
        }))
        
        alert.addAction(UIAlertAction(title: "العشاء", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "3")
        }))
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
}

extension DisplayCateItemsSenfController : ItemsViewList {
    func setBreakfdast(ListEatenBreakfast: [tblUserWajbehEaten]!) {
        
    }
    
    func setLuansh(ListEatenLaunsh: [tblUserWajbehEaten]!) {
        
    }
    
    func setDinner(ListEatenDinner: [tblUserWajbehEaten]!) {
        
    }
    
    func setTasber1(ListEatenTasbera1: [tblUserWajbehEaten]!) {
        
    }
    
    func setTasber2(ListEatenTasbera2: [tblUserWajbehEaten]!) {
        
    }
    
    func setTamreen(ListTamreen: [tblUserTamreen]!) {
        
    }
    
    
}

extension DisplayCateItemsSenfController: HADropDownDelegate {
    func didSelectItem(dropDown: HADropDown, at index: Int) {
        print("Item selected at index \(dropDown.items[index])")
        valueWeightOutlet = dropDown.items[index]
        if valueWeightOutlet != "-1" {
            let value = valueWeightOutlet
        obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: Int(value)!)
        ratio = Double(value)!
        }else {
            obADDManualSenf.calculateManualQuantityWeightWajbeh(ob: senf_item,unit:Int(btnUnitOutlet.titleLabel!.text!.westernArabicNumeralsOnly)!, quatnity: 1)
            ratio = 1.0
        }
    }
    func didShow(dropDown: HADropDown) {

    }
    override class func didChangeValue(forKey key: String) {
    }
}

extension DisplayCateItemsSenfController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
