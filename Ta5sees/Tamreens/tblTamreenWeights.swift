//
//  tblTamreenWeights.swift
//  Ta5sees tamreenWeights
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblTamreenWeights:Object {
    
    @objc dynamic var id:Int = -4
    @objc dynamic var weight:Int = -4
    @objc dynamic var calories:Int = -4
    @objc dynamic var refTamreenId:Int = -4

 
   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    
    static func getItem(id:Int,weight:Int,completion: @escaping (tblTamreenWeights) -> Void){
        let item:tblTamreenWeights? = setupRealm().objects(tblTamreenWeights.self).filter("refTamreenId == %@ AND weight == %@",id,weight).first
        if item!.isEqual(nil) {
            return
        }
        completion(item!)

    }
    func readJson(completion: @escaping (Bool) -> Void){
       
        let rj = ReadJSON()
        var list = [tblTamreenWeights]()

        rj.readJson(tableName: "tamareen/tamreenWeights") {(response, Error) in
//            let thread =  DispatchQueue.global(qos: .userInitiated)
//            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblTamreenWeights = tblTamreenWeights()
                        data.id = item["id"].intValue
                        data.calories = item["calories"].intValue
                        data.refTamreenId = item["refTamreenId"].intValue
                        data.weight = item["weight"].intValue
                        list.append(data)

                            
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
//                }
                
            }
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
       
        var list = [tblTamreenWeights]()

        if let recommends = JSON(response).array {
                    for item in recommends {
                        let data:tblTamreenWeights = tblTamreenWeights()
                        data.id = item["id"].intValue
                        data.calories = item["calories"].intValue
                        data.refTamreenId = item["refTamreenTypeId"].intValue
                        data.weight = item["weight"].intValue
                        list.append(data)

                            
                        }
                    }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
        completion(true)

        }
    }
