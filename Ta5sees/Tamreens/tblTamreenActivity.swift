//
//  tblTamreenActivity.swift
//  Ta5sees tamreenActivity
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm

class tblTamreenActivity:Object  {

    @objc dynamic var id:Int = -4
    @objc dynamic var discription:String? = ""
 
   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    

    static func getTamreenName(completion: @escaping ([tblTamreenActivity]) -> Void){
        completion(Array(setupRealm().objects(tblTamreenActivity.self)))
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
       
        let rj = ReadJSON()
        var list = [tblTamreenActivity]()

        rj.readJson(tableName: "tamareen/tamreenActivity") {(response, Error) in
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblTamreenActivity = tblTamreenActivity()
                        data.id = item["id"].intValue
                        data.discription = item["description"].stringValue
                        list.append(data)
                            
                        }
                    }
            try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }

        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
       
        var list = [tblTamreenActivity]()

        if let recommends = JSON(response).array {
                    for item in recommends {
                        let data:tblTamreenActivity = tblTamreenActivity()
                        data.id = item["id"].intValue
                        data.discription = item["description"].stringValue
                        list.append(data)
                            
                        }
                    }
            try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                }
        completion(true)

    }
//    func getPosts(completion: @escaping (([Post]) -> Void)) {
//        let postRef = Database.database().reference()
//            .child("tamareen/tamreenActivity")
//        postRef.observeSingleEvent(of: .value) { (snapshot) in
//            completion(Mapper<Post>().mapArray(snapshot: snapshot))
//        }
//    }
//   static func getPosts() -> Observable<[tblTamreenActivity]> {
//        let postRef = Database.database().reference()
//            .child("tamareen/tamreenActivity")
//        print("tamareen/tamreenActivity")
//        return postRef.rx_observeSingleEvent(of: .value)
//            .map { Mapper<tblTamreenActivity>().mapArray(snapshot: $0) }
//    }
}
//extension BaseMappable {
//
//    init?(snapshot: DataSnapshot) {
//        self.init(snapshot: snapshot)
//        print("TTTTTT \(snapshot)")
//
//    }
//}

//extension Mapper {
//    func mapArray(snapshot: DataSnapshot) -> [N] {
//        print("snapshot2222\(snapshot)")
//        return snapshot.children.map { (child) -> N? in
//            if let childSnap = child as? DataSnapshot {
//                return N(snapshot: childSnap)
//            }
//            print("nill")
//
//            return nil
//        //flatMap here is a trick
//        //to filter out `nil` values
//        }.compactMap { $0 }
//    }
//}
//extension DatabaseQuery {
//
//    func rx_observeSingleEvent(of event: DataEventType) -> Observable<DataSnapshot> {
//        return Observable.create({ (observer) -> Disposable in
//            self.observeSingleEvent(of: event, with: { (snapshot) in
//                observer.onNext(snapshot)
//                observer.onCompleted()
//            }, withCancel: { (error) in
//                observer.onError(error)
//            })
//            return Disposables.create()
//        })
//    }
//
//    func rx_observeEvent(event: DataEventType) -> Observable<DataSnapshot> {
//        return Observable.create({ (observer) -> Disposable in
//            let handle = self.observe(event, with: { (snapshot) in
//                observer.onNext(snapshot)
//            }, withCancel: { (error) in
//                observer.onError(error)
//            })
//            return Disposables.create {
//                self.removeObserver(withHandle: handle)
//            }
//        })
//    }
//}
