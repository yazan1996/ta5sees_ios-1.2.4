//
//  tblTamreenType.swift
//  Ta5sees tamreenType
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblTamreenType:Object {
    
    @objc dynamic var id:Int = -4
    @objc dynamic var discription:String? = ""
    @objc dynamic var refTamreenActivity:Int = -4

 
   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    
   
    
    static func getTamreenTypeData(id:String,completion: @escaping ([tblTamreenType]) -> Void){
           
        completion(Array(setupRealm().objects(tblTamreenType.self).filter("refTamreenActivity == %@",Int(id)!)))
       }
    
    func readJson(completion: @escaping (Bool) -> Void){
       
        let rj = ReadJSON()
        var list = [tblTamreenType]()
        rj.readJson(tableName: "tamareen/tamreenType") {(response, Error) in
//            let thread =  DispatchQueue.global(qos: .userInitiated)
//            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblTamreenType = tblTamreenType()
                        data.id = item["id"].intValue
                        data.discription = item["description"].stringValue
                        data.refTamreenActivity = item["refTamreenActivity"].intValue
                        list.append(data)
                       
                            
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
//                }
                
            }
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
       
        var list = [tblTamreenType]()

                if let recommends = JSON(response).array {
                    for item in recommends {
                        let data:tblTamreenType = tblTamreenType()
                        data.id = item["id"].intValue
                        data.discription = item["description"].stringValue
                        data.refTamreenActivity = item["refTamreenActivityId"].intValue
                        list.append(data)
                        }
                    }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                
        }
        completion(true)

    }
}

