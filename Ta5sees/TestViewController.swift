//
//  TestViewController.swift
//  ViewPager-Swift
//
//  Created by Nishan Niraula on 4/13/19.
//  Copyright © 2019 Nishan. All rights reserved.
//

import UIKit
import Firebase
class TestViewController: UIViewController {

    var tabs = [ViewPagerTab]()
    var options: ViewPagerOptions?
    var pager:ViewPager?
    var defualteIndex = 0
    var newTabs = [ViewPagerTab]()
    var delegateMain:ViewController!
    var pg:protocolWajbehInfoTracker?
    override func loadView() {
        
        let newView = UIView()
        newView.backgroundColor = UIColor.white
        
        view = newView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("tabs",tabs.count)
        guard let options = self.options else { return }
        
        pager = ViewPager(viewController: self)
        pager?.setOptions(options: options)
        pager?.setDataSource(dataSource: self)
        pager?.setDelegate(delegate: self)
        
        
        if !tabs.isEmpty || tabs.count != 0 {
            pager?.build()
//            pager?.displayViewController(atIndex: defualteIndex)
        }else{
            if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 2 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                print("numberOfTabsForViewPager1")
                
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                    
                    showButtonAlertSubc()
                }else{
                    showButtonAlertSubc()

                }
            }else{
                showButtonAlertSubc()
                
            }
        }
        
        
    }
    
    
    deinit {
        tabs.removeAll()
        print("Memory Deallocation")
    }
    
    func showButtonAlertSubc(){

    
        let btnRect = CGRect(origin: CGPoint(x: 0,y : 0), size: CGSize(width: view.frame.size.width, height: 44))

        let btn = UIButton(frame: btnRect)
        btn.setTitle("تفعيل إشتراك النظام الغذائي", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false

        btn.titleLabel?.textColor = .white
        btn.backgroundColor = UIColor(red: 106/255, green: 197/255, blue: 182/255,alpha: 1.0)
        btn.titleLabel?.textAlignment = .center;
        btn.titleLabel?.font = UIFont(name:"GE Dinar One", size: 20)!
        btn.titleLabel?.sizeToFit()
        btn.isHighlighted = true
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.layer.cornerRadius = btn.frame.height*0.5

        view.addSubview(btn)
        
        btn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btn.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        btn.widthAnchor.constraint(equalTo: view.widthAnchor,constant: -32).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 44).isActive = true

        btn.addTarget(self, action: #selector(clickSubscribeNow(_:)), for: .touchUpInside)
    }
    
    @objc func clickSubscribeNow(_ sender:UIButton){
        dismiss(animated: true, completion: nil)
        if delegateMain != nil {
            delegateMain.selectItemWithIndex(value: 2)
            createEvent(key: "18", date: getDateTime())
        }
    }
}

extension TestViewController: ViewPagerDataSource {
    
    func numberOfPages() -> Int {
        return tabs.count
    }
    
    func viewControllerAtPosition(position:Int) -> UIViewController {
        
        let vc = ItemViewController()
        vc.itemText = "\(tabs[position].title)"
        vc.txtDateDay = "\(tabs[position].date)"
        vc.MainView = delegateMain
        vc.pg = pg
        tblWajbatUserMealPlanner.getAllData(date: tabs[position].date) {  (arrWajbatUserMealPlanner) in
            vc._arrWajbat = arrWajbatUserMealPlanner.sorted(by:{ $0.refOrderID < $1.refOrderID })
        }
        return vc
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return tabs
    }
    
    func startViewPagerAtIndex() -> Int {
        return defualteIndex
    }
}

extension TestViewController: ViewPagerDelegate {
    
    func willMoveToControllerAtIndex(index:Int) {
        print("Moving to page \(index)")
        
    }

    func didMoveToControllerAtIndex(index: Int) {
        print("Moved to page - \(index)")

        if index == tabs.endIndex-1 {
            let  date_ = tabs.map {$0.date}
            tblWajbatUserMealPlanner.getAllDate3(date: date_[index],flag:1) { [self] arr in
                if arr.isEmpty || arr.count == 0 {
                    print("nill data")
                    return
                }

                for i in 0..<arr.count {
                    if i == 5 {
                        break
                    }
                    tabs.append(ViewPagerTab(title: getDayOfWeek( arr[i]) ?? "", image:UIImage(named: "apple"),date: arr[i]))

                }
                DispatchQueue.main.async {
                    pager?.invalidateCurrentTabs()
                }
            }
        }else if index <= 0 {
            let  date_ = tabs.map {$0.date}
            tblWajbatUserMealPlanner.getAllDate3(date:date_[index],flag:0) { [self] arr in
               
                if arr.isEmpty || arr.count == 0 {
                    print("nill data")
                    return
                }

                
                for i in 0..<arr.count {
                    if i == 5 {
                        break
                    }
                    newTabs.append(ViewPagerTab(title: getDayOfWeek( arr[i]) ?? "", image:UIImage(named: "apple"),date: arr[i]))
                 
                }
                DispatchQueue.main.async {
                    tabs.insert(contentsOf: newTabs, at: 0)
                    pager?.invalidateCurrentTabs()
                    pager?.displayViewController(atIndex: newTabs.endIndex-1)
                    newTabs.removeAll()
                }
            }
        }
    }
    
    func getDayOfWeek(_ date: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        guard let todayDate = formatter.date(from: date) else { return nil }
        let weekday = Calendar(identifier: .gregorian).component(.weekday, from: todayDate)
       
        switch Calendar.current.weekdaySymbols[weekday-1]  {
          case "Sunday":
              return "الأحد"
          case "Monday":
              return "الإثنين"
          case "Tuesday":
              return "الثلاثاء"
          case "Wednesday":
              return "الأربعاء"
          case "Thursday":
              return "الخميس"
          case "Friday":
              return "الجمعة"
          case "Saturday":
              return "السبت"
          default:
              return ""
          }
    }
    
}
extension TestViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
