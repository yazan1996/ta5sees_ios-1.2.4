//
//  CheckInternet.swift
//  Ta5sees
//
//  Created by Admin on 11/29/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD
class CheckInternet {
    
    static  let reachabilityManager = NetworkReachabilityManager(host: "www.apple.com")
    static func checkIntenet(callBack: @escaping (Bool)->Void){
        reachabilityManager?.startListening(onUpdatePerforming: { (status) in
            switch status {

            case .notReachable:
              print("The network is not reachable")
                callBack(false)
            case .unknown :
              print("It is unknown whether the network is reachable")
                callBack(false)
            case .reachable(.ethernetOrWiFi):
              print("The network is reachable over the WiFi connection")
                callBack(true)
            case .reachable(.cellular):
              print("The network is reachable over the WWAN connection")
                callBack(true)
            }
        })
    }
    func disableView(bool:Bool,view:UIView,tabBarController:UITabBarController){
        view.isUserInteractionEnabled = bool
        tabBarController.view.isUserInteractionEnabled = bool
    }
    static func startListnerIntenet(){
        reachabilityManager?.startListening(onUpdatePerforming: { (status) in
            switch status {

            case .notReachable:
              print("The network is not reachable")
                SVProgressHUD.show(withStatus: "يرجى الاتصال بالانترنت")
//                UIApplication.shared.beginIgnoringInteractionEvents()
//                disableView(bool:Bool,view:UIView,tabBarController:UITabBarController)
            case .unknown :
              print("It is unknown whether the network is reachable")
                SVProgressHUD.show(withStatus: "يرجى الاتصال بالانترنت")
//                UIApplication.shared.beginIgnoringInteractionEvents()
//                disableView(bool:Bool,view:UIView,tabBarController:UITabBarController)
            case .reachable(.ethernetOrWiFi):
              print("The network is reachable over the WiFi connection")
//                UIApplication.shared.endIgnoringInteractionEvents()
//                disableView(bool:Bool,view:UIView,tabBarController:UITabBarController)
                SVProgressHUD.dismiss()
            case .reachable(.cellular):
              print("The network is reachable over the WWAN connection")
//                UIApplication.shared.endIgnoringInteractionEvents()
//                disableView(bool:Bool,view:UIView,tabBarController:UITabBarController)
                SVProgressHUD.dismiss()
            }
        })
    }
    
    static func stopListnerIntenet(){
        reachabilityManager?.stopListening()
//        UIApplication.shared.endIgnoringInteractionEvents()
    }
}
