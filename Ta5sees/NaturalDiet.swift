//
//  newRegiterStep6.swift
//  Ta5sees
//
//  Created by Admin on 8/20/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
//import fluid_slider
//import TNSlider
//import RNCryptor
import Firebase
import FirebaseAnalytics

 
class NaturalDiet: UIViewController,UIViewControllerTransitioningDelegate {
    
    

    @IBOutlet weak var slectMaintain: viewColorsWithRaduis!
    @IBOutlet weak var selectGain: viewColorsWithRaduis!
    @IBOutlet weak var selectLoss: viewColorsWithRaduis!
    var scenarioStep = "0" // 1-> updateDietType else registration

    override func viewDidLoad() {
        super.viewDidLoad()
       
        setUpView()

//        if scenarioStep == "1" {
//            switch getDataFromSheardPreferanceString(key: "dietType") {
//            case "1":
//                selectGain.isUserInteractionEnabled = false
//                gainDiet.isUserInteractionEnabled = false
//                gainDiet.alpha = 0.5
//                print("123")
//                break;
//            case "2":
//                selectLoss.isUserInteractionEnabled = false
//                lossDiet.isUserInteractionEnabled = false
//                selectLoss.alpha = 0.5
//                print("12345")
//                break;
//            case "3":
//                slectMaintain.isUserInteractionEnabled = false
//                maintainDiet.isUserInteractionEnabled = false
//                slectMaintain.alpha = 0.5
//                print("1236")
//                break;
//            default:
//                print("diet type is null")
//            }
//           
//        }

    }
    
    
    @IBAction func btnBack(_ sender: Any) {

            dismiss(animated: true, completion: nil)
//
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
  
    
    func delegetGenderType(){
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "GenderType") as! GenderType
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
    }
    
    @IBOutlet weak var gainDiet: viewColorsWithRaduis!
    @IBOutlet weak var lossDiet: viewColorsWithRaduis!
    @IBOutlet weak var maintainDiet: viewColorsWithRaduis!
    @IBOutlet weak var txtGain: UILabel!
    @IBOutlet weak var txtLoss: UILabel!

    @IBOutlet weak var txtMaintain: UILabel!
    
    func setUpView(){
        
        maintainDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        maintainDiet.tag = 3
        txtMaintain.text = "تثبيت الوزن"
        maintainDiet.isUserInteractionEnabled = true
        
        lossDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lossDiet.tag = 2
        txtLoss.text = "تخسيس الوزن"
        lossDiet.isUserInteractionEnabled = true
        
        gainDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        gainDiet.tag = 1
        txtGain.text = "زيادة الوزن"
        gainDiet.isUserInteractionEnabled = true
        
    }
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        if getDataFromSheardPreferanceString(key: "loginType") == "1" {
            print("createEvent")
            createEvent(key: "53", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "2" {
            createEvent(key: "37", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "3" {
            createEvent(key: "42", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "4" {
            createEvent(key: "47", date: getDateTime())
        }
        
        let tappedImageView = gestureRecognizer.view!
        print(tappedImageView.tag)
        if scenarioStep == "1" {
            createEvent(key: "14",date: getDateTime())
            setDataInSheardPreferance(value:String(tappedImageView.tag),key:"copyDietType")
        }else{
            setDataInSheardPreferance(value:String(tappedImageView.tag),key:"dietType")
        }
        let detailView = storyboard!.instantiateViewController(withIdentifier: "GeneralInformationTest") as! TextInputViewController
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        if scenarioStep == "1" {
            detailView.scenarioStep = "1"
        }else{
            detailView.scenarioStep = "0"
        }
        present(detailView, animated: true, completion: nil)
    }
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "GeneralInformationTest" {
              if let dis=segue.destination as?  TextInputViewController{
                if  let data=sender as? String {
                      dis.scenarioStep = data
                  }
              }
          }
    }
}
extension NaturalDiet:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
