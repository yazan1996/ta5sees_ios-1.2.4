//
//  LableRaduis.swift
//  Ta5sees
//
//  Created by Admin on 10/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LableRaduis: UILabel {


    let textField: UILabel = {
        let v = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func prepareForInterfaceBuilder() {
        commonInit()
    }

    func commonInit() -> Void {

//        layer.borderColor =  UIColor(red: 141/255, green: 222/255, blue: 187/255, alpha: 1.0).cgColor
//        layer.borderWidth = 1
//        layer.masksToBounds = true
        layer.shadowColor = UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 1.0).cgColor
        layer.shadowOpacity = 10
        layer.cornerRadius = bounds.size.height * 0.5
        addSubview(textField)

    
        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: topAnchor, constant: 0.0),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0.0),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20.0),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20.0),
            
            ])

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.size.height * 0.5
    }

}



/*
 // The textfield outlets
    //    @IBOutlet weak var item1: UITextField!
    //    @IBOutlet weak var item2: UITextField!
    //    @IBOutlet weak var item3: UITextField!
    //    @IBOutlet weak var item4: UITextField!
    //    @IBOutlet weak var item5: UITextField!
    //
    //    // the button action function
    //    @IBAction func uploadData(_ sender: Any)
    //    {
    //        let url = NSURL(string: "http://localhost:8888/sdb/receive.php") // locahost MAMP - change to point to your database server
    //
    //        var request = URLRequest(url: url! as URL)
    //        request.httpMethod = "POST"
    //
    //        var dataString = "secretWord=44fdcv8jf3" // starting POST string with a secretWord
    //
    //        // the POST string has entries separated by &
    //
    //        dataString = dataString + "&item1=\(item1.text!)" // add items as name and value
    //        dataString = dataString + "&item2=\(item2.text!)"
    //        dataString = dataString + "&item3=\(item3.text!)"
    //        dataString = dataString + "&item4=\(item4.text!)"
    //        dataString = dataString + "&item5=\(item5.text!)"
    //
    //        // convert the post string to utf8 format
    //
    //        let dataD = dataString.data(using: .utf8) // convert to utf8 string
    //
    //        do
    //        {
    //
    //            // the upload task, uploadJob, is defined here
    //
    //            let uploadJob = URLSession.shared.uploadTask(with: request, from: dataD)
    //            {
    //                data, response, error in
    //
    //                if error != nil {
    //
    //                    // display an alert if there is an error inside the DispatchQueue.main.async
    //
    //                    DispatchQueue.main.async
    //                        {
    //                            let alert = UIAlertController(title: "Upload Didn't Work?", message: "Looks like the connection to the server didn't work.  Do you have Internet access?", preferredStyle: .alert)
    //                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    //                            self.present(alert, animated: true, completion: nil)
    //                    }
    //                }
    //                else
    //                {
    //                    if let unwrappedData = data {
    //
    //                        let returnedData = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue) // Response from web server hosting the database
    //
    //                        if returnedData == "1" // insert into database worked
    //                        {
    //
    //                            // display an alert if no error and database insert worked (return = 1) inside the DispatchQueue.main.async
    //
    //                            DispatchQueue.main.async
    //                                {
    //                                    let alert = UIAlertController(title: "Upload OK?", message: "Looks like the upload and insert into the database worked.", preferredStyle: .alert)
    //                                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    //                                    self.present(alert, animated: true, completion: nil)
    //                            }
    //                        }
    //                        else
    //                        {
    //                            // display an alert if an error and database insert didn't worked (return != 1) inside the DispatchQueue.main.async
    //
    //                            DispatchQueue.main.async
    //                                {
    //
    //                                    let alert = UIAlertController(title: "Upload Didn't Work", message: "Looks like the insert into the database did not worked.", preferredStyle: .alert)
    //                                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    //                                    self.present(alert, animated: true, completion: nil)
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            uploadJob.resume()
    //        }
    //    }
 */
