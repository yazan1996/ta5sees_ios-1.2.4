//
//  MealsDistrbutions.swift
//  Ta5sees
//
//  Created by Admin on 3/3/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SimpleCheckbox

class MealsDistrbutions: UIViewController {

    @IBOutlet weak var tt: UITextField!
    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var timeBreakfastOut: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        datePicker.datePickerMode = .time
        createUIToolBar()
        tt?.inputAccessoryView = pickerToolbar
        
        tt?.inputView = datePicker

        setupRadiokBox(flag: meals[0].checkAsMain, view: seletedMainBreafast)
        setupRadiokBox(flag: meals[2].checkAsMain, view: seletedMainLuanch)
        setupRadiokBox(flag: meals[4].checkAsMain, view: seletedMainDinner)
        
        setupCheckBox(flag: meals[0].selected, view: seletedBreakfast)
        setupCheckBox(flag: meals[1].selected, view: seletedSnak1)
        setupCheckBox(flag: meals[2].selected, view: seletedLuanch)
        setupCheckBox(flag: meals[3].selected, view: seletedSnak2)
        setupCheckBox(flag: meals[4].selected, view: seletedDinner)


    }
    
    var disposeBag = DisposeBag()
    let meals = ModelMEalDistrbution.list
    var datePicker: UIDatePicker = UIDatePicker()
    var pickerToolbar: UIToolbar?

    @IBOutlet weak var seletedBreakfast: UIView!
    @IBOutlet weak var seletedSnak1: UIView!
    @IBOutlet weak var seletedDinner: UIView!
    @IBOutlet weak var seletedSnak2: UIView!
    @IBOutlet weak var seletedLuanch: UIView!
    @IBOutlet weak var seletedMainBreafast: UIView!
    @IBOutlet weak var seletedMainLuanch: UIView!
    @IBOutlet weak var seletedMainDinner: UIView!
    
    @IBAction func timeBerafast(_ sender: Any) {
//        createAndDisplayPicker()
    }
    @IBAction func timeSnak1(_ sender: Any) {
    }
    @IBAction func timeLuanch(_ sender: Any) {
    }
    @IBAction func timeSnak2(_ sender: Any) {
    }
    @IBAction func timeDinner(_ sender: Any) {
    }
    
    
  
    
    func setupCheckBox(flag:Bool,view:UIView){
        let squareBox = Checkbox(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        squareBox.tintColor = .black
        squareBox.borderStyle = .square
        squareBox.checkmarkStyle = .square
        squareBox.uncheckedBorderColor = .lightGray
        squareBox.borderLineWidth = 2
        squareBox.checkedBorderColor = colorBasicApp
        squareBox.checkmarkSize = 0.8
        squareBox.backgroundColor = colorGray
        squareBox.checkboxFillColor = .white
        squareBox.checkmarkColor = colorBasicApp
        squareBox.isChecked = flag
        squareBox.valueChanged = { (value) in
              print("squarebox value change: \(value)")
          }
          view.addSubview(squareBox)
      }
      
    func setupRadiokBox(flag:Bool,view:UIView){
        let circleBox = Checkbox(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        circleBox.borderStyle = .circle
        circleBox.checkmarkStyle = .circle
        circleBox.borderLineWidth = 2
        circleBox.uncheckedBorderColor = .lightGray
        circleBox.checkedBorderColor = colorBasicApp
        circleBox.checkmarkSize = 0.8
        circleBox.backgroundColor = colorGray
        circleBox.checkboxFillColor = .white
        circleBox.checkmarkColor = colorBasicApp
        circleBox.isChecked = flag
        circleBox.valueChanged = { (value) in
            print("squarebox value change: \(value)")
        }
        // circleBox.addTarget(self, action: #selector(circleBoxValueChanged(sender:)), for: .valueChanged)
        view.addSubview(circleBox)
    }
    
    
    func createUIToolBar() {
          
          pickerToolbar = UIToolbar()
          pickerToolbar?.autoresizingMask = .flexibleHeight
          
          //customize the toolbar
          pickerToolbar?.barStyle = .default
          pickerToolbar?.barTintColor = UIColor.black
          pickerToolbar?.backgroundColor = UIColor.white
          pickerToolbar?.isTranslucent = false
          
          //add buttons
          let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action:
              #selector(cancelBtnClicked(_:)))
          cancelButton.tintColor = UIColor.white
          let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
          let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action:
              #selector(MealsDistrbutions.doneBtnClicked(_:)))
          doneButton.tintColor = UIColor.white
          
          //add the items to the toolbar
          pickerToolbar?.items = [cancelButton, flexSpace, doneButton]
          
      }
      
      @objc func cancelBtnClicked(_ button: UIBarButtonItem?) {
          tt?.resignFirstResponder()
      }
      
      @objc func doneBtnClicked(_ button: UIBarButtonItem?) {
          tt?.resignFirstResponder()
          let formatter = DateFormatter()
          formatter.timeStyle = .short
        tt?.text = formatter.string(from: datePicker.date)
      }
}


