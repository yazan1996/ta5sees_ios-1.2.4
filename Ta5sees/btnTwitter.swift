//
//  btnTwitter.swift
//  Ta5sees
//
//  Created by Admin on 1/16/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import UIKit

@IBDesignable
public class btnTwitter: UIButton {
    
    let buttonNormalBackgroundColor = UIColor(red: 29/255.0, green: 161/255.0, blue: 242/255.0, alpha: 1.0)
    let buttonHighlightBackgroundColor = UIColor(red: 156/255.0, green: 171/255.0, blue: 179/255.0, alpha: 1.0)
    
    @IBInspectable var fontColor: UIColor = UIColor.clear {
         didSet {
             commonSetup()
         }
     }
    @IBInspectable var firstColor: UIColor = UIColor.clear {
            didSet {
                commonSetup()
            }
        }
    
    @IBInspectable var imageName: String = ""{
               didSet {
                   commonSetup()
               }
           }
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
   
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Set button corner radius
        layer.cornerRadius = bounds.size.height * 0.5
        clipsToBounds = true
        
        // Set Facebook icon
       
        let bundle = Bundle(for: btnTwitter.self)
        let icon = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        setImage(icon, for: .normal)
        imageView?.contentMode = .scaleAspectFit
        imageEdgeInsets = UIEdgeInsets(top: 0, left: 90, bottom: 0, right: 0)
    }
    
    override public var isHighlighted: Bool {
        willSet {
            backgroundColor = newValue ? buttonHighlightBackgroundColor : firstColor
        }
    }
    
    // MARK: - Private
    private func commonSetup() {
        backgroundColor = firstColor
        setTitleColor(fontColor, for: .normal)
        setTitleColor(fontColor, for: .highlighted)
        if #available(iOS 8.2, *) {
            titleLabel?.font =  UIFont(name: "GE Dinar One", size: 19.0)!
        } else {
            // Fallback on earlier versions
            titleLabel?.font =  UIFont(name: "GE Dinar One", size: 19.0)!
        }
    }
    
}
