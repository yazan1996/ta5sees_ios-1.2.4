//
//  regesterStep10.swift
//  Ta5sees
//
//  Created by Admin on 8/28/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit
import iOSDropDown
import TextFieldEffects
class FormateDiabetsDeit: UIViewController,UIViewControllerTransitioningDelegate {
    
    
    @IBOutlet weak var txtinsolin2: HoshiTextField!
    @IBOutlet weak var txtinsolen1: HoshiTextField!
    @IBOutlet weak var listpregnant: DropDown!
    @IBOutlet weak var listtypemidication: DropDown!
    var idSelectPregnant = "null"
 
    @IBOutlet weak var listTypeMidication2: DropDown!
    @IBOutlet weak var FBG: HoshiTextField!
    @IBOutlet weak var isHiddinSeekBar: UIStackView!
    @IBOutlet weak var typeDiabets: DropDown!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.HideKeyboard()
    }
    
    @IBAction func btnShowAddInsolin(_ sender: Any) {
        let firstView8 = isHiddinSeekBar.arrangedSubviews[7]
        firstView8.isHidden = false
        let firstView9 = isHiddinSeekBar.arrangedSubviews[8]
        firstView9.isHidden = false
    }
    
    func HideSeekbar(flag:Bool,flag2:Bool,flag3:Bool,flag4:Bool,flag5:Bool,flag6:Bool){
        let firstView3 = isHiddinSeekBar.arrangedSubviews[1]
        firstView3.isHidden = flag
        let firstView1 = isHiddinSeekBar.arrangedSubviews[2]
        firstView1.isHidden = flag2
        let firstView4 = isHiddinSeekBar.arrangedSubviews[3]
        firstView4.isHidden = flag3
        let firstView41 = isHiddinSeekBar.arrangedSubviews[4]
        firstView41.isHidden = flag4
        let firstView11 = isHiddinSeekBar.arrangedSubviews[5]
        firstView11.isHidden = flag5
        let firstView114 = isHiddinSeekBar.arrangedSubviews[6]
        firstView114.isHidden = flag6
        
        self.HideKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    var refIDiet = getDataFromSheardPreferanceString(key: "dietType")
    func setBackground(){
        imgBackground(imgname: "curve")
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setBackground()
        if getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0 {
            if getDataFromSheardPreferanceString(key: "gender") == "1" {
                HideSeekbar(flag:false,flag2:false, flag3: false,flag4:true,flag5:false, flag6: false)
                
                typeDiabets.placeholder = "اختر نوع السكري"
                
                
                listtypemidication.placeholder = "اختر نوع الانسولين"
                
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
                
            }else{
                HideSeekbar(flag:false,flag2:false, flag3: false,flag4:false,flag5:false, flag6: false)
                
                typeDiabets.placeholder = "اختر نوع السكري"
                
                
                listtypemidication.placeholder = "اختر نوع الانسولين"
                listpregnant.placeholder = "اختر فترة الحمل"
                listpregnant.optionArray = tblPregancyMonths.getData()
                listpregnant.optionIds =  tblPregancyMonths.getDataID()
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
            }
        }else{ //child
            if getDataFromSheardPreferanceString(key: "gender") == "1" {
                HideSeekbar(flag:true,flag2:false, flag3: false,flag4:true,flag5:false, flag6: false)
                
                
                
                listtypemidication.placeholder = "اختر نوع الانسولين"
                
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
                
            }else{
                HideSeekbar(flag:true,flag2:false, flag3: false,flag4:false,flag5:false, flag6: false)
                
                
                listtypemidication.placeholder = "اختر نوع الانسولين"
                listpregnant.placeholder = "اختر فترة الحمل"
                listpregnant.optionArray = tblPregancyMonths.getData()
                listpregnant.optionIds =  tblPregancyMonths.getDataID()
                FBG.placeholder = "ادخل نسبة السكر التراكمي بالدم"
                txtinsolen1.placeholder = "ادخل نسبة الانسولين"
            }
        }
        
        
        
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            typeDiabets.optionArray = ["سكري نوع اول","سكري نوع ثاني"]
            typeDiabets.optionIds = [1,2]
        }else{
            typeDiabets.optionArray = ["سكري نوع اول","سكري نوع ثاني","سكري حوامل"]
            typeDiabets.optionIds = [1,2,3]
        }
        
        
        if getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0 {
            
            self.listtypemidication.optionArray = tblInsulinMedicines.getData()
            self.listtypemidication.optionIds = tblInsulinMedicines.getDataID()
            self.listTypeMidication2.optionArray = tblInsulinMedicines.getData()
            self.listTypeMidication2.optionIds = tblInsulinMedicines.getDataID()
        }
        
        typeDiabets.isSearchEnable = false
        typeDiabets.didSelect{(selectedText , index ,id) in
            
            if selectedText == "سكري نوع اول" {
                setDataInSheardPreferance(value: "20", key: "dietType")
                setDataInSheardPreferance(value: "1", key: "typeDiabites")
                
                self.listtypemidication.optionArray = tblInsulinMedicines.getData()//1
                self.listtypemidication.optionIds = tblInsulinMedicines.getDataID()
                self.listTypeMidication2.optionArray = tblInsulinMedicines.getData()//1
                self.listTypeMidication2.optionIds = tblInsulinMedicines.getDataID()
//                let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
//                firstView11.isHidden = true
            }else if  selectedText == "سكري نوع ثاني"{
                setDataInSheardPreferance(value: "21", key: "dietType")
                setDataInSheardPreferance(value: "2", key: "typeDiabites")
                self.listtypemidication.optionArray = tblInsulinMedicines.getData()//2
                self.listtypemidication.optionIds = tblInsulinMedicines.getDataID()
                self.listTypeMidication2.optionArray = tblInsulinMedicines.getData()//2
                self.listTypeMidication2.optionIds = tblInsulinMedicines.getDataID()
//                let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
//                firstView11.isHidden = true
            }else if  selectedText == "سكري حوامل"{
                setDataInSheardPreferance(value: "22", key: "dietType")
                setDataInSheardPreferance(value: "3", key: "typeDiabites")
                self.listtypemidication.optionArray = tblInsulinMedicines.getData()//2
                self.listtypemidication.optionIds = tblInsulinMedicines.getDataID()
                self.listTypeMidication2.optionArray = tblInsulinMedicines.getData()//2
                self.listTypeMidication2.optionIds = tblInsulinMedicines.getDataID()
//                let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
//                firstView11.isHidden = false
            }
        }
        
        
        listtypemidication.isSearchEnable = false
        listtypemidication.didSelect{(selectedText , index ,id) in
            setDataInSheardPreferance(value: String(id), key: "typeMedication")
//            if  selectedText == "long"{
//                setDataInSheardPreferance(value: "21", key: "dietType")
//                setDataInSheardPreferance(value: "2", key: "typeMedication")
//
//            }else  if  selectedText == "short"{
//                setDataInSheardPreferance(value: "21", key: "dietType")
//                setDataInSheardPreferance(value: "1", key: "typeMedication")
//
//            }else{
//                setDataInSheardPreferance(value: "21", key: "dietType")
//                setDataInSheardPreferance(value: "2", key: "typeMedication")
//            }
        }
        
        
            listTypeMidication2.isSearchEnable = false
                listTypeMidication2.didSelect{(selectedText , index ,id) in
                    setDataInSheardPreferance(value: String(id), key: "typeMedication2")
                }
        
        listpregnant.optionArray = tblPregancyMonths.getData()
        listpregnant.optionIds =  tblPregancyMonths.getDataID()
        
        listpregnant.isSearchEnable = false
        listpregnant.didSelect{(selectedText , index ,id) in
            
            self.idSelectPregnant = String(id)
            
        }
        
    }
     func imgBackground (imgname:String) {
//           let myLayer = CALayer()
//           let myImage = UIImage(named: imgname)?.cgImage
//           myLayer.frame = CGRect(x: -50, y: -567,  width: 1535, height: 881)
//           myLayer.contents = myImage
//           view.layer.addSublayer(myLayer)
//           subview.layer.insertSublayer(myLayer, at: 0)
//
//           let Diabetes = UIImageView(frame: CGRect(x: 180, y: 40, width: 160, height: 160))
//           Diabetes.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
//           self.subview.addSubview(Diabetes)
//
//           let spicialDiet = UILabel(frame: CGRect(x: 8, y: 13, width: 224, height: 96))
//           spicialDiet.textAlignment = .center
//           spicialDiet.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
//           spicialDiet.textColor = UIColor(red: 255, green: 255, blue: 255)
//           spicialDiet.font =  UIFont(name: "GE Dinar One", size: 25.0)!
//           self.subview.addSubview(spicialDiet)
//
//           let image = UIImage(named: "imgback") as UIImage?
//           let imageBack = UIButton(frame: CGRect(x: 320, y: 20, width: 50, height: 50))
//           imageBack.setImage(image, for: .normal)
//           imageBack.tag = 4
//           imageBack.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//           self.subview.addSubview(imageBack)
        let Diabetes = UIImageView(frame: CGRect(x: 224, y: 68, width: 100, height: 100))
        Diabetes.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
         self.subview.addSubview(Diabetes)
        
        let spicialDiet = UILabel(frame: CGRect(x: 8, y: 13, width: 224, height: 96))
        spicialDiet.textAlignment = .center
        spicialDiet.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        spicialDiet.textColor = UIColor(red: 255, green: 255, blue: 255)
        spicialDiet.font =  UIFont(name: "GE Dinar One", size: 25.0)!
         self.subview.addSubview(spicialDiet)
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    @IBOutlet weak var subview: UIView!
    
    @IBAction func btnNext(_ sender: Any) {
        
        if isHiddinSeekBar.arrangedSubviews[1].isHidden == false  && typeDiabets.text!.isEmpty {
            showAlert(str:"يجب اختيار نوع السكري")
        }else if isHiddinSeekBar.arrangedSubviews[2].isHidden == false  && listtypemidication.text!.isEmpty {
            showAlert(str:"يجب اختيار نوع الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[4].isHidden == false  && listpregnant.text!.isEmpty {
            showAlert(str:"يجب اختيار فترة الحمل")
        }else if isHiddinSeekBar.arrangedSubviews[3].isHidden == false  && Formatter(str:txtinsolen1).text!.isEmpty {
            showAlert(str:"يجب ادخال كمية الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[5].isHidden == false  && Formatter(str:FBG).text!.isEmpty {
            showAlert(str:"يجب ادخال كمية الانسولين")
        }else if isHiddinSeekBar.arrangedSubviews[8].isHidden == false  && listTypeMidication2.text!.isEmpty {
            showAlert(str:"يجب اختيار نوع الانسولين / دواء")
        }else if isHiddinSeekBar.arrangedSubviews[7].isHidden == false  && Formatter(str:txtinsolin2).text!.isEmpty {
            showAlert(str:"يجب ادخال نسبة الانسولين")
        }else{
            if txtinsolin2.text!.isEmpty {
                txtinsolin2.text! = "0"
                
            }
            setDataInSheardPreferance(value: FBG.text!, key: "FBG")
            setDataInSheardPreferance(value: txtinsolen1.text!, key: "insolin1")
            setDataInSheardPreferance(value: txtinsolin2.text!, key: "insolin2")
            setDataInSheardPreferance(value: idSelectPregnant , key: "PregancyMonth")

            let detailView = storyboard!.instantiateViewController(withIdentifier: "newRegesterStep9") as! FormatSpecialDiet
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            detailView.transitioningDelegate = self
            present(detailView, animated: true, completion: nil)
            self.performSegue(withIdentifier: "step12", sender: nil)
        }
        //
    }
    func Formatter(str:HoshiTextField)->HoshiTextField{
                  let NumberStr: String = str.text!
                        let Formatter = NumberFormatter()
                       Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
                        if let final = Formatter.number(from: NumberStr) {
                          str.text = final.stringValue
                        }
                  return str
              }
          
    func showAlert(str:String){
        let alert = UIAlertController(title: "خطأ", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        subview.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        subview.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.subview.frame.origin.y == 0 {
                self.subview.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.subview.frame.origin.y != 0 {
            self.subview.frame.origin.y = 0
        }
    }
}



/*
 //
 //  regesterStep10.swift
 //  Ta5sees
 //
 //  Created by Admin on 8/28/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import iOSDropDown
 import TextFieldEffects
 class regesterStep10: UIViewController,UIViewControllerTransitioningDelegate {
 
 
 @IBOutlet weak var txtinsolin2: HoshiTextField!
 @IBOutlet weak var txtinsolen1: HoshiTextField!
 @IBOutlet weak var listpregnant: DropDown!
 @IBOutlet weak var listtypemidication: DropDown!
 
 @IBOutlet weak var isHiddinSeekBar: UIStackView!
 @IBOutlet weak var txtparcentig: HoshiTextField!
 @IBOutlet weak var typeDiabets: DropDown!
 override func viewDidLoad() {
 super.viewDidLoad()
 self.HideKeyboard()
 
 
 
 
 
 
 }
 
 @IBAction func btnShowAddInsolin(_ sender: Any) {
 let firstView11 = isHiddinSeekBar.arrangedSubviews[7]
 firstView11.isHidden = false
 }
 func HideSeekbar(flag:Bool,flag2:Bool,flag4:Bool,flag5:Bool){
 let firstView3 = isHiddinSeekBar.arrangedSubviews[1]
 firstView3.isHidden = flag
 let firstView1 = isHiddinSeekBar.arrangedSubviews[2]
 firstView1.isHidden = flag2
 let firstView4 = isHiddinSeekBar.arrangedSubviews[4]
 firstView4.isHidden = flag4
 let firstView11 = isHiddinSeekBar.arrangedSubviews[5]
 firstView11.isHidden = flag5
 
 self.HideKeyboard()
 NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
 NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
 }
 
 
 var refIDiet = getDataFromSheardPreferanceString(key: "dietType")
 func setBackground(){
 imgBackground(imgname: "curve")
 }
 
 
 
 
 override func viewWillAppear(_ animated: Bool) {
 super.viewWillAppear(true)
 setBackground()
 if getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0 {
 if getDataFromSheardPreferanceString(key: "gender") == "1" {
 HideSeekbar(flag:false,flag2:false,flag4:false,flag5:false)
 
 typeDiabets.placeholder = "اختر نوع السكري"
 
 
 listtypemidication.placeholder = "اختر نوع الدواء/الانسولين"
 
 txtparcentig.placeholder = "ادخل نسبة السكر التراكمي بالدم"
 txtinsolen1.placeholder = "ادخل نسبة الانسولين"
 
 }else{
 HideSeekbar(flag:false,flag2:false,flag4:false,flag5:false)
 
 typeDiabets.placeholder = "اختر نوع السكري"
 
 
 listtypemidication.placeholder = "اختر نوع الدواء/الانسولين"
 listpregnant.placeholder = tblPlanMasterNeededMidicalTests.getDisc(id:Int(refIDiet)!)
 listpregnant.optionArray = tblPregancyMonths.getData()
 listpregnant.optionIds =  tblPregancyMonths.getDataID()
 
 txtparcentig.placeholder = "ادخل نسبة السكر التراكمي بالدم"
 txtinsolen1.placeholder = "ادخل نسبة الانسولين"
 }
 }else{
 if getDataFromSheardPreferanceString(key: "gender") == "1" {
 HideSeekbar(flag:true,flag2:false,flag4:false,flag5:false)
 
 
 
 listtypemidication.placeholder = "اختر نوع الدواء/الانسولين"
 
 txtparcentig.placeholder = "ادخل نسبة السكر التراكمي بالدم"
 txtinsolen1.placeholder = "ادخل نسبة الانسولين"
 
 }else{
 HideSeekbar(flag:true,flag2:false,flag4:false,flag5:false)
 
 
 listtypemidication.placeholder = "اختر نوع الدواء/الانسولين"
 listpregnant.placeholder = tblPlanMasterNeededMidicalTests.getDisc(id:Int(refIDiet)!)
 listpregnant.optionArray = tblPregancyMonths.getData()
 listpregnant.optionIds =  tblPregancyMonths.getDataID()
 txtparcentig.placeholder = "ادخل نسبة السكر التراكمي بالدم"
 txtinsolen1.placeholder = "ادخل نسبة الانسولين"
 }
 }
 
 
 
 if getDataFromSheardPreferanceString(key: "gender") == "1" {
 typeDiabets.optionArray = ["سكري نوع اول","سكري نوع ثاني"]
 typeDiabets.optionIds = [0,1]
 }else{
 typeDiabets.optionArray = ["سكري نوع اول","سكري نوع ثاني","سكري حوامل"]
 typeDiabets.optionIds = [0,1,2]
 }
 
 
 if getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0 {
 
 self.listtypemidication.optionArray = ["short","long"]
 self.listtypemidication.optionIds = [0,1]
 }
 typeDiabets.isSearchEnable = false
 typeDiabets.didSelect{(selectedText , index ,id) in
 
 if selectedText == "سكري نوع اول" {
 setDataInSheardPreferance(value: "20", key: "dietType")
 setDataInSheardPreferance(value: "1", key: "typeDiabites")
 
 self.listtypemidication.optionArray = ["short","long"]
 self.listtypemidication.optionIds = [0,1]
 let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
 firstView11.isHidden = true
 }else if  selectedText == "سكري نوع ثاني"{
 setDataInSheardPreferance(value: "21", key: "dietType")
 setDataInSheardPreferance(value: "2", key: "typeDiabites")
 self.listtypemidication.optionArray = ["حبوب ١","حبوب ٢"]
 self.listtypemidication.optionIds = [0,1]
 let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
 firstView11.isHidden = true
 }else if  selectedText == "سكري حوامل"{
 setDataInSheardPreferance(value: "22", key: "dietType")
 setDataInSheardPreferance(value: "3", key: "typeDiabites")
 self.listtypemidication.optionArray = ["حبوب ١","حبوب ٢"]
 self.listtypemidication.optionIds = [0,1]
 let firstView11 = self.isHiddinSeekBar.arrangedSubviews[3]
 firstView11.isHidden = false
 print("****************************************************************************************************")
 }
 }
 
 
 listtypemidication.isSearchEnable = false
 listtypemidication.didSelect{(selectedText , index ,id) in
 setDataInSheardPreferance(value: self.listtypemidication.text!, key: "typeMedication")
 if  selectedText == "long"{
 setDataInSheardPreferance(value: "21", key: "dietType")
 setDataInSheardPreferance(value: "2", key: "typeMedication")
 
 }else  if  selectedText == "short"{
 setDataInSheardPreferance(value: "21", key: "dietType")
 setDataInSheardPreferance(value: "1", key: "typeMedication")
 
 }else{
 setDataInSheardPreferance(value: "21", key: "dietType")
 setDataInSheardPreferance(value: "2", key: "typeMedication")
 }
 }
 
 
 self.listpregnant.optionArray = tblPregancyMonths.getData()
 self.listpregnant.optionIds = tblPregancyMonths.getDataID()
 listpregnant.isSearchEnable = false
 listpregnant.didSelect{(selectedText , index ,id) in
 
 setDataInSheardPreferance(value: String(id) , key: "statePregnatDiabets")
 
 
 }
 
 }
 func imgBackground (imgname:String) {
 let myLayer = CALayer()
 let myImage = UIImage(named: imgname)?.cgImage
 myLayer.frame = CGRect(x: -17, y: -367,  width: 1535, height: 681)
 myLayer.contents = myImage
 subview.layer.addSublayer(myLayer)
 subview.layer.insertSublayer(myLayer, at: 0)
 let Diabetes = UIImageView(frame: CGRect(x: 180, y: 40, width: 160, height: 160))
 Diabetes.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
 self.subview.addSubview(Diabetes)
 
 let spicialDiet = UILabel(frame: CGRect(x: 8, y: 13, width: 224, height: 96))
 spicialDiet.textAlignment = .center
 spicialDiet.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
 spicialDiet.textColor = UIColor(red: 255, green: 255, blue: 255)
 spicialDiet.font =  UIFont(name: "GE Dinar One", size: 25.0)!
 self.subview.addSubview(spicialDiet)
 
 
 
 
 let image = UIImage(named: "imgback") as UIImage?
 let imageBack = UIButton(frame: CGRect(x: 320, y: 24, width: 32, height: 32))
 imageBack.setImage(image, for: .normal)
 imageBack.tag = 4
 imageBack.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
 self.subview.addSubview(imageBack)
 
 }
 
 
 @objc func buttonAction(sender: UIButton!) {
 if sender.tag == 4 {
 
 dismiss(animated: true, completion: nil)
 }
 
 }
 @IBOutlet weak var subview: UIView!
 
 @IBAction func btnNext(_ sender: Any) {
 setDataInSheardPreferance(value: txtparcentig.text!, key: "midical")
 setDataInSheardPreferance(value: txtinsolen1.text!, key: "insolin1")
 setDataInSheardPreferance(value: txtinsolin2.text!, key: "insolin2")
 
 let detailView = storyboard!.instantiateViewController(withIdentifier: "newRegesterStep9") as! newRegesterStep9
 detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
 detailView.transitioningDelegate = self
 present(detailView, animated: true, completion: nil)
 self.performSegue(withIdentifier: "step12", sender: nil)
 
 }
 
 func HideKeyboard(){
 let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
 subview.addGestureRecognizer(tap)
 }
 @objc func dismissKeyboard() {
 subview.endEditing(true)
 }
 @objc func keyboardWillShow(notification: NSNotification) {
 if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
 if self.subview.frame.origin.y == 0 {
 self.subview.frame.origin.y -= keyboardSize.height
 }
 }
 }
 
 @objc func keyboardWillHide(notification: NSNotification) {
 if self.subview.frame.origin.y != 0 {
 self.subview.frame.origin.y = 0
 }
 }
 }
 
 */
