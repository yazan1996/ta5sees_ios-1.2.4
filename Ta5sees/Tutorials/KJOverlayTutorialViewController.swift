 /**
  * Copyright © SaigonMD, Inc - All Rights Reserved
  * Licensed under the MIT license.
  * Written by Tran Quan <tranquan221b@gmail.com>, Feb 2018
  */
 
 
 import UIKit
 import pop
 import Firebase
 
 class KJOverlayTutorialViewController: UIViewController {
    
    var overlayColor: UIColor = UIColor.black.withAlphaComponent(0.6)
    var showWajbehMealPlannerVC:showWajbehMealPlanner!
    var addEditWajbeh:SearchItems!
    var DisplayTamreen:DisplayTamreenController!
    var DisplayCateWajbeh:DisplayCateItemsController!
    var viewMainHomeMealPlanner = false
    var tutAddWajbehLastStep = false
    var isMainTut = false
    var showAlertUnitWajbeh = false
    var addEditWajbehLastStep = false
    var showAlertUnit = false
    var addTutTamreen = false
    var tutAddTamreenMainHome = false
    var presentedUIAlertController = false
    var presentedUIAlertControllerUnit = false
    var isFlagWajbeh = false
    var isFlagTamreen = false
    var addTutTamreen1 = false
    var id_category:String? = "1"
    var addEditWajbehFirstStep = false
    var viewMainHome:ViewController!
    var tutAddWajbeh = false
    var viewMainHomeNutrationValues = false
    var viewMainHomeflage = false
    var isCircle = false
    var TamreenTypeListCV:TamreenTypeListController!
    var tutorials: [KJTutorial] = []
    var currentTutorialIndex = 0
    var showAlertUnitWajbehCounter = 0
    let maskLayer = CAShapeLayer()
    var countWater = -1
    var countTamreen = 0
    var countWajbeh = -1
    var countNutration = -1
    var bgView=UIView()
    var views:UIViewController?
    lazy var hintLabel: UILabel = {
        let label = UILabel()
        label.text = "(Tap to continue)"
        return label
    }()
    
    init(views:UIViewController){
        views.view.superview?.isUserInteractionEnabled = false
        super.init(nibName: nil, bundle: nil)
    }
    
    deinit {
        views?.view.superview?.isUserInteractionEnabled = true
        print("deinit tutorial")
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var timer:Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        if viewMainHomeflage || isCircle || tutAddWajbeh || tutAddWajbehLastStep || tutAddTamreenMainHome {
            viewMainHome.view.superview?.isUserInteractionEnabled = false
        }else if addEditWajbehFirstStep || addEditWajbehLastStep{
            addEditWajbeh.view.superview?.isUserInteractionEnabled = false
        }else if showAlertUnitWajbeh && isMainTut{
            DisplayCateWajbeh.view.superview?.isUserInteractionEnabled = false
        }else if showAlertUnit || isFlagTamreen {
            DisplayTamreen.view.superview?.isUserInteractionEnabled = false
        }else if addTutTamreen || addTutTamreen1{
            TamreenTypeListCV.view.superview?.isUserInteractionEnabled = false
        }
        if addEditWajbehFirstStep || addTutTamreen1 {
            timer = Timer.scheduledTimer(timeInterval: 1.8, target:self, selector: #selector(handleTapGesture), userInfo: nil,repeats: true)
        }else if viewMainHomeflage {
            timer = Timer.scheduledTimer(timeInterval: 2.5, target:self, selector: #selector(handleTapGesture), userInfo: nil,repeats: true)
        }else if viewMainHomeNutrationValues{
            timer = Timer.scheduledTimer(timeInterval: 2, target:self, selector: #selector(handleTapGesture), userInfo: nil,repeats: true)
        }else{
            timer = Timer.scheduledTimer(timeInterval: 2.5, target:self, selector: #selector(handleTapGesture), userInfo: nil,repeats: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
        //    let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        //    self.view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Methods
    
    func showInViewController(_ viewController: UIViewController) {
        
        let window = viewController.view.window
        if window == nil {return}
        self.view.frame = window!.bounds
        window?.addSubview(self.view)
        
        viewController.addChild(self)
        
        self.didMove(toParent: viewController)
        
        self.currentTutorialIndex = -1
        self.showNextTutorial()
    }
    
    func close() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
        UIApplication.shared.isIdleTimerDisabled = false
        //        UIApplication.shared.endIgnoringInteractionEvents()
        if viewMainHomeflage || isCircle || tutAddWajbeh || tutAddWajbehLastStep || tutAddTamreenMainHome {
            viewMainHome.view.superview?.isUserInteractionEnabled = true
        }else if addEditWajbehFirstStep || addEditWajbehLastStep{
            addEditWajbeh.view.superview?.isUserInteractionEnabled = true
        }else if showAlertUnitWajbeh  && isMainTut {
            DisplayCateWajbeh.view.superview?.isUserInteractionEnabled = true
        }else if showAlertUnit || isFlagTamreen {
            DisplayTamreen.view.superview?.isUserInteractionEnabled = true
        }else if addTutTamreen || addTutTamreen1{
            TamreenTypeListCV.view.superview?.isUserInteractionEnabled = true
        }else if showWajbehMealPlannerVC != nil{
            showWajbehMealPlannerVC.moveToTopPage()
        }else if DisplayCateWajbeh != nil && !isMainTut{
            DisplayCateWajbeh.moveToTopPage()
        }
    }
    func showSimpleActionSheet() {
        
        let alert = UIAlertController(title: "حدد وحدة الوقت", message: nil , preferredStyle: .alert)
        
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title:"دقيقة", style: .default, handler: { (_) in
        }))
        
        alert.addAction(UIAlertAction(title:"ساعة", style: .default, handler: { (_) in
        }))
        
        
        
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: { [self] in
            presentedUIAlertControllerUnit = true
            alert.view.superview?.isUserInteractionEnabled = false
            if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") == "1" {
                var f = alert.view.frame
                f.size.height = 30
                f.origin.y = f.origin.y + 70
                f.origin.x = f.origin.x + 2
                f.size.width = f.size.width - 5
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                    DisplayTamreen.showTutorialAddTamreenShowAlert(location:f)
                }
            }
        })
    }
    func showDialog(id:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        let alert_cancel = UIAlertAction.init(title: "الغاء", style: .cancel, handler: nil)
        alert.view.tintColor = colorGreen
        if id != "6" {
            alert.addAction(UIAlertAction(title: viewMainHome.getText(id: id), style: .default, handler: {  _ in
            }))
        }
        alert.addAction(UIAlertAction(title: "تعديل المنبه", style: .default) {  _ in
        })
        
        var titleTut = "كيفية إضافة وجبة"
        if id == "6" {
            titleTut = "كيفية إضافة تمرين"
        }
        
        alert.addAction(UIAlertAction(title:titleTut, style: .default) { _ in
            
        })
        
        
        alert.addAction(alert_cancel)
        self.present(alert, animated: true) {  [unowned self]  in
            
            presentedUIAlertController = true
            alert.view.superview?.isUserInteractionEnabled = false
            var f = alert.view.frame
            f.size.height = 30
            f.origin.y = f.origin.y + 5
            f.origin.x = f.origin.x + 2
            f.size.width = f.size.width - 5
            
            viewMainHome.location.append(f)
            
            if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1" {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                    if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1" {
                        viewMainHome.startShowTutorialWajbehAlert()
                    }
                }
            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        guard let point = touch?.location(in: view) else { return }
        guard let sublayers = view.layer.sublayers as? [CAShapeLayer] else { return }
        
        for layer in sublayers {
            if let path = layer.path, path.contains(point) {
                print("#######")
            }
        }
    }
    @objc func handleTapGesture(gesture: UITapGestureRecognizer) {
        self.showNextTutorial()
    }
    func removeAlertController(){
        

        if presentedUIAlertController {
            UIAlertController.top?.dismiss(animated: true, completion: nil)
            if DisplayTamreen != nil {
            DisplayTamreen.perform(#selector(DisplayTamreen.setAutText),with:0,afterDelay: 0.2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            DisplayTamreen.dropDown.selectPrgramticlly(index: 1)
            }
            }
            presentedUIAlertController = false
        }
        if presentedUIAlertControllerUnit {
            UIAlertController.top?.dismiss(animated: true, completion: nil)
            presentedUIAlertControllerUnit = false
            
        }
    }
    @objc func skipTapGesture(gesture: UITapGestureRecognizer) {
        print("skip tutorial")
        //        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
        
        if getUserInfo().loginType == "1" {
            createEvent(key: "59", date: getDateTime())
        }else if getUserInfo().loginType == "2" {
            createEvent(key: "56", date: getDateTime())
        }else if getUserInfo().loginType == "3" {
            createEvent(key: "57", date: getDateTime())
        }else if getUserInfo().loginType == "4" {
            createEvent(key: "58", date: getDateTime())
        }
        
        timer?.invalidate()
        timer = nil
        setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
        setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
        setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
        setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
        setDataInSheardPreferance(value: "1", key: "isTutorial-water")
        setDataInSheardPreferance(value: "1", key: "isTutorial")
        removeAlertController()
        self.close()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        if viewMainHomeflage || isCircle || tutAddWajbeh || tutAddWajbehLastStep || tutAddTamreenMainHome {
            //             NSObject.cancelPreviousPerformRequests(withTarget: viewMainHome!)
        }else if addEditWajbehFirstStep || addEditWajbehLastStep{
            NSObject.cancelPreviousPerformRequests(withTarget: addEditWajbeh!)
        }else if showAlertUnitWajbeh && isMainTut{
            NSObject.cancelPreviousPerformRequests(withTarget: DisplayCateWajbeh!)
        }else if showAlertUnit || isFlagTamreen {
            NSObject.cancelPreviousPerformRequests(withTarget: DisplayTamreen!)
        }else if addTutTamreen || addTutTamreen1{
            NSObject.cancelPreviousPerformRequests(withTarget: TamreenTypeListCV!)
        }else if DisplayCateWajbeh != nil && !isMainTut{
            DisplayCateWajbeh.moveToTopPage()
        }
        if viewMainHomeflage {
            tblUserWaterDrinked.removeForUser()
            viewMainHome.removePrgramticlly()
            setDataInSheardPreferance(value: "0", key: "tutWaterRun")
        }
        
        if viewMainHomeflage || isCircle || tutAddWajbeh || tutAddWajbehLastStep || tutAddTamreenMainHome {
            //            DisplayCateWajbeh.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            
        }else if addEditWajbehFirstStep || addEditWajbehLastStep{
            addEditWajbeh.presentingViewController?.dismiss(animated: true, completion: nil)
        }else if showAlertUnitWajbeh && isMainTut{
            DisplayCateWajbeh.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else if showAlertUnit || isFlagTamreen {
            DisplayTamreen.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }else if addTutTamreen || addTutTamreen1{
            TamreenTypeListCV.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        moveTopController(scroll: scroll_home_main)
    }
    
    func showNextTutorial() {
        UIApplication.shared.isIdleTimerDisabled = true
        currentTutorialIndex += 1
        countWater+=1
        countTamreen+=1
        countNutration+=1
        showAlertUnitWajbehCounter+=1
        print("currentTutorialIndex \(currentTutorialIndex)")
        
        if currentTutorialIndex >= self.tutorials.count {
            if addEditWajbehLastStep {
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    addEditWajbeh.toutorialViewWajbeh()
                }
            }else if addTutTamreen {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                    if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                        TamreenTypeListCV.goToPage(id: 73, title: "سرعة ٩ كيلومتر/ساعة")
                    }
                    close()
                }
            }else if countTamreen == 4 && isFlagTamreen == true {
                print("T!")
                setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
                if getDataFromSheardPreferanceString(key: "isTutorial-nutration") != "2"{
                    setDataInSheardPreferance(value: "1", key: "isTutorial-nutration")
                    
                }
                DisplayTamreen.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }else if isFlagWajbeh == true {
                setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
                if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                    setDataInSheardPreferance(value: "1", key: "isTutorial-tamreen")
                }
                DisplayCateWajbeh.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }else if viewMainHomeNutrationValues {
                print("nutraion \(countNutration)")
                if countNutration == 1 {
                    print("nutraion index 1")
                    isCircle = false
                }else if countNutration == 4 {
                    print("nutraion index 4")
                    setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
                }
            }else if viewMainHomeflage {
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    tblUserWaterDrinked.removeForUser()
                    viewMainHome.enableController()
                    viewMainHome.removePrgramticlly()
                    viewMainHome.startShowTutorialWajbeh()
                }
            }else if showAlertUnitWajbeh && isMainTut{
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    showAlertUnitWajbeh = false
                    flagTut = 0
                }
            }else if showAlertUnitWajbehCounter == 3 && showWajbehMealPlannerVC != nil{
                print("showAlertUnitWajbehCounter ",showAlertUnitWajbehCounter)
                showWajbehMealPlannerVC.moveToTopPage()
            }else if DisplayCateWajbeh != nil && !isMainTut{
                print("showAlertUnitWajbehCounter ",showAlertUnitWajbehCounter)
                DisplayCateWajbeh.moveToTopPage()
            }else if  viewMainHomeMealPlanner{
                viewMainHome.startTutorialWater()
            }
            
            
            self.close()
            return
        }
        if viewMainHomeflage {
            //             if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
            setDataInSheardPreferance(value: "1", key: "isTutorial-water")
            viewMainHome.selectPrgramticlly(index: countWater)
            //             }
        }
        else if tutAddWajbeh {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    //                     viewMainHome.showAlertTut()
                    showDialog(id:id_category ?? "1")
                }
            }
        }
        else if addTutTamreen {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                    TamreenTypeListCV.goToPage(id: 73, title: "سرعة ٩ كيلومتر/ساعة")
                }
//                close()
            }
        }
        else if tutAddWajbehLastStep {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    viewMainHome.toAddWajbehSenfManual(ob:self)
                }
//                close()
            }
        }else if tutAddTamreenMainHome {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                    viewMainHome.performSegue(withIdentifier: "TamreenTypeListController", sender: self)
                }
//                close()
            }
        }else if showAlertUnit {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                    showSimpleActionSheet()
                }
//                close()
            }
        }else if countTamreen == 1 && isFlagTamreen == true {
            //             if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
            //                removeAlertController()
            //             }
        }else if countTamreen == 2 && isFlagTamreen == true {
            if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2"{
                removeAlertController()
            }
        }else if countTamreen == 3 && isFlagTamreen == true {
           
        }else if showAlertUnitWajbeh && isMainTut{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                    flagTut = 1
//                    DisplayCateWajbeh.selectPrgramticlly(index:0)
                    isFlagWajbeh = true
                }
            }
        }else if viewMainHomeNutrationValues {
            if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
                print("nutraion \(countNutration)")
                if countNutration == 1 {
                    print("nutraion - index 1")
                    isCircle = false
                }else if countNutration == 4 {
                    print("nutraion - index 4")
                    setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
                }
            }
        }
        let tut = self.tutorials[self.currentTutorialIndex]
        self.showTutorial(tut)
    }
    
    func showTutorial(_ tutorial: KJTutorial) {
        
        // remove old tutorial
        self.view.backgroundColor = UIColor.clear
        for subView in self.view.subviews {
            if subView.tag == 9999 {
                subView.removeFromSuperview()
            }
        }
        
        // config tutorial
        bgView = UIView(frame: self.view.bounds)
        bgView.tag = 9999
        bgView.backgroundColor = self.overlayColor
        
        // add focus region
        let path = CGMutablePath()
        path.addRect(self.view.bounds)
        if isCircle {
            path.addRoundedRect(in: tutorial.focusRectangle,
                                cornerWidth: tutorial.focusRectangle.height/2-1,
                                cornerHeight: tutorial.focusRectangle.height/2-1)
        }else{
            path.addRoundedRect(in: tutorial.focusRectangle,
                                cornerWidth: tutorial.focusRectangleCornerRadius,
                                cornerHeight: tutorial.focusRectangleCornerRadius)
        }
        maskLayer.path = path
        maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        bgView.clipsToBounds = true
        bgView.layer.mask = maskLayer
        
        
        let tutView = UIView(frame: self.view.bounds)
        tutView.tag = 9999
        tutView.backgroundColor = UIColor.clear
        
        // add message
        let lblMessage = UILabel()
        lblMessage.numberOfLines = 0
        lblMessage.textAlignment = .center
        lblMessage.attributedText = tutorial.message
        lblMessage.lineBreakMode = .byWordWrapping
        let fitSize = lblMessage.sizeThatFits(CGSize(width: bgView.bounds.size.width-40,
                                                     height: bgView.bounds.size.height))
        lblMessage.frame.size = fitSize
        lblMessage.center = tutorial.messagePosition
        lblMessage.font = UIFont(name:"GE Dinar One", size: 17)
        tutView.addSubview(lblMessage)
        //        if countWater == 1 && viewMainHomeflage{
        let btnSkip = UIButton()
        btnSkip.frame.size = fitSize
        btnSkip.center = tutorial.messagePosition
        btnSkip.frame.size.height = 60
        btnSkip.contentMode = .scaleAspectFit
        //        btnSkip.center.x = tutorial.messagePosition.x
        btnSkip.layer.cornerRadius = btnSkip.frame.size.height/2
        btnSkip.frame.origin.x = 16
        btnSkip.frame.size.width = 60
        //        if addEditWajbehFirstStep || addEditWajbehLastStep {
        //            print("yazan")
        //            btnSkip.frame.origin.y = tutorial.messagePosition.y + 100
        //        }else{
        btnSkip.frame.origin.y = UIScreen.main.bounds.height - 70
        //        }
        //         btnSkip.titleLabel?.font = UIFont(name:"GE Dinar One", size: 19)
        btnSkip.backgroundColor = colorBasicApp
        let tintedImage = UIImage(named: "ic_close")!.withRenderingMode(.alwaysTemplate)
        btnSkip.setImage(tintedImage, for: .normal)
        btnSkip.tintColor = .black
        //         btnSkip.setTitle("تخطي", for: .normal)
        if !viewMainHomeMealPlanner {
        tutView.addSubview(btnSkip)
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(skipTapGesture))
        btnSkip.addGestureRecognizer(tap)
        //        }
        // add image
        if let icon = tutorial.icon {
            let imvIcon = UIImageView(frame: tutorial.iconFrame)
            imvIcon.image = icon
            imvIcon.contentMode = .scaleAspectFit
            if tutAddTamreenMainHome {
                imvIcon.transform = CGAffineTransform(scaleX: -1.0, y: -1.0)
            }else if viewMainHomeMealPlanner {
                imvIcon.transform = CGAffineTransform(scaleX: -1.0, y: -1.0)
            }
            tutView.addSubview(imvIcon)
        }
        
        if viewMainHomeMealPlanner {
            if let icon1 = tutorial.icon {
                var locate = tutorial.iconFrame
                locate.origin.y = locate.maxY+70
                let imvIcon = UIImageView(frame: locate)
                imvIcon.image = icon1
                imvIcon.contentMode = .scaleAspectFit
                tutView.addSubview(imvIcon)
            }
        }
        if !tutorial.isArrowHidden {
            
            // add arrow line
            let fromPoint = lblMessage.frame.topCenterPoint.moveY(-4)
            let toPoint = tutorial.focusRectangle.bottomCenterPoint.moveY(16)
            
            let lineLayer = CAShapeLayer()
            lineLayer.path = KJOverlayTutorialViewController.createCurveLinePath(from: fromPoint, to: toPoint)
            lineLayer.strokeColor = UIColor.white.cgColor
            lineLayer.fillColor = nil
            lineLayer.lineWidth = 4.0;
            lineLayer.lineDashPattern = [10, 5, 5, 5]
            tutView.layer.addSublayer(lineLayer)
            
            // add arrow icon
            let arrowLayer = CAShapeLayer()
            arrowLayer.path = KJOverlayTutorialViewController.createArrowPathWithCurveLine(from: fromPoint, to: toPoint, size: 8.0)
            arrowLayer.strokeColor = UIColor.white.cgColor
            arrowLayer.fillColor = UIColor.white.cgColor
            arrowLayer.lineWidth = 1.0;
            tutView.layer.addSublayer(arrowLayer)
        }
        
        // showing
        bgView.alpha = 0.0
        tutView.alpha = 0.0
        self.view.addSubview(bgView)
        self.view.addSubview(tutView)
        //    let tap = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        //    view.addGestureRecognizer(tap)
        // fade animation
        //    UIView.animate(withDuration: 0.35, delay: 0, options: .curveEaseOut, animations: {
        //      bgView.alpha = 1.0
        //    })
        //    UIView.animate(withDuration: 0.35, delay: 0.25, options: .curveEaseOut, animations: {
        //      tutView.alpha = 1.0
        //    })
        
        // focus animation
        bgView.alpha = 1.0
        //        let focusAnim:CAAnimation!
        //        if isCircle {
        //            focusAnim = KJOverlayTutorialViewController.createFocusAnimation(outsideRect: self.view.bounds, focusRect: tutorial.focusRectangle, cornerRadius: tutorial.focusRectangle.height/2-1, duration: 0.35)
        //        }else{
        //            focusAnim = KJOverlayTutorialViewController.createFocusAnimation(outsideRect: self.view.bounds, focusRect: tutorial.focusRectangle, cornerRadius: 4.0, duration: 0.35)
        //        }
        //                maskLayer.add(focusAnim, forKey: nil)
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        maskLayer.path = path
        CATransaction.commit()
        
        // floating animation
        tutView.alpha = 1.0
        let floatingAnim = KJOverlayTutorialViewController.createFloatingPOPAnimation(centerPoint: tutView.center, float: 8)
        tutView.pop_add(floatingAnim, forKey: nil)
    }
    
    
 }
 
 
 // MARK: - Path Helper
 
 extension KJOverlayTutorialViewController {
    
    static func createCurveLinePath(from: CGPoint, to: CGPoint) -> CGPath {
        
        let vector = CGPoint.createVector(from: from, to: to, isUnit: true)
        let perpendVector = CGPoint.createPerpendicularVectorWith(vector: vector)
        let distance = CGFloat(CGPoint.distanceOf(p1: from, p2: to) * 0.085)
        
        let centerPoint = CGPoint(x: (from.x + to.x)/2.0, y: (from.y + to.y)/2.0)
        let controlDirection: CGFloat = from.x <= to.x ? -1 : 1
        let controlPoint = CGPoint(x: centerPoint.x + perpendVector.x * distance,
                                   y: centerPoint.y + perpendVector.y * distance * controlDirection)
        
        let path = CGMutablePath()
        path.move(to: from)
        path.addQuadCurve(to: to, control: controlPoint)
        return path
    }
    
    static func createArrowPathWithCurveLine(from: CGPoint, to: CGPoint, size: CGFloat) -> CGPath {
        
        // vectors
        let vector = CGPoint.createVector(from: from, to: to, isUnit: true)
        
        let controlDirection: CGFloat = from.x <= to.x ? -1 : 1
        let distance = CGFloat(CGPoint.distanceOf(p1: from, p2: to) * 0.00085) * controlDirection
        var fromCurve = CGPoint(x: to.x - vector.x, y: to.y - vector.y)
        fromCurve = CGPoint(x: fromCurve.x + (-vector.y) * distance, y: fromCurve.y + vector.x * distance)
        
        let curveVector = CGPoint.createVector(from: fromCurve, to: to, isUnit: false)
        let reverseVector = CGPoint.createReverseVectorWith(vector: curveVector)
        let perpendVector = CGPoint.createPerpendicularVectorWith(vector: curveVector)
        
        // calculate points
        let centerPoint = CGPoint(x: to.x + reverseVector.x * size,
                                  y: to.y + reverseVector.y * size)
        let leftPoint = CGPoint(x: centerPoint.x - perpendVector.x * size * 0.5,
                                y: centerPoint.y - perpendVector.y * size * 0.5)
        let rightPoint = CGPoint(x: centerPoint.x + perpendVector.x * size * 0.5,
                                 y: centerPoint.y + perpendVector.y * size * 0.5)
        
        // make triangle
        let path = CGMutablePath()
        path.move(to: to)
        path.addLine(to: leftPoint)
        path.addLine(to: rightPoint)
        path.closeSubpath()
        return path
    }
    
 }
 
 // MARK: - Animation Helper
 
 extension KJOverlayTutorialViewController {
    
    static func createFloatingPOPAnimation(centerPoint: CGPoint, float: CGFloat) -> POPAnimation {
        
        let anim = POPSpringAnimation(propertyNamed: kPOPViewCenter)!
        
        anim.fromValue = NSValue(cgPoint: centerPoint)
        anim.toValue = NSValue(cgPoint: centerPoint.moveY(-8))
        anim.springBounciness = 0
        anim.springSpeed = 1
        anim.autoreverses = true
        anim.repeatForever = true
        
        return anim;
    }
    
    static func createFocusAnimation(outsideRect: CGRect, focusRect: CGRect, cornerRadius: CGFloat, duration: CFTimeInterval) -> CAAnimation {
        
        let duration1 = duration * 0.4
        let duration2 = duration * 0.3
        let duration3 = duration - duration1 - duration2
        let begin1 = 0.0
        let begin2 = begin1 + duration1
        let begin3 = begin2 + duration2
        
        let distance = min(12, min(focusRect.width, focusRect.height) - 8);
        let offset1 = distance
        let offset2 = distance * 0.5
        
        let rect0 = outsideRect
        _ = focusRect.insetBy(dx: offset1, dy: offset1)
        let rect2 = focusRect.insetBy(dx: -offset2, dy: -offset2)
        _ = focusRect
        
        let path0 = CGMutablePath()
        path0.addRect(rect0)
        //            path0.addRoundedRect(in: rect0, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
        
        let path1 = CGMutablePath()
        path1.addRect(rect0)
        //    path1.addRoundedRect(in: rect1, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
        
        let path2 = CGMutablePath()
        path2.addRect(rect0)
        path2.addRoundedRect(in: rect2, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
        
        let path3 = CGMutablePath()
        path3.addRect(rect0)
        //            path3.addRoundedRect(in: rect3, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
        
        let anim1 = createPathAnimation(fromPath: path0, toPath: path1, beginTime: begin1, duration: duration1)
        let anim2 = createPathAnimation(fromPath: path1, toPath: path2, beginTime: begin2, duration: duration2)
        let anim3 = createPathAnimation(fromPath: path2, toPath: path3, beginTime: begin3, duration: duration3)
        
        let anim = CAAnimationGroup()
        anim.duration = duration
        anim.animations = [anim1, anim2, anim3]
        return anim
    }
    
    static func createPathAnimation(fromPath: CGPath, toPath: CGPath, beginTime: CFTimeInterval, duration: CFTimeInterval) -> CABasicAnimation {
        
        let anim = CABasicAnimation(keyPath: "path")
        
        anim.fromValue = fromPath
        anim.toValue = toPath
        anim.beginTime = beginTime
        anim.duration = duration
        
        return anim
    }
    
 }
 extension KJOverlayTutorialViewController:EventPresnter{
     func createEvent(key: String, date: String) {
         Analytics.logEvent("ta5sees_\(key)", parameters: [
           "date": date
         ])
     }
 }
