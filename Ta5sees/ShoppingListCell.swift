//
//  CollectionViewCell1234.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 15/04/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//



import UIKit

class ShoppingListCell: UICollectionViewCell {

    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    var delegate:FridgeList!
    var delegateShopping:ShoppingList!
    override func awakeFromNib() {
        super.awakeFromNib()
        btn.addTarget(self, action: #selector(tap(_:)), for: UIControl.Event.touchUpInside)

        // Initialization code
    }

    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        print("delegateShopping123")
        if item.isCheck == 0 {
            tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:1) { (response) in
                item.isCheck = 1
                btn.setImage(UIImage(named: "green_ic_checked"), for: .normal)
            }
        }else{
            tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:0) { (response) in
                item.isCheck = 0
                btn.setImage(UIImage(named: "green_ic_unchecked"), for: .normal)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            refreshPage()
        }
    }
    
    func refreshPage(){
        if delegateShopping != nil {
            print("delegateShopping")
            delegateShopping.setupShowData()
        }
        if delegate != nil {
            print("delegate")
            delegate.viewDidAppear(true)
        }
    }
    var item: shoppingModel! {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI(){
        var textCal = ""
        if item.isCheck == 1 {
            self.btn.setImage(UIImage(named: "green_ic_checked"), for: .normal)
        }else{
            self.btn.setImage(UIImage(named: "green_ic_unchecked"), for: .normal)
        }
        self.nameLable.text = item.nameSenf
        
        if Double(item.quantity)! < 1 {
             textCal = "\(1) غم"
        }else{
            textCal =  "\(Int(round(Double(item.quantity)!))) غم"
        }
        let attributedString1 = NSMutableAttributedString.init(string: textCal)
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:16)!, range: (textCal as NSString).range(of: "\(item.quantity)"))
        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:17)!, range: (textCal as NSString).range(of: "غم"))
        
        self.quantity.attributedText = attributedString1
    }
    
 

}
