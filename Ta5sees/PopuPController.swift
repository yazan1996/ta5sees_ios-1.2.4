//
//  PopuPController.swift
//  Ta5sees
//
//  Created by Admin on 9/22/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class PopuPController: UIViewController {
    
    @IBOutlet weak var firstNumber: UILabel!
    @IBOutlet weak var lastNumber: UILabel!
    var plusFlag = 0
    var MinusFlag = 0
    var userInfo:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!
    var viewAccountPresnter:PresnterAccount?
    var weight:Double!
    var refPlan:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        tabBarController?.tabBar.barTintColor = .white
        tabBarController?.tabBar.tintColor = .white
//        self.navigationItem.hidesBackButton = true
      
        setUpLabel()
        firstNumber.font = firstNumber.font.withSize(30)
        lastNumber.font = firstNumber.font.withSize(40)
        firstNumber.alpha = 0.7
        lastNumber.alpha = 1
        plusFlag = 2
        MinusFlag = 2
      
        getInfoUser { (item, error) in
            self.userInfo = item
        }
        
        getInfoHistoryProgressUser { [self] (user, err) in
            self.user_ProgressHistory = user
        }
        
        if userInfo.grown != "2" {
        let newBackButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(backButtonTapped(sender:)))
        self.navigationItem.rightBarButtonItem = newBackButton
        self.navigationItem.rightBarButtonItem?.tintColor = colorGray

        }
        refPlan = user_ProgressHistory!.refPlanMasterID
        weight = Double(user_ProgressHistory.weight)
        var _target = user_ProgressHistory!.target
        if userInfo!.grown == "2" {
          if refPlan == "1" {
            _target = "\(Double(user_ProgressHistory!.weight)!-1.0)"
            }else{
                _target = "\(Double(user_ProgressHistory!.weight)!-1.0)"

            }
        }
        
       if let index = _target.firstIndex(of: ".") {
           firstNumber.text = "\(_target.prefix(upTo: index))"
          let x = _target.suffix(from: index)
        if x.firstIndex(of: ".") != nil {
            lastNumber.text = "\(x.dropFirst(1))"
        }else{
            lastNumber.text = "0"
        }
       }else{
        firstNumber.text = "\(_target)"
        lastNumber.text = "0"
        

        }
    }

//    func checkPalnMaster(valu:Double)->Bool{
//        if refPlan == "1" {
//            print((valu-weight))
//            if (valu-weight) <= 0 {
////            if valu <= weight{
//                return false
//            }
//        }else if refPlan == "2"{
////            if valu >= weight{
//                if (weight-valu) >= 1 {
//                return false
//            }
//        }
//        return true
//    }
    @IBAction func btnMinus(_ sender: Any) {
        if MinusFlag == 1 {
            animationviewBottom(dataView: firstNumber)
          
            var num = Int(firstNumber.text!)
            num!-=1
            if num! < 0 {
                return
            }
//            let x = Double("\(num!).\(lastNumber.text!)")!
//            if refPlan == "1" && x <= Double(userInfo.weight)! {
//                return
//            }else if refPlan == "2" && x >= Double(userInfo.weight)! {
//                return
//            }
//
            
           
            firstNumber.text = "\(num!)"
        }else if MinusFlag == 2{
            animationviewBottom(dataView: lastNumber)
            var num = Int(lastNumber.text!)
//            let x = Double("\(firstNumber.text!).\(abs(num!))")!

//            if refPlan == "1" && x <= Double(userInfo.weight)! {
//                return
//            }else if refPlan == "2" && x >= Double(userInfo.weight)! {
//                return
//            }
            num!-=1

            if num! < 0 {
                
                num = 9
                firstNumber.text! =  "\(Int(firstNumber.text!)!-1)"
                animationviewBottom(dataView: firstNumber)
            }
            lastNumber.text = "\(num!)"
        }
    }
    @IBAction func btnPlus(_ sender: Any) {
        if plusFlag == 1 {
            animationviewTop(dataView: firstNumber)
            var num = Int(firstNumber.text!)
//            let x = Double("\(num!).\(lastNumber.text!)")!+1.0
//            if !checkPalnMaster(valu: x) {
//                return
//            }else{
                num!+=1
//            }
            if num! < 0 {
                return
            }
            firstNumber.text = "\(num!)"
        }else if plusFlag == 2{
            animationviewTop(dataView: lastNumber)
            var num = Int(lastNumber.text!)
//            let x = Double("\(firstNumber.text!).\(num!)")!
//            if !checkPalnMaster(valu: x) {
//                return
//            }else{
                num!+=1
//            }
            if num! > 9 {
                num = 0
                animationviewTop(dataView: firstNumber)
                firstNumber.text! =  "\(Int(firstNumber.text!)!+1)"
            }
            lastNumber.text = "\(num!)"
        }
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem) {
        viewAccountPresnter?.setAlphaView(flag: false)
        dismiss(animated: true, completion: nil)
        //         self.navigationController?.preferredContentSize = CGSize(width: 348, height: 200)
        //         self.navigationController?.popViewController(animated: true)
    }
    
    
    func setUpLabel() -> Void {
        
        firstNumber.isUserInteractionEnabled = true
        lastNumber.isUserInteractionEnabled = true
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        firstNumber.addGestureRecognizer(tap1)
        lastNumber.addGestureRecognizer(tap2)
        
    }
    
    @IBAction func btnUpdateTarget(_ sender: Any) {
        let new =  "\(firstNumber.text!).\(lastNumber.text! )"

        if refPlan == "1" && (Double(new)!-Double(user_ProgressHistory.weight)!) < 1 {
            alert(mes: "يجب ان يكون الهدف اكبر بمقدار كيلو من الوزن الحالي ", selfUI: self)
            return
        }else if refPlan == "2" && (Double(user_ProgressHistory.weight)!-Double(new)!) < 1 {
            alert(mes: "يجب ان يكون الهدف اقل بمقدار كيلو من الوزن الحالي ", selfUI: self)
            return
        }
       dismiss(animated: true, completion: nil)
        viewAccountPresnter?.setNewTargets(val:new)
//        viewAccountPresnter?.setAlphaView()
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let tapped = sender.view!
        if tapped.tag == 1 {
            firstNumber.font = firstNumber.font.withSize(40)
            lastNumber.font = firstNumber.font.withSize(30)
            firstNumber.alpha = 1
            lastNumber.alpha = 0.7
            plusFlag = 1
            MinusFlag = 1
        }else if tapped.tag == 2{
            firstNumber.font = firstNumber.font.withSize(30)
            lastNumber.font = firstNumber.font.withSize(40)
            firstNumber.alpha = 0.7
            lastNumber.alpha = 1
            plusFlag = 2
            MinusFlag = 2
        }else{
            print("tap not found")
            
        }
    }
}
