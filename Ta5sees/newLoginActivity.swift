//
//  ViewController123.swift
//  Ta5sees
//
//  Created by Admin on 7/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
//import SwiftGifOrigin
import SwiftyGif
import FirebaseAuth
import Firebase
import SwiftyJSON
//import KSFacebookButton
import TwitterKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import FlagPhoneNumber
import FirebaseFirestore
 

class newLoginActivity: UIViewController,UIViewControllerTransitioningDelegate ,UITextFieldDelegate,FPNTextFieldDelegate{
    
    
   
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        print( textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil")
        textField.rightViewMode = .always
        textField.textColor =  isValid ? colorGray : UIColor.red
        isNumberValid = isValid
        numberPhone = textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil"
        if isNumberValid {
//            firstPinView.becomeFirstResponder()
        }
    }
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print("name country \(name)  - \(dialCode)  - \(code)")
//        cutomeHintFormatPhon(country: name,text: textPhone)
        
    }
    

    
    
    var loginType:String!
    var numberPhone:String!
    var isNumberValid:Bool!
    @IBOutlet weak var textPhone: FPNTextField!
    var KeymobNum:String!
    let db = Firestore.firestore()
    var obAPI = APiSubscribeUser()
    

    
    @IBOutlet weak var lablePhone: RoundedTextField!
    
    let obcheckInternet = checkInternet()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUPTextField()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    

    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func setupCuntryPickr(){
        textPhone.parentViewController = self
        textPhone.delegate = self
        textPhone.hasPhoneNumberExample = false

        textPhone.setFlag(for: FPNCountryCode(rawValue: "JO")!)
//        textPhone.setCountries(including: [.JO,.SA,.QA,.KW,.EG,.BH,.AE,.PS])
        textPhone.layer.cornerRadius = textPhone.bounds.size.height * 0.5
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        obcheckInternet.setupCheckInternet()
//         let c = setupRealm().objects(tblUserInfo.self).filter("facebookID == %@","787773358326238")
//                print(c)
//                try! setupRealm().write {
//                    setupRealm().delete(c)
//                }
        setupCuntryPickr()
        
    
    }
    
    
    
    
    func setUPTextField(){
        changePlaceHolder(text: lablePhone.textField,str:"رقم الهاتف",flag:1)
        lablePhone.textField.isEnabled = false
        lablePhone.textField.textColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    }
    
    
    
    
    
    @objc func forgetPassword(_ gestureRecognizer: UITapGestureRecognizer) { // forgetPassword
        SVProgressHUD.show()
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 99 {
            if self.textPhone.text == "" || isNumberValid == false{
                alert(mes:"يجب ادخال رقم الهاتف",selfUI: self)
            }else if isNumberValid == true {
                PhoneAuthVerfy.sheard.sentCodePinForgetPass(numberPhone: self.numberPhone) { (flag,err) in
                if flag == 0 {
                    SVProgressHUD.dismiss()
                    alert(mes:"\(err)" ,selfUI: self)
                    //                         "لم يتم ارسال طلب رمز التحقق\nحاول مرة اخرى"
                    return
                }
                SVProgressHUD.dismiss()
                self.performSegue(withIdentifier: "ForgetPasswordController", sender: self.numberPhone)
                }
//
//
            }
        }
        
    }
    
    
    func changePlaceHolder(text:UITextField,str:String,flag:Int){
        if flag == 0 {
            text.attributedPlaceholder = NSAttributedString(string: str,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        }else{
            text.attributedPlaceholder = NSAttributedString(string: str,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
  
  
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        view.endEditing(true)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if segue.identifier == "SetPasswordController" {
//            if let dis=segue.destination as? SetPasswordController{
//
//                if  let number=sender as? String {
//                    dis.numberPhone = number
//                }
//            }
//        }
//        else
        if segue.identifier == "VerificationCode" {
            if let dis=segue.destination as?  VerificationCode{
                
                if  let number=sender as? String {
                    dis.numberPhone = number
                }
            }
        }
        
    }

    
    
    @IBAction func btnSignIn(_ sender: Any) {
        if self.textPhone.text == "" || isNumberValid == false{
            alert(mes:"يجب ادخال رقم الهاتف",selfUI: self)
        }else {
            print("createEvent")
            createEvent(key: "51", date: getDateTime())
            requestAPiMSISDN(mobID:String(numberPhone), pass:  FormatterTextString(str:"nil"))
            
            
            //         requestAPi(mobID:String(numberPhone.dropFirst()), password: password,loginType:"1")
        }
    }
    
    
    func checkdataFromFirestore(id:String,pass:String){
        print("*****")
        SVProgressHUD.show()
        let isExist = db.collection("Users").document(String(numberPhone))
        isExist.getDocument { (document, error) in
            print("******")
            if error == nil {
            if let document = document, document.exists {
//                SVProgressHUD.dismiss()
//                let detailView = self.storyboard!.instantiateViewController(withIdentifier: "SetPasswordController") as! SetPasswordController
//                    detailView.numberPhone =  self.numberPhone
//                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
//                    detailView.transitioningDelegate = self
//                    self.present(detailView, animated: true, completion: nil)
//                self.performSegue(withIdentifier: "SetPasswordController", sender: self.numberPhone)
            } else {
                SVProgressHUD.dismiss()
                self.sentCodePin()
                print("Document does not exist")
            }
            }else{
                SVProgressHUD.dismiss()
                showToast(message: "لم يتم إكمال طلبك", view: self.view,place:0)

            }
        }
        
    }
   
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    func requestAPiMSISDN(mobID:String,pass:String){
        SVProgressHUD.show()
        tblUserInfo.checkFoundIDUserMSISDN(iduser: mobID) { (bool) in
            if bool == true {
                SVProgressHUD.dismiss()
                sentCodePin()
            }else {
                print("****")
                sentCodePin()
            }
        }
    }
    
  
    func sentCodePin(){
        PhoneAuthVerfy.sheard.sentCodePinForgetPass(numberPhone: self.numberPhone) { [self] (flag,err) in
            if flag == 0 {
                SVProgressHUD.dismiss()
                if err.contains("17010") {
                    alert(mes:"لقد تخطيت عدد الرسائل المسموحة" ,selfUI: self)
                }else if err.contains("17042") {
                    alert(mes:"رقم الهاتف غير فعال" ,selfUI: self)
                }else{
                    alert(mes:"\(err)" ,selfUI: self)
                }
                //                         "لم يتم ارسال طلب رمز التحقق\nحاول مرة اخرى"
                return
            }
//            obAPI.saveUserFirestore(id:self.numberPhone)
            SVProgressHUD.dismiss()
            setDataInSheardPreferance(value: self.numberPhone, key: "phone")
            setDataInSheardPreferance(value: String(self.numberPhone), key: "mobNum")
            setDataInSheardPreferance(value: "1", key: "loginType")
            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "VerificationCode") as! VerificationCode
            detailView.numberPhone =  self.numberPhone
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            detailView.transitioningDelegate = self
            self.present(detailView, animated: true, completion: nil)
//            self.performSegue(withIdentifier: "VerificationCode", sender: self.numberPhone)

            
        }
        
        
    }
    
    func requestAPi(mobID:String,password:String,loginType:String){//unused
        tblUser.checkFoundIDUserMSISDN(iduser: mobID) { (bool) in
            if bool == true {
                SVProgressHUD.dismiss()
                if UserDefaults.standard.integer(forKey: "login") == 0 {
                    UserDefaults.standard.set(1, forKey: "login")
                    print("user login app ")
                }
                self.performSegue(withIdentifier: "toHomePage", sender: self)
            }else {
                alert(mes: "لا يوجد بيانات مخزنة في قاعدة البيانات الخاصة بالجهاز", selfUI: self)
            }
        }
        APILogin.sheard.loginUser(numberPhone:mobID, password: password,loginType:loginType) { (flag,iduser) in
            if flag == 1 {
                
            }else if flag == 2 {
                alert(mes:"يوجد بيانات مفقودة",selfUI:self)
            }else if flag == 4 {
                alert(mes:"المستخدم غير مشترك مسبقا",selfUI:self)
            }else if flag == 3 {
                alert(mes:"تحقق من كلمة السر او اسم المستخدم",selfUI:self)
            }else {
                alert(mes:"حدث خطا،يرجى المحالة مجددا",selfUI:self)
            }
        }
    }
}
extension newLoginActivity:EventPresnter{
    func createEvent(key: String, date: String) {
        print("createEvent")
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
