//
//  btnFiacebook.swift
//  Ta5sees
//
//  Created by Admin on 1/16/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class btnFiacebook: UIButton {
    
    let buttonNormalBackgroundColor = UIColor(red: 59.0/255.0, green: 89.0/255.0, blue: 152.0/255.0, alpha: 1.0)
    let buttonHighlightBackgroundColor = UIColor(red: 31.0/255.0, green: 44.0/255.0, blue: 82.0/255.0, alpha: 1.0)
    
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonSetup()
    }
    
    override public func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // Set button corner radius
        layer.cornerRadius = bounds.size.height * 0.5
        clipsToBounds = true
        
        // Set Facebook icon
        let bundle = Bundle(for: btnFiacebook.self)
        let icon = UIImage(named: "iconFacebook", in: bundle, compatibleWith: nil)
        setImage(icon, for: .normal)
        imageView?.contentMode = .scaleAspectFit
        imageEdgeInsets = UIEdgeInsets(top: 0, left: 70, bottom: 0, right: 0)
    }
    
    override public var isHighlighted: Bool {
        willSet {
            backgroundColor = newValue ? buttonHighlightBackgroundColor : buttonNormalBackgroundColor
        }
    }
    
    // MARK: - Private
    private func commonSetup() {
        backgroundColor = buttonNormalBackgroundColor
        setTitleColor(.white, for: .normal)
        setTitleColor(.white, for: .highlighted)
        if #available(iOS 8.2, *) {
            titleLabel?.font =  UIFont(name: "GE Dinar One", size: 17.0)!
        } else {
            // Fallback on earlier versions
            titleLabel?.font =  UIFont(name: "GE Dinar One", size: 17.0)!
        }
    }
    
}
