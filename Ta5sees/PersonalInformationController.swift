//
//  PersonalInformationController.swift
//  Ta5sees
//
//  Created by Admin on 9/29/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import TextFieldEffects
import SVProgressHUD
class PersonalInformationController: UIViewController ,UIViewControllerTransitioningDelegate{
    
    @IBOutlet weak var lblLayaCondition: UILabel!
    @IBOutlet weak var lblAllaergy: UILabel!
    @IBOutlet weak var lblCusine: UILabel!
    @IBOutlet weak var txtdate: HoshiTextField!
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var btnFemaleOutlet: UIButton!
    @IBOutlet weak var btnMaleOutlet: UIButton!
    var obselectionDelegate:selectionDelegate!
    var idFetnessRate:Int!
    var gender:String!
    var ageChild:Float = 0.0
    var user_item:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!

    var obtblAllergyInfo:tblAllergyInfo!
    var obtblCusisneType:tblCusisneType!
    var flagChangeName = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtdate.clearsOnBeginEditing = true
        txtName.clearsOnBeginEditing = true
        
        btnMaleOutlet.titleLabel?.font = UIFont(name: "GE Dinar One",size: 17)
        btnFemaleOutlet.titleLabel?.font = UIFont(name:"GE Dinar One",size: 17)
        
    
        
        obselectionDelegate = self
        let showPicker = UITapGestureRecognizer(target: self, action: #selector((self.TimePicker(_:))))
        showPicker.numberOfTapsRequired=1
        txtdate.addGestureRecognizer(showPicker)
        getInfoUser { (user, err) in
            self.user_item = user
        }
        
        getInfoHistoryProgressUser { [self] (user, err) in
            
            self.user_ProgressHistory = user
        }
        
        if user_ProgressHistory.refGenderID == "1" {
            gender = "1"
            selectGender(Fbtn:btnMaleOutlet,Lbtn:btnFemaleOutlet)
        }else{
            gender = "2"
            selectGender(Fbtn:btnFemaleOutlet,Lbtn:btnMaleOutlet)
        }
        
        
        txtName.text = user_item.firstName
        txtdate.text = user_ProgressHistory.birthday
        lblAllaergy.text = tblAllergyUserItems.getDataSelectedProgressHistory()
        
        lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        lblCusine.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        lblAllaergy.font = UIFont(name: "GEDinarOne-Bold",size: 17)

        lblCusine.text = tblCuisnesUSerItems.getDataSelectedProgressHistory()
        lblLayaCondition.text = tblLayaqaCond.getItem(refID: Int(user_ProgressHistory.refLayaqaCondID)!)
        
        clicableLabel(lbl:lblCusine,tag:1)
        clicableLabel(lbl:lblAllaergy,tag:2)
        clicableLabel(lbl:lblLayaCondition,tag:3)
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    @IBAction func btnSave(_ sender: Any) {
        let cusins = tblCusisneType.getIDCusinenSelected(string: lblCusine.text!)
        let allegy = tblAllergyInfo.getIDtblAllergyInfoSelected(string: lblAllaergy.text!)
        let layaq = String(setupRealm().objects(tblLayaqaCond.self).filter("discription == %@",lblLayaCondition.text!).first!.id)

        if txtName.text != user_item.firstName || txtdate.text != user_ProgressHistory.birthday || gender != user_ProgressHistory.refGenderID || cusins != user_ProgressHistory.refCuiseseID || allegy != user_ProgressHistory.refAllergyInfoID || layaq != user_ProgressHistory.refLayaqaCondID {
         if txtName.text!.isEmpty{
            alert(mes:"يجب إدخال الاسم", selfUI: self)
        }else if txtdate.text!.isEmpty {
            alert(mes: "يجب تحديد تاريخ الميلاد", selfUI: self)
        }else if lblLayaCondition.text!.isEmpty {
            alert(mes: "يجب اختيار معدل اللياقة", selfUI: self)
        }else if lblAllaergy.text!.isEmpty{
            alert(mes: "يجب اختيار نوع الحساسية", selfUI: self)
        }else if lblCusine.text!.isEmpty{
            alert(mes:"يجب اختيار نوع المطبخ", selfUI: self)
        }else {
//        if cusins.isEmpty {cusins = "0"}
//        if allegy.isEmpty {allegy = "0"}
            if txtName.text != user_item.firstName {
                flagChangeName = true
            }else{
                flagChangeName = false
            }
            if  txtdate.text != user_ProgressHistory.birthday || gender != user_ProgressHistory.refGenderID || cusins != user_ProgressHistory.refCuiseseID || allegy != user_ProgressHistory.refAllergyInfoID || layaq != user_ProgressHistory.refLayaqaCondID {
                flagChangeName = false

            }
//            CheckInternet.checkIntenet { (bool) in
//                if !bool {
//                    SVProgressHUD.dismiss()
//                    showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
//                    return
//                }
//            }
            tblUserProgressHistory.updateInfoUser(birthday: FormatterTextField(str: txtdate).text!, refCuiseseID: cusins, refLayaqaCondID: layaq, refAllergyInfoID: allegy, gender: gender, date: getDateOnly()) { [self] (res, err) in
//            tblUserInfo.updateNameDatethUser(firstName: txtName.text!, birthday: FormatterTextField(str: txtdate).text!, refCuiseseID:cusins, refLayaqaCondID: layaq, refAllergyInfoID: allegy  ,gender:gender) { [self] (res, err) in
            if res == "success" {
//                tblAllergyUserItems.updateUserAllergy(id_User:self.user_ProgressHistory.refUserID,str: self.lblAllaergy.text!)
//                tblCuisnesUSerItems.updateUserCuisnes(id_User:self.user_ProgressHistory.refUserID,str: self.lblCusine.text!)
//                print("user ii \(setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!)")
//                _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,flag:"2")
                if flagChangeName {
                    tblUserInfo.updateNameUser(firstName: txtName.text!) { [self] (res, err) in
                        if res == "success" {
                            SVProgressHUD.dismiss {
                                showToast(message: "تم تحديث المعلومات الشخصية", view: self.view,place:0)
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                }
////                tblUserInfo.getMealDistrbutionUser()
////                tblUserInfo.updateMealPlanner { (s, e) in
//                SVProgressHUD.dismiss {
//                    showToast(message: "تم تحديث المعلومات الشخصية", view: self.view,place:0)
//                    self.dismiss(animated: true, completion: nil)
////                }
//                }
//                }else{
                    SVProgressHUD.dismiss {
                        showToast(message: "تم تحديث المعلومات الشخصية", view: self.view,place:0)
                        self.dismiss(animated: true, completion: nil)
                    }
//                }
            }else if res == "false"{
//                showToast(message: "نعتذر لم يتم تحديث المعلومات الشخصية", view: self.view,place:0)
                self.dismiss(animated: true, completion: nil)
            }else{
                showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
            }
            }
        }
    }else{
        self.dismiss(animated: true, completion: nil)

    }
    }
    
    
   
    @IBAction func btnDissmis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMale(_ sender: Any) {
        gender = "1"
        selectGender(Fbtn:btnMaleOutlet,Lbtn:btnFemaleOutlet)
    }
    
    @IBAction func btnFemale(_ sender: Any) {
        gender = "2"
        selectGender(Fbtn:btnFemaleOutlet,Lbtn:btnMaleOutlet)
    }
    
    
    func selectGender(Fbtn:UIButton,Lbtn:UIButton){
        UIView.animate(withDuration: 0.5, animations:{
            Fbtn.layer.cornerRadius = Fbtn.frame.height/2
            Fbtn.layer.borderWidth = 0.5
            Fbtn.layer.borderColor = colorGreen.cgColor
            Fbtn.backgroundColor = colorGreen
            Fbtn.setTitleColor(.white, for: .normal)
            
            Lbtn.layer.cornerRadius = Lbtn.frame.height/2
            Lbtn.layer.borderWidth = 0.5
            Lbtn.layer.borderColor = colorGreen.cgColor
            Lbtn.backgroundColor = .white
            Lbtn.setTitleColor(colorGreen, for: .normal)
            
        })
        
    }
    
    func clicableLabel(lbl:UILabel,tag:Int){
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lbl.tag = tag
        lbl.isUserInteractionEnabled = true
    }
    
    
}


extension PersonalInformationController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //  flag 1 >>> cusin
        // flag 2 >>> alleagy
        //flag 3 >> layaqa
        if let dis=segue.destination as? MultiSeletetionView {
            if  let dictionary=sender as? [String:String] {
                dis.title_Navigation = dictionary["title"]
                dis.obselectionDelegate = self
                dis.optionsSelected = dictionary["options"]
                dis.flag = dictionary["flag"]
            }
        }
    }
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 1 {
            
            self.performSegue(withIdentifier: "customSelectList", sender: ["title": "انواع المطابخ", "options": lblCusine.text,"flag":"1"])
        }else if tappedImageView.tag == 2 {
            self.performSegue(withIdentifier: "customSelectList", sender: ["title": "انواع الحساسيات", "options": lblAllaergy.text,"flag":"2"])
        }else{
            self.performSegue(withIdentifier: "customSelectList", sender: ["title": "اختر معدل  اللياقة", "options": lblLayaCondition.text,"flag":"3"])
        }
    }
    
    @objc func TimePicker(_ gestureRecognizer: UITapGestureRecognizer) {
        DPPickerManager.shared.showPicker(title: "الوقت", time: "", flag: 0, idTime: "-2", picker: { (picker) in
            picker.date = Date()
            self.customeDate(datePicker:picker)
            picker.datePickerMode = .date
        }) { (date, cancel) in
            if !cancel {
                self.selectDate(date: date!)
                //
            }
        }
    }
    @objc func TimePickerNotification(_ notification: Notification) {
        DPPickerManager.shared.showPicker(title: "الوقت", time: "", flag: 0, idTime: "-2", picker: { (picker) in
            picker.date = Date()
            self.customeDate(datePicker:picker)
            picker.datePickerMode = .date
        }) { (date, cancel) in
            if !cancel {
                self.selectDate(date: date!)
                //
            }
        }
    }
    func selectDate(date:Date){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        let myStringafd = formatter.string(from: yourDate!)
        ageChild = calclateAge(str: myString)
        txtdate.text = myStringafd
        ageChild = calclateAge(str: formatter.string(from: date))
        print("ageChild\(ageChild)")
        //            textLength.becomeFirstResponder()
    }
    func customeDate(datePicker:UIDatePicker){
        
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        
        if user_item.grown == "2" {
            datePicker.maximumDate = Calendar.current.date(byAdding:.year, value: -9, to: Date())
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value:  -13, to: Date())
            
        }else{
            var components = DateComponents()
            components.year = -14
            datePicker.maximumDate = calendar.date(byAdding: components, to: Date())!
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value:  -200, to: Date())
        }
        
    }
}


extension PersonalInformationController:selectionDelegate {
    
    func setPragnentMonths(str: String, id: String) {
        
    }
    func setMidctionDiabetsTypeOptionTwo(str: String, type: String) {
        
    }
    
    func setDiabetsType(str: String, typeDiabites: String) {
        
    }
    func setMidctionDiabetsTypeOptionOne(str: String, type: String) {
        
    }
    
    func setSeletedCusine(str:String,strID:String){
        if !str.isEmpty {
            lblCusine.text = str
            saveCusine(strId:strID)
            lblCusine.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblCusine.font = UIFont(name: "GE Dinar One",size: 17)
            lblCusine.text = "اختر نوع المطبخ-اختيار متعدد"
            saveCusine(strId:strID)
        }
    }
    
    func setSeletedallaegy(str:String,strID:String){
        if !str.isEmpty {
            lblAllaergy.text = str
            saveAllergy(strId:strID)
            lblAllaergy.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblAllaergy.font = UIFont(name: "GE Dinar One",size: 17)
            lblAllaergy.text = "أختر حساسية الطعام-اختيار متعدد"
            saveAllergy(strId:strID)
        }
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        if !str.isEmpty {
            lblLayaCondition.text = str
            saveLayaqa(strId:strID)
            idFetnessRate = Int(strID)
            lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblLayaCondition.font = UIFont(name: "GE Dinar One",size: 17)
            lblLayaCondition.text = "اختر معدل اللياقة"
            saveLayaqa(strId:strID)
            idFetnessRate = 0
        }
    }
}



