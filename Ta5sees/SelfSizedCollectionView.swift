//
//  SelfSizedCollectionView.swift
//  Ta5sees
//
//  Created by Admin on 2/6/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class SelfSizedCollectionView: UICollectionView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
    super.reloadData()
    self.invalidateIntrinsicContentSize()
    self.layoutIfNeeded()
  }
  
//  override var intrinsicContentSize: CGSize {
//    let height = min(contentSize.height, maxHeight)
//    return CGSize(width: contentSize.width, height: height)
//  }
    override var contentSize:CGSize {
          didSet {
              invalidateIntrinsicContentSize()
          }
      }
      override var intrinsicContentSize: CGSize {
          layoutIfNeeded()
          return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height+20)
      }
}
