//
//  NavigationViewController.swift
//  news_Barsh4Arabs
//
//  Created by Admin on 2/5/1398 AP.
//  Copyright © 1398 Telecom Enterprise. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationBar {
    let gradient = CAGradientLayer()
    
    @IBInspectable var firstColor: UIColor = UIColor.clear {
    didSet {
    updateView()
    }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
    didSet {
    updateView()
    }
    }
    @IBInspectable var isHorizontal: Bool = true {
    didSet {
    updateView()
    }
    }
    
    func updateView() {
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        
        gradient.frame = defaultNavigationBarFrame
        
        gradient.colors = [UIColor(red: 98/255, green: 230/255, blue: 155/255, alpha: 1.0) ,UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0) ].map {$0.cgColor}
        
        UINavigationBar.appearance().setBackgroundImage(self.image(fromLayer: gradient), for: .default)
    }
    


    
    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return outputImage!
    }
}
