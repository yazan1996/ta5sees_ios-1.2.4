
//  TTTTTTViewController.swift
//  Ta5sees
//
//  Created by Admin on 9/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
//import fluid_slider

 
class SpecialDiet: UIViewController,UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var viewSeekbar: UIView!
    @IBOutlet weak var stackSeekBar: UIStackView!
    @IBOutlet weak var stackSpiecalDiet: UIStackView!
    @IBOutlet weak var stackbarview: UIView!
    @IBOutlet weak var stackFemaleDiet: UIStackView!
    @IBOutlet weak var v8: viewColorsWithRaduis!
    @IBOutlet weak var v7: viewColorsWithRaduis!
    @IBOutlet weak var v5: viewColorsWithRaduis!
    @IBOutlet weak var v4: viewColorsWithRaduis!
    @IBOutlet weak var v3: viewColorsWithRaduis!
    @IBOutlet weak var v2: viewColorsWithRaduis!
    @IBOutlet weak var v1: viewColorsWithRaduis!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var imgbloodPr: UIImageView!
    
    @IBOutlet weak var txtDiabets: UILabel!
    @IBOutlet weak var txtprusser: UILabel!
    @IBOutlet weak var txtibs: UILabel!
    @IBOutlet weak var txtchoestrol: UILabel!
    @IBOutlet weak var txtgout: UILabel!
    @IBOutlet weak var txtlacting: UILabel!
    @IBOutlet weak var txtpragning: UILabel!
    
//    let slider = Slider()
    var idDiet:Int!
    let weight =  getDataFromSheardPreferanceFloat(key: "txtweight")
    var pregnant:UILabel!
    var Lactating:UILabel!
    var pregnantImg:UIImageView!
    var LactatingImg:UIImageView!
//    var sheard = SliderTarget()

    var arrayDiet = tblPlanMaster.sheard.getSpicalDiets()
    override func viewDidLoad() {
        super.viewDidLoad()
        setImage()
        HideSeekbar(flag: true)
//        sheard.slideSetup(flag:"nil",view:viewSeekbar,flagType:1,viewMain:view)
        print( tblPlanMaster.sheard.getSpicalDiets())
        let firstView32 = stackSeekBar.arrangedSubviews[0]
        firstView32.isHidden = true
        
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            let firstView3 = stackFemaleDiet.arrangedSubviews[2]
            firstView3.isHidden = true
        }
        
        //        self.view.layoutIfNeeded()
        //        scrollview.translatesAutoresizingMaskIntoConstraints = false
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var btnNextOutlet: ButtonColorRaduis!
    @IBOutlet weak var stackTypDietSlider: UIStackView!
    
    
    @IBAction func btnNext(_ sender: Any) {
        
        if getDataFromSheardPreferanceFloat(key: "dateInYears") > 16 {
            var flag:String!
            let num = Float(getDataFromSheardPreferanceString(key: "targetValue"))!
            if num > weight {
                flag = "2"
                setDataInSheardPreferance(value:"gain",key:"subDiet")
                setDataInSheardPreferance(value: "4", key: "fatInfo")
            }else  if num < weight {
                flag = "1"
                setDataInSheardPreferance(value:"loss",key:"subDiet")
                setDataInSheardPreferance(value: "2", key: "fatInfo")
            }else  if num == weight {
                flag = "3"
                setDataInSheardPreferance(value:"maintain",key:"subDiet")
                setDataInSheardPreferance(value: "3", key: "fatInfo")
                
            }
            
            if flag !=  BMIAdult(){
                if  BMIAdult() == "1" {
                    showAlert(str:"نقترح عليك ان تقوم بتخسيس وزنك")
                }else if BMIAdult() == "2" {
                    showAlert(str:"نقترح عليك ان تقوم بزيادة وزنك")
                }else if BMIAdult() == "3" {
                    showAlert(str:"نقترح عليك ان تقوم بالمحافظة على وزنك")
                }
            }else{
                goToPageNext()
            }
            
        }else {
            goToPageNext()
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        setBackground()
        //        setupLable()
        
    }
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        
        let tappedImageView = gestureRecognizer.view!
        print(tappedImageView.tag)
        if tappedImageView.tag == arrayDiet[0].id  || tappedImageView.tag == arrayDiet[1].id  || tappedImageView.tag == arrayDiet[3].id {
            
            scrollview.scrollRectToVisible(CGRect(x:0, y:view.frame.width/3,width: view.frame.width,height: view.frame.height), animated: true)
        }else {
            scrollview.scrollRectToVisible(CGRect(x:0, y:view.frame.height,width: view.frame.width,height: view.frame.height), animated: true)
        }
        
        if tappedImageView.tag == arrayDiet[4].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            selectDiet(v1:v2,v2:v1,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v7,v8:v8)
//            if slider.isHidden { HideSeekbar(flag:false)}
        }else if tappedImageView.tag == arrayDiet[4].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "2"  {
            selectDiet(v1:v2,v2:v1,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v7,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}

        }else if tappedImageView.tag == arrayDiet[0].id  {
            selectDiet(v1:v8,v2:v2,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v7,v8:v1)
//            if !slider.isHidden { HideSeekbar(flag:true)}

        }else if tappedImageView.tag == arrayDiet[2].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  {
            selectDiet(v1:v5,v2:v1,v3:v3,
                       viewDiabets:v4,v5:v2,
                       v7:v7,v8:v8)
//            if slider.isHidden { HideSeekbar(flag:false)}
        }else if tappedImageView.tag == arrayDiet[2].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "2"  {
            selectDiet(v1:v5,v2:v1,v3:v3,
                       viewDiabets:v4,v5:v2,
                       v7:v7,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}
        }else if tappedImageView.tag == arrayDiet[5].id   && getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  {
            selectDiet(v1:v3,v2:v2,v3:v4,
                       viewDiabets:v1,v5:v5,
                       v7:v7,v8:v8)
//            if slider.isHidden { HideSeekbar(flag:false)}
        }else if tappedImageView.tag == arrayDiet[5].id   && getDataFromSheardPreferanceString(key: "UserAgeID") == "2"   {
            selectDiet(v1:v3,v2:v2,v3:v4,
                       viewDiabets:v1,v5:v5,
                       v7:v7,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}

        }
            //        else if tappedImageView.tag == 9 {
            //
            //            selectDiet(v1:v6,v2:v2,v3:v3,
            //                       v4:v4,v5:v5,v6:v1,
            //                       v7:v7,v8:v8)
            //
            //            HideSeekbar(flag:true)
            //        }
        else if tappedImageView.tag == arrayDiet[1].id   {
            selectDiet(v1:v7,v2:v2,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v1,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}

        }else if tappedImageView.tag == arrayDiet[3].id  {
            selectDiet(v1:v4,v2:v2,v3:v1,
                       viewDiabets:v5,v5:v3,
                       v7:v7,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}
        }else if tappedImageView.tag == arrayDiet[7].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  {
            selectDiet(v1:v1,v2:v2,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v7,v8:v8)
//            if slider.isHidden { HideSeekbar(flag:false)}
        }
        else if tappedImageView.tag == arrayDiet[7].id  && getDataFromSheardPreferanceString(key: "UserAgeID") == "2"  {
            selectDiet(v1:v1,v2:v2,v3:v3,
                       viewDiabets:v4,v5:v5,
                       v7:v7,v8:v8)
//            if !slider.isHidden { HideSeekbar(flag:true)}
        }
        idDiet = tappedImageView.tag
        setDataInSheardPreferance(value:String(tappedImageView.tag),key:"dietType")
    }
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 77 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func HideSeekbar(flag:Bool){
        
        UIView.animate(withDuration: 0.5, animations:{
            
            let stack1 = self.stackTypDietSlider.arrangedSubviews[1]
            stack1.isHidden = flag
            let stack2 = self.stackSeekBar.arrangedSubviews[0]
            stack2.isHidden = flag
//            self.slider.isHidden = flag
            
            self.view.layoutIfNeeded()
        })
        
    }
    
    
    func selectDiet(v1:viewColorsWithRaduis,v2:viewColorsWithRaduis,v3:viewColorsWithRaduis,
                    viewDiabets:viewColorsWithRaduis,v5:viewColorsWithRaduis,
                    v7:viewColorsWithRaduis,v8:viewColorsWithRaduis){
        v1.backgroundColor = UIColor(red: 156, green: 171, blue: 179, alpha: 1.0)
        v3.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        v2.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        viewDiabets.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        v5.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        v7.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        v8.backgroundColor = UIColor(red: 255,green: 255,blue: 255, alpha: 1.0)
        
    }
    
    
    func createStorybord(){
        
    }
    
    func goToPageNext(){
        if idDiet != nil {
            
            if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[3].id){
                let detailView = self.storyboard!.instantiateViewController(withIdentifier: "regesterStep10") as! FormateDiabetsDeit
                
                detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                detailView.transitioningDelegate = self
                self.present(detailView, animated: true, completion: nil)
                //            self.performSegue(withIdentifier: "dietType", sender: nil)
                self.view.snapshotView(afterScreenUpdates: true)
                
            }else {
                if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[2].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Heypertention") as! Heypertention
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "Heypertention", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }else if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[4].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Cholestrole") as! Cholestrole
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "Cholestrole", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }else if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[7].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "IBS") as! IBS
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "IBS", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }else if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[5].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Gout") as! Gout
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "Gout", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }else if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[1].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Lactation") as! Lactation
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "Lactation", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }else if getDataFromSheardPreferanceString(key: "dietType") == String(arrayDiet[0].id){
                    let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Pregnant") as! Pregnant
                    detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                    detailView.transitioningDelegate = self
                    self.present(detailView, animated: true, completion: nil)
                    //                self.performSegue(withIdentifier: "Pregnant", sender: nil)
                    self.view.snapshotView(afterScreenUpdates: true)
                }
            }
        }else{
            alert(mes:"يجب اختيار نوع الحمية", selfUI: self)
        }
    }
    
    func showAlert(str:String){
        let alert = UIAlertController(title: "نصيحة", message:str, preferredStyle: .alert)
        
        
        let done = UIAlertAction(title: "تخطي", style: .default, handler: { action in
            
            self.goToPageNext()
        })
        done.setValue(UIColor.black, forKey: "titleTextColor")
        
        alert.addAction(done)
        
        self.present(alert, animated: true)
    }
    
    
    
    func BMIAdult()->String{
        
        if getDataFromSheardPreferanceFloat(key: "bmi") <= 18.5{
            return "2"
        }else if getDataFromSheardPreferanceFloat(key: "bmi") > 18.5 &&  getDataFromSheardPreferanceFloat(key: "bmi") <= 24.9{
            return "3"
        }else if getDataFromSheardPreferanceFloat(key: "bmi") >= 25.0 {
            return "1"
        }
        return "0"
    }
    
    
    
    func setImage(){
        
        v4.tag = arrayDiet[3].id
        txtDiabets.text = arrayDiet[3].discription
        v4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v4.isUserInteractionEnabled = true
        
        v5.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v5.isUserInteractionEnabled = true
        v5.tag = arrayDiet[2].id
        txtprusser.text = arrayDiet[2].discription
        
        v1.tag = arrayDiet[7].id
        txtibs.text = arrayDiet[7].discription
        v1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v1.isUserInteractionEnabled = true
        //
        //        let Kidney = UIImageView(frame: CGRect(x: 259.39, y: 498.53, width: 65.21, height: 48.71))
        //        Kidney.image = UIImage(named: "renal")
        //        Kidney.tag = 9
        //        Kidney.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        //        Kidney.isUserInteractionEnabled = true
        //        self.subView.addSubview(Kidney)
        
        v2.tag = arrayDiet[4].id
        txtchoestrol.text = arrayDiet[4].discription
        v2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v2.isUserInteractionEnabled = true
        
        v3.tag = arrayDiet[5].id
        txtgout.text = arrayDiet[5].discription
        v3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v3.isUserInteractionEnabled = true
        
        v8.tag = arrayDiet[0].id
        txtpragning.text = arrayDiet[0].discription
        v8.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v8.isUserInteractionEnabled = true
        
        v7.tag = arrayDiet[1].id
        txtlacting.text = arrayDiet[1].discription
        v7.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        v7.isUserInteractionEnabled = true
        
    }
}


