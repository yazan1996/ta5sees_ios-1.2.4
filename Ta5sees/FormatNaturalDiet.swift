//
//  newRegisterStep8.swift
//  Ta5sees
//
//  Created by Admin on 8/22/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON
import Firebase
import FirebaseAuth
import SVProgressHUD
import FirebaseAnalytics

class FormatNaturalDiet: UIViewController,UIViewControllerTransitioningDelegate {
    
    
    @IBOutlet weak var lblLayaCondition: UILabel!
    @IBOutlet weak var lblAllaergy: UILabel!
    @IBOutlet weak var lblCusine: UILabel!
    
    lazy var obAPiSubscribeUser = APiSubscribeUser(with: self)
    
    let header = HTTPHeaders()
    var tee:Float!
    var scenarioStep = "0" // 1-> updateDietType else registration
    var idFetnessRate:Int!
    var incrementKACL:Float = Float(getDataFromSheardPreferanceString(key: "incrementKACL"))!
    var energyTotal = 0
    var flagNotfy = false
    let ageChild = getDataFromSheardPreferanceFloat(key: "dateInYears")
    let weight = getDataFromSheardPreferanceString(key: "txtweight")
    let gender = getDataFromSheardPreferanceString(key: "gender")
    let height = getDataFromSheardPreferanceString(key: "txtheight")
    var dietType =  getDataFromSheardPreferanceString(key: "dietType")
    var obselectionDelegate:selectionDelegate!
    //    lazy var obPresnter = FormatNaturalPresenter(with : self)
    //    lazy var  obAPiSubscribeUser = APciSubscribeUser(with: self)
    //copyDietType
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 1 {
            
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "انواع المطابخ", "options": lblCusine.text,"flag":"1"])
        }else if tappedImageView.tag == 2 {
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "انواع الحساسيات", "options": lblAllaergy.text,"flag":"2"])
        }else{
            self.performSegue(withIdentifier: "selectioncusine", sender: ["title": "اختر معدل  اللياقة", "options": lblLayaCondition.text,"flag":"3"])
        }
    }
    
    
    //    @IBOutlet weak var titlrPage: UILabel!
    //    @IBOutlet weak var imageDiet: UIImageView!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //  flag 1 >>> cusin
        // flag 2 >>> alleagy
        //flag 3 >> layaqa
        if segue.identifier == "SourcesAppController" {
            if let dis=segue.destination as?  SourcesAppController{
                if  sender != nil  {
                    dis.flagNotfy = flagNotfy
                }
            }
        }else if let dis=segue.destination as? MultiSeletetionView {
            if  let dictionary=sender as? [String:String] {
                dis.title_Navigation = dictionary["title"]
                dis.obselectionDelegate = self
                dis.optionsSelected = dictionary["options"]
                dis.flag = dictionary["flag"]
            }
        }
        
    }
    
    func clicableLabel(lbl:UILabel,tag:Int){
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lbl.tag = tag
        lbl.isUserInteractionEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblLayaCondition.font = UIFont(name: "GE Dinar One",size: 17)
        
        if scenarioStep == "1" {
            lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
            lblCusine.font = UIFont(name: "GEDinarOne-Bold",size: 17)
            lblAllaergy.font = UIFont(name: "GEDinarOne-Bold",size: 17)
            
            idFetnessRate = Int(getUserInfoProgressHistory().refLayaqaCondID)!//Int(getDataFromSheardPreferanceString(key: "refLayaqaCondID"))!
            dietType = getDataFromSheardPreferanceString(key: "copyDietType")
            lblAllaergy.text = tblAllergyUserItems.getDataSelectedProgressHistory()
            lblCusine.text = tblCuisnesUSerItems.getDataSelectedProgressHistory()
            lblLayaCondition.text = tblLayaqaCond.getItem(refID: Int(getUserInfoProgressHistory().refLayaqaCondID)!)
        }
        clicableLabel(lbl:lblCusine,tag:1)
        clicableLabel(lbl:lblAllaergy,tag:2)
        clicableLabel(lbl:lblLayaCondition,tag:3)
        obselectionDelegate = self
        
    }
    
    
    @IBOutlet weak var switchNotifOutlet: UISwitch!
    @IBAction func switchNotify(_ sender: UISwitch) {
        if (sender.isOn == true){
            flagNotfy = true
        }else{
            flagNotfy = false
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        SVProgressHUD.show()
        if lblLayaCondition.text! == "اختر معدل اللياقة"{
            showAlert(str:"يجب اختيار معدل اللياقة",view: self)
            SVProgressHUD.dismiss()
        }else if lblAllaergy.text! == "أختر حساسية الطعام-اختيار متعدد"{
            showAlert(str:"يجب اختيار نوع الحساسية لديك",view: self)
            SVProgressHUD.dismiss()
        }else if lblCusine.text! == "اختر نوع المطبخ-اختيار متعدد"{
            showAlert(str:"يجب اختيار نوع المطبخ",view: self)
            SVProgressHUD.dismiss()
        }else {
            if getDataFromSheardPreferanceString(key: "UserAgeID") == "2"{
                calculateNormalDietChild()
                SVProgressHUD.dismiss()
                print("Child")
            }else{
                calculateNormalDietAdult()
                SVProgressHUD.dismiss()
                print("Adult")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if setupRealm().isInWriteTransaction {
            setupRealm().cancelWrite()
        }
        //        if getDataFromSheardPreferanceString(key: "dietType") == "3" || getDataFromSheardPreferanceFloat(key: "dateInYears")  <= 18.0 || getDataFromSheardPreferanceString(key: "dietType") == "19" || getDataFromSheardPreferanceString(key: "dietType") == "4" {
        //            HideSeekbar(flag:true)
        //        }
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    func delegetGenderType(){
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "GeneralInformationTest") as! TextInputViewController
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
    }
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
    
    
    
    func calculateNormalDietAdult(){
        var bmr:Float
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            
            bmr = Float(24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }  else{
            bmr = Float(0.9 * 24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }
        
        let pa:Float = Float(getRatePA(id:idFetnessRate)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        if getDataFromSheardPreferanceString(key: "gender") == "2" && dietType == "4" || dietType == "5" {
            if getDataFromSheardPreferanceString(key: "LactationMonths") == "1"  && dietType == "5"{
                energyTotal = Int(round(tee + 330))
            }
            else if getDataFromSheardPreferanceString(key: "LactationMonths") == "2" && dietType == "5"{
                energyTotal = Int(round(tee + 440))
            } else if getDataFromSheardPreferanceString(key: "Pregnant") == "3" && dietType == "4"{
                energyTotal = Int(round(tee  + 452))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "2"  && dietType == "4"{
                energyTotal = Int(round(tee  + 350))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "1" && dietType == "4" {
                energyTotal = Int(round(tee))
                
            }else {
                energyTotal = Int(round(tee))
                
            }
        }else {
            energyTotal = processDietOrderForTEEAdulte(tee:tee, incrementKACL: incrementKACL)
            
        }
        setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
        setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
        setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(dietType)!)
        
    }
    
    func calculateNormalDietChild(){
        if ageChild >= 1.0 && ageChild <= 3.0{
            print("****0")
            let sub1 = (89.0 * (weight as NSString).floatValue - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= 4.0 && ageChild <= 8.0 && gender == "1" {
            print("****1")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= 4.0 && ageChild <= 8.0 && ageChild <= tblChildAges.range2EndTo(id:2)  && gender == "2" {
            print("****2")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= 9  && ageChild <= 16 && gender == "1" {
            print("****3")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
            
        }else if ageChild >= 9  && ageChild <= 16 && ageChild <= tblChildAges.range3EndTo(id:3)  && gender == "2"{
            print("****4")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }else {
            print("****5")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }
        
        //        if getDataFromSheardPreferanceString(key: "gender") == "2" && getDataFromSheardPreferanceString(key: "dietType") == "4" || getDataFromSheardPreferanceString(key: "dietType") == "5" {
        //            if getDataFromSheardPreferanceString(key: "LactationMonths") == "1"  && getDataFromSheardPreferanceString(key: "dietType") == "5"{
        //                energyTotal = Int(round(tee + 330))
        //            }
        //            else if getDataFromSheardPreferanceString(key: "LactationMonths") == "2" && getDataFromSheardPreferanceString(key: "dietType") == "5"{
        //                energyTotal = Int(round(tee + 440))
        //            } else if getDataFromSheardPreferanceString(key: "Pregnant") == "3" && getDataFromSheardPreferanceString(key: "dietType") == "4"{
        //                energyTotal = Int(round(tee  + 452))
        //            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "2"  && getDataFromSheardPreferanceString(key: "dietType") == "4"{
        //                energyTotal = Int(round(tee  + 350))
        //            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "1" && getDataFromSheardPreferanceString(key: "dietType") == "4" {
        //                energyTotal = Int(round(tee))
        //
        //            }else {
        //                energyTotal = Int(round(tee))
        //
        //            }
        //        }else {
        energyTotal = processDietOrderForTEEChild(tee:tee)
        print("tee \(tee ?? 0.0) - energyTotal \(energyTotal)")
        
        //        }
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(dietType)!)
    }
    
    
    
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let age = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let sum1 = v2 * age
        let sum2 = (v3 * (weight as NSString).floatValue)
        let sum5 = (((height as NSString).floatValue/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(getRatePA(id:idFetnessRate)) * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        print(sum4)
        return sum4
    }
    
    
    
    
    func processDietOrderForTEEAdulte(tee:Float,incrementKACL:Float)->Int{
        if dietType == "2"  {
            return Int(round(tee - incrementKACL))
        }else if dietType == "1"   {
            return Int(round(tee + incrementKACL))
        }else if dietType == "3"   {
            return Int(round(tee))
        }else if dietType == "loss" {
            print(Int(round(tee - incrementKACL)))
            return Int(round(tee - incrementKACL))
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "gain" {
            return Int(round(tee + incrementKACL))
            
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "maintain" {
            return Int(round(tee))
            
        }
        return 1
    }
    
    
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let bmi = getDataFromSheardPreferanceFloat(key: "bmi")
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: bmi ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        //else if ob.value >= 25 && ob.value <= 25   {
        //        return Int(round(tee + 500))
        //    }
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value <= 25 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = String(Int(getDataFromSheardPreferanceFloat(key: "gender")))
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  { // adulte
            return Float(ob.PAadult)
        } else{
            if gender == "1"  { // childe male
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
        }
    }
    
    
    func getservMenuPlan(id:Int){
        
        var ob = tblMenuPlanExchangeSystem()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }
        
        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        
        
        //        _ = CalculationServing(malik: m, fruit: f, vg: v, fat: Int(getDataFromSheardPreferanceString(key: "fat"))!, pro: Int(getDataFromSheardPreferanceString(key: "pro"))!, carb: Int(getDataFromSheardPreferanceString(key: "carbo"))!, energy: energy,dietType:getDataFromSheardPreferanceString(key: "dietType"), FBG: getDataFromSheardPreferanceFloat(key: "FBG"))
        
        if flagNotfy {
            runNotfyWajbeh() // else run notfy 2 days as default
        }else{
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["1","2","3"])
            tblAlermICateItems.DeleteAll()
        }
        
        if scenarioStep == "1" {
            createEvent(key: "16",date: getDateTime())
            setDataInSheardPreferance(value:getDataFromSheardPreferanceString(key: "copyDietType"), key: "dietType")
            sntDataToFirebase()
        }else{
            if getDataFromSheardPreferanceString(key: "loginType") == "1" {
                createEvent(key: "55", date: getDateTime())
            }else if getDataFromSheardPreferanceString(key: "loginType") == "2" {
                createEvent(key: "39", date: getDateTime())
            }else if getDataFromSheardPreferanceString(key: "loginType") == "3" {
                createEvent(key: "44", date: getDateTime())
            }else if getDataFromSheardPreferanceString(key: "loginType") == "4" {
                createEvent(key: "49", date: getDateTime())
            }
            self.performSegue(withIdentifier: "SourcesAppController", sender: self) //sub1
        }
        
        
    }
//    func updateMealPlanner(){
//        var filterList = [tblWajbatUserMealPlanner]()
//        var filteringPackge = [tblShoppingList]()
//        var days:Int!
//        var expirDays:Int!
//        var first7Days:Int!
//        print("@3")
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        if getUserInfo().subscribeType == -1  ||  getUserInfo().subscribeType == 3 ||  getUserInfo().subscribeType == 2 { // غير مشترك
//            print("@4")
//
//            self.obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 1) { [self] (bool) in
//                if bool {
//
////                    SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
////                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
////                        tblPackeg.updateMealPlannerFree7Days(date:getDateOnly()) { [self] (err) in
////                            SVProgressHUD.dismiss {
////
//                                self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
//                                ActivationModel.sharedInstance.dispose()
////                            }
////                        }
////                    }
//                }else{
//                    Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
//                    SVProgressHUD.dismiss()
//                }
//            }
//            //            ActivationModel.sharedInstance.subscribeType = 0 // free
//            //            ActivationModel.sharedInstance.startSubMilli = 0 // free
//            //            ActivationModel.sharedInstance.endSubMilli = 7 // free
//            //
//            //            ActivationModel.sharedInstance.startPeriod = 0
//            //            ActivationModel.sharedInstance.endPeriod = 7
////            self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
//            return
//        }else {
//            if getUserInfo().subscribeType == 1 {
//                ActivationModel.sharedInstance.subscribeType = 1 // buy
//            }else if getUserInfo().subscribeType == 4 {
//                ActivationModel.sharedInstance.subscribeType = 4 // grace period
//            }else{
//                ActivationModel.sharedInstance.subscribeType = 0 // free
//            }
//
//
//
//            filterList = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).filter { (item) -> Bool in
//                if dateFormatter.date(from: item.date)!.timeIntervalSince1970 >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
//                    return true
//                }
//                return false
//            }
//
//            filteringPackge = setupRealm().objects(tblShoppingList.self).filter("userID == %@",getUserInfo().id).filter { (item) -> Bool in
//                if  item.date >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
//                    return true
//                }
//                return false
//            }
//
//            print(filterList,"filterList",filterList.map {$0.date}.removeDuplicates().count)
//
//
//            let dateUser = Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000)
//            let startMili = Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)
//            expirDays = Date().daysBetween(start: getDateOnlyasDate(), end: startMili)
//            days = Date().daysBetween(start: getDateOnlyasDate(), end: dateUser)
//            print("days",days,Date().getDaysDate(value: days),getUserInfo().endSubMilli)
//
//            ActivationModel.sharedInstance.startSubMilli = 0
//
//            if days == 0 {
//                ActivationModel.sharedInstance.startPeriod = 0
//
//            }else{
//                ActivationModel.sharedInstance.startPeriod = days-(filterList.map {$0.date}.removeDuplicates().count-1)
//            }
//
//            if  ActivationModel.sharedInstance.startPeriod >= 30 {
//                ActivationModel.sharedInstance.startPeriod = 0
//            }
//            ActivationModel.sharedInstance.endPeriod =  days-1
//            ActivationModel.sharedInstance.endSubMilli = days
//
//            ActivationModel.sharedInstance.expireDate = Double(getUserInfo().expirDateSubscribtion) ?? 0.0
//
//
//
//        }
//        self.obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 1) { [self] (bool) in
//            if bool {
//                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
//
//                let queue = DispatchQueue(label: "com.appcoda.myqueue")
//                queue.async {
//
//                    if days-expirDays >= 7 {
//                        first7Days = 6
//                    }else{
//                                                first7Days = days+abs(expirDays)

//                    }
//                    tblPackeg.GenerateMealPlannerFree7Days1(startPeriod:expirDays,endPeriod:  first7Days,date:getDateOnly())
//                    SVProgressHUD.dismiss {
//                        try! setupRealm().write{
//                            if !filterList.isEmpty{
//                                setupRealm().delete(filterList)
//                            }
//                        }
//                        try! setupRealm().write{
//                            if !filteringPackge.isEmpty{
//                                setupRealm().delete(filteringPackge)
//                            }
//                        }
//                        self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
//                        //                            ActivationModel.sharedInstance.dispose()
//                    }
//                }
//                queue.async {
//                    if getUserInfo().subscribeType != 0 {
//                        if first7Days != days {
//
//                            tblPackeg.GenerateMealPlannerFree7Days1(startPeriod:first7Days+1,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())
//                        }
//                    }
//                    ActivationModel.sharedInstance.dispose()
//                }
////                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
////                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
////                    tblPackeg.updateMealPlannerFree7Days(date:getDateOnly()) { [self] (err) in
////                        SVProgressHUD.dismiss {
////                            try! setupRealm().write{
////                                if !filterList.isEmpty{
////                                    setupRealm().delete(filterList)
////                                }
////                            }
////                            try! setupRealm().write{
////                                if !filteringPackge.isEmpty{
////                                    setupRealm().delete(filteringPackge)
////                                }
////                            }
////                            self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
////                            ActivationModel.sharedInstance.dispose()
////                        }
////                    }
////                }
//            }else{
//                Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
//                SVProgressHUD.dismiss()
//            }
//        }
//    }
    func getMealDistrbutionUser(){

        let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
        if item?.isLunchMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "2"
        }else if item?.isDinnerMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "3"
        }else if item?.isBreakfastMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "1"
        }
        if item?.Breakfast == 1 && item?.isBreakfastMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("1")
        }
        if item?.Dinner == 1 && item?.isDinnerMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("3")
        }
        if item?.Lunch == 1 && item?.isLunchMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("2")
        }
        if item?.Snack1 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("4")
        }
        if item?.Snack2 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("5")
        }
        ActivationModel.sharedInstance.subscribeType =  getUserInfo().subscribeType
        ActivationModel.sharedInstance.expireDate = Double(getUserInfo().expirDateSubscribtion)!

        ActivationModel.sharedInstance.startSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Int(getUserInfo().startSubMilli) / 1000)))

        ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//
        ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//Array(arrayLiteral: getUserInfo().refAllergyInfoID)

        ActivationModel.sharedInstance.endSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Int(getUserInfo().endSubMilli) / 1000)))

        ActivationModel.sharedInstance.refMeat = getUserInfo().meat.split(separator: ",").map { String($0) }
        ActivationModel.sharedInstance.refFruit = getUserInfo().frute.split(separator: ",").map { String($0) }
        ActivationModel.sharedInstance.refVegetables = getUserInfo().vegetabels.split(separator: ",").map { String($0) }


    }
    func sntDataToFirebase(){
        getMealDistrbutionUser()
        
        SVProgressHUD.show()
        self.obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 1) { [self] (bool) in
            if bool {
            self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
                SVProgressHUD.dismiss()
            }else{
                Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
                SVProgressHUD.dismiss()
            }
        }
//        getMealDistrbutionUser()
        
        //        self.obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 0) { [self] (bool) in
        //        if bool {
        //                if getUserInfo().subscribeType != -1 {
        //                    print("@1")
//        updateMealPlanner()
        //                }else{
        //                    print("@2")
        //                    self.performSegue(withIdentifier: "formatechangtypediet", sender: self) //sub1
        //                }
        //            }else{
        //                Ta5sees.alert(mes: "لم يتم تغيير نوع الحمية،حاول لاحقا", selfUI: self)
        //                SVProgressHUD.dismiss()
        //            }
        //        }
    }
    func getTeeDistribution(energy:Int){
        var ref = Int(dietType)
        
        if getDataFromSheardPreferanceString(key: "cusinene") == "8" {
            ref = 8
        }
     
        var ob = tblTEEDistribution()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" { // adulte
            ob.getTeeDistrbution(refID: ref!,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{ // childe
            ob.getTeeDistrbution(refID: ref!,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
    }
    
    
    
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)
        //        setDataInSheardPreferance(value: "CC", key: String(c/15))
        //        setDataInSheardPreferance(value: "KCALTo1CC", key: String(energy/(c/15)))
        
        
        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        sheardPreferanceWrite(fat: Float(fat),pro: Float((pro)),carbo: Float(carbo),energy: Float(energy))
        
    }
    
    
    func alert(mess:String){
        let alert = UIAlertController(title: "خطأ", message: mess, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


//extension FormatNaturalDiet:FormatNaturalViewDelegate {
//
//
//    func setHideSeekbar(flag:Bool) {
//         HideSeekbar(flag:flag)
//    }
//
//}


 
extension FormatNaturalDiet:selectionDelegate {
    
    func setPragnentMonths(str: String, id: String) {}
    func setMidctionDiabetsTypeOptionTwo(str: String, type: String) {}
    func setDiabetsType(str: String, typeDiabites: String) {}
    func setMidctionDiabetsTypeOptionOne(str: String, type: String) {}
    
    func setSeletedCusine(str:String,strID:String){
        if !str.isEmpty {
            lblCusine.text = str
            saveCusine(strId:strID)
            lblCusine.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblCusine.font = UIFont(name: "GE Dinar One",size: 17)
            lblCusine.text = "اختر نوع المطبخ-اختيار متعدد"
            saveCusine(strId:strID)
        }
    }
    
    func setSeletedallaegy(str:String,strID:String){
        if !str.isEmpty {
            lblAllaergy.text = str
            saveAllergy(strId:strID)
            lblAllaergy.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblLayaCondition.font = UIFont(name: "GE Dinar One",size: 17)
            lblAllaergy.text = "أختر حساسية الطعام-اختيار متعدد"
            saveAllergy(strId:strID)
        }
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        if !str.isEmpty {
            lblLayaCondition.text = str
            saveLayaqa(strId:strID)
            idFetnessRate = Int(strID)
            lblLayaCondition.font = UIFont(name: "GEDinarOne-Bold",size: 17)
        }else{
            lblLayaCondition.font = UIFont(name: "GE Dinar One",size: 17)
            lblLayaCondition.text = "اختر معدل اللياقة"
            saveLayaqa(strId:strID)
            idFetnessRate = 0
        }
    }
    
}


protocol selectionDelegate {
    func setSeletedCusine(str:String,strID:String)
    func setSeletedallaegy(str:String,strID:String)
    func setSeletedLayaqa(str:String,strID:String)
    func setDiabetsType(str:String,typeDiabites:String)
    func setMidctionDiabetsTypeOptionOne(str:String,type:String)
    func setMidctionDiabetsTypeOptionTwo(str:String,type:String)
    func setPragnentMonths(str:String,id:String)
    
    
}



//extension FormatNaturalDiet:ApiResponseDaelegat{
//  func showIndecator() {
//
//      ANLoader.showLoading(" يرجى الانتظار قليلا ....", disableUI: true)
//      ANLoader.showLoading()
//  }
//
//  func HideIndicator() {
//      ANLoader.hide()
//  }
//
//  func getResponseSuccuss() {
//
//  }
//
//  func getResponseFalueir(meeage:String) {
//      let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
//      alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
//      self.present(alert, animated: true, completion: nil)
//
//  }
//
//  func getResponseMissigData() {
//
//  }
//
//
//
//}
 
extension FormatNaturalDiet:ApiResponseDaelegat{
    func showIndecator() {
        
        SVProgressHUD.show()
    }
    
    func HideIndicator() {
        SVProgressHUD.dismiss()
    }
    
    func getResponseSuccuss() {
        
    }
    
    func getResponseFalueir(meeage:String) {
        let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getResponseMissigData() {
        
    }
    
    func runNotfyWajbeh(){
        let  BreaktitleNotfy =  getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
        
        let  launchtitleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
        
        let  dinnertitleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
        
        
        showNotify(id:"1",body:BreaktitleNotfy,hour:9,minut:0)
        showNotify(id:"2",body:launchtitleNotfy,hour:16,minut:0)
        showNotify(id:"3",body:dinnertitleNotfy,hour:21,minut:0)
    }
    
    
}



/*
 let  BreaktitleNotfy =  getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
 
 let  launchtitleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
 
 let  dinnertitleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
 
 
 showNotify(id:"1",body:BreaktitleNotfy,hour:9,minut:0)
 showNotify(id:"2",body:launchtitleNotfy,hour:16,minut:0)
 showNotify(id:"3",body:dinnertitleNotfy,hour:21,minut:0)
 
 */
extension Array {
    
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()
        
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        
        return results
    }
}
 
extension FormatNaturalDiet:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
