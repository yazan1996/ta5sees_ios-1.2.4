//
//  tblCuiseneType.swift
//  Ta5sees
//
//  Created by Admin on 4/18/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblCusisneType:Object {
    
    static var arrayCusisneType:[tblCusisneType] = []
    static var listCusisneType:Results<tblCusisneType>!
    static var listCusisneTypeDes:[String] = []
    static var CusisneTypeDesc :[String:String]=[:]
   static var txtcusineneID = [String]()
    static var sheard = tblCusisneType()
    
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    @objc dynamic var imgPath = ""
    @objc dynamic var isActive = ""
    @objc dynamic var orderNumber = ""
    @objc dynamic var sizeType = ""

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
       
            let rj = ReadJSON()
            var list = [tblCusisneType]()
            rj.readJson(tableName: "others/tblCuiseneType") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblCusisneType(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                        list.append(obj)
                        }
                    try! setupRealm().write {

                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                    }
                
                        }
            }
        }
    }
    
    
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblCusisneType]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    let obj = tblCusisneType(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!,"imgPath" : String(item["imgPath"].intValue),"isActive" : String(item["isActive"].intValue),"orderNumber" : String(item["orderNumber"].intValue),"sizeType" : String(item["sizeType"].intValue)])

                    list.append(obj)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblCusisneType.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
       
            var list = [tblCusisneType]()
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
//                        let obj = tblCusisneType(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                        
                        let obj = tblCusisneType(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!,"imgPath" : String(item["imgPath"].intValue),"isActive" : String(item["isActive"].intValue),"orderNumber" : String(item["orderNumber"].intValue),"sizeType" : String(item["sizeType"].intValue)])

                        list.append(obj)
                        }
                    try! setupRealm().write {

                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }
                    completion(true)

                        }
            }
        }
    
    static func getIDCusinenSelected( string:String)-> String{
        txtcusineneID.removeAll()
        let a2:[String]! = string.split(separator: ",").map(String.init)

        for x in a2 {
            txtcusineneID.append(setupRealm().objects(tblCusisneType.self).filter("discription contains '\(x)'").first!.id)
        }
       
        return txtcusineneID.joined(separator:",")
    }
    
    static func getData() ->Array<String>{
        
        listCusisneType = (setupRealm().objects(tblCusisneType.self))
        arrayCusisneType = Array(listCusisneType)
        for x in 0..<listCusisneType.count {
            CusisneTypeDesc[listCusisneType[x].id]=listCusisneType[x].discription
        }
        var array:[String]=[]
        for name in CusisneTypeDesc.values {
            array.append(name.description)
        }
        
        return array
    }
    static func showCusiene3(id:String,completion: @escaping ([Item],Error?) -> Void) {
               
        listCusisneType = (setupRealm().objects(tblCusisneType.self).filter("isActive == %@ AND id != %@","1",id))
           arrayCusisneType = Array(listCusisneType)
             var modelAry = [Item]()
           for x in 0..<listCusisneType.count {
            if listCusisneType[x].id != "7" && listCusisneType[x].id != "8"{
            modelAry.append(Item(id: listCusisneType[x].id, name: listCusisneType[x].discription))
            }
           }

        completion(modelAry, nil)
       }
    
    static func showCusiene2(completion: @escaping ([Item],Error?) -> Void) {
               
           listCusisneType = (setupRealm().objects(tblCusisneType.self))
           arrayCusisneType = Array(listCusisneType)
             var modelAry = [Item]()
           for x in 0..<listCusisneType.count {
            if listCusisneType[x].id != "7" && listCusisneType[x].id != "8" && listCusisneType[x].id != "9"{
            modelAry.append(Item(id: listCusisneType[x].id, name: listCusisneType[x].discription))
            }
           }

        completion(modelAry, nil)
       }
    
    static func showCusiene(completion: @escaping ([Item],Error?) -> Void) {
               
           listCusisneType = (setupRealm().objects(tblCusisneType.self))
           arrayCusisneType = Array(listCusisneType)
             var modelAry = [Item]()
           for x in 0..<listCusisneType.count {
            modelAry.append(Item(id: listCusisneType[x].id, name: listCusisneType[x].discription))
           }

        completion(modelAry, nil)
       }
    static func showCusiene1(completion: @escaping ([Item],Error?) -> Void) {
        
        listCusisneType = (setupRealm().objects(tblCusisneType.self))
        arrayCusisneType = Array(listCusisneType)
        var modelAry = [Item]()
        for x in 0..<listCusisneType.count {
            if listCusisneType[x].id != "7" && listCusisneType[x].id != "8" && ActivationModel.sharedInstance.refCusine != listCusisneType[x].id  {
                print("!!!!",listCusisneType[x].id)
                modelAry.append(Item(id: listCusisneType[x].id, name: listCusisneType[x].discription))
            }
        }
        
        completion(modelAry, nil)
    }
    
    static func getdataJoin(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblCusisneType.self),nil)
    }
    
    static func getdataJoinActive(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblCusisneType.self).filter("isActive == %@","1"),nil)
    }
    static func getDataID() ->Array<String>{
        var arrayID:[String]=[]
        
        listCusisneType = (setupRealm().objects(tblCusisneType.self))
        arrayCusisneType = Array(listCusisneType)
        for x in 0..<listCusisneType.count {
            arrayID.append(listCusisneType[x].id)
        }
        return arrayID
        
        
    }
    
    static var arraylist:[tblCusisneType] = []
    static var resultelist:Results<tblCusisneType>!
    class func generateModelArray() -> [tblCusisneType]{
        var modelAry = [tblCusisneType]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblCusisneType.getdataJoin() {(response, Error) in
            resultelist = (response) as? Results<tblCusisneType>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
    class func getCusineActive() -> [tblCusisneType]{
        var modelAry = [tblCusisneType]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblCusisneType.getdataJoinActive() {(response, Error) in
            resultelist = (response) as? Results<tblCusisneType>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
}
