//  tblWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblPackeg:Object,Codable {
    
    
    @objc dynamic var id:Int = -4
    @objc dynamic var calories:String? = ""
    @objc dynamic var carbohydrate:String? = ""
    @objc dynamic var cholesterol:String? = ""
    @objc dynamic var fat:String? = ""
    @objc dynamic var mainTrackerSugg:String? = ""
    @objc dynamic var mealType:String? = "" // 0 not main / 1 main / 2 tracker
    @objc dynamic var name:String? = ""
    @objc dynamic var potassium:String? = ""
    @objc dynamic var preparingTime:String? = ""
    @objc dynamic var protein:String? = ""
    @objc dynamic var recipe:String? = ""
    @objc dynamic var recipeYield:String? = ""
    @objc dynamic var sodium:String? = ""
    @objc dynamic var weightPerServing:String? = ""
    @objc dynamic var totalWeight:String? = ""
    @objc dynamic var imagePath:String? = ""
    @objc dynamic var isChildRestricted:String? = ""
    @objc dynamic var searchKey:String? = ""

    
    
    
    
    static var  arraylist:[tblPackeg] = []
    static var resultelist:Results<tblPackeg>!
    
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
        var list = [tblPackeg]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {
                for item in recommends {
                    let data:tblPackeg = tblPackeg()
                    data.id = item["id"].intValue
                    data.calories = item["calories"].stringValue
                    data.carbohydrate = item["carbohydrate"].stringValue
                    data.cholesterol = item["cholestrol"].stringValue
                    data.fat = item["fat"].stringValue
                    
                    data.isChildRestricted = item["isChildRestricted"].stringValue
                    data.imagePath = item["imgPath"].stringValue
                    data.searchKey = item["searchkey"].stringValue
                    
                    data.mainTrackerSugg = item["mainTrackerSugg"].stringValue
                    data.mealType = item["mealType"].stringValue
                    data.name = item["name"].stringValue
                    data.potassium = item["potassium"].stringValue
                    
                    data.preparingTime = item["preparingTime"].stringValue
                    data.protein = item["protein"].stringValue
                    data.recipe = item["recipe"].stringValue
                    data.recipeYield = item["recipeYield"].stringValue
                    data.sodium = item["sodium"].stringValue
                    data.weightPerServing = item["weightPerServing"].stringValue
                    
                    data.preparingTime = item["preparingTime"].stringValue
                    data.totalWeight = item["totalWeight"].stringValue
                    list.append(data)
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblPackeg.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                
            }
            completion(true)
            
        }
    }
    
    static func getMealDistrbutionUser(cateInfo:Int)->String{
        
        let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
        if item?.isLunchMain == 1 && cateInfo == 2 {
            return "1"
        }else if item?.isDinnerMain == 1 && cateInfo == 3 {
            return "1"
        }else if item?.isBreakfastMain == 1 && cateInfo == 3 {
            return "1"
        }else{
            return "0"
        }
    }
    static func replaceWajbeh(cateInfo:Int,old_item_id:Int,responsEHendler:@escaping (tblWajbatUserMealPlanner)->Void){
        var idPackgeSelectd = [Int]()
        var newItem:tblPackeg!
//        itemShopiingList.removeAll()
        tblUserInfo.getMealDistrbutionUser()
        print("cateInfo",cateInfo)
        let UnWantedFruit = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().frute.split(separator: ",").map { Int($0)!}))
        
        let UnWantedVegetabl = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().vegetabels.split(separator: ",").map { Int($0)!}))
        
        let UnWantedMeats = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().meat.split(separator: ",").map { Int($0)!}))
        
        print(ActivationModel.sharedInstance.refAllergys,ActivationModel.sharedInstance.xtraCusine)
        let packgTest =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ",getMealDistrbutionUser(cateInfo:cateInfo),getWajbhInofID(id:cateInfo),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
        
        for itemIng in packgTest{
            
            let ingTest = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@","\(itemIng.id)"))
            let senfTest1 = Array(setupRealm().objects(tblSenf.self).filter("id IN %@",ingTest.map {Int($0.senfId!)!}))
            
            print("senfTest1",senfTest1.map({$0.id}))
            if !senfTest1.map({$0.id}).contains(where: UnWantedFruit.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedVegetabl.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedMeats.map{$0.refSenfID}.contains)  {
                idPackgeSelectd.append(itemIng.id)
                
            }
        }
        if !idPackgeSelectd.isEmpty{
            
            newItem = setupRealm().objects(tblPackeg.self).filter("id IN %@ ",idPackgeSelectd).shuffled().choose(1).first
            print("cateInfo",newItem.id)
//            if newItem.id == old_item_id {
//                ShowTost(title: "نعتذر", message: "لا يوجد وجبة للاستبدال")
//                return
//            }
        }else{
//            showToast(message: "لا يوجد وجبة للاستبدال", view: UIView, place: 1)
//            ShowTost(title: "نعتذر", message: "لا يوجد وجبة للاستبدال")
            return
        }
//        let newItem = setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@","1",getWajbhInofID(id:cateInfo)).shuffled().choose(1).first!
        
        let result = tblPackeg.calculateNTPackge(idPackge:"\(newItem.id)",id_Cate:cateInfo, date: "nil", saveShoppingList: false)
        let old_item = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("id == %@",old_item_id)
        try! setupRealm().write {
            old_item.setValue("\(newItem.id)",forKey: "refPackgeID")
        }
        try! setupRealm().write {
            old_item.setValue("\(result[0])",forKey: "packagesCalories")
        }
        try! setupRealm().write {
            old_item.setValue("\(result[1])",forKey: "packagesWeights")
        }
        try! setupRealm().write {
            old_item.setValue(newItem.name,forKey: "packgeName")
        }
        
        responsEHendler(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("id == %@",old_item_id).first!)
    }
    static func GenerateMealPlannerFree7Days(date:String,callback:@escaping (Error?)->Void){
        var newItem:tblPackeg!
        _ = [tblWajbatUserMealPlanner]()
        var flag = false
        var countID = tblWajbatUserMealPlanner().IncrementaID()
        itemShopiingList.removeAll()
        let UnWantedFruit = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refFruit.map {Int($0)!}))
        
        let UnWantedVegetabl = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refVegetables.map {Int($0)!}))
        
        let UnWantedMeats = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refMeat.map {Int($0)!}))
        
        print("@@@@",ActivationModel.sharedInstance.startPeriod,ActivationModel.sharedInstance.endPeriod,ActivationModel.sharedInstance.refAllergys)
        
        if ActivationModel.sharedInstance.startPeriod > ActivationModel.sharedInstance.endPeriod {
            ActivationModel.sharedInstance.endPeriod = ActivationModel.sharedInstance.startPeriod
        }
        
        let cusine = ActivationModel.sharedInstance.xtraCusine
        let allergy = ActivationModel.sharedInstance.refAllergys

        if cusine.isEmpty {
            ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refCuiseseID))
        }
        
        if allergy.isEmpty {
            ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refAllergyInfoID))
        }
        
        var all_packg = [tblPackeg]()
        for day in ActivationModel.sharedInstance.startPeriod...ActivationModel.sharedInstance.endPeriod {
            print("day************************",day)


            for index in 1...5 {
                var idPackgeSelectd = [Int]()
                    if index == Int(ActivationModel.sharedInstance.refWajbehMain){
                        all_packg =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ","1",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                        
                    }else if ActivationModel.sharedInstance.refWajbehNotMain.contains("\(index)")  || ActivationModel.sharedInstance.refTasbera.contains("\(index)")  {
                        all_packg =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND id IN %@","0",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                        
                        
                    }else{
                        removeWajbeh(date:Date().getDays(value:day),refPackgeCateItme:"\(index)")
                        continue
                    }
                    
                
                    
                    for itemIng in all_packg{
                        
                        let ingTest = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@","\(itemIng.id)"))
                        let senfTest1 = Array(setupRealm().objects(tblSenf.self).filter("id IN %@",ingTest.map {Int($0.senfId!)!}))
                        
                        if !senfTest1.map({$0.id}).contains(where: UnWantedFruit.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedVegetabl.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedMeats.map{$0.refSenfID}.contains)  {
                            idPackgeSelectd.append(itemIng.id)
                            
                        }
                    }
                    
                    if !idPackgeSelectd.isEmpty {
                        newItem = setupRealm().objects(tblPackeg.self).filter("id IN %@",idPackgeSelectd).shuffled().choose(1).first!
                        flag = true
                        countID+=1
                    }
                
                
                    if flag {
                            let result = tblPackeg.calculateNTPackge(idPackge:"\(newItem.id)",id_Cate:index,date: Date().getDays(value:day), saveShoppingList: false)
                            
                        _ = tblWajbatUserMealPlanner(packgeName: newItem.name!, date: Date().getDays(value:day), refPackgeID:"\(newItem.id)" , packagesCalories: "\(result[0])", packagesWeights: "\(result[1])", refPackgeCateItme: "\(index)", refOrderID: getOrderID(id:index),countID:countID)
//                            listFinalWajbaht.append(item)
                        }
                      
                        
        
                    flag = false
            }
        }
//            try? setupRealm().write {
//                setupRealm().add(listFinalWajbaht,update: .modified)
//            }
//        try? setupRealm().write {
//            setupRealm().add(itemShopiingList,update: .modified)
//        }
        callback(nil)
    }

    static func GenerateMealPlannerFree7Days11(startPeriod:Int,endPeriod:Int,date:String)-> [[AnyObject]]{
        var newItem:tblPackeg!
        let listFinalWajbaht = [tblWajbatUserMealPlanner]()
        var flag = false
        var countID = tblWajbatUserMealPlanner().IncrementaID()-1
        var endPeriod_val:Int
//        itemShopiingList.removeAll()
        
        let UnWantedFruit = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refFruit.map {Int($0)!}))
        //001442.8d10a79e4fc3453b9152379d291283e1.1355
        let UnWantedVegetabl = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refVegetables.map {Int($0)!}))
        
        let UnWantedMeats = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",ActivationModel.sharedInstance.refMeat.map {Int($0)!}))
        
        print("@@@@",ActivationModel.sharedInstance.startPeriod,ActivationModel.sharedInstance.endPeriod,ActivationModel.sharedInstance.refAllergys)
        
//        if ActivationModel.sharedInstance.startPeriod > ActivationModel.sharedInstance.endPeriod {
//            ActivationModel.sharedInstance.endPeriod = ActivationModel.sharedInstance.startPeriod
//        }

        if startPeriod > endPeriod {
            endPeriod_val = startPeriod
        }else{
            endPeriod_val = endPeriod
        }
        
        let cusine = ActivationModel.sharedInstance.xtraCusine
        let allergy = ActivationModel.sharedInstance.refAllergys
        
        if cusine.isEmpty {
            ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refCuiseseID))
        }
        
        if ActivationModel.sharedInstance.refAllergys.isEmpty || ActivationModel.sharedInstance.refAllergys.contains("null") {
            ActivationModel.sharedInstance.refAllergys.removeAll()
            ActivationModel.sharedInstance.refAllergys.append("-1")
        }
        
        if allergy.isEmpty {
            ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refAllergyInfoID))
        }
    
        print("start generate ...")

        var all_packg = [tblPackeg]()
        
        for day in startPeriod...endPeriod_val {
            print("day************************",day)
            
//            DispatchQueue.main.sync {
            if dispatchWorkItem != nil {
                print("KILL GENERATE MEAL PLANNER111")
                if dispatchWorkItem.isCancelled {
                    print("KILL GENERATE MEAL PLANNER")
                    break
                }
            }
            
            for index in 1...5 {
                var idPackgeSelectd = [Int]()
                if index == Int(ActivationModel.sharedInstance.refWajbehMain){
                    all_packg =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ","1",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                    
                }else if ActivationModel.sharedInstance.refWajbehNotMain.contains("\(index)")  || ActivationModel.sharedInstance.refTasbera.contains("\(index)")  {
                    print("refAllergys",ActivationModel.sharedInstance.refAllergys)
                     all_packg = Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ","0",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                    
                    
                }else{
                    removeWajbeh(date:Date().getDays(value:day),refPackgeCateItme:"\(index)")
                    continue
                }
                
                
                
                for itemIng in all_packg{
                    
                    let ingTest = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@","\(itemIng.id)"))
                    let senfTest1 = Array(setupRealm().objects(tblSenf.self).filter("id IN %@",ingTest.map {Int($0.senfId!)!}))
                    
                    if !senfTest1.map({$0.id}).contains(where: UnWantedFruit.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedVegetabl.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedMeats.map{$0.refSenfID}.contains)  {
                        idPackgeSelectd.append(itemIng.id)
                    }
                }
                
                if !idPackgeSelectd.isEmpty {
                    newItem = setupRealm().objects(tblPackeg.self).filter("id IN %@",idPackgeSelectd).shuffled().choose(1).first!
                    flag = true
                    countID+=1
                }
                
                
                if flag {
                    print("countID",countID)
                    let result = tblPackeg.calculateNTPackge(idPackge:"\(newItem.id)",id_Cate:index,date: Date().getDays(value:day), saveShoppingList: false)
                    
                    _ = tblWajbatUserMealPlanner(packgeName: newItem.name!, date: Date().getDays(value:day), refPackgeID:"\(newItem.id)" , packagesCalories: "\(result[0])", packagesWeights: "\(result[1])", refPackgeCateItme: "\(index)", refOrderID: getOrderID(id:index),countID:countID)
//                    listFinalWajbaht.append(item)
                    flag = false

                }
                
                
                
               
            }
        }
//        try? setupRealm().write {
//            setupRealm().add(listFinalWajbaht)
//        }
//        try? setupRealm().write {
//            setupRealm().add(itemShopiingList)
//        }
//
        return [listFinalWajbaht]
    }
    static func removeWajbeh(date:String,refPackgeCateItme:String){
        let deleted = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("date == %@ AND refUserID == %@ && refPackgeCateItme == %@",date,getUserInfo().id,refPackgeCateItme)
        
        try! setupRealm().write{
            setupRealm().delete(deleted)
        }
    }
    static func createItemShoppingList(responsEHendler:@escaping (Error?)->Void){
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        var mealPlanner:[tblWajbatUserMealPlanner] = Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@","\(getUserInfo().id)"))
        
        mealPlanner = mealPlanner.filter({ item in
            
            if dateFormatter.date(from: item.date)!.timeIntervalSince1970*1000.0.rounded() >=  getUserInfo().startSubMilli  && dateFormatter.date(from: item.date)!.timeIntervalSince1970*1000.0.rounded() <= getUserInfo().endSubMilli   {
                return true
            }
            return false
        })

        
        if mealPlanner.isEmpty {
            print("mealPlanner empty")
            return
        }
        
        for index in 0..<(mealPlanner.count-1) {
            
            let wajbeh:tblPackeg =  setupRealm().objects(tblPackeg.self).filter("id == %@",Int(mealPlanner[index].refPackgeID)!).first ?? tblPackeg()
            
            if !wajbeh.isEqual(nil)  {
                _ = tblPackeg.calculateNTPackge(idPackge:"\(wajbeh.id)",id_Cate:mealPlanner[index].refOrderID,date: mealPlanner[index].date, saveShoppingList: true)
            }
        }
        
        responsEHendler(nil)
    }


    
    
    static func updateMealPlannerFree7Days(date:String,responsEHendler:@escaping (Error?)->Void){
        var newItem:tblPackeg!
        var flag = false
        var packgTest = [tblPackeg]()
        let UnWantedFruit = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().frute.split(separator: ",").map { Int($0)!}))
        
        let UnWantedVegetabl = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().vegetabels.split(separator: ",").map { Int($0)!}))
        
        let UnWantedMeats = Array(setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).filter("refUnwantedFruitsAndVegetablesID IN %@",getUserInfo().meat.split(separator: ",").map { Int($0)!}))
        
        print("@@@@",ActivationModel.sharedInstance.startPeriod,ActivationModel.sharedInstance.endPeriod,ActivationModel.sharedInstance.refAllergys)
        if ActivationModel.sharedInstance.startPeriod > ActivationModel.sharedInstance.endPeriod {
            ActivationModel.sharedInstance.endPeriod = ActivationModel.sharedInstance.startPeriod
        }
        for day in ActivationModel.sharedInstance.startPeriod...ActivationModel.sharedInstance.endPeriod {
            for index in 1...5 {
                
                var idPackgeSelectd = [Int]()
                
                if index == Int(ActivationModel.sharedInstance.refWajbehMain){
                    
                    packgTest =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ","1",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                }else if ActivationModel.sharedInstance.refWajbehNotMain.contains("\(index)")  || ActivationModel.sharedInstance.refTasbera.contains("\(index)")  {
                    packgTest =  Array(setupRealm().objects(tblPackeg.self).filter("mealType == %@ AND id IN %@ AND id IN %@ AND NOT id IN %@ ","0",getWajbhInofID(id:index),getCuisenesInofID(id:ActivationModel.sharedInstance.xtraCusine.map { Int($0)!}),getAllergyInofID(id:ActivationModel.sharedInstance.refAllergys.map { Int($0)!})))
                    
                }else{
                    continue
                }
                
                
                for itemIng in packgTest{
                    
                    let ingTest = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@","\(itemIng.id)"))
                    let senfTest1 = Array(setupRealm().objects(tblSenf.self).filter("id IN %@",ingTest.map {Int($0.senfId!)!}))
                    if !senfTest1.map({$0.id}).contains(where: UnWantedFruit.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedVegetabl.map{$0.refSenfID}.contains) && !senfTest1.map({$0.id}).contains(where: UnWantedMeats.map{$0.refSenfID}.contains)  {
                        idPackgeSelectd.append(itemIng.id)
                        
                    }
                    
                }
                if !idPackgeSelectd.isEmpty{
                    newItem = setupRealm().objects(tblPackeg.self).filter("id IN %@",idPackgeSelectd).shuffled().choose(1).first!
                    flag = true
                }
                
                
                if flag {
                    let result = tblPackeg.calculateNTPackge(idPackge:"\(newItem.id)",id_Cate:index,date: Date().getDays(value:day), saveShoppingList: false)
                    print("getDays1",Date().getDays(value:day))
                    
                    _ = tblWajbatUserMealPlanner(packgeName: newItem.name!, date: Date().getDays(value:day), refPackgeID:"\(newItem.id)" , packagesCalories: "\(result[0])", packagesWeights: "\(result[1])", refPackgeCateItme: "\(index)", refOrderID: getOrderID(id:index))
                    flag = false
                }
            }
        }
        
        responsEHendler(nil)
    }
    
    
    static func getOrderID(id:Int)->Int{
        switch id {
        case 1:
           return 1
        case 2:
           return 3
        case 3:
           return 5
        case 4:
           return 2
        case 5:
           return 4
        default:
            return 0
        }
    }
    static func calculateNTPackge(idPackge:String,id_Cate:Int,date: String,saveShoppingList:Bool) -> [Int]{
        var grams:Float = 0.0
        var target:Float = 0.0
        var plateID:[String]!
        var total_caloris = 0.0
        var total_caloris1 = 0.0
        
        tblPackageIngredients.getPlatID(refID: idPackge){ (arrPlateID) in
            plateID = arrPlateID.removeDuplicates()
        }
        
        target = calculateBasedCalorisAllowdUser(idWajbeh:id_Cate)
        
        for id in plateID {
            let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
            
            let percentge = target * nutrationPlate!.Percentage
            
            tblPackageIngredients.getIngsPlat(packgID: idPackge, platID: id) { (arrIngredients) in
                
                let resulteNutrtion = getIngPlate(arr:arrIngredients, id_waj:idPackge,plateId: id,date:date,saveShoppingList:saveShoppingList)
                total_caloris1 += Double(percentge)
                total_caloris += Double(resulteNutrtion[0])
                let valKCal:Float = percentge * resulteNutrtion[4]
                
                grams += valKCal/Float(resulteNutrtion[0])
                
            }
        }
        
        return [Int(round(total_caloris1)),Int(round(grams))]
        
        
    }
    
    static func getWajbhInofID(id:Int)->[Int]{
        let arr = Array(setupRealm().objects(tblPackageInfoItems.self).filter("refWajbehInfoID == %@",id))
        let ids = arr.map { $0.refPackageID }
        return ids
    }
    static func getAllergyInofID(id:[Int])->[Int]{
        let arr = Array(setupRealm().objects(tblWajbehAllergy.self).filter("refAllergyInfoID IN %@",id))
        let ids = arr.map { $0.refWajbehID }
        return ids
    }
    static func getCuisenesInofID(id:[Int])->[Int]{
        let arr = Array(setupRealm().objects(tblWajbehCuisenes.self).filter("refCuiseneID IN %@",id))
        let ids = arr.map { $0.refWajbehID }
        return ids
    }
    static func getIDCategory(id:Int,id_Cate: Int)->Int{
        let arr = setupRealm().objects(tblPackageInfoItems.self).filter("refPackageID == %@ AND refWajbehInfoID == %@",id,id_Cate).first!
        return arr.refWajbehInfoID
    }
    static func getAllWajbeh(responsEHendler:@escaping (Any?,Error?)->Void){
        autoreleasepool{
        responsEHendler(setupRealm().objects(tblPackeg.self).filter("mealType == %@","2").sorted(byKeyPath: "id", ascending: true),nil)
    }
    }
    static func removeWajbeh(id:Int){
        let item = setupRealm().objects(tblPackeg.self).filter("id == %@",id)
        if !item.isEmpty {
            try! setupRealm().write {
                setupRealm().delete(item)
            }
        }
    }

    class func searchModelArray() -> [SearchItem] {
        autoreleasepool{

        var arraylistSearchItem = [SearchItem]()
        tblPackeg.getAllWajbeh() {(response, Error) in
            resultelist = (response) as? Results<tblPackeg>
            arraylist = Array(resultelist)
            for item in arraylist {
            arraylistSearchItem.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!))
        }
        }
            
        
        return arraylistSearchItem
        }
    }
    
}





extension Collection {
    func choose(_ n: Int) -> ArraySlice<Element> { shuffled().prefix(n) }
}

