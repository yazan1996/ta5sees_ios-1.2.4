//
//  tblWajbehCuisenes.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehCuisenes:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refWajbehID = -2
    @objc dynamic var refCuiseneID = -2
    
    
    //    let lines = LinkingObjects(fromType: tblWajbehCuisenes.self, property: "id")
    override static func primaryKey() -> String? {
        return "id"
    }

    func readJson(completion: @escaping (Bool) -> Void){
       
            let rj = ReadJSON()
            var list = [tblWajbehCuisenes]()
            
            rj.readJson(tableName: "wajbeh/tblWajbehCuisenes") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblWajbehCuisenes = tblWajbehCuisenes()
                        data.id = item["id"].intValue
                        data.refWajbehID = item["refWajbehID"].intValue
                        data.refCuiseneID = item["refCuiseneID"].intValue
                        list.append(data)
                         
                        }
                    }
                        try! setupRealm().write {
                            let allData =  setupRealm().objects(tblWajbehCuisenes.self)
                            if !allData.isEmpty {
                                setupRealm().delete(allData)
                            }
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
        }
       
        }
    }
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblWajbehCuisenes]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    
                    let data:tblWajbehCuisenes = tblWajbehCuisenes()
                    data.id = item["id"].intValue
                    data.refWajbehID = item["refPackageID"].intValue
                    data.refCuiseneID = item["refCuiseneID"].intValue
                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblWajbehCuisenes.self)
                if allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
    
}
