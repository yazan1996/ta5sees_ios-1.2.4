//
//  tblPackageIngredients.swift
//  Ta5sees
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblPackageIngredients:Object {
    
    @objc dynamic var id:Int = -4
    @objc dynamic var calories:String? = ""
    @objc dynamic var carbohydrate:String? = ""
    @objc dynamic var cholesterol:String? = "" //cholestrol
    @objc dynamic var fat:String? = ""
    @objc dynamic var item:String? = ""
    @objc dynamic var order:String? = ""
    @objc dynamic var packageId:String? = ""
    @objc dynamic var plateId:String? = ""//refPlateID
    @objc dynamic var potassium:String? = ""
    @objc dynamic var senfId:String? = ""//refSenfID
    @objc dynamic var text:String? = ""
    @objc dynamic var sodium:String? = ""
    @objc dynamic var protein:String? = ""
    @objc dynamic var percentage:String? = ""
    @objc dynamic var quantity:String? = ""
    @objc dynamic var refMainHesaID:String? = ""
    @objc dynamic var netWeight:String? = ""

    
    static let shared = tblPackageIngredients()
    
    
    
//    var reftblPackge = List<tblPackeg>()
//    var reftblPlateItem = List<tblPlateItem>()
//    var reftblSenf = List<tblSenf>()
    
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    static func getIngsPlat(packgID:String,platID:String,callBack:([tblPackageIngredients])->Void){
        let arrIng:[tblPackageIngredients] = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND plateId == %@",packgID,platID))
        
        callBack(arrIng)
        
        
    }

    
    static func getPlatID(refID:String,callBack:([String])->Void){
        let refPackgeIngr:[tblPackageIngredients] = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@",refID))
        
        let refPlateID:[String] = refPackgeIngr.map { (packageIngredients) in
            
            return packageIngredients.plateId!
        }
        callBack(refPlateID)
        
        
    }
    static func getSenfID(refID:String,callBack:([tblPackageIngredients])->Void){
        let refPackgeIngr:[tblPackageIngredients] = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@",refID))
    
        callBack(refPackgeIngr)
        
        
    }
    
    static func getPlatPercentage(refIDPKG:String,refIDPL:String)->String{
        let refPackgeIngr:tblPackageIngredients = setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND plateId == %@",refIDPKG,refIDPL).first!
        if refPackgeIngr.percentage == "" {
            return "0"
        }
        return refPackgeIngr.percentage ?? "0"
        
    }
    static func getIngPlate(refIDPKG:String,refIDPL:String,callBack:([String],[String])->Void){
        
        let refPackgeIngr:[tblPackageIngredients] = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND plateId == %@",refIDPKG,refIDPL))
        var arr = [String]()
        let refPlateID:[String] = refPackgeIngr.map { (packageIngredients) in
            print("refAllergyID",Int(packageIngredients.senfId!)!,getUserInfo().refAllergyInfoID)
            
            let newInfAllergy:tblIngredientRulesAllergy? = setupRealm().objects(tblIngredientRulesAllergy.self).filter("senfId == %@ AND refAllergyID IN %@",Int(packageIngredients.senfId!)!,getUserInfo().refAllergyInfoID.split(separator: ",").map { Int($0)!}).first
            if newInfAllergy == nil {
                return packageIngredients.item!
            }
            if newInfAllergy?.actionType == 1 {
                let newSenf = setupRealm().object(ofType: tblSenf.self, forPrimaryKey:newInfAllergy?.senfId)
                arr.append("يرجى استبدال \(packageIngredients.item!) ب\(newSenf!.ingredientName)")
                return "يرجى استبدال -\(packageIngredients.item!) -ب\(newSenf!.ingredientName)"
            } else if newInfAllergy?.actionType == 2 {
                arr.append("يرجى حذف \(packageIngredients.item!)")
                return "يرجى حذف -\(packageIngredients.item!)"
            }else{
                return packageIngredients.item!

            }
        }
        callBack(refPlateID,arr)
        
        
    }
    static func getIngPlateReplac(refIDPKG:String,refIDPL:String,callBack:([tblPackageIngredients])->Void){
        let refPackgeIngr:[tblPackageIngredients] = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND plateId == %@",refIDPKG,refIDPL))
        
    
        callBack(refPackgeIngr)
    }
    static func removeWajbeh(id:Int){
        let item:Results = setupRealm().objects(tblPackageIngredients.self).filter("id == %@",id)
        if !item.isEmpty {
        try! setupRealm().write {
            setupRealm().delete(item)
        }
        }
    }

 
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblPackageIngredients]()
        print("insert new Data packeg Ing")
        autoreleasepool {

//            let thread =  DispatchQueue.global(qos: .utility)
//            thread.async {[self] in
                if let recommends = JSON(response).array {
                    for item in recommends {
                        
                        let data:tblPackageIngredients = tblPackageIngredients()
                        data.id = item["id"].intValue
                        data.calories = item["calories"].stringValue
                        data.carbohydrate = item["carbohydrate"].stringValue
                        data.cholesterol = item["cholestrol"].stringValue
                        data.fat = item["fat"].stringValue
                        data.text = item["text"].stringValue
                        data.senfId = item["refSenfID"].stringValue
                        data.packageId = item["refPackageID"].stringValue
                        data.potassium = item["potassium"].stringValue
                        data.quantity = item["quantity"].stringValue
                        data.refMainHesaID = item["refMainHesaID"].stringValue
                        
                        data.item = item["item"].stringValue
                        data.protein = item["protein"].stringValue
                        data.order = item["order"].stringValue
                        data.percentage = item["percentage"].stringValue
                        
                        
                        data.plateId = item["refPlateID"].stringValue
                        data.sodium = item["sodium"].stringValue
                        
                        data.netWeight = item["netWeight"].stringValue

                        
                        list.append(data)
                        
                        
                    }
                    
                    try! setupRealm().write {
                        let allData =  setupRealm().objects(tblPackageIngredients.self)
                        if !allData.isEmpty {
                            setupRealm().delete(allData)
                        }
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        
                                completion(true)
                    }
//                    let main_part = recommends.splitted()
//                    let part1 =  main_part.0.splitted()
//                    let part2 =  main_part.1.splitted()
//                    let subPart1 = part1.0
//                    let subPart12 = part1.1
//                    let subPart2 = part2.0
//                    let subPart21 = part2.1
//                    insertData(recommends:subPart1)
//                    insertData(recommends:subPart12)
//                    insertData(recommends:subPart2)
//                    insertData(recommends:recommends)
//                    completion(true)
                    
                
//            }
            }
        
    }
    }
  
  
    func insertData(recommends:Array<JSON>){
        print("insert new Data packeg Ing")
        
       
    }
  
}



