//
//  tblWajbehAllergy.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehAllergy:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refWajbehID = -2
    @objc dynamic var refAllergyInfoID = -2
    //    let lines = LinkingObjects(fromType: tblWajbehAllergy.self, property: "id")
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        
        let rj = ReadJSON()
        var list = [tblWajbehAllergy]()
        
        rj.readJson(tableName: "wajbeh/tblWajbehAllergy") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblWajbehAllergy = tblWajbehAllergy()
                        data.id = item["id"].intValue
                        data.refWajbehID = item["refPackageID"].intValue
                        data.refAllergyInfoID = item["refAllergyInfoID"].intValue
                        list.append(data)
                        
                        
                    }
                }
                try! setupRealm().write {
                    let allData =  setupRealm().objects(tblWajbehAllergy.self)
                    if !allData.isEmpty {
                        setupRealm().delete(allData)
                    }
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    
                    completion(true)
                    
                }
            }
        }
    }
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblWajbehAllergy]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    
                    let data:tblWajbehAllergy = tblWajbehAllergy()
                    data.id = item["id"].intValue
                    data.refWajbehID = item["refWajbehID"].intValue
                    data.refAllergyInfoID = item["refAllergyInfoID"].intValue
                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblWajbehAllergy.self)
                if allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
    
}
