//
//  tblPackageInfoItems.swift
//  Ta5sees
//
//  Created by Admin on 12/7/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblPackageInfoItems:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var refPackageID:Int = 0
    @objc dynamic var refWajbehInfoID:Int = 0

   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
 
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
        var list = [tblPackageInfoItems]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {
                
                for item in recommends {
                    
                    let data:tblPackageInfoItems = tblPackageInfoItems()
                    data.id = item["id"].intValue
                    data.refPackageID = item["refPackageID"].intValue
                    data.refWajbehInfoID = item["refWajbehInfoID"].intValue
                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblPackageInfoItems.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

        }
    }


}
    
    



/*
 
 Removing FirebaseAnalyticsInterop
 Removing FirebaseAuthInterop
 Removing FirebaseCoreDiagnosticsInterop
 Removing GoogleDataTransportCCTSupport
 Generating Pods project

 
 # Uncomment the next line to define a global platform for your project
 # platform :ios, '9.0'

 target 'Ta5sees' do
   # Comment the next line if you don't want to use dynamic frameworks
   use_frameworks!
   
   # Pods for Ta5sees
   pod 'TextFieldEffects'
   pod 'RSSelectionMenu'
   pod 'SwiftGifOrigin'
   pod 'SwiftyGif'
   pod 'Alamofire'
   pod 'AlamofireObjectMapper'
   pod 'ObjectMapper'
   pod 'iOSDropDown'
   pod 'Realm'
   pod 'RealmSwift'
   pod 'SwiftyJSON'
   pod 'CountryPickerSwift'
  # pod 'ANLoader'
   pod 'PieCharts'
   pod 'YYCalendar'
   pod 'Charts'
   pod 'FlagPhoneNumber'
   pod 'SwiftyCodeView/RxSwift'
   pod 'Firebase/Analytics'
   pod 'Firebase/Auth'
   pod 'Firebase/Core'
   pod 'Firebase/Database'
   pod 'Firebase/Messaging'
   #     pod 'EFInternetIndicator'
   pod 'Firebase/RemoteConfig'
   pod 'GTProgressBar'
   pod 'Firebase/DynamicLinks'
   pod 'MessageKit'
   pod 'Firebase/Storage'
   pod 'Firebase/Firestore'
   pod 'MessageInputBar'
   pod 'KSFacebookButton'
   pod 'TwitterKit'
   pod 'fluid-slider'
   pod 'FacebookCore'
   pod 'TwitterCore'
   pod 'FacebookLogin'
   pod 'VKPinCodeView'
   pod 'TNSlider'
   pod 'DLRadioButton'
   pod 'CRNotifications'
   pod "FLAnimatedImage"
   pod 'CarbonKit'
   pod 'RxSwift'
   pod 'RxCocoa'
   pod 'SimpleCheckbox'#-> delete
   pod 'TestFairy'
   pod 'ReachabilitySwift'
   pod "SimpleAlert" #-> delete
   
   pod 'FBSDKCoreKit'
   pod 'FBSDKShareKit'
   pod 'FBSDKLoginKit'
   pod 'SVProgressHUD'
   
   pod 'RNCryptor', '~> 5.0'

 end

 */
