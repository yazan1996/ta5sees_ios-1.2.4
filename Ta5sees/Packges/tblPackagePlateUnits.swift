//
//  tblPackagePlateUnits.swift
//  Ta5sees
//
//  Created by Admin on 1/4/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblPackagePlateUnits:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var PackageID = -4
    @objc dynamic var PlateID  = -4
    @objc dynamic var Unit:String? = ""
    @objc dynamic var Weight  = -4

    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
  
    static func greateDemoItem(PlateID:Int)->[tblPackagePlateUnits]{
        var arr = [tblPackagePlateUnits]()
        for x in 0..<2 {
        let data:tblPackagePlateUnits = tblPackagePlateUnits()
        data.id = -3
        data.PackageID = 0
        data.PlateID = PlateID
            if x == 0 {
                data.Unit = "الوزن"
            }else{
                data.Unit = "غم"
            }
        data.Weight = 0
        arr.append(data)
      }
        return arr
    }
    static func greateDemoItem2(PlateID:Int)->[tblPackagePlateUnits]{
        var arr = [tblPackagePlateUnits]()
        for x in 0..<1 {
        let data:tblPackagePlateUnits = tblPackagePlateUnits()
        data.id = -3
        data.PackageID = 0
        data.PlateID = PlateID
            if x == 0 {
                data.Unit = "defualt"
            }
        data.Weight = 0
        arr.append(data)
      }
        return arr
    }
    static func getUnits(PlateID:Int,PackageID:Int)-> [tblPackagePlateUnits]{
       var list = [tblPackagePlateUnits]()
        
        list.append(contentsOf: greateDemoItem2(PlateID:PlateID))
        
        list.append(contentsOf: setupRealm().objects(tblPackagePlateUnits.self).filter("PlateID == %@ AND PackageID == %@",PlateID,PackageID))
        list.append(contentsOf: greateDemoItem(PlateID:PlateID))
        print(list)
        return list
        
    }
    
   
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var count_id = 1
        var list = [tblPackagePlateUnits]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    
                    let data:tblPackagePlateUnits = tblPackagePlateUnits()
                    data.id = count_id
                    data.PackageID = item["PackageID"].intValue
                    data.PlateID = item["PlateID"].intValue
                    data.Unit = item["Unit"].stringValue
                    data.Weight = item["Weight"].intValue
                    count_id+=1
                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblPackagePlateUnits.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
}
