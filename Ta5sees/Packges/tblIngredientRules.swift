//
//  tblIngredientRules.swift
//  Ta5sees
//
//  Created by Admin on 12/7/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblIngredientRules:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var senfId:Int = 0
    @objc dynamic var replaceWithSenfId:Int = 0
    @objc dynamic var actionType:Int = 0 // 1->replace // 2-> delete
    @objc dynamic var dietType:Int = 0

   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    static func getIDs(ids:Int,dietType:Int,completion: @escaping ([tblIngredientRules]) -> Void){
        let ar = setupRealm().objects(tblIngredientRules.self).filter("dietType == %@ AND senfId == %@",dietType,ids)
        
        completion(Array(ar))
    }
    public func getUserInfo()->tblUserInfo{
        var user:tblUserInfo!
        getInfoUser { (res, error) in
            user = res
        }
        return user
    }
//    static func getIDsReplacemnt(id_packge:String)->[[Int]]{
//        let ar = setupRealm().objects(tblIngredientRules.self).filter("dietType == %@ AND senfId IN %@",1,getReplacementIng(id_packge: id_packge, itemIng: "1"))
//        let arr:[Int]  = ar.map { (packageIngredients) in
//
//            return packageIngredients.replaceWithSenfId
//        }
//        let arr2:[Int]  = ar.map { (packageIngredients) in
//            return packageIngredients.senfId
//        }
//        let actionType:[Int]  = ar.map { (packageIngredients) in
//            return packageIngredients.actionType
//        }
//        return [arr,arr2,actionType]
//    }
    static func getIDsReplacemnt1(id_packge:String,itemIng:String)->tblIngredientRules{
        let ar:tblIngredientRules = setupRealm().objects(tblIngredientRules.self).filter("dietType == %@ AND senfId IN %@",Int(getUserInfo().refPlanMasterID)!,getReplacementIng(id_packge: id_packge,itemIng:itemIng)).first ?? tblIngredientRules()
        return ar
    }
    static func getUserInfo()->tblUserInfo{
        var user:tblUserInfo!
        getInfoUser { (res, error) in
            user = res
        }
        return user
    }

    static func getReplacementIng(id_packge:String,itemIng:String)->[Int]{
        let q1 = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND senfId == %@",id_packge,itemIng))

        let refSenfID:[Int] = q1.map { (packageIngredients) in
            
            return Int(packageIngredients.senfId!)!
        }
        return refSenfID
    }

    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
        var list = [tblIngredientRules]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {
                
                for item in recommends {
                    
                    let data:tblIngredientRules = tblIngredientRules()
                    data.id = item["id"].intValue
                    data.senfId = item["senfId"].intValue
                    data.replaceWithSenfId = item["replaceWithSenfId"].intValue
                    data.actionType = item["actionType"].intValue
                    data.dietType = item["dietType"].intValue

                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblIngredientRules.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

        }
    }


}
    
    
