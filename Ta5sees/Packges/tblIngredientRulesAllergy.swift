//
//  tblIngredientRulesAllergyAllergy.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 07/07/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblIngredientRulesAllergy:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var senfId:Int = 0
    @objc dynamic var replaceWithSenfId:Int = 0
    @objc dynamic var actionType:Int = 0 // 1->replace // 2-> delete
    @objc dynamic var refAllergyID:Int = 0

   
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    static func getIDs(ids:Int,refAllergyID:Int,completion: @escaping ([tblIngredientRulesAllergy]) -> Void){
        let ar = setupRealm().objects(tblIngredientRulesAllergy.self).filter("refAllergyID == %@ AND senfId == %@",refAllergyID,ids)
        
        completion(Array(ar))
    }
    public func getUserInfo()->tblUserInfo{
        var user:tblUserInfo!
        getInfoUser { (res, error) in
            user = res
        }
        return user
    }
//    static func getIDsReplacemnt(id_packge:String)->[[Int]]{
//        let ar = setupRealm().objects(tblIngredientRulesAllergy.self).filter("refAllergyID == %@ AND senfId IN %@",1,getReplacementIng(id_packge: id_packge, itemIng: "1"))
//        let arr:[Int]  = ar.map { (packageIngredients) in
//
//            return packageIngredients.replaceWithSenfId
//        }
//        let arr2:[Int]  = ar.map { (packageIngredients) in
//            return packageIngredients.senfId
//        }
//        let actionType:[Int]  = ar.map { (packageIngredients) in
//            return packageIngredients.actionType
//        }
//        return [arr,arr2,actionType]
//    }
    static func getIDsReplacemnt1(id_packge:String,itemIng:String)->tblIngredientRulesAllergy{
        let ar:tblIngredientRulesAllergy = setupRealm().objects(tblIngredientRulesAllergy.self).filter("refAllergyID == %@ AND senfId IN %@",Int(getUserInfo().refPlanMasterID)!,getReplacementIng(id_packge: id_packge,itemIng:itemIng)).first ?? tblIngredientRulesAllergy()
        return ar
    }
    static func getUserInfo()->tblUserInfo{
        var user:tblUserInfo!
        getInfoUser { (res, error) in
            user = res
        }
        return user
    }

    static func getReplacementIng(id_packge:String,itemIng:String)->[Int]{
        let q1 = Array(setupRealm().objects(tblPackageIngredients.self).filter("packageId == %@ AND senfId == %@",id_packge,itemIng))

        let refSenfID:[Int] = q1.map { (packageIngredients) in
            
            return Int(packageIngredients.senfId!)!
        }
        return refSenfID
    }

    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
        var list = [tblIngredientRulesAllergy]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {
                
                for item in recommends {
                    
                    let data:tblIngredientRulesAllergy = tblIngredientRulesAllergy()
                    data.id = item["id"].intValue
                    data.senfId = item["senfId"].intValue
                    data.replaceWithSenfId = item["replaceWithSenfId"].intValue
                    data.actionType = item["actionType"].intValue
                    data.refAllergyID = item["refAllergyID"].intValue

                    list.append(data)
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblIngredientRulesAllergy.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

        }
    }


}
    

