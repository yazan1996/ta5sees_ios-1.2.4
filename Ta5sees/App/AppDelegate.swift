 //
 //  AppDelegate.swift
 //  Ta5sees
 //
 //  Created by Admin on 2/22/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //
 // 1.1.8 -- 15
 
 /*
  /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Ta5sees/GoogleService-Info.plist -p ios /Users/telecomenterprise/Downloads/appDsyms-5
  */
 
 import UIKit
 import Firebase
 //import CoreData
 import SVProgressHUD
 import Alamofire
 import SwiftyJSON
 import ObjectMapper
 import RealmSwift
 import Realm
 import Foundation
 import AlamofireObjectMapper
 import FirebaseAuth
 import UserNotifications
 import FirebaseDatabase
 import UserNotifications
 import FirebaseMessaging
 import MessageKit
 import FirebaseFirestore
 import RxSwift
 import RxCocoa
 import FirebaseDynamicLinks
 import FirebaseCore
 import FBSDKCoreKit
 import TwitterKit
 import FirebaseRemoteConfig
 import FirebaseInstanceID
 import AuthenticationServices
 //import CommonCryptoModule
 import FirebaseCrashlytics
 import SwiftyStoreKit
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate ,URLSessionDelegate ,UNUserNotificationCenterDelegate, ApiResponseDaelegat {
    
    func getResponseSuccuss() {}
    
    func getResponseFalueir(meeage: String) {}
    
    func getResponseMissigData() {}
    
    func showIndecator() {}
    
    func HideIndicator() {}
    static var db:Firestore!
    
    static var appDelegate:AppDelegate!
    //    let realm = try! Realm()
    var window: UIWindow?
    let logoAnimationView = LogoAnimationView()
    var currentTime: Date?
    var Currencies:[Any] = []
    var restrictRotation:UIInterfaceOrientationMask = .portrait
    static var UID_Firebase:String!
    var ob:AppDelegate!
    //    var countSaveTable:Results<tblPlanMaster>?
    //    var notification:NotificationToken!
    
    private var channelReference: CollectionReference {
        return Firestore.firestore().collection("channels")
    }
    lazy var  obAPiSubscribeUser = APiSubscribeUser(with: self)
    
    
    let gcmMessageIDKey = "gcm.message_id"
    static var ListData = Array<tblSenfItems>()
    
    var appBundleId = "-1"
    override init() {
        
        // code connected with firebase test project
        //        let filePath = Bundle.main.path(forResource: "GoogleService-Info-4", ofType: "plist")
        //        guard let fileopts = FirebaseOptions(contentsOfFile: filePath!)
        //          else { assert(false, "Couldn't load config file") }
        //        FirebaseApp.configure(options: fileopts)
      

        FirebaseApp.configure()
        if  !Database.database().isPersistenceEnabled {
            print("YYYYYYY")
            Database.database().isPersistenceEnabled = true
        }
        AppDelegate.db = Firestore.firestore()
       
    }
    
//    func sntDataToFirebase(){
//
//        obAPiSubscribeUser.subscribeUser(mealDitr: ActivationModel.sharedInstance.TeeDistribution,flag: 1) { (bool) in
//            if bool {
//
//
//
//                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
//
//                let queue = DispatchQueue(label: "com.appcoda.myqueue")
//                queue.async {
//                    _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod:  6,date:getDateOnly())
//                }
//                dispatchWorkItem = DispatchWorkItem {
//                    //مش مبين معي السبب  لو بعتلي اسم المستخدم قبل ما ترجع تعمل اشتراك جديد كان بينة اسرع
//                    //                queue.async {
//                    if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
//                        _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod+7,endPeriod:   30,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
//                    }
//                    if dispatchWorkItem != nil {
//                        print("finish generate and cancel thread ....")
//                        dispatchWorkItem.cancel()
//                    }
//                    ActivationModel.sharedInstance.dispose()
//                }
//
//                queue.async(execute: dispatchWorkItem)
//
//                //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                //                    tblPackeg.GenerateMealPlannerFree7Days(date:getDateOnly()) { _ in
//                //                        SVProgressHUD.dismiss {
//                //                            ActivationModel.sharedInstance.dispose()
//                //                        }
//                //                    }
//                //                }
//            }else{
//                //Ta5sees.alert(mes: "لم تتم العملية", selfUI: self)
//                SVProgressHUD.dismiss()
//            }
//        }
//    }
    
    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>) {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
            
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() <= Double(getUserInfo().endSubMilli) {
                print("active Subscribtion")
                return
            }
            
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().endSubMilli) {
                
                if getUserInfo().expirDateSubscribtion != "" {
                    if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                        print("expirDate Subscribtion")
//                        checkSubcribtion(purchases)
                        return
                    }
                }
                // user active - renewal proccess
                
                _ =  getUserInfo().subDuration
                let endSubMilli = Int(getUserInfo().endSubMilli)
                print("getUserInfo().endSubMilli",getUserInfo().endSubMilli)
                
                let latestWajbeh = Date(timeIntervalSince1970: TimeInterval(endSubMilli) / 1000)
                print("latestWajbeh",latestWajbeh, endSubMilli,getUserInfo().endSubMilli)
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
                dateFormatter1.calendar = Calendar(identifier: .gregorian)
                dateFormatter1.timeZone = TimeZone(secondsFromGMT: 0)
                dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
                let days =  getComparDate(date1:getDateOnly(),date2:dateFormatter.string(from:latestWajbeh))
                
                let daysWithExpireSub =  getComparDate(date1:dateFormatter.string(from:latestWajbeh),date2:Date().getDays(value: Int(getUserInfo().subDuration)!))
                
                print("daysWithExpireSub",daysWithExpireSub,days)
                
                let latestHestory:tblPaymentHistory? = Array( setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@",getUserInfo().id)).last
                
                if latestHestory?.product_id == "1" {
                    return
                }
                
                if days <= 5 && latestHestory?.subscriptionDuration != "1" {
                    // renewal
                    print("generate new month")
                    if daysWithExpireSub < 40 {
                        print("endSubMilli = daysWithExpireSub")
                        ActivationModel.sharedInstance.endSubMilli = daysWithExpireSub
                    }else{
                        print("endSubMilli = 30")
                        ActivationModel.sharedInstance.endSubMilli = 30
                    }
                    ActivationModel.sharedInstance.startPeriod = days+1
                    ActivationModel.sharedInstance.endPeriod =  ActivationModel.sharedInstance.endSubMilli
                    
                    ActivationModel.sharedInstance.expireDate =  Double(getUserInfo().expirDateSubscribtion)!
                    ActivationModel.sharedInstance.subDuration =  Int(getUserInfo().subDuration)!
                    
                    updateMealPlanner()
                }
                
            }else if getUserInfo().subscribeType == 2{
                checkSubcribtion(purchases)
            }
        }else if getUserInfo().subscribeType == 2{
            checkSubcribtion(purchases)
        }
    }
    
    
    
    func checkSubcribtion(_ purchases: Set<RegisteredPurchase>){
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { [self] result in
            var arrPaymentHistory =  [[String:Any]]()
            NetworkActivityIndicatorManager.networkOperationFinished()
            print("verifyReceipt ",result)
            
            switch result {
            
            case .success(let receipt):
                setDataInSheardPreferance(value: "0", key: "noInternet")
                
                //                                print("pending_renewal_info",pending_renewal_info[0][_,pending_renewal_info[0]["expiration_intent"])
                
                let all_latest_local_productID = setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@",getUserInfo().id).last
                
                
                appBundleId = all_latest_local_productID?.product_id ?? "-1"
                
                if appBundleId == "-1" {
                    return
                }
                let productIds = Set(purchases.map { _ in appBundleId })
                
                let result = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                
                var pending_renewal_info =  receipt["pending_renewal_info"] as! [[String:AnyObject]]
                
                
                pending_renewal_info = pending_renewal_info.filter { item in
                    
                    
                    if item["product_id"] as! String == appBundleId {
                        return true
                    }
                    return false
                }
                if pending_renewal_info.isEmpty {
                    obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                    return
                }
                print(appBundleId,"pending_renewal_info" ,pending_renewal_info)
                //if pending_renewal_info[0]["auto_renew_status"]  as? String ?? "0"  == "0" {
                //                            print("auto_renew_status = 0")
                //                            return
                //}
                switch result {
                case .purchased(let expiryDate, let items):
                    
                    let receipt_info  = receipt["receipt"] as! [String:AnyObject]
                    
                    
                    var arr_receipt = receipt_info["in_app"] as! [[String:AnyObject]]
                    let sortedArray = arr_receipt.sorted(by: { ($0["purchase_date_ms"]?.doubleValue)! > ($1["purchase_date_ms"]?.doubleValue)! })
                    arr_receipt = sortedArray
                    //                    print("arr_receipt",arr_receipt.first)
                    //1000000827539918
                    
                    let laest_local_transaction = Array(setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@ AND transactionID == %@",getUserInfo().id,"\(arr_receipt.first!["transaction_id"]!)"))
                    
                    print("laest_local_transaction",laest_local_transaction)
                    
                    
                    let all_latest_local_transaction:[tblPaymentHistory]? = Array(setupRealm().objects(tblPaymentHistory.self) .filter("refUserID == %@",getUserInfo().id))
                    
                    print("latestTrancation_Date","\(String(describing: arr_receipt.first?["transaction_id"]))")
                    
                    
                    //                            laest_local_transaction.append(tblPaymentHistory())
                    
                    
                    arrPaymentHistory.append(["status": "1", "datetime": "\( arr_receipt.first!["purchase_date"]!)", "amount": all_latest_local_transaction?[0].amount ?? "null", "transactionID": "\( arr_receipt.first!["transaction_id"]!)", "description": "renewal","iosOrginalTransactionID": "\( arr_receipt.first!["original_transaction_id"]!)","iosOrginalDateTime": "\( arr_receipt.first!["original_purchase_date"]!)","iosOrginalDescription":"renewal","isFirstTimeSubscription":"0","subscriptionDuration":all_latest_local_transaction?[0].subscriptionDuration ?? "null","productId": pending_renewal_info[0]["product_id"] ?? "","purchaseToken":"null"])
                    
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
                    dateFormatter.calendar = Calendar(identifier: .gregorian)
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    
                    let d = dateFormatter1.date(from: dateFormatter1.string(from: expiryDate))
                    
                    let newFormate = dateFormatter.string(from: d!)
                    
                    let d1 =  dateFormatter1.date(from: arr_receipt.first!["purchase_date"] as! String)
                    
                    let DateStartSubsc = dateFormatter.date(from: dateFormatter.string(from: d1!))
                    
                    
                    let purchase_date = dateFormatter.date(from: dateFormatter.string(from: DateStartSubsc!))
                    print("purchase_date",purchase_date," / ",purchase_date)
//                    ActivationModel.sharedInstance.endSubMilli = 0
//                    ActivationModel.sharedInstance.startSubMilli = 0
//                    ActivationModel.sharedInstance.startSubMilli =  calendar.component(.day, from: DateStartSubsc!)
                    
                    if getDateOnly() == dateFormatter.string(from: purchase_date!) {
                        ActivationModel.sharedInstance.startSubMilli  = Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!)
                        
                    }else
                    if dateFormatter.date(from: getDateOnly())! > purchase_date!{
                        ActivationModel.sharedInstance.startSubMilli  =  -Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!) * -1
                        
                        
                    }else if dateFormatter.date(from: getDateOnly())! < purchase_date!{
                        ActivationModel.sharedInstance.startSubMilli  = +Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!) * 1
                        
                    }
                    
                    print("startSubMilli",ActivationModel.sharedInstance.startSubMilli)
                    
                    if Date().daysBetween(start:purchase_date!, end: dateFormatter.date(from: newFormate)!) >= 31 {
                        ActivationModel.sharedInstance.endSubMilli = 31
                    }else{
                        ActivationModel.sharedInstance.endSubMilli = Int(Double(Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)))
                        
                        print("endSubMilli",ActivationModel.sharedInstance.endSubMilli)
                    }
                    
//                    if appBundleId == "1" {
//                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
//                        ActivationModel.sharedInstance.subDuration = 30
//
//                    }else if appBundleId == "3" {
//                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 90).timeIntervalSince1970 * 1000.0.rounded()
//                        ActivationModel.sharedInstance.subDuration = 90
//
//
//                    }else {
//                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 365).timeIntervalSince1970 * 1000.0.rounded()
//                        ActivationModel.sharedInstance.subDuration = 365
//
//                    }
                    ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)
                    
                    ActivationModel.sharedInstance.expireDate = dateFormatter.date(from: newFormate)!.timeIntervalSince1970 * 1000.0.rounded()
                    print("expireDate",newFormate,ActivationModel.sharedInstance.expireDate)
                    
                    ActivationModel.sharedInstance.endPeriod = Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)-1
                    if ActivationModel.sharedInstance.endPeriod < 0 {
                        ActivationModel.sharedInstance.endPeriod = 0
                    }
                    ActivationModel.sharedInstance.subscribeType = 1
                    
                    if !laest_local_transaction.isEmpty { // check if transactionID exite or not
                        print("transaction_id found in database")
                        obAPiSubscribeUser.updateUserInfoPayment()
                        updateMealPlanner()
                        return
                    }
                    //                        print("\(productIds) is valid until \(String(describing: dateFormatter.date(from: newFormate)))\n\(items)\n",Int(expiryDate.timeIntervalSince1970 * 1000.0 .rounded()))
                    APiSubscribeUser.setInfoPayment(obj: arrPaymentHistory)
                    obAPiSubscribeUser.updateUserInfoPayment()
                    updateMealPlanner()
                    
                    print("\(productIds) is valid until \(expiryDate)\n\(items)")
                case .expired(let expiryDate, let items):
                    print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
                    if pending_renewal_info[0]["is_in_billing_retry_period"] != nil {
                        if pending_renewal_info[0]["is_in_billing_retry_period"] as? String == "1" {
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
                            dateFormatter.calendar = Calendar(identifier: .gregorian)
                            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            let grace_period_expires_date = dateFormatter1.date(from: pending_renewal_info[0]["grace_period_expires_date"] as! String)
                            
                            ActivationModel.sharedInstance.expireDate = grace_period_expires_date!
                                .timeIntervalSince1970 * 1000.0.rounded()
                            ActivationModel.sharedInstance.subscribeType = 4 // grace pierod
                            ActivationModel.sharedInstance.startPeriod = 0
                            ActivationModel.sharedInstance.endPeriod = Date().daysBetween(start: getDateOnlyasDate(), end: grace_period_expires_date!)-1
                            if ActivationModel.sharedInstance.endPeriod < 0 {
                                ActivationModel.sharedInstance.endPeriod = 0
                            }
                            ActivationModel.sharedInstance.endSubMilli =  Date().daysBetween(start: getDateOnlyasDate(), end: grace_period_expires_date!)
                            ActivationModel.sharedInstance.startSubMilli = 0
                            
                            ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: getDateOnlyasDate(), end: grace_period_expires_date!)-1
                            
                            //                                ActivationModel.sharedInstance.endSubMilli
//                            if appBundleId == "1" {
//                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
//                                ActivationModel.sharedInstance.subDuration = 30
//
//                            }else if appBundleId == "3" {
//                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 90).timeIntervalSince1970 * 1000.0.rounded()
//                                ActivationModel.sharedInstance.subDuration = 90
//
//
//                            }else {
//                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 365).timeIntervalSince1970 * 1000.0.rounded()
//                                ActivationModel.sharedInstance.subDuration = 365
//
//                            }
                            arrPaymentHistory.append(["status": "1", "datetime": "\( grace_period_expires_date ?? getDateOnlyasDate())", "amount": "", "transactionID": "", "description": "grace_period","iosOrginalTransactionID": "","iosOrginalDateTime": "\( grace_period_expires_date ?? getDateOnlyasDate())","iosOrginalDescription":"grace_period","isFirstTimeSubscription":"0","subscriptionDuration": "\(ActivationModel.sharedInstance.endSubMilli)","productId": pending_renewal_info[0]["product_id"] ?? "","purchaseToken":"null"])
                            
                            APiSubscribeUser.setInfoPayment(obj: arrPaymentHistory)
                            obAPiSubscribeUser.updateUserInfoPayment()
                            updateMealPlanner()
                        }else{
                            // expiryDate
                            obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                        }
                    }
                    
                    if pending_renewal_info[0]["expiration_intent"] as? String == "1" {
                        // The customer voluntarily canceled their subscription.
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:3)
                    }else if pending_renewal_info[0]["auto_renew_status"] as? String == "0" {
                        // canceled
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:3)
                    }else{
                        // expiryDate
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                    }
                    print("\(productIds) is expired since \(expiryDate)\n\(items)")
                    
                case .notPurchased:
                    print("\(productIds) has never been purchased")
                }
            case .error(let receiptError):
                SVProgressHUD.dismiss()
                if receiptError.localizedDescription.debugDescription == "The operation couldn’t be completed. (SwiftyStoreKit.ReceiptError error 1.)" {
                    setDataInSheardPreferance(value: "1", key: "noInternet")
                }
                //
                print("unknownerror",  receiptError.localizedDescription.debugDescription)
            }
        }
    }
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "02d69ba2f957418c80ae8fb699376187")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
    
    
    func checkGenerateSubcribe(){
        verifySubscriptions([.autoRenewableMonthly,.autoRenewableYearly,.autoRenewalEvery3Months])
    }
    func setupIAP() {
        
        SwiftyStoreKit.completeTransactions(atomically: true) {  purchases in
            for purchase in purchases {
                
                switch purchase.transaction.transactionState {
                
                case .purchased, .restored:
                    //                    print("completeTransactions...","\(purchase.transaction.transactionState.debugDescription): \(purchase.productId) : \(purchase.transaction.transactionDate)")
                    
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break // do nothing
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if UserDefaults.standard.integer(forKey: "login") == 0 {
            //        CheckInternet.startListnerIntenet()
        }
        UITextViewWorkaround.unique.executeWorkaround()// solve propbelm ios 12.9 or later class named _UITextLayoutView
        setDataInSheardPreferance(value: "0", key: "finishRegistration")
        autoreleasepool {
            
            if getDataFromSheardPreferanceString(key: "Encrption") != "1" {
                //create database encrpted
                try?  FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
                var key = Data(count: 64)
                
                //                _ = key.withUnsafeMutableBytes {
                //                      SecRandomCopyBytes(kSecRandomDefault, 64, $0.baseAddress!)
                //                  }
                
                _ = key.withUnsafeMutableBytes { bytes in
                    SecRandomCopyBytes(kSecRandomDefault, 64, bytes)
                }
                setDataInSheardPreferanceData(value: key, key: "keyEncription")
                //                _ = Realm.Configuration(fileURL:inlibraryFolder(name:"encrepred.realm"),encryptionKey: getDataFromSheardPreferanceData(key: "keyEncription"))
                setDataInSheardPreferance(value: "1", key: "Encrption")
            }
            
        }
        
        
        
        setupIAP() // payemnt
    
        if UserDefaults.standard.integer(forKey: "login") != 0 {
            if getDataFromSheardPreferanceString(key: "latestDateOpenApp") != "0" {
                if getNumberDaysBetweenDate(date1:getDataFromSheardPreferanceString(key: "latestDateOpenApp")) <= 2592000 { //as sec for 30 days
                    DispatchQueue.main.async { [self] in
                        print("checkGenerateSubcribe 123")
                        
                        checkGenerateSubcribe() // payemnt
                    }
                }
            }
        }
        
        setDataInSheardPreferance(value: "0", key: "tblAllergyInfo")
        
        
        loadJson(fileName: "tblPlanMaster" ,flag: "1")
        loadJson(fileName: "tblLayaqaCond" ,flag: "2")
        loadJson(fileName: "tblMenuPlanExchangeSystem" ,flag: "10")
        loadJson(fileName: "tblTEEDistribution" ,flag: "3")
        loadJson(fileName: "tblChildAges" ,flag: "4")
        loadJson(fileName: "tblAllergyInfo" ,flag: "5")
        loadJson(fileName: "tblMealDistribution" ,flag: "6")
        loadJson(fileName: "tblCDCGrowthChild" ,flag: "7")
        loadJson(fileName: "tblCusisneType" ,flag: "8")
        loadJson(fileName: "tblCountry" ,flag: "9")
        loadJson(fileName: "tblWajbehInfo" ,flag: "11")
        loadJson(fileName: "tblSenfCat" ,flag: "12")
        loadJson(fileName: "tblSenfCatSub" ,flag: "13")
        loadJson(fileName: "tblTamareenWeights" ,flag: "14")
        loadJson(fileName: "tblTamareenTypes" ,flag: "15")
        loadJson(fileName: "tblTamareenActivity" ,flag: "16")
        loadJson(fileName: "tblMainHesa" ,flag: "17")
        
        setupRealm().autorefresh = true
        
        UNUserNotificationCenter.current().delegate = self
        //        clearAllSheardPrefrence()
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey: "V7R09IBrOP0HRGreFmCaamCqx", consumerSecret: "olYtOmQMR5XoSd3gscGySSoh2JTUi7J6OniyHMusVbLh8GDcnh")
        
        customFontColor()
        
        
        
        //        application.setMinimumBackgroundFetchInterval(1)//86400
        remoteConfig = RemoteConfig.remoteConfig()
        
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(readLocalRemoteConfg() as? [String : NSObject])
        Messaging.messaging().delegate = self
        //        Messaging.messaging().isAutoInitEnabled = true
        
        _ = Messaging.messaging().fcmToken // unused
        Crashlytics.crashlytics()
        
        
        Crashlytics.crashlytics().setUserID(getDataFromSheardPreferanceString(key: "userID"))
        
        
        self.window!.tintColor = UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0)
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("fcmToken \(fcmToken)")
        print("messaging \(messaging)")
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        if ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]) {
            return true
        }
        else
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options){
            return true
        }
        else {
            return false
        }
        //     return  twitterAuthentication
    }
    
    ///// silent notification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("this will return '32 bytes' in iOS 13+ rather than the token \(tokenString)")
        
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let token = token {
                print("Remote instance ID token: \(token)")
                Messaging.messaging().apnsToken = deviceToken
                deviceTokens =  token
            }
            Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        }
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = notification[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(notification)
        
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
        completionHandler(UIBackgroundFetchResult.newData)
        
        // This notification is not auth related, developer should handle it.
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
    }
    
    
    //Daynamc linke
    
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            let handleLink = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamicLink, error) in
                if let dynamicLink = dynamicLink, let _ = dynamicLink.url
                {
                    print("Your Dynamic Link parameter: \(dynamicLink)")
                } else {
                    // Check for errors
                }
            })
            return handleLink
        }
        return false
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        print("Your Dynamic Link parameter: \(dynamicLink)")
    }
    
    
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {}
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
        if UserDefaults.standard.integer(forKey: "login") == 1 {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["9"]) // general notification
        }
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("applicationWillEnterForeground")
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        setupRemoteConfig()
        print("applicationDidBecomeActive")
        if UserDefaults.standard.integer(forKey: "login") == 1 {
            obAPiSubscribeUser.updateAccessLoginUser(id:getDataFromSheardPreferanceString(key: "userID"))
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["9"]) // general notification
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if UserDefaults.standard.integer(forKey: "login") == 1 {
            print("applicationDidEnterBackground")
            checkCenterNotification { (bool) in
                if bool {
                    print("applicationDidEnterBackground12")
                    //            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["9"]) // general notification
                    let  GeneralNotfy =  getTitleNotify(keyRemote:"generalRemindersTitles",keyValue:"generalTitle",sheardKey:"generalTitleCounter")
                    showNotifyGeneral(id:"9",body:GeneralNotfy)
                }
            }
            
        }else{
            //            print(dictUser["txtheight"] as? String)
            //            setDataInSheardPreferanceDic(value: dictUser, key: "dictUser-\(getDataFromSheardPreferanceString(key: "mobNum"))")
        }
    }
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate")
        if UserDefaults.standard.integer(forKey: "login") == 1 {
            checkCenterNotification { (bool) in
                if bool {
                    print("applicationDidEnterBackground12")
                    //            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["9"]) // general notification
                    let  GeneralNotfy =  getTitleNotify(keyRemote:"generalRemindersTitles",keyValue:"generalTitle",sheardKey:"generalTitleCounter")
                    showNotifyGeneral(id:"9",body:GeneralNotfy)
                }
            }
        }else{
            //            print(dictUser["txtheight"] as? String)
            //            setDataInSheardPreferanceDic(value: dictUser, key: "dictUser-\(getDataFromSheardPreferanceString(key: "mobNum"))")
        }
    }
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool{
        return true
    }
    
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.restrictRotation
    }
    
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Ta5sees")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    private func application(application: UIApplication, willChangeStatusBarFrame newStatusBarFrame: CGRect) {
        let windows = UIApplication.shared.windows
        
        for window in windows {
            window.removeConstraints(window.constraints)
        }
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        _ = notification.request.identifier
        //        createNOtifcation(id_Notification:id)
        //        hanelOpenAppwithNotif(id:id)
        
        //        if id == "8"{ // alarm wieght
        //            tblNotifications.setNotification(title: notification.request.content.title, body: notification.request.content.body, flag: id)
        //        }
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        completionHandler([.alert, .badge, .sound])
        
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler:
                                 @escaping () -> Void) {

        let id = response.notification.request.identifier
        
        if id != "1" && id != "2" && id != "3" && id != "4" && id != "5" && id != "6" && id != "7" && id != "8" && id != "9" {
            print("id notfy \(id)")
            hanelOpenAppwithNotifChate()
        }else{
            let title = response.notification.request.content.body
            switch response.actionIdentifier {
            case UNNotificationDefaultActionIdentifier:
                createNOtifcation(id_Notification:id)
                hanelOpenAppwithNotif(id:id, title: title)
                break
            case "ACCEPT_ACTION":
                createNOtifcation(id_Notification:id)
                hanelOpenAppwithNotif(id:id, title: title)
                break
                
            case "SYNC_ACTION":
                createNOtifcation(id_Notification:id)
                hanelOpenAppwithNotif(id:id, title: title)
                self.Syncss { (response,error) in
                    print("response\(response!)")
                }
                completionHandler()
                break
            default:
                //            if id == "8"{ // alarm wieght //save in table database as notification
                tblNotifications.setNotification(title: response.notification.request.content.title, body: response.notification.request.content.body, flag: id)
                //            }
                createNOtifcation(id_Notification:id)
                //            hanelOpenAppwithNotif(id:id)
                completionHandler()
            }
        }
        
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        completionHandler()
        
    }
    
    func hanelOpenAppwithNotif(id:String,title:String){
      
        //                let appDelegate = UIApplication.shared.delegate as!AppDelegate
        //                appDelegate.window = UIWindow(frame:UIScreen.main.bounds)
        
        let mainStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain") as! TabBarControllerMain
        tabbarVC.selectedIndex = 0
        tabbarVC.selectedViewController = tabbarVC.viewControllers![0]
        
        if let vcs = tabbarVC.viewControllers,
           let nc = vcs.first as? UINavigationController,
           let pendingOverVC = nc.topViewController as? ViewController {
            pendingOverVC.title_alarm = title
            pendingOverVC.id_alarm = id
            
        }
        
        UIApplication.shared.topViewController()?.present(tabbarVC, animated: true, completion: nil)
        //        windowAlarm?.rootViewController = tabbarVC
        //        windowAlarm?.makeKeyAndVisible()
        
        
        // --------
    }
    
    func hanelOpenAppwithNotifChate(){
        
       
//        let appDelegate = UIApplication.shared.delegate as!AppDelegate
//     appDelegate.window = UIWindow(frame:UIScreen.main.bounds)
        let mainStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain") as! UITabBarController
        tabbarVC.selectedViewController = tabbarVC.viewControllers![3]
        UIApplication.shared.topViewController()?.present(tabbarVC, animated: true, completion: nil)

//        windowAlarm?.rootViewController = tabbarVC
//        windowAlarm?.makeKeyAndVisible()
        
    }
    
    func createNOtifcation(id_Notification:String){
        
        if id_Notification == "9" {//general notification
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["9"]) // general notification
            let  GeneralNotfy =  getTitleNotify(keyRemote:"generalRemindersTitles",keyValue:"generalTitle",sheardKey:"generalTitleCounter")
            showNotifyGeneral(id:"9",body:GeneralNotfy)
        }else if id_Notification == "1" && id_Notification == "2" && id_Notification == "3" && id_Notification == "4" && id_Notification == "5" && id_Notification == "6" && id_Notification == "7" && id_Notification == "8"{
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id_Notification])
            var titleNotfy = ""
            let item:tblAlermICateItems = tblAlermICateItems.getDataAlertNotification(cateID: id_Notification)
            if id_Notification == "1" {
                titleNotfy =  getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "2" {
                titleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "3" {
                titleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "4" {
                titleNotfy =  getTitleNotify(keyRemote:"snack1RemindersTitles",keyValue:"snack1Title",sheardKey:"snack1TitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "5" {
                titleNotfy =  getTitleNotify(keyRemote:"snack2RemindersTitles",keyValue:"snack2Title",sheardKey:"snack2TitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "6" {
                titleNotfy =  getTitleNotify(keyRemote:"exerciseRemindersTitles",keyValue:"exerciseTitle",sheardKey:"exerciseTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "7" {
                titleNotfy =  getTitleNotify(keyRemote:"waterRemindersTitles",keyValue:"waterTitle",sheardKey:"waterTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }else if id_Notification == "8" {
                titleNotfy =  getTitleNotify(keyRemote:"weightRemindersTitles",keyValue:"weightTitle",sheardKey:"weightTitleCounter")
                showNotify(id:id_Notification,body:titleNotfy,hour:item.alermHour,minut:item.alermMinuet)
            }
            
            
        }
    }
    
    
    
    func Syncss(responsEHendler:@escaping (Any?,Error?)->Void){
        //        let headers = HTTPHeaders()
        //        Alamofire.request("http://dev.tele-ent.com/TE-WebServices/diet/changePassword", method:.post, parameters:["key" :"WfJGygCfEu6n5ZhhhucT","password" :"\(Int.random(in: 1000..<6000))","mobNum" :"+962786110923"],encoding:URLEncoding.default, headers:headers).responseJSON{
        //            response in switch response.result
        //            {case.success:
        //
        //                if let recommends = JSON(response.result.value!).dictionary {
        //                    if recommends["errorCode"] == 0{
        //                        responsEHendler(0,nil)
        //                    }else{
        //                        responsEHendler(1,nil)
        //
        //                    }
        //                }
        //
        //            case.failure(let error):
        //                responsEHendler(error,nil)
        //                break
        //
        //            }
        //        }
    }
    
    
    //     @objc func Syncss1()->String {
    //         var cc:String = "-1"
    //         let headers = HTTPHeaders()
    //         Alamofire.request("http://dev.tele-ent.com/TE-WebServices/diet/changePassword", method:.post, parameters:["key" :"WfJGygCfEu6n5ZhhhucT","password" :"\(Int.random(in: 1000..<6000))","mobNum" :"0786110923"],encoding:URLEncoding.default, headers:headers).responseJSON{
    //             response in switch response.result
    //             {case.success:
    //
    //                 if let recommends = JSON(response.result.value!).dictionary {
    //                     if recommends["errorCode"] == 0{
    //                         print("(((((((((((((((((((((((((((((((((((((((((((((1")
    //                         cc = "0"
    //                     }else{
    //                         print("((((((((((333(((((((((((((((((((((((((((((((((((1")
    //                         print("0")
    //                         cc = "2"
    //
    //                     }
    //                 }
    //
    //             case.failure(let error):
    //                 print(error)
    //                 print("(((((((((((((((((434343(((((((((((((((((((((((((((1")
    //                 cc = "1"
    //             }
    //         }
    //         return cc
    //
    //
    //     }
    
    
    
    
    
    
    func setupRemoteConfig(){
        
        
        remoteConfig.fetch() { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                remoteConfig.activate() { (changed, error) in
                    // ...
                }
            } else {
                print("Config not fetched")
                print("Error: \(error?.localizedDescription ?? "No error available.")")
            }
        }
        
        //            if let recommends = JSON(remoteConfig["dinnerRemindersTitles"].jsonValue!).array {
        ////                for item in recommends {
        ////                    print(item["dinnerTitle"].stringValue)
        ////                }
        //                print("RRR \(recommends[2]["dinnerTitle"])")
        //
        //            }
        //             setDataInSheardPreferanceInt(value: 1, key: "dinnerRemindersTitlesCounters")
        
        
        //            let version = remoteConfig.configValue(forKey: "mealsReminders").stringValue
        //            let json = JSON(version!)
        //
        //
        //            print("json \(json) ")
        
    }
    
    
    func customFontColor(){
        
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: "GE Dinar One", size: 20)!,
            NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font:UIFont(name: "GE Dinar One", size: 20)!], for:.normal)
        
        UIButton.appearance().titleLabel?.font = UIFont(name: "GE Dinar One", size: 20)!
        //           UILabel.appearance().font = UIFont(name: "GE Dinar One", size: 20)
        
        
        UITextView.appearance().linkTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: "GE Dinar One", size: 20)!,
            NSAttributedString.Key.foregroundColor: UIColor.white]
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "GE Dinar One", size: 12)!], for: UIControl.State.normal)
    }
    
    //    func UserLogin(){
    //        Auth.auth().signInAnonymously(){ (user,error) in
    //            let isAnonymous=user!.user.isAnonymous
    //            if isAnonymous==true{
    //                AppDelegate.UID_Firebase=user!.user.uid
    //                print( "uid:\(AppDelegate.UID_Firebase!)")
    //            }
    //        }
    //    }
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    func loadJson(fileName:String,flag:String){
        if getDataFromSheardPreferanceString(key: fileName) == "1" {
            return
        }
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSON(data: data).array
                getClass(flag:flag,json:json!)
                
                
            } catch {
                // handle error
            }
        }
    }
    
    func getClass(flag:String,json:[JSON]){
        switch flag {
        case "1":
            let tbl = tblPlanMaster()
            tbl.readFileJson(response: json) {  (bool) in
                print("load tblPlanMaster\(bool)")
                setDataInSheardPreferance(value: "1", key: "tblPlanMaster")
                
            }
            break
        case "2":
            let tbl = tblLayaqaCond()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblLayaqaCond \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblLayaqaCond")
            }
            break
        case "3":
            let tbl = tblTEEDistribution()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblTEEDistribution \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblTEEDistribution")
            }
            break
        case "4":
            let  tbl = tblChildAges()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblChildAges \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblChildAges")
            }
            break
        case "5":
            let tbl = tblAllergyInfo()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblAllergyInfo \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblAllergyInfo")
            }
            break
        case "6":
            let tbl = tblMealDistribution()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblMealDistribution \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblMealDistribution")
            }
            break
        case "7":
            let  tbl = tblCDCGrowthChild()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblCDCGrowthChild \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblCDCGrowthChild")
            }
            break
        case "8":
            let tbl = tblCusisneType()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblCusisneType \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblCusisneType")
            }
            break
        case "9":
            let tbl = tblCountry()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblCountry \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblCountry")
            }
            break
        case "10":
            let tbl = tblMenuPlanExchangeSystem()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblMenuPlanExchangeSystem \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblMenuPlanExchangeSystem")
            }
            break
        case "11":
            let tbl = tblWajbehInfo()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblWajbehInfo \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblWajbehInfo")
            }
            break
        case "12":
            let tbl = tblSenfCat()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblSenfCat \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblSenfCat")
            }
            break
        case "13":
            let tbl = tblSenfCatSub()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblSenfCatSub \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblSenfCatSub")
                
            }
            break
        case "14":
            let tbl = tblTamreenWeights()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblTamreenWeights \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblTamareenWeights")
            }
            break
        case "15":
            let tbl = tblTamreenType()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblTamreenType \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblTamareenTypes")
            }
            break
        case "16":
            let tbl = tblTamreenActivity()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblTamreenActivity \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblTamareenActivity")
            }
            break
        case "17":
            let tbl = tblMainHesa()
            tbl.readFileJson(response: json) { (bool) in
                print("load tblMainHesa \(bool)")
                setDataInSheardPreferance(value: "1", key: "tblMainHesa")
            }
            break
        default:
            print("not found id class")
        }
    }
    
    
    
 }
 
 
 
 
 extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    
 }
 
 
 extension AppDelegate {
    
    func updateMealPlanner(){
        getMealDistrbutionUser()
        //        SVProgressHUD.show()
        SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
//        DispatchQueue.main.async {
//        UIApplication.shared.beginIgnoringInteractionEvents()
//        }
        let userProgressHistory = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@",getUserInfo().id).last!
        tblUserInfo.EditeInfoCalculateUser(progressHistory: userProgressHistory) { [self] str, err in
            
            obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 1) { bool in
                if bool {
                    
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    //                        tblPackeg.updateMealPlannerFree7Days(date:getDateOnly()) {  err in
                    //                            SVProgressHUD.dismiss {
                    //                                UIApplication.shared.endIgnoringInteractionEvents()
                    //                                ActivationModel.sharedInstance.dispose()
                    //                            }
                    //                        }
                    //                    }
                    
                    
                    dispatchWorkItem = DispatchWorkItem {
                        SVProgressHUD.dismiss()

                        //                    queue.async {
                        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                            _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod+7,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
                        }
                        if dispatchWorkItem != nil {
                            print("finish generate and cancel thread ....")
                            dispatchWorkItem.cancel()
                        }
                        ActivationModel.sharedInstance.dispose()
                    }
                    
                    let queue = DispatchQueue(label: "com.appcoda.myqueue")
                    queue.async {
                        
                        _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod:  6,date:getDateOnly())
//                        DispatchQueue.main.async {
//                            UIApplication.shared.endIgnoringInteractionEvents()
//                        }
                        queue.async(execute: dispatchWorkItem)
                    }
                    
                 
                    
                }else{
                    SVProgressHUD.dismiss()
                }
            }
        }
        
        func getMealDistrbutionUser(){
            
            let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
            if item?.isLunchMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "2"
            }else if item?.isDinnerMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "3"
            }else if item?.isBreakfastMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "1"
            }
            if item?.Breakfast == 1 && item?.isBreakfastMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("1")
            }
            if item?.Dinner == 1 && item?.isDinnerMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("3")
            }
            if item?.Lunch == 1 && item?.isLunchMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("2")
            }
            if item?.Snack1 == 1 {
                ActivationModel.sharedInstance.refTasbera.append("4")
            }
            if item?.Snack2 == 1 {
                ActivationModel.sharedInstance.refTasbera.append("5")
            }
            ActivationModel.sharedInstance.subscribeType =  getUserInfo().subscribeType
            
            ActivationModel.sharedInstance.refMeat = getUserInfo().meat.split(separator: ",").map { String($0) }
            ActivationModel.sharedInstance.refFruit = getUserInfo().frute.split(separator: ",").map { String($0) }
            ActivationModel.sharedInstance.refVegetables = getUserInfo().vegetabels.split(separator: ",").map { String($0) }
            
            
            ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//append(contentsOf:Array(arrayLiteral: getUserInfo().refAllergyInfoID))
            
            ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refCuiseseID))
            
        }
    }
 }
 
 
