//
//  AudioRecording.swift
//  Ta5sees
//
//  Created by Admin on 2/19/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation

import Foundation
//import RealmSwift
import AVFoundation
import Firebase
extension HomeChatViewController {
    
    
    
    func startRecording() {
        
        print("Start Recored")
        let settings =
            [
             AVNumberOfChannelsKey: 2,
             AVFormatIDKey : kAudioFormatMPEG4AAC,
             AVSampleRateKey: 44100.0] as [String : Any]
        
        
        do {
            audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
            audioRecorder.delegate = self
            audioRecorder.isMeteringEnabled = true
            audioRecorder.prepareToRecord()
            audioRecorder.record()
            isPlayedAudio = true
        } catch {
            finishRecording()
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        isPlayedAudio = false
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        //        print("RRRRE")
    }
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    
    
    func getFileUrl() -> URL
    {
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        
        return filePath
    }
    
    
    func finishRecording() {
        if audioRecorder != nil {
            audioRecorder.stop()
        }
        
        print("finish Recored")
        if FileManager.default.fileExists(atPath: getFileUrl().path)
        {
          
            obviewPresnterMessage.generateURlAudio(url:getFileUrl() )
            audioRecorder = nil
        }
        
        
    }
 
    
}


