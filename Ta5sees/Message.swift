//
//  Message.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase
//import CodableFirebase
class Message : NSObject, Codable {
    
    
    
    
    var text:String?
    var UserName:String?
    var urlImage:String?
    var receiverId:String?
    var mark:String?
    var senderID:String?
    var PostDate:Int?
    var audio:String?
    var flageInternet:String?
    var id:String?
    
    override init(){}
    
    init(text:String,PostDate:Int,mark:String,senderID:String,audio:String,flageInternet:String,receiverId:String,urlImage:String) {
        
        self.text=text
        self.PostDate=PostDate
        self.mark=mark
        self.senderID=senderID
        self.audio=audio
        self.receiverId=receiverId
        self.flageInternet=flageInternet
        self.urlImage=urlImage
    }
    init(text:String,PostDate:Int,mark:String,senderID:String,audio:String,receiverId:String,urlImage:String) {
        
        self.text=text
        self.PostDate=PostDate
        self.mark=mark
        self.senderID=senderID
        self.audio=audio
        self.receiverId=receiverId
        self.urlImage=urlImage
        
    }
    
    
    
    init(msgData:[String:AnyObject],id_msg:String,firstName:String,id_user:String) {
        
        print("msgData",msgData)
        if let senderID1 = msgData["senderId"] as? String {
            senderID = senderID1
        }
        
        id = id_msg
        if let mark1 = msgData["seen"] as? String {
            if getDataFromSheardPreferanceString(key: "userID") != senderID {
                if id_msg != "0" {
                    ref.child("messages").child("\(id_user)").child(id_msg).updateChildValues(["seen": "تمت رؤيته"])
                    mark =  "تمت رؤيته"
                }
            }else{
                mark = mark1
            }
        }
        if let postDate1 = msgData["date"] as? Int {
            PostDate = postDate1
            
        }
        else{
            PostDate=0
        }
        
        if let receiverId1 = msgData["receiverId"] as? String {
            receiverId = receiverId1
            
        }
        else{
            receiverId = "not Found"
        }
        
        
        if msgData["imgUrl"] != nil {
            urlImage =  msgData["imgUrl"] as? String
        }else{
            urlImage = "not Found"
        }
        
        if  msgData["textMessage"] != nil {
            text  = msgData["textMessage"] as? String
        }
        else{
            text = "not Found"
            
        }
        if msgData["textMessage"] as? String  != "تسجيل صوتي"{
            audio = "not Found"
        }else{
            if let refaudio = msgData["voiceUrl"] as? String {
                audio = refaudio
            }
        }
        
        
    }
    func AccessFirebase()->DatabaseReference{
        return Database.database().reference()
    }
    
    
    
    
    
    
    
}
