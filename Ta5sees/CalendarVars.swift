
import Foundation
import UIKit
import Firebase
import FirebaseAuth
import CRNotifications
import RealmSwift
import Realm
import FirebaseRemoteConfig
import SwiftyJSON
import FirebaseDatabase
import TextFieldEffects
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
//import Reachability
import FirebaseFirestore

var remoteConfig: RemoteConfig!
var header = HTTPHeaders()
var dictUser=[String:Any]() 
var id_alarm:String!
var flagTut = 0
var package_id_Tut = -1
let date = Date()
var id_Ing_rep = [Int]()
var scroll_home_main:UIScrollView!
var delegateDisplayPackge:DisplayCateItemsController!
var locationUnitTut:CGRect!
var CollectionUnitTut:UICollectionView!
var deviceTokens:String!
var calendar = Calendar.current
var ValuerefUserID = -3
var refPlanMaster = -1
var refSubPlanMater = -1
var nc = NotificationCenter.default
var AgeUser = -1 //-> 1 = adult / 2 = child
var refIDSenfCat:Int!
let dispatchGroup = DispatchGroup()
var nameItem:String!
let window = UIApplication.shared.windows.first { $0.isKeyWindow }
var ref = Database.database().reference()
var db = Firestore.firestore()
var data_all:[String:AnyObject]!
let day = calendar.component(.day , from: date)
var weekday = calendar.component(.weekday, from: date) - 1
var month = calendar.component(.month, from: date) - 1
var year = calendar.component(.year, from: date)
var itemShopiingList  = [tblShoppingList]()

let defaultInnerColor: UIColor = UIColor(red: 255/255, green: 227/255, blue: 87/255, alpha: 1.0)
var colorGray =  UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 1.0)
var colorBasicApp =  UIColor(red: 141/255, green: 251/255, blue: 187/255, alpha: 1.0)
var colorGreen =  UIColor(red: 94/255, green: 204/255, blue: 154/255, alpha: 1.0)
let arrMealPlaner = [mealPlaner( id :"1",time :"9:00 AM" ,selectionID: "0"), mealPlaner( id : "2",time :"4:00 PM" ,selectionID: "1"),
                     mealPlaner( id :"3", time :"9:00 PM" ,selectionID: "0"),mealPlaner( id :"4",time :"12:00 PM" ,selectionID: "0"),
                     mealPlaner( id :"5", time : "7:00 PM" ,selectionID: "0"),
]
let regularFont = UIFont.systemFont(ofSize: 16)
let boldFont = UIFont.boldSystemFont(ofSize: 16)
var listUnitEdite = [UnitEdite]()
var dispatchWorkItem:DispatchWorkItem!
//var exchangeSystem = tblExchangeSystem()
let centerNotification = UNUserNotificationCenter.current()

func setGradientBackgroundLable(txt:UILabel,delegate:UIViewController) {
    
//    let gradientLayer = CAGradientLayer()
//    gradientLayer.colors = [UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0) ,UIColor(red: 106/255, green: 197/255, blue: 182/255, alpha: 1.0)].map {$0.cgColor}
//    gradientLayer.locations = [0.0, 1.0]
//    gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
//    gradientLayer.endPoint = CGPoint (x: 1, y: 0.5)
//    gradientLayer.frame = delegate.view.bounds
//    txt.superview!.layer.insertSublayer(gradientLayer, at: 0)
    
    let gradient = CAGradientLayer()

    gradient.frame = delegate.view.bounds
    gradient.colors = [UIColor(hexString:"#7BDEB3").cgColor,UIColor(hexString:"#7BDEB3").cgColor,UIColor(hexString:"#B6FBD3").cgColor]

    txt.superview!.layer.insertSublayer(gradient, at: 0)
    
}
func setGradientBackgroundButton(btn:btnTwitter,delegate:UIViewController) {
    
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0) ,UIColor(red: 106/255, green: 197/255, blue: 182/255, alpha: 1.0)].map {$0.cgColor}
    gradientLayer.locations = [0.0, 1.0]
    gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
    gradientLayer.endPoint = CGPoint (x: 1, y: 0.5)
    //    gradientLayer.frame = delegate.view.bounds
    btn.layer.insertSublayer(gradientLayer, at: 0)
    
}
func moveTopController(scroll:UIScrollView) {
    var bottomOffset1:CGPoint!
    bottomOffset1 = CGPoint(x: 0, y:0)
    scroll.setContentOffset(bottomOffset1, animated: true)
    
    if bottomOffset1 != nil {
        scroll.setContentOffset(bottomOffset1, animated: true)
    }
}
func showAlert(str:String,view:UIViewController){
    let alert = UIAlertController(title: "خطأ", message: str, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
    view.present(alert, animated: true, completion: nil)
}
func customError(textFeild:HoshiTextField,color:UIColor,placeHolder:String){
    textFeild.isError( numberOfShakes: 2.5, revert: true)
    //        textFeild.borderInactiveColor = color_Inactive
    //        textFeild.borderActiveColor = color
    textFeild.placeholder = placeHolder
    textFeild.placeholderColor = color
}
func deleteCenterNotification(){
    centerNotification.getPendingNotificationRequests { (notifications) in
        if notifications.isEmpty {
            let  GeneralNotfy =  getTitleNotify(keyRemote:"generalRemindersTitles",keyValue:"generalTitle",sheardKey:"generalTitleCounter")
            showNotifyGeneral(id:"9",body:GeneralNotfy)
        }else if notifications.count > 0{
            let general_notfy = notifications.filter( {$0.identifier == "9" }).first
            if general_notfy != nil {
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [String(general_notfy!.identifier)]) // general notification
            }
        }
    }
}

func checkCenterNotification(completion: @escaping (Bool)->Void){
    centerNotification.getPendingNotificationRequests { (notifications) in
        if notifications.count >= 1 && notifications.filter( {$0.identifier != "9"}).first != nil{
            print("false")
            completion(false)
        }else{
            print("true")
            completion(true)
        }
    }
}

func checkCenterNotificationWajbat(completion: @escaping (Bool)->Void){
    centerNotification.getPendingNotificationRequests { (notifications) in
        for x in notifications {
            print("xxx \(x.identifier)")
            
        }
        if  notifications.filter( {$0.identifier == "2" || $0.identifier == "1" || $0.identifier == "3" || $0.identifier == "4" || $0.identifier == "5"}).first != nil{
            completion(true)
        }else{
            print("false")
            completion(false)
        }
    }
}
//func checkInterntFirebase(completion: @escaping (Bool)->Void){
//    CheckInternet.checkIntenet { (bool) in
//        if bool {
//            completion(true)
//        }
//        completion(false)
//    }
////    let connectedRef = Database.database().reference(withPath: ".info/connected")
////    connectedRef.observe(.value, with: { snapshot in
////        if let connected = snapshot.value as? Bool, connected {
////            print("Connected")
////            completion(true)
////        } else {
////            print("Not connected")
////            completion(false)
////        }
////    })
//
//}
var idUserExchangeSystem:Int!

func getDateOnlyasDate()->Date{
    
    let dateFormatter = DateFormatter()
    dateFormatter.calendar = Calendar(identifier: .gregorian)
    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    let currentDate = Date()
    return  dateFormatter.date(from:dateFormatter.string(from: currentDate))!
}
func getDateOnly()->String{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let currentDate = Date()
    return dateFormatter.string(from: currentDate)
}
func getDateTime()->String{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    let currentDate = Date()
    return dateFormatter.string(from: currentDate)
}
func getDateTimeAsDateType()->Date{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    let currentDate = Date()
    return currentDate
}
func getTimeAsDateType(time:String)->Date{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "hh:mm a"
    let timeAsDate = dateFormatter.date(from: time)
    return timeAsDate!
}
func getDateOnlyAsNumber()->Int{
    //    let datePicker: UIDatePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let currentDate = Date()
    let day = calendar.component(.weekday, from: currentDate)
    return day
}
func getDateOnlyAsNumbers(date:String)->[Int]{
    //    let datePicker: UIDatePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let currentDate = dateFormatter.date(from: date)
    calendar.timeZone = TimeZone(abbreviation: "UTC")!
    let day = calendar.component(.day, from: currentDate!)
    let month = calendar.component(.month, from: currentDate!)
    let year = calendar.component(.year, from: currentDate!)
    print("#### \(day),\(year),\(month)")
    return [day,year,month]
}
func getDateHoursMinuteAsNumber(isHours:Bool)->Int{
    //    let datePicker: UIDatePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let currentDate = Date()
    if isHours {
        return calendar.component(.hour, from: currentDate)
    }else{
        return calendar.component(.hour, from: currentDate)
    }
}
func getComparDate(date1:String,date2:String)->Int{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let d1 = dateFormatter.date(from: date1)
    let d2 = dateFormatter.date(from: date2)
    _ = calendar.component(.day, from: d1!)
    _ = calendar.component(.day, from: d2!)
    let components = calendar.dateComponents([.day], from: d1!, to: d2!)
    return components.day!
}
func getDateOnlyAsMillSeconde()->Int64{
    let currentDate = Date()
    
    return Int64((currentDate.timeIntervalSince1970 * 1000.0).rounded())
    //    return Int(currentDate.timeIntervalSince1970)
}
func getDateOnlyAsIntType()->Double{
    //    let datePicker: UIDatePicker = UIDatePicker()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let currentDate = Date()
    let timeInterval = currentDate.timeIntervalSince1970
    print("timeInterval \(timeInterval)")
    return  timeInterval
}

func setBorderImage(image:UIImageView){
    image.layer.borderWidth = 1
    image.layer.borderColor = UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 1.0).cgColor
}
func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}
func getInfoUser(responsEHendler:@escaping (tblUserInfo,Error?)->Void){
    let ob:tblUserInfo? = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).last
    if ob != nil{
        responsEHendler(ob!,nil)
    }else{
        responsEHendler(tblUserInfo(),nil)
    }
}
func getInfoHistoryProgressUser(responsEHendler:@escaping (tblUserProgressHistory,Error?)->Void){
    let ob:tblUserProgressHistory? = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),getDateOnly()).last
//    print("tblUserProgressHistory123",setupRealm().objects(tblUserProgressHistory.self).last)
    if ob != nil{
        responsEHendler(ob!,nil)
    }else{
        let ob:tblUserProgressHistory? = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).last

        responsEHendler(ob!,nil)
    }
}
//func getUserInfo()->tblUser{
//    let realm = try! Realm()
//    let ob:tblUser = realm.objects(tblUser.self).filter("id = %@",Int(getDataFromSheardPreferanceString(key: "userID"))!).first!
//    return ob
//}

func boldenParts(string: String, boldCharactersRanges: [[Int]], regularFont: UIFont?, boldFont: UIFont?) -> NSAttributedString {
    let attributedString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: regularFont ?? UIFont.systemFont(ofSize: 12)])
    let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: boldFont ?? UIFont.boldSystemFont(ofSize: regularFont?.pointSize ?? UIFont.systemFontSize)]
    for range in boldCharactersRanges {
        let currentRange = NSRange(location: range[0], length: range[1]-range[0]+1)
        attributedString.addAttributes(boldFontAttribute, range: currentRange)
    }
    return attributedString
}

func setupRealm()->Realm{
    
    let conf = Realm.Configuration(fileURL:inlibraryFolder(name:"encrepred.realm"),encryptionKey: getDataFromSheardPreferanceData(key: "keyEncription") , schemaVersion: 28 ,migrationBlock: { migration, oldSchemaVersion in
        if oldSchemaVersion < 24 {
            migration.enumerateObjects(ofType: tblUserInfo.className()) { oldObject, newObject in
                
                newObject!["endSubMilli"] = 0.0
                newObject!["startSubMilli"] = 0.0
                newObject!["expirDateSubscribtion"] = "0.0"
                newObject!["subDuration"] = "0"
                
                newObject?["subscribeType"] = -1
                newObject?["frute"] = "-1"
                newObject?["meat"] = "-1"
                newObject?["vegetabels"] = "-1"
                
            }
            
            migration.enumerateObjects(ofType: tblPaymentHistory.className()) { oldObject, newObject in
                newObject!["product_id"] = oldObject!["product_id"]
            }
            
            migration.enumerateObjects(ofType:tblPackageIngredients.className()) { oldObject, newObject in
                newObject?["netWeight"] = "1"
            }
            
            migration.enumerateObjects(ofType:tblSenf.className()) { oldObject, newObject in
                newObject?["ingredientName"] = ""
            }
            
            migration.enumerateObjects(ofType:tblWajbatUserMealPlanner.className()) { oldObject, newObject in
                newObject?["packgeName"] = "unknown"
                newObject?["refOrderID"] = 0
            }
            
            migration.enumerateObjects(ofType:tblShoppingList.className()) { oldObject, newObject in
                newObject?["packgeID"] = 0
            }
            
            migration.enumerateObjects(ofType:tblShoppingList.className()) { oldObject, newObject in
                newObject?["date"] = 0.0
            }
        }
    })//last 20
    return try! Realm(configuration: conf)
    
    
}


func convertEngNumToArabicNum(num: String)->String{
    //let number = NSNumber(value: Int(num)!)
    let format = NumberFormatter()
    format.locale = Locale(identifier: "ar")
    let number =   format.number(from: num)
    let faNumber = format.string(from: number!)
    
    return faNumber!
    
}
func FormatterRoundedTextField(str:RoundedTextField)->RoundedTextField{
    let NumberStr: String = str.textField.text!
    let Formatter = NumberFormatter()
    Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
    if let final = Formatter.number(from: NumberStr) {
        str.textField.text = final.stringValue
    }
    return str
}
func FormatterHoshiTextField(str:HoshiTextField)->HoshiTextField{
    let NumberStr: String = str.text!
    let Formatter = NumberFormatter()
    Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
    if let final = Formatter.number(from: NumberStr) {
        str.text = final.stringValue
    }
    return str
}
func FormatterTextField(str:UITextField)->UITextField{
    let NumberStr: String = str.text!
    let Formatter = NumberFormatter()
    Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
    if let final = Formatter.number(from: NumberStr) {
        str.text = final.stringValue
    }
    return str
}



func cutomeHintFormatPhon(country:String,text:UITextField) {
    if country == "Jordan" {
        text.placeholder = "7X XXXX XXXX"
    }else  if country == "Qatar" {
        text.placeholder = "66XX XXXX"
    }else  if country == "Kuwait" {
        text.placeholder = "5XX XXXXX"
    }else  if country == "Egypt" {
        text.placeholder = "1XX XXX XXXX"
    }else  if country == "Bahrain" {
        text.placeholder = "355 X XXXX"
    }else  if country == "United Arab Emirates" {
        text.placeholder = "50 XXX XXXX"
    }else  if country == "Palestinian Territories" {
        text.placeholder = "5X XXX XXXX"
    }else  if country == "Saudi Arabia" {
        text.placeholder = "5X XXX XXXX"
    }
}
func FormatterTextString(str:String)->String{
    let NumberStr: String = str
    let Formatter = NumberFormatter()
    Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
    if let final = Formatter.number(from: NumberStr) {
        return final.stringValue
    }
    //    return str
    return " "
}

func calclateAge(str:String)->Float {
    let startDate = str
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let formatedStartDate = dateFormatter.date(from: startDate)
    let currentDate = Date()
    let components = Set<Calendar.Component>([.year,.month,.day])
    let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate!, to: currentDate)
    print("age client \(differenceOfDate.year!) \(differenceOfDate.month!) \(differenceOfDate.day!)")
    //    if differenceOfDate.year! > 16 || differenceOfDate.year! < 16 {
    //        print("age > <")
    //        return Float(differenceOfDate.year!)
    //    }else if differenceOfDate.year! == 16 && differenceOfDate.month! > 0 || differenceOfDate.day! > 0 {
    //        print("age ==")
    //        return Float("\(differenceOfDate.year!).\(differenceOfDate.month!)")!
    //    }
    
    return round(Float("\(differenceOfDate.year!).\(differenceOfDate.month!)")!)
    
    //    return Float(differenceOfDate.year!)
}

func getNumberDaysBetweenDate(date1:String)->Int{
    let calendar = Calendar.current
    print(date1,"date2",date1)

    let dateFormatter = DateFormatter()
    dateFormatter.calendar = Calendar(identifier: .gregorian)
    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    let date1 = calendar.startOfDay(for: (dateFormatter.date(from: date1) ??  dateFormatter.date(from: getDateOnly()))!)
    let date2 = calendar.startOfDay(for: getDateOnlyasDate())
    print(date1,"date1",date2)
    let components = calendar.dateComponents([.second], from: date1, to: date2)
    return abs(components.second!)
}

func getNumberDaysBetweenDate(date1:String,date2:String)->Int{
    let calendar = Calendar.current
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    let date1 = calendar.startOfDay(for: (dateFormatter.date(from: date1) ??  dateFormatter.date(from: getDateTime()))!)
    let date2 = calendar.startOfDay(for:  (dateFormatter.date(from: date2) ??  dateFormatter.date(from: getDateTime()))!)
    
    let components = calendar.dateComponents([.minute], from: date1, to: date2)
    return abs(components.minute!)
}




func setFontText(text:UILabel,size:CGFloat){
    text.font =  UIFont(name: "GE Dinar One", size: size)!
}
func getCurrentDate(flag:Int)->String{
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    let result:String!
    let today = Date()
    if flag == 0 {
        result = formatter.string(from: today)
    }else{
        let nextDate = Calendar.current.date(byAdding: .day, value: flag, to: today)
        result = formatter.string(from: nextDate!)
        
    }
    return result
}

func ShowTost(title:String,message:String){
    CRNotifications.showNotification(type: CRNotifications.success, title: title, message: message, dismissDelay: 3, completion: {
        print("Successfully Toast")
    })
    
}


func ShowTostِError(title:String,message:String){
    CRNotifications.showNotification(type: CRNotifications.error, title: title, message: message, dismissDelay: .infinity, completion: {
        print("error Toast")
    })
    
    
}
func setArrayWater(amountDrinked:Int,amountDefualt:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    var modelData = [model]()
    for x in 0..<amountDefualt{
        if x < amountDrinked{
            modelData.append(model(laImage: "waterfill",select: true,flag: false))
        }else{
            modelData.append(model(laImage: "waternil",select: false,flag: false))
        }
    }
    responsEHendler(modelData,nil)
    
}

func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
}
func downloadImage(from url: URL,img:UIImageView) {
    print("Download Started")
    getData(from: url) { data, response, error in
        guard let data = data, error == nil else { return }
        print(response?.suggestedFilename ?? url.lastPathComponent)
        print("Download Finished")
        DispatchQueue.main.async() {
            //            let imgEdite = UIImage(data: data)!
            if let imgEdite = UIImage(data: data){
                img.image = imgEdite
            }else {
                img.image = UIImage(named: "logo.png")!
            }
            //            if let imageData =  resizeImage(image: imgEdite, targetSize: CGSize(width: 300.0, height: 600.0)).jpegData(compressionQuality: 0.0) {
            //                let bytes = imageData.count
            //                let KB = Double(bytes) / 1024.0 // Note the difference
            //                print(" KB : \(KB)")
            //            }
        }
    }
    
    //            img.image?.jpegData(compressionQuality: 10.0)
}


func downloadImage1(from url: URL) {
    print("Download Started")
    getData(from: url) { data, response, error in
        guard let data = data, error == nil else { return }
        print(response?.suggestedFilename ?? url.lastPathComponent)
        print("Download Finished")
        if let imageData = UIImage(data: data)?.pngData() {
            let bytes = imageData.count
            //let kB = Double(bytes) / 1000.0 // Note the difference
            let KB = Double(bytes) / 1024.0 // Note the difference
            print(KB)
        }
    }
}

//func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
//    let size = image.size
//
//    let widthRatio  = targetSize.width  / size.width
//    let heightRatio = targetSize.height / size.height
//
//    // Figure out what our orientation is, and use that to form the rectangle
//    var newSize: CGSize
//    if(widthRatio > heightRatio) {
//        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
//    } else {
//        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
//    }
//
//    // This is the rect that we've calculated out and this is what is actually used below
//    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
//    // Actually do the resizing to the rect using the ImageContext stuff
//    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
//    image.draw(in: rect)
//    let newImage = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//
//    return newImage!
//}
//
//func resizeImage3(image: UIImage, newWidth: CGFloat) -> UIImage {
//
//    let scale = newWidth / image.size.width
//    let newHeight = image.size.height * scale
//    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
//    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
//    let newImage = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//
//    return newImage!
//}
//
//func resizedImage1(at url: URL, for size: CGSize) -> UIImage? {
//    guard let image = UIImage(contentsOfFile: url.path) else {
//        return nil
//    }
//
//    let renderer = UIGraphicsImageRenderer(size: size)
//    return renderer.image { (context) in
//        image.draw(in: CGRect(origin: .zero, size: size))
//    }
//}


func animationviewLeft(dataView:UIView,duration:CFTimeInterval){
    
    let trans = CATransition()
    trans.type = CATransitionType.moveIn
    trans.subtype = CATransitionSubtype.fromLeft
    trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    trans.duration = duration
    dataView.layer.add(trans, forKey: nil)
}

func animationviewTop(dataView:UILabel){
    let trans = CATransition()
    trans.type = CATransitionType.moveIn
    trans.subtype = CATransitionSubtype.fromTop
    trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    trans.duration = 0.2
    dataView.layer.add(trans, forKey: nil)
}

func animationviewBottom(dataView:UILabel){
    let trans = CATransition()
    trans.type = CATransitionType.moveIn
    trans.subtype = CATransitionSubtype.fromBottom
    trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    trans.duration = 0.2
    dataView.layer.add(trans, forKey: nil)
}

func animationviewRight(dataView:UIView){
    
    let trans = CATransition()
    trans.type = CATransitionType.moveIn
    trans.subtype = CATransitionSubtype.fromRight
    trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    trans.duration = 0.2
    dataView.layer.add(trans, forKey: nil)
}



func effectButton(sender:UIButton){
    let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
    colorAnimation.fromValue = UIColor.init(red: 156, green: 171, blue: 179, alpha: 1.0).cgColor
    colorAnimation.duration = 1  // animation duration
    //    colorAnimation.autoreverses = true // optional in my case
    //    colorAnimation.repeatCount = FLT_MAX // optional in my case
    sender.layer.add(colorAnimation, forKey: "ColorPulse")
}

func effectView(sender:UIView){
    let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
    colorAnimation.fromValue = UIColor.init(red: 94, green: 204, blue: 156, alpha: 1.0).cgColor
    colorAnimation.duration = 1  // animation duration
    //    colorAnimation.autoreverses = true // optional in my case
    //    colorAnimation.repeatCount = FLT_MAX // optional in my case
    sender.layer.add(colorAnimation, forKey: "ColorPulse")
}

func effectViewButtom(sender:UIButton){
    let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
    colorAnimation.fromValue = UIColor.init(red: 94, green: 204, blue: 156, alpha: 1.0).cgColor
    colorAnimation.duration = 1  // animation duration
    //    colorAnimation.autoreverses = true // optional in my case
    //    colorAnimation.repeatCount = FLT_MAX // optional in my case
    sender.layer.add(colorAnimation, forKey: "ColorPulse")
}


func sheardPreferanceWrite(fat:Float,pro:Float,carbo:Float,energy:Float){
    print("fat \(fat) - pro \(pro) - \(carbo) - \(energy)")
    let preferences = UserDefaults.standard
    preferences.set(fat, forKey: "newfat")
    preferences.set(pro, forKey: "newpro")
    preferences.set(carbo, forKey: "newcarbo")
    preferences.set(energy, forKey: "newenergy")
    //  Save to disk
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage nutration valus")
        
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
    
}

func saveCusine(strId:String){
    setDataInSheardPreferance(value: strId, key: "cusinene")
}

func saveAllergy(strId:String){
    setDataInSheardPreferance(value:  strId, key: "allergy")
}
func saveLayaqa(strId:String){
    setDataInSheardPreferance(value:  strId, key: "refLayaqaCondID")
}

func savedietType(strId:String){
    setDataInSheardPreferance(value: strId, key: "dietType")
}
func savetypeDiabets(strId:String){
    setDataInSheardPreferance(value: strId, key: "typeDiabites")
    var dietType:String!
    if strId == "1" {
        dietType = "20"
    }else if strId == "2" {
        dietType = "21"
    }else if strId == "3" {
        dietType = "22"
    }
    savedietType(strId:dietType)
    
}

func savetMidctionDiabetsTypeOptionOne(str:String){
    setDataInSheardPreferance(value:  str, key: "typeMedication")
    
}
func savetMidctionDiabetsTypeOptionTwo(str:String){
    setDataInSheardPreferance(value:  str, key: "typeMedication2")
}

func setPersonalDataInSheardPreferance(name:String,date:String,height:String,weight:String,dateInYears:Float){
    let preferences = UserDefaults.standard
    preferences.set(name, forKey: "name")
    preferences.set(date, forKey: "txtdate")
    preferences.set(height, forKey: "txtheight")
    preferences.set(weight, forKey: "txtweight")
    preferences.set(round(dateInYears), forKey: "dateInYears")
    
    //  Save to disk
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}
func setDataInSheardPreferance(value:String,key:String){
    let preferences = UserDefaults.standard
    if value.isEmpty {
        preferences.set("0", forKey: key)
    }else{
        preferences.set(value, forKey: key)
        
    }
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence \(key)")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}

func setDataInSheardPreferanceData(value:Data,key:String){
    let preferences = UserDefaults.standard
    
    preferences.set(value, forKey: key)
    
    
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence \(key)")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}
func setDataInSheardPreferanceDic(value:[String:Any],key:String){
    let preferences = UserDefaults.standard
    
    preferences.set(value, forKey: key)
    
    
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence \(key)")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}
func setDataInSheardPreferanceInt(value:Int,key:String){
    let preferences = UserDefaults.standard
    
    preferences.set(value, forKey: key)
    
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence \(key)")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}
func setSearchLimitSheardPrefrence(value:Int,key:String){
    let preferences = UserDefaults.standard
    
    preferences.set(value, forKey: key)
    
    let didSave = preferences.synchronize()
    if didSave{
        print("save flage shaerdPrefrence \(key)")
    }
    if !didSave {
        print("Couldn't save (I've never seen this happen in real world testing")
    }
}
func getSearchLimit(key:String) -> Int{
    let defaults = UserDefaults.standard
    if  defaults.object(forKey: key) == nil{
        return 0
    }else{
        return defaults.integer(forKey: key)
    }
}
func proccessSearchLimit(id_items:String){
    if getUserInfo().subscribeType != -1 {
        if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
            let count = getSearchLimit(key: "\(getDateOnly())-\(id_items)-\(getUserInfo().id)")
            setSearchLimitSheardPrefrence(value: count+1, key: "\(getDateOnly())-\(id_items)-\(getUserInfo().id)")
        }
    }else if getUserInfo().subscribeType == -1 {
        let count = getSearchLimit(key: "\(getDateOnly())-\(id_items)-\(getUserInfo().id)")
        setSearchLimitSheardPrefrence(value: count+1, key: "\(getDateOnly())-\(id_items)-\(getUserInfo().id)")
    }
}
func alert(mes:String,selfUI:UIViewController){
    let alert = UIAlertController(title: "خطأ", message: mes, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
    selfUI.present(alert, animated: true, completion:nil)
}
func clearAllSheardPrefrence(){
    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    
    dictionary.keys.forEach { key in
        if key != "login" || key != "Key" || key != "Encrption"  {
            defaults.removeObject(forKey: key)
        }
    }
}

func getDataFromSheardPreferanceString(key:String)->String{
    let defaults = UserDefaults.standard
    if  defaults.object(forKey: key) == nil{
        return "0"
    }else{
        return defaults.string(forKey: key)!
    }
}
func getDataFromSheardPreferanceDic(key:String)->[String:Any]{
    let defaults = UserDefaults.standard
    if  defaults.object(forKey: key) == nil{
        return [:]
    }else{
        return defaults.dictionary(forKey: key)!
    }
}
func getDataFromSheardPreferanceData(key:String)->Data{
    let defaults = UserDefaults.standard
    return defaults.data(forKey: key)!
}
func getDataFromSheardPreferanceFloat(key:String)->Float{
    let defaults = UserDefaults.standard
    return defaults.float(forKey: key)
}
func getDataFromSheardPreferanceInt(key:String)->Int{
    let defaults = UserDefaults.standard
    var i:Int?
    i = defaults.integer(forKey: key)
    if i == nil {
        return 0
    }else{
        return defaults.integer(forKey: key)
    }
}
func sheardPreferanceReadFat()->Float{
    let defaults = UserDefaults.standard
    return defaults.float(forKey: "fat")
}

func sheardPreferanceReadPro()->Float{
    let defaults = UserDefaults.standard
    return defaults.float(forKey: "pro")
}

func sheardPreferanceReadCarbo()->Float{
    let defaults = UserDefaults.standard
    return defaults.float(forKey: "carbo")
}

func sheardPreferanceReadenergy()->Float{
    let defaults = UserDefaults.standard
    return defaults.float(forKey: "energy")
}


func createFinalImageText (str:String,view:UIView,flgBackground:Int) -> UIImage? {
    
    
    let image = UIImage(named: "curve.png")
    
    let viewToRender = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.width)) // here you can set the actual image width : image.size.with ?? 0 / height : image.size.height ?? 0
    
    let imgView = UIImageView(frame: viewToRender.frame)
    if flgBackground == 1 {
    imgView.image = image
    }else if flgBackground == 1 {
        imgView.backgroundColor = .white
    }else{
        imgView.backgroundColor = nil
    }
    viewToRender.addSubview(imgView)
    
    let textImgView = UIImageView(frame: viewToRender.frame)
    
    textImgView.image = imageFrom(text:str, size: viewToRender.frame.size, flgBackground: flgBackground)
    
    viewToRender.addSubview(textImgView)
    
    UIGraphicsBeginImageContextWithOptions(viewToRender.frame.size, false, 0)
    viewToRender.layer.render(in: UIGraphicsGetCurrentContext()!)
    let finalImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return finalImage
}

func imageFrom(text: String , size:CGSize,flgBackground:Int) -> UIImage {
    var foregroundColor:UIColor!
    var sizeFont:CGFloat!

    if flgBackground == 1 || flgBackground == 2{
        foregroundColor = .white
    }else{
        foregroundColor = .black
    }
    
    if flgBackground == 1 {
        sizeFont = 150
    }else if flgBackground == 2 {
        sizeFont = 150
    }else{
        sizeFont = 220
    }
    let renderer = UIGraphicsImageRenderer(size: size)
    let img = renderer.image { ctx in
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attrs = [NSAttributedString.Key.font: UIFont(name: "GE Dinar One", size: sizeFont)!, NSAttributedString.Key.foregroundColor: foregroundColor, NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        text.draw(with: CGRect(x: 0, y: size.height / 5, width: size.width, height: size.height), options: .usesLineFragmentOrigin, attributes: attrs as [NSAttributedString.Key : Any], context: nil)
        
        
    }
    return img
}


var array = tblPlanMaster.sheard.getAllDiets()
func setImageTitlePage(id:String)->String{
    if id == "\(2)" {
        return "logoLoss"
    }else if id == "\(1)"{
        return "logoGain"
    }else if id == "\(3)"{
        return "logoMaintain"
    }
    //        else if id == "\(array[3].id)"{
    //        return "pregnant"
    //    }else if id == "\(array[6].id)"{
    //        return "diabetes"
    //    }else if id == "\(array[5].id)"{
    //        return "pressure"
    //    }else if id == "\(array[10].id)"{
    //        return "ibs"
    //    }else if id == "\(array[9].id)"{
    //        return "renal"
    //    }else if id == "\(array[7].id)"{
    //        return "cholestrol"
    //    }else if id == "\(array[8].id)"{
    //        return "gout"
    //    }else if id == "\(array[4].id)"{
    //        return "lactating"
    //    }
    //    else if id == "21"{
    //        return "diabetes"
    //    }
    //    else if id == "22"{
    //        return "diabetes"
    //    }
    return ""
}


func setTextTitlePage(id:String)->String{
    if id == "\(2)" {
        return "تخسيس"
    }else if id == "\(1)"{
        return "زيادة وزن"
    }else if id == "\(3)"{
        return "محافظة على الوزن"
    }
    //        else if id == "\(array[3].id)"{
    //        return "حامل"
    //    }else if id == "\(array[6].id)"{
    //        return "السكري"
    //    }else if id == "\(array[5].id)"{
    //        return "الضغط"
    //    }else if id == "\(array[10].id)"{
    //        return "القولون العصبي"
    //    }else if id == "\(array[9].id)"{
    //        return "كلى"
    //    }else if id == "\(array[7].id)"{
    //        return "كولسترول"
    //    }else if id == "\(array[8].id)"{
    //        return "نقرس"
    //    }else if id == "\(array[4].id)"{
    //        return "مرضع"
    //    }
    //    else if id == "21"{
    //        return "سكري نوع 2"
    //    }
    //    else if id == "22"{
    //        return "سكري حوامل"
    //    }
    return ""
}

var numbersGout = ["1.0","1.1","1.2","1.3","1.4","1.5","1.6","1.7","1.8","1.9","2.0","2.1","2.2","2.3","2.4","2.5","2.6","2.7","2.8","2.9","3.0","3.1","3.2","3.3","3.4","3.5","3.6","3.7","3.8","3.9","4.0","4.1","4.2","4.3","4.4","4.5","4.6","4.7","4.8","4.9","5.0","5.1","5.2","5.3","5.4","5.5","5.6","5.7","5.8","5.9","6.0","6.1","6.2","6.3","6.4","6.5","6.6","6.7","6.8","6.9","7.0","7.1","7.2","7.3","7.4","7.5","7.6","7.7","7.8","7.9","8.0","8.1","8.2","8.3","8.4","8.5","8.6","8.7","8.8","8.9","9.0","9.1","9.2","9.3","9.4","9.5","9.6","9.7","9.8","9.9","10.0","10.1","10.2","10.3","10.4","10.5","10.6","10.7","10.8","10.9","11.0","11.1","11.2","11.3","11.4","11.5","11.6","11.7","11.8","11.9","12.0","12.1","12.2","12.3","12.4","12.5","12.6","12.7","12.8","12.9","13.0","13.1","13.2","13.3","13.4","13.5","13.6","13.7","13.8","13.9","14.0","14.1","14.2","14.3","14.4","14.5","14.6","14.7","14.8","14.9","15.0","15.1","15.2","15.3","15.4","15.5","15.6","15.7","15.8","15.9","16.0","16.1","16.2","16.3","16.4","16.5","16.6","16.7","16.8","16.9","17.0","17.1","17.2","17.3","17.4","17.5","17.6","17.7","17.8","17.9","18.0","18.1","18.2","18.3","18.4","18.5","18.6","18.7","18.8","18.9","19.0","19.1","19.2","19.3","19.4","19.5","19.6","19.7","19.8","19.9"]
var numbersCholestrole = ["50.0","50.5","51.0","51.5","52.0","52.5","53.0","53.5","54.0","54.5","55.0","55.5","56.0","56.5","57.0","57.5","58.0","58.5","59.0","59.5","60.0","60.5","61.0","61.5","62.0","62.5","63.0","63.5","64.0","64.5","65.0","65.5","66.0","66.5","67.0","67.5","68.0","68.5","69.0","69.5","70.0","70.5","71.0","71.5","72.0","72.5","73.0","73.5","74.0","74.5","75.0","75.5","76.0","76.5","77.0","77.5","78.0","78.5","79.0","79.5","80.0","80.5","81.0","81.5","82.0","82.5","83.0","83.5","84.0","84.5","85.0","85.5","86.0","86.5","87.0","87.5","88.0","88.5","89.0","89.5","90.0","90.5","91.0","91.5","92.0","92.5","93.0","93.5","94.0","94.5","95.0","95.5","96.0","96.5","97.0","97.5","98.0","98.5","99.0","99.5","100.0","100.5","101.0","101.5","102.0","102.5","103.0","103.5","104.0","104.5","105.0","105.5","106.0","106.5","107.0","107.5","108.0","108.5","109.0","109.5","110.0","110.5","111.0","111.5","112.0","112.5","113.0","113.5","114.0","114.5","115.0","115.5","116.0","116.5","117.0","117.5","118.0","118.5","119.0","119.5","120.0","120.5","121.0","121.5","122.0","122.5","123.0","123.5","124.0","124.5","125.0","125.5","126.0","126.5","127.0","127.5","128.0","128.5","129.0","129.5","130.0","130.5","131.0","131.5","132.0","132.5","133.0","133.5","134.0","134.5","135.0","135.5","136.0","136.5","137.0","137.5","138.0","138.5","139.0","139.5","140.0","140.5","141.0","141.5","142.0","142.5","143.0","143.5","144.0","144.5","145.0","145.5","146.0","146.5","147.0","147.5","148.0","148.5","149.0","149.5","150.0","150.5","151.0","151.5","152.0","152.5","153.0","153.5","154.0","154.5","155.0","155.5","156.0","156.5","157.0","157.5","158.0","158.5","159.0","159.5","160.0","160.5","161.0","161.5","162.0","162.5","163.0","163.5","164.0","164.5","165.0","165.5","166.0","166.5","167.0","167.5","168.0","168.5","169.0","169.5","170.0","170.5","171.0","171.5","172.0","172.5","173.0","173.5","174.0","174.5","175.0","175.5","176.0","176.5","177.0","177.5","178.0","178.5","179.0","179.5","180.0","180.5","181.0","181.5","182.0","182.5","183.0","183.5","184.0","184.5","185.0","185.5","186.0","186.5","187.0","187.5","188.0","188.5","189.0","189.5","190.0","190.5","191.0","191.5","192.0","192.5","193.0","193.5","194.0","194.5","195.0","195.5","196.0","196.5","197.0","197.5","198.0","198.5","199.0","199.5","200.0","200.5","201.0","201.5","202.0","202.5","203.0","203.5","204.0","204.5","205.0","205.5","206.0","206.5","207.0","207.5","208.0","208.5","209.0","209.5","210.0","210.5","211.0","211.5","212.0","212.5","213.0","213.5","214.0","214.5","215.0","215.5","216.0","216.5","217.0","217.5","218.0","218.5","219.0","219.5","220.0","220.5","221.0","221.5","222.0","222.5","223.0","223.5","224.0","224.5","225.0","225.5","226.0","226.5","227.0","227.5","228.0","228.5","229.0","229.5","230.0","230.5","231.0","231.5","232.0","232.5","233.0","233.5","234.0","234.5","235.0","235.5","236.0","236.5","237.0","237.5","238.0","238.5","239.0","239.5","240.0","240.5","241.0","241.5","242.0","242.5","243.0","243.5","244.0","244.5","245.0","245.5","246.0","246.5","247.0","247.5","248.0","248.5","249.0","249.5","250.0","250.5","251.0","251.5","252.0","252.5","253.0","253.5","254.0","254.5","255.0","255.5","256.0","256.5","257.0","257.5","258.0","258.5","259.0","259.5","260.0","260.5","261.0","261.5","262.0","262.5","263.0","263.5","264.0","264.5","265.0","265.5","266.0","266.5","267.0","267.5","268.0","268.5","269.0","269.5","270.0","270.5","271.0","271.5","272.0","272.5","273.0","273.5","274.0","274.5","275.0","275.5","276.0","276.5","277.0","277.5","278.0","278.5","279.0","279.5","280.0","280.5","281.0","281.5","282.0","282.5","283.0","283.5","284.0","284.5","285.0","285.5","286.0","286.5","287.0","287.5","288.0","288.5","289.0","289.5","290.0","290.5","291.0","291.5","292.0","292.5","293.0","293.5","294.0","294.5","295.0","295.5","296.0","296.5","297.0","297.5","298.0","298.5","299.0","299.5","300.0","300.5","301.0","301.5","302.0","302.5","303.0","303.5","304.0","304.5","305.0","305.5","306.0","306.5","307.0","307.5","308.0","308.5","309.0","309.5","310.0","310.5","311.0","311.5","312.0","312.5","313.0","313.5","314.0","314.5","315.0","315.5","316.0","316.5","317.0","317.5","318.0","318.5","319.0","319.5","320.0","320.5","321.0","321.5","322.0","322.5","323.0","323.5","324.0","324.5","325.0","325.5","326.0","326.5","327.0","327.5","328.0","328.5","329.0","329.5","330.0","330.5","331.0","331.5","332.0","332.5","333.0","333.5","334.0","334.5","335.0","335.5","336.0","336.5","337.0","337.5","338.0","338.5","339.0","339.5","340.0","340.5","341.0","341.5","342.0","342.5","343.0","343.5","344.0","344.5","345.0","345.5","346.0","346.5","347.0","347.5","348.0","348.5","349.0","349.5","350.0","350.5","351.0","351.5","352.0","352.5","353.0","353.5","354.0","354.5","355.0","355.5","356.0","356.5","357.0","357.5","358.0","358.5","359.0","359.5","360.0","360.5","361.0","361.5","362.0","362.5","363.0","363.5","364.0","364.5","365.0","365.5","366.0","366.5","367.0","367.5","368.0","368.5","369.0","369.5","370.0","370.5","371.0","371.5","372.0","372.5","373.0","373.5","374.0","374.5","375.0","375.5","376.0","376.5","377.0","377.5","378.0","378.5","379.0","379.5","380.0","380.5","381.0","381.5","382.0","382.5","383.0","383.5","384.0","384.5","385.0","385.5","386.0","386.5","387.0","387.5","388.0","388.5","389.0","389.5","390.0","390.5","391.0","391.5","392.0","392.5","393.0","393.5","394.0","394.5","395.0","395.5","396.0","396.5","397.0","397.5","398.0","398.5","399.0","399.5","400.0","400.5","401.0","401.5","402.0","402.5","403.0","403.5","404.0","404.5","405.0","405.5","406.0","406.5","407.0","407.5","408.0","408.5","409.0","409.5","410.0","410.5","411.0","411.5","412.0","412.5","413.0","413.5","414.0","414.5","415.0","415.5","416.0","416.5","417.0","417.5","418.0","418.5","419.0","419.5","420.0","420.5","421.0","421.5","422.0","422.5","423.0","423.5","424.0","424.5","425.0","425.5","426.0","426.5","427.0","427.5","428.0","428.5","429.0","429.5","430.0","430.5","431.0","431.5","432.0","432.5","433.0","433.5","434.0","434.5","435.0","435.5","436.0","436.5","437.0","437.5","438.0","438.5","439.0","439.5","440.0","440.5","441.0","441.5","442.0","442.5","443.0","443.5","444.0","444.5","445.0","445.5","446.0","446.5","447.0","447.5","448.0","448.5","449.0","449.5","450.0","450.5","451.0","451.5","452.0","452.5","453.0","453.5","454.0","454.5","455.0","455.5","456.0","456.5","457.0","457.5","458.0","458.5","459.0","459.5","460.0","460.5","461.0","461.5","462.0","462.5","463.0","463.5","464.0","464.5","465.0","465.5","466.0","466.5","467.0","467.5","468.0","468.5","469.0","469.5","470.0","470.5","471.0","471.5","472.0","472.5","473.0","473.5","474.0","474.5","475.0","475.5","476.0","476.5","477.0","477.5","478.0","478.5","479.0","479.5","480.0","480.5","481.0","481.5","482.0","482.5","483.0","483.5","484.0","484.5","485.0","485.5","486.0","486.5","487.0","487.5","488.0","488.5","489.0","489.5","490.0","490.5","491.0","491.5","492.0","492.5","493.0","493.5","494.0","494.5","495.0","495.5","496.0","496.5","497.0","497.5","498.0","498.5","499.0","499.5","500.0","500.5","501.0","501.5","502.0","502.5","503.0","503.5","504.0","504.5","505.0","505.5","506.0","506.5","507.0","507.5","508.0","508.5","509.0","509.5","510.0","510.5","511.0","511.5","512.0","512.5","513.0","513.5","514.0","514.5","515.0","515.5","516.0","516.5","517.0","517.5","518.0","518.5","519.0","519.5","520.0","520.5","521.0","521.5","522.0","522.5","523.0","523.5","524.0","524.5","525.0","525.5","526.0","526.5","527.0","527.5","528.0","528.5","529.0","529.5","530.0","530.5","531.0","531.5","532.0","532.5","533.0","533.5","534.0","534.5","535.0","535.5","536.0","536.5","537.0","537.5","538.0","538.5","539.0","539.5","540.0","540.5","541.0","541.5","542.0","542.5","543.0","543.5","544.0","544.5","545.0","545.5","546.0","546.5","547.0","547.5","548.0","548.5","549.0","549.5","550.0","550.5","551.0","551.5","552.0","552.5","553.0","553.5","554.0","554.5","555.0","555.5","556.0","556.5","557.0","557.5","558.0","558.5","559.0","559.5","560.0","560.5","561.0","561.5","562.0","562.5","563.0","563.5","564.0","564.5","565.0","565.5","566.0","566.5","567.0","567.5","568.0","568.5","569.0","569.5","570.0","570.5","571.0","571.5","572.0","572.5","573.0","573.5","574.0","574.5","575.0","575.5","576.0","576.5","577.0","577.5","578.0","578.5","579.0","579.5","580.0","580.5","581.0","581.5","582.0","582.5","583.0","583.5","584.0","584.5","585.0","585.5","586.0","586.5","587.0","587.5","588.0","588.5","589.0","589.5","590.0","590.5","591.0","591.5","592.0","592.5","593.0","593.5","594.0","594.5","595.0","595.5","596.0","596.5","597.0","597.5","598.0","598.5","599.0","599.5","600.0","600.5","601.0","601.5","602.0","602.5","603.0","603.5","604.0","604.5","605.0","605.5","606.0","606.5","607.0","607.5","608.0","608.5","609.0","609.5","610.0","610.5","611.0","611.5","612.0","612.5","613.0","613.5","614.0","614.5","615.0","615.5","616.0","616.5","617.0","617.5","618.0","618.5","619.0","619.5","620.0","620.5","621.0","621.5","622.0","622.5","623.0","623.5","624.0","624.5","625.0","625.5","626.0","626.5","627.0","627.5","628.0","628.5","629.0","629.5","630.0","630.5","631.0","631.5","632.0","632.5","633.0","633.5","634.0","634.5","635.0","635.5","636.0","636.5","637.0","637.5","638.0","638.5","639.0","639.5","640.0","640.5","641.0","641.5","642.0","642.5","643.0","643.5","644.0","644.5","645.0","645.5","646.0","646.5","647.0","647.5","648.0","648.5","649.0","649.5","650.0","650.5","651.0","651.5","652.0","652.5","653.0","653.5","654.0","654.5","655.0","655.5","656.0","656.5","657.0","657.5","658.0","658.5","659.0","659.5","660.0","660.5","661.0","661.5","662.0","662.5","663.0","663.5","664.0","664.5","665.0","665.5","666.0","666.5","667.0","667.5","668.0","668.5","669.0","669.5","670.0","670.5","671.0","671.5","672.0","672.5","673.0","673.5","674.0","674.5","675.0","675.5","676.0","676.5","677.0","677.5","678.0","678.5","679.0","679.5","680.0","680.5","681.0","681.5","682.0","682.5","683.0","683.5","684.0","684.5","685.0","685.5","686.0","686.5","687.0","687.5","688.0","688.5","689.0","689.5","690.0","690.5","691.0","691.5","692.0","692.5","693.0","693.5","694.0","694.5","695.0","695.5","696.0","696.5","697.0","697.5","698.0","698.5","699.0","699.5","700.0"]



class EdgeInsetLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let textRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                                          left: -textInsets.left,
                                          bottom: -textInsets.bottom,
                                          right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}


func setUpNotification(flagNotfy:Bool){ // save in local database info notfy
    if flagNotfy { // active
        print("flagNotfy \(flagNotfy)")
        tblAlermICateItems.setalertNotification(hour:9,min:0,cateItem:"1", isActive: "0")
        tblAlermICateItems.setalertNotification(hour:16,min:0,cateItem:"2", isActive: "0")
        tblAlermICateItems.setalertNotification(hour:21,min:0,cateItem:"3", isActive: "0")
    }else{
        print("flagNotfy1 \(flagNotfy)")
        tblAlermICateItems.setalertNotification(hour:9,min:0,cateItem:"1", isActive: "1")
        tblAlermICateItems.setalertNotification(hour:16,min:0,cateItem:"2", isActive: "1")
        tblAlermICateItems.setalertNotification(hour:21,min:0,cateItem:"3", isActive: "1")
    }
    tblAlermICateItems.setalertNotification(hour:12,min:0,cateItem:"4", isActive: "1")
    tblAlermICateItems.setalertNotification(hour:19,min:0,cateItem:"5", isActive: "1")
    tblAlermICateItems.setalertNotification(hour:19,min:30,cateItem:"6", isActive: "1")
    tblAlermICateItems.setalertNotification(hour:15,min:0,cateItem:"7", isActive: "1")
    tblAlermICateItems.setalertNotification(hour:20,min:0,cateItem:"8", isActive: "1")
    
    //            let  BreaktitleNotfy =  getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
    //
    //            let  launchtitleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
    //
    //            let  dinnertitleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
    //
    //            let  weighttitleNotfy =  getTitleNotify(keyRemote:"weightRemindersTitles",keyValue:"weightTitle",sheardKey:"weightTitleCounter")
    //
    //            let exercisetitleNotfy =  getTitleNotify(keyRemote:"exerciseRemindersTitles",keyValue:"exerciseTitle",sheardKey:"exerciseTitleCounter")
    
    //            showNotify(id:"1",body:BreaktitleNotfy,hour:9,minut:0)
    //            showNotify(id:"2",body:launchtitleNotfy,hour:16,minut:0)
    //            showNotify(id:"3",body:dinnertitleNotfy,hour:21,minut:0)
    //            showNotify(id:"6",body:exercisetitleNotfy,hour:19,minut:30)
    //            showNotify(id:"8",body:weighttitleNotfy,hour:20,minut:0)
}

func showNotify(id:String,body:String,hour:Int,minut:Int)   {
    
    let content = UNMutableNotificationContent()
    content.title = "للتذكير"
    content.body = body
    content.sound = .default
    var dateComponents = DateComponents()
    dateComponents.calendar = Calendar.current
    content.userInfo = ["alarm":id]
    if id == "8" { // weight
        dateComponents.weekday = getDateOnlyAsNumber()
    }
    if id == "9" {
        dateComponents.day = getDateOnlyAsNumber()
        
    }
    dateComponents.hour = hour
    dateComponents.minute = minut
    content.categoryIdentifier = "SYNC_CATEGORY"
    // Create the trigger as a repeating event.
    let trigger = UNCalendarNotificationTrigger(
        dateMatching: dateComponents, repeats: true)
    //    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 172800, repeats: false)
    
    //        let uuidString = UUID().uuidString
    let request = UNNotificationRequest(identifier: id,
                                        content: content, trigger: trigger)
    let syncAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                                          title: "دخول",
                                          options: UNNotificationActionOptions(rawValue: 0))
    let syncCategory =
        UNNotificationCategory(identifier: "SYNC_CATEGORY",
                               actions: [syncAction],
                               intentIdentifiers: [],
                               hiddenPreviewsBodyPlaceholder: body,
                               options: .customDismissAction)
    let notificationCenter = UNUserNotificationCenter.current()
    notificationCenter.setNotificationCategories([syncCategory])
    notificationCenter.add(request)
    
    // Schedule the request with the system.
    //        let notificationCenter = UNUserNotificationCenter.current()
    
    
    //        notificationCenter.add(request) { (error) in
    //            if error != nil {
    //                print("error")
    //            }else{
    //                self.Syncss { (response,error) in
    //
    //                    print("OK(((((((\((response!)))))))))))))^^^^^^^^&&&&&&&&&&&%%%%%%%")
    //                }
    //
    //            }
    //        }
    
    
}




func showNotifyGeneral(id:String,body:String){
    
    let content = UNMutableNotificationContent()
    content.title = "للتذكير"
    content.body = body
    content.sound = .default
    content.userInfo = ["alarm":id ]
    
    content.categoryIdentifier = "general_caegory"
    // Create the trigger as a repeating event.
    
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 172800, repeats: false)
    
    let request = UNNotificationRequest(identifier: id ,
                                        content: content, trigger: trigger)
    let syncAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                                          title: "دخول",
                                          options: UNNotificationActionOptions(rawValue: 0))
    
    let syncCategory =
        UNNotificationCategory(identifier: "general_caegory",
                               actions: [syncAction],
                               intentIdentifiers: [],
                               hiddenPreviewsBodyPlaceholder: body,
                               options: .customDismissAction)
    let notificationCenter = UNUserNotificationCenter.current()
    notificationCenter.setNotificationCategories([syncCategory])
    notificationCenter.add(request)
    
}

func getTitleNotify(keyRemote:String,keyValue:String,sheardKey:String)->String{
    var titleNotfy = ""
    DispatchQueue.global().sync {
        var count = getDataFromSheardPreferanceInt(key: sheardKey)
        
        if let recommends = JSON(remoteConfig[keyRemote].jsonValue ??  "تخسيس").array {
            titleNotfy = recommends[count][keyValue].stringValue
        }
        
        count+=1
        if count > 4{
            count = 0
        }
        setDataInSheardPreferanceInt(value:count, key: sheardKey)
    }
    return titleNotfy
}


func inlibraryFolder(name:String)->URL{
    return URL(fileURLWithPath:NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0],isDirectory: true).appendingPathComponent(name)
    
}


let colors:[UIColor] = [UIColor(hexString:"#DFC824"),UIColor(hexString : "#64DFA6"),UIColor(hexString : "#3F51B5"),UIColor(hexString : "#4781E3"),UIColor(hexString :"#ffff4444"),UIColor(hexString : "#008000" ),UIColor(hexString : "#ffaa66cc"),UIColor(hexString : "#B00020"),UIColor(hexString : "#ffff8800"),UIColor(hexString : "#00BCD4"),UIColor(hexString : "#26A69A"),UIColor(hexString : "#5C6BC0")]


let dateFormatter1 = DateFormatter()
let dateFormatter = DateFormatter()

func updateWeightUser(date:String,new_weight:String,view:UIView,completion: @escaping (Bool) -> Void){
    //    var userItem:tblUserInfo!
    tblUserHistory.setInsertNewWeight(iduser: getDataFromSheardPreferanceString(key: "userID"), h: "nil",w: new_weight,date: date)
    completion(true)
    
    //    getInfoUser { (user, err) in
    //        userItem = user
    //    }
    //    dateFormatter1.dateFormat = "yyyy-MM-dd"
    //    dateFormatter.dateFormat = "dd - MM - yyyy"
    //
    //    let d = dateFormatter1.date(from: date)
    //
    //    let newFormate = dateFormatter.string(from: d!)
    //    db.collection("UserProgressHistory").document(getDataFromSheardPreferanceString(key: "userID")).collection(getDataFromSheardPreferanceString(key: "userID")).document(newFormate).setData(
    //        [
    //            "date": newFormate,
    //            "height": userItem.height,
    //            "id": "0",
    //            "refUserId": getDataFromSheardPreferanceString(key: "userID"),
    //            "weight": new_weight,
    //
    //
    //    ]){ err in
    //        if err != nil {
    //            showToast(message:"لم يتم تعديل الوزن", view: view,place:0)
    //            print("faliur")
    //            completion(false)
    //        } else {
    //            showToast(message:"تم تعديل الوزن", view: view,place:0)
    //            completion(true)
    //            print("success")
    //
    //        }
    //
    //    }
}



func showToast(message : String,view:UIView,place:Int) {
    
//    let window = UIApplication.shared.connectedScenes
//        .filter({$0.activationState == .foregroundActive})
//        .map({$0 as? UIWindowScene})
//        .compactMap({$0})
//        .first?.windows
//        .filter({$0.isKeyWindow}).first
    let view:UIView!
    if place == 1 { //in half screen
        view = UIView(frame:CGRect(x: 16,y: UIScreen.main.bounds.size.height/2,width: UIScreen.main.bounds.size.width-40,height: 40))
    }else{ //in below screen
        view = UIView(frame:CGRect(x: 0,y: UIScreen.main.bounds.size.height-100,width: UIScreen.main.bounds.size.width-40,height: 40))
    }
    
    
    let toastLabel = UILabel(frame: CGRect(x: 16, y: view.frame.size.height-100, width: view.frame.width, height: view.frame.height+100))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = UIFont(name: "GE Dinar One", size: 18)!
    toastLabel.textAlignment = .center;
    toastLabel.text = "\n\(message)\n"
    toastLabel.numberOfLines = 6
    toastLabel.translatesAutoresizingMaskIntoConstraints = false
    toastLabel.lineBreakMode = .byWordWrapping
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.center.x = view.center.x
    view.center.x = window?.center.x ?? 0.0
    
    view.addSubview(toastLabel)
    NSLayoutConstraint.activate([
        toastLabel.topAnchor.constraint(equalTo: view.topAnchor),
        toastLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        toastLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor)
    ])
    window?.addSubview(view);
    UIView.animate(withDuration: 2.0, delay: 0.9, options: .curveEaseOut, animations: {
                    toastLabel.alpha = 0.0 }, completion: {(isCompleted) in view.removeFromSuperview()
                    })
}

func showToastNutritional(message : String,view:UIView,place:Int,frame:CGRect,item:UIStackView) {
    
    let toastLabel = UILabel()
    toastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.9)
    toastLabel.textColor = UIColor.white
    toastLabel.font = UIFont(name: "GE Dinar One", size: 13)!
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.tag = 899
    toastLabel.numberOfLines = 0
    toastLabel.translatesAutoresizingMaskIntoConstraints = false
    toastLabel.lineBreakMode = .byWordWrapping
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.center = view.center
    
    view.addSubview(toastLabel)
    
    toastLabel.center.y = item.center.y
    toastLabel.center.y -= item.bounds.height
    UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear], animations: {
        toastLabel.center.y += item.bounds.height
        item.layoutIfNeeded()
    }, completion: nil)
    NSLayoutConstraint.activate([
        toastLabel.widthAnchor.constraint(equalToConstant: 300),
        toastLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        toastLabel.centerYAnchor.constraint(equalTo: item.centerYAnchor)
    ])
    
}


func isPaymentRun(completion:@escaping (String)->Void){
   
    let path = "http://dev.tele-ent.com/TE-WebServices/diet/GetiosPaymentStatus"
    AF.request(path).responseJSON { response in
        switch response.result {
        
        case .success( _):
            if let json = response.data {
                do{
                    let data = try JSON(data: json).arrayValue
                    if "\(data[0]["status"])" == "1"{
                        NSLog("isPaymentRun = 1")
                        completion("\(data[0]["status"])")
                    }else{
                        NSLog("isPaymentRun = 0")
                        completion("\(data[0]["status"])")
                    }
                }
                catch{
                    completion("1")
                    print("JSON Error")
                }
                
            }
        case .failure(let error):
            completion("1")
            print(error)
        }
    }
}

func get3Digit(appVersionstr:[String])->Int{
    var num = ""
    for x in 0..<appVersionstr.count {
        print(appVersionstr,"x",x)
        if x == 3 {
            break
        }
        num.append(appVersionstr[x])
    }
    print("get3Digit",num)
    if num == "" {
        num = String(filterVersionApp(str:(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!))
    }
    return Int(num)!
}
func checkLatstVersion(view:UIViewController,responsEHendler:@escaping (String,String)->Void){
    let appVersion = filterVersionApp(str:(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)!)
    let appVersionStr = String(appVersion)
    let arr_str_version:[String] = appVersionStr.map { String($0)}
    print("arr_str_version",arr_str_version)
    let num_app_versoin = get3Digit(appVersionstr:arr_str_version)
    print("appVersion \(appVersion)")
    let path = "http://dev.tele-ent.com/TE-WebServices/dietapp/GetLatestVersion?type=IOS&key=WfJGygCfEu6n5ZhhhucT"
    AF.request(path).responseJSON { response in
        switch response.result {
        
        case .success( _):
            if let json = response.data {
                do{
                    let data = try JSON(data: json).arrayValue
                    if get3Digit(appVersionstr: "\(filterVersionApp(str:"\(data[0]["latestVersionMajor"])"))".map { String($0)}) > num_app_versoin {
                        
                        responsEHendler("major","\(data[0]["latestVersionMajor"])")
                    }else if get3Digit(appVersionstr: "\(filterVersionApp(str:"\(data[0]["latestVersionMinor"])"))".map { String($0)}) > num_app_versoin {
                      
                        responsEHendler("NOTMajor","\(data[0]["latestVersionMinor"])")
                    }else {
                        NSLog("not found ant update")
                    }
                }catch{
                    print("JSON Error")
                }
                
            }
        case .failure(let error):
            print(error)
        }
    }
}


func filterVersionApp(str:String)->Int{
    let str2 = str.replacingOccurrences(of: ".", with: "", options:.literal, range: nil)
    return Int(str2)!
}


func showAlertMajor(view:UIViewController,isMajor:Bool,version:String){
    
    
    let alertController = UIAlertController(title: "إصدار رقم :\(version)", message: "توفر إصدار جديد،يرجى تحديث التطبيق", preferredStyle: .alert)
    
    alertController.setValue(NSAttributedString(string: alertController.message!, attributes: [NSAttributedString.Key.font : UIFont(name: "GE Dinar One", size: 17)!, NSAttributedString.Key.foregroundColor : colorGray]), forKey: "attributedMessage")
    alertController.setValue(NSAttributedString(string: alertController.title!, attributes: [NSAttributedString.Key.font : UIFont(name: "GE Dinar One", size: 18)!, NSAttributedString.Key.foregroundColor : colorGray]), forKey: "attributedTitle")
    // Create the actions
    let okAction = UIAlertAction(title: "تحديث", style: UIAlertAction.Style.default) {
        UIAlertAction in
        goToAppStore()
        NSLog("Update")
    }
    let cancelAction = UIAlertAction(title: "إلغاء", style: UIAlertAction.Style.cancel) {
        UIAlertAction in
        NSLog("Cancel Pressed")
    }
    
    
    // Add the actions
    alertController.addAction(okAction)
    if isMajor == false {
        alertController.addAction(cancelAction)
    }
    // Present the controller
    view.present(alertController, animated: true, completion: nil)
}
var userCountry = ""

func readLocalRemoteConfg()->NSDictionary{
    var nsDictionary: NSDictionary?
    if let path = Bundle.main.path(forResource: "RemoteConfigDefaults", ofType: "plist") {
        nsDictionary = NSDictionary(contentsOfFile: path)
    }
    return nsDictionary!
}

func getIpLocation(completion: @escaping(String) -> Void)
{
//    let url = URL(string: "http://ip-api.com/json")!
    
    AF.request("http://dev.tele-ent.com/TE-WebServices/diet/getCountry",method:.post,  parameters:nil,encoding:URLEncoding.default, headers:header).responseJSON{
            response in
        switch response.result {
        case .success(let value):
                if let recommends = value as? [String: Any] {
                    print("AF.request",recommends["country"] as! String)
                    completion(recommends["country"] as! String)
                }
            case.failure(let error):
                completion("unknown")
                print("error AF.request \(error)")
            }

        }
//    var request = URLRequest(url: url)
//    request.httpMethod = "GET"
//
//    URLSession.shared.dataTask(with: request as URLRequest, completionHandler:
//                                { (data, response, error) in
//                                    DispatchQueue.main.async
//                                    {
//                                        if let content = data
//                                        {
//                                            do
//                {
//                    if let object = try JSONSerialization.jsonObject(with: content, options: .allowFragments) as? NSDictionary
//                    {
//                        completion(object, error)
//                    }
//                    else
//                    {
//                        // TODO: Create custom error.
//                        completion(nil, nil)
//                    }
//                }
//                                            catch
//                                            {
//                                                // TODO: Create custom error.
//                                                completion(nil, nil)
//                                            }
//                                        }
//                                        else
//                                        {
//                                            completion(nil, error)
//                                        }
//                                    }
//                                }).resume()
}

func goToAppStore(){
    guard let url = URL(string: "itms-apps://apple.com/app/id1537778153") else {
        return //be safe
    }
    
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    } else {
        UIApplication.shared.openURL(url)
    }
}


let modelNameDevice: String = {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    let identifier = machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
    
    func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
        #if os(iOS)
        switch identifier {
        case "iPod5,1":                                 return "iPod touch (5th generation)"
        case "iPod7,1":                                 return "iPod touch (6th generation)"
        case "iPod9,1":                                 return "iPod touch (7th generation)"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPhone12,1":                              return "iPhone 11"
        case "iPhone12,3":                              return "iPhone 11 Pro"
        case "iPhone12,5":                              return "iPhone 11 Pro Max"
        case "iPhone12,8":                              return "iPhone SE (2nd generation)"
        case "iPhone13,1":                              return "iPhone 12 mini"
        case "iPhone13,2":                              return "iPhone 12"
        case "iPhone13,3":                              return "iPhone 12 Pro"
        case "iPhone13,4":                              return "iPhone 12 Pro Max"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
        case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
        case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
        case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
        case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
        case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
        case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
        case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
        case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
        case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
        case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "AudioAccessory5,1":                       return "HomePod mini"
        case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
        default:                                        return identifier
        }
        #elseif os(tvOS)
        switch identifier {
        case "AppleTV5,3": return "Apple TV 4"
        case "AppleTV6,2": return "Apple TV 4K"
        case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
        default: return identifier
        }
        #endif
    }
    
    return mapToDevice(identifier: identifier)
}()




extension StringProtocol  {
    var digits: [Int] { compactMap(\.wholeNumberValue) }
}
