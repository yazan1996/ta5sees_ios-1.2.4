//
//  CircularProgressBar.swift
//  Ta5sees
//
//  Created by Admin on 5/8/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

public class CircularProgressBar: CALayer {
    
    private var circularPath: UIBezierPath!
    private var innerTrackShapeLayer: CAShapeLayer!
    private var outerTrackShapeLayer: CAShapeLayer!
    private let rotateTransformation = CATransform3DMakeRotation(90.0 / 180.0 * -.pi, 0.0, 0.0, 1.0)
    private var completedLabel: UILabel!
    private var completedLabel1: UILabel!
    public var progressLabel: UILabel!
    public var isUsingAnimation: Bool!
    public var progress: CGFloat = 0 {
        didSet {
            progressLabel.font = UIFont.init(name: "GE Dinar One", size: 40)
//            if progress < 0 {
//            progressLabel.text = "\(0)"
//            }else{
                progressLabel.text = "\(abs(Int(progress)))"
//            }
            progressLabel.textAlignment = .center
        }
        
    }
    public var progressEate: CGFloat = 0 {
        didSet {
            innerTrackShapeLayer.strokeEnd = CGFloat(Double(progressEate) / Double(progress+progressEate))

        }
        
    }
    public var str: String = "" {
           didSet {
            completedLabel.text = str

           }
           
       }
    public init(radius: CGFloat, position: CGPoint, innerTrackColor: UIColor, outerTrackColor: UIColor, lineWidth: CGFloat) {
        super.init()
        
        circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        outerTrackShapeLayer = CAShapeLayer()
        outerTrackShapeLayer.path = circularPath.cgPath
        outerTrackShapeLayer.position = position
        outerTrackShapeLayer.strokeColor = outerTrackColor.cgColor
        outerTrackShapeLayer.fillColor = UIColor.clear.cgColor
        outerTrackShapeLayer.lineWidth = lineWidth
        outerTrackShapeLayer.strokeEnd = 1
        outerTrackShapeLayer.lineCap = CAShapeLayerLineCap.round
        outerTrackShapeLayer.transform = rotateTransformation
        addSublayer(outerTrackShapeLayer)
        
        innerTrackShapeLayer = CAShapeLayer()
        innerTrackShapeLayer.strokeColor = innerTrackColor.cgColor
        innerTrackShapeLayer.position = position
        //        innerTrackShapeLayer.strokeEnd = progress
        innerTrackShapeLayer.lineWidth = lineWidth
        innerTrackShapeLayer.lineCap = CAShapeLayerLineCap.round
        innerTrackShapeLayer.fillColor = UIColor.clear.cgColor
        innerTrackShapeLayer.path = circularPath.cgPath
        innerTrackShapeLayer.transform = rotateTransformation
        addSublayer(innerTrackShapeLayer)
        
        progressLabel = UILabel()
        let size = CGSize(width: 100, height: 100)
        let origin = CGPoint(x: position.x, y: position.y)
        progressLabel.frame = CGRect(origin: origin, size: size)
        progressLabel.center = position
        progressLabel.center.y = position.y-20
        progressLabel.font = UIFont.init(name: "GE Dinar One", size: 30)
        progressLabel.text = "0"
        progressLabel.textColor = .white
        progressLabel.textAlignment = .center
        progressLabel.translatesAutoresizingMaskIntoConstraints = false
        
        insertSublayer(progressLabel.layer, at: 0)
        
        completedLabel1 = UILabel()
        let completedLabelOrigin = CGPoint(x: position.x , y: position.y)
        completedLabel1.frame = CGRect(origin: completedLabelOrigin, size: CGSize.init(width: radius, height: radius * 0.6))
        completedLabel1.font = UIFont.systemFont(ofSize: radius * 0.2, weight: UIFont.Weight.light)
        completedLabel1.textAlignment = .center
        completedLabel1.font = UIFont.init(name: "GE Dinar One", size: radius * 0.2)
        completedLabel1.center = position
        completedLabel1.center.y = position.y+20
        completedLabel1.textColor = .white
//        completedLabel1.numberOfLines = 0
//        completedLabel1.lineBreakMode = .byWordWrapping
        completedLabel1.text =  "سعرة حرارية"
        insertSublayer(completedLabel1.layer, at: 0)

        completedLabel = UILabel()
        let completedLabelOrigin1 = CGPoint(x: position.x , y: position.y)
        completedLabel.frame = CGRect(origin: completedLabelOrigin1, size: CGSize.init(width: radius, height: radius * 0.6))
        completedLabel.font = UIFont.systemFont(ofSize: radius * 0.2, weight: UIFont.Weight.light)
        completedLabel.textAlignment = .center
        completedLabel.font = UIFont.init(name: "GE Dinar One", size: 20)
        completedLabel.center = position
        completedLabel.center.y = position.y+50
        completedLabel.textColor = .white
        completedLabel.numberOfLines = 0
        completedLabel.lineBreakMode = .byWordWrapping
        completedLabel.text =  "متبقي"
        insertSublayer(completedLabel.layer, at: 0)
        //
    }
    
    
    public init(radius: CGFloat,position: CGPoint, innerTrackColor: UIColor, outerTrackColor: UIColor, lineWidth: CGFloat,colorLable:UIColor) {
        super.init()
        
        circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        outerTrackShapeLayer = CAShapeLayer()
        outerTrackShapeLayer.path = circularPath.cgPath
        outerTrackShapeLayer.position = position
        outerTrackShapeLayer.strokeColor = outerTrackColor.cgColor
        outerTrackShapeLayer.fillColor = UIColor.clear.cgColor
        outerTrackShapeLayer.lineWidth = lineWidth
        outerTrackShapeLayer.strokeEnd = 1
        outerTrackShapeLayer.lineCap = CAShapeLayerLineCap.round
        outerTrackShapeLayer.transform = rotateTransformation
        addSublayer(outerTrackShapeLayer)
        
        innerTrackShapeLayer = CAShapeLayer()
        innerTrackShapeLayer.strokeColor = innerTrackColor.cgColor
        innerTrackShapeLayer.position = position
        innerTrackShapeLayer.strokeEnd = progress
        innerTrackShapeLayer.lineWidth = lineWidth
        innerTrackShapeLayer.lineCap = CAShapeLayerLineCap.round
        innerTrackShapeLayer.fillColor = UIColor.clear.cgColor
        innerTrackShapeLayer.path = circularPath.cgPath
        innerTrackShapeLayer.transform = rotateTransformation
        addSublayer(innerTrackShapeLayer)
        
        progressLabel = UILabel()
        let size = CGSize(width: radius, height: radius)
        let origin = CGPoint(x: position.x, y: position.y)
        progressLabel.frame = CGRect(origin: origin, size: size)
        progressLabel.center = position
        progressLabel.center.y = position.y
        progressLabel.sizeToFit()
        progressLabel.font = UIFont.init(name: "GE Dinar One", size: 30.0)
        progressLabel.text = "0%"
        progressLabel.textColor = colorLable
        progressLabel.textAlignment = .center
        insertSublayer(progressLabel.layer, at: 0)
        
        //        completedLabel = UILabel()
        //        let completedLabelOrigin = CGPoint(x: position.x , y: position.y)
        //        completedLabel.frame = CGRect(origin: completedLabelOrigin, size: CGSize.init(width: radius, height: radius * 0.6))
        //        completedLabel.font = UIFont.systemFont(ofSize: radius * 0.2, weight: UIFont.Weight.light)
        //        completedLabel.textAlignment = .center
        //        completedLabel.center = position
        //        completedLabel.center.y = position.y + 20
        //        completedLabel.textColor = .white
        //        completedLabel.text = "Completed"
        //        insertSublayer(completedLabel.layer, at: 0)
        //
    }
    public override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



