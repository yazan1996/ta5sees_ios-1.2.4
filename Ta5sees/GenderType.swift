//
//  newRegisterStep2.swift
//  Ta5sees
//
//  Created by Admin on 8/19/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
 
class GenderType: UIViewController,UIViewControllerTransitioningDelegate {
    
    
    @IBOutlet weak var mainStack: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            getIpLocation { (str) in
//                if er == nil {
                    userCountry = str//ns?.object(forKey: "countryCode") as? String ?? ""
//                }
            }
        viewFemaleAdult.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        viewMaleAdult.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
       viewFemaleChild.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
       viewMaleChilde.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
    }
    
    @IBOutlet weak var viewFemaleChild: UIStackView!
    @IBOutlet weak var viewMaleChilde: UIStackView!
    @IBOutlet weak var viewFemaleAdult: UIStackView!
    @IBOutlet weak var viewMaleAdult: UIStackView!
    
    @IBOutlet weak var btnChildfemale: UIImageView!
    @IBOutlet weak var btnChildMeal: UIImageView!
    @IBOutlet weak var btnmale: UIImageView!
    @IBOutlet weak var btnFemale: UIImageView!
    
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    var idGender:Int!
    @IBAction func btnNext(_ sender: Any) {}
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        if getDataFromSheardPreferanceString(key: "loginType") == "1" {
            print("createEvent")
            createEvent(key: "52", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "2" {
            createEvent(key: "36", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "3" {
            createEvent(key: "41", date: getDateTime())
        }else if getDataFromSheardPreferanceString(key: "loginType") == "4" {
            createEvent(key: "46", date: getDateTime())
        }
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 1 {
            setDataInSheardPreferance(value:"1",key:"gender")//male adult
            setDataInSheardPreferance(value:"1",key:"UserAgeID")
        }else  if tappedImageView.tag == 2 {
            setDataInSheardPreferance(value:"2",key:"gender") //female adult
            setDataInSheardPreferance(value:"1",key:"UserAgeID")

        }else  if tappedImageView.tag == 3 {
            setDataInSheardPreferance(value:"1",key:"gender") //male child
            setDataInSheardPreferance(value:"2",key:"UserAgeID")

        }else  if tappedImageView.tag == 4 {
            setDataInSheardPreferance(value:"2",key:"gender") //female child
            setDataInSheardPreferance(value:"2",key:"UserAgeID")
        }

     
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "2" {
            self.performSegue(withIdentifier: "child", sender: nil)

        }else{
        
        let detailView = storyboard!.instantiateViewController(withIdentifier: "NaturalDiet") as! NaturalDiet
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
        
        }
        
    }
    
    
}

extension GenderType:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}
