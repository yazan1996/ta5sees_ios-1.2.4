//
//  AboutController.swift
//  Ta5sees
//
//  Created by Admin on 9/28/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class AboutController: UIViewController {
    
    @IBOutlet weak var txtAbout: UILabel!
    
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    var  listNameRescourses = ["MyPlate | ChooseMyPlate","USDA","What is clean eating? Infographic | American Heart Association","Water in diet: MedlinePlus Medical Encyclopedia","All about the Vegetable Group | ChooseMyPlate","All About the Fruit Group | ChooseMyPlate","Advice about Eating Fish | FDA","Healthy diet (who.int)"]
    
    let listResourses = ["https://www.choosemyplate.gov","https://www.usda.gov","https://www.heart.org/en/healthy-living/healthy-eating/eat-smart/nutrition-basics/what-is-clean-eating","https://medlineplus.gov/ency/article/002471.htm","https://www.choosemyplate.gov/eathealthy/vegetables","https://www.choosemyplate.gov/eathealthy/fruits","https://www.fda.gov/food/consumers/advice-about-eating-fish","https://www.who.int/news-room/fact-sheets/detail/healthy-diet"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITextView.appearance().linkTextAttributes = [ .foregroundColor: UIColor.blue ]
        
        txtAbout.text =
            "تخسيس هو تطبيق من اجل نمط حياة صحي يوفر إرشادات وتوصيات لمساعدتك في رحلتك نحو حياة أكثر صحة.\n\nبينما تخسيس يشجعك على فهم ما يمكن أن يقدمه لك في مجال التغذية وممارسة الرياضة والعادات الصحية ، فإن استخدامه ليس مخصصًا في أي تشخيص أو نصيحة أو وقاية أو علاج لمرض أو حالة طبية .\n\n تخسيس ليس جهازًا أو تطبيقًا طبيًا.استشر دائمًا طبيبًا أو أخصائي رعاية صحية للحصول على المشورة الطبية والعلاجات قبل اتخاذ أي قرارات طبية\n\nالمصادر:\n"
        
        
        
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.fillProportionally
        stackView.alignment = UIStackView.Alignment.fill
        stackView.spacing   = 4
        
        for index in 0..<listResourses.count {
            stackView.addArrangedSubview(createHiperLinkLable(text:listResourses[index],name:listNameRescourses[index]))
        }
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo:  txtAbout.bottomAnchor) .isActive = true
        stackView.bottomAnchor.constraint(equalTo:  subview.bottomAnchor) .isActive = true
        
        
        
        //remoteConfig["aboutUs2"].stringValue
    }
    
    func createHiperLinkLable(text:String,name:String)->UITextView{
        let textLabel = UITextView()
        textLabel.backgroundColor = UIColor.clear
        textLabel.text = name
        
        textLabel.font = UIFont(name: "GE Dinar One", size: 15)
        
        textLabel.dataDetectorTypes = .link
        textLabel.isEditable = false
        textLabel.isSelectable = true
        textLabel.isScrollEnabled = false
        textLabel.translatesAutoresizingMaskIntoConstraints = true
        textLabel.sizeToFit()
        let attrtbued = NSAttributedString.makeHyperlink(for: text, in: textLabel.text, as: name)
        
        textLabel.attributedText = attrtbued
        textLabel.textAlignment = .center
        
        textLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        
        return textLabel
    }
    
    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
}


extension NSAttributedString {
    static func makeHyperlink(for path : String , in string : String ,as substring : String)->NSAttributedString{
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        let nsString = NSString(string: string)
        let subStringRange = nsString.range(of: substring)
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttribute(.link,value : path, range: subStringRange)
        attributedString.addAttributes([ NSAttributedString.Key.paragraphStyle: style ], range: (substring as NSString).range(of: substring))
        attributedString.addAttributes([.underlineStyle: NSUnderlineStyle.single.rawValue], range: (substring as NSString).range(of: substring))
        return attributedString
    }
}
