//
//  UnitMealPlannerCell.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 07/07/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class UnitMealPlannerCell: UICollectionViewCell {
    @IBOutlet var UnitLabel: UILabel!
    @IBOutlet weak var btnGramWrigth: UITextField!
    var delegate:UnitWajbehMEalPlannerCells!
    var plate_ID:Int!
    var valGram:Float!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = self.bounds.height*0.4
        UnitLabel.textColor = UIColor.gray
        UnitLabel.transform = CGAffineTransform(scaleX:-1,y:1)
        btnGramWrigth.transform = CGAffineTransform(scaleX:-1,y:1)
        self.semanticContentAttribute = .forceLeftToRight
        UnitLabel.semanticContentAttribute = .forceLeftToRight
        UnitLabel.font = UIFont(name: "GE Dinar One", size: 15)
        btnGramWrigth.font = UIFont(name: "GE Dinar One", size: 15)
        btnGramWrigth.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        btnGramWrigth.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        btnGramWrigth.addTarget(self, action:#selector(textFieldDidEnd), for: UIControl.Event.editingDidEnd)
        if self.traitCollection.userInterfaceStyle == .dark {
            // User Interface is Dark
            UnitLabel.textColor = .black
            btnGramWrigth.textColor = .black
            UnitLabel.textColor = .black
        }
    }
    
    @IBAction func btn_GramWeigth(_ sender: Any) {
        if btnGramWrigth.text!.isEmpty || btnGramWrigth.text == "" {
            delegate.setGrams(gram: Int("0")!, plate_ID: plate_ID)
            return
        }
        let result =  FormatterTextField(str: btnGramWrigth!).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        delegate.setGrams(gram: Int(result)!, plate_ID: plate_ID)
    }
    @objc func textFieldDidChange(textField:UITextField) {
        
        let text =  FormatterTextField(str:textField).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        if text == "" { return }
        return
    }
    
    @objc func textFieldDidBeign(textField: UITextField) {
        let result =  textField.text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        textField.text = result
    }
    @objc func textFieldDidEnd(textField: UITextField) {
        if  btnGramWrigth.text?.isEmpty == true{
            btnGramWrigth.text = "0" + " غم"
        }else{
            btnGramWrigth.text = btnGramWrigth.text! + " غم"
        }
        self.endEditing(true)
    }
}





