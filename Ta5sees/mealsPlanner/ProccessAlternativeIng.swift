//
//  ProccessAlternativeIng.swift
//  Ta5sees
//
//  Created by Admin on 3/9/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation

func getUserInfo()->tblUserInfo{
    var user:tblUserInfo!
    getInfoUser { (res, error) in
        user = res
    }
    return user
}
func getUserInfoProgressHistory()->tblUserProgressHistory{
    var user:tblUserProgressHistory!
    getInfoHistoryProgressUser { (res, error) in
        user = res
    }
    return user
}
func calculateBasedCalorisAllowdUser(idWajbeh:Int)->Float{
    
    let mealDistrbu:tblMealDistribution =  tblMealDistribution.getItem(id: Int(getUserInfo().mealDistributionId)!)
    print("mealDistrbu",mealDistrbu)
    switch  idWajbeh{
    case 1:
        print("mealDistrbu",(Float(mealDistrbu.BreakfastPercentage)/100)*Float(getUserInfo().totalCalories)!)
        return (Float(mealDistrbu.BreakfastPercentage)/100)*Float(getUserInfo().totalCalories)!
    case 2:
        return (Float(mealDistrbu.LunchPercentage)/100)*Float(getUserInfo().totalCalories)!
    case 3:
        return (Float(mealDistrbu.DinnerPercentage)/100)*Float(getUserInfo().totalCalories)!
    case 4:
        return (Float(mealDistrbu.Snack1Percentage)/100)*Float(getUserInfo().totalCalories)!
    case 5:
        return   (Float(mealDistrbu.Snack2Percentage)/100)*Float(getUserInfo().totalCalories)!
    default:
        return  1.0
    }
    
}
func getNameSenfIng(arr:[tblPackageIngredients],plateId:String)->[String]{
    var arrName = [String]()
    for item in arr {
        let senf:tblSenf = setupRealm().objects(tblSenf.self).filter("id == %@",Int(item.senfId!)!).first!
        arrName.append(senf.trackerName)
    }
    return arrName
}
func getIngPlate(arr:[tblPackageIngredients],id_waj:String,plateId:String,date:String,saveShoppingList:Bool)->[Float]{
    var protein:Float = 0.0
    var fat:Float = 0.0
    var carbohydrate:Float = 0.0
    var calories:Float = 0.0
    var grame:Float = 0.0
    var resulteNutrtion = [Float]()
    
    for item in arr {

        if date == "nil" {
            resulteNutrtion = replacmetIng1(id_waj:id_waj,itemIng:Int(item.senfId!)!, plateId: plateId)
        }else{
            resulteNutrtion = replacmetIng12(id_waj:id_waj,itemIng:Int(item.senfId!)!, plateId: plateId,date:date,saveShoppingList:saveShoppingList)
        }
        protein += resulteNutrtion[3]
        fat += resulteNutrtion[1]
        carbohydrate += resulteNutrtion[2]
        calories += resulteNutrtion[0]
        grame += resulteNutrtion[4]
        
    }
    print("N.T", protein,
          fat ,
          carbohydrate ,
          calories ,
          grame )
    return [abs(calories),abs(fat),abs(carbohydrate),abs(protein),grame]
    
}
func replacmetIng1(id_waj:String,itemIng:Int,plateId:String)->[Float]{
    let result:tblIngredientRules? = tblIngredientRules.getIDsReplacemnt1(id_packge: "\(id_waj)",itemIng:"\(itemIng)")
    //    print("\(id_waj)","\(itemIng)","result \(result)")
    var protein:Float = 0.0
    var fat:Float = 0.0
    var carbohydrate:Float = 0.0
    var calories:Float = 0.0
    var grame:Float = 0.0
    let senf_new = setupRealm().objects(tblSenf.self).filter("id == %@",result!.replaceWithSenfId).first
    let senf_old_Ing = setupRealm().objects(tblPackageIngredients.self).filter("senfId == %@ AND packageId == %@ AND plateId == %@ ","\(itemIng)",id_waj,plateId).first
    //    print("senf_new",senf_new)
    if result?.id != 0 {
        if result!.actionType == 2 {
            print("dele action")
            protein = 0.0
            fat = 0.0
            carbohydrate = 0.0
            calories = 0.0
            grame = 0.0
            id_Ing_rep.append(itemIng)
        }else if result!.actionType == 1 {
            print("replacment action")
            protein = calclatNuitrationFacts1(val1:Float((senf_new?.wajbehProteinTotal!)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            fat = calclatNuitrationFacts1(val1:Float((senf_new?.wajbehFatTotal!)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            carbohydrate = calclatNuitrationFacts1(val1:Float(senf_new!.wajbehCarbTotal!)!,val2:Float(senf_old_Ing!.calories!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            grame = calclatNuitrationFacts1(val1:Float((senf_new?.servingTypeWeight)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            calories = Float(senf_old_Ing!.calories!)!
            
        }
        
    }else{
//        print("no action")
        protein = Float(senf_old_Ing!.protein!)!
        fat = Float(senf_old_Ing!.fat!)!
        carbohydrate = Float(senf_old_Ing!.carbohydrate!)!
        calories = Float(senf_old_Ing!.calories!)!
        grame = Float(senf_old_Ing!.netWeight!)!
        
    }

    
    
    return [abs(calories),abs(fat),abs(carbohydrate),abs(protein),grame]
    
}

func replacmetIng12(id_waj:String,itemIng:Int,plateId:String,date:String,saveShoppingList:Bool)->[Float]{
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"

    let package:tblPackeg? = setupRealm().objects(tblPackeg.self).filter("id == %@",Int(id_waj)!).first
    
    let result:tblIngredientRules? = tblIngredientRules.getIDsReplacemnt1(id_packge: "\(id_waj)",itemIng:"\(itemIng)")
    var protein:Float = 0.0
    var fat:Float = 0.0
    var carbohydrate:Float = 0.0
    var calories:Float = 0.0
    var grame:Float? = 0.0
    let senf_new = setupRealm().objects(tblSenf.self).filter("id == %@",result!.replaceWithSenfId).first
    let senf_old = setupRealm().objects(tblSenf.self).filter("id == %@",itemIng).first
    let senf_old_Ing = setupRealm().objects(tblPackageIngredients.self).filter("senfId == %@ AND packageId == %@ AND plateId == %@ ","\(itemIng)",id_waj,plateId).first
    //    print("senf_new",senf_new)
    if result?.id != 0 {
        if result!.actionType == 2 {
            print("dele action")
            protein = 0.0
            fat = 0.0
            carbohydrate = 0.0
            calories = 0.0
            grame = 0.0
            id_Ing_rep.append(itemIng)
        }else if result!.actionType == 1 {
            print("replacment action")
            protein = calclatNuitrationFacts1(val1:Float((senf_new?.wajbehProteinTotal!)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            fat = calclatNuitrationFacts1(val1:Float((senf_new?.wajbehFatTotal!)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            carbohydrate = calclatNuitrationFacts1(val1:Float(senf_new!.wajbehCarbTotal!)!,val2:Float(senf_old_Ing!.calories!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            grame = calclatNuitrationFacts1(val1:Float((senf_new?.servingTypeWeight)!)!,val2:Float((senf_old_Ing?.calories)!)!,val3:Float(senf_new!.wajbehCaloriesTotal!)!)
            calories = Float(senf_old_Ing!.calories!)!
            }
//            if saveShoppingList {
        print("itemShopiingList.count",itemShopiingList.count+1)

//        itemShopiingList.append(
            _=tblShoppingList(id: itemShopiingList.count+1,date: date, quantity: "\(round(grame ?? 1.0/Float((package?.recipeYield) ?? "1.0")!))", senfID: senf_new!.id, isCheck: 0, nameSenf: senf_new!.ingredientName,packgeID:Int(id_waj)!, dateDouble: dateFormatter.date(from: date)!.timeIntervalSince1970*1000.0.rounded())
//        )
//            }
    }else{
//        print("no action")
        protein = Float(senf_old_Ing!.protein!)!
        fat = Float(senf_old_Ing!.fat!)!
        carbohydrate = Float(senf_old_Ing!.carbohydrate!)!
        calories = Float(senf_old_Ing!.calories!)!
        grame = Float(senf_old_Ing!.netWeight!)!
        
        print("itemShopiingList.count",itemShopiingList.count+1,dateFormatter.date(from: date)!.timeIntervalSince1970*1000.0.rounded())
//        if saveShoppingList {
//        itemShopiingList.append(
        _=tblShoppingList(id: itemShopiingList.count+1, date: date, quantity: "\(round(Double(senf_old_Ing!.netWeight!) ?? 1.0/Double(package?.recipeYield ?? "1.0")!))", senfID: senf_old!.id, isCheck: 0, nameSenf: senf_old!.ingredientName ,packgeID:Int(id_waj)!, dateDouble: dateFormatter.date(from: date)!.timeIntervalSince1970*1000.0.rounded())
//        )

//        }
    }

    
    
    return [abs(calories),abs(fat),abs(carbohydrate),abs(protein),grame!]
    
}



func calclatNuitrationFacts(val1:Float,val2:Float,val3:Float)->Float{
    let valGram:Float = val1 * val2
    return valGram/val3
    
}
func calclatNuitrationFacts1(val1:Float,val2:Float,val3:Float)->Float{
    let valGram = val1 * val2
    return valGram/val3
    
}
func calclatNuitrationFacts2(val1:Float,val2:Float,val3:Float)->Float{
    let valGram:Float = val1 * val2
    return valGram/val3
    
}


