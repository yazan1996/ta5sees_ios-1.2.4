 //
 //  ViewController.swift
 //  GLViewPagerViewControllerForSwift
 //
 //  Created by Yanci on 2017/6/26.
 //  Copyright © 2017年 Yanci. All rights reserved.
 //

 
 import UIKit
 import SVProgressHUD
 import Firebase
 
 class GLPresentViewController: UIViewController,UIViewControllerTransitioningDelegate  {
    
    
    
    var dateLabel: UILabel = UILabel()
    
    internal var _title : NSString = "Page Zero"
    internal var _setupSubViews:Bool = false
    internal var _arrWajbat = [tblWajbatUserMealPlanner]()
    var MainView:ViewControllerMealPlanner!
    //    var id_items = "1"
    var idWajbeh = ""
    var txtDateDay = ""
    var delegateMain:ViewController!
    var pg:protocolWajbehInfoTracker?
    
    init(title : NSString) {
        _title = title
        super.init(nibName: nil, bundle: nil)
    }
    
    init(arr : [tblWajbatUserMealPlanner],MainView:ViewControllerMealPlanner,txtDateDay:String,pg:protocolWajbehInfoTracker,delegateMain:ViewController) {
        //        _arrWajbat = arr
        self.pg = pg
        self.MainView = MainView
        self.txtDateDay = txtDateDay
        self.delegateMain = delegateMain
        
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    var tblView = UITableView(frame: CGRect(x: 0,y: 0,width: 300,height: 300))
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tblView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        dateLabel.text = txtDateDay//getDateOnly()
        tblView.delegate = self
        tblView.dataSource = self
        
        tblView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
        //        if setupRealm().objects(tblPackeg.self).isEmpty {
        //            SVProgressHUD.show(withStatus: "جاري تهيئة وجباتك")
        //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
        //                tblPackeg.GenerateMealPlanner(date:txtDateDay) { [self] (res, err) in
        //                    SVProgressHUD.dismiss {
        //                        _arrWajbat = res
        //                        tblView.reloadData()
        //                    }
        //                }
        //            }
        //        }
        //        else{
        if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() <= Double(getUserInfo().expirDateSubscribtion)! {
            print("txtDateDay \(txtDateDay)")
            tblWajbatUserMealPlanner.getAllData(date: txtDateDay) { [self] (arrWajbatUserMealPlanner) in
                _arrWajbat.append(contentsOf: arrWajbatUserMealPlanner)
                _arrWajbat = _arrWajbat.sorted(by:{ $0.refOrderID < $1.refOrderID })
                tblView.reloadData()
            }
        }else{
            if getUserInfo().subscribeType == 0 {
                showAlert(str: " تم الانتهاء من الفترة التجريبية ،هل تريد الاشتراك الآن ؟", view: self)
            }else if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 2{
                showAlert(str: "انت غير مشترك بالخدمة،هل تريد الاشتراك الآن ؟", view: self)
            }
        }
    }
    
    func getDateOnly()->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currentDate = Date()
        return dateFormatter.string(from: currentDate)
    }
    func setupTableView() {
        self.view.addSubview(tblView)
        tblView.translatesAutoresizingMaskIntoConstraints = false
        tblView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor,constant: 10).isActive = true
        tblView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tblView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tblView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
    }
    func setupLabel() {
        self.view.addSubview(dateLabel)
        dateLabel.textAlignment = .center
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.topAnchor.constraint(equalTo: view.topAnchor,constant: 10).isActive = true
        dateLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        //        dateLabel.bottomAnchor.constraint(equalTo: tblView.topAnchor).isActive = true
        dateLabel.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
    }
    override func viewWillLayoutSubviews() {
        setupLabel()
        setupTableView()
        //self.presentLabel.center = self.view.center
    }
    
 }
 
 
 class ViewControllerMealPlanner: GLViewPagerViewController,GLViewPagerViewControllerDataSource,GLViewPagerViewControllerDelegate {
    
    
    
    // MARK: - cache properties
    var viewControllers = [GLPresentViewController]()
    var tabTitles = [String]()
    var arr = [tblWajbatUserMealPlanner]()
    var txtDateDay = ""
    var lastItemReload = 0
    var firstItemReload = -1
    var counter = -1
    var delegateMain:ViewController!
    var pg:protocolWajbehInfoTracker?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewControllers.isEmpty {
            getFirstRowUser()
        }
    }
    func getDayOfWeek(_ date: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        guard let todayDate = formatter.date(from: date) else { return nil }
        let weekday = Calendar(identifier: .gregorian).component(.weekday, from: todayDate)
        
        switch Calendar.current.weekdaySymbols[weekday-1]  {
        case "Sunday":
            return "الأحد"
        case "Monday":
            return "الإثنين"
        case "Tuesday":
            return "الثلاثاء"
        case "Wednesday":
            return "الأربعاء"
        case "Thursday":
            return "الخميس"
        case "Friday":
            return "الجمعة"
        case "Saturday":
            return "السبت"
        default:
            return ""
        }
    }
    func getFirstRowUser(){
        var indexDate = 0
        tblWajbatUserMealPlanner.getAllDate { [self] (items) in
            if items.isEmpty {
                return
            }
            if let index = items.firstIndex(where: { $0 == txtDateDay }) {
                indexDate = index
                print("Found at \(index)")
            }
            for date in indexDate...items.count-1 {
                //                 if items[date] == txtDateDay{
                //                     self.defaultDisplayPageIndex = date // index
                for i in date-5...date-1 {
                    if i >= 0 {
                        if firstItemReload == -1 {
                            firstItemReload = i
                        }
                        print("txtDateDay1",txtDateDay,items[i])
                        if items[i] == txtDateDay {
                            print("here2",i)
                            self.defaultDisplayPageIndex = viewControllers.count // index
                        }
                        let day = getDayOfWeek(items[i])
                        tabTitles.append(day ?? "")
                        viewControllers.append(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain))
                    }
                }
                
                for i in date...date+5 {
                    if firstItemReload == -1 {
                        firstItemReload = i
                    }
                    if items.count <= i {
                        return
                    }
                    print("txtDateDay",txtDateDay,items[i])
                    if items[i] == txtDateDay {
                        print("here1",i)
                        self.defaultDisplayPageIndex = viewControllers.count // index
                    }
                    let day = getDayOfWeek(items[i])
                    tabTitles.append(day ?? "")
                    viewControllers.append(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain))
                    lastItemReload = i
                }
                return
            }
            
            //             }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setDataSource(newDataSource: self)
        self.setDelegate(newDelegate: self)
        self.padding = 10
        self.leadingPadding = 10
        self.trailingPadding = 10
        if viewControllers.isEmpty {
            self.defaultDisplayPageIndex = 0
        }
        self.tabAnimationType = GLTabAnimationType.GLTabAnimationType_WhileScrolling
        self.indicatorColor = UIColor(red: 156 / 255.0, green: 171 / 255.0, blue: 179/255.0, alpha: 1.0)
        self.supportArabic = false
        self.fixTabWidth = false
        self.fixIndicatorWidth = true
        self.indicatorWidth = 20.0
        
    }
    @objc func clickSubscribeNow(_ sender:UIButton){
        dismiss(animated: true, completion: nil)
        if delegateMain != nil {
            delegateMain.selectItemWithIndex(value: 2)
        }
    }
    
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - GLViewPagerViewControllerDataSource
    func numberOfTabsForViewPager(_ viewPager: GLViewPagerViewController) -> Int {
        if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 2 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
            print("numberOfTabsForViewPager1")
            
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                
                showButtonAlertSubc()
                return 0
            }else{
                if self.viewControllers.isEmpty {
                    showButtonAlertSubc()
                    return 0
                }
                return self.viewControllers.count
                
            }
        }else{
            if self.viewControllers.isEmpty {
                showButtonAlertSubc()
                return 0
            }
            return self.viewControllers.count
            
        }
    }
    
    func showButtonAlertSubc(){
        //        showAlert1()
        let rect = CGRect(origin: CGPoint(x: 0,y :view.frame.height*0.5), size: CGSize(width: view.bounds.size.width, height: view.bounds.size.height))
        let subView = UIView(frame: rect)
        subView.center = view.center
        
        let btnRect = CGRect(origin: CGPoint(x: 0,y : 0), size: CGSize(width: subView.bounds.size.width-32, height: 44))
        
        let btn = UIButton(frame: btnRect)
        btn.setTitle("تفعيل إشتراك النظام الغذائي", for: .normal)
        btn.center = subView.center
        btn.titleLabel?.textColor = .white
        btn.backgroundColor = UIColor(red: 106/255, green: 197/255, blue: 182/255,alpha: 1.0)
        btn.titleLabel?.textAlignment = .center;
        btn.titleLabel?.font = UIFont(name:"GE Dinar One", size: 20)!
        btn.titleLabel?.sizeToFit()
        btn.isHighlighted = true
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.layer.cornerRadius = btn.frame.height*0.5
        subView.addSubview(btn)
        view.addSubview(subView)
        btn.addTarget(self, action: #selector(clickSubscribeNow(_:)), for: .touchUpInside)
    }
    //    func showAlert1(){
    //        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "DialogSubscribeMealPlanner") as! DialogSubscribeMealPlannerViewController
    //        customAlert.providesPresentationContextTransitionStyle = true
    //        customAlert.definesPresentationContext = true
    //        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    //        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    //
    //        self.present(customAlert, animated: true, completion: nil)
    //
    //    }
    func viewForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIView {
        let label:UILabel = UILabel.init()
        label.text = self.tabTitles[index]
        label.textColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1.0)
        label.textAlignment = NSTextAlignment.center
        label.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        label.font = UIFont(name: "GE Dinar One",size: 16)
        return label
    }
    
    func contentViewControllerForTabAtIndex(_ viewPager: GLViewPagerViewController, index: Int) -> UIViewController {
        
        return self.viewControllers[index] as UIViewController
    }
    
    // MARK: - GLViewPagaerViewControllerDelegate
    func didChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int) {
        DispatchQueue.main.async { [self] in
            if viewControllers.isEmpty{
                return
            }
            if abs((fromTabIndex - (viewControllers.count-1))) == 1{
                
                tblWajbatUserMealPlanner.getAllDate { [self] (items) in
                    for date in 0...items.count-1 {
                        print(date,lastItemReload)
                        if date >= lastItemReload+1{
                            for i in date...date+5 {
                                print("###",i,items.count-1)
                                
                                if i > items.count-1 {
                                    
                                    print("index rang out ",items[i-1])
                                    lastItemReload = i-1
                                    self.view.isUserInteractionEnabled = true
                                    break
                                }
                                let day = getDayOfWeek(items[i])
                                tabTitles.append(day ?? "")
                                viewControllers.append(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain   ))
                                lastItemReload = i
                                
                            }
                            self.view.isUserInteractionEnabled = false
                            //                                SVProgressHUD.show(withStatus: "جاري تحميل المزيد من الوجبات")
                            //                                UIView.animate(withDuration: 3) {
                            changeColorLable(viewPager: viewPager,index:index, fromTabIndex: fromTabIndex-1)
                            self.reloadData()
                            self._setNeedsReload()
                            self.defaultDisplayPageIndex = index+1
                            //                                } completion: { (Bool) in
                            self.view.isUserInteractionEnabled = true
                            //                                    SVProgressHUD.dismiss()
                            
                            //                                }
                            break
                        }
                        
                    }
                }
                
            }else if index <= 0 {
                print("-------------------------------+++")
                if fromTabIndex-1 <= viewControllers.startIndex{
                    tblWajbatUserMealPlanner.getAllDate { [self] (items) in
                        
                        for date in 0...items.count {
                            if date < firstItemReload{
                                if counter == 5 {
                                    break
                                }
                                counter+=1
                            }else{
                                counter = 1
                                break
                            }
                        }
                        if firstItemReload < 0 {
                            print("HERE!")
                            return
                        }
                        if counter >= 0 {
                            if firstItemReload == 1 && counter == 1{
                                firstItemReload = 1
                                counter = 0
                            }
                            print("###%%% ",firstItemReload-counter-1...firstItemReload-1)
                            for i in firstItemReload-counter-1...firstItemReload-1 {
                                if i < 0 {
                                    print("index rang out ")
                                    return
                                }
                                let day = getDayOfWeek(items[i])
                                print(counter,i,"day",items[i])
                                //                                    if i <= counter {
                                //                                        tabTitles.insert(day ?? "", at: i)
                                //                                        viewControllers.insert(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain), at: i)
                                //                                        self.defaultDisplayPageIndex = i
                                //                                    }else{
                                tabTitles.insert(day ?? "", at: i-(firstItemReload-counter-1))
                                viewControllers.insert(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain), at: i-(firstItemReload-counter-1))
                                
                                self.defaultDisplayPageIndex = i-(firstItemReload-counter-1)
                                
                                //                                    }
                                
                            }
                            
                            self.view.isUserInteractionEnabled = false
                            //                                SVProgressHUD.show(withStatus: "جاري تحميل المزيد من الوجبات")
                            //                                UIView.animate(withDuration: 3) {
                            firstItemReload = firstItemReload-counter-1
                            counter = -1
                            
                            self.reloadData()
                            self._setNeedsReload()
                            //                                } completion: { (Bool) in
                            self.view.isUserInteractionEnabled = true
                            changeColorLable(viewPager: viewPager,index:index, fromTabIndex: fromTabIndex-1)
                            //                                    SVProgressHUD.dismiss()
                            
                            //                                }
                            //                                return
                        }else{
                            print("HERE@#")
                            return
                        }
                    }
                }
                
            }
            
            changeColorLable(viewPager: viewPager,index:index, fromTabIndex: fromTabIndex)
            
        }
    }
    
    func changeColorLable(viewPager: GLViewPagerViewController,index:Int, fromTabIndex: Int){
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        prevLabel.textColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1.0)//UIColor.init(colorLiteralRed: 0.3, green: 0.3, blue: 0.3, alpha: 1.0)
        currentLabel.textColor = UIColor(red: 127/255, green: 183/255, blue: 162/255, alpha: 1.0)//UIColor.init(colorLiteralRed: 0.5, green: 0.0, blue: 0.5, alpha: 1.0)
    }
    func willChangeTabToIndex(_ viewPager: GLViewPagerViewController, index: Int, fromTabIndex: Int, progress: CGFloat) {
        
        let prevLabel:UILabel = viewPager.tabViewAtIndex(index: fromTabIndex) as! UILabel
        let currentLabel:UILabel = viewPager.tabViewAtIndex(index: index) as! UILabel
        prevLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.0 - (0.1 * progress), y: 1.0 - (0.1 * progress))
        currentLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9 + (0.1 * progress), y: 0.9 + (0.1 * progress))
        currentLabel.textColor =  UIColor.init(_colorLiteralRed: Float(0.3 * 353.4)/255.0, green: Float(0.3 * 656.7)/255.0, blue: Float(0.3 * 606.7)/255.0, alpha: 1.0)
        prevLabel.textColor = UIColor.init(_colorLiteralRed: Float(0.3 * 520)/255.0, green: Float(0.3 * 570)/255.0, blue: Float(0.3 * 596.7)/255.0, alpha: 1.0)
    }
    
    func widthForTabIndex(_ viewPager: GLViewPagerViewController, index: Int) -> CGFloat {
        let prototypeLabel:UILabel = UILabel.init()
        prototypeLabel.text = self.tabTitles[index]
        prototypeLabel.textAlignment = NSTextAlignment.center
        prototypeLabel.font = UIFont(name: "GE Dinar One",size: 17)
        return prototypeLabel.intrinsicContentSize.width
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 }
 
 extension GLPresentViewController:UITableViewDelegate,UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _arrWajbat.isEmpty {
            TableViewHelper.EmptyMessage(message: "لا يوجد لديك نظام غذائي،يرجى الاشتراك الان", viewController: tblView)
            return 0
        }else {
            return _arrWajbat.count
        }
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = _arrWajbat[indexPath.row]
        let cell = Bundle.main.loadNibNamed("CustomCell", owner: self, options: nil)?.first as! CustomCell
        cell.lblName.text = item.packgeName
        cell.lblCaloris.text = " \(item.packagesCalories) سعرة "
        cell.lblGram.text = " \(item.packagesWeights) غم "
        cell.view = self
        cell.item = item
        cell.index = indexPath.row
        if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
            cell.isFavrite = true
            cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
        }else{
            cell.isFavrite = false
            cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
            cell.imgFav.alpha = 0.2
        }
        cell.lbltet.text = setTilePage(id:tblPackeg.getIDCategory(id: Int(item.refPackgeID)!,id_Cate:Int(item.refPackgeCateItme)!))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPathForFirstRow = IndexPath(row: indexPath.row, section: 0)
        let currentCell = tableView.cellForRow(at: indexPathForFirstRow)! as! CustomCell
        UIView.animate(withDuration: 0.1, delay: 0.01, options: .allowAnimatedContent) {
            //
            currentCell.contentView.backgroundColor = .gray.withAlphaComponent(0)
        } completion: {  bool in
            currentCell.contentView.backgroundColor = UIColor(hexString: "#F2F2F7")
        }
        DisplayWajbh(index:indexPath.row)
        
    }
    func DisplayWajbh(index:Int){
        print("DisplayWajbh",index)
        let item = _arrWajbat[index]
        
        if item.refPackgeCateItme == "1" {
            createEvent(key: "61", date: getDateTime())
        }else if item.refPackgeCateItme == "2" {
            createEvent(key: "63", date: getDateTime())
        }else if item.refPackgeCateItme == "3" {
            createEvent(key: "65", date: getDateTime())
        }else if item.refPackgeCateItme == "4" {
            createEvent(key: "67", date: getDateTime())
        }else if item.refPackgeCateItme == "5" {
            createEvent(key: "69", date: getDateTime())
        }
        
        let detailView = MainView.storyboard!.instantiateViewController(withIdentifier: "showWajbehMealPlanner") as! showWajbehMealPlanner
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        detailView.date = item.date
        detailView.asMealPlanner = true
        detailView.idPackge = Int(item.refPackgeID)
        detailView.id_items = item.refPackgeCateItme
        detailView.flg_category_mealPlanner = Int(item.refPackgeCateItme)!
        detailView.pg = pg
        if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
            detailView.isFavrite = true
        }else{
            detailView.isFavrite = false
        }
        detailView.delegateMain = self.delegateMain
        detailView.itemMealPlanner = item
        detailView.flagFromMealPlanner = true
        
        
        tblUserWajbehEaten.checkFoundItemIsNotProposal(id:Int(item.refPackgeID)!, wajbehInfo:item.refPackgeCateItme, date: item.date, isProposal: "1", completion: { (id,bool) in
            print("bool \(bool)")
            detailView.idItemsEaten = id
            if bool == true {
                detailView.isEaten = true
            }
            else{
                detailView.isEaten = false
            }
        })
        present(detailView, animated: true, completion: nil)
    }
    
    
    func setTilePage(id:Int)->String{
        
        switch id {
        case 1:
            return "فطور"
        case 2:
            return "غداء"
        case 3:
            return "عشاء"
        case 4:
            return "تصبيرة صباحي"
        case 5:
            return "تصبيرة مسائي"
        default:
            print("not found !!")
        }
        return ""
    }
    
    
 }
 extension Formatter {
    static let weekdayName: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "cccc"
        return formatter
    }()
    static let customDate: DateFormatter = {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
 }
 extension Date {
    var weekdayName: String { Formatter.weekdayName.string(from: self) }
 }
 
 extension GLPresentViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
 }
