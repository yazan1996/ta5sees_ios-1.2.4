//   CustomCell.swift
//   GLViewPagerViewControllerForSwift
//
//   Created by Admin on 3/3/21.
//   Copyright © 2021 Yanci. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
class CustomCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnsh.layer.cornerRadius = btnsh.bounds.height*0.5
        btnre.layer.cornerRadius = btnre.bounds.height*0.5
        imgFav.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(btnFavurite(_:))))
        imgFav.tintColor = .red
        //          Initialization code
    }
    var view:GLPresentViewController!
//    var view:ItemViewController!
    var index:Int!
    var isFavrite = false
    var item:tblWajbatUserMealPlanner!
    @IBOutlet weak var lblCaloris: UILabel!
    
    @IBOutlet weak var lblGram: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnsh: UIButton!
    @IBOutlet weak var btnre: UIButton!
    @IBOutlet weak var lbltet: UILabel!
    @IBOutlet weak var imgFav: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //          Configure the view for the selected state
    }
   
    @IBAction func btnReplace(_ sender: Any) {
        tblPackeg.replaceWajbeh(cateInfo:Int(item.refPackgeCateItme)!, old_item_id:item.id) { [self] (item) in
            let indexPath = IndexPath(row: index, section: 0)

            view.tblView.reloadRows(at:[indexPath], with: UITableView.RowAnimation.top)
        }
    }
    @IBAction func btnShow(_ sender: Any) {
        view.DisplayWajbh(index : index)
    }
    @objc func btnFavurite(_ gestureRecognizer: UITapGestureRecognizer) {
        if isFavrite == false {
            let obFavurite = tblItemsFavurite()
            obFavurite.id = obFavurite.IncrementaID()
            obFavurite.refUserId = getDataFromSheardPreferanceString(key: "userID")
            obFavurite.flag = "0"
            obFavurite.refItem = String(item.refPackgeID)
            
            try! setupRealm().write {
                setupRealm().add(obFavurite, update: Realm.UpdatePolicy.modified)
                imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
                imgFav.alpha = 1
                showToast(message: "تم إضافة الوجبة الى المفضلة", view: view.view,place:0)
            }
            isFavrite = true
        }else{
            tblItemsFavurite.deleteItem(id: String(item.refPackgeID),flag:"0")
            imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
            imgFav.alpha = 0.1
            showToast(message: "تم حذف الوجبة المفضلة", view: view.view,place:0)
            isFavrite = false
        }
    }
    public func getIngrPlate(idPackge:String,id_Cate:Int){
        var protein:Float = 0.0
        var fat:Float = 0.0
        var carbohydrate:Float = 0.0
        var caloris:Float = 0.0
        var grams:Float = 0.0
        var target:Float = 0.0
        var plateID:[String]!
        var total_caloris = 0.0
        var total_caloris1 = 0.0
        
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            plateID = arrPlateID.removeDuplicates()
        }
        
     
        target = calculateBasedCalorisAllowdUser(idWajbeh:id_Cate)
        for id in plateID {
            let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
            caloris = target
                let percentge = Float(caloris) * nutrationPlate!.Percentage

            tblPackageIngredients.getIngsPlat(packgID: idPackge, platID: id) { (arrIngredients) in
                let resulteNutrtion = getIngPlate(arr:arrIngredients, id_waj:idPackge,plateId: id, date: "nil", saveShoppingList: false)
                total_caloris1 += Double(percentge)
                total_caloris += Double(resulteNutrtion[0])
                let valKCal:Float = percentge * resulteNutrtion[4]

                grams += valKCal/Float(resulteNutrtion[0])
          
                
                
                protein += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[3]),val3:Float(resulteNutrtion[0]))
                
                fat += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[1]),val3:Float(resulteNutrtion[0]))
                
                carbohydrate += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[2]),val3:Float(resulteNutrtion[0]))
                print("********* \(total_caloris1) - protien \(protein) - cho\(carbohydrate) - fat\(fat) rm \(grams)")

            }
            
          
        }
        
        print("total_caloris \(total_caloris1) - protien \(protein) - cho\(carbohydrate) - fat\(fat) rm \(grams)")

        
        lblCaloris.text = " \(Int(round(total_caloris1))) سعرة "
        lblGram.text = " \(String(Int(round(grams)))) غم "
        
    }
    
    
}







struct UnitValue {
    var baseUnit = ""
    var targetUnit = ""
    var value:Float  = 0.0
    init(baseUnit:String, targetUnit:String, value:Float) {
        self.baseUnit = baseUnit
        self.targetUnit = targetUnit
        self.value = value
    }
}


