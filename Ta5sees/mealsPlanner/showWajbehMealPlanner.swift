//
//  showWajbehMealPlanner.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 07/07/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import Charts
import ReadMoreTextView
//import GTProgressBar
import RealmSwift
import SVProgressHUD
import Firebase
import FirebaseAnalytics

class showWajbehMealPlanner: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate, CustomAlertViewDelegate {
    func okButtonTapped(selectedOption: String, textFieldValue: String) {
        
    }
    
    func cancelButtonTapped() {
        
    }
    
    var chartView: PieChartView!
    
    var arrayPlates=[PlateArray]()
    //    var arrOrigin=[PlateArray]()
    var defualtWeight = "1"
    var button:ButtonColorRaduis?
    var asMealPlanner = false
    var movefromHome = false
    var flg_category_mealPlanner = -1
    var flagFromMealPlanner = false
    var isCaloresEmpty:Int!
    var textRecipe = ""
    
    @IBOutlet weak var fat: UIImageView!
    @IBOutlet weak var protein: UIImageView!
    @IBOutlet weak var carbo: UIImageView!
    @IBOutlet weak var gramEatenAllowed: UIButton!
    @IBOutlet weak var lblcalorisEatenAllowed: UIButton!
    @IBOutlet weak var gramEaten: UIButton!
    @IBOutlet weak var lblcalorisEaten: UIButton!
    @IBOutlet weak var txtDescriptionAllowd: UILabel!
    
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var subView: UIView!
    //    @IBOutlet weak var stack1: UIStackView!
    
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var btnAddWajbrhOutlet: ButtonColorRaduis!
    lazy var obPresenter = Presenter(with : self)
    var idItemsEaten:Int!
    let alert = UIAlertController(title: "حدد الكمية", message: nil , preferredStyle: .alert)
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
        //        textField.clearsOnBeginEditing
    }
    
    
    
    @IBOutlet weak var tblUnits: SelfSizedTableView!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var tblItemsIngr: SelfSizedTableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblUnits{
            return 1
        }else{
            return arrayPlates[section].Ingr.count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblUnits{
            return arrUnit.count
        }else{
            return arrayPlates.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblUnits{
            
            let cell:UnitWajbehMEalPlannerCells = tblUnits.dequeueReusableCell(withIdentifier: "unitTableViewCells", for: indexPath) as! UnitWajbehMEalPlannerCells
            cell.arrUnit = arrUnit[indexPath.section].units
            print("valGrams",arrUnit[indexPath.section].valGrams)
            cell.valGram = arrUnit[indexPath.section].valGrams
            cell.view = self 
            cell.package_id = arrUnit[indexPath.section].package_id
            cell.plate_id = arrUnit[indexPath.section].plate_id
            if indexPath.section == 0{
                cell.section = 1
            }
            
            return cell
        }else{
            let cell:DisplayItemsViewCell = tblItemsIngr.dequeueReusableCell(withIdentifier: "cellItems", for: indexPath) as! DisplayItemsViewCell
            
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .right
            
            var txt = arrayPlates[indexPath.section].Ingr[indexPath.row].replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
            txt = txt.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
            let txt_array = arrayPlates[indexPath.section].Ingr[indexPath.row].split(separator: "-")
            let attributedString1 = NSMutableAttributedString.init(string: txt)
            
            attributedString1.addAttribute(NSAttributedString.Key.paragraphStyle, value:  paragraph, range: (txt as NSString).range(of: txt))
            
            attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:15)!, range: (txt as NSString).range(of: txt))
            
            print("txt_array",txt_array.count)
            if txt_array.count <= 2 {
                cell.textName.attributedText = attributedString1
            }else{
                
                attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:15)!, range: (txt as NSString).range(of: String(txt_array[0]).replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)))
                attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range : (txt as NSString).range(of: String(txt_array[0]).replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)))
                
                attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:15)!, range: (txt as NSString).range(of: String(txt_array[2]).replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)))
                attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range : (txt as NSString).range(of: String(txt_array[2]).replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)))
                
                attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:15)!, range: (txt as NSString).range(of: txt))
                
                cell.textName.textAlignment = .right
                cell.textName.attributedText = attributedString1
            }
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return  "مكونات طبق \(arrayPlates[section].plateMain)"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textAlignment = .right
            
            if tableView == tblUnits {
                let str:String = "\(arrUnit[section].plate_name) (إختر الكمية المستهلكة)"
                let attributedString1 = NSMutableAttributedString.init(string: str)
                let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = NSTextAlignment.right
                attributedString1.addAttribute(NSAttributedString.Key.paragraphStyle, value:  paragraphStyle, range: (str as NSString).range(of: str))
                attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:22)!, range: (str as NSString).range(of: arrUnit[section].plate_name))
                attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:15)!, range: (str as NSString).range(of: "(إختر الكمية المستهلكة)"))
                attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 106/255, green: 197/255, blue: 182/255, alpha: 1), range: (str as NSString).range(of: "(إختر الكمية المستهلكة)"))
                
                headerView.textLabel?.attributedText = attributedString1
            }else{
                headerView.textLabel?.font = UIFont(name: "GE Dinar One", size: 24)
            }
            
        }
        
    }
    func selectPrgramticlly(index:Int){
        let indexPathForFirstRow = IndexPath(row: index, section: 0)
        tblUnits.reloadRows(at: [indexPathForFirstRow], with: .automatic)
        flagTut = 0
    }
    
    @IBOutlet weak var txtRecipe: ReadMoreTextView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblWieghtItem: EdgeInsetLabel!
    @IBOutlet weak var lblPersoneNumber: EdgeInsetLabel!
    @IBOutlet weak var lblTime: EdgeInsetLabel!
    //@IBOutlet weak var btnUnitOutlet: UIButton!
    
    @IBOutlet weak var name_wajbeh: UILabel!
    //@IBOutlet weak var valueWeightOutlet: UITextField!
    
    @IBOutlet weak var viewsCharts: UIView!
    //@IBOutlet weak var lblcaloris:UITextView!
    var plateID:[String]!
    var unit:Int!
    var idPackge:Int!
    var pg:protocolWajbehInfoTracker?
    
    lazy var obADDManualPackege = ADDManualPackege(with: self)
    lazy var obAddEatenItem = AddEatenItem(with: self,pd: pg!)
    var wajbeh_item:tblPackeg!
    var id_items:String!
    var date:String!
    var isFavrite:Bool!
    var isEaten:Bool!
    var itemMealPlanner:tblWajbatUserMealPlanner!
    var arrListRestrctedAllergy = [String]()
    @IBAction func btnRestrectedAllergy(_ sender: Any) {
        if !removeLableWindow() {
            showToastNutritional(message : textRecipe,view:view,place:1,frame:view.frame,item:stack2)
        }
    }
    
    @IBAction func replayTutorial(_ sender: Any) {
        replayTutorial()
    }
    func removeLableWindow()->Bool{
        for subview in view.subviews {
            if subview.tag == 899 {
                subview.removeFromSuperview()
                return true
            }
        }
        return false
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        _=removeLableWindow()
        
    }
    func showToastNutritional(message : String,view:UIView,place:Int,frame:CGRect,item:UIStackView) {
        
        let toastLabel = UILabel()
        toastLabel.backgroundColor = UIColor.gray.withAlphaComponent(0.9)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont(name: "GE Dinar One", size: 14)!
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.tag = 899
        toastLabel.numberOfLines = 0
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastLabel.lineBreakMode = .byWordWrapping
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.center = view.center
        
        view.addSubview(toastLabel)
        
        toastLabel.center.y = name_wajbeh.center.y
        toastLabel.center.y -= name_wajbeh.bounds.height
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveLinear], animations: { [self] in
            toastLabel.center.y += name_wajbeh.bounds.height
            name_wajbeh.layoutIfNeeded()
        }, completion: nil)
        NSLayoutConstraint.activate([
            toastLabel.widthAnchor.constraint(equalToConstant: 350),
            toastLabel.centerXAnchor.constraint(equalTo: name_wajbeh.centerXAnchor),
            toastLabel.centerYAnchor.constraint(equalTo: name_wajbeh.centerYAnchor)
        ])
        
    }
    
    func convertLocationItems(location:CGRect,stack:UIStackView)->CGRect{
        let l1 = stack.convert(location, to: subView)
        let l2 = subView.convert(l1, to: self.scroll)
        return scroll.convert(l2, to: self.view)
    }
    func convertLocationItems(location:CGRect)->CGRect{
        let l = CollectionUnitTut.convert(location, to: tblUnits)
        let l1 = tblUnits.convert(l, to: self.viewUnit)
        let l2 = viewUnit.convert(l1, to: self.subView)
        let l3 = subView.convert(l2, to: self.scroll)
        return scroll.convert(l3, to: self.view.superview)
    }
    
    func showTutorialAddWajbeh() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let focusRect1 = convertLocationItems(location:locationUnitTut)
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        let icon3Frame = CGRect(x: convertLocationItems(location:locationUnitTut).origin.x, y: focusRect1.maxY + 30, width: 72, height: 72)
        let message1 =  "اختر الكمية المستهلكة"
        let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
        
        var tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame)
        tut1.isArrowHidden = true
        let focusRect3 = convertLocationItems(location:btnAddWajbrhOutlet.frame, stack: stack2)
        
        let icon4 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        
        let icon3Frame3 = CGRect(x: self.btnAddWajbrhOutlet.bounds.width/2-72/2, y: focusRect3.maxY + 30, width: 72, height: 72)
        
        
        let message3 =  "اضف الوجبة"
        
        let message1Center1 = CGPoint(x: view.bounds.width/2, y: focusRect3.maxY + 24)
        
        
        var tut3 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect3, text: message3, textPosition: message1Center1, icon: icon4, iconFrame: icon3Frame3)
        
        tut3.isArrowHidden = true
        setDataInSheardPreferanceInt(value: getDataFromSheardPreferanceInt(key: "isTutorial-limite-MealPlanner")+1, key: "isTutorial-limite-MealPlanner")
        let tutorials = [tut1,tut3]
        tutorialVC.tutorials = tutorials
        tutorialVC.showWajbehMealPlannerVC  = self
        tutorialVC.showAlertUnitWajbeh = false
        tutorialVC.showInViewController(self)
        
    }
    func loadData() {
        // code to load data from network, and refresh the interface
        chartView.data = nil
        
        arrUnit.removeAll()
        arrayPlates.removeAll()
        tblUnits.reloadData()
        
        tblItemsIngr.reloadData()
    }
    var flagEaten = false
    @IBAction func btnDelete(_ sender: Any) {
        
        if asMealPlanner == true && !flagEaten{ //replace item
            pulse()
            tblPackeg.replaceWajbeh(cateInfo:Int(itemMealPlanner.refPackgeCateItme)!, old_item_id:itemMealPlanner.id) { [self]  (item) in
                idPackge = Int(item.refPackgeID)
                asMealPlanner = true
                flg_category_mealPlanner = Int(item.refPackgeCateItme)!
                id_items =  item.refPackgeCateItme
                itemMealPlanner = item
                loadData()
                
                viewDidLoad()
                viewWillAppear(true)
                //                viewDidAppear(true)
                
            }
        }else{
            obPresenter.getWajbeh(id: idItemsEaten, date: date, caloris: -1, isProposal: ["1"]) { [self] (res, err) in
                try! setupRealm().write{
                    setupRealm().delete((res as! tblUserWajbehEaten?)! )
                    delegateMain.viewWillAppear(true)
                    //                    self.btnDeleteOutlet.isHidden = true
                    btnDeleteOutlet.setImage(UIImage(named : "replace"), for: .normal)
                    btnDeleteOutlet.tintColor = .white
                    showToast(message:"تم حذف الوجبة", view: self.view,place:0)
                    self.dismiss(animated: true, completion: nil)
                    flagEaten = false
                }
            }
        }
    }
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBOutlet weak var btnRestrectedAllergyOutlet: UIButton!
    let attributedString = NSMutableAttributedString()
    let style = NSMutableParagraphStyle()
    var delegateMain:ViewController!
    @IBOutlet weak var btnFavuriteOutlet: UIButton!
    @IBAction func btnFavurite(_ sender: Any) {
        if isFavrite == false {
            let obFavurite = tblItemsFavurite()
            obFavurite.id = obFavurite.IncrementaID()
            obFavurite.refUserId = getDataFromSheardPreferanceString(key: "userID")
            obFavurite.flag = "0"
            obFavurite.refItem = String(idPackge)
            
            try! setupRealm().write {
                setupRealm().add(obFavurite, update: Realm.UpdatePolicy.modified)
                btnFavuriteOutlet.setImage(UIImage(named : "heartred"), for: .normal)
                showToast(message: "تم إضافة الوجبة الى المفضلة", view: self.view,place:0)
            }
            isFavrite = true
        }else{
            tblItemsFavurite.deleteItem(id: String(idPackge),flag:"0")
            btnFavuriteOutlet.setImage(UIImage(named : "heart"), for: .normal)
            showToast(message: "تم حذف الوجبة المفضلة", view: self.view,place:0)
            isFavrite = false
        }
    }
    @IBAction func buDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        listUnitEdite.removeAll()
    }
    var arrUnit = [PlateArrayUnit]()
    
    
    func disableController(){
        view.isUserInteractionEnabled = false
    }
    func enableController(){
        view.isUserInteractionEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") == "1" {
            flagTut = 0
        }
        
        tblItemsIngr.delegate = self
        tblItemsIngr.dataSource = self
        tblItemsIngr.maxHeight = 7000
        
        tblUnits.delegate = self
        tblUnits.dataSource = self
        tblUnits.maxHeight = 7000
        
        scroll.delegate = self
        
        locationUnitTut = nil
        hideKeyboardWhenTappedAround()
        print("id \(String(idPackge))")
        package_id_Tut = idPackge
        style.alignment = NSTextAlignment.right
        
        gramEaten.titleLabel?.font = UIFont(name: "GE Dinar One", size: 18.0)!
        lblcalorisEaten.titleLabel?.font = UIFont(name: "GE Dinar One", size: 18.0)!
        
        //        gramEaten.addBorder(side: .Top, color: colorGray, width: 1)
        gramEaten.addBorder(side: .Left, color: colorGray, width: 1, widithBtn: 0)
        //        lblcalorisEaten.addBorder(side: .Top, color: colorGray, width: 1)
        /*if let image = createFinalImageText(str:itemMealPlanner.packagesCalories, view: view, flgBackground: 2) {
         self.lblcalorisEatenAllowed.image = image
         }
         
         if let image = createFinalImageText(str: itemMealPlanner.packagesWeights, view: view, flgBackground: 2) {
         self.gramEatenAllowed.image = image
         }*/
        
        gramEatenAllowed.setTitle("\(itemMealPlanner.packagesWeights)", for: .normal)
        lblcalorisEatenAllowed.setTitle("\(itemMealPlanner.packagesCalories)", for: .normal)
        
        txtDescriptionAllowd.text = "وزن الكمية المسموح استهلاكها \(setTilePage(id:Int(id_items)!))"
        proccessSearchLimit(id_items: id_items)
        
        chartView = PieChartView(frame: viewsCharts.frame)
        
        
        DispatchQueue.main.async { [self] in
            tblRecentUsed.processInsert(id:String(idPackge),flag:"0")
            obADDManualPackege.getNewWjbeh(idPackge: idPackge,asMealPlanner:asMealPlanner, flg_Cate: flg_category_mealPlanner)
        }
        button = ButtonColorRaduis(frame: CGRect(x: 16, y: view.frame.size.height-60, width: 130, height: 40)) //UIScreen.main.bounds.width
        //        valueWeightOutlet.clearsOnBeginEditing = true
        
        
        
        self.viewContent.layoutIfNeeded()
        viewContent.sizeToFit()
        setMerginLabel(lbl:lblTime)
        setMerginLabel(lbl:lblWieghtItem)
        setMerginLabel(lbl:lblPersoneNumber)
        name_wajbeh.font = UIFont(name: "GE Dinar One",size:25)
        txtDescriptionAllowd.font = UIFont(name: "GE Dinar One",size:15)
        
        let centerText = NSMutableAttributedString()
        let descriptionText = NSMutableAttributedString(string: "الاطباق", attributes: [NSAttributedString.Key.font: UIFont(name: "GE Dinar One",size:20)!])
        centerText.append(descriptionText)
        chartView.entryLabelFont = UIFont(name: "GE Dinar One",size:10)!
        chartView.centerAttributedText = centerText
        chartView.center =  CGPoint(x: view.frame.width / 2, y: viewsCharts.frame.height / 2)
        viewsCharts.addSubview(chartView)
        
        chartView.animate(yAxisDuration: 1, easingOption: ChartEasingOption.easeOutSine)
        
        
        
        //lblcaloris.isUserInteractionEnabled = true
        //lblcaloris.isEditable = false
        
        txtRecipe.clipsToBounds = false
        txtRecipe.layer.shadowOpacity=0.4
        txtRecipe.layer.shadowOffset = CGSize(width: 3, height: 3)
        txtRecipe.layer.shadowColor = colorGray.cgColor
        txtRecipe.textContainer.lineBreakMode = .byWordWrapping
        txtRecipe.sizeToFit()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            tblUnits.reloadData()
        }
        
    }
    
    func setMerginLabel(lbl:EdgeInsetLabel){
        lbl.textInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    }
    func pulse() {
        UIView.animate(withDuration: 0.3, animations:{
            self.btnDeleteOutlet.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                self.btnDeleteOutlet.transform = .identity
            })
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        if isFavrite == true {
            btnFavuriteOutlet.setImage(UIImage(named : "heartred"), for: .normal)
        }
        
        //        let ob = setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND refItemID == %@ AND isProposal IN %@",getDataFromSheardPreferanceString(key: "userID"), "\(idPackge!)",["1"])
        
        if isEaten {
            flagEaten = true
            //            btnDeleteOutlet.isHidden = true
            btnDeleteOutlet.setImage(UIImage(named : "trash"), for: .normal)
        }else if asMealPlanner == true {
            btnDeleteOutlet.tintColor = .white
            btnDeleteOutlet.setImage(UIImage(named : "replace"), for: .normal)
        }
        
        
        
    }
    
    
    
    
    func addItems(){
        if isCaloresEmpty == 0 || isCaloresEmpty == nil {
            Ta5sees.alert(mes: "اختر طبق واحد على الاقل", selfUI: self)
            return
        }
        let ob = tblUserWajbehEaten()
        ob.id = ob.IncrementaID()
        ob.userID = getDataFromSheardPreferanceString(key: "userID")
        ob.refItemID = String(wajbeh_item.id)
        ob.isWajbeh = "0"
        ob.wajbehInfiItem = id_items
        ob.date = date
        ob.isProposal = "1"
        ob.kCal =  "\(isCaloresEmpty!)"
        ob.ratio = "0"
        obAddEatenItem.addItems(ob: ob, date: date, calorisBurned: Int(ob.kCal.westernArabicNumeralsOnly)!) { [self] (error) in
            if movefromHome {
                if id_items == "1" {
                    createEvent(key: "34", date: getDateTime())
                }else if id_items == "2" {
                    createEvent(key: "72", date: getDateTime())
                }else if id_items == "3" {
                    createEvent(key: "74", date: getDateTime())
                }else if id_items == "4" {
                    createEvent(key: "76", date: getDateTime())
                }else if id_items == "5" {
                    createEvent(key: "78", date: getDateTime())
                }
            }else{
                if id_items == "1" {
                    createEvent(key: "62", date: getDateTime())
                }else if id_items == "2" {
                    createEvent(key: "64", date: getDateTime())
                }else if id_items == "3" {
                    createEvent(key: "66", date: getDateTime())
                }else if id_items == "4" {
                    createEvent(key: "68", date: getDateTime())
                }else if id_items == "5" {
                    createEvent(key: "70", date: getDateTime())
                }
            }
            showToast(message : "تم إضافة الوجبة ",view:self.view,place:0)
        }
    }
    /*
     
     /Users/admin/Desktop/ta5sees_ios-swift_new/Pods/FirebaseCrashlytics/upload-symbols -gsp  /Users/admin/Desktop/ta5sees_ios-swift_new/Ta5sees/GoogleService-Info.plist -p ios /Users/admin/Downloads/appDsyms-6
     
     
     
     /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Pods/FirebaseCrashlytics/upload-symbols -gsp  /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Ta5sees/GoogleService-Info.plist -p ios /Users/telecomenterprise/Library/Developer/Xcode/Archives/2021-06-01/Ta5sees\ 01-06-2021\,\ 6.43\ AM.xcarchive/dSYM
     */
    @IBAction func btnAdd(_ sender: Any) {
        if id_items != "-1" {
            addItems()
        }else{
            showDialogCateInfo()
        }
    }
    
    func createButton(flag:Int){
        if flag == 1 {
            button!.firstColor = UIColor(hexString: "#7BDEB3")
            button!.secondColor = UIColor(hexString: "#8DF8BA")
            button?.isHorizontal = true
            button!.updateView()
            button?.titleLabel?.font = UIFont(name: "GE Dinar One",size: 17)
            button!.setTitle("إضافة الوجبة", for: .normal)
            button!.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button!.setImage(UIImage(named: "check"), for: .normal)
            button!.imageEdgeInsets.left = -10
            self.view.addSubview(button!)
            
            
        }else{
            UIView.animate(withDuration: 0.6,
                           animations: { [self] in
                            button?.backgroundColor = UIColor.clear
                           },
                           completion: { _ in
                            UIView.animate(withDuration: 0.6) { [self] in
                                button!.removeFromSuperview()
                            }
                           })
        }
    }
    @objc func buttonAction(sender: UIButton!) {
        if id_items != "-1" {
            addItems()
        }else{
            showDialogCateInfo()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let viewFrame = scrollView.convert(btnAddWajbrhOutlet.bounds, from: btnAddWajbrhOutlet)
        if viewFrame.intersects(scrollView.bounds) {
            // targetView is visible
            createButton(flag:2)
        }else {
            // targetView is not visible
            createButton(flag:1)
            
        }
    }
    
    
    func replayTutorial(){
        if getDataFromSheardPreferanceInt(key: "isTutorial-limite-MealPlanner") <= 10 {
            setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
            if !locationUnitTut.isEmpty {
                var bottomOffset1:CGPoint!
                bottomOffset1 = CGPoint(x: 0, y:locationUnitTut.origin.y+120)
                scroll.setContentOffset(bottomOffset1, animated: true)
                
                if bottomOffset1 != nil {
                    scroll.setContentOffset(bottomOffset1, animated: true)
                }
                view.superview?.isUserInteractionEnabled = false
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) { [self] in
                    showTutorialAddWajbeh()
                }
            }else{
                dismiss(animated: true,completion: nil)
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tblUnits.reloadData()
        replayTutorial()
        
    }
    
    func moveToTopPage(){
        setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
        var bottomOffset1:CGPoint!
        bottomOffset1 = CGPoint(x: 0, y:0)
        scroll.setContentOffset(bottomOffset1, animated: true)
        if bottomOffset1 != nil {
            scroll.setContentOffset(bottomOffset1, animated: true)
        }
        view.superview?.isUserInteractionEnabled = true
    }
    func setTilePage(id:Int)->String{
        
        switch id {
        case 1:
            return "للفطور اليومي"
        case 2:
            return "للغداء اليومي"
        case 3:
            return "للعشاء اليومي"
        case 4:
            return "للتصبيرة الصباحية اليومية"
        case 5:
            return "للتصبيرة المسائية اليومية"
        default:
            print("not found !!")
        }
        return ""
    }
    
}
extension showWajbehMealPlanner:viewAddEatenItem{
    func finshPage() {
        if flagFromMealPlanner {
            dismiss(animated: true,completion: nil)
        }else{
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
}


extension showWajbehMealPlanner:viewSetCateInfo{
    func setIDCateInfo(title: String) {
        id_items=title
        addItems()
    }
    
    
    
    
}

extension showWajbehMealPlanner:setPackageDailyView{
    
    func setDefualtWeight(weight: String) {
        defualtWeight = weight
    }
    
    func setOtherData(val1: String, val2: String, val3: String) {
        self.lblTime.text = "\(val1) دقيقة"
        self.lblPersoneNumber.text = "\(val2) اشخاص"
        self.lblWieghtItem.text = "\(val3) غم"
    }
    
    func setRecipe(recipe:String,strRestrc:String){
        txtRecipe.font = UIFont(name: "GE Dinar One", size: 17.0)!
        
        if !strRestrc.isEmpty {
            btnRestrectedAllergyOutlet.isHidden = false
        }
        textRecipe = strRestrc
        let attributedStringTextAttachment = NSTextAttachment()
        if !strRestrc.isEmpty {
            if #available(iOS 13.0, *) {
                attributedStringTextAttachment.image = UIImage(named: "warning.png")?.withTintColor(.red)
            } else {
                // Fallback on earlier versions
            }//UIColor(hexString: "#C53230")
            attributedString.append(NSAttributedString(attachment: attributedStringTextAttachment))
        }
        
        print("recipe",recipe,"&&&&")
        let string = "طريقة التحضير \n \(recipe) \n"
        
        attributedString.append( NSMutableAttributedString.init(string: string))
        
        let range = (string as NSString).range(of: "طريقة التحضير ")
        let range1 = (string as NSString).range(of: strRestrc)
        
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:17)!, range: (string as NSString).range(of: string))
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:24)!, range: range)
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:style,range: (string as NSString).range(of: string))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value:colorGray,range: (string as NSString).range(of: string))
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:style,range: range)
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:style,range: range1)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red,range: range1)
        
        txtRecipe.attributedText = attributedString
        txtRecipe.isUserInteractionEnabled = true
        txtRecipe.textAlignment = .right
        txtRecipe.translatesAutoresizingMaskIntoConstraints = false
        txtRecipe.attributedReadMoreText = NSMutableAttributedString(string: "..قراءة المزيد", attributes: [NSAttributedString.Key.paragraphStyle: style,NSAttributedString.Key.foregroundColor:colorGreen,NSAttributedString.Key.font: UIFont(name: "GE Dinar One",size:16)!])
        
        txtRecipe.attributedReadLessText =  NSMutableAttributedString(string: "أقرأ أقل ", attributes: [NSAttributedString.Key.paragraphStyle: style,NSAttributedString.Key.foregroundColor:colorGreen,NSAttributedString.Key.font: UIFont(name: "GE Dinar One",size:16)!])
        txtRecipe.isEditable = false
        txtRecipe.isSelectable = false
        
    }
    
    func setIngrANDPate(plates:[PlateArray],origin:[PlateArray],weight:String,arrInfo:[Int],quatnity:Int, unit: Int,caloris:Int,ArrayPlateUnit: [PlateArrayUnit]){
        
        self.arrayPlates = plates
        //        btnUnitOutlet.setTitle("\(weight) غم", for: .normal)
        print("ArrayPlateUnit",arrayPlates)
        
        let yValues = arrayPlates.enumerated().map { (arg) -> PieChartDataEntry in
            let (index, element) = arg
            return PieChartDataEntry(value: round(Double(element.percentge)), label:  element.plateMain, data: index )
        }
        arrUnit = ArrayPlateUnit
        let dataSet = PieChartDataSet(entries: yValues, label: "غم")
        dataSet.colors = colors // [.black,.blue,.brown]
        let data = PieChartData(dataSet: dataSet)
        let legend = chartView.legend
        legend.font = UIFont(name: "GE Dinar One", size: 12.0)!
        
        dataSet.sliceSpace = 2.0
        
        chartView.data = data
        
        //        arrOrigin = origin
        
        if let image = createFinalImageText(str:String (arrInfo[1]), view: view, flgBackground: 1){
            self.protein.image = image
        }
        
        if let image = createFinalImageText(str:String (arrInfo[0]), view: view, flgBackground: 1) {
            self.fat.image = image
        }
        if let image = createFinalImageText(str:String (arrInfo[2]), view: view, flgBackground: 1) {
            self.carbo.image = image
        }
        lblcalorisEaten.setTitle("\(caloris)", for:  .normal)
        gramEaten.setTitle("\(weight)", for:  .normal)
        //        if let image = createFinalImageText(str:String (caloris), view: view, flgBackground: 0) {
        //            self.lblcalorisEaten.image = image
        isCaloresEmpty = caloris
        //        }
        
        //        if let image = createFinalImageText(str:String (weight), view: view, flgBackground: 0) {
        //            self.gramEaten.image = image
        //        }
        
        //        let textCal =  "سعرات حرارية   \(caloris)"
        //        let attributedString1 = NSMutableAttributedString.init(string: textCal)
        //        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:40)!, range: (textCal as NSString).range(of: "\(caloris)"))
        //        attributedString1.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "GE Dinar One",size:20)!, range: (textCal as NSString).range(of: "سعرات حرارية"))
        //
        //        lblcaloris.attributedText = attributedString1
    }
    
    func setDataWajbeh(wahbeh: tblPackeg,quatnity:Int,unit:Int) {
        self.unit = unit
        wajbeh_item = wahbeh
        name_wajbeh.text = wajbeh_item?.name
        
    }
    
}


//extension showWajbehMealPlanner {
//
//    func showSimpleActionSheet(hesa:Int) {
//
//
//        alert.view.tintColor = colorGreen
//
//        alert.addAction(UIAlertAction(title:"\(convertEngNumToArabicNum(num:String(hesa)))  غم", style: .default, handler: { (_) in
//            self.setUnit(title:hesa)
//        }))
//
//        alert.addAction(UIAlertAction(title: "١٠٠ غم", style: .default, handler: { (_) in
//            self.setUnit(title:100)
//        }))
//
//        alert.addAction(UIAlertAction(title: "١ غم", style: .default, handler: { (_) in
//            self.setUnit(title:1)
//        }))
//
//        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
//        }))
//
//        self.present(alert, animated: true, completion: { [self] in
//            var f = alert.view.frame
//            f.size.height = 50
//            f.origin.y = f.origin.y + 50
//            f.origin.x = f.origin.x + 2
//            f.size.width = f.size.width - 5
//            //            showTutorialAddTamreenShowAlert(location:f)
//        })
//    }
//
//
//}

extension showWajbehMealPlanner {
    func showDialogCateInfo(){
        let alert = UIAlertController(title: "اختر نوع الوجبة اليومية", message: nil , preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title:"الافطار", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "1")
        }))
        
        alert.addAction(UIAlertAction(title: "تصبيرة  صباحي", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "4")
        }))
        
        alert.addAction(UIAlertAction(title: "الغداء", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "2")
        }))
        
        alert.addAction(UIAlertAction(title: "تصبيرة مسائي", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "5")
        }))
        
        alert.addAction(UIAlertAction(title: "العشاء", style: .default, handler: { (_) in
            self.setIDCateInfo(title: "3")
        }))
        alert.addAction(UIAlertAction(title: "إخفاء", style: .cancel, handler: { (_) in
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
}


extension showWajbehMealPlanner : ItemsViewList {
    func setBreakfdast(ListEatenBreakfast: [tblUserWajbehEaten]!) {
        
    }
    
    func setLuansh(ListEatenLaunsh: [tblUserWajbehEaten]!) {
        
    }
    
    func setDinner(ListEatenDinner: [tblUserWajbehEaten]!) {
        
    }
    
    func setTasber1(ListEatenTasbera1: [tblUserWajbehEaten]!) {
        
    }
    
    func setTasber2(ListEatenTasbera2: [tblUserWajbehEaten]!) {
        
    }
    
    func setTamreen(ListTamreen: [tblUserTamreen]!) {
        
    }
    
    
}

extension showWajbehMealPlanner {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension showWajbehMealPlanner:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}
