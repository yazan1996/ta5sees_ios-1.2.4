//
//  UnitWajbehMEalPlannerCells.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 07/07/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation


import UIKit

class UnitWajbehMEalPlannerCells: UITableViewCell,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UICollectionViewDelegate {
    @IBOutlet weak var collectionViewUnit: SelfSizedCollectionView!
    
    var arrUnit = [tblPackagePlateUnits]() {
        didSet{
            DispatchQueue.main.async { [self] in
                collectionViewUnit.reloadData()
            }
        }
    }
    var valGram:Float = 0.0
    var package_id = 0
    var plate_id = 0
    var sumGram:Float = 0.0
    var selectedArr = [Int]()
    var gramIndex = -1
    var section:Int!
    var view:showWajbehMealPlanner!
    override func awakeFromNib() {
        super.awakeFromNib()
        let layout = TagFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 40)
        collectionViewUnit.collectionViewLayout = layout
        collectionViewUnit.delegate = self
        collectionViewUnit.dataSource = self
        collectionViewUnit.maxHeight = 7000
        collectionViewUnit.semanticContentAttribute = .forceLeftToRight
        hideKeyboardWhenTappedAround()
        collectionViewUnit.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  arrUnit.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnitMealPlannerCell",for: indexPath) as? UnitMealPlannerCell else {
            return UnitMealPlannerCell()
        }
//        let totalRows = collectionViewUnit.numberOfItems(inSection: 0)
    
        unSelected(cell:cell)
        cell.plate_ID = plate_id
        cell.UnitLabel.textAlignment = .right
//        cell.UnitLabel.numberOfLines = 1
        cell.UnitLabel.lineBreakMode = .byWordWrapping
        cell.delegate = self
        cell.valGram = valGram
        if indexPath.row == arrUnit.count-2 || arrUnit[indexPath.row].Unit ==  "الوزن" {
            cell.UnitLabel.text = "\(arrUnit[indexPath.row].Unit!)"
            if flagTut == 0{
//                selectedArr.append(indexPath.item)
//                gramIndex = indexPath.item
            }
        }else{
            if indexPath.row == 0 {
                cell.UnitLabel.text = "\(Int(round(valGram))) غم (وزن مقترح)"
//                isSelected(cell: cell)
            }else{
                cell.UnitLabel.text = "\(arrUnit[indexPath.row].Unit!) (\(arrUnit[indexPath.row].Weight) غم)"
            }
        }
        cell.btnGramWrigth.text = "\(0) غم"
        cell.UnitLabel.preferredMaxLayoutWidth = collectionView.frame.width - 32
//        cell.btnGramWrigth.isEnabled = false
        
        if indexPath.row == arrUnit.count-1{
            cell.UnitLabel.isHidden = true
            cell.btnGramWrigth.isHidden = false
            //            cell.layer.cornerRadius = 0
            //            cell.isHidden = true
        }else {
            cell.btnGramWrigth.isHidden = true
            cell.UnitLabel.isHidden = false
        }
        if section == 1 {
            if  indexPath.row == 0 {
                locationUnitTut = cell.frame
                CollectionUnitTut = collectionViewUnit
            }
        }
    
        if flagTut == 1 && indexPath.row == 1{
//            view = delegateDisplayPackge
            print("flagTut flagTut flagTut")
            selectPrgramticlly(index:indexPath.row-1)
        }
        return cell
    }
    
    func setGrams(gram:Int,plate_ID:Int){
        let item = UnitEdite(val_gram: Float(gram),plate_id: String(plate_ID))
        if view.flg_category_mealPlanner != -1 {
            view.obADDManualPackege.NewCalculatePerPlateUnitMealPlanner(idPackge:  String(package_id_Tut), quatnity:  1, listUnitEdite:checkItemEdite(item: item), id_Cate: view.flg_category_mealPlanner)
        }else{
        view.obADDManualPackege.NewCalculatePerPlateUnit(idPackge: String(package_id), quatnity: 1,listUnitEdite: checkItemEdite(item: item))
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        self.endEditing(true)
        let totalRows = collectionViewUnit.numberOfItems(inSection: 0)
        if totalRows < 0 {
            return
        }
        // tutorial
        if flagTut == 1 {
            let item = UnitEdite(val_gram: Float(arrUnit[indexPath.row].Weight),plate_id: String(arrUnit[indexPath.row].PlateID))
            isSelected(cell: cell as! UnitMealPlannerCell)
            print("id = \(package_id) \(listUnitEdite)")
            if package_id_Tut == -1 {
                return
            }
            if view.flg_category_mealPlanner != -1 {
                view.obADDManualPackege.NewCalculatePerPlateUnitMealPlanner(idPackge:  String(package_id_Tut), quatnity:  1, listUnitEdite:checkItemEdite(item: item), id_Cate: view.flg_category_mealPlanner)
            }else{
            view.obADDManualPackege.NewCalculatePerPlateUnit(idPackge: String(package_id_Tut), quatnity: 1,listUnitEdite: checkItemEdite(item: item))
            }
            return
        }
        //
        
        
        let lastItem = NSIndexPath(row: totalRows-1, section: 0)
        
        if lastItem.isEqual(nil){
            return
        }
        if indexPath.row + 1 >= collectionViewUnit.numberOfItems(inSection: 0){
            return
        }
        let cell_lastItem  = collectionView.cellForItem(at: lastItem as IndexPath) as?
            UnitMealPlannerCell
       
        cell_lastItem?.btnGramWrigth.isEnabled = false
        if arrUnit[indexPath.row].Unit == "الوزن"{
            unSelectedAll(selectedArr:selectedArr)
            cell_lastItem?.btnGramWrigth.isEnabled = true
            cell_lastItem?.btnGramWrigth.becomeFirstResponder()
            isSelected(cell:cell as! UnitMealPlannerCell)
            gramIndex = indexPath.item
            sumGram = 0//valGram
        }
        else if gramIndex != -1 { // unselected item wieght
            cell_lastItem?.btnGramWrigth.isEnabled = false
            if let index = selectedArr.firstIndex(of:gramIndex) {
                selectedArr.remove(at: index)
                let lastItem = NSIndexPath(row: gramIndex, section: 0)
                let cell = collectionViewUnit.cellForItem(at: lastItem as IndexPath)
                unSelected(cell:cell as! UnitMealPlannerCell)
                gramIndex = -1
                sumGram = 0
            }
        }
        
        if !selectedArr.contains(indexPath.item) {
            isSelected(cell:cell! as! UnitMealPlannerCell)
            selectedArr.append(indexPath.item)
            if arrUnit[indexPath.row].Unit == "defualt" {
                sumGram += valGram
            }else{
                sumGram += Float(arrUnit[indexPath.row].Weight)
            }

        }else {
            unSelected(cell:cell as! UnitMealPlannerCell)
            if let index = selectedArr.firstIndex(of:indexPath.item) {
                selectedArr.remove(at:index)
                if arrUnit[indexPath.row].Unit == "defualt" {
                    sumGram = abs(sumGram - valGram)
                }else{
                    sumGram = abs(sumGram - Float(arrUnit[indexPath.row].Weight))
                }
            }
        }
        let item = UnitEdite(val_gram: Float(sumGram),plate_id: String(arrUnit[indexPath.row].PlateID))
        
        if view.flg_category_mealPlanner != -1 {
            view.obADDManualPackege.NewCalculatePerPlateUnitMealPlanner(idPackge:  String(package_id_Tut), quatnity:  1, listUnitEdite:checkItemEdite(item: item), id_Cate: view.flg_category_mealPlanner)
        }else{
        view.obADDManualPackege.NewCalculatePerPlateUnit(idPackge: String(package_id), quatnity: 1,listUnitEdite: checkItemEdite(item: item))
        }
        selectedArr = selectedArr.removeDuplicates()

    }
    func selectPrgramticlly(index:Int){
        let indexPathForFirstRow = IndexPath(row: index, section: 0)
        self.collectionView(collectionViewUnit, didSelectItemAt: indexPathForFirstRow)
        flagTut = 0
    }
    func checkItemEdite(item:UnitEdite)->[UnitEdite]{
         guard let row = listUnitEdite.firstIndex(where: {$0.plate_id == item.plate_id }) else {
            listUnitEdite.append(item)
            return listUnitEdite
        }
        listUnitEdite[row].val_gram = item.val_gram
        return listUnitEdite
    }


    func isSelected(cell:UnitMealPlannerCell){
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.backgroundColor = colorGreen
    }
    func unSelected(cell:UnitMealPlannerCell){
        cell.backgroundColor = .white
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.borderWidth = 1
    }
    func unSelectedAll(selectedArr:[Int]){
        print("selectedArr \(selectedArr)")
        for x in selectedArr {
            let lastItem = NSIndexPath(row: x, section: 0)
            let cell = collectionViewUnit.cellForItem(at: lastItem as IndexPath)
            cell?.backgroundColor = .white
            cell?.layer.borderColor = UIColor.gray.cgColor
            cell?.layer.borderWidth = 1
         
        }
        self.selectedArr.removeAll()
    }
    func unSelectedAllGram(indexItem:Int){
        let lastItem = NSIndexPath(row: indexItem, section: 0)
        let cell = collectionViewUnit.cellForItem(at: lastItem as IndexPath)
        cell?.backgroundColor = .white
        cell?.layer.borderColor = UIColor.gray.cgColor
        cell?.layer.borderWidth = 1
        let cell_lastItem  = collectionViewUnit.cellForItem(at: lastItem as IndexPath) as! UnitMealPlannerCell
        cell_lastItem.btnGramWrigth.resignFirstResponder()
        if let index = selectedArr.firstIndex(of:indexItem) {
            selectedArr.remove(at:index)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 200, height: 30)
    }
    
    
}




extension UnitWajbehMEalPlannerCells {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
}


