//
//  FruitsVC.swift
//  Ta5sees
//
//  Created by Admin on 3/23/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit

class FruitsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        return items.count
        return items.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multiSelect1",for: indexPath) as? formateSelectedCell else {
            return formateSelectedCell()
        }
        let item = section[indexPath.section].items[indexPath.row]
        if section[indexPath.section].isMultiple {
            if item.name == "لا شيء" {
                index = indexPath.row
            }else{
                
            }
            //For multiple selection
            if item.selected {
                //                cell.accessoryType = .checkmark
                cell.img.setImage(UIImage(named: "checked_checkbox")!.withRenderingMode(.alwaysTemplate))
            }
            else {
                cell.img.setImage(UIImage(named: "unchecked_checkbox")!.withRenderingMode(.alwaysTemplate))
                //                cell.accessoryType = .none
            }
        }
        else {
            //For single selection
            if item.selected {
                //                cell.accessoryType = .checkmark
                cell.img.setImage(UIImage(named: "checked_checkbox")!.withRenderingMode(.alwaysTemplate))
            }
            else {
                cell.img.setImage(UIImage(named: "unchecked_checkbox")!.withRenderingMode(.alwaysTemplate))
                //                cell.accessoryType = .none
            }
        }
        cell.txt.text = section[indexPath.section].items[indexPath.row].name
        cell.txt.textAlignment = .right
        cell.txt.font = UIFont(name: "GE Dinar One",size: 16)
        cell.txt.textColor = colorGray
        return cell
    }
    
    
    
    var items = [Item]()
    var section = [Section]()
    var obselectionDelegate:selectionDelegate!
    var optionsSelected = ""
    var flag = "10" //Fruits
    var index:Int!
    var seletAll = 0
    lazy var obviewMultiSelectPresnter = viewMultiSelectPresnter(with: self)
    @IBOutlet weak var titlePage: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        obviewMultiSelectPresnter.getData(flag:flag)
        setUpCollectionView()
        
        let filtered = items.map{$0.name}.intersection(with:optionsSelected.split(separator: ",").map { String($0) })
        
        // items.filter { filtered.map { String($0) }.contains($0.name)}
        _ = items.filter { (Item) -> Bool in
            Item.selected = filtered.map { String($0) }.contains(Item.name)
            return true
        }
        
      
        
        let secretMessage = boldenParts(string: "إستبعد الفواكه التي لا تفضلها", boldCharactersRanges: [[19,20],[20,21]], regularFont: UIFont(name: "GE Dinar One", size: 18), boldFont: UIFont(name: "GE Dinar One", size: 25))
        titlePage.attributedText = secretMessage
    }
    
 
    
   
    
    func setUpCollectionView() {
        /// 1
        
        
        /// 2
        tblview.delegate = self
        tblview.dataSource = self
        tblview.semanticContentAttribute = .forceRightToLeft

        /// 3
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        /// 4
        layout.minimumLineSpacing = 8
        /// 5
        layout.minimumInteritemSpacing = 4
        
        /// 6
        tblview
            .setCollectionViewLayout(layout, animated: true)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    
    
    
    
    
    func setUNSeletd(falg:Bool){
        for item in items {
            item.selected = falg
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if section[indexPath.section].isMultiple {
            let item = section[indexPath.section].items[indexPath.row]
            if item.name == "لا شيء" && item.selected == false {
                
                index = indexPath.row
                _ = items.map { (item_) -> Void in
                    if items[index].name == item_.name {
                        item_.selected = true
                    }else{
                        item_.selected = false
                    }
                }
                tblview.reloadData()
            } else if item.name != "لا شيء" && item.selected == false {
                if index != nil {
                    items[index].selected = false
                }
                item.selected = true
                tblview.reloadData()
            }else{
                //For multiple selection
                item.selected = !item.selected
                
            }
            section[indexPath.section].items[indexPath.row] = item
            tblview.reloadItems(at: [indexPath])
            //            tblview.reloadRows(at: [indexPath], with: .automatic)
        }
        else {
            //For multiple selection
            let items = section[indexPath.section].items
            
            if let selectedItemIndex = items.indices.first(where: { items[$0].selected }) {
                section[indexPath.section].items[selectedItemIndex].selected = false
                if selectedItemIndex != indexPath.row {
                    section[indexPath.section].items[indexPath.row].selected = true
                }
            }
            else {
                section[indexPath.section].items[indexPath.row].selected = true
            }
            tblview.reloadItems(at: [indexPath])
            //            tblview.reloadSections([indexPath.section], with: .automatic)
        }
        let refItem:[Item] = items.filter { (item) -> Bool in
            if item.selected {
                return true
            }
            return false
        }
        let refIDs:[String] = refItem.map { (item) in
            item.id
        }
        ActivationModel.sharedInstance.refFruit = refIDs
    }
    
    
    
    @IBOutlet weak var tblview: UICollectionView!
}

extension FruitsVC:viewMultiSelectDelegate {
    
    func getDataSection(section: [Section]) {
        self.section = section
    }
    
    func getDataArray(list: [Item]) {
        items = list
        if items.contains(where: {$0.selected == true}) {
            // it exists, do something
            NSLog("here")
            seletAll = 0
        }else {
            seletAll = 1
            NSLog("here2")
            //item could not be found
        }
    }
    
    
}

extension FruitsVC: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        /// 2
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
    
    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// 4
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// 5
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        /// 6
        return CGSize(width: widthPerItem - 8, height: 45)
    }
    
}

