//
//  ContainerVC.swift
//  Ta5sees
//
//  Created by Admin on 3/17/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase
import FirebaseAnalytics

class ContainerVC: UIViewController {
    
    
    @IBOutlet var containerView:UIView!
    @IBOutlet var scroll:UIScrollView!
    
    @IBOutlet var supview:UIView!
    @IBOutlet var titlePage:UILabel!
    
    @IBOutlet var btnNextOut:UIButton!
    lazy var obAPiSubscribeUser = APiSubscribeUser(with: self)
    let ageChild = getDataFromSheardPreferanceFloat(key: "dateInYears")
    var count = 0
    var tee:Float!
    var energyTotal = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.translatesAutoresizingMaskIntoConstraints = false
      
        SVProgressHUD.show()
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
            if ActivationModel.sharedInstance.xtraCusine.contains("9") {
                titlePage.text = "الخضراوات"
                count += 1
                ActivationModel.sharedInstance.refMeat = ["16","15","14"]
                add(asChildViewController: sessionsVegetablesVC)
                SVProgressHUD.dismiss()
            }else{
                count = 0
                titlePage.text = "اللحوم"
                add(asChildViewController: sessionsMeatsVC)
                SVProgressHUD.dismiss()
            }
        }
 
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
     
    }
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    private lazy var sessionsFruitsVC: FruitsVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "FruitsVC") as! FruitsVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    private lazy var sessionsVegetablesVC: VegetablesVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "VegetablesVC") as! VegetablesVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    private lazy var sessionsMeatsVC: MeatsVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "MeatsVC") as! MeatsVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    
    
    
    @IBAction func dissmes(_ sender: Any) {
        if count == 0{
            dismiss(animated: true, completion: nil)
        }else if count == 1 {
            count -= 1
            if ActivationModel.sharedInstance.xtraCusine.contains("9") {
                dismiss(animated: true, completion: nil)
                return
            }
            remove(asChildViewController: sessionsVegetablesVC)
            titlePage.text = "اللحوم"
        }else if count == 2 || count == 3 {
            count -= 1
            remove(asChildViewController: sessionsFruitsVC)
            titlePage.text = "الخضراوات"
        }
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        
        viewController.view.removeFromSuperview()
        
        viewController.removeFromParent()
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChild(viewController)
        
        containerView.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        viewController.didMove(toParent: self)
        
        
        
    }
    @IBAction func noThing(_ sender: Any) {
        if count == 0{
            ActivationModel.sharedInstance.refMeat = ["-1"]
            count += 1
            createEvent(key: "28",date: getDateTime())
            titlePage.text = "الخضراوات"
            add(asChildViewController: sessionsVegetablesVC)
        }else if count == 1{
            ActivationModel.sharedInstance.refVegetables = ["-1"]
            count += 1
            titlePage.text = "الفواكه"
            createEvent(key: "29",date: getDateTime())
            add(asChildViewController: sessionsFruitsVC)
        }else if count == 2{
            createEvent(key: "30",date: getDateTime())

//            count += 1
            ActivationModel.sharedInstance.refFruit = ["-1"]
//            getUserInfoProgressHistory
            if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 || getUserInfo().subscribeType == 0 {

                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                    
                    self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
                    
                }else if getUserInfo().subscribeType == 0 {
//                    if getUserInfo().grown == "2"{ // still in free trial
//                        calculateNormalDietChild()
//                        SVProgressHUD.dismiss()
//                        print("Child")
//                    }else{
//                        calculateNormalDietAdult()
//                        SVProgressHUD.dismiss()
//                        print("Adult")
//                    }//
                    showAlert()
                }else{
                    if dispatchWorkItem != nil {
                        print("dispatchWorkItem cancel .... ")
                        dispatchWorkItem.cancel()
                    }
                    disableView(bool:false)
                if getUserInfo().grown == "2"{
                    calculateNormalDietChild()
                    SVProgressHUD.dismiss()
                    print("Child")
                }else{
                    calculateNormalDietAdult()
                    SVProgressHUD.dismiss()
                    print("Adult")
                }
                }
            }else{
                if getUserInfo().subscribeType == -1 {
                    if getUserInfo().grown == "2"{ // still in free trial
                        calculateNormalDietChild()
                        SVProgressHUD.dismiss()
                        print("Child")
                    }else{
                        calculateNormalDietAdult()
                        SVProgressHUD.dismiss()
                        print("Adult")
                    }
                }else{
                    self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
                }
            }
        }else{
            print("yazan1234")
            if getUserInfo().subscribeType == -1 {
                if getUserInfo().grown == "2"{ // still in free trial
                    calculateNormalDietChild()
                    SVProgressHUD.dismiss()
                    print("Child")
                }else{
                    calculateNormalDietAdult()
                    SVProgressHUD.dismiss()
                    print("Adult")
                }
            }else{
                self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
            }
//            self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)

        }
    }
    
    func showAlert(){
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertView") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.remainingDays = Date().daysBetween(start: Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000.rounded()), end: Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000.rounded()))+1
        self.present(customAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        if count == 0{
            if  ActivationModel.sharedInstance.refMeat.isEmpty {
                ActivationModel.sharedInstance.refMeat = ["-1"]
            }
            createEvent(key: "28",date: getDateTime())
            count += 1
            titlePage.text = "الخضراوات"
            add(asChildViewController: sessionsVegetablesVC)
        }else if count == 1{
            if  ActivationModel.sharedInstance.refVegetables.isEmpty {
                ActivationModel.sharedInstance.refVegetables = ["-1"]
            }
            createEvent(key: "29",date: getDateTime())
            count += 1
            titlePage.text = "الفواكه"
            add(asChildViewController: sessionsFruitsVC)
        }else if count == 2{
            createEvent(key: "30",date: getDateTime())
//            count += 1
            if  ActivationModel.sharedInstance.refFruit.isEmpty {
                ActivationModel.sharedInstance.refFruit = ["-1"]
            }
            if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 || getUserInfo().subscribeType == 0 {
                
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion)! {
                    self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
                    
                }else if getUserInfo().subscribeType == 0 {
//                    if getUserInfo().grown == "2"{ // still in free trial
//                        calculateNormalDietChild()
//                        SVProgressHUD.dismiss()
//                        print("Child")
//                    }else{
//                        calculateNormalDietAdult()
//                        SVProgressHUD.dismiss()
//                        print("Adult")
//                    }
                    showAlert()
                }else{
                    if dispatchWorkItem != nil {
                        print("dispatchWorkItem cancel .... ")
                        dispatchWorkItem.cancel()
                    }
                    disableView(bool:false)
                if getUserInfo().grown == "2"{
                    calculateNormalDietChild()
                    SVProgressHUD.dismiss()
                    print("Child")
                }else{
                    calculateNormalDietAdult()
                    SVProgressHUD.dismiss()
                    print("Adult")
                }
                }
            }else{
                if getUserInfo().subscribeType == -1 {
                    if getUserInfo().grown == "2"{ // still in free trial
                        calculateNormalDietChild()
                        SVProgressHUD.dismiss()
                        print("Child")
                    }else{
                        calculateNormalDietAdult()
                        SVProgressHUD.dismiss()
                        print("Adult")
                    }
                }else{
                    self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
                }
            }
        }else{
            print("yazan1234")
            if getUserInfo().subscribeType == -1 {
                if getUserInfo().grown == "2"{ // still in free trial
                    calculateNormalDietChild()
                    SVProgressHUD.dismiss()
                    print("Child")
                }else{
                    calculateNormalDietAdult()
                    SVProgressHUD.dismiss()
                    print("Adult")
                }
            }else{
                self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
            }
//            self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)

        }
    }
    
    func sntDataToFirebaseFreeTrial(subcribeType:Int){

        print("sntDataToFirebaseFreeTrial")
        _ = DispatchQueue(label: "swiftlee.serial.queue")
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        ActivationModel.sharedInstance.startSubMilli = 0
        
        if subcribeType == 0 { // free
            print("subcribeType == 0")
            ActivationModel.sharedInstance.subscribeType = 0
            ActivationModel.sharedInstance.startPeriod = 0
            ActivationModel.sharedInstance.endPeriod = 6
            ActivationModel.sharedInstance.endSubMilli = 6
            ActivationModel.sharedInstance.expireDate = Date().getDaysDate(value: 6)
            ActivationModel.sharedInstance.subDuration = 7
        }
        

        self.obAPiSubscribeUser.subscribeUser(mealDitr: ActivationModel.sharedInstance.TeeDistribution,flag: 1) { (bool) in
            if bool {
                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
              
                let queue = DispatchQueue(label: "com.appcoda.myqueue")
                
                
                queue.async {
                    DispatchQueue.main.async {
                        self.view.isUserInteractionEnabled = false
                        
                    }
                    _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod:  6,date:getDateOnly())
                    SVProgressHUD.dismiss {
                        self.view.isUserInteractionEnabled = true
                        window?.alpha = 1
                        window?.isOpaque = false
                        DispatchQueue.main.async {
                            self.view.isUserInteractionEnabled = true
                        }
                        ActivationModel.sharedInstance.dispose()
                        self.performSegue(withIdentifier: "finishActicationProccesToHome", sender: self) //sub1
                    }
                }
            }else{
                Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func sntDataToFirebase(){
        print("sntDataToFirebase")
        var filterList = [tblWajbatUserMealPlanner]()
        var filteringPackge = [tblShoppingList]()
        var days:Int!
        var first7Days:Int!
        var expirDays:Int!
        SVProgressHUD.show()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if getUserInfo().subscribeType == -1 ||  getUserInfo().subscribeType == 3 ||  getUserInfo().subscribeType == 2 { // غير مشترك
            // no action
//            Ta5sees.alert(mes: "نعتذر انت غير مشترك", selfUI: self)
            SVProgressHUD.dismiss()
        }else {
            SVProgressHUD.show(withStatus: "يرجى الانتظار ... ")
//            if getUserInfo().subscribeType == 1 {
//                ActivationModel.sharedInstance.subscribeType = 1 // buy
//            } else if getUserInfo().subscribeType == 4{
//                ActivationModel.sharedInstance.subscribeType = 4 // grace period
//            }else{
//                ActivationModel.sharedInstance.subscribeType = 0 // free
//            }
            
            ActivationModel.sharedInstance.subscribeType = getUserInfo().subscribeType
            
            filterList = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).filter { (item) -> Bool in
                if dateFormatter.date(from: item.date)!.timeIntervalSince1970 >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                    return true
                }
                return false
            }
            
            filteringPackge = setupRealm().objects(tblShoppingList.self).filter("userID == %@",getUserInfo().id).filter { (item) -> Bool in
                if  item.date >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                    return true
                }
                return false
            }
            
            
            print(filterList,"filterList",filterList.map {$0.date}.removeDuplicates().count)
            
            
            let dateUser = Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000)
            let startMili = Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)

             days = Date().daysBetween(start: getDateOnlyasDate(), end: dateUser)
            expirDays = Date().daysBetween(start: getDateOnlyasDate(), end: startMili)

//            print("days",expirDays,days,Date().getDaysDate(value: days),getUserInfo().endSubMilli)
            
            ActivationModel.sharedInstance.startSubMilli = 0
            
            if days == 0 {
                ActivationModel.sharedInstance.startPeriod = 0
                
            }else{
                ActivationModel.sharedInstance.startPeriod = days-(filterList.map {$0.date}.removeDuplicates().count-1)
            }
            
            if  ActivationModel.sharedInstance.startPeriod >= 30 {
                ActivationModel.sharedInstance.startPeriod = 0
            }
            ActivationModel.sharedInstance.endPeriod =  days-1
            ActivationModel.sharedInstance.endSubMilli = days
           
            ActivationModel.sharedInstance.expireDate = Double(getUserInfo().expirDateSubscribtion)!
            
            ActivationModel.sharedInstance.subDuration = Int(getUserInfo().subDuration)!

        
        
        self.obAPiSubscribeUser.subscribeUser(mealDitr: ActivationModel.sharedInstance.TeeDistribution,flag: 1) { (bool) in
            if bool {
                
                SVProgressHUD.dismiss()
                
                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
                
                let queue = DispatchQueue(label: "com.appcoda.myqueue")
                dispatchWorkItem = DispatchWorkItem {
              

//                   DispatchQueue.main.async {
                    if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                        if first7Days != days {
                            
                            _ =   tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:first7Days+1,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
//                                let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
//                                let newShoppingList = r[0] as! [tblShoppingList]

                            SVProgressHUD.dismiss {
//                                    try! setupRealm().write{
//                                            setupRealm().add(newWjbah, update:.modified)

//                                    }
//                                    try! setupRealm().write{
//
//                                    setupRealm().add(newShoppingList, update: .modified)
//
//                                    }
                                
//                                try! setupRealm().write{
//                                    if !filterList.isEmpty{
//                                        setupRealm().delete(filterList)
//        //                                setupRealm().add(newWjbah, update:.modified)
//                                    }
//                                }
                                try! setupRealm().write{
                                    if !filteringPackge.isEmpty{
                                        setupRealm().delete(filteringPackge)
        //                                setupRealm().add(newShoppingList, update: .modified)
                                    }
                                }
                                
                                if dispatchWorkItem != nil {
                                    print("finish generate and cancel thread ....")
                                    dispatchWorkItem.cancel()
                                }
                        }
                    }

//                }
//                ActivationModel.sharedInstance.dispose()
                   }
                }
                queue.async {
                    
                    if days >= 7 {
                        first7Days = 6
                    }else{
                        first7Days = days
                    }
                    _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:0,endPeriod:  first7Days,date:getDateOnly())
//                    let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
//                    let newShoppingList = r[1] as! [tblShoppingList]

                    SVProgressHUD.dismiss { [self] in
//                        try! setupRealm().write{
//                            if !filterList.isEmpty{
//                                setupRealm().delete(filterList)
////                                setupRealm().add(newWjbah, update:.modified)
//                            }
//                        }
//                        try! setupRealm().write{
//                            if !filteringPackge.isEmpty{
//                                setupRealm().delete(filteringPackge)
////                                setupRealm().add(newShoppingList, update: .modified)
//                            }
//                        }
                        queue.async(execute: dispatchWorkItem)
                        disableView(bool: true)
                        self.performSegue(withIdentifier: "finishActicationProccesToHome", sender: self) //sub1

                    }
                }
              

              
         

                /*
                 queue.async {
             
 //                    DispatchQueue.global(qos: .background).async {
                         if getUserInfo().subscribeType == 1 {
                             if first7Days != days {
                                 
                                 _ =   tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:first7Days+1,endPeriod:   30,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
 //                                let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
 //                                let newShoppingList = r[0] as! [tblShoppingList]

                                 SVProgressHUD.dismiss {
 //                                    try! setupRealm().write{
 //                                            setupRealm().add(newWjbah, update:.modified)

 //                                    }
 //                                    try! setupRealm().write{
 //
 //                                    setupRealm().add(newShoppingList, update: .modified)
 //
 //                                    }
                                     
                                     try! setupRealm().write{
                                         if !filterList.isEmpty{
                                             setupRealm().delete(filterList)
             //                                setupRealm().add(newWjbah, update:.modified)
                                         }
                                     }
                                     try! setupRealm().write{
                                         if !filteringPackge.isEmpty{
                                             setupRealm().delete(filteringPackge)
             //                                setupRealm().add(newShoppingList, update: .modified)
                                         }
                                     }
                             }
                         }

                     }
                     ActivationModel.sharedInstance.dispose()

                 }
                 */
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
//                    tblPackeg.GenerateMealPlannerFree7Days(date:getDateOnly()) { [self] (err) in
//                        SVProgressHUD.dismiss {
//                            try! setupRealm().write{
//                                if !filterList.isEmpty{
//                                    setupRealm().delete(filterList)
//                                }
//                            }
//                            try! setupRealm().write{
//                                if !filteringPackge.isEmpty{
//                                    setupRealm().delete(filteringPackge)
//                                }
//                            }
//                            self.performSegue(withIdentifier: "finishActicationProccesToHome", sender: self) //sub1
//                            ActivationModel.sharedInstance.dispose()
//                        }
//                    }
//                }
            }else{
                Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
                SVProgressHUD.dismiss()
            }
        }
        }
    }
    func calculateNormalDietAdult(){
        var bmr:Float
        if getUserInfoProgressHistory().refGenderID == "1" {
            
            bmr = Float(24.0 * Double(ActivationModel.sharedInstance.weight)!)
        }  else{
            bmr = Float(0.9 * 24.0 * Double(ActivationModel.sharedInstance.weight)!)
        }
        
        let pa:Float = Float(getRatePA(id:Int(ActivationModel.sharedInstance.refLayaqa)!)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        if getUserInfoProgressHistory().refGenderID == "2" && ActivationModel.sharedInstance.dietType == "4" || ActivationModel.sharedInstance.dietType == "5" {
            if getDataFromSheardPreferanceString(key: "LactationMonths") == "1"  && ActivationModel.sharedInstance.dietType == "5"{
                energyTotal = Int(round(tee + 330))
            }
            else if getDataFromSheardPreferanceString(key: "LactationMonths") == "2" && ActivationModel.sharedInstance.dietType == "5"{
                energyTotal = Int(round(tee + 440))
            } else if getDataFromSheardPreferanceString(key: "Pregnant") == "3" && ActivationModel.sharedInstance.dietType == "4"{
                energyTotal = Int(round(tee  + 452))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "2"  && ActivationModel.sharedInstance.dietType == "4"{
                energyTotal = Int(round(tee  + 350))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "1" && ActivationModel.sharedInstance.dietType == "4" {
                energyTotal = Int(round(tee))
                
            }else {
                energyTotal = Int(round(tee))
                
            }
        }else {
            energyTotal = processDietOrderForTEEAdulte(tee:tee, incrementKACL: Float(ActivationModel.sharedInstance.incrementKACL)!)
            
        }
        setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
        setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
        setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(ActivationModel.sharedInstance.dietType)!)
        
    }
    
    func processDietOrderForTEEAdulte(tee:Float,incrementKACL:Float)->Int{
        if ActivationModel.sharedInstance.dietType == "2"  {
            return Int(round(tee - incrementKACL))
        }else if ActivationModel.sharedInstance.dietType == "1"   {
            return Int(round(tee + incrementKACL))
        }else if ActivationModel.sharedInstance.dietType == "3"   {
            return Int(round(tee))
        }else if ActivationModel.sharedInstance.dietType == "loss" {
            print(Int(round(tee - incrementKACL)))
            return Int(round(tee - incrementKACL))
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "gain" {
            return Int(round(tee + incrementKACL))
            
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "maintain" {
            return Int(round(tee))
            
        }
        return 1
    }
    
    func calculateNormalDietChild(){
        if ageChild >= 1.0 && ageChild <= 3.0{
            print("****0")
            let sub1 = (89.0 * (ActivationModel.sharedInstance.weight as NSString).floatValue - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= 4.0 && ageChild <= 8.0 && getUserInfoProgressHistory().refGenderID == "1" {
            print("****1")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= 4.0 && ageChild <= 8.0 && ageChild <= tblChildAges.range2EndTo(id:2)  && getUserInfoProgressHistory().refGenderID == "2" {
            print("****2")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= 9  && ageChild <= 16 && getUserInfoProgressHistory().refGenderID == "1" {
            print("****3")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
            
        }else if ageChild >= 9  && ageChild <= 16 && ageChild <= tblChildAges.range3EndTo(id:3)  && getUserInfoProgressHistory().refGenderID == "2"{
            print("****4")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }else {
            print("****5")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }
        
        energyTotal = processDietOrderForTEEChild(tee:tee)
        print("tee \(tee ?? 0.0) - energyTotal \(energyTotal)")
        
        //        }
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(ActivationModel.sharedInstance.dietType)!)
    }
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let age = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let sum1 = v2 * age
        let sum2 = (v3 * (ActivationModel.sharedInstance.weight as NSString).floatValue)
        let sum5 = (((getUserInfoProgressHistory().height as NSString).floatValue/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(getRatePA(id:Int(ActivationModel.sharedInstance.refLayaqa)!)) * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        print(sum4)
        return sum4
    }
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let bmi = ActivationModel.sharedInstance.BMI
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: Float(bmi) ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value <= 25 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = getUserInfoProgressHistory().refGenderID
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if getUserInfo().grown == "1"  { // adulte
            return Float(ob.PAadult)
        } else{
            if gender == "1"  { // childe male
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
        }
    }
    func getTeeDistribution(energy:Int){
        var ref = Int(ActivationModel.sharedInstance.dietType)
        
        if ActivationModel.sharedInstance.xtraCusine.contains("8") {
            ref = 8
        }
        var ob = tblTEEDistribution()
        if getUserInfo().grown == "1" { // adulte
            ob.getTeeDistrbution(refID: ref!,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{ // childe
            ob.getTeeDistrbution(refID: ref!,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
    }
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)
        
        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        sheardPreferanceWrite(fat: Float(fat),pro: Float((pro)),carbo: Float(carbo),energy: Float(energy))
        
    }
    
    func getservMenuPlan(id:Int){
        
        var ob = tblMenuPlanExchangeSystem()
        if getUserInfo().grown == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }
        
        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AdvViewControllerstoHome" {
            if let dis=segue.destination as? AdvViewControllers{
                dis.loadViewIfNeeded()
            }
        }
    }
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.dietType, key: "dietType")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.target_weight, key: "targetValue")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.week, key: "week")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.incrementKACL, key: "incrementKACL")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.refLayaqa, key: "refLayaqaCondID")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.weight, key: "txtweight")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.xtraCusine.joined(separator: ","), key: "cusinene")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.refAllergys.joined(separator: ","), key: "allergy")
        
        setDataInSheardPreferance(value:getUserInfo().id, key: "mobNum")

        setDataInSheardPreferance(value:getUserInfo().loginType, key: "loginType")
        
//        setDataInSheardPreferance(value: getUserInfo().refAllergyInfoID, key: "allergy")
        runNotfyWajbeh()
    }
}



extension ContainerVC:ApiResponseDaelegat{
    
    func showIndecator() {
        SVProgressHUD.show()
    }
    
    func HideIndicator() {
        SVProgressHUD.dismiss()
    }
    
    func getResponseSuccuss() {}
    
    func getResponseFalueir(meeage:String) {
        let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getResponseMissigData() {}
    
    func runNotfyWajbeh(){
        var selctionID = "0"
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let notfy = ActivationModel.sharedInstance.refMealDistrbution
        tblAlermICateItems.disableAllNotfy()
        for item in notfy {
            
            
            if item.selected {
                if !item.checkAsMain {
                    selctionID = "2" //main
                }else{
                    selctionID = "1" // not main
                }

                let time = selectDate(date:getTimeAsDateType(time:item.time_food_date_str))
                showNotify(id:item.id,body:getInfoNotfy(id:item.id),hour:time[0],minut:time[1])
                
                tblAlermICateItems.updateAlerttimeNotificationActivation(cateItem:item.id,hour:time[0],min:time[1])
                ActivationModel.sharedInstance.refmealPlaners.append(["id":Int(item.id!)!,"selectionId" :Int(selctionID)!,"time": item.time_food_date_str!])
            }else{
                ActivationModel.sharedInstance.refmealPlaners.append(["id":Int(item.id!)!,"selectionId" :0,"time": item.time_food_date_str!])
            }
        }
        if getUserInfo().subscribeType == -1 {
            sntDataToFirebaseFreeTrial(subcribeType: 0)
        }else {
            sntDataToFirebase()
        }
        
    }
    func disableView(bool:Bool){
        view.isUserInteractionEnabled = bool
    }
    func selectDate(date:Date)->[Int]{
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        
        print("TIME",hour,minute)
        return [hour,minute]
    }
    func getInfoNotfy(id:String) ->String {
        switch id {
        case "1":
            return   getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
        case "2":
            return getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
        case "3":
            return getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
        case "4":
            return getTitleNotify(keyRemote:"snack1RemindersTitles",keyValue:"snack1Title",sheardKey:"snack1TitleCounter")
        case "5":
            return getTitleNotify(keyRemote:"snack2RemindersTitles",keyValue:"snack2Title",sheardKey:"snack2TitleCounter")
        default:
            return ""
        }
    }
}
extension ContainerVC : CustomAlertViewDelegate {
    func okButtonTapped(selectedOption: String, textFieldValue: String) {
        if getUserInfo().grown == "2"{ // still in free trial
            calculateNormalDietChild()
            SVProgressHUD.dismiss()
            print("Child")
        }else{
            calculateNormalDietAdult()
            SVProgressHUD.dismiss()
            print("Adult")
        }
    }
    
    func cancelButtonTapped() { // go to cv payment
        print("AdvViewControllerstoHome")
        self.performSegue(withIdentifier: "AdvViewControllerstoHome", sender: nil)
    }
}


extension ContainerVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
