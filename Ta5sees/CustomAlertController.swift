//
//  CustomAlertController.swift
//  Ta5sees
//
//  Created by Admin on 9/15/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SwiftyJSON

class CustomAlertController: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var btnDismessOutlet: UIButton!
    @IBAction func dissmes(_ sender: Any) {
        self.view.window?.removeGestureRecognizer(tap)
        dissmesController()
    }
    var pd:UIViewController?
    var id_Notification:String!
    var isFlagController = "0" // 0-> viewCotroller else AlarmSitingController
    var datePicker: UIDatePicker = UIDatePicker()
    var viewAccountPresnter:PresnterAccount?

    @IBOutlet weak var lblstate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        print("#### \(setupRealm().objects(tblAlermICateItems.self))")
        lblTitle.text = "تعديل منبه \(setTilePage(id:id_Notification))"
        btnShowDialogOutlet.backgroundColor = .clear
        btnShowDialogOutlet.layer.cornerRadius = btnShowDialogOutlet.frame.height/2
        btnShowDialogOutlet.layer.borderWidth = 0.5
        btnShowDialogOutlet.layer.borderColor = colorGray.cgColor
        lblTitle.font = UIFont(name: "GE Dinar One",size:18)
        btnShowDialogOutlet.titleLabel?.font =  UIFont(name: "GE Dinar One",size:17)
        btnDismessOutlet.titleLabel?.font =  UIFont(name: "GE Dinar One",size:17)
        btnShowDialogOutlet.setTitle(tblAlermICateItems.getalertNotification(cateID: id_Notification), for: .normal)
        
//        btnDismessOutlet.isHidden = true
        btnDismessOutlet.layer.cornerRadius = 9
        btnDismessOutlet.layer.borderWidth = 0.5
        btnDismessOutlet.layer.borderColor = colorGreen.cgColor
        
        if tblAlermICateItems.getalertNotificationActive(cateID:id_Notification) == "0" {
            lblstate.text = "إلغاء"
            switchValueOutlet.isOn = true
        }else{
            lblstate.text = "تفعيل"
            switchValueOutlet.isOn = false
            
        }
    }
    var tap: UITapGestureRecognizer!
    override func viewDidAppear(_ animated: Bool) {

        tap = UITapGestureRecognizer(target: self, action: #selector(onTap(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.cancelsTouchesInView = true
        tap.delegate = self
        self.view.window?.addGestureRecognizer(tap)
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let location = touch.location(in: self.view)

        if self.view.point(inside: location, with: nil)  {
        
            return false
        }else {
            return true
        }
    }

    @objc private func onTap(sender: UITapGestureRecognizer) {
        self.view.window?.removeGestureRecognizer(sender)
        dissmesController()
    }
    @IBOutlet weak var switchValueOutlet: UISwitch!
    
    @IBAction func switchValue(_ sender: UISwitch) {
        if (sender.isOn == true){
            print("on")
            let time = tblAlermICateItems.getalertNotificationAsNumer(cateID: id_Notification)
            print("time ---  \(time)")
            if id_Notification == "1" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])
 
            }else  if id_Notification == "2" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])

            }else  if id_Notification == "3" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])

            }else  if id_Notification == "4" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])

            }else  if id_Notification == "5" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])

            }else  if id_Notification == "6" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])

            }else  if id_Notification == "7" {
                craeteNOtfy(hours:time[0],min:time[1])
                tblAlermICateItems.updateAlertNotification(cateItem:id_Notification,hour:time[0],min:time[1])
            }

        }
        else{
            print("off")
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id_Notification])
            tblAlermICateItems.updateAlertNotificationActive(cateItem: id_Notification)
        }
        deleteCenterNotification()
    }
    @IBOutlet weak var btnShowDialogOutlet: UIButton!
    
    @IBAction func btnShowDialog(_ sender: Any) {
       
        TimePicker(time: tblAlermICateItems.getalertNotificationForText(cateID: id_Notification))
    }
    func setTilePage(id:String)->String{
        switch id {
        case "1":
            return "الفطور"
        case "2":
            return "الغداء"
        case "3":
            return "العشاء"
        case "4":
            return "تصبيرة  صباحي"
        case "5":
            return "تصبيرة مسائي"
        case "6":
            return "التمرين"
        case "7":
            return "الماء"
        default:
            print("not found !!")
        }
        return ""
    }
    func TimePicker(time:String) {
        DPPickerManager.shared.showPicker(title: "الوقت",time: time, flag: 1, idTime: id_Notification, picker: { [self] (picker) in
            self.view.window?.removeGestureRecognizer(tap)
            picker.datePickerMode = .time
            picker.datePickerMode = UIDatePicker.Mode.time
            picker.locale = NSLocale(localeIdentifier: "da_DK") as Locale
        }) { [self] (date, cancel) in
            if !cancel {
                self.selectDate(date: date!)
                self.view.window?.addGestureRecognizer(tap)
       
            }
        }
    }
    func checkTime(cutentDate:Date)->Bool{
        let arrDate = tblAlermICateItems.getalertNotificationAsTime(id: id_Notification)
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.string(from:cutentDate)
        let seletedDate = dateFormatter.date(from: date)
        
        if id_Notification == "1" {
            for item in arrDate {
                if seletedDate!.timeIntervalSince1970 > item.time{
                    return false
                }
            }
            return true
            
        }else  if id_Notification == "2" {
            for item in arrDate {
                if item.id == "3" || item.id == "5" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "2" ||  item.id == "1"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }else  if id_Notification == "3" {
            for item in arrDate {
                if seletedDate!.timeIntervalSince1970 < item.time{
                    return false
                }
            }
            return true
        }else  if id_Notification == "4" {
            for item in arrDate {
                if item.id == "3" ||  item.id == "2" || item.id == "5" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "1"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }else  if id_Notification == "5" {
            for item in arrDate {
                if  item.id == "3" {
                    if seletedDate!.timeIntervalSince1970 > item.time{
                        return false
                    }
                }else if  item.id == "1" || item.id == "4" ||  item.id == "2"{
                    if seletedDate!.timeIntervalSince1970 < item.time{
                        return false
                    }
                }
            }
            return true
        }
        return true
    }
    func selectDate(date:Date){
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        if !checkTime(cutentDate:date) {
            alert(mes:"يجب ان يكون وقت الوجبة اقل من وقت الوجبات الاخزى" , selfUI: self)
            return
        }
        var hourAS12 = -1
        var isMorning = ""
        var minuet = ""
        
        if hour > 12 {
            isMorning = "PM"
            hourAS12 = hour - 12
        }else{
            isMorning = "AM"
            hourAS12 = hour
        }
        if minute == 0 {
            minuet = "00"
        }else {
            minuet = String(minute)
        }
        
     
        
        if tblAlermICateItems.getalertNotificationActive(cateID:id_Notification) == "0" {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id_Notification])
            if id_Notification == "1" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "2" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "3" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "4" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "5" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "6" {
                craeteNOtfy(hours:hour,min:minute)
            }else  if id_Notification == "7" {
                craeteNOtfy(hours:hour,min:minute)
            }
        }
       
        btnShowDialogOutlet.setTitle("\(hourAS12):\(minuet) \(isMorning)", for: .normal)
        tblAlermICateItems.updateAlerttimeNotification(cateItem:id_Notification,hour:hour,min:minute)
        
        print(hour)
        print(minute)
        
        
    }
    
    
    func showNotify(title:String,body:String,hours:Int,min:Int)   {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id_Notification])
        let content = UNMutableNotificationContent()
        content.title = "للتذكير"
        content.body = body
        content.sound = .default
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        content.userInfo = ["alarm":"\(id_Notification!)"]
        //        dateComponents.weekday = 3
        dateComponents.hour = hours
        dateComponents.minute = min
        content.categoryIdentifier = "SYNC_CATEGORY"
        // Create the trigger as a repeating event.
        let trigger = UNCalendarNotificationTrigger(
            dateMatching: dateComponents, repeats: true)
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
        //        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: id_Notification,
                                            content: content, trigger: trigger)
        let syncAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                                              title: "دخول",
                                              options: UNNotificationActionOptions(rawValue: 0))
        let syncCategory =
            UNNotificationCategory(identifier: "SYNC_CATEGORY",
                                   actions: [syncAction],
                                   intentIdentifiers: [],
                                   hiddenPreviewsBodyPlaceholder: body,
                                   options: .customDismissAction)
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.setNotificationCategories([syncCategory])
        notificationCenter.add(request)
        
    }
    
   
    func craeteNOtfy(hours:Int,min:Int){
         var titleNotfy = ""
        if id_Notification == "1" {
             titleNotfy = getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "2" {
             titleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "3" {
           titleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "4" {
             titleNotfy =  getTitleNotify(keyRemote:"snack1RemindersTitles",keyValue:"snack1Title",sheardKey:"snack1TitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "5" {
             titleNotfy =  getTitleNotify(keyRemote:"snack2RemindersTitles",keyValue:"snack2Title",sheardKey:"snack2TitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "6" {
             titleNotfy =  getTitleNotify(keyRemote:"exerciseRemindersTitles",keyValue:"exerciseTitle",sheardKey:"exerciseTitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "7" {
             titleNotfy =  getTitleNotify(keyRemote:"waterRemindersTitles",keyValue:"waterTitle",sheardKey:"waterTitleCounter")
            showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
        }else if id_Notification == "8" {
                     titleNotfy =  getTitleNotify(keyRemote:"weightRemindersTitles",keyValue:"weightTitle",sheardKey:"weightTitleCounter")
              showNotify(title:"تخسيس",body:titleNotfy,hours:hours,min:min)
                }
        
    }
    
    func dissmesController(){
        pd?.view.isUserInteractionEnabled = true
        if isFlagController == "0" {
        pd?.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        dismiss(animated: true, completion: nil)
    }
//    func getTitleNotify(keyRemote:String,keyValue:String,sheardKey:String)->String{
//        var titleNotfy = ""
//         var count = getDataFromSheardPreferanceInt(key: sheardKey)
//        
//        if let recommends = JSON(remoteConfig[keyRemote].jsonValue!).array {
//            titleNotfy = recommends[count][keyValue].stringValue
//        }
//       
//        count+=1
//        if count > 4{
//            count = 0
//        }
//        setDataInSheardPreferanceInt(value:count, key: sheardKey)
//        return titleNotfy
//    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
extension CustomAlertController:DatePickerDelegate {
    func cancelBtnClicked() {
        
        self.view.endEditing(true)
    }
    
    func doneBtnClicked(){
        
        let hour = calendar.component(.hour, from: datePicker.date)
        let minute = calendar.component(.minute, from: datePicker.date)
        
        print("hour\(hour)")
        print(minute)
        
    }
}


/*
 
 
 //
 //  AddEditViewController.swift
 //  Ta5sees
 //
 //  Created by Admin on 5/29/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //


 protocol ShowNewWajbeh {
     func goToViewWajbeh(idWajbeh:String,flag:String)
 }

 import UIKit

 class AddEditCategoryController: UIViewController ,UISearchBarDelegate {
     
     
     var refWInfo:Int!
     var refWCAtegory:Int!
     var arrPassData:[Int] = []
     var obShowNewWajbeh:ShowNewWajbeh?
     var id_items:String!
     var date:String!
     var pg:protocolWajbehInfoTracker?
     var filteredData: [tblPackeg]!
     var filteredDataSenf =  [SearchItem]()
     var filteredDataSenf1 =  [SearchItem]()

     let list =  tblPackeg.searchModelArray()
     let listSenf1 =  tblSenf.searchModelArray()
     
     var listSenf =  [SearchItem]()
     var lissPrioritySenf = [SearchItem]()
     var filterItem = [SearchItem]()
     let tableView = UITableView()
     var safeArea: UILayoutGuide!
     
     @IBOutlet weak var titlePage: UILabel!
     @IBAction func buDismiss(_ sender: Any) {
         dismiss(animated: true, completion: nil)
     }
     
     @IBAction func favourites(_ sender: Any) {
         self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")
         
     }
     @IBAction func addANDEdite(_ sender: Any) {
         self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
         
     }
     @IBAction func recentlyUsed(_ sender: Any) {
         //
         self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
     }
     
     @IBOutlet weak var  searchBar: UISearchBar!
     
     
     @IBOutlet weak var lblTestSearch: UISearchBar!
     
     //    @IBAction func btndelet(_ sender: Any) {
     //        let ob = realm.objects(tblWajbehDetailing.self).filter("id == %@",1)
     //        try! realm.write {
     //            realm.delete(ob)
     //        }
     //    }
     //    @IBAction func edite(_ sender: Any) {
     //        self.performSegue(withIdentifier: "mainmenus", sender: [1,3])
     //
     //    }
     //    @IBOutlet weak var search: UISearchBar!
     override func viewDidLoad() {
         super.viewDidLoad()
         //        txtSearchOutlet.obShowNewWajbeh = self
         obShowNewWajbeh = self
         tableView.dataSource = self
         tableView.delegate = self
         
         searchBar.delegate = self
         setTilePage(id:id_items)
         let thread =  DispatchQueue.global(qos: .background)
         thread.sync {
             self.listSenf.append(contentsOf: self.list)
         }
         let thread1 =  DispatchQueue.global(qos: .background)
         thread1.sync {
             self.listSenf.append(contentsOf:self.listSenf1)
         }
         //        setTilePage(id:"1")
         
     }
     
     func textFieldDidChange(textField: UITextField) {
         //your code
     }
     
     
     
   
     
     func setTilePage(id:String){
         
         switch id {
         case "1":
             titlePage.text! = "إضافة فطور"
             break
         case "2":
             titlePage.text! = "إضافة غداء"
             break
         case "3":
             titlePage.text! = "إضافة عشاء"
             break
         case "4":
             titlePage.text! = "إضافة تصبيرة ١"
             break
         case "5":
             titlePage.text! = "إضافة تصبيرة ٢"
             break
         default:
             print("not found !!")
         }
     }
     
     
     func getUserInWithThe( userIn:String)->String {
         var polyUserIn = userIn;
         if userIn.count >= 3{
             let new = Array(userIn) //userIn.split(separator: ",")
 //            print("new \(new)")
             if "\(new[0])\(new[1])" == "ال" {
                 polyUserIn = String(userIn.dropFirst(2))
             }else{
                 polyUserIn = "ال\(userIn)";
             }
             
 //            if (userIn.hasPrefix("ال")) {
 //                polyUserIn = String(userIn.dropFirst(2))
 //            }else{
 //                polyUserIn = "ال\(userIn)";
 //            }
         }
             
         
         return polyUserIn;
     }
     func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
         let components = searchKey.components(separatedBy: "،")
         for item in components {
 //            if item.elementsEqual(userINp) ||  checkString(item:item,item2:userINp) ||  checkString(item:item,item2:getUserInWithThe(userIn: userINp))  {
 //                return true
 //            }
 //            if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
 //            return true
 //            }
             if item.contains(userINp)  {
                 return true
             }
         }
         return false
     }
     
     func checkString(item:String,item2:String)->Bool{
         let arr = Array(item)
         var str = ""
         for cha in arr {
             if cha != "," || cha != "،" {
                 str.append(cha)
                 if str == item2 {
                     return true
                 }
             }
         }
         return false
     }

     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
     }
     @objc func saveItemHistory(){
         if filteredDataSenf.isEmpty {
             tblHistoryNotFoundMeals.setMeals(name:lblTestSearch.text!)
         }
         
     }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         self.perform(#selector(saveItemHistory),with:nil,afterDelay: 2)
         if searchText.isEmpty == true {
             tableView.isHidden = true
             tableView.removeFromSuperview()
         }else{
             autoreleasepool {

             setupTableView()
             safeArea = view.layoutMarginsGuide
             var lissPrioritySenf = [SearchItem]()
             filteredDataSenf = []
             self.filteredDataSenf  = searchText.isEmpty ? self.listSenf : self.listSenf.filter { (item: SearchItem) -> Bool in
                 
                 let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
                 
                 if range.contains(" ") {
                     
                     let input =  searchText.components(separatedBy: " ")

                     var counter = 0
                     let thread =  DispatchQueue.global(qos: .background)
                     thread.async {
                         for text in input {
                             if self.confirmSearchKey(userINp:text,searchKey:item.iteme_search,nameMel:item.iteme_name) {
                                 counter+=1
                             }
                         }
                     }
                     if counter == input.count {
                         return true
                     }else{
                         for index in input{
                             if self.confirmSearchKey(userINp:index,searchKey:item.iteme_search,nameMel:item.iteme_name){
                                 return true

                             }else{
 //                                lissPrioritySenf.append(item)
                                 
                             }
                         }
                     }
                 }else{
                     if self.confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.iteme_search,nameMel:item.iteme_name) {
                         return true

                     }
                 }
                 
                 
                 //
                 
                 
                 
                 return false
                 
                 
             }
             let thread =  DispatchQueue.global(qos: .background)
             thread.async {
                 if lissPrioritySenf.count > 0 {
                     self.filteredDataSenf.append(contentsOf: lissPrioritySenf)
                     self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
                     lissPrioritySenf.removeAll()
                 }
             }
             
         
                 
                 var sortedResultSenf = [SearchItem]()
                 var lissPrioritys = [SearchItem]()

                 for resultsSenf in self.filteredDataSenf {
                     let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()

                     if range.contains(" ") {
 //                        let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                         let constraintKeys =  searchText.components(separatedBy: " ")

                         var counter = 0
                         let thread =  DispatchQueue.global(qos: .background)
                         thread.async {
                             for text in constraintKeys {
                                 if self.confirmSearchKey(userINp:text,searchKey:resultsSenf.iteme_search,nameMel:resultsSenf.iteme_name) {
                                     counter+=1
                                 }

                             }
                         }
                         if counter == constraintKeys.count {
                             sortedResultSenf.append(resultsSenf)
                         }else{

                             for index in constraintKeys{
                                 if self.matchesKey(constraint: index, searchKey: resultsSenf.iteme_search) == true {
                                     sortedResultSenf.append(resultsSenf)

                                 }else {
                                     lissPrioritys.append(resultsSenf)
                                 }
                             }
                         }
                     }else if self.matchesKey(constraint: searchText, searchKey: resultsSenf.iteme_search) == true {
                         sortedResultSenf.append(resultsSenf)


                     }else{
                         lissPrioritys.append(resultsSenf)
                     }


                 }
                 if lissPrioritys.count > 0 {
                     sortedResultSenf.append(contentsOf: lissPrioritys)
                 }
                 filteredDataSenf = []
                 self.filteredDataSenf = sortedResultSenf
                 self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
 //
 //
             }
 //
             
             UIView.animate(withDuration: 5) {
                 DispatchQueue.main.async {
                     self.tableView.reloadData()
                     self.tableView.isHidden = false
                 }
                 
             }
     }
 }
     
     func binarySearch<T:Comparable>(_ inputArr:Array<T>, _ searchItem: T) -> Int? {
         var lowerIndex = 0
         var upperIndex = inputArr.count - 1

         while (true) {
             let currentIndex = (lowerIndex + upperIndex)/2
             if("\(inputArr[currentIndex])".elementsEqual("\(searchItem)")) {
                 return currentIndex
             } else if (lowerIndex > upperIndex) {
                 return nil
             } else {
                 if(!"\(inputArr[currentIndex])".elementsEqual("\(searchItem)")) {
                     upperIndex = currentIndex - 1
                 } else {
                     lowerIndex = currentIndex + 1
                 }
             }
         }
     }
     
     func matchesKey(constraint:String,searchKey:String)->Bool{
 //        if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
 //            return true
 //        }
         
 //        let myArray = ["1","2","e"]
 //        let myArray = searchKey.components(separatedBy: " ")
 //        if let searchIndex = binarySearch(myArray, constraint) {
 //            print("Element found on index: \(searchIndex)")
 //            if searchIndex > -1 {
 //            return true
 //            }
 //        }
 //
         return false
     }
     
     
     
     func setupTableView() {
         view.addSubview(tableView)
         tableView.translatesAutoresizingMaskIntoConstraints = false
         tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
         tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
         tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     
     override func loadView() {
         super.loadView()
         
         searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
         searchBar.searchTextField.textAlignment = .right
         self.tableView.isHidden = true
         
     }
     
     
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         searchBar.resignFirstResponder()
         
     }
     
     
     
 }




 extension AddEditCategoryController:ShowNewWajbeh{
     func goToViewWajbeh(idWajbeh: String, flag: String) {
         
         if flag == "wajbeh"{
             self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
         }else {
             self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
         }
         //        txtSearchOutlet.text = nil
         
     }
     
     
     
 }


 extension AddEditCategoryController: UITableViewDelegate, UITableViewDataSource {
     
     
     //////////////////////////////////////////////////////////////////////////////
     // Table View related methods
     //////////////////////////////////////////////////////////////////////////////
     
     
     // MARK: TableView creation and updating
     
     // Create SearchTableview
     
     
     
     // MARK: TableViewDataSource methods
     public func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
     
     public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return filteredDataSenf.count
     }
     
     // MARK: TableViewDelegate methods
     
     //Adding rows in the tableview with the data from dataList
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
         
         
         cell.nameSanf.text = filteredDataSenf[indexPath.row].getStringText()
         cell.caloris.text =  filteredDataSenf[indexPath.row].getStringTextcaloris()
         cell.catrgory.text =  filteredDataSenf[indexPath.row].getStringTextCate()
         
         cell.imageSenf.image = UIImage(named : "logo")
         SetImage(laImage:cell.imageSenf)
         
         return cell
     }
     
     func SetImage(laImage:UIImageView){
         laImage.layer.borderWidth = 1
         laImage.layer.masksToBounds = false
         laImage.layer.borderColor = colorGray.cgColor
         laImage.layer.cornerRadius = laImage.frame.height/2
         laImage.clipsToBounds = true
         
     }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("selected row \(filteredDataSenf[indexPath.row].item_id)")
         obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filteredDataSenf[indexPath.row].item_id,flag:filteredDataSenf[indexPath.row].item_flag)
         tableView.isHidden = true
         
     }
     
     
     
 }



 extension AddEditCategoryController {
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "FavuriteRecentController" {
               if let dis=segue.destination as?  FavuriteRecentController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }else   if segue.identifier == "DisplayCateItemsController" {
               if let dis=segue.destination as?  DisplayCateItemsController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                               
                           }
                           else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "DisplayCateItemsSenfController" {
               if let dis=segue.destination as?  DisplayCateItemsSenfController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                           }else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "RecentUsedItemController" {
               if let dis=segue.destination as?  RecentUsedItemController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }
           
       }
       
 }


 /*
  //
  //  TestSearch.swift
  //  Ta5sees
  //
  //  Created by Admin on 9/10/20.
  //  Copyright © 2020 Telecom enterprise. All rights reserved.
  //
  
  import UIKit
  
  class TestSearch: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return resultsList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
  cell.textLabel?.text = resultsList[indexPath.row].getStringText()
  return cell
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  var filteredData: [tblPackeg]!
  var resultsList : [SearchItem] = [SearchItem]()
  var resultsListLiss : [SearchItem] = [SearchItem]()
  
  let list =  tblPackeg.searchModelArray()
  
  override func viewDidLoad() {
  super.viewDidLoad()
  tableView.dataSource = self
  searchBar.delegate = self
  filteredData = list
  print("RRRRRRR\("DFSDASDBMNMF ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  }
  
  func getUserInWithThe( userIn:String)->String {
  var polyUserIn = userIn;
  if userIn.count >= 3{
  if (userIn.hasPrefix("ال")) {
  polyUserIn = String(userIn.dropFirst(2))
  }else{
  polyUserIn = "ال\(userIn)";
  }
  }
  return polyUserIn;
  }
  func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
  let components = searchKey.components(separatedBy: "،")
  for item in components {
  if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
  return true
  }
  }
  return false
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
  var lissPriority = [tblPackeg]()
  resultsList.removeAll()
  filteredData = searchText.isEmpty ? list : list.filter { (item: tblPackeg) -> Bool in
  
  ////            let whitespace = NSCharacterSet.whitespaces
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  //.self.rangeOfCharacter(from: whitespace)
  
  if range.contains(" ") {
  let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  for text in input {
  if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
  counter+=1
  }
  
  }
  if counter == input.count {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  for index in input.reversed(){
  if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  lissPriority.append(item)
  resultsListLiss.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  
  }
  }
  }
  }else{
  if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }
  }
  
  
  //
  
  
  
  return false
  
  
  }
  
  
  if lissPriority.count > 0 {
  filteredData.append(contentsOf: lissPriority)
  filteredData = filteredData.removeDuplicates()
  lissPriority.removeAll()
  }
  
  if resultsListLiss.count > 0 {
  resultsList.append(contentsOf: resultsListLiss)
  resultsList = resultsList.removeDuplicates()
  resultsListLiss.removeAll()
  }
  //        var sortedResult = [tblPackeg]();
  var sortedResult = [SearchItem]();
  if sortedResult.count > 0 {
  sortedResult.removeAll()
  }
  
  for results in resultsList {
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  
  if range.contains(" ") {
  let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  
  for text in constraintKeys {
  if confirmSearchKey(userINp:text,searchKey:results.iteme_search,nameMel:results.iteme_name) {
  counter+=1
  }
  
  }
  if counter == constraintKeys.count {
  sortedResult.append(results)
  //                    sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else{
  for index in constraintKeys.reversed(){
  if matchesKey(constraint: index, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //                              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else {
  //                            lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  }
  }
  }else if matchesKey(constraint: searchText, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  
  }else{
  //                lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  
  
  }
  if resultsListLiss.count > 0 {
  sortedResult.append(contentsOf: resultsListLiss)
  }
  //        if lissPriority.count > 0 {
  //            sortedResult.append(contentsOf: lissPriority)
  //        }
  resultsList.removeAll()
  resultsList = sortedResult
  
  tableView.reloadData()
  }
  
  
  
  func matchesKey(constraint:String,searchKey:String)->Bool{
  if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
  return true
  }
  
  return false
  }
  
  
  
  func binarySearch( arr:[String],firstIndex:Int, lastIndex:Int, key:String)->Int
  {
  if (lastIndex >= firstIndex) {
  let mid = firstIndex + (lastIndex - firstIndex) / 2;
  
  //arr[mid].compare(key, options: NSString.CompareOptions.caseInsensitive).rawValue)
  if arr[mid] == key {
  return mid;
  
  }
  
  if arr[mid] > key {
  return binarySearch(arr: arr, firstIndex: firstIndex, lastIndex: mid - 1, key: key)
  }
  
  return binarySearch(arr: arr, firstIndex: mid + 1, lastIndex: lastIndex, key: key)
  }
  
  
  return -1;
  }
  
  
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  self.searchBar.showsCancelButton = true
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
  searchBar.showsCancelButton = false
  searchBar.text = ""
  searchBar.resignFirstResponder()
  }
  }
  
  struct SenfPackage {
  var tilie = ""
  var name = ""
  var id = ""
  var isWajbeh = ""
  var searchKey = ""
  //    init(tilie:String,) {
  //        statements
  //    }
  }
  
  
  //   let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                print(input)
  //                for text in input {
  //                    if text != "" {
  //                        return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                    }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                          print(input)
  //                for text in input {
  //                    if text != "" {
  //                   return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  
  /*
  filteredData.removeAll()
  for item in list {
  let components = item.searchKey!.components(separatedBy: "،")
  //            print(components.joined(separator: " "))
  
  let whitespace = NSCharacterSet.whitespaces
  let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  let input =  searchText.lowercased().components(separatedBy: " ")
  if range != nil {
  
  for text in input {
  if   components.joined(separator: "").lowercased().contains(text.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: text, isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }else{
  if   components.joined(separator: " ").lowercased().contains(searchText.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  //            return false
  //
  //
  //        }
  
  }
  
  */
  
  
  
  
  */

 */



/*
 
 //
 //  AddEditViewController.swift
 //  Ta5sees
 //
 //  Created by Admin on 5/29/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //


 protocol ShowNewWajbeh {
     func goToViewWajbeh(idWajbeh:String,flag:String)
 }

 import UIKit

 class AddEditCategoryController: UIViewController ,UISearchBarDelegate {
     
     
     var refWInfo:Int!
     var refWCAtegory:Int!
     var arrPassData:[Int] = []
     var obShowNewWajbeh:ShowNewWajbeh?
     var id_items:String!
     var date:String!
     var pg:protocolWajbehInfoTracker?
     var filteredData: [tblPackeg]!
     var filteredDataSenf =  [SearchItem]()
     var filteredDataSenf1 =  [SearchItem]()

     let list =  tblPackeg.searchModelArray()
     let listSenf1 =  tblSenf.searchModelArray()
     
     var listSenf =  [SearchItem]()
     var lissPrioritySenf = [SearchItem]()
     var filterItem = [SearchItem]()
     let tableView = UITableView()
     var safeArea: UILayoutGuide!
     
     @IBOutlet weak var titlePage: UILabel!
     @IBAction func buDismiss(_ sender: Any) {
         dismiss(animated: true, completion: nil)
     }
     
     @IBAction func favourites(_ sender: Any) {
         self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")
         
     }
     @IBAction func addANDEdite(_ sender: Any) {
         self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
         
     }
     @IBAction func recentlyUsed(_ sender: Any) {
         //
         self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
     }
     
     @IBOutlet weak var  searchBar: UISearchBar!
     
     
     @IBOutlet weak var lblTestSearch: UISearchBar!
     
     //    @IBAction func btndelet(_ sender: Any) {
     //        let ob = realm.objects(tblWajbehDetailing.self).filter("id == %@",1)
     //        try! realm.write {
     //            realm.delete(ob)
     //        }
     //    }
     //    @IBAction func edite(_ sender: Any) {
     //        self.performSegue(withIdentifier: "mainmenus", sender: [1,3])
     //
     //    }
     //    @IBOutlet weak var search: UISearchBar!
     override func viewDidLoad() {
         super.viewDidLoad()
         //        txtSearchOutlet.obShowNewWajbeh = self
         obShowNewWajbeh = self
         tableView.dataSource = self
         tableView.delegate = self
         
         searchBar.delegate = self
         setTilePage(id:id_items)
         let thread =  DispatchQueue.global(qos: .background)
         thread.sync {
             self.listSenf.append(contentsOf: self.list)
         }
         let thread1 =  DispatchQueue.global(qos: .background)
         thread1.sync {
             self.listSenf.append(contentsOf:self.listSenf1)
         }
         //        setTilePage(id:"1")
         
     }
     
     func textFieldDidChange(textField: UITextField) {
         //your code
     }
     
     
     
   
     
     func setTilePage(id:String){
         
         switch id {
         case "1":
             titlePage.text! = "إضافة فطور"
             break
         case "2":
             titlePage.text! = "إضافة غداء"
             break
         case "3":
             titlePage.text! = "إضافة عشاء"
             break
         case "4":
             titlePage.text! = "إضافة تصبيرة ١"
             break
         case "5":
             titlePage.text! = "إضافة تصبيرة ٢"
             break
         default:
             print("not found !!")
         }
     }
     
     
     func getUserInWithThe( userIn:String)->String {
         var polyUserIn = userIn;
         if userIn.count >= 3{
             let new = Array(userIn) //userIn.split(separator: ",")
 //            print("new \(new)")
             if "\(new[0])\(new[1])" == "ال" {
                 polyUserIn = String(userIn.dropFirst(2))
             }else{
                 polyUserIn = "ال\(userIn)";
             }
             
 //            if (userIn.hasPrefix("ال")) {
 //                polyUserIn = String(userIn.dropFirst(2))
 //            }else{
 //                polyUserIn = "ال\(userIn)";
 //            }
         }
             
         
         return polyUserIn;
     }
     func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
         let components = searchKey.components(separatedBy: "،")
         for item in components {
 //            if item.elementsEqual(userINp) ||  checkString(item:item,item2:userINp) ||  checkString(item:item,item2:getUserInWithThe(userIn: userINp))  {
 //                return true
 //            }
 //            if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
 //            return true
 //            }
             if item.contains(userINp)  {
                 return true
             }
         }
         return false
     }
     
     func checkString(item:String,item2:String)->Bool{
         let arr = Array(item)
         var str = ""
         for cha in arr {
             if cha != "," || cha != "،" {
                 str.append(cha)
                 if str == item2 {
                     return true
                 }
             }
         }
         return false
     }

     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
     }
     @objc func saveItemHistory(){
         if filteredDataSenf.isEmpty {
             tblHistoryNotFoundMeals.setMeals(name:lblTestSearch.text!)
         }
         
     }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         self.perform(#selector(saveItemHistory),with:nil,afterDelay: 2)
         if searchText.isEmpty == true {
             tableView.isHidden = true
             tableView.removeFromSuperview()
         }else{
             autoreleasepool {
              
             setupTableView()
             safeArea = view.layoutMarginsGuide
             var lissPrioritySenf = [SearchItem]()
             filteredDataSenf = []
 //                let thread =  DispatchQueue.global(qos: .background)
 //                                              thread.sync {
                 
             self.filteredDataSenf  = searchText.isEmpty ? self.listSenf : self.listSenf.filter { (item: SearchItem) -> Bool in
                 
                 let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
                 
                 if range.contains(" ") {
                     
                     let input =  searchText.components(separatedBy: " ")

                     var counter = 0
                  
                         for text in input {
                             if self.confirmSearchKey(userINp:text,searchKey:item.iteme_search,nameMel:item.iteme_name) {
                                 counter+=1
                             }
                         }
                     
                     if counter == input.count {
                         return true
                     }else{
                         for index in input{
                             if self.confirmSearchKey(userINp:index,searchKey:item.iteme_search,nameMel:item.iteme_name){
                                 return true

                             }else{
 //                                lissPrioritySenf.append(item)
                                 
                             }
                         }
                     }
                 }else{
                     if self.confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.iteme_search,nameMel:item.iteme_name) {
                         return true

                     }
                 }
                 
                 
                 //
                 
                 
                 
                 return false
                 
                 
             }
 //            let thread =  DispatchQueue.global(qos: .background)
 //            thread.async {
 //                if lissPrioritySenf.count > 0 {
 //                    print("HERE")
 //                    self.filteredDataSenf.append(contentsOf: lissPrioritySenf)
 //                    self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
 //                    lissPrioritySenf.removeAll()
 //                }
 //            }
             
         
                 
                 var sortedResultSenf = [SearchItem]()
                 var lissPrioritys = [SearchItem]()

                 for resultsSenf in self.filteredDataSenf {
                     let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()

                     if range.contains(" ") {
 //                        let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                         let constraintKeys =  searchText.components(separatedBy: " ")

                         var counter = 0
 //                        let thread =  DispatchQueue.global(qos: .background)
 //                        thread.sync {
                             for text in constraintKeys {
                                 if self.confirmSearchKey(userINp:text,searchKey:resultsSenf.iteme_search,nameMel:resultsSenf.iteme_name) {
                                     counter+=1
                                 }

                             }
 //                        }
                         if counter == constraintKeys.count {
                             sortedResultSenf.append(resultsSenf)
                         }else{

                             for index in constraintKeys{
                                 if self.matchesKey(constraint: index, searchKey: resultsSenf.iteme_search) == true {
                                     sortedResultSenf.append(resultsSenf)

                                 }else {
                                     lissPrioritys.append(resultsSenf)
                                 }
                             }
                         }
                     }else if self.matchesKey(constraint: searchText, searchKey: resultsSenf.iteme_search) == true {
                         sortedResultSenf.append(resultsSenf)


                     }else{
                         lissPrioritys.append(resultsSenf)
                     }


                 }
                 if lissPrioritys.count > 0 {
                     sortedResultSenf.append(contentsOf: lissPrioritys)
                 }
                 filteredDataSenf = []
                 self.filteredDataSenf = sortedResultSenf
                 self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
 //
 //
             }
 //
             
 //            UIView.animate(withDuration: 5) {
 //                DispatchQueue.main.async {
                     self.tableView.reloadData()
                     self.tableView.isHidden = false
 //                }
                 
 //            }
     }
 //        }
 }
     

     
     func matchesKey(constraint:String,searchKey:String)->Bool{
 //        if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
 //            return true
 //        }

         return false
     }
     
     
     
     func setupTableView() {
         view.addSubview(tableView)
         tableView.translatesAutoresizingMaskIntoConstraints = false
         tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
         tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
         tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     
     override func loadView() {
         super.loadView()
         
         searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
         searchBar.searchTextField.textAlignment = .right
         self.tableView.isHidden = true
         
     }
     
     
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         searchBar.resignFirstResponder()
         
     }
     
     
     
 }




 extension AddEditCategoryController:ShowNewWajbeh{
     func goToViewWajbeh(idWajbeh: String, flag: String) {
         
         if flag == "wajbeh"{
             self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
         }else {
             self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
         }
         //        txtSearchOutlet.text = nil
         
     }
     
     
     
 }


 extension AddEditCategoryController: UITableViewDelegate, UITableViewDataSource {
     
     
     //////////////////////////////////////////////////////////////////////////////
     // Table View related methods
     //////////////////////////////////////////////////////////////////////////////
     
     
     // MARK: TableView creation and updating
     
     // Create SearchTableview
     
     
     
     // MARK: TableViewDataSource methods
     public func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
     
     public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return filteredDataSenf.count
     }
     
     // MARK: TableViewDelegate methods
     
     //Adding rows in the tableview with the data from dataList
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
         
         
         cell.nameSanf.text = filteredDataSenf[indexPath.row].getStringText()
         cell.caloris.text =  filteredDataSenf[indexPath.row].getStringTextcaloris()
         cell.catrgory.text =  filteredDataSenf[indexPath.row].getStringTextCate()
         
         cell.imageSenf.image = UIImage(named : "logo")
         SetImage(laImage:cell.imageSenf)
         
         return cell
     }
     
     func SetImage(laImage:UIImageView){
         laImage.layer.borderWidth = 1
         laImage.layer.masksToBounds = false
         laImage.layer.borderColor = colorGray.cgColor
         laImage.layer.cornerRadius = laImage.frame.height/2
         laImage.clipsToBounds = true
         
     }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("selected row \(filteredDataSenf[indexPath.row].item_id)")
         obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filteredDataSenf[indexPath.row].item_id,flag:filteredDataSenf[indexPath.row].item_flag)
         tableView.isHidden = true
         
     }
     
     
     
 }



 extension AddEditCategoryController {
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "FavuriteRecentController" {
               if let dis=segue.destination as?  FavuriteRecentController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }else   if segue.identifier == "DisplayCateItemsController" {
               if let dis=segue.destination as?  DisplayCateItemsController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                               
                           }
                           else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "DisplayCateItemsSenfController" {
               if let dis=segue.destination as?  DisplayCateItemsSenfController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                           }else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "RecentUsedItemController" {
               if let dis=segue.destination as?  RecentUsedItemController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }
           
       }
       
 }


 /*
  //
  //  TestSearch.swift
  //  Ta5sees
  //
  //  Created by Admin on 9/10/20.
  //  Copyright © 2020 Telecom enterprise. All rights reserved.
  //
  
  import UIKit
  
  class TestSearch: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return resultsList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
  cell.textLabel?.text = resultsList[indexPath.row].getStringText()
  return cell
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  var filteredData: [tblPackeg]!
  var resultsList : [SearchItem] = [SearchItem]()
  var resultsListLiss : [SearchItem] = [SearchItem]()
  
  let list =  tblPackeg.searchModelArray()
  
  override func viewDidLoad() {
  super.viewDidLoad()
  tableView.dataSource = self
  searchBar.delegate = self
  filteredData = list
  print("RRRRRRR\("DFSDASDBMNMF ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  }
  
  func getUserInWithThe( userIn:String)->String {
  var polyUserIn = userIn;
  if userIn.count >= 3{
  if (userIn.hasPrefix("ال")) {
  polyUserIn = String(userIn.dropFirst(2))
  }else{
  polyUserIn = "ال\(userIn)";
  }
  }
  return polyUserIn;
  }
  func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
  let components = searchKey.components(separatedBy: "،")
  for item in components {
  if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
  return true
  }
  }
  return false
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
  var lissPriority = [tblPackeg]()
  resultsList.removeAll()
  filteredData = searchText.isEmpty ? list : list.filter { (item: tblPackeg) -> Bool in
  
  ////            let whitespace = NSCharacterSet.whitespaces
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  //.self.rangeOfCharacter(from: whitespace)
  
  if range.contains(" ") {
  let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  for text in input {
  if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
  counter+=1
  }
  
  }
  if counter == input.count {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  for index in input.reversed(){
  if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  lissPriority.append(item)
  resultsListLiss.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  
  }
  }
  }
  }else{
  if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }
  }
  
  
  //
  
  
  
  return false
  
  
  }
  
  
  if lissPriority.count > 0 {
  filteredData.append(contentsOf: lissPriority)
  filteredData = filteredData.removeDuplicates()
  lissPriority.removeAll()
  }
  
  if resultsListLiss.count > 0 {
  resultsList.append(contentsOf: resultsListLiss)
  resultsList = resultsList.removeDuplicates()
  resultsListLiss.removeAll()
  }
  //        var sortedResult = [tblPackeg]();
  var sortedResult = [SearchItem]();
  if sortedResult.count > 0 {
  sortedResult.removeAll()
  }
  
  for results in resultsList {
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  
  if range.contains(" ") {
  let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  
  for text in constraintKeys {
  if confirmSearchKey(userINp:text,searchKey:results.iteme_search,nameMel:results.iteme_name) {
  counter+=1
  }
  
  }
  if counter == constraintKeys.count {
  sortedResult.append(results)
  //                    sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else{
  for index in constraintKeys.reversed(){
  if matchesKey(constraint: index, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //                              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else {
  //                            lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  }
  }
  }else if matchesKey(constraint: searchText, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  
  }else{
  //                lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  
  
  }
  if resultsListLiss.count > 0 {
  sortedResult.append(contentsOf: resultsListLiss)
  }
  //        if lissPriority.count > 0 {
  //            sortedResult.append(contentsOf: lissPriority)
  //        }
  resultsList.removeAll()
  resultsList = sortedResult
  
  tableView.reloadData()
  }
  
  
  
  func matchesKey(constraint:String,searchKey:String)->Bool{
  if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
  return true
  }
  
  return false
  }
  
  
  
  func binarySearch( arr:[String],firstIndex:Int, lastIndex:Int, key:String)->Int
  {
  if (lastIndex >= firstIndex) {
  let mid = firstIndex + (lastIndex - firstIndex) / 2;
  
  //arr[mid].compare(key, options: NSString.CompareOptions.caseInsensitive).rawValue)
  if arr[mid] == key {
  return mid;
  
  }
  
  if arr[mid] > key {
  return binarySearch(arr: arr, firstIndex: firstIndex, lastIndex: mid - 1, key: key)
  }
  
  return binarySearch(arr: arr, firstIndex: mid + 1, lastIndex: lastIndex, key: key)
  }
  
  
  return -1;
  }
  
  
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  self.searchBar.showsCancelButton = true
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
  searchBar.showsCancelButton = false
  searchBar.text = ""
  searchBar.resignFirstResponder()
  }
  }
  
  struct SenfPackage {
  var tilie = ""
  var name = ""
  var id = ""
  var isWajbeh = ""
  var searchKey = ""
  //    init(tilie:String,) {
  //        statements
  //    }
  }
  
  
  //   let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                print(input)
  //                for text in input {
  //                    if text != "" {
  //                        return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                    }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                          print(input)
  //                for text in input {
  //                    if text != "" {
  //                   return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  
  /*
  filteredData.removeAll()
  for item in list {
  let components = item.searchKey!.components(separatedBy: "،")
  //            print(components.joined(separator: " "))
  
  let whitespace = NSCharacterSet.whitespaces
  let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  let input =  searchText.lowercased().components(separatedBy: " ")
  if range != nil {
  
  for text in input {
  if   components.joined(separator: "").lowercased().contains(text.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: text, isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }else{
  if   components.joined(separator: " ").lowercased().contains(searchText.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  //            return false
  //
  //
  //        }
  
  }
  
  */
  
  
  
  
  */
 //
 //  AddEditViewController.swift
 //  Ta5sees
 //
 //  Created by Admin on 5/29/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //


 protocol ShowNewWajbeh {
     func goToViewWajbeh(idWajbeh:String,flag:String)
 }

 import UIKit

 class AddEditCategoryController: UIViewController ,UISearchBarDelegate {
     
     
     var refWInfo:Int!
     var refWCAtegory:Int!
     var arrPassData:[Int] = []
     var obShowNewWajbeh:ShowNewWajbeh?
     var id_items:String!
     var date:String!
     var pg:protocolWajbehInfoTracker?
     var filteredData: [tblPackeg]!
     var filteredDataSenf =  [SearchItem]()
     var filteredDataSenf1 =  [SearchItem]()

     let list =  tblPackeg.searchModelArray()
     let listSenf1 =  tblSenf.searchModelArray()
     
     var listSenf =  [SearchItem]()
     var lissPrioritySenf = [SearchItem]()
     var filterItem = [SearchItem]()
     let tableView = UITableView()
     var safeArea: UILayoutGuide!
     
     @IBOutlet weak var titlePage: UILabel!
     @IBAction func buDismiss(_ sender: Any) {
         dismiss(animated: true, completion: nil)
     }
     
     @IBAction func favourites(_ sender: Any) {
         self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")
         
     }
     @IBAction func addANDEdite(_ sender: Any) {
         self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
         
     }
     @IBAction func recentlyUsed(_ sender: Any) {
         //
         self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
     }
     
     @IBOutlet weak var  searchBar: UISearchBar!
     
     
     @IBOutlet weak var lblTestSearch: UISearchBar!
     
     //    @IBAction func btndelet(_ sender: Any) {
     //        let ob = realm.objects(tblWajbehDetailing.self).filter("id == %@",1)
     //        try! realm.write {
     //            realm.delete(ob)
     //        }
     //    }
     //    @IBAction func edite(_ sender: Any) {
     //        self.performSegue(withIdentifier: "mainmenus", sender: [1,3])
     //
     //    }
     //    @IBOutlet weak var search: UISearchBar!
     override func viewDidLoad() {
         super.viewDidLoad()
         //        txtSearchOutlet.obShowNewWajbeh = self
         obShowNewWajbeh = self
         tableView.dataSource = self
         tableView.delegate = self
         
         searchBar.delegate = self
         setTilePage(id:id_items)
         let thread =  DispatchQueue.global(qos: .background)
         thread.sync {
             self.listSenf.append(contentsOf: self.list)
         }
         let thread1 =  DispatchQueue.global(qos: .background)
         thread1.sync {
             self.listSenf.append(contentsOf:self.listSenf1)
         }
         //        setTilePage(id:"1")
         
     }
     
     func textFieldDidChange(textField: UITextField) {
         //your code
     }
     
     
     
   
     
     func setTilePage(id:String){
         
         switch id {
         case "1":
             titlePage.text! = "إضافة فطور"
             break
         case "2":
             titlePage.text! = "إضافة غداء"
             break
         case "3":
             titlePage.text! = "إضافة عشاء"
             break
         case "4":
             titlePage.text! = "إضافة تصبيرة ١"
             break
         case "5":
             titlePage.text! = "إضافة تصبيرة ٢"
             break
         default:
             print("not found !!")
         }
     }
     
     
     func getUserInWithThe( userIn:String)->String {
         var polyUserIn = userIn;
         if userIn.count >= 3{
             let new = Array(userIn) //userIn.split(separator: ",")
 //            print("new \(new)")
             if "\(new[0])\(new[1])" == "ال" {
                 polyUserIn = String(userIn.dropFirst(2))
             }else{
                 polyUserIn = "ال\(userIn)";
             }
             
 //            if (userIn.hasPrefix("ال")) {
 //                polyUserIn = String(userIn.dropFirst(2))
 //            }else{
 //                polyUserIn = "ال\(userIn)";
 //            }
         }
             
         
         return polyUserIn;
     }
     func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
         let components = searchKey.components(separatedBy: "،")
         for item in components {
 //            if item.elementsEqual(userINp) ||  checkString(item:item,item2:userINp) ||  checkString(item:item,item2:getUserInWithThe(userIn: userINp))  {
 //                return true
 //            }
 //            if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
 //            return true
 //            }
             if item.contains(userINp)  {
                 return true
             }
         }
         return false
     }
     
     func checkString(item:String,item2:String)->Bool{
         let arr = Array(item)
         var str = ""
         for cha in arr {
             if cha != "," || cha != "،" {
                 str.append(cha)
                 if str == item2 {
                     return true
                 }
             }
         }
         return false
     }

     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
     }
     @objc func saveItemHistory(){
         if filteredDataSenf.isEmpty {
             tblHistoryNotFoundMeals.setMeals(name:lblTestSearch.text!)
         }
         
     }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         self.perform(#selector(saveItemHistory),with:nil,afterDelay: 2)
         if searchText.isEmpty == true {
             tableView.isHidden = true
             tableView.removeFromSuperview()
         }else{
             autoreleasepool {

             setupTableView()
             safeArea = view.layoutMarginsGuide
             var lissPrioritySenf = [SearchItem]()
             filteredDataSenf = []
             self.filteredDataSenf  = searchText.isEmpty ? self.listSenf : self.listSenf.filter { (item: SearchItem) -> Bool in
                 
                 let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
                 
                 if range.contains(" ") {
                     
                     let input =  searchText.components(separatedBy: " ")

                     var counter = 0
                     let thread =  DispatchQueue.global(qos: .background)
                     thread.sync {
                         for text in input {
                             if self.confirmSearchKey(userINp:text,searchKey:item.iteme_search,nameMel:item.iteme_name) {
                                 counter+=1
                             }
                         }
                     }
                     if counter == input.count {
                         return true
                     }else{
                         for index in input{
                             if self.confirmSearchKey(userINp:index,searchKey:item.iteme_search,nameMel:item.iteme_name){
                                 return true

                             }else{
 //                                lissPrioritySenf.append(item)
                                 
                             }
                         }
                     }
                 }else{
                     if self.confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.iteme_search,nameMel:item.iteme_name) {
                         return true

                     }
                 }
                 
                 
                 //
                 
                 
                 
                 return false
                 
                 
             }
 //            let thread =  DispatchQueue.global(qos: .background)
 //            thread.async {
 //                if lissPrioritySenf.count > 0 {
 //                    print("HERE")
 //                    self.filteredDataSenf.append(contentsOf: lissPrioritySenf)
 //                    self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
 //                    lissPrioritySenf.removeAll()
 //                }
 //            }
             
         
                 
                 var sortedResultSenf = [SearchItem]()
                 var lissPrioritys = [SearchItem]()

                 for resultsSenf in self.filteredDataSenf {
                     let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()

                     if range.contains(" ") {
 //                        let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                         let constraintKeys =  searchText.components(separatedBy: " ")

                         var counter = 0
                         let thread =  DispatchQueue.global(qos: .background)
                         thread.sync {
                             for text in constraintKeys {
                                 if self.confirmSearchKey(userINp:text,searchKey:resultsSenf.iteme_search,nameMel:resultsSenf.iteme_name) {
                                     counter+=1
                                 }

                             }
                         }
                         if counter == constraintKeys.count {
                             sortedResultSenf.append(resultsSenf)
                         }else{

                             for index in constraintKeys{
                                 if self.matchesKey(constraint: index, searchKey: resultsSenf.iteme_search) == true {
                                     sortedResultSenf.append(resultsSenf)

                                 }else {
                                     lissPrioritys.append(resultsSenf)
                                 }
                             }
                         }
                     }else if self.matchesKey(constraint: searchText, searchKey: resultsSenf.iteme_search) == true {
                         sortedResultSenf.append(resultsSenf)


                     }else{
                         lissPrioritys.append(resultsSenf)
                     }


                 }
                 if lissPrioritys.count > 0 {
                     sortedResultSenf.append(contentsOf: lissPrioritys)
                 }
                 filteredDataSenf = []
                 self.filteredDataSenf = sortedResultSenf
                 self.filteredDataSenf = self.filteredDataSenf.removeDuplicates()
 //
 //
             }
 //
             
             UIView.animate(withDuration: 5) {
                 DispatchQueue.main.async {
                     self.tableView.reloadData()
                     self.tableView.isHidden = false
                 }
                 
             }
     }
 }
     
     func binarySearch<T:Comparable>(_ inputArr:Array<T>, _ searchItem: T) -> Int? {
         var lowerIndex = 0
         var upperIndex = inputArr.count - 1

         while (true) {
             let currentIndex = (lowerIndex + upperIndex)/2
             if("\(inputArr[currentIndex])".elementsEqual("\(searchItem)")) {
                 return currentIndex
             } else if (lowerIndex > upperIndex) {
                 return nil
             } else {
                 if(!"\(inputArr[currentIndex])".elementsEqual("\(searchItem)")) {
                     upperIndex = currentIndex - 1
                 } else {
                     lowerIndex = currentIndex + 1
                 }
             }
         }
     }
     
     func matchesKey(constraint:String,searchKey:String)->Bool{
 //        if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
 //            return true
 //        }

         return false
     }
     
     
     
     func setupTableView() {
         view.addSubview(tableView)
         tableView.translatesAutoresizingMaskIntoConstraints = false
         tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
         tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
         tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     
     override func loadView() {
         super.loadView()
         
         searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
         searchBar.searchTextField.textAlignment = .right
         self.tableView.isHidden = true
         
     }
     
     
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         searchBar.resignFirstResponder()
         
     }
     
     
     
 }




 extension AddEditCategoryController:ShowNewWajbeh{
     func goToViewWajbeh(idWajbeh: String, flag: String) {
         
         if flag == "wajbeh"{
             self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
         }else {
             self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
         }
         //        txtSearchOutlet.text = nil
         
     }
     
     
     
 }


 extension AddEditCategoryController: UITableViewDelegate, UITableViewDataSource {
     
     
     //////////////////////////////////////////////////////////////////////////////
     // Table View related methods
     //////////////////////////////////////////////////////////////////////////////
     
     
     // MARK: TableView creation and updating
     
     // Create SearchTableview
     
     
     
     // MARK: TableViewDataSource methods
     public func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
     
     public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return filteredDataSenf.count
     }
     
     // MARK: TableViewDelegate methods
     
     //Adding rows in the tableview with the data from dataList
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
         
         
         cell.nameSanf.text = filteredDataSenf[indexPath.row].getStringText()
         cell.caloris.text =  filteredDataSenf[indexPath.row].getStringTextcaloris()
         cell.catrgory.text =  filteredDataSenf[indexPath.row].getStringTextCate()
         
         cell.imageSenf.image = UIImage(named : "logo")
         SetImage(laImage:cell.imageSenf)
         
         return cell
     }
     
     func SetImage(laImage:UIImageView){
         laImage.layer.borderWidth = 1
         laImage.layer.masksToBounds = false
         laImage.layer.borderColor = colorGray.cgColor
         laImage.layer.cornerRadius = laImage.frame.height/2
         laImage.clipsToBounds = true
         
     }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("selected row \(filteredDataSenf[indexPath.row].item_id)")
         obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filteredDataSenf[indexPath.row].item_id,flag:filteredDataSenf[indexPath.row].item_flag)
         tableView.isHidden = true
         
     }
     
     
     
 }



 extension AddEditCategoryController {
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "FavuriteRecentController" {
               if let dis=segue.destination as?  FavuriteRecentController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }else   if segue.identifier == "DisplayCateItemsController" {
               if let dis=segue.destination as?  DisplayCateItemsController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                               
                           }
                           else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "DisplayCateItemsSenfController" {
               if let dis=segue.destination as?  DisplayCateItemsSenfController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                           }else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "RecentUsedItemController" {
               if let dis=segue.destination as?  RecentUsedItemController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }
           
       }
       
 }


 /*
  //
  //  TestSearch.swift
  //  Ta5sees
  //
  //  Created by Admin on 9/10/20.
  //  Copyright © 2020 Telecom enterprise. All rights reserved.
  //
  
  import UIKit
  
  class TestSearch: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return resultsList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
  cell.textLabel?.text = resultsList[indexPath.row].getStringText()
  return cell
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  var filteredData: [tblPackeg]!
  var resultsList : [SearchItem] = [SearchItem]()
  var resultsListLiss : [SearchItem] = [SearchItem]()
  
  let list =  tblPackeg.searchModelArray()
  
  override func viewDidLoad() {
  super.viewDidLoad()
  tableView.dataSource = self
  searchBar.delegate = self
  filteredData = list
  print("RRRRRRR\("DFSDASDBMNMF ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  }
  
  func getUserInWithThe( userIn:String)->String {
  var polyUserIn = userIn;
  if userIn.count >= 3{
  if (userIn.hasPrefix("ال")) {
  polyUserIn = String(userIn.dropFirst(2))
  }else{
  polyUserIn = "ال\(userIn)";
  }
  }
  return polyUserIn;
  }
  func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
  let components = searchKey.components(separatedBy: "،")
  for item in components {
  if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
  return true
  }
  }
  return false
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
  var lissPriority = [tblPackeg]()
  resultsList.removeAll()
  filteredData = searchText.isEmpty ? list : list.filter { (item: tblPackeg) -> Bool in
  
  ////            let whitespace = NSCharacterSet.whitespaces
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  //.self.rangeOfCharacter(from: whitespace)
  
  if range.contains(" ") {
  let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  for text in input {
  if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
  counter+=1
  }
  
  }
  if counter == input.count {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  for index in input.reversed(){
  if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  lissPriority.append(item)
  resultsListLiss.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  
  }
  }
  }
  }else{
  if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }
  }
  
  
  //
  
  
  
  return false
  
  
  }
  
  
  if lissPriority.count > 0 {
  filteredData.append(contentsOf: lissPriority)
  filteredData = filteredData.removeDuplicates()
  lissPriority.removeAll()
  }
  
  if resultsListLiss.count > 0 {
  resultsList.append(contentsOf: resultsListLiss)
  resultsList = resultsList.removeDuplicates()
  resultsListLiss.removeAll()
  }
  //        var sortedResult = [tblPackeg]();
  var sortedResult = [SearchItem]();
  if sortedResult.count > 0 {
  sortedResult.removeAll()
  }
  
  for results in resultsList {
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  
  if range.contains(" ") {
  let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  
  for text in constraintKeys {
  if confirmSearchKey(userINp:text,searchKey:results.iteme_search,nameMel:results.iteme_name) {
  counter+=1
  }
  
  }
  if counter == constraintKeys.count {
  sortedResult.append(results)
  //                    sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else{
  for index in constraintKeys.reversed(){
  if matchesKey(constraint: index, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //                              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else {
  //                            lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  }
  }
  }else if matchesKey(constraint: searchText, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  
  }else{
  //                lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  
  
  }
  if resultsListLiss.count > 0 {
  sortedResult.append(contentsOf: resultsListLiss)
  }
  //        if lissPriority.count > 0 {
  //            sortedResult.append(contentsOf: lissPriority)
  //        }
  resultsList.removeAll()
  resultsList = sortedResult
  
  tableView.reloadData()
  }
  
  
  
  func matchesKey(constraint:String,searchKey:String)->Bool{
  if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
  return true
  }
  
  return false
  }
  
  
  
  func binarySearch( arr:[String],firstIndex:Int, lastIndex:Int, key:String)->Int
  {
  if (lastIndex >= firstIndex) {
  let mid = firstIndex + (lastIndex - firstIndex) / 2;
  
  //arr[mid].compare(key, options: NSString.CompareOptions.caseInsensitive).rawValue)
  if arr[mid] == key {
  return mid;
  
  }
  
  if arr[mid] > key {
  return binarySearch(arr: arr, firstIndex: firstIndex, lastIndex: mid - 1, key: key)
  }
  
  return binarySearch(arr: arr, firstIndex: mid + 1, lastIndex: lastIndex, key: key)
  }
  
  
  return -1;
  }
  
  
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  self.searchBar.showsCancelButton = true
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
  searchBar.showsCancelButton = false
  searchBar.text = ""
  searchBar.resignFirstResponder()
  }
  }
  
  struct SenfPackage {
  var tilie = ""
  var name = ""
  var id = ""
  var isWajbeh = ""
  var searchKey = ""
  //    init(tilie:String,) {
  //        statements
  //    }
  }
  
  
  //   let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                print(input)
  //                for text in input {
  //                    if text != "" {
  //                        return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                    }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                          print(input)
  //                for text in input {
  //                    if text != "" {
  //                   return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  
  /*
  filteredData.removeAll()
  for item in list {
  let components = item.searchKey!.components(separatedBy: "،")
  //            print(components.joined(separator: " "))
  
  let whitespace = NSCharacterSet.whitespaces
  let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  let input =  searchText.lowercased().components(separatedBy: " ")
  if range != nil {
  
  for text in input {
  if   components.joined(separator: "").lowercased().contains(text.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: text, isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }else{
  if   components.joined(separator: " ").lowercased().contains(searchText.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  //            return false
  //
  //
  //        }
  
  }
  
  */
  
  
  
  
  */

 */
