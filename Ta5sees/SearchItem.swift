//
//  SearchItem.swift
//  Ta5sees
//
//  Created by Admin on 7/14/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation

class SearchItem : Hashable{
    static func == (lhs: SearchItem, rhs: SearchItem) -> Bool {
          return lhs.iteme_name == rhs.iteme_name && lhs.iteme_search == rhs.iteme_search
      }

      func hash(into hasher: inout Hasher) {
          hasher.combine(iteme_name)
          hasher.combine(iteme_search)
      }
    var attributedItemName: NSMutableAttributedString?
    var attributedItemID: NSMutableAttributedString?
    var allAttributedName : NSMutableAttributedString?
    
    var iteme_name: String
    var iteme_search: String
    var item_id: String
    var caolris:String
    var item_flag: String
    
    public init(iteme_name: String, item_id: String,item_flag:String,caolris:String,iteme_search:String) {
        self.iteme_name = iteme_name
        self.item_id = item_id
        self.item_flag = item_flag
        self.caolris = caolris
        self.iteme_search = iteme_search

    }
    
    public func getFormatedText() -> NSMutableAttributedString{
        allAttributedName = NSMutableAttributedString()
        allAttributedName!.append(attributedItemName!)
        allAttributedName!.append(NSMutableAttributedString(string: ""))
//        allAttributedName!.append(attributedCountryName!)
        
        return allAttributedName!
    }
    
    public func getStringText() -> String{
        return "\(iteme_name)"
    }
    
    
    public func getStringTextcaloris() -> String{
        return "سعرات حرارية \(Int(round(Double(caolris)!)))"
    }
    
    
    public func getStringTextCate() -> String{
        if item_flag == "wajbeh" {
            return "وجبة"
        }else{
            return "صنف"
        }
          
      }
}
