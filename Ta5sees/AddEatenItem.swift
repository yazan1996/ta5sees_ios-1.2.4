//
//  AddEatenItem.swift
//  Ta5sees
//
//  Created by Admin on 2/3/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation


class AddEatenItem{
    
    var view:viewAddEatenItem?
    var pd:protocolWajbehInfoTracker?
    init(with view:viewAddEatenItem ,pd:protocolWajbehInfoTracker) {
        self.view = view
         self.pd = pd
    }
    
    public func addItems(ob:tblUserWajbehEaten,date:String,calorisBurned:Int, completion: @escaping (Error?) -> Void) {
        DispatchQueue.main.async {
            ob.addItem(ob: ob, completion: { (Bool,error) in
                if Bool != true {
                    completion(Bool as? Error)
                    self.view?.finshPage()
                }
            })
        }
        DispatchQueue.main.async {
            tblDailyWajbeh.updateDataDaily(date:date, calorisBurned: calorisBurned, flag:"eaten")
            { (res, err) in
                if res as? String == "true"{
                    completion(Bool.self as? Error)
                    print("Save Manual Wajbhe : \(res!)")
                }else{
                    print("Save Manual Wajbhe : \(res!)")
                    completion(Bool.self as? Error)
                }
            }
        }
        
        DispatchQueue.main.async {
            self.pd?.refrechData()
                switch ob.wajbehInfiItem {
                case "1":
                    self.pd?.refreshListItemBreakfast(itemTasbera2: ob)
                    break
                case "2":
                    self.pd?.refreshListItemLaunsh(itemTasbera2: ob)
                    break
                case "3":
                    self.pd?.refreshListItemDinner(itemTasbera2: ob)
                    break
                case "4":
                    self.pd?.refreshListItemTasbera1(itemTasbera2: ob)
                    break
                default :
                    self.pd?.refreshListItemTasbera2(itemTasbera2: ob)
                    break
                    
                }
            
            completion(Bool.self as? Error)
            self.view?.finshPage()
        }
    }
    
    
    public func refreshAfterRemoveMealPlanner(idCate:String,completion: @escaping (Error?) -> Void) {
  
    
        
        DispatchQueue.main.async {
            self.pd?.refrechData()
            switch idCate {
            case "1":
                self.pd?.refreshListItemBreakfast(itemTasbera2: tblUserWajbehEaten())
                break
            case "2":
                self.pd?.refreshListItemLaunsh(itemTasbera2: tblUserWajbehEaten())
                break
            case "3":
                self.pd?.refreshListItemDinner(itemTasbera2: tblUserWajbehEaten())
                break
            case "4":
                self.pd?.refreshListItemTasbera1(itemTasbera2: tblUserWajbehEaten())
                break
            default :
                self.pd?.refreshListItemTasbera2(itemTasbera2: tblUserWajbehEaten())
                break

            }
            completion(Bool.self as? Error)
            self.view?.finshPage()
        }
    }
    
    
    
    
}


protocol viewAddEatenItem {
    
    func finshPage()
}
protocol viewSetUnit {
    
    func setUnit(title:Int)
    func setUnit(title:String)

}




protocol viewSetCateInfo {
    
    func setIDCateInfo(title:String)
}
