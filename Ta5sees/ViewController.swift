//
//  ViewController.swift
//  NavigationDrawer
//
//  Created by Sowrirajan Sugumaran on 05/10/17.
//  Copyright © 2017 Sowrirajan Sugumaran. All rights reserved.
//Bridging.h

import UIKit
import RealmSwift
import Realm
import CRNotifications
import SwiftyGif
import FLAnimatedImage
import AVFoundation
//import SimpleAlert
import FirebaseFirestore
import Firebase
import FirebaseDatabase
import SwiftyJSON
import Security
import SVProgressHUD
import FirebaseCrashlytics
import RxSwift
import FirebaseMessaging
import FirebaseAuth
import SwiftyStoreKit
import FirebaseAnalytics

class ViewController: UIViewController,UIScrollViewDelegate
                      ,protocolGeneral,UIStepperControllerDelegate,SwiftyGifDelegate,goToViewWajbeh,AVAudioPlayerDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UIViewControllerTransitioningDelegate,UIPopoverPresentationControllerDelegate, ApiResponseDaelegat{
    func getResponseSuccuss() {}
    
    func getResponseFalueir(meeage: String) {}
    
    func getResponseMissigData() {}
    
    func showIndecator() {}
    
    func HideIndicator() {}
    
    
    func toViewWajbeh(parsData:[String]) {
        tblUserWajbehForWeek.checkWajbehDate(date: lblDate.text!,wajbehInfo:parsData[1]) { (response, error) in
            if response as? String == "found" {
                self.performSegue(withIdentifier: "editWajbeh", sender: parsData)
            }else{
                self.showAlert(str: "لا يوجد لديك وجبات مقترحة في هذااليوم")
            }
        }
    }
    //
    func TrackerTamreen() {
        DiplayDailyData()
    }
    
    func TrackerWeight(date: String) {
        
        getInfoUser { [self] (user, err) in
            tblUserHistory.getUserWeight(date:lblDate.text!, weight: user.weight) { (response, error) in
                self.currentWeight = Double((response as? String)!)
                self.obDisplayUserWeight.setCurrentWeightValue(currentWeight:self.currentWeight)
            }
        }
    }
    
    func TrackerWater(date: String) {
        WaterSetting(Usedate: date) { (res, err) in
            print(res!)
            self.collectionviewoutl.reloadData()
            
        }
    }
    
    
    
    func refreshWater(flag:Bool) {
        WaterSetting(Usedate: lblDate.text!) { (res, err) in
            print(res!)
            self.collectionviewoutl.reloadData()
            
        }
    }
    
    func saveAmountWaterGoal(x: Int) {
    }
    
    func saveAmountWaterDrink(x: Int) {
        tblUserWaterDrinked.saveWaterDrink(x: x,date:lblDate.text!) { (resp, err) in
            print(resp!)
            self.waterConsu.text = "أستهلكت :\(Double(x)/4.0) لتر "
        }
    }
    
    func stepperDidAddValues(stepper: UIStepperController) {
        print(stepper.count)
    }
    
    func stepperDidSubtractValues(stepper: UIStepperController) {
        print(stepper.count)
    }
    
    func setdate(m: String, d: Int, y: Int) {
        currentMonth = m
        year = y
        dayInDate=d
        lblDate.text = "\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))"
        
        
        if dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 < dateFormatter.date(from:"\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")!.timeIntervalSince1970 {
            setTextDateAsDay(date:"\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")
            animationviewLeft(dataView:self.dataView, duration: 0.2)
        }else if dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 > dateFormatter.date(from:"\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")!.timeIntervalSince1970 {
            setTextDateAsDay(date:"\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")
            animationviewRight(dataView:self.dataView)
        }else{
            setTextDateAsDay(date:"\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")
        }
        //                    animationviewLeft(dataView:dataView)
        
        TrackerWater(date: lblDate.text!)
        TrackerWeight(date: lblDate.text!)
        TrackerTamreen()
        loadWajbehEaten()
        
    }
    
    var appBundleId = "-1"
    
    @IBOutlet weak var btnTasbera1: UIButton!
    @IBOutlet weak var nutritionalValues: UIStackView!
    @IBOutlet weak var viewTasberh2: CapsuleView!
    @IBOutlet weak var viewTamreen: CapsuleView!
    @IBOutlet weak var viewTasber1: CapsuleView!
    
    @IBOutlet weak var stackTasbera1: UIStackView!
    @IBOutlet weak var stackLunch: UIStackView!
    @IBOutlet weak var stackTasbera2: UIStackView!
    @IBOutlet weak var stackDinner: UIStackView!
    @IBOutlet weak var stackTamreen: UIStackView!
    @IBOutlet weak var viewDinner: CapsuleView!
    @IBOutlet weak var viewLaunch: CapsuleView!
    @IBOutlet weak var btnBreakOut: UIButton!
    @IBOutlet weak var btnnextDayOut: UIButton!
    
    @IBOutlet weak var viewUpdateWeight: CapsuleView!
    @IBOutlet weak var btnTmreenOut: UIButton!
    @IBOutlet weak var btnTasberaOut: UIButton!
    @IBOutlet weak var btnDinnerOut: UIButton!
    @IBOutlet weak var btnLaunchOut: UIButton!
    @IBOutlet weak var btnwaterOut: UIButton!
    @IBOutlet weak var btnbakDayOut: UIButton!
    /////////////////////////custom Date/////////////////////////
    @IBOutlet weak var KaclأBurend: UILabel!
    @IBOutlet weak var Kacleaten: UILabel!
    @IBOutlet weak var cho: UILabel!
    @IBOutlet weak var fate: UILabel!
    @IBOutlet weak var protein: UILabel!
    
    @IBOutlet weak var stackDataDaily: UIStackView!
    @IBOutlet weak var imgTamreen: UIImageView!
    
    @IBOutlet weak var tblBreakfast: SelfSizedTableView!
    var protocalViewWajbeh:goToViewWajbeh!
    var currentWeight:Double!
    var indexTrue:Int!
    var dateNotfy = ""
    //    var titleDialogNotfy = ""
    let dateFormatter = DateFormatter()
    let dateFormatter1 = DateFormatter()
    var pd:protocolGeneral?
    var obprotocolWajbehInfoTracker:protocolWajbehInfoTracker?
    var refprotocolGeneralTamreen:protocolGeneral?
    var obItemsViewList:ItemsViewList!
    var obPogressBar:progresBarHome!
    var obDisplayUserWeight:DisplayUserWeight!
    var ListEatenBreakfast = [tblUserWajbehEaten]()
    var ListEatenLaunsh = [tblUserWajbehEaten]()
    var ListEatenDinner = [tblUserWajbehEaten]()
    var ListEatenTasbera2 = [tblUserWajbehEaten]()
    var ListEatenTasbera1 = [tblUserWajbehEaten]()
    var ListTamreenBurned:[tblUserTamreen]!
    var db = Firestore.firestore()
    lazy var obPresenter = Presenter(with: self)
    var resultelist:Results<tblUserWajbehEaten>!
    let Months = ["01","02","03","04","05","06","07","08","09","10","11","12"]
    var hightBanner:CGFloat = 0
    var selectTap2 = false
    //    let Months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    //    let DaysOfMonth = ["Monday","Thuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    
    var DaysInMonths = [31,28,31,30,31,30,31,31,30,31,30,31]
    
    var currentMonth = String()
    
    var PositionIndex = 0
    let packeg = tblPackeg()
    let packageIngredients = tblPackageIngredients()
    
    var LeapYearCounter = 2
    var dayInDate = day
    var id_alarm = "0"
    var title_alarm = ""
    var countPackage = 0
    @IBOutlet weak var dataView: UIView!
    var dayCounter = 0
    @IBOutlet weak  var lblDate: UILabel!
    var actionButton : ActionButton!
    lazy var  obAPiSubscribeUser = APiSubscribeUser(with: self)
    
    @IBOutlet weak var tblTamreen: SelfSizedTableView!
    @IBOutlet weak var tblTasber2: SelfSizedTableView!
    @IBOutlet weak var tblDinner: SelfSizedTableView!
    @IBOutlet weak var tblLaunsh: SelfSizedTableView!
    @IBOutlet weak var tblTasber1: SelfSizedTableView!
    @IBOutlet weak var waterGoal: UILabel!
    
    
    @IBOutlet weak var imageTasber2: UIImageView!
    
    
    
    
    
    var location=[CGRect]()
    
    func disableController(){
        view.isUserInteractionEnabled = false
    }
    func enableController(){
        view.isUserInteractionEnabled = true
    }
    func startTutorial(){
        print("LIV!7",getDataFromSheardPreferanceString(key: "isTutorial"))
        if getDataFromSheardPreferanceString(key: "isTutorial") == "0" {
            print("LIV!8")
            view.superview?.isUserInteractionEnabled = false
            var bottomOffset1:CGPoint!
            bottomOffset1 = CGPoint(x: 0, y:viewMealPlanner.frame.origin.y+120)
            if bottomOffset1 != nil {
                scroll.setContentOffset(bottomOffset1, animated: true)
            }
            scroll.setContentOffset(bottomOffset1, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.90) { [self] in
                showTutorialMealPlanner()
            }
        }else{
            print("LIV!9")
        }
    }
    
    func startTutorialWater(){
        var bottomOffset1:CGPoint!
        bottomOffset1 = CGPoint(x: 0, y:viewBreakfast.frame.origin.y+120)
        if bottomOffset1 != nil {
            scroll.setContentOffset(bottomOffset1, animated: true)
        }
        scroll.setContentOffset(bottomOffset1, animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.90) { [self] in
            self.showTutorialWater()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //        Date().daysBetween(start: getDateOnlyasDate(), end: getDateOnlyasDate())
        //        print("tblUserProgressHistory12",getDateOnly(),setupRealm().objects(tblUserProgressHistory.self))
        
        if setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@",getUserInfo().id).isEmpty { //save first row for old  user
            
            _ = tblUserProgressHistory(refCuiseseID: getUserInfo().refCuiseseID, date: getDateOnly(), weeksNeeded: getUserInfo().weeksNeeded, gramsToLose: getUserInfo().gramsToLose, dateInMili: getDateOnlyasDate().timeIntervalSince1970*1000.0.rounded(), refAllergyInfoID: getUserInfo().refAllergyInfoID, refUserID: getUserInfo().id, target: getUserInfo().target, refPlanMasterID: getUserInfo().refPlanMasterID, refLayaqaCondID: getUserInfo().refLayaqaCondID, birthday: getUserInfo().birthday, weight: getUserInfo().weight,height: getUserInfo().height, refGenderID: getUserInfo().refGenderID)
        }
        
        
        print("UserInfo",getUserInfo())
        
        
        
        if getDataFromSheardPreferanceString(key: "isSyncData") == "yes" { // sync data each 7 days
            disableView(bool:false)
            setDataInSheardPreferance(value: "0", key: "loadWajbehSenf")
            SVProgressHUD.show(withStatus: "جاري تحديث البيانات")
            setDataInSheardPreferanceInt(value: 0, key: "counterRetryLoad")
            Downloader.loadData_firebase(flag:0) { (bool) in
                SVProgressHUD.dismiss { [self] in
                    DispatchQueue.main.async {
                        UIApplication.shared.isIdleTimerDisabled = false
                    }
                    setDataInSheardPreferance(value: "no", key: "isSyncData")
                    disableView(bool:true)
                    setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                    
                }
            }
        }
        
        if getDataFromSheardPreferanceString(key: "fix_appleID") != "1" {
            print("LIV!1")
            getInfoUser(responsEHendler: { [self]  (item, err) in
                let detailView:UIViewController?
                if getDataFromSheardPreferanceString(key: "userID").contains("."){
                    print("LIV!2")
                    downladDataAPI()
                    setDataInSheardPreferance(value: "1", key: "fix_appleID")
                    return
                }
                if item.loginType == "4" {
                    print("LIV!3")
                    if #available(iOS 13.0, *) {
                        detailView = self.storyboard!.instantiateViewController(withIdentifier: "firstPage") as! FirstPageController
                        UserDefaults.standard.set(0, forKey: "login")
                        detailView!.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        detailView!.transitioningDelegate = self
                        let center = UNUserNotificationCenter.current()//remove alarm
                        center.removeAllPendingNotificationRequests()
                        //
                        var id = getDataFromSheardPreferanceString(key: "userID")//remove topic
                        if (id.firstIndex(of: "+") != nil){
                            id = String(id.dropFirst())
                        }
                        Messaging.messaging().unsubscribe(fromTopic: id) { error in
                            
                            print("unSubscribe to \(id) topic")
                        }
                        setDataInSheardPreferance(value: "1", key: "fix_appleID")
                        self.present(detailView!, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                    
                }else{
                    print("LIV!4")
                    downladDataAPI()
                }
            })
            
        }else if getDataFromSheardPreferanceString(key: "isDownloadAPI") != "1" {
            print("LIV!5")
            downladDataAPI()
        }else{
            print("LIV!6")
            //startTutorial()
        }
        
        //        if setupRealm().objects(tblIngredientRulesAllergy.self).isEmpty || setupRealm().objects(tblIngredientRules.self).isEmpty {
        //            setDataInSheardPreferance(value: "yes", key: "isSyncData")
        //        } removed
        
        if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") == "1" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) { [self] in
                startShowTutorialTamreen()
            }
        }
        
        if getDataFromSheardPreferanceString(key: "isTutorial-nutration") == "1" {
            moveTopController(scroll:scroll)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) { [self] in
                showTutorialNutraionValus()
            }
        }
        
        if getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "1" && getDataFromSheardPreferanceString(key: "isTutorial-isBeign") == "1"  {
//            getInfoUser { [self] (item, err) in
//                print("calac \(calclateAge(str: item.birthday))")
//                if calclateAge(str: item.birthday) >= 14 && calclateAge(str: item.birthday) <= 16 && item.grown == "2" {
//                    print("success convert grown to 1")
//                    if item.refPlanMasterID == "1"  || item.refPlanMasterID == "2" {
//                        viewPopupTarget()
//                    }else{
//                        print("here***")
//                        setAlpha(flag: true)
//                    }
//                }else{
//
//                    print("will reload data")
//                    // if failure load data reload
//
                    
                    if setupRealm().objects(tblPackeg.self).isEmpty || setupRealm().objects(tblSenf.self).isEmpty || setupRealm().objects(tblPlateItem.self).isEmpty || setupRealm().objects(tblPackageIngredients.self).isEmpty ||  setupRealm().objects(tblIngredientRules.self).isEmpty || setupRealm().objects(tblPlatePerPackage.self).isEmpty || setupRealm().objects(tblPackagePlateUnits.self).isEmpty || setupRealm().objects(tblPackageInfoItems.self).isEmpty || setupRealm().objects(tblIngredientRulesAllergy.self).isEmpty || setupRealm().objects(tblUnwantedFruitsAndVegetablesSenfs.self).isEmpty || setupRealm().objects(tblWajbehAllergy.self).isEmpty || setupRealm().objects(tblWajbehCuisenes.self).isEmpty {
                        
                        print("start reload data")
                        disableView(bool:false)
                        setDataInSheardPreferance(value: "0", key: "loadWajbehSenf")
                        SVProgressHUD.show(withStatus: "جاري تحديث البيانات")
                        setDataInSheardPreferanceInt(value: 0, key: "counterRetryLoad")
                        setDataInSheardPreferance(value: "0", key: "isDownloadAPI")
                        Downloader.loadData_firebase(flag:0) { (bool) in
                            if bool {
                                SVProgressHUD.dismiss {
                                    DispatchQueue.main.async { [self] in
                                        UIApplication.shared.isIdleTimerDisabled = false
                                        disableView(bool:true)
                                        setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                                    }
                                }
                            }else{
                                print("HERE96")
                                SVProgressHUD.dismiss {
                                    DispatchQueue.main.async { [self] in
                                        disableView(bool:true)
                                    }
                                }
                            }
                            
                        }
                        
                    }
//                }
            }
//        }else{
//            NSLog("********************____")
//        }
        
        
        if getUserInfo().refCuiseseID != "-1" {
            if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 4 {
                btnMealPalnnerOut.titleLabel?.numberOfLines = 10
                btnMealPalnnerOut.titleLabel?.lineBreakMode = .byWordWrapping
                btnMealPalnnerOut.titleLabel?.textAlignment = .center
                let cus = tblCuisnesUSerItems.getDataSelected()
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                    customizeSubscribeButton()
                }else{
                    if cus.isEmpty || cus == "" {
                        btnMealPalnnerOut.setTitle("النظام الغذائي", for: .normal)
                    }else{
                        btnMealPalnnerOut.setTitle("النظام الغذائي\n (\(tblCuisnesUSerItems.getDataSelected()))", for: .normal)
                    }
                }
            }else if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 3 || getUserInfo().subscribeType == 2 {
                customizeSubscribeButton()
            }else{
                btnMealPalnnerOut.setTitle("النظام الغذائي", for: .normal)
            }
        }
        setupTapNotfy()
        
    }
    
    func setGradientBackgroundButton() {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(red: 159/255, green: 177/255, blue: 189/255, alpha: 1.0),UIColor(red: 130/255, green: 147/255, blue: 158/255, alpha: 1.0)].map {$0.cgColor}
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint (x: 1, y: 0.5)
        gradientLayer.frame = self.view.bounds
        btnMealPalnnerOut.layer.insertSublayer(gradientLayer, at: 0)
        btnMealPalnnerOut.fontColor = .white
    }
    
    func customizeSubscribeButton() {
        
        btnMealPalnnerOut.titleLabel?.numberOfLines = 30
        let title = NSMutableAttributedString()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let part1 = NSAttributedString(string:  "النظام الغذائي\n",
                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                    NSAttributedString.Key.font: UIFont(name: "GEDinarOne-Bold", size: 20)!,
                                                    NSAttributedString.Key.paragraphStyle: paragraphStyle])
        let part2 = NSAttributedString(string: "(غير مشترك)",
                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.red,
                                                    NSAttributedString.Key.font: UIFont(name: "GEDinarOne-Bold", size: 15)!,
                                                    NSAttributedString.Key.paragraphStyle: paragraphStyle])
        title.append(part1)
        title.append(part2)
        btnMealPalnnerOut.setAttributedTitle(title, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        print("VIEW1")
        //        print(getDateOnly())
        
        
        //        print("YAZAN ",setupRealm().objects(tblWajbatUserMealPlanner.self))
        
        //        KaclأBurend.fadeTransition(1)
        //        Kacleaten.fadeTransition(1)
        
        
        
        //        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getUserInfo().id,getDateOnly())
        //
        //        let ob1 = setupRealm().objects(tblUserProgressHistory.self)
        //                print("tblUserWaterDrinked",ob1)
        //                let ob = setupRealm().objects(tblUserInfo.self)
        //        //        print("tblUserProgressHistory",ob1.last,ob1.count)
        ////
        //                try! setupRealm().write {
        //                                setupRealm().delete(ob)
        //                    ob.setValue("1", forKey: "subscribeType")
        //                }
        
        
        //        if getUserInfo().subscribeType == -1 {
        //            btnMealPalnnerOut.backgroundColor = UIColor(hexString: "#FFDE2E")
        //            btnMealPalnnerOut.setTitleColor(.white, for: .normal)
        ////        self.tabBarController?.tabBar.items![1].image = UIImage(named: "cusineGold")?.withRenderingMode(.alwaysOriginal)
        //            //fridge
        //
        //        }else{
        //            btnMealPalnnerOut.backgroundColor = .white
        //        btnMealPalnnerOut.setTitleColor(colorBasicApp, for: .normal)
        ////            self.tabBarController?.tabBar.items![1].image = UIImage(named: "cusineGold")?.withRenderingMode(.alwaysOriginal)
        //
        
        //        }
        
        
        
        //        print(setupRealm().objects(tblWajbatUserMealPlanner.self))
        
        
        
        //        dispatchGroup.notify(queue: .main) {
        //            SVProgressHUD.dismiss()
        //            print("run 22...")
        //            if getDataFromSheardPreferanceString(key: "isTutorial-water") == "0" {
        //                DispatchQueue.main.async { [self] in
        //                    setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
        //                    disableView(bool:true)
        //                    startTutorial()
        //                }
        //            }
        //        }
        
        self.WaterSetting(Usedate: self.lblDate.text!) { (res, err) in
            print(" WaterSetting get data : \(res!)")
            DispatchQueue.main.async {
                self.collectionviewoutl.reloadData()
            }
        }
        loadWajbehEaten()
        
        
        getInfoUser { (user, err) in
            DispatchQueue.main.async {
                tblUserHistory.getUserWeight(date: self.lblDate.text!, weight: user.weight) { (response, error) in
                    self.currentWeight = Double((response as? String)!)
                    self.obDisplayUserWeight.setCurrentWeightValue(currentWeight:self.currentWeight)
                }
            }
        }
        
        DispatchQueue.main.async {
            self.DiplayDailyData()
        }
        
    }
    
    func disableView(bool:Bool){
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = bool
            self.tabBarController?.view.isUserInteractionEnabled = bool
        }
        
    }
    
    func downladDataAPI(){
        print("run 3", getDataFromSheardPreferanceString(key: "isDownloadAPI") == "0" , getDataFromSheardPreferanceInt(key: "aleadyExistUser") )
        if getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "1" && getDataFromSheardPreferanceString(key: "isDownloadAPI") == "1" &&
            getDataFromSheardPreferanceString(key: "isTutorial-isBeign") == "0"  {//finish load data befor appaer page home
            NSLog("startTutorial*******")
            print("run 4")
            
            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
            DispatchQueue.main.async { [self] in
                disableView(bool:true)
            }
            
            startTutorial()
        }
        //        else if getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "0" && getDataFromSheardPreferanceString(key: "isDownloadAPI") == "1" && getDataFromSheardPreferanceString(key: "finishRegistration") == "0"{
        //            print("run 5")
        //            NSLog("start Download data*******")
        //
        //            //            disableView(bool:false)
        //            SVProgressHUD.show(withStatus: "جاري تحميل بياناتك")
        //            //            Downloader.loadData_firebase(flag: 0) { [self] (bool) in
        //            //                print("loadData_firebase \(bool)")
        //            NSLog("startTutorial*******")
        //            setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
        //            DispatchQueue.main.async { [self] in
        //                UIApplication.shared.isIdleTimerDisabled = false
        //                disableView(bool:true)
        //            }
        //            SVProgressHUD.dismiss()
        //            setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
        //            startTutorial()
        //            //            }
        //        }
        
        else if getDataFromSheardPreferanceString(key: "isDownloadAPI") == "0"  { // download data when login user
            print("run 6")
            NSLog("else startTutorial 1*******")
            NSLog("start Download data*******")
            
            disableView(bool:false)
            SVProgressHUD.show(withStatus: "جاري تحميل بياناتك")
            setDataInSheardPreferanceInt(value: 0, key: "counterRetryLoad")
            Downloader.loadData_firebase(flag: 0) { [self] (bool) in
                print("loadData_firebase \(bool)")
                NSLog("startTutorial*******")
                if bool {
                    setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            UIApplication.shared.isIdleTimerDisabled = false
                            disableView(bool:true)
                        }
                        setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                        startTutorial()
                    })
                }else{
                    print("HERE97")
                    SVProgressHUD.dismiss(completion: {
                        DispatchQueue.main.async {
                            UIApplication.shared.isIdleTimerDisabled = false
                            disableView(bool:true)
                        }
                        //                        setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                        //                        startTutorial()
                    })
                }
                //                SVProgressHUD.dismiss()
                //                _ = getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded()
                //                if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 4{
                //                    //                    if currentDate <= Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                //                    //                        SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
                //                    //                        tblUserInfo.getMealDistrbutionUser()
                //                    //                        tblUserInfo.generteAgainMealPlanner { (s, e) in
                //                    ////                            SVProgressHUD.dismiss(completion: {
                //                    //                                DispatchQueue.main.async {
                //                    //                                    UIApplication.shared.isIdleTimerDisabled = false
                //                    //                                    disableView(bool:true)
                //                    //                                }
                //                    //                            SVProgressHUD.dismiss()
                //                    //                                setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                //                    //                                startTutorial()
                //                    ////                            })
                //                    //                        }
                //                    //                    }
                //                    setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                //                    SVProgressHUD.dismiss()
                //                    disableView(bool:true)
                //                }else {
                //                    print("yazan1")
                //
                //                    SVProgressHUD.dismiss(completion: {
                //                        DispatchQueue.main.async {
                //                            UIApplication.shared.isIdleTimerDisabled = false
                //                            disableView(bool:true)
                //                        }
                //                        setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                //                        startTutorial()
                //                    })
                //
                //
                //                }
                
            }
        }
    }
    /*
     /Users/admin/Desktop/ta5sees_ios-swift_new/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/admin/Desktop/ta5sees_ios-swift_new/Ta5sees/GoogleService-Info.plist -p ios /Users/admin/Library/Developer/Xcode/Archives/2021-03-01/Ta5sees\ 3-1-21\,\ 5.35\ PM.xcarchive/dSYMs
     */
    func setupButtons(){
        let breakfast = ActionButtonItem(title: "فطور", image: #imageLiteral(resourceName: "breakfast"))
        breakfast.action = {item in self.toAddWajbehSenfManual(id:"1")}
        let tasber1 = ActionButtonItem(title: "تصبيرة  صباحي", image: #imageLiteral(resourceName: "apple"))
        //           tasber1.action = { item in self.view.backgroundColor = UIColor.blue }
        tasber1.action =  {item in self.toAddWajbehSenfManual(id:"4")}
        let launsh = ActionButtonItem(title: "غداء", image: #imageLiteral(resourceName: "launch"))
        launsh.action =  {item in self.toAddWajbehSenfManual(id:"2")}
        let tasber2 = ActionButtonItem(title: "تصبيرة مسائي", image: #imageLiteral(resourceName: "tsbera"))
        tasber2.action =  {item in self.toAddWajbehSenfManual(id:"5")}
        let dinner = ActionButtonItem(title: "عشاء", image: #imageLiteral(resourceName: "dinner"))
        dinner.action =  {item in self.toAddWajbehSenfManual(id:"3")}
        actionButton = ActionButton(attachedToView: self.view, items: [dinner,tasber2,launsh,tasber1,breakfast])
        
        //           actionButton.setTitle("+", forState: UIControl.State())
        //           actionButton.backgroundColor = UIColor(red: 238.0/255.0, green: 130.0/255.0, blue: 130.0/255.0, alpha: 1)
        actionButton.action = { button in button.toggleMenu()}
    }
    func DiplayDailyData(){
        //        self.Kacleaten.isHidden = true
        //        self.KaclأBurend.isHidden = true
        tblDailyWajbeh.getDailyData(date: lblDate.text!) { [self] (res, err) in
            var obDistribution:tblUserTEEDistribution!
            tblUserTEEDistribution.getDataUserTEE(date: self.lblDate.text!) { (res, err) in
                obDistribution = (res as? tblUserTEEDistribution)
            }
            if res == nil {
                return
            }
            
            if res as? String == "false" {
                //                UIView.animate(withDuration: 0.6) {
                //                    self.Kacleaten.isHidden = false
                //                    self.KaclأBurend.isHidden = false
                //                    self.Kacleaten.alpha = 0
                //                    self.KaclأBurend.alpha = 0
                //                } completion: { bool in
                //                    self.Kacleaten.text = "\(String(0))\nتم الاكل"
                //                    self.KaclأBurend.text = "\(String(0))\nتم حرق"
                self.Kacleaten.alpha = 1
                self.KaclأBurend.alpha = 1
                //
                //                }
                KaclأBurend.fadeTransition(1)
                Kacleaten.fadeTransition(1)
                
                self.Kacleaten.text = "\(String(0))\nتم الاكل"
                self.KaclأBurend.text = "\(String(0))\nتم حرق"
                self.obPogressBar.setProgressValue(totalCalorisDay: (Int(obDistribution.Caloris)!), totalCalorisEate: 0)
                self.SetLable(totalCaloris: Double(obDistribution.Caloris)!)
                self.setDateDaily(Fat:Int(obDistribution.Fat)!, Pro: Int(obDistribution.Protien)!, cho: Int(obDistribution.Carbo)!, date: self.lblDate.text!)
            }else {
                self.Kacleaten.alpha = 1
                self.KaclأBurend.alpha = 1
                KaclأBurend.fadeTransition(1)
                Kacleaten.fadeTransition(1)
                
                let ob:tblDailyWajbeh = (res as? tblDailyWajbeh)!
                //                UIView.animate(withDuration: 0.6) {
                //                    self.Kacleaten.isHidden = false
                //                    self.KaclأBurend.isHidden = false
                //                    self.Kacleaten.alpha = 0
                //                    self.KaclأBurend.alpha = 0
                //                } completion: { bool in
                //                    self.Kacleaten.text = "\(String(ob.totalCalEaten))\nتم الاكل"
                //                    self.KaclأBurend.text = "\(String(ob.totalCalBurned))\nتم حرق"
                
                //                }
                self.Kacleaten.text = "\(String(ob.totalCalEaten))\nتم الاكل"
                self.KaclأBurend.text = "\(String(ob.totalCalBurned))\nتم حرق"
                self.obPogressBar.setProgressValue(totalCalorisDay: (ob.totalCalAllowed + ob.totalCalEaten), totalCalorisEate: ob.totalCalEaten)
                self.SetLable(totalCaloris: Double(ob.totalCalAllowed)+Double(ob.totalCalEaten)-Double(ob.totalCalBurned))
                setDateDaily(Fat:ob.totalFat, Pro: ob.totalProtein, cho: ob.totalCarb, date: self.lblDate.text!)
            }
            
            
        }
        
        
    }
    func setDateDaily(Fat:Int,Pro:Int,cho:Int,date:String){
        
        
        
        tblUserWajbehEaten.getProteinFatCarbo (date:date){ (arr, err) in
            
            print( "\(arr[2])  \(cho)")
            print(" \(arr[0])  \(Pro)")
            print(" \(arr[1])  \(Fat) ")
            
            if arr[2] <= cho {
                self.cho.text =  "\(abs(arr[2] - cho)) متبقي"
            }else{
                self.cho.text = "\(abs(arr[2] - cho)) زيادة"
            }
            
            if arr[0] <= Pro {
                self.protein.text = "\(abs(arr[0] - Pro)) متبقي"
            }else{
                self.protein.text = "\(abs(arr[0] - Pro)) زيادة"
            }
            
            
            if arr[1] <= Fat {
                self.fate.text = "\(abs(arr[1] - Fat)) متبقي"
            }else{
                self.fate.text = "\(abs(arr[1] - Fat)) زيادة"
            }
        }
        
    }
    
    @objc func showCalendar(){
        let childVC = storyboard!.instantiateViewController(withIdentifier: "Child") as! CustomCalender
        childVC.pd = self
        let segue = BottomCardSegue(identifier: nil, source: self, destination: childVC)
        view.isUserInteractionEnabled = false
        self.tabBarController!.tabBar.isUserInteractionEnabled = false
        segue.customHright = 300
        prepare(for: segue, sender: nil)
        segue.perform()
    }
    
    
    func showAlertDateTime(id_notification:String){
        //                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["1"])
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let childVC = storyboard!.instantiateViewController(withIdentifier: "CustomAlertController") as! CustomAlertController
        childVC.pd = self
        childVC.id_Notification = id_notification
        let segue = BottomCardSegue(identifier: nil, source: self, destination: childVC)
        segue.customHright = 200
        prepare(for: segue, sender: id_notification)
        view.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        segue.perform()
        
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
    func WaterSetting(Usedate:String,responsEHendler:@escaping (Any?,Error?)->Void){
        var date = Usedate
        if date.isEmpty || date == ""{
            date = getDateOnly()
        }
        tblUserWaterDrinked.getWaterDrink(date: date) { (response, error) in
            if response == nil {
                
                responsEHendler(false,nil)
                print("Query is null")
                setArrayWater(amountDrinked: 0, amountDefualt: 7) {
                    (res, err) in
                    self.modelData = res as! [model]
                    self.indexTrue = 0
                    self.waterConsu.text = "أستهلكت :\(0) لتر "
                    self.waterGoal.text = "الهدف : \(2) لتر"
                    responsEHendler(true,nil)
                }
                
                return
            }
            let ob:tblUserWaterDrinked = (response as? tblUserWaterDrinked)!
            if !ob.isEqual(nil){
                setArrayWater(amountDrinked: ob.amountDrinked, amountDefualt: ob.amountDefualt) { (res, err) in
                    self.modelData = res as! [model]
                    self.indexTrue = ob.amountDrinked
                    self.waterConsu.text = "أستهلكت :\(Double(ob.amountDrinked)/4.0) لتر "
                    self.waterGoal.text = "الهدف : \(Double(ob.amountDefualt)/4.0) لتر"
                    responsEHendler(true,nil)
                }
            }
        }
        
    }
    
    func loadWajbehEaten(){
        //test push bitbuket
        
        obPresenter.loadItemDinner(date: lblDate.text!)
        obPresenter.loadItemLuasnsh(date: lblDate.text!)
        obPresenter.loadItemBreakfast(date: lblDate.text!)
        obPresenter.loadItemDinner(date: lblDate.text!)
        obPresenter.loadItemTasbera1(date: lblDate.text!)
        obPresenter.loadItemTasber2(date: lblDate.text!)
        obPresenter.loadItemTamreen(date: lblDate.text!)
    }
    @IBOutlet weak var stackDateControl: UIStackView!
    var dayAsText:UILabel!
    
    @IBAction func nextDay(_ sender: Any) {
        
        effectButton(sender: btnnextDayOut)
        if LeapYearCounter  < 5 {
            LeapYearCounter += 1
        }
        
        if LeapYearCounter == 4 {
            DaysInMonths[1] = 29
        }
        
        if LeapYearCounter == 5{
            LeapYearCounter = 1
            DaysInMonths[1] = 28
        }
        
        dayInDate=dayInDate+01
        if dayInDate > DaysInMonths[month] {
            month += 1
            if month > 11 {
                month = 0
                year += 1
            }
            dayInDate =  1
        }
        currentMonth = Months[month]
        self.lblDate.text = "\(year)-\( self.currentMonth)-\( String(format: "%02d", dayInDate))"
        setTextDateAsDay(date:"\(year)-\(self.currentMonth)-\(String(format: "%02d", dayInDate))")
        animationviewLeft(dataView:dataView, duration: 0.2)
        TrackerWater(date: lblDate.text!)
        TrackerWeight(date: lblDate.text!)
        TrackerTamreen()
        loadWajbehEaten()
        
    }
    
    
    @IBAction func backDay(_ sender: Any) {
        effectButton(sender: btnbakDayOut)
        dayInDate=dayInDate-01
        if dayInDate < 1 {
            month -= 1
            if month == -1  {
                dayInDate =  DaysInMonths[0]
                if self.dayInDate <= 0 {
                    dayInDate =  DaysInMonths[0]
                }
            }else{
                dayInDate =  DaysInMonths[month]
            }
            
            
            if month < 0 {
                month = 11
                year-=1
            }
        }
        currentMonth = Months[month]
        self.lblDate.text = "\(year)-\(self.currentMonth)-\(String(format: "%02d", dayInDate))"
        
        setTextDateAsDay(date:"\(year)-\(self.currentMonth)-\(String(format: "%02d", dayInDate))")
        animationviewRight(dataView:dataView)
        TrackerTamreen()
        TrackerWater(date: lblDate.text!)
        TrackerWeight(date: lblDate.text!)
        loadWajbehEaten()
        //        loadItemInfo()
    }
    
    func setTextDateAsDay(date:String){
        if dateFormatter.date(from:date)?.timeIntervalSince1970 == dateFormatter.date(from: dateFormatter.string(from: Date.yesterday))!.timeIntervalSince1970 && dateFormatter.date(from: dateFormatter.string(from: Date.yesterday))!.timeIntervalSince1970 < dateFormatter.date(from:getDateOnly())!.timeIntervalSince1970{
            print("yesterday \( Date.yesterday)")
            dayAsText.text = "أمس"
            lblDate.isHidden = true
            stackDateControl.insertArrangedSubview(dayAsText, at: 1)
        }else if dateFormatter.date(from:date)?.timeIntervalSince1970 == dateFormatter.date(from: dateFormatter.string(from: Date.tomorrow))!.timeIntervalSince1970 && dateFormatter.date(from: dateFormatter.string(from: Date.tomorrow))!.timeIntervalSince1970 > dateFormatter.date(from:getDateOnly())!.timeIntervalSince1970{
            print("tomorrow \( Date.tomorrow)")
            dayAsText.text = "غداً"
            lblDate.isHidden = true
            stackDateControl.insertArrangedSubview(dayAsText, at: 1)
        }else if dateFormatter.date(from: date)?.timeIntervalSince1970 == dateFormatter.date(from:getDateOnly())!.timeIntervalSince1970 {
            print("yesterday \( Date.yesterday)")
            dayAsText.text = "اليوم"
            lblDate.isHidden = true
            stackDateControl.insertArrangedSubview(dayAsText, at: 1)
        }else {
            lblDate.isHidden = false
            //                dayAsText.isHidden = true
            dayAsText.removeFromSuperview()
        }
    }
    //    func setTextDateAsDay1(date:String){
    //        if dateFormatter.date(from:date)?.timeIntervalSince1970 == dateFormatter.date(from: dateFormatter.string(from: Date.yesterday))!.timeIntervalSince1970 {
    //            print("yesterday \( Date.yesterday)")
    //            dayAsText.text = "أمس"
    //            lblDate.isHidden = true
    //      stackDateControl.insertArrangedSubview(dayAsText, at: 1)
    //        }else if dateFormatter.date(from:date)?.timeIntervalSince1970 == dateFormatter.date(from: dateFormatter.string(from: Date.tomorrow))!.timeIntervalSince1970 {
    //            print("tomorrow \( Date.tomorrow)")
    //            dayAsText.text = "غداً"
    //            lblDate.isHidden = true
    //      stackDateControl.insertArrangedSubview(dayAsText, at: 1)
    //        }else if dateFormatter.date(from: date)?.timeIntervalSince1970 == dateFormatter.date(from: dateFormatter.string(from: Date.today))!.timeIntervalSince1970 {
    //            print("yesterday \( Date.yesterday)")
    //            dayAsText.text = "اليوم"
    //            lblDate.isHidden = true
    //      stackDateControl.insertArrangedSubview(dayAsText, at: 1)
    //        }else {
    //            lblDate.isHidden = false
    //            //                dayAsText.isHidden = true
    //            dayAsText.removeFromSuperview()
    //        }
    //    }
    //////////////Updaet Weight
    
    @IBAction func btnUpdateWeight(_ sender: Any) {
        
        //        SVProgressHUD.show()
        let date = lblDate.text
        var value = String(Double(updateweight!.count))
        if  (value.firstIndex(of: ".") != nil) {
            value = String(format: "%.1f", CGFloat(Double(value)!))
            print("value \(value)   \(String(format: "%.0f", CGFloat(Double(value)!)))")
        }
        
        updateWeightUser(date: date!,new_weight:value,view:view) { [self] (bool) in
            SVProgressHUD.dismiss()
            createEvent(key: "12", date: getDateTime())
            showToast(message:"تم تحديث الوزن", view: self.view,place:0)
            print(bool)
        }
        
        //        CheckInternet.checkIntenet { (bool) in
        //            if !bool {
        //                SVProgressHUD.dismiss()
        //                showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
        //                return
        //            }
        //        }
        tblUserProgressHistory.updateWeightHeightUser(weight: value, height: "", date: getDateOnly()) {  (str, err) in
            if str == "success" {
                //                updateWeightUser(date: date!,new_weight:value,view:view) { (bool) in
                SVProgressHUD.dismiss()
                //                    createEvent(key: "12", date: getDateTime())
                //                    showToast(message:"تم تحديث الوزن", view: self.view,place:0)
                //                    print(bool)
                //                }
            }else if str == "false" {
                SVProgressHUD.dismiss()
                //                showToast(message:"لم يتم تحديث الوزن", view: self.view,place:0)
            }
        }
        
        
        //        tblUserInfo.updateWeightUser(weight: value) { [self] (str, err) in
        //            if str == "success" {
        //                updateWeightUser(date: date!,new_weight:value,view:view) { (bool) in
        //                    SVProgressHUD.dismiss()
        //                    showToast(message:"تم تعديل الوزن", view: self.view,place:0)
        //                    print(bool)
        //                }
        //            }else if str == "false" {
        //                SVProgressHUD.dismiss()
        //                showToast(message:"لم يتم تعديل الوزن", view: self.view,place:0)
        //            }
        //        }
        
    }
    
    @IBOutlet weak var breakfastTarget: UILabel!
    @IBOutlet weak var snak1Target: UILabel!
    @IBOutlet weak var launshTarget: UILabel!
    @IBOutlet weak var snak2Target: UILabel!
    @IBOutlet weak var dinnerTarget: UILabel!
    
    @IBOutlet weak var breakfastEate: UILabel!
    @IBOutlet weak var snak1Eate: UILabel!
    @IBOutlet weak var launshEate: UILabel!
    @IBOutlet weak var snak2Eate: UILabel!
    @IBOutlet weak var dinnerEate: UILabel!
    @IBOutlet weak var btnMealPalnnerOut: btnTwitter!
    @IBOutlet weak var viewMealPlanner: CapsuleView!
    
    
    @IBOutlet weak var updateweight: UIStepperController!
    //////////////////////////////////////////////////
    
    @IBOutlet weak var waterConsu: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var waterview: CapsuleView!
    
    /****************** btn add wajbh ***********************/
    @IBAction func waterSettinga(_ sender: Any) {
        effectButton(sender: btnwaterOut)
        //        var alertStyle = UIAlertController.Style.actionSheet
        //        if (UIDevice.current.userInterfaceIdiom == .pad) {
        //          alertStyle = UIAlertController.Style.alert
        //        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.view.tintColor = colorGreen
        
        //        alert.addAction(UIAlertAction(title: "اقرا المزيد حول الماء", style: .default, handler: { _ in}))
        alert.addAction(UIAlertAction(title: "اعدادات الماء", style: .default, handler: { c in
            self.performSegue(withIdentifier: "ws", sender: self)
        }))
        alert.addAction(UIAlertAction(title: "تعديل المنبه", style: .default) {  _ in
            self.showAlertDateTime(id_notification: "7")
            
        })
        alert.addAction(UIAlertAction.init(title: "الغاء", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func getText(id:String)->String{
        switch id {
        case "1":
            return "إضافة فطور"
        case "2":
            return "إضافة غداء"
        case "3":
            return "إضافة عشاء"
        case "4":
            return "إضافة تصبيرة صباحية"
        case "5":
            return "إضافة تصبيرة مسائية"
        case "6":
            return "إضافة تمرين"
        default:
            return ""
        }
    }
    func showDialogAlarm(id:String) {
        let mainmessage =  "هل تريد \(getText(id:id))"
        let alert = UIAlertController(title:title_alarm, message: mainmessage, preferredStyle: .alert)
        
        
        alert.view.tintColor = colorGreen
        
        alert.addAction(UIAlertAction(title: "\(getText(id:id))", style: .default, handler: { [self] _ in
            if id != "6" {
                self.performSegue(withIdentifier: "addWajbehSenfManual", sender: id)
            }else{
                self.performSegue(withIdentifier: "TamreenTypeListController", sender: self)
            }
            id_alarm = "0"
        }))
        alert.addAction(UIAlertAction(title: "لا", style: .default, handler: { [self] _ in
            NSLog("Cancel Pressed")
            id_alarm = "0"
            
        }))
        
        self.present(alert, animated: true) { [self] in
            id_alarm = "0"
        }
        
        
    }
    
    func showDialog(id:String) {
        //        var alertStyle = UIAlertController.Style.actionSheet
        //        if (UIDevice.current.userInterfaceIdiom == .pad) {
        //          alertStyle = UIAlertController.Style.alert
        //        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        
        
        let alert_cancel = UIAlertAction.init(title: "الغاء", style: .cancel, handler: nil)
        alert.view.tintColor = colorGreen
        if id != "6" {
            alert.addAction(UIAlertAction(title: getText(id: id), style: .default, handler: { _ in
                self.toAddWajbehSenfManual(id:id)
            }))
        }
        alert.addAction(UIAlertAction(title: "تعديل المنبه", style: .default) {  _ in
            self.showAlertDateTime(id_notification: id)
        })
        
        var titleTut = "كيفية إضافة وجبة"
        
        if id == "6" {
            titleTut = "كيفية إضافة تمرين"
        }
        
        alert.addAction(UIAlertAction(title:titleTut, style: .default) { [self]  _ in
            view.superview?.isUserInteractionEnabled = false
            if id != "6" {
                
                setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
                showTutorialWajbeh(view: getView(flag:id), btn: getBtn(flag:id))
                
            }else{
                setDataInSheardPreferance(value: "1", key: "isTutorial-tamreen")
                startShowTutorialTamreen()
            }
        })
        
        
        alert.addAction(alert_cancel)
        
        self.present(alert, animated: true) { [self] in
            
            if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") == "0" {
                var f = alert.view.frame
                f.size.height = 30
                f.origin.y = f.origin.y + 5
                f.origin.x = f.origin.x + 2
                f.size.width = f.size.width - 5
                
                location.append(f)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
                    startShowTutorialWajbehAlert()
                }
            }
            
        }
    }
    
    //    func getCodrdinatBtn(flag:String)->CGRect{
    //         switch flag {
    //         case "1":
    //             return btnBreakOut.frame
    //         case "2":
    //            return btnLaunchOut.frame
    //         case "3":
    //            return btnDinnerOut.frame
    //         case "4":
    //            return btnTasbera1.frame
    //         case "5":
    //            return btnTasberaOut.frame
    //         case "6":
    //            return btnTmreenOut.frame
    //         default:
    //            return btnBreakOut.frame
    //         }
    //     }
    func getBtn(flag:String)->UIButton{
        switch flag {
        case "1":
            return btnBreakOut
        case "2":
            return btnLaunchOut
        case "3":
            return btnDinnerOut
        case "4":
            return btnTasbera1
        case "5":
            return btnTasberaOut
        case "6":
            return btnTmreenOut
        default:
            print("no select")
        }
        return btnBreakOut
    }
    func getView(flag:String)->UIView{
        switch flag {
        case "1":
            return viewBreakfast
        case "2":
            return viewLaunch
        case "3":
            return viewDinner
        case "4":
            return viewTasber1
        case "5":
            return viewTasberh2
        case "6":
            return viewTamreen
        default:
            print("no select")
            return viewBreakfast
        }
        
    }
    @IBAction func btnbreakfast(_ sender: UIButton) {
        showDialog(id:"1")
        //        createEvent(key: "1", date: getDateTime())
        effectButton(sender: btnBreakOut)
    }
    @IBAction func btnlaunch(_ sender: Any) {
        showDialog(id:"2")
        //        createEvent(key: "5", date: getDateTime())
        effectButton(sender: btnLaunchOut)
    }
    @IBAction func btndiner(_ sender: Any) {
        showDialog(id:"3")
        //        createEvent(key: "6", date: getDateTime())
        effectButton(sender: btnDinnerOut)
    }
    @IBAction func btntsbera(_ sender: Any) {
        showDialog(id:"4")
        //        createEvent(key: "7", date: getDateTime())
        effectButton(sender: btnTasbera1)
    }
    @IBAction func btntsbera2(_ sender: Any) {
        showDialog(id:"5")
        //        createEvent(key: "8", date: getDateTime())
        effectButton(sender: btnTasberaOut)
    }
    @IBAction func btntmreen(_ sender: Any) {
        print("createEvent")
        showDialog(id:"6")
        //        createEvent(key: "9", date: getDateTime())
        effectButton(sender: btnTmreenOut)
    }
    
    @objc func tap(gesture: UIGestureRecognizer){
        let tag = gesture.view?.tag
        //        effectView(sender: (gesture.view)!)
        FilterIDGesture(tag:tag!)
        
    }
    
    func FilterIDGesture(tag:Int){
        var arr:[String] = []
        
        switch tag {
        case 1 :
            self.toAddWajbehSenfManual(id:String(tag))
            break
        case 2 :
            
            self.toAddWajbehSenfManual(id:String(tag))
            break
        case 3 :
            
            self.toAddWajbehSenfManual(id:String(tag))
            break
        case 5 :
            self.toAddWajbehSenfManual(id:String(tag))
            break
        case 4 :
            self.toAddWajbehSenfManual(id:String(tag))
            break
        case 6 :
            arr.append("التمارين")
            createEvent(key: "9", date: getDateTime())
            self.performSegue(withIdentifier: "TamreenTypeListController", sender: self)
            break
        default:
            print("default")
        }
        
        
        
    }
    
    func alertAddwajbeh(parsData:[String]){
        protocalViewWajbeh.toViewWajbeh(parsData:parsData)
    }
    
    /*****************************************/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "mealPlanner" {
            if let destVC = segue.destination as? UINavigationController,
               let targetController = destVC.topViewController as? ViewControllerMealPlanner {
                targetController.txtDateDay = lblDate.text!
                targetController.delegateMain = self
                targetController.pg = self
                
            }
        }else
        if segue.identifier == "mealPlanner1" {
            if let dis=segue.destination as? ViewControllerMealPlanner{
                if  let data=sender as? String {
                    print("data ____ \(data)")
                    dis.txtDateDay = data
                    dis.delegateMain = self
                    dis.pg = self
                }
            }
        }
        //        else if segue.identifier == "editWajbeh" {
        //            if let dis=segue.destination as?  ViewWahjbeh{
        //                if  let data=sender as? [String] {
        //                    dis.titleNavigation=data[0]
        //                    dis.wajbehInfo = data[1]
        //                    dis.txtDateDay = lblDate.text
        //                    dis.refprotocolGeneralTamreen = self
        //                }
        //            }
        //        }
        else if segue.identifier == "ws" {
            if let dis=segue.destination as?  WaterSettingVC{
                if  let data=sender as? ViewController {
                    dis.refprotocolDate = data
                    dis.date = lblDate.text
                }
            }
        }
        
        else if segue.identifier == "addWajbehSenfManual" {
            if let dis=segue.destination as?  SearchItems{
                if  let data=sender as? String {
                    dis.id_items = data
                    dis.date = lblDate.text
                    dis.pg = self
                    dis.delegateMain = self
                }
            }
        }
        else if segue.identifier == "TamreenTypeListController" {
            if let dis=segue.destination as?  TamreenTypeListController{
                if  let data=sender as? ViewController {
                    dis.date = lblDate.text!
                    dis.refprotocolGeneralTamreen = data
                }
            }
        }else if segue.identifier == "discountSubscribe" {
            if let dis=segue.destination as?  AdvViewControllers{
                if  let data=sender as? [Bool] {
                    dis.setupButton()
                    dis.subscribeDirect = data[0]
                    dis.hideFreeTrialBTN = data[1]
                    dis.CVHome = self
                    dis.loadViewIfNeeded()
                }
            }
        }
        
        
        
    }
    @IBOutlet weak var collectionviewoutl: SelfSizedCollectionView!
    
    @IBOutlet weak var stack2Tamren: UIStackView!
    @IBOutlet weak var stack1Tamreen: UIStackView!
    @IBOutlet weak var test_view: UIView!
    @IBOutlet weak var stackBreakfast: UIStackView!
    var modelData = [model]()
    var selectedIndex = Int ()
    @IBOutlet weak var pieView: UIView!
    @IBOutlet weak var toolbar: UINavigationItem!
    @IBOutlet weak var muview: UIView!
    
    @IBOutlet weak var viewBreakfast: CapsuleView!
    var lastOffsetY:CGFloat!
    var vwBG = UIView()
    
    static var config:Any!
    
    func convertLocationItems(location:CGRect)->CGRect{
        let l1 = collectionviewoutl.convert(location, to: self.muview)
        let l2 = muview.convert(l1, to: self.dataView)
        let l3 = dataView.convert(l2, to: self.muview)
        let l42 = muview.convert(l3, to: self.scroll)
        var d = scroll.convert(l42, to: self.view.superview)
        
        d.origin.y+=hightBanner
        return  d//scroll.convert(l42, to: self.view.superview)
    }
    func convertLocationItems2(location:CGRect,view_:UIView)->CGRect{
        let l1 = view_.convert(location, to: self.dataView)
        let l3 = dataView.convert(l1, to: self.muview)
        let l42 = muview.convert(l3, to: self.scroll)
        var d = scroll.convert(l42, to: self.view.superview)
        d.origin.y+=hightBanner
        return d// scroll.convert(l42, to: self.view.superview)
    }
    func convertLocationMealPlannerFocus(location:CGRect)->CGRect{
        let l3 = dataView.convert(viewMealPlanner.frame, to: self.muview)
        let l42 = muview.convert(l3, to: self.scroll)
        var d = scroll.convert(l42, to: self.view.superview)
        d.origin.y+=hightBanner
        return d
    }
    
    var imagTut = [TrackerWaterCell]()
    
    func showTutorialMealPlanner() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        
        let focusRect4 = convertLocationMealPlannerFocus(location:viewMealPlanner.frame)
        
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame4 = CGRect(x:self.view.bounds.width/2-72/2, y: focusRect4.maxY-120, width: 72, height: 72)
        
        let message2 = "فعل اشتراكك المجاني من هنا"
        let message1Center = CGPoint(x: dataView.bounds.width/2, y: focusRect4.maxY+20)
        
        
        let tut4 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect4, text: message2, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame4)
        
        let tutorials = [tut4]
        tutorialVC.tutorials = tutorials
        tutorialVC.viewMainHome = self
        tutorialVC.viewMainHomeMealPlanner = true
        tutorialVC.showInViewController(self)
    }
    
    func showTutorialWater() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let focusRect1 = convertLocationItems(location:location[0])
        let focusRect2 = convertLocationItems(location:location[1])
        let focusRect3 = convertLocationItems(location:location[2])
        
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame1 = CGRect(x: focusRect1.origin.x-20 , y: focusRect1.maxY, width: 72, height: 72)
        let icon3Frame2 = CGRect(x: focusRect2.origin.x/2+15 , y: focusRect2.maxY, width: 72, height: 72)
        let icon3Frame3 = CGRect(x: focusRect3.origin.x-20, y: focusRect3.maxY, width: 72, height: 72)
        
        let message1 = "إضغط على كأس الماء لإضافة ٢٥٠ مل من الماء"
        let message1Center = CGPoint(x: dataView.bounds.width/2, y: focusRect1.maxY + focusRect1.maxY/2)
        
        let tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame1)
        //        tut1.isArrowHidden = true
        
        let tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame2)
        //        tut2.isArrowHidden = true
        
        let tut3 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect3, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame3)
        
        
        //        tut3.isArrowHidden = true
        
        let tutorials = [tut1,tut2,tut3]
        tutorialVC.tutorials = tutorials
        tutorialVC.viewMainHome = self
        tutorialVC.viewMainHomeflage = true
        tutorialVC.showInViewController(self)
    }
    func showTutorialNutraionValus() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let focusRect1 = convertLocation(location: pieView.frame, flag: 0)
        let focusRect2 = convertLocation(location: Kacleaten.frame, flag: 0)
        let focusRect3 = convertLocation(location: KaclأBurend.frame, flag: 0)
        
        let focusRect4 = convertLocation(location: nutritionalValues.frame, flag: 1)
        
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame1 = CGRect(x: view.center.x-20, y: focusRect1.maxY, width: 72, height: 72)
        let icon3Frame2 = CGRect(x: focusRect2.origin.x/2+15 , y: focusRect2.maxY, width: 72, height: 72)
        let icon3Frame3 = CGRect(x: focusRect3.origin.x, y: focusRect3.maxY, width: 72, height: 72)
        let icon3Frame4 = CGRect(x: view.center.x-20, y: focusRect4.maxY+20, width: 72, height: 72)
        
        
        let message1 = "مجموع السعرات الحرارية اليومية المسموح استهلاكها للوصول للوزن المناسب من مجموع التمارين و الوجبات اليومية"
        let message2 =  "السعرات الحرارية التي تم استهلاكها من الوجبات اليومية"
        let message3 = "السعرات الحرارية التي تم حرقها بالتمارين"
        let message4 = "مجموع القيم الغذائية"
        
        let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + focusRect1.maxY/2)
        let message4Center = CGPoint(x: view.bounds.width/2, y: focusRect4.maxY + 15)
        
        let tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame1)
        
        let tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message2, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame2)
        
        let tut3 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect3, text: message3, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame3)
        
        let tut4 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect4, text: message4, textPosition: message4Center, icon: icon3, iconFrame: icon3Frame4)
        
        
        let tutorials = [tut1,tut2,tut3,tut4]
        tutorialVC.tutorials = tutorials
        tutorialVC.viewMainHome = self
        tutorialVC.tutAddTamreenMainHome = false
        tutorialVC.tutAddWajbehLastStep = false
        tutorialVC.isCircle = true
        tutorialVC.viewMainHomeNutrationValues = true
        tutorialVC.viewMainHomeflage = false
        tutorialVC.tutAddWajbeh = false
        tutorialVC.showInViewController(self)
    }
    func showTutorialWajbeh(view:UIView,btn:UIButton) {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let focusRect4 = convertLocationItems2(location:btn.frame, view_: view)
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame1 = CGRect(x: btn.frame.origin.x , y: focusRect4.maxY, width: 72, height: 72)
        let message12 = "يمكنك إضافة وجبة الفطور او الغداء او العشاء كالتالي"
        let message1Center2 = CGPoint(x: dataView.bounds.width/2, y: focusRect4.maxY - focusRect4.maxY*0.3)
        let tut4 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect4, text: message12, textPosition: message1Center2, icon: icon3, iconFrame: icon3Frame1)
        
        //            tut4.isArrowHidden = true
        let tutorials = [tut4]
        tutorialVC.tutAddWajbeh = true
        tutorialVC.id_category = "\(btn.tag)"
        tutorialVC.tutorials = tutorials
        tutorialVC.viewMainHome = self
        tutorialVC.tutAddTamreenMainHome = false
        tutorialVC.tutAddWajbehLastStep = false
        tutorialVC.viewMainHomeflage = false
        tutorialVC.showInViewController(self)
    }
    
    func showTutorialWajbehAlert() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let focusRect5 = location[location.count-1]
        
        let message123 = "يمكنك إضافة وجبة الفطور او الغداء او العشاء كالتالي"
        let message1Center2 = CGPoint(x: view.bounds.width/2, y: focusRect5.maxY - focusRect5.maxY*0.4)
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect5.maxY + 30, width: 72, height: 72)
        
        let tut5 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect5, text: message123, textPosition: message1Center2, icon: icon3, iconFrame: icon3Frame)
        if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1" {
            
            let tutorials = [tut5]
            tutorialVC.tutAddWajbeh = false
            tutorialVC.tutAddWajbehLastStep = true
            tutorialVC.tutorials = tutorials
            tutorialVC.tutAddTamreenMainHome = false
            tutorialVC.viewMainHome = self
            tutorialVC.presentedUIAlertController = true
            
            tutorialVC.viewMainHomeflage = false
            tutorialVC.showInViewController(self)
        }
        
    }
    func setupBanner(){
        
        if getUserInfo().subscribeType == -1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 2 || getUserInfo().subscribeType == 3 {
            hightBanner = 70
            tabBarController?.view.superview?.frame.origin.y = 70
            tabBarController?.view.superview?.frame.size.height-=70
            //             self.navigationController!.navigationBar.layer.zPosition = -1;
            
            let customView = Bundle.main.loadNibNamed("BannerVC", owner: self, options: nil)![0] as? BannerVC
            customView?.tag = 890
            if #available(iOS 13.0, *) {
                // no action
            }else{
                customView?.frame.size.width = window!.frame.width
            }
            customView!.delegate = self
            if getUserInfo().subscribeType == -1 {
                customView?.btnsubcribeNow.setTitle("فعل الاشتراك المجاني الآن", for: .normal)
            }else{
                customView?.btnsubcribeNow.setTitle("فعل الاشتراك الآن", for: .normal)
            }
            
            window?.addSubview(customView!)
            if #available(iOS 13.0, *) {
                customView!.translatesAutoresizingMaskIntoConstraints = false
            }
            customView!.leadingAnchor.constraint(equalTo: window!.leadingAnchor, constant: 0).isActive = true
            customView!.trailingAnchor.constraint(equalTo: window!.trailingAnchor, constant: 0).isActive = true
            customView!.topAnchor.constraint(equalTo: window!.topAnchor, constant: 0).isActive = true
            
            customView!.bottomAnchor.constraint(equalTo:    (tabBarController?.view.superview?.topAnchor)!, constant: 0).isActive = true
            //            customView!.bottomAnchor.constraint(equalTo:(window?.subviews[1].topAnchor)!, constant: 0).isActive = true
            
            
            customView!.heightAnchor.constraint(equalToConstant: 70).isActive = true
            tabBarController?.tabBar.items?[2].badgeValue = ""
            tabBarController?.tabBar.items![2].badgeColor = UIColor.init(hexString: "#FFD700")
        }else{
            
            removeBanner()
        }
    }
    func removeBanner(){
        
        tabBarController?.tabBar.items?[2].badgeValue = nil
        hightBanner = 0
        if  tabBarController?.view.superview?.frame.origin.y == 70 {
            tabBarController?.view.superview?.frame.origin.y = 0
            tabBarController?.view.superview?.frame.size.height+=70
        }else{
            tabBarController?.view.superview?.frame.origin.y = 0
        }
    }
    func showTutorialTamreenAlert() {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        
        let l1 = stack1Tamreen.convert(imgTamreen.frame, to: self.stack2Tamren)
        let l2 = stack2Tamren.convert(l1, to: self.viewTamreen)
        var l3 =  convertLocationItems2(location:l2, view_: viewTamreen)
        l3.size.width = 70
        l3.origin.x = self.view.bounds.width/2-72/2
        let focusRect5 = l3
        let message123 = "يمكنك إضافة تمرين كالتالي"
        let message1Center2 = CGPoint(x: view.bounds.width/2, y: focusRect5.maxY - (focusRect5.maxY*0.5/1.5))
        let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
        
        let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect5.maxY - (focusRect5.maxY*0.5/2), width: 72, height: 72)
        
        let tut5 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect5, text: message123, textPosition: message1Center2, icon: icon3, iconFrame: icon3Frame)
        
        let tutorials = [tut5]
        tutorialVC.tutAddWajbeh = false
        tutorialVC.tutAddWajbehLastStep = false
        tutorialVC.tutAddTamreenMainHome = true
        tutorialVC.tutorials = tutorials
        tutorialVC.viewMainHome = self
        tutorialVC.viewMainHomeflage = false
        tutorialVC.showInViewController(self)
    }
    
    
    func startShowTutorialWajbeh(){
        var bottomOffset1:CGPoint!
        bottomOffset1 = CGPoint(x: 0, y:viewBreakfast.frame.origin.y+120)
        scroll.setContentOffset(bottomOffset1, animated: true)
        
        if bottomOffset1 != nil {
            scroll.setContentOffset(bottomOffset1, animated: true)
        }
        view.superview?.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.90) { [self] in
            showTutorialWajbeh(view: viewBreakfast, btn: btnBreakOut)
        }
    }
    func startShowTutorialTamreen(){
        var bottomOffset1:CGPoint!
        bottomOffset1 = CGPoint(x: 0, y:scroll.contentSize.height - scroll.bounds.height + scroll.contentInset.bottom)
        scroll.setContentOffset(bottomOffset1, animated: true)
        
        if bottomOffset1 != nil {
            scroll.setContentOffset(bottomOffset1, animated: true)
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) { [self] in
            if getDataFromSheardPreferanceString(key: "isTutorial-tamreen") != "2" {
                showTutorialTamreenAlert()
            }
        }
    }
    func startShowTutorialWajbehAlert(){
        if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                showTutorialWajbehAlert()
            }
        }
    }
    
    //    lazy var tutorialVC: KJOverlayTutorialViewController = {
    //        return KJOverlayTutorialViewController()
    //    }()
    
    func showAlertTut(){
        showDialog(id: "1")
    }
    
    @objc func TimePickerNotification(_ notification: Notification) {
        SVProgressHUD.dismiss()
        print("run 2")
        if getDataFromSheardPreferanceString(key: "isTutorial-water") == "0" {
            DispatchQueue.main.async { [self] in
                setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                disableView(bool:true)
                startTutorial()
            }
        }
    }
    func convertLocation(location:CGRect,flag:Int)->CGRect{
        if flag == 1 {
            var l2 = muview.convert(location, to: self.scroll)
            l2.origin.y+=hightBanner
            return scroll.convert(l2, to: self.view.superview)
        }
        let l1 = stackDataDaily.convert(location, to: self.muview)
        var l42 = muview.convert(l1, to: self.scroll)
        l42.origin.y+=hightBanner
        return  scroll.convert(l42, to: self.view.superview)
    }
    func TapGestureNutritionalValues(view:UIStackView){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapNutritionalValues(gesture:)))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    func TapGesturePieView(view:UIView){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapPieView(gesture:)))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    func TapGestureLabel(view:UILabel){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapLabel(gesture:)))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    
    
    @objc func tapNutritionalValues(gesture: UIGestureRecognizer){
        if !removeLableWindow() {
            showToastNutritional(message: "\nمجموع القيم الغذائية\n", view: view, place: 1,frame: convertLocation(location:nutritionalValues.frame,flag:1),item:nutritionalValues)
        }
    }
    @objc func tapPieView(gesture: UIGestureRecognizer){
        if !removeLableWindow() {
            showToastNutritional(message: "\nمجموع السعرات الحرارية اليومية المسموح استهلاكها للوصول للوزن المناسب من مجموع التمارين و الوجبات اليومية\n", view: view, place: 1,frame:convertLocation(location:pieView.frame,flag:0),item:stackDataDaily)
        }
    }
    @objc func tapViewMain(gesture: UIGestureRecognizer){
        _ = removeLableWindow()
    }
    
    func removeLableWindow()->Bool{
        for subview in view.subviews {
            if subview.tag == 899 {
                subview.removeFromSuperview()
                return true
            }
        }
        return false
    }
    @objc func tapLabel(gesture: UIGestureRecognizer){
        let tag = gesture.view?.tag
        if !removeLableWindow() {
            if tag == 66 {
                showToastNutritional(message: "\nالسعرات الحرارية التي تم استهلاكها من الوجبات اليومية\n", view: view, place: 1,frame: convertLocation(location:Kacleaten.frame,flag:0),item:stackDataDaily)
            }else if tag == 65{
                showToastNutritional(message: "\nالسعرات الحرارية التي تم حرقها بالتمارين\n", view: view, place: 1,frame: convertLocation(location:KaclأBurend.frame,flag:0),item:stackDataDaily)
            }
        }
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        _=removeLableWindow()
        
    }
    
    func viewPopupTarget() {
        
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "PopuPController") as! PopuPController
        
        popoverContent.viewAccountPresnter = obpresnterAccount
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
        popover!.delegate = self
        popover!.sourceView = self.view
        popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)
        
        
        self.present(nav, animated: true, completion: nil)
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
        
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    lazy var obpresnterAccount = PresnterAccount(with:self)
    
    func viewPopuSlider(new_target:String) {
        
        
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "SliderPopupController") as! SliderPopupController
        popoverContent.new_target = Double(new_target)
        popoverContent.viewAccountPresnter = obpresnterAccount
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = .popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
        popover!.delegate = self
        popover!.permittedArrowDirections = .up
        popover!.sourceView = self.view
        
        popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)
        
        
        self.present(nav, animated: true, completion: nil)
        
    }
    
    func setupTapNotfy(){
        var bottomOffset_alarm:CGPoint!
        
        if id_alarm != "0" {
            if id_alarm == "7" {
                bottomOffset_alarm = CGPoint(x: 0, y:waterview.frame.origin.y+100)
            }else if id_alarm == "8" {
                bottomOffset_alarm = CGPoint(x: 0, y:viewUpdateWeight.frame.origin.y+100)
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                    showDialogAlarm(id:id_alarm)
                }
            }
        }
        if bottomOffset_alarm != nil {
            scroll.setContentOffset(bottomOffset_alarm, animated: true)
            id_alarm = "0"
        }
    }
    @objc func willEnterForeground() {
        print("will enter foreground")
        setupTapNotfy()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.delegate = self
        //        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.didBecomeActiveNotification, object: nil)
      
        setGradientBackgroundButton()
        
        
        if getNumberDaysBetweenDate(date1:getDataFromSheardPreferanceString(key: "latestDateOpenApp")) > 2592000 {//as sec for 30 days
            self.performSegue(withIdentifier: "LastOpenAppController", sender: nil)
        }
        
        //display bannar
        setupBanner()
        
        if getUserInfo().subscribeType == 0 {
            
            if getUserInfo().expirDateSubscribtion != "" || getUserInfo().expirDateSubscribtion != "0.0" {
                
                if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                    customShowAlert(m1: "الغاء", m2: "اشتراك مدفوع", m3:"هل تريد الاشتراك الآن ؟", m4:"تم الانتهاء من الفترة التجريبية" , flag: -1)
                    //                    let alert = UIAlertController(title:  "تم الانتهاء من الفترة التجريبية؟", message:"هل تريد الاشتراك الآن ؟" , preferredStyle: .alert)
                    //
                    //                    alert.view.tintColor = colorGreen
                    //
                    //                    alert.addAction(UIAlertAction(title: "اشتراك مدفوع", style: .default,handler: { c in
                    //                        self.tabBarController?.selectedIndex = 2
                    //                    }))
                    //
                    //                    alert.addAction(UIAlertAction.init(title: "الغاء", style: .cancel, handler: nil))
                    //
                    //                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.TimePickerNotification(_:)), name: Notification.Name("dissmesDialog"), object: nil)
        
        //        nc.addObserver(self, selector: #selector(self.TimePickerNotification(_:)), name: Notification.Name("notificationName1"), object: nil)
        
        scroll_home_main = scroll
        
        
        
        APiSubscribeUser.apiSubscribeUser().updateUserInfo()
        
        if getDataFromSheardPreferanceString(key: "latestDateOpenApp") == "0" {
            setDataInSheardPreferance(value: getDateOnly(), key: "latestDateOpenApp")
        }
        
        if getDataFromSheardPreferanceString(key: "lastUpdataSyncData") == "0" {
            setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
        }
        
        if getNumberDaysBetweenDate(date1:getDataFromSheardPreferanceString(key: "lastUpdataSyncData"))  >= 604800 {//as sec for 7 days
            setDataInSheardPreferance(value: "yes", key: "isSyncData")
        }
        
        if getDataFromSheardPreferanceString(key: "isTutorial") == "0" && getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "0"{
            print("here90")
            disableView(bool:false)
            SVProgressHUD.show(withStatus: "يرجى الانتظار قليلا")
        }
        
        CheckInternet.stopListnerIntenet()
        view.isUserInteractionEnabled = true
        scroll.isUserInteractionEnabled = true
        
        
        TapGestureLabel(view: Kacleaten)
        TapGestureLabel(view: KaclأBurend)
        
        scroll.decelerationRate = UIScrollView.DecelerationRate.fast
        
        //        checkLatstVersion(view: self)
        if  getDataFromSheardPreferanceString(key: "isCompletedDataLoad") != "0"{ // data is download
            if  getDataFromSheardPreferanceString(key: "isTutorial") == "1"{ // tutorial finish
                
                checkLatstVersion(view: self) { [self] str,version in
                    if str == "major" {
                        customShowAlert(m1: "إلغاء", m2: "تحديث", m3: "توفر إصدار جديد،يرجى تحديث التطبيق", m4: "إصدار رقم :\(version)",flag:1)
                    }else{
                        customShowAlert(m1: "إلغاء", m2: "تحديث", m3: "توفر إصدار جديد،يرجى تحديث التطبيق", m4: "إصدار رقم :\(version)",flag:2)
                    }
                } //update popup
            }
        }
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dayAsText = UILabel()
        dayAsText.frame = stackDateControl.frame
        dayAsText.center.x = stackDateControl.center.x
        dayAsText.textAlignment = .center
        dayAsText.textColor = UIColor(hexString:"#9CABB3")
        dayAsText.font = UIFont.init(name: "GE Dinar One", size: 17)
        dayAsText.isEnabled = true
        dayAsText.isUserInteractionEnabled = true
        showCalender(label:dayAsText)
        
        tblLaunsh.rowHeight = UITableView.automaticDimension
        scroll.delegate = self
        let bottomOffset = CGPoint(x: 0, y:300)
        scroll.setContentOffset(bottomOffset, animated: true)
        
        tblLaunsh.estimatedRowHeight = 140
        pd = self
        obprotocolWajbehInfoTracker = self
        protocalViewWajbeh = self
        obItemsViewList = self
        setupButtons()
        obPogressBar = progresBarHome(view: muview ,contentView: pieView)
        obDisplayUserWeight = DisplayUserWeight(view:self,updateweight: updateweight)
        //        imgTamreenTap.tag = 6
        tblBreakfast.maxHeight = 1000
        tblDinner.maxHeight = 1000
        tblLaunsh.maxHeight = 1000
        tblTasber2.maxHeight = 1000
        tblTasber1.maxHeight = 1000
        tblTamreen.maxHeight = 1000
        collectionviewoutl.maxHeight = 500
        
        
        showCalender(label:lblDate)
        //        TapGestureView(view:viewBreakfast)
        TapGestureStackview(view:stackBreakfast)
        TapGestureStackview(view:stackLunch)
        TapGestureStackview(view:stackTasbera1)
        TapGestureStackview(view:stackTasbera2)
        TapGestureStackview(view:stackDinner)
        TapGestureStackview(view:stackTamreen)
        TapGestureNutritionalValues(view:nutritionalValues)
        TapGesturePieView(view:pieView)
        
        //                TapGestureView(view:viewDinner)
        //        TapGestureView(view:viewLaunch)
        //        TapGestureView(view:viewTasber1)
        //        TapGestureView(view:viewTasberh2)
        //        TapGestureView(view:viewTamreen)
        //        viewBreakfast/
        currentMonth = Months[month]
        
        if  dateNotfy != ""{
            let arr = getDateOnlyAsNumbers(date:dateNotfy)
            year = arr[1]
            dayInDate = arr[0]
            currentMonth = "\(arr[2])"
            lblDate.text = dateNotfy
            setTextDateAsDay(date:dateNotfy)
            dateNotfy = ""
        }else{
            lblDate.text = "\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))"
            
            print("lblDate.text \(lblDate.text!)")
            if dateFormatter.date(from: "\(year)-\(currentMonth)-\(String(format: "%02d", dayInDate))")!.timeIntervalSince1970 == dateFormatter.date(from:getDateOnly())!.timeIntervalSince1970 {
                dayAsText.text = "اليوم"
                lblDate.isHidden = true
                stackDateControl.insertArrangedSubview(dayAsText, at: 1)
            }else {
                lblDate.isHidden = false
                dayAsText.removeFromSuperview()
            }
        }
        draw()
        self.navigationController?.navigationBar.isTranslucent = false
        animationviewLeft(dataView:dataView, duration: 0.6)
        btnMealPalnnerOut.layer.cornerRadius = btnMealPalnnerOut.frame.height*0.5
        
        
        
    }
    func showCalender(label:UILabel){
        let showcalender = UITapGestureRecognizer.init(target: self, action: #selector(showCalendar))
        showcalender.numberOfTapsRequired = 1
        label.addGestureRecognizer(showcalender)
    }
    func TapGestureView(view:UIView){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    
    func TapGestureStackview(view:UIStackView){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        view.addGestureRecognizer(tapGesture)
        view.isUserInteractionEnabled = true
    }
    
    let progressView:UIProgressView = {
        var p = UIProgressView(progressViewStyle: .bar)
        p.trackTintColor = .red
        p.progressTintColor = .blue
        p.backgroundColor = .black
        return p
    }()
    
    func doSomething() {
        
        var continueForLoop = true
        
        DispatchQueue.global(qos: .userInteractive).async {
            for x in 0...100000 {
                print(x)
                DispatchQueue.main.sync {
                    //Update UI
                    
                    if continueForLoop {
                        // Show UIAlert
                        //Exit function
                        continueForLoop = false
                    }
                    
                }
            }
        }
    }
    
    func goToMealPannerPager() {
        
//        if  getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
//
//            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() <= Double(getUserInfo().expirDateSubscribtion)! {
//                getFirstRowUser()
//            }
//
//        }
//
//        let options = ViewPagerOptions()
//        var tabs = [ViewPagerTab]()
//
//        tabs = tabs1
//        options.tabType = .basic
//        options.distribution = .normal
//
//
//        let controller = TestViewController()
//        controller.options = options
//        controller.delegateMain = self
//        controller.tabs = tabs
//        controller.defualteIndex = defaultDisplayPageIndex
//        controller.pg = self
        if #available(iOS 13.0, *) {
//            self.present(controller, animated: true, completion: nil)
                        self.performSegue(withIdentifier: "mealPlanner1", sender: lblDate.text) // old
        }else{
//            self.navigationController?.pushViewController(controller, animated: true)
                        self.performSegue(withIdentifier: "mealPlanner", sender: lblDate.text) // old
        }
        
    }
    //1622937600000 /1622937600000
    var defaultDisplayPageIndex = 0
    
    func getFirstRowUser(){
        tabs1.removeAll()
        var indexDate = 0
        var firstItemReload = -1
        let txtDateDay = lblDate.text
        //        var lastItemReload = 0
        tblWajbatUserMealPlanner.getAllDate { [self] (items) in
            if items.isEmpty {
                return
            }
            if let index = items.firstIndex(where: { $0 == txtDateDay }) {
                indexDate = index
                print("Found at \(index)")
            }
            for date in indexDate...items.count-1 {
                //                 if items[date] == txtDateDay{
                defaultDisplayPageIndex = date // index
                for i in date-5...date-1 {
                    if i >= 0 {
                        if firstItemReload == -1 {
                            firstItemReload = i
                        }
                        if items[i] == txtDateDay {
                            print("here2",i)
                            defaultDisplayPageIndex = tabs1.count // index
                        }
                        let day = getDayOfWeek(items[i])
                        tabs1.append(ViewPagerTab(title: day ?? "", image: UIImage(named: "apple"),date: items[i]))
                        //                           viewControllers.append(GLPresentViewController.init(arr: arr,MainView:self, txtDateDay: items[i], pg: pg!, delegateMain: delegateMain))
                    }
                }
                
                for i in date...date+5 {
                    if firstItemReload == -1 {
                        firstItemReload = i
                    }
                    if items.count <= i {
                        return
                    }
                    if items[i] == txtDateDay {
                        print("here1",i)
                        defaultDisplayPageIndex = tabs1.count // index
                    }
                    let day = getDayOfWeek(items[i])
                    tabs1.append(ViewPagerTab(title: day ?? "", image: UIImage(named: "apple"),date: items[i]))
                    //                        lastItemReload = i
                }
                return
            }
            
            //             }
        }
    }
    func getDayOfWeek(_ date: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        guard let todayDate = formatter.date(from: date) else { return nil }
        let weekday = Calendar(identifier: .gregorian).component(.weekday, from: todayDate)
        
        switch Calendar.current.weekdaySymbols[weekday-1]  {
        case "Sunday":
            return "الأحد"
        case "Monday":
            return "الإثنين"
        case "Tuesday":
            return "الثلاثاء"
        case "Wednesday":
            return "الأربعاء"
        case "Thursday":
            return "الخميس"
        case "Friday":
            return "الجمعة"
        case "Saturday":
            return "السبت"
        default:
            return ""
        }
    }
    var tabs1 = [ViewPagerTab]()
    
    @IBAction func btnMealPlanner(_ sender: Any) {
//        let ob = setupRealm().objects(tblUserInfo.self)
//        try! setupRealm().write {
//            ob.setValue("-1", forKey: "subscribeType")
//            ob.setValue("0", forKey: "subDuration")
//            ob.setValue(0.0, forKey: "endSubMilli")
//            ob.setValue(0.0, forKey: "startSubMilli")
//            ob.setValue("0.0", forKey: "expirDateSubscribtion")
//
//        }
        
        
        //        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        //          AnalyticsParameterItemID: "banner-click-event-time-ios",
        //          AnalyticsParameterItemName: getDateOnly(),
        //
        //        ])
        
        
        
        //        return
        // Crashlytics.crashlytics()
        // fatalError()
        //        progressView.frame = CGRect(x: 10, y: 30, width: 200, height: 200)
        //
        //        var v = UIView(frame: CGRect(x: 10, y: 30, width: 200, height: 200))
        //        v.backgroundColor = .white
        //        v.addSubview(progressView)
        //
        //        view.addSubview(v)
        
        //        view.addSubview(progressView)
        //        progressView.setProgress(0.5, animated: false)
        //        for x in 0..<100 {
        //            DispatchQueue.main.asyncAfter(deadline: .now()+(Double(x)*0.25)) { [self] in
        //                progressView.setProgress(Float(x)/100, animated: true)
        //            }
        //        }
        //        return
        
        
        createEvent(key: "60", date: getDateTime())
        
        //        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 0 || getUserInfo().subscribeType == 4 {
        //            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() < Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
        //                createEvent(key: "60", date: getDateTime())
        //            }
        //        }
        
        if getDataFromSheardPreferanceString(key: "noInternet") == "1" { // if 1->not internet else found internet
            
            showAlert(str:  "لم نستطع التحقق من إشتراكك،يرجى الإتصال بشبكة الإنترنت")
            return
        }
        let countBefor = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).count
        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
            print(setupRealm().objects(tblWajbatUserMealPlanner.self))
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                goToMealPannerPager()
                return
            }
            
            if setupRealm().objects(tblWajbatUserMealPlanner.self).filter("date == %@",dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000))).isEmpty {
                
                let latest_day_generated = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).last
                
                var latest_day_generate:String!
                var increase:Int!
                var indexDay:Int!
                let dateFormatter = DateFormatter()
                let lastest_date_generate:Int!
                let date_expire:Int!
                dateFormatter.calendar = Calendar(identifier: .gregorian)
                dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
                if latest_day_generated == nil {
                    
                    if getDateOnly() == dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)) {
                        indexDay  = Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000))
                        print("indexDay=",indexDay!)
                    }else if dateFormatter.date(from: getDateOnly())! >  Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000){
                        indexDay =  -Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end:  Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)) * -1
                        print("indexDay>",indexDay!)
                        
                    }else if dateFormatter.date(from: getDateOnly())! <  Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000){
                        indexDay  = +Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)) * 1
                        print("indexDay<",indexDay!)
                        
                    }
                    print("indexDay",indexDay!)
                    latest_day_generate = Date().getDays(value: indexDay)
                    lastest_date_generate = indexDay
                    print("latest_day_generate",latest_day_generate!)
                    increase = 0
                    date_expire =  Date().daysBetween(start: dateFormatter.date(from: latest_day_generate)! , end: dateFormatter.date(from: dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000)))!)
                }else{
                    latest_day_generate = latest_day_generated!.date
                    increase = 1
                    lastest_date_generate =  Date().daysBetween(start: dateFormatter.date(from: dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)))! , end: dateFormatter.date(from:latest_day_generate)!)
                    date_expire =  Date().daysBetween(start: dateFormatter.date(from: latest_day_generate)! , end: dateFormatter.date(from: dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000)))!)-1
                }
                var first7Days:Int!
                
                
                print("lastest_date_generate",lastest_date_generate!)
                
                
                
                
                //                print("empty",lastest_date_generate+increase,latest_day_generate!,date_expire)
                
                let countAfter = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).count
                
                print(countAfter , countBefor)
                if dispatchWorkItem != nil {
                    if !dispatchWorkItem.isCancelled {
                        goToMealPannerPager()
                        return
                    }
                }
                //                else if countAfter != countBefor {
                //                   goToMealPannerPager()
                //                    return
                //                }
                let queue = DispatchQueue(label: "com.appcoda.myqueue")
                SVProgressHUD.show(withStatus: "جاري تحضير و جباتك")
                
                dispatchWorkItem = DispatchWorkItem {
                    //                queue.async {
                    //                DispatchQueue.global(qos: .userInitiated).async {
                    if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4{
                        
                        if first7Days != date_expire {
                            tblUserInfo.getMealDistrbutionUser()
                            print("new thread")
                            _ =  tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   lastest_date_generate+increase+first7Days+1,endPeriod:date_expire,date:getDateOnly())
                            //                            let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
                            //                            try! setupRealm().write{
                            //                                    setupRealm().add(newWjbah, update:.modified)
                            
                            //                            }
                            if dispatchWorkItem != nil {
                                print("finish generate and cancel thread ....")
                                dispatchWorkItem.cancel()
                            }
                        }
                    }
                    ActivationModel.sharedInstance.dispose()
                }
                
                queue.async {
                    
                    
                    if date_expire >= 7 {
                        first7Days = 6
                    }else if date_expire < 7 {
                        first7Days = date_expire
                    }else{
                        first7Days = date_expire
                    }
                    tblUserInfo.getMealDistrbutionUser()
                    _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:lastest_date_generate+increase,endPeriod:  lastest_date_generate+increase+first7Days,date:getDateOnly())
                    //                    let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
                    
                    SVProgressHUD.dismiss { [self] in
                        print("new")
                        //                        try! setupRealm().write{
                        //                                setupRealm().add(newWjbah, update:.modified)
                        //
                        //                        }
                        queue.async(execute: dispatchWorkItem)
                        goToMealPannerPager()
                    }
                }
                
                
            }else if getUserInfo().startSubMilli == 0.0 || getUserInfo().endSubMilli == 0.0 || getUserInfo().expirDateSubscribtion == "0.0" || getUserInfo().expirDateSubscribtion == ""  {
                print("case 6")
                SVProgressHUD.show(withStatus: "جاري التحقق من حالة الاشتراك")
                disableView(bool:false)
                verifySubscriptions([.autoRenewableMonthly,.autoRenewalEvery3Months,.autoRenewableYearly]) { [self] bool in // save date subcribe
                    if bool {
                        SVProgressHUD.dismiss()
                        updateMealPlanner()
                        print("case 12")
                    }else{
                        SVProgressHUD.dismiss()
                        disableView(bool:true)
                        goToMealPannerPager()
                        
                    }
                    //                    else {
                    //                        print("case 1")
                    //                        if setupRealm().objects(tblWajbatUserMealPlanner.self).filter("date == %@",getDateOnly()).isEmpty {
                    //                            print("case 2")
                    //                            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                    //                                print("case 3")
                    //                                goToMealPannerPager()
                    //                                return
                    //                            }
                    //                            print("case 4")
                    //                            generteMealPlanner()
                    //                        }else{
                    //                            print("case 5")
                    //                            goToMealPannerPager()
                    //
                    //                        }
                    //                        SVProgressHUD.dismiss()
                    //                    }
                }
            }
            else{
                //                print("case 7")
                //                if setupRealm().objects(tblWajbatUserMealPlanner.self).filter("date == %@",getDateOnly()).isEmpty {
                //                    print("case 8")
                //                    if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                //                        print("case 9")
                
                goToMealPannerPager()
                //                        return
                //                    }
                //                    print("case 10")
                //                    generteMealPlanner()
                //                }else{
                //                    print("case 11")
                //                    goToMealPannerPager()
                //
                //                }
            }
            
        }else if getUserInfo().subscribeType == 0 {
            SVProgressHUD.dismiss()
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                goToMealPannerPager()
                return
            }
            if setupRealm().objects(tblWajbatUserMealPlanner.self).filter("date == %@",getDateOnly()).isEmpty {
                generteMealPlanner()
            }else{
                goToMealPannerPager()
            }
        }else {
            
            tabBarController?.selectedIndex = 2
            self.tabBarController(self.tabBarController!, didSelect: (self.tabBarController!.viewControllers)![2]);
        }
    }
    func generteMealPlanner(){
        
        SVProgressHUD.show(withStatus: "جاري تحضير و جباتك")
        
        disableView(bool:false)
        tblUserInfo.getMealDistrbutionUser()
        tblUserInfo.generteAgainMealPlanner { [self] (s, e) in
//            if s != "failed" {
            DispatchQueue.main.async {
                goToMealPannerPager()
                disableView(bool:true)
                SVProgressHUD.dismiss()

            }
              
//            }
            
        }
    }
    func toAddWajbehSenfManual(id:String){
        if id  == "1" {
            createEvent(key: "1", date: getDateTime())
        }else  if id  == "2" {
            createEvent(key: "5", date: getDateTime())
        }else  if id  == "3" {
            createEvent(key: "6", date: getDateTime())
        }else  if id  == "4" {
            createEvent(key: "7", date: getDateTime())
        }else  if id  == "5" {
            createEvent(key: "8", date: getDateTime())
        }
        self.performSegue(withIdentifier: "addWajbehSenfManual", sender: id)
    }
    func toAddWajbehSenfManual(ob:KJOverlayTutorialViewController){ // for tutorial
        if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") != "1"{
            ob.close()
            self.dismiss(animated: true) {
                self.performSegue(withIdentifier: "addWajbehSenfManual", sender: "1")
            }
        }
    }
    //navigation drwar
    @IBAction func actShowMenu(_ sender: Any) {
        
    }
    
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        scroll.endEditing(true)
    //
    //        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    //
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func draw(){
        let shape = CAShapeLayer()
        shape.lineJoin = CAShapeLayerLineJoin.miter
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 360))
        path.addLine(to: CGPoint(x: view.bounds.width/2, y: 380))
        path.addLine(to: CGPoint(x: view.bounds.width, y: 360))
        path.addLine(to: CGPoint(x: view.bounds.width, y: 0))
        path.close()
        shape.path = path.cgPath
        shape.fillColor = UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0).cgColor
        muview.layer.mask = shape
        muview.layer.insertSublayer(shape, at: 0)
    }
    
    
    
    
    func showAlert(str:String){
        let alert = UIAlertController(title: "نعتذر", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}




extension ViewController:protocolWajbehInfoTracker{
    
    
    func refreshListItemTasbera2(itemTasbera2: tblUserWajbehEaten!) {
        self.ListEatenTasbera2.append(itemTasbera2)
        self.ListEatenTasbera2 = filterListWajbeh(old:ListEatenTasbera2)
        tblTasber2.reloadData()
    }
    
    func refreshListItemTasbera1(itemTasbera2: tblUserWajbehEaten!) {
        self.ListEatenTasbera1.append(itemTasbera2)
        self.ListEatenTasbera1 = filterListWajbeh(old:ListEatenTasbera1)
        tblTasber1.reloadData()
        
    }
    
    func refreshListItemBreakfast(itemTasbera2: tblUserWajbehEaten!) {
        self.ListEatenBreakfast.append(itemTasbera2)
        self.ListEatenBreakfast = filterListWajbeh(old:ListEatenBreakfast)
        tblBreakfast.reloadData()
    }
    
    func refreshListItemDinner(itemTasbera2: tblUserWajbehEaten!) {
        self.ListEatenDinner.append(itemTasbera2)
        self.ListEatenDinner = filterListWajbeh(old:ListEatenDinner)
        tblDinner.reloadData()
    }
    
    func refreshListItemLaunsh(itemTasbera2: tblUserWajbehEaten!) {
        self.ListEatenLaunsh.append(itemTasbera2)
        self.ListEatenLaunsh = filterListWajbeh(old:ListEatenLaunsh)
        tblLaunsh.reloadData()
    }
    
    func refreshListItemTamreen(itemTamreen: tblUserTamreen!) {
        self.ListTamreenBurned.append(itemTamreen)
        self.ListTamreenBurned = filterListTamreen(old:ListTamreenBurned)
        tblTamreen.reloadData()
    }
    func refrechData(){
        DiplayDailyData()
    }
    
    func filterListWajbeh(old:[tblUserWajbehEaten])->[tblUserWajbehEaten]{
        let new = old.filter { obj in
            if obj.date == lblDate.text {
                return true
            }
            return false
        }
        return new
    }
    
    func filterListTamreen(old:[tblUserTamreen])->[tblUserTamreen]{
        let new = old.filter { obj in
            if obj.date == lblDate.text {
                return true
            }
            return false
        }
        return new
    }
    func setUPViewEnable(view:UIView){
        view.isUserInteractionEnabled = true
        view.alpha = 1
    }
    
    func setUPViewDisable(view:UIView){
        view.isUserInteractionEnabled = false
        view.alpha = 0.5
    }
    
}

extension ViewController:ItemsViewList{
    func setBreakfdast(ListEatenBreakfast: [tblUserWajbehEaten]!) {
        self.ListEatenBreakfast = ListEatenBreakfast
        if ListEatenBreakfast.isEmpty {
            tblMealDistribution.getUserBreakfastMealDistribution(brekfast: viewBreakfast)
        }else{
            setUPViewEnable(view:viewBreakfast)
        }
        DispatchQueue.main.async {
            self.tblBreakfast.reloadData()
        }
    }
    
    func setLuansh(ListEatenLaunsh: [tblUserWajbehEaten]!) {
        self.ListEatenLaunsh = ListEatenLaunsh
        if ListEatenLaunsh.isEmpty {
            tblMealDistribution.getUserLunchMealDistribution(luansh: viewLaunch)
        }else{
            setUPViewEnable(view:viewLaunch)
        }
        DispatchQueue.main.async {
            self.tblLaunsh.reloadData()
        }
    }
    
    func setDinner(ListEatenDinner: [tblUserWajbehEaten]!) {
        self.ListEatenDinner = ListEatenDinner
        if ListEatenDinner.isEmpty {
            tblMealDistribution.getUserDinnerMealDistribution(dinner: viewDinner)
        }else{
            setUPViewEnable(view:viewDinner)
        }
        DispatchQueue.main.async {
            self.tblDinner.reloadData()
        }
    }
    
    func setTasber1(ListEatenTasbera1: [tblUserWajbehEaten]!) {
        self.ListEatenTasbera1 = ListEatenTasbera1
        if ListEatenTasbera1.isEmpty {
            tblMealDistribution.getUserSnack1MealDistribution(tasber1: viewTasber1)
        }else{
            setUPViewEnable(view:viewTasber1)
        }
        DispatchQueue.main.async {
            self.tblTasber1.reloadData()
        }
    }
    
    func setTasber2(ListEatenTasbera2: [tblUserWajbehEaten]!) {
        self.ListEatenTasbera2 = ListEatenTasbera2
        if ListEatenTasbera2.isEmpty {
            tblMealDistribution.getUserSnack2MealDistribution(tasber2: viewTasberh2)
        }else{
            setUPViewEnable(view:viewTasberh2)
        }
        DispatchQueue.main.async {
            self.tblTasber2.reloadData()
        }
    }
    func setTamreen(ListTamreen: [tblUserTamreen]!) {
        self.ListTamreenBurned = ListTamreen
        DispatchQueue.main.async {
            self.tblTamreen.reloadData()
        }
    }
    
}


protocol goToViewWajbeh {
    func toViewWajbeh(parsData:[String])
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    static var today:  Date { return Date().dayToday }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayToday: Date {
        return Calendar.current.date(byAdding: .day, value: 0, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
    func getDays(value:Int)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from:Calendar.current.date(byAdding: .day, value: value, to: noon)!)
    }
    func getDays1(value:Int)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: dateFormatter.string(from:Calendar.current.date(byAdding: .day, value: value, to: noon)!))!
    }
    func getDaysDate(value:Int)->Double{
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: dateFormatter.string(from:Calendar.current.date(byAdding: .day, value: value, to: noon)!))!.timeIntervalSince1970 * 1000.0.rounded()
    }
    
    func daysBetween(start: Date, end: Date) -> Int {
        
        return Calendar.current.dateComponents([.day], from: start, to:end).day!
        
    }
}
extension ViewController:viewAccount{
    func setNewTarget(val: String) {
        print("val \(val)")
        viewPopuSlider(new_target:val)
    }
    
    func setAlpha(flag:Bool) {
        if flag == true {
            tblUserInfo.updateGrownUser(grown: "1") { (str,err) in
                setDataInSheardPreferance(value: "1", key: "UserAgeID")
            }
            obpresnterAccount.showDialog()
            _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,ob:obpresnterAccount, user_ProgressHistory: setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@",getUserInfo().id).first!)
        }
        view.alpha = 1
        viewWillAppear(true)
        
    }
    
    
}



extension ViewController:UITabBarControllerDelegate {
    func selectItemWithIndex(value: Int) {
        selectTap2 = true
        self.tabBarController!.selectedIndex = value;
        self.tabBarController(self.tabBarController!, didSelect: (self.tabBarController!.viewControllers)![value]);
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        print(tabBarController.selectedIndex)
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 1 {
            createEvent(key: "81", date: getDateTime())
        }else  if tabBarIndex == 2 {
            createEvent(key: "80", date: getDateTime())
            let vc = viewController as! DietsVC
            vc.tblview.reloadData()
            vc.delegatViewController = self
            
            if selectTap2 {
                vc.showList = true
                selectTap2 = false
            }else{
                vc.showList = false
                selectTap2 = false
            }
            
        }
        else if tabBarIndex == 3 {
            createEvent(key: "82", date: getDateTime())
            
        }else if tabBarIndex == 4 {
            createEvent(key: "83", date: getDateTime())
            
        }
    }
}



extension ViewController {
    
    
    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>,responsEHendler:@escaping (Bool)->Void){
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { [self] result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            print("verifyReceipt ",result)
            var arrPaymentHistory =  [[String:Any]]()
            switch result {
            case .success(let receipt):
                let all_latest_local_productID = setupRealm().objects(tblPaymentHistory.self) .filter("refUserID == %@",getUserInfo().id).last
                
                
                appBundleId = all_latest_local_productID?.product_id ?? "-1"
                
                if appBundleId == "-1" {
                    responsEHendler(false)
                    return
                }
                
                let productIds = Set(purchases.map { _ in appBundleId })
                let result = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                var pending_renewal_info =  receipt["pending_renewal_info"] as! [[String:AnyObject]]
                
                pending_renewal_info = pending_renewal_info.filter { item in
                    if pending_renewal_info[0]["product_id"] as! String == appBundleId {
                        return true
                    }
                    return false
                }
                if pending_renewal_info.isEmpty {
                    SVProgressHUD.dismiss()
                    obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                    responsEHendler(false)
                    return
                }
                switch result {
                case .purchased(let expiryDate, let items):
                    
                    let receipt_info  = receipt["receipt"] as! [String:AnyObject]
                    
                    
                    var arr_receipt = receipt_info["in_app"] as! [[String:AnyObject]]
                    let sortedArray = arr_receipt.sorted(by: { ($0["purchase_date_ms"]?.doubleValue)! > ($1["purchase_date_ms"]?.doubleValue)! })
                    arr_receipt = sortedArray
                    print("arr_receipt",arr_receipt)
                    let laest_local_transaction = Array(setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@ AND transactionID == %@",getUserInfo().id,"\( arr_receipt.first!["transaction_id"]!)"))
                    
                    let all_latest_local_transaction:[tblPaymentHistory]? = Array(setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@",getUserInfo().id))
                    
                    
                    print("latestTrancation_Date","\(String(describing: arr_receipt.first?["original_transaction_id"]))")
                    
                    
                    //                    laest_local_transaction.append(tblPaymentHistory())
                    
                    
                    arrPaymentHistory.append(["status": "1", "datetime": "\( arr_receipt.first!["purchase_date"]!)", "amount": all_latest_local_transaction?[0].amount ?? "null", "transactionID": "\( arr_receipt.first!["transaction_id"]!)", "description": "renewal","iosOrginalTransactionID": "\( arr_receipt.first!["original_transaction_id"]!)","iosOrginalDateTime": "\( arr_receipt.first!["original_purchase_date"]!)","iosOrginalDescription":"renewal","isFirstTimeSubscription":"0","subscriptionDuration":all_latest_local_transaction?[0].subscriptionDuration ?? "null","productId": pending_renewal_info[0]["product_id"] ?? "","purchaseToken":"null"])
                    
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
                    dateFormatter1.calendar = Calendar(identifier: .gregorian)
                    dateFormatter1.timeZone = TimeZone(secondsFromGMT: 0)
                    dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
                    
                    
                    dateFormatter.calendar = Calendar(identifier: .gregorian)
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    
                    let d = dateFormatter1.date(from: dateFormatter1.string(from: expiryDate))
                    
                    let newFormate = dateFormatter.string(from: d!)
                    
                    let DateStartSubsc =  dateFormatter1.date(from: arr_receipt.first!["original_purchase_date"] as! String)
                    
                    let purchase_date = dateFormatter.date(from: dateFormatter.string(from: DateStartSubsc!))
                    
                    if getDateOnly() == dateFormatter.string(from: purchase_date!) {
                        ActivationModel.sharedInstance.startSubMilli  = Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!)
                        
                    }else
                    if dateFormatter.date(from: getDateOnly())! > purchase_date!{
                        ActivationModel.sharedInstance.startSubMilli  =  -Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!) * -1
                        
                        
                    }else if dateFormatter.date(from: getDateOnly())! < purchase_date!{
                        ActivationModel.sharedInstance.startSubMilli  = +Date().daysBetween(start:dateFormatter.date(from: getDateOnly())!, end: purchase_date!) * 1
                        
                    }
                    
                    print("startSubMilli",ActivationModel.sharedInstance.startSubMilli)
                    
                    if Date().daysBetween(start:purchase_date!, end: dateFormatter.date(from: newFormate)!) >= 31 {
                        ActivationModel.sharedInstance.endSubMilli = 31
                    }else{
                        ActivationModel.sharedInstance.endSubMilli = 31 //Int(Double(Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)))
                    }
                    
                    if appBundleId == "1" {
                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
                        ActivationModel.sharedInstance.subDuration = 30
                        
                    }else if appBundleId == "3" {
                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 90).timeIntervalSince1970 * 1000.0.rounded()
                        ActivationModel.sharedInstance.subDuration = 90
                        
                        
                    }else {
                        ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 365).timeIntervalSince1970 * 1000.0.rounded()
                        ActivationModel.sharedInstance.subDuration = 365
                        
                    }
                    //                    ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)
                    
                    //                    ActivationModel.sharedInstance.expireDate = dateFormatter.date(from: newFormate)!.timeIntervalSince1970 * 1000.0.rounded()
                    
                    print("ActivationModel.sharedInstance.expireDate", ActivationModel.sharedInstance.expireDate,ActivationModel.sharedInstance.endSubMilli,newFormate)
                    
                    ActivationModel.sharedInstance.endPeriod = 30//Date().daysBetween(start: purchase_date!, end: dateFormatter.date(from: newFormate)!)-1
                    if ActivationModel.sharedInstance.endPeriod < 0 {
                        ActivationModel.sharedInstance.endPeriod = 0
                    }
                    ActivationModel.sharedInstance.subscribeType = 1
                    
                    if !laest_local_transaction.isEmpty { // check if transactionID exite or not
                        
                        obAPiSubscribeUser.updateUserInfoPayment()
                        responsEHendler(true)
                        return
                    }
                    APiSubscribeUser.setInfoPayment(obj: arrPaymentHistory)
                    obAPiSubscribeUser.updateUserInfoPayment()
                    print("\(productIds) is valid until \(expiryDate)\n\(items)")
                    disableView(bool: true)
                    
                    responsEHendler(true)
                    
                case .expired(let expiryDate, let items):
                    if pending_renewal_info[0]["is_in_billing_retry_period"] != nil {
                        if pending_renewal_info[0]["is_in_billing_retry_period"] as? String == "1" {
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
                            dateFormatter.calendar = Calendar(identifier: .gregorian)
                            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            let grace_period_expires_date = dateFormatter1.date(from: pending_renewal_info[0]["grace_period_expires_date"] as! String)
                            print("gracePeriod")
                            ActivationModel.sharedInstance.expireDate = grace_period_expires_date!
                                .timeIntervalSince1970 * 1000.0.rounded()
                            ActivationModel.sharedInstance.subscribeType = 4 // grace pierod
                            ActivationModel.sharedInstance.startPeriod = 0
                            ActivationModel.sharedInstance.endPeriod = 30//Date().daysBetween(start: getDateOnlyasDate(), end: grace_period_expires_date!)-1
                            if ActivationModel.sharedInstance.endPeriod < 0 {
                                ActivationModel.sharedInstance.endPeriod = 0
                            }
                            ActivationModel.sharedInstance.endSubMilli =  31//Date().daysBetween(start: getDateOnlyasDate(), end: grace_period_expires_date!)
                            ActivationModel.sharedInstance.startSubMilli = 0
                            
                            //                            ActivationModel.sharedInstance.subDuration =
                            //                                ActivationModel.sharedInstance.endSubMilli
                            if appBundleId == "1" {
                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
                                ActivationModel.sharedInstance.subDuration = 30
                                
                            }else if appBundleId == "3" {
                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 90).timeIntervalSince1970 * 1000.0.rounded()
                                ActivationModel.sharedInstance.subDuration = 90
                                
                                
                            }else {
                                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 365).timeIntervalSince1970 * 1000.0.rounded()
                                ActivationModel.sharedInstance.subDuration = 365
                                
                            }
                            arrPaymentHistory.append(["status": "1", "datetime": "\( grace_period_expires_date ?? getDateOnlyasDate())", "amount": "", "transactionID": "", "description": "grace_period","iosOrginalTransactionID": "","iosOrginalDateTime": "\( grace_period_expires_date ?? getDateOnlyasDate())","iosOrginalDescription":"grace_period","isFirstTimeSubscription":"0","subscriptionDuration": "\(ActivationModel.sharedInstance.endSubMilli)","productId": pending_renewal_info[0]["product_id"] ?? "","purchaseToken":"null"])
                            
                            APiSubscribeUser.setInfoPayment(obj: arrPaymentHistory)
                            obAPiSubscribeUser.updateUserInfoPayment()
                            updateMealPlanner()
                        }
                    }
                    if pending_renewal_info[0]["expiration_intent"] as? String == "1" {
                        // The customer voluntarily canceled their subscription.
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:3)
                    }else if pending_renewal_info[0]["auto_renew_status"] as? String == "0" {
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                    }else{
                        // expiryDate
                        obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:2)
                    }
                    disableView(bool: true)
                    print("\(productIds) is expired since \(expiryDate)\n\(items)")
                    responsEHendler(false)
                case .notPurchased:
                    print("\(productIds) has never been purchased")
                    responsEHendler(false)
                }
            case .error:
                disableView(bool: true)
                SVProgressHUD.dismiss()
                showAlert(str: "حدث مشكلة لم نستطع التحقق من الاشتراك")
                print("error")
            }
        }
    }
    
    
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "02d69ba2f957418c80ae8fb699376187")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }
}



extension ViewController {
    
    func updateMealPlanner(){
        
        
        
        getMealDistrbutionUser()
        SVProgressHUD.show()
        obAPiSubscribeUser.subscribeUser(mealDitr:Int(getUserInfo().mealDistributionId)!,flag: 1) { [self] (bool) in
            if bool {
                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
                let queue = DispatchQueue(label: "com.appcoda.myqueue")
                dispatchWorkItem = DispatchWorkItem {
                    //                queue.async {
                    if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                        _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod+7,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
                    }
                    if dispatchWorkItem != nil {
                        print("finish generate and cancel thread ....")
                        dispatchWorkItem.cancel()
                    }
                    ActivationModel.sharedInstance.dispose()
                }
                queue.async {
                    _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod: 6,date:getDateOnly())
                    SVProgressHUD.dismiss {
                        disableView(bool:true)
                        queue.async(execute: dispatchWorkItem)
                        goToMealPannerPager()
                    }
                }
                
                //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                //                    tblPackeg.GenerateMealPlannerFree7Days(date:getDateOnly()) { (err) in
                //                        SVProgressHUD.dismiss {
                //                            ActivationModel.sharedInstance.dispose()
                //                            disableView(bool:true)
                //                            goToMealPannerPager()
                //                        }
                //                    }
                //                }
            }else{
                SVProgressHUD.dismiss()
            }
            
        }
        func getMealDistrbutionUser(){
            
            let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
            if item?.isLunchMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "2"
            }else if item?.isDinnerMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "3"
            }else if item?.isBreakfastMain == 1 {
                ActivationModel.sharedInstance.refWajbehMain = "1"
            }
            if item?.Breakfast == 1 && item?.isBreakfastMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("1")
            }
            if item?.Dinner == 1 && item?.isDinnerMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("3")
            }
            if item?.Lunch == 1 && item?.isLunchMain == 0 {
                ActivationModel.sharedInstance.refWajbehNotMain.append("2")
            }
            if item?.Snack1 == 1 {
                ActivationModel.sharedInstance.refTasbera.append("4")
            }
            if item?.Snack2 == 1 {
                ActivationModel.sharedInstance.refTasbera.append("5")
            }
            ActivationModel.sharedInstance.subscribeType =  getUserInfo().subscribeType
            
            ActivationModel.sharedInstance.startSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Int(getUserInfo().startSubMilli) / 1000)))
            
            ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refCuiseseID))
            
            ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refAllergyInfoID))
            
            ActivationModel.sharedInstance.endSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Int(getUserInfo().endSubMilli) / 1000)))
            
            ActivationModel.sharedInstance.refMeat = getUserInfo().meat.split(separator: ",").map { String($0) }
            ActivationModel.sharedInstance.refFruit = getUserInfo().frute.split(separator: ",").map { String($0) }
            ActivationModel.sharedInstance.refVegetables = getUserInfo().vegetabels.split(separator: ",").map { String($0) }
            
        }
    }
}



extension ViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}

extension UIView {
    
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
extension ViewController : CustomAlertViewDelegate {
    func okButtonTapped(selectedOption: String, textFieldValue: String) {
        print("cancelButtonTapped $$$ \(textFieldValue)")
        if textFieldValue == "1" || textFieldValue == "2" {
            goToAppStore()
        }else if textFieldValue == "-1" {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    func cancelButtonTapped() { // go to cv payment
        print("cancelButtonTapped ... ")
    }
}
extension ViewController {
    func customShowAlert(m1:String,m2:String,m3:String,m4:String,flag:Int){
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "GeneralAlertView2") as! GeneralAlertView2
        
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.cancelBtnMessage = m1
        customAlert.okBtnMessage = m2
        customAlert.noteMessage = m3
        customAlert.titltMessage = m4
        customAlert.flag = flag
        customAlert.pd = self
        view.isUserInteractionEnabled = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        
        self.present(customAlert, animated: true, completion: nil)
        
    }
    
    //    func customShowAlert1(m1:String,m2:String,m3:String,m4:String){
    //        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "GeneralAlertView3") as! GeneralAlertView3
    //        customAlert.providesPresentationContextTransitionStyle = true
    //        customAlert.definesPresentationContext = true
    //        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    //        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    //        customAlert.delegate = self
    //        customAlert.btn1_titlt = m1
    //        customAlert.btn2_titlt = m2
    //        customAlert.btn3_titlt = m3
    //        customAlert.btn4_titlt = m4
    //        customAlert.pd = self
    //        view.isUserInteractionEnabled = false
    //        self.tabBarController?.tabBar.isUserInteractionEnabled = false
    //
    //        self.present(customAlert, animated: true, completion: nil)
    //
    //    }
}
