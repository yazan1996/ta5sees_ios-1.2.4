//
//  FrequentlyAskedQuestionsVC.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 17/06/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit

//class FrequentlyAskedQuestionsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
extension AdvViewControllers : UITableViewDelegate,UITableViewDataSource {


     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let question = twoDimensionalArray[section].question
        let button = UIButton(type: .system)
        button.setTitle(question, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 0.2)
        button.titleLabel?.numberOfLines = 20
        button.titleLabel?.font = UIFont(name: "GE Dinar One", size: 15.0)
        button.titleLabel?.textAlignment = .right
        button.titleLabel?.lineBreakMode = .byWordWrapping;
        button.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        
        button.tag = section
        
        return button
    }
    
    @objc func handleExpandClose(button: UIButton) {
        print("Trying to expand and close section...")
        let section = button.tag
        let question = twoDimensionalArray[section].question

        if section == twoDimensionalArray.count-1 {
            var bottomOffset1:CGPoint!
            bottomOffset1 = CGPoint(x: 0, y:scroll.contentSize.height - scroll.bounds.height + scroll.contentInset.bottom+50)
            scroll.setContentOffset(bottomOffset1, animated: true)
        }
     
        // we'll try to close the section first by deleting the rows
        var indexPaths = [IndexPath]()
        for row in twoDimensionalArray[section].answer.indices {
            print(0, row)
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = twoDimensionalArray[section].isExpanded
        twoDimensionalArray[section].isExpanded = !isExpanded
        
        button.setTitle(question, for: .normal)
        
        if isExpanded {
            tableView.deleteRows(at: indexPaths, with: .fade)
        } else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return twoDimensionalArray.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !twoDimensionalArray[section].isExpanded {
            return 0
        }
        
        return twoDimensionalArray[section].answer.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//           if section % 2 == 0 {
               return 2
//           }
//           return 0
       }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//            if section % 2 == 0 {
                let headerView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                headerView.backgroundColor = .white
                return headerView
//            }
//            return nil
        }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let name = twoDimensionalArray[indexPath.section].answer[indexPath.row]
        
        cell.textLabel?.text = name
        cell.textLabel?.numberOfLines = 20
        cell.textLabel?.lineBreakMode = .byWordWrapping
        cell.textLabel?.font = UIFont(name: "GE Dinar One", size: 15.0)
        cell.textLabel?.textAlignment = .right
        if showIndexPaths {
            cell.textLabel?.text = "\(name)   Section:\(indexPath.section) Row:\(indexPath.row)"
        }
        
        return cell
    }

}





struct ExpandableNames {
    
    var isExpanded: Bool
    let answer: [String]
    let question: String
 
}



