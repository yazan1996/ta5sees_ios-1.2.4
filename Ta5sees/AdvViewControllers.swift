
//
//   AdvViewControllers.swift
//   Ta5sees
//
//   Created by TelecomEnterprise on 05/05/2021.
//   Copyright © 2021 Telecom enterprise. All rights reserved.


import UIKit
import SVProgressHUD
import SwiftyStoreKit
import Firebase
import FirebaseAnalytics

enum RegisteredPurchase: String {
    
    case nonRenewingPurchase
    case autoRenewalEvery3Months
    case autoRenewableMonthly
    case autoRenewableYearly
    
}


struct modelFeture {
    var name:String!
    var found:String!
    var notFound:String!
    
    init(name:String,found:String,notFound:String) {
        self.name = name
        self.found = found
        self.notFound = notFound
    }
}
class AdvViewControllers: UIViewController {
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 1,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )
    
    //    var arrFeaturs:[modelFeture] = [modelFeture(name: "المزايا",found: "المجانية",notFound: "المدفوعة"),modelFeture(name: "حساب السعرات الحرارية",found: "",notFound: ""),modelFeture(name: "متابعة السعرات الحرارية",found: "",notFound: "")]
    
    
    var arrFeaturs =
        ["المزايا","المجانية","المدفوعة","حساب السعرات الحرارية","check.png","check.png","متابعة السعرات الحرارية","check.png","check.png","مراقبة الوزن","check.png","check.png","متابعة شرب الماء","check.png","check.png","متابعة التمارين الرياضية","check.png","check.png","الدردشة مع خبير تغذية معتمد","check.png","check.png","النظام الغذائي الشهري","clear.png","check.png","سلة التسوق","clear.png","check.png","اقتراح وجبات طعام يوميا","clear.png","check.png","اقتراح وجبات من مطابخنا العربية","clear.png","check.png","الوجبات حول العالم","500","1500+","وجبات الكيتو","100","500+","أصناف","1000","4000+","برنامج تذكير للوجبات","clear.png","check.png","برنامج تذكير شرب الماء","clear.png","check.png"]
    
    
    
    lazy var  obAPiSubscribeUser = APiSubscribeUser(with: self)
    let ageChild = getDataFromSheardPreferanceFloat(key: "dateInYears")
    var count = 0
    var tee:Float!
    var energyTotal = 0
    var DateStartSubsc:Date!
    var appBundleId =  ""
    let autoRenewableIsAtomic = true
    var subcribeType = 0
    var subscribeDirect = false
    var hideFreeTrialBTN = false
    var filterList = [tblWajbatUserMealPlanner]()
    var filteringPackge = [tblShoppingList]()
    var CVHome:ViewController!
    @IBOutlet var txtPrice3Month: UILabel!//3 month
    @IBOutlet var btnFreeTrial: UIButton!//3 month
    
    @IBOutlet var txtPrice12Month: UILabel!
    @IBOutlet var txtPrice1Month: UILabel!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var collectionView: UICollectionView!
    
    let cellId = "cellId123123"
    
    @IBOutlet weak var tableView: SelfSizedTableView!
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    var twoDimensionalArray = [
        ExpandableNames(isExpanded: false, answer: ["نعم يمكن ذلك اذا كنت مشترك عن طريق متجر ابل، لالغاء الاشتراك اتبع الخطوات التالية:\n1) افتح الاعدادات الهاتف\n2) انقر على معلومات الحساب\n3) اختر الاشتراكات\n4) اختر الاشتراك الذي تريد الغاؤه\n5) اضغط الغاء الاشتراك\n6) تابع مع الخطوات الموجودة على الشاشة"], question: "هل استطيع الغاء الاشتراك الخاص بي؟"),
        ExpandableNames(isExpanded: false, answer: ["جميع اشتراكات تطبيق تخسيس تجدد تلقائيا لتجنب انقطاع الخدمة، الا اذا تم الغاء ميزة التجديد التلقائي من لائحة الدفع لدى متجر ابل، عندها سيتم وقف الخدمة عند موعد الدفع التالي."], question: "هل الاشتراك يدفع لمرة واحدة او يجدد تلقائي؟"),
        ExpandableNames(isExpanded: false, answer: ["حسب الحزمة التي اختارها العميل عند الاشتراكات، مثلا اذا كانت حزمة شهر واحد سيتم خصم شهر واحد فقط في كل عملية دفع، اما اذا كان الاشتراك سنوي سيتم خصم قيمة الاشتراك كامل على طول الفترة."], question: "هل الاشتراكات تخصم شهريا او سنويا على تطبيق تخسيس؟"),
    ]
    
    var showIndexPaths = false
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: "02d69ba2f957418c80ae8fb699376187")
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
        
    }
    
    func verifySubscriptions(_ purchases: Set<RegisteredPurchase>) {
        
        NetworkActivityIndicatorManager.networkOperationStarted()
        verifyReceipt { [self] result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            
            switch result {
            case .success(let receipt):
                print("step000008")
                let productIds = Set(purchases.map { _ in appBundleId })
                print("productIds",productIds)
                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                self.showAlert1(self.alertForVerifySubscriptions(purchaseResult, productIds: productIds))
            //                self.alertForVerifySubscriptions(purchaseResult, productIds: productIds)
            case .error:
                print("step000004")
                view.isUserInteractionEnabled = true
                window?.alpha = 1
                window?.isOpaque = false
                self.showAlert(self.alertForVerifyReceipt(result))
            }
        }
    }
    
    
    func purchase(atomically: Bool) {
        
        SVProgressHUD.show()
        view.isUserInteractionEnabled = false
        window?.isOpaque = true
        window?.alpha = 0.5
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        SwiftyStoreKit.purchaseProduct(appBundleId, atomically: true) { [self] result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            print("result",result)
            
            if case .success(let purchase) = result {
                
                obAPiSubscribeUser.updateSubscribeTypeUser(subscribeType:1)
                
                //Deliver content from server, then:
                if purchase.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                
                
            }
            if let alert = self.alertForPurchaseResult(result) {
                print("alert",alert)
                print("step000001")
                SVProgressHUD.dismiss()
                view.isUserInteractionEnabled = true
                window?.isOpaque = false
                window?.alpha = 1
                self.showAlert(alert)
            }
        }
        
    }
    
    @IBAction func btnFreeTrial(_ sender: Any) {
        
        subcribeType = 0
        if getUserInfo().grown == "2"{
            calculateNormalDietChild()
            SVProgressHUD.dismiss()
            print("Child")
        }else{
            calculateNormalDietAdult()
            SVProgressHUD.dismiss()
            print("Adult")
        }
        
    }
    @IBOutlet weak var adv3: UIView!
    @IBOutlet weak var adv2: UIView!
    @IBOutlet weak var adv1: UIView!
    @IBOutlet weak var stackAdv: UIStackView!
    @IBOutlet weak var txtPrivcyPloicy: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITextView.appearance().linkTextAttributes = [ .foregroundColor: UIColor.blue ]
        
        txtPrivcyPloicy.isSelectable = true
        txtPrivcyPloicy.dataDetectorTypes = .link
        
        let attrtbued = NSAttributedString.makeHyperlink(for: "https://www.apple.com/legal/internet-services/itunes/dev/stdeula/", in: txtPrivcyPloicy.text, as: "Privacy policy & Terms of service")
        
        txtPrivcyPloicy.attributedText = attrtbued
        
        collectionView?.collectionViewLayout = columnLayout
        collectionView?.contentInsetAdjustmentBehavior = .always
        //        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        btnFreeTrial.titleLabel?.textAlignment = .center
        tableView.maxHeight = 1000
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        if setupRealm().isInWriteTransaction {
            setupRealm().cancelWrite()
        }
        /*
         /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/telecomenterprise/Desktop/telecom_enterprise-ta5sees_ios-9a490305e97c/Ta5sees/GoogleService-Info.plist -p ios /Users/telecomenterprise/Downloads/appDsyms-4
         */
        
        if hideFreeTrialBTN {
            stackAdv.arrangedSubviews[0].isHidden = true
        }
        if getUserInfo().subscribeType != -1 {
            stackAdv.arrangedSubviews[0].isHidden = true
        }
        
        if getUserInfo().subscribeType != -1 && getUserInfo().subscribeType != 0 {
            subcribeType = 1
        }
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap1Month(gesture:)))
        adv1.addGestureRecognizer(tapGesture)
        
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(didTap3Month(gesture:)))
        adv2.addGestureRecognizer(tapGesture3)
        
        
        let tapGesture12 = UITapGestureRecognizer(target: self, action: #selector(didTap12Month(gesture:)))
        adv3.addGestureRecognizer(tapGesture12)
       
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //
        SVProgressHUD.show()
        DispatchQueue.global(qos: .background).async { [self] in
            
            // load your data here
            setupButton()
            DispatchQueue.main.async { [self] in
                // reload your collection view here:
                view.isUserInteractionEnabled = true
                imgBackground (imgname:"PayB3",myview:adv1)
                imgBackground (imgname:"PayB4",myview:adv2)
                imgBackground (imgname:"PayB5",myview:adv3)
            }
        }
        
    }
    func showAlert(){
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "GeneralAlertView3") as! GeneralAlertView3
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.appBundelID = appBundleId
        customAlert.arrayString = ["تخسيس Premium Subscription (1, 3, and 12 months):","• The subscription period will automatically renew unless auto-renew is turned off at least 24 hours before the end of the current subscription period.","• To turn this function off, simply go to your iTunes account and turn off auto-renew.","• Renewal payments will differ depending on subscription and pricing at the time of renewal.","• Your iTunes account will be charged when the purchase is confirmed.","• Terms and Privacy Policy: (https://www.apple.com/legal/internet-services/itunes/dev/stdeula/)"]
        self.present(customAlert, animated: true, completion: nil)
        
    }
    
    /*
     

      To turn this function off, simply go to your iTunes account and turn off auto-renew. Renewal payments will differ depending on subscription and pricing at the time of renewal. Your iTunes account will be charged when the purchase is confirmed.

     Terms and Privacy Policy: http://api.lifesum.com/mobile-terms
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    func setupButton(){
        NetworkActivityIndicatorManager.networkOperationStarted()
        DispatchQueue.main.async { [self] in
            view.isUserInteractionEnabled = false
        }
        SwiftyStoreKit.retrieveProductsInfo(["3","12","1"]) { [self] result in
            NetworkActivityIndicatorManager.networkOperationFinished()
            let products = Array(result.retrievedProducts)
            print("products",products.count)
            if result.error?.localizedDescription != nil {
                view.isUserInteractionEnabled = true
                SVProgressHUD.dismiss()
                return
            }
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            for product in products {
                print("####",product.productIdentifier)
                
                if product.productIdentifier == "12" {
                    formatter.locale = product.priceLocale
                    let cost = formatter.string(from: product.price)
                    txtPrice12Month.text = cost
                }else if product.productIdentifier == "1" {
                    formatter.locale = product.priceLocale
                    let cost = formatter.string(from: product.price)
                    txtPrice1Month.text = cost
                }else if product.productIdentifier == "3" {
                    
                    formatter.locale = product.priceLocale
                    let cost = formatter.string(from: product.price)
                    txtPrice3Month.text = cost
                }
                
            }
            SVProgressHUD.dismiss()
            //self.showAlert(self.alertForProductRetrievalInfo(result))
        }
        
    }
    @objc func didTap1Month(gesture: UIGestureRecognizer) {
        print("You clicked on Monthly")
        appBundleId = "1"
        if txtPrice1Month.text == nil || appBundleId == "" {
            print("missing data")
            return
        }
//        showAlert()

        createEvent(key: "31", date: getDateTime())
        subcribeType = 1
        purchase(atomically: autoRenewableIsAtomic)
    }
    
    @objc func didTap3Month(gesture: UIGestureRecognizer) {
        print("You clicked on 3Months")
        appBundleId = "3"
        if txtPrice3Month.text == nil  || appBundleId == "" {
            print("missing data")
            return
        }
//        showAlert()

        createEvent(key: "31", date: getDateTime())
        subcribeType = 1
        purchase(atomically: autoRenewableIsAtomic)
        
    }
    
    
    @objc func didTap12Month(gesture: UIGestureRecognizer) {
        print("You clicked on Yearly")
        appBundleId = "12"
        if txtPrice12Month.text == nil || appBundleId == "" {
            print("missing data")
            return
        }
//        showAlert()

        createEvent(key: "31", date: getDateTime())
        subcribeType = 1
        purchase(atomically: autoRenewableIsAtomic)
    }
    
    func imgBackground (imgname:String,myview:UIView) {
        DispatchQueue.main.async {
            let myLayer = CALayer()
            let myImage = UIImage(named: imgname)?.cgImage
            myLayer.frame = CGRect(x: 0, y: 0,  width: myview.bounds.size.width, height:  myview.bounds.size.height)
            myLayer.contents = myImage
            myview.layer.addSublayer(myLayer)
            myview.layer.insertSublayer(myLayer, at: 0)
        }
    }
    
    @IBAction func buDismiss(_ sender: Any) {
        view.isUserInteractionEnabled = true
        dismiss(animated: true, completion: nil)
    }
    
    func sntDataToFirebase(subcribeType:Int){
  
        _ = DispatchQueue(label: "swiftlee.serial.queue")
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        ActivationModel.sharedInstance.startSubMilli = 0
        
        if subcribeType == 0 { // free
            print("subcribeType == 0")
            ActivationModel.sharedInstance.subscribeType = 0
            ActivationModel.sharedInstance.startPeriod = 0
            ActivationModel.sharedInstance.endPeriod = 6
            ActivationModel.sharedInstance.endSubMilli = 6
            ActivationModel.sharedInstance.expireDate = Date().getDaysDate(value: 6)
            ActivationModel.sharedInstance.subDuration = 7
        }
        

        self.obAPiSubscribeUser.subscribeUser(mealDitr: ActivationModel.sharedInstance.TeeDistribution,flag: 1) { (bool) in
            if bool {
                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
              
                let queue = DispatchQueue(label: "com.appcoda.myqueue")
                
                dispatchWorkItem = DispatchWorkItem {
                    //                queue.async {
                    if subcribeType == 1 {
                        
                        _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod+7,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
                      
                    }
                    if dispatchWorkItem != nil {
                        print("finish generate and cancel thread ....")
                        dispatchWorkItem.cancel()
                        DispatchQueue.main.async {
                            self.view.isUserInteractionEnabled = true
                        }
                    }
                    ActivationModel.sharedInstance.dispose()
                    
                }
                
                queue.async {
                    DispatchQueue.main.async {
                        self.view.isUserInteractionEnabled = false
                        
                    }
                    _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod:  6,date:getDateOnly())
                    SVProgressHUD.dismiss {
                        self.view.isUserInteractionEnabled = true
                        window?.alpha = 1
                        window?.isOpaque = false
                        queue.async(execute: dispatchWorkItem)
                        self.performSegue(withIdentifier: "finishActicationProcces", sender: self) //sub1
                    }
                }
            }else{
                Ta5sees.alert(mes: "حدث خطا،يرجى المحالة مجددا", selfUI: self)
                SVProgressHUD.dismiss()
            }
        }
    }
    func calculateNormalDietAdult(){
        var bmr:Float
        if getUserInfoProgressHistory().refGenderID == "1" {
            
            bmr = Float(24.0 * Double(ActivationModel.sharedInstance.weight)!)
        }  else{
            bmr = Float(0.9 * 24.0 * Double(ActivationModel.sharedInstance.weight)!)
        }
        
        let pa:Float = Float(getRatePA(id:Int(ActivationModel.sharedInstance.refLayaqa)!)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        if getUserInfoProgressHistory().refGenderID == "2" && ActivationModel.sharedInstance.dietType == "4" || ActivationModel.sharedInstance.dietType == "5" {
            if getDataFromSheardPreferanceString(key: "LactationMonths") == "1"  && ActivationModel.sharedInstance.dietType == "5"{
                energyTotal = Int(round(tee + 330))
            }
            else if getDataFromSheardPreferanceString(key: "LactationMonths") == "2" && ActivationModel.sharedInstance.dietType == "5"{
                energyTotal = Int(round(tee + 440))
            } else if getDataFromSheardPreferanceString(key: "Pregnant") == "3" && ActivationModel.sharedInstance.dietType == "4"{
                energyTotal = Int(round(tee  + 452))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "2"  && ActivationModel.sharedInstance.dietType == "4"{
                energyTotal = Int(round(tee  + 350))
            }else if getDataFromSheardPreferanceString(key: "Pregnant") == "1" && ActivationModel.sharedInstance.dietType == "4" {
                energyTotal = Int(round(tee))
                
            }else {
                energyTotal = Int(round(tee))
                
            }
        }else {
            energyTotal = processDietOrderForTEEAdulte(tee:tee, incrementKACL: Float(ActivationModel.sharedInstance.incrementKACL)!)
            
        }
        setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
        setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
        setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(ActivationModel.sharedInstance.dietType)!)
        
    }
    
    func processDietOrderForTEEAdulte(tee:Float,incrementKACL:Float)->Int{
        if ActivationModel.sharedInstance.dietType == "2"  {
            return Int(round(tee - incrementKACL))
        }else if ActivationModel.sharedInstance.dietType == "1"   {
            return Int(round(tee + incrementKACL))
        }else if ActivationModel.sharedInstance.dietType == "3"   {
            return Int(round(tee))
        }else if ActivationModel.sharedInstance.dietType == "loss" {
            print(Int(round(tee - incrementKACL)))
            return Int(round(tee - incrementKACL))
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "gain" {
            return Int(round(tee + incrementKACL))
            
        }else if getDataFromSheardPreferanceString(key: "subDiet") == "maintain" {
            return Int(round(tee))
            
        }
        return 1
    }
    
    func calculateNormalDietChild(){
        if ageChild >= 1.0 && ageChild <= 3.0{
            print("****0")
            let sub1 = (89.0 * (ActivationModel.sharedInstance.weight as NSString).floatValue - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= 4.0 && ageChild <= 8.0 && getUserInfoProgressHistory().refGenderID == "1" {
            print("****1")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= 4.0 && ageChild <= 8.0 && ageChild <= tblChildAges.range2EndTo(id:2)  && getUserInfoProgressHistory().refGenderID == "2" {
            print("****2")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= 9  && ageChild <= 16 && getUserInfoProgressHistory().refGenderID == "1" {
            print("****3")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
            
        }else if ageChild >= 9  && ageChild <= 16 && ageChild <= tblChildAges.range3EndTo(id:3)  && getUserInfoProgressHistory().refGenderID == "2"{
            print("****4")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }else {
            print("****5")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }
        
        energyTotal = processDietOrderForTEEChild(tee:tee)
        print("tee \(tee ?? 0.0) - energyTotal \(energyTotal)")
        
        //        }
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(ActivationModel.sharedInstance.dietType)!)
    }
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let age = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let sum1 = v2 * age
        let sum2 = (v3 * (ActivationModel.sharedInstance.weight as NSString).floatValue)
        let sum5 = (((getUserInfoProgressHistory().height as NSString).floatValue/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(getRatePA(id:Int(ActivationModel.sharedInstance.refLayaqa)!)) * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        print(sum4)
        return sum4
    }
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let bmi = ActivationModel.sharedInstance.BMI
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: Float(bmi) ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value <= 25 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = getUserInfoProgressHistory().refGenderID
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if getUserInfo().grown == "1"  { // adulte
            return Float(ob.PAadult)
        } else{
            if gender == "1"  { // childe male
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
        }
    }
    func getTeeDistribution(energy:Int){
        var ref = Int(ActivationModel.sharedInstance.dietType)
        
        if ActivationModel.sharedInstance.xtraCusine.contains("8") {
            ref = 8
        }
     
        var ob = tblTEEDistribution()
        if getUserInfo().grown == "1" { // adulte
            ob.getTeeDistrbution(refID: ref!,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{ // childe
            ob.getTeeDistrbution(refID: ref!,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
    }
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)
        
        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        sheardPreferanceWrite(fat: Float(fat),pro: Float((pro)),carbo: Float(carbo),energy: Float(energy))
        
    }
    
    func getservMenuPlan(id:Int){
        
        var ob = tblMenuPlanExchangeSystem()
        if getUserInfo().grown == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }
        
        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.dietType, key: "dietType")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.target_weight, key: "targetValue")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.week, key: "week")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.incrementKACL, key: "incrementKACL")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.refLayaqa, key: "refLayaqaCondID")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.weight, key: "txtweight")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.xtraCusine.joined(separator: ","), key: "cusinene")
        setDataInSheardPreferance(value:ActivationModel.sharedInstance.refAllergys.joined(separator: ","), key: "allergy")
        
        setDataInSheardPreferance(value:getUserInfo().id, key: "mobNum")
        
        setDataInSheardPreferance(value:getUserInfo().loginType, key: "loginType")
        
        runNotfyWajbeh()
    }
}



extension AdvViewControllers:ApiResponseDaelegat{
    
    func showIndecator() {
        SVProgressHUD.show()
    }
    
    func HideIndicator() {
        SVProgressHUD.dismiss()
    }
    
    func getResponseSuccuss() {}
    
    func getResponseFalueir(meeage:String) {
        let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getResponseMissigData() {}
    
    func runNotfyWajbeh(){
        var selctionID = "0"
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let notfy = ActivationModel.sharedInstance.refMealDistrbution
        print("notfy",notfy)
        tblAlermICateItems.disableAllNotfy()
        for item in notfy {
            if item.selected {
                if !item.checkAsMain {
                    selctionID = "2"
                }else{
                    selctionID = "1"
                }
                //                print("item.time_food",item.id,item.time_food_date_str!)
                let time = selectDate(date:getTimeAsDateType(time:item.time_food_date_str))
                showNotify(id:item.id,body:getInfoNotfy(id:item.id),hour:time[0],minut:time[1])
                tblAlermICateItems.updateAlerttimeNotificationActivation(cateItem:item.id,hour:time[0],min:time[1])
                ActivationModel.sharedInstance.refmealPlaners.append(["id":Int(item.id!)!,"selectionId" :Int(selctionID)!,"time": item.time_food_date_str!])
            }else{
                ActivationModel.sharedInstance.refmealPlaners.append(["id":Int(item.id!)!,"selectionId" :0,"time": item.time_food_date_str!])
            }
            
        }
        sntDataToFirebase(subcribeType:subcribeType)
        
    }
    
    func selectDate(date:Date)->[Int]{
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        
        print("TIME",hour,minute)
        return [hour,minute]
    }
    func getInfoNotfy(id:String) ->String {
        switch id {
        case "1":
            return   getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
        case "2":
            return getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
        case "3":
            return getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
        case "4":
            return getTitleNotify(keyRemote:"snack1RemindersTitles",keyValue:"snack1Title",sheardKey:"snack1TitleCounter")
        case "5":
            return getTitleNotify(keyRemote:"snack2RemindersTitles",keyValue:"snack2Title",sheardKey:"snack2TitleCounter")
        default:
            return ""
        }
    }
}





// MARK: User facing alerts
extension AdvViewControllers {
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    func showAlert1(_ alert: UIAlertController) {
        
    }
    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = product.localizedPrice!
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        } else if let invalidProductId = result.invalidProductIDs.first {
            return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
        } else {
            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
            return alertWithTitle("Could not retrieve product info", message: errorString)
        }
    }
    
    // swiftlint:disable cyclomatic_complexity
    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("step000002")
            window?.alpha = 1
            window?.isOpaque = false
            createEvent(key: "32", date: getDateTime())
//            if getUserInfo().subscribeType == 1 {
//                return alertWithTitle("Purchase Status", message: "You are already subscribed")
//            }
            print("purchased :\(purchase.transaction.transactionState)")
            print("purchase result:\(result)")
            
            //                if "\(purchase.transaction.transactionState)" != "purchased" {
            var arr =  [[String:Any]]()
            
            var duration = purchase.product.localizedSubscriptionPeriod
            print("duration",duration)
            if duration == "1 mth" {
                duration = "30"
            }else if duration == "3 mths" {
                duration = "90"
            }else if duration == "1 yr"{ //1 yr
                duration = "365"
            }else {
                duration = "0"
            }
            // isFirstTimeSubscription field (1 => if first time, 0 => if renewal hit).
            // status (0: fail, 1: success).
            arr.append(["status": "1", "datetime": "\(purchase.transaction.transactionDate!)", "amount": "\(purchase.product.price)", "transactionID": "\(purchase.transaction.transactionIdentifier!)", "description": "\(purchase.transaction.transactionState)","iosOrginalTransactionID": "\(purchase.originalTransaction?.transactionIdentifier ?? purchase.transaction.transactionIdentifier!)","iosOrginalDateTime": "\(purchase.originalTransaction?.transactionDate! ?? purchase.transaction.transactionDate!)","iosOrginalDescription":"\(purchase.originalTransaction?.transactionState ?? purchase.transaction.transactionState)","isFirstTimeSubscription":"1","subscriptionDuration":duration,"productId": purchase.productId,"purchaseToken":"null"])
            //transactionDate -> start date
            
            ActivationModel.sharedInstance.startSubMilli = 0//Int( purchase.transaction.transactionDate!.timeIntervalSince1970 * 1000.0.rounded())
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            
            let d = dateFormatter1.date(from: dateFormatter1.string(from: purchase.transaction.transactionDate!))
            
            DateStartSubsc = dateFormatter.date(from: dateFormatter.string(from: d!))
            
            
            
            print("DateStartSubsc ",DateStartSubsc!)
            
            
            APiSubscribeUser.setInfoPayment(obj: arr)
            
            if appBundleId == "1" {
                verifySubscriptions([.autoRenewableMonthly])
            }else if appBundleId == "3" {
                verifySubscriptions([.autoRenewalEvery3Months])
            }else if appBundleId == "12" {
                verifySubscriptions([.autoRenewableYearly])
            }
            
            print("Purchase Success: \(purchase.productId)")
            return nil
        case .error(let error):
            view.isUserInteractionEnabled = true
            window?.alpha = 1
            window?.isOpaque = false
            print("step000003")
            print("Purchase Failed: \(error)")
            SVProgressHUD.dismiss()
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                return nil
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
            default:
                return alertWithTitle("Purchase failed", message: (error as NSError).localizedDescription)
            }
        }
    }
    
    
    
    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
        
        switch result {
        case .success(let receipt):
            print("Verify receipt Success: \(receipt)")
            return alertWithTitle("Receipt verified", message: "Receipt verified remotely")
        case .error(let error):
            print("Verify receipt Failed: \(error)")
            switch error {
            case .noReceiptData:
                return alertWithTitle("Receipt verification", message: "No receipt data. Try again.")
            case .networkError(let error):
                return alertWithTitle("Receipt verification", message: "Network error while verifying receipt: \(error)")
            default:
                return alertWithTitle("Receipt verification", message: "Receipt verification failed: \(error)")
            }
        }
    }
    
    
    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) -> UIAlertController {
        
        switch result {
        case .purchased(let expiryDate, let items):
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            
            let d = dateFormatter1.date(from: dateFormatter1.string(from: expiryDate))
            
            let newFormate = dateFormatter.string(from: d!)
            
            
            
            if Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!) >= 31 {
                ActivationModel.sharedInstance.endSubMilli = 31
                ActivationModel.sharedInstance.endPeriod = 30
            }else{
                
                //                ActivationModel.sharedInstance.endSubMilli = 31
                //                ActivationModel.sharedInstance.endPeriod = 30
                
                ActivationModel.sharedInstance.endSubMilli = Int(Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!))
                ActivationModel.sharedInstance.endPeriod = Int(Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!))-1
                
                if ActivationModel.sharedInstance.endPeriod < 0 {
                    ActivationModel.sharedInstance.endPeriod = 0
                }
                print("endSubMilli",ActivationModel.sharedInstance.endSubMilli)
            }
            
            ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!)
            
            //            if appBundleId == "1" {
            //                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
            //                ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: DateStartSubsc, end: Date().getDays1(value: 30))
            //
            //            }else if appBundleId == "3" {
            //                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 90).timeIntervalSince1970 * 1000.0.rounded()
            //                ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: DateStartSubsc, end: Date().getDays1(value: 90))
            //
            //
            //            }else {
            //                ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 365).timeIntervalSince1970 * 1000.0.rounded()
            //                ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: DateStartSubsc, end: Date().getDays1(value: 365))
            //
            //            }
            
            ActivationModel.sharedInstance.expireDate = dateFormatter.date(from: newFormate)!.timeIntervalSince1970 * 1000.0.rounded()
            
            //             ActivationModel.sharedInstance.expireDate = Date().getDays1(value: 30).timeIntervalSince1970 * 1000.0.rounded()
            
            
            ActivationModel.sharedInstance.startPeriod = 0
            print("expireDate",newFormate,ActivationModel.sharedInstance.expireDate)
            
            //            ActivationModel.sharedInstance.endPeriod = Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!)
            ActivationModel.sharedInstance.subscribeType = 1
            
            print("daysBetween expire , start",Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!))
            
            print("\(productIds) is valid until \(String(describing: dateFormatter.date(from: newFormate)))\n\(items)\n",Int(expiryDate.timeIntervalSince1970 * 1000.0 .rounded()))
            if !subscribeDirect {
                print("HRHRS")
                if getUserInfo().grown == "2"{
                    print("Child")
                    
                    calculateNormalDietChild()
                    SVProgressHUD.dismiss()
                }else{
                    print("Adult")
                    calculateNormalDietAdult()
                    SVProgressHUD.dismiss()
                }
                APiSubscribeUser.apiSubscribeUser().updateUserInfoPayment()
                view.isUserInteractionEnabled = true
                window?.alpha = 1
                window?.isOpaque = false
                return alertWithTitle("Product is purchased", message: "Product is valid until \(newFormate)")
            }else{
                // genertat
                print("genertat >>>")
                filterList = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).filter { (item) -> Bool in
                    if dateFormatter.date(from: item.date)!.timeIntervalSince1970 >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                        return true
                    }
                    return false
                }
                
                filteringPackge = setupRealm().objects(tblShoppingList.self).filter("userID == %@",getUserInfo().id).filter { (item) -> Bool in
                    if  item.date >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                        return true
                    }
                    return false
                }
                ActivationModel.sharedInstance.setData()
                APiSubscribeUser.apiSubscribeUser().updateUserInfoPayment()
                SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
                    tblPackeg.GenerateMealPlannerFree7Days(date:getDateOnly()) { [self] (err) in
                        SVProgressHUD.dismiss {
                            
                            try! setupRealm().write{
                                print("@9")
                                setupRealm().delete(filterList)
                            }
                            try! setupRealm().write{
                                setupRealm().delete(filteringPackge)
                                
                            }
                            dismiss(animated: true, completion: nil)
                            if subscribeDirect {
                                if CVHome != nil {
                                    CVHome.setupBanner()
                                }
                            }
                            //                        self.performSegue(withIdentifier: "finishActicationProcces", sender: self) //sub1
                            //                        ActivationModel.sharedInstance.dispose()
                        }
                    }
                }
                
                return alertWithTitle("Product is purchased", message: "Product is valid until \(newFormate)")
            }
            
        case .expired(let expiryDate, let items):
            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
            view.isUserInteractionEnabled = true
            window?.alpha = 1
            window?.isOpaque = false
            SVProgressHUD.dismiss()
            return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
        case .notPurchased:
            print("\(productIds) has never been purchased")
            view.isUserInteractionEnabled = true
            window?.alpha = 1
            window?.isOpaque = false
            SVProgressHUD.dismiss()
            return alertWithTitle("Not purchased", message: "This product has never been purchased")
        }
    }
    
    
    //    func alertForVerifySubscriptions(_ result: VerifySubscriptionResult, productIds: Set<String>) {
    //
    //        switch result {
    //        case .purchased(let expiryDate, let items):
    //
    //            dateFormatter.dateFormat = "yyyy-MM-dd"
    //            dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'zzz"
    //            dateFormatter.calendar = Calendar(identifier: .gregorian)
    //            dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    //            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    //
    //            let d = dateFormatter1.date(from: dateFormatter1.string(from: expiryDate))
    //
    //            let newFormate = dateFormatter.string(from: d!)
    //
    //
    //
    //            if Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!) >= 31 {
    //                ActivationModel.sharedInstance.endSubMilli = 31
    //            }else{
    //                ActivationModel.sharedInstance.endSubMilli = Int(Double(Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!)))
    //
    //                print("endSubMilli",ActivationModel.sharedInstance.endSubMilli)
    //            }
    //            ActivationModel.sharedInstance.subDuration = Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!)
    //
    //            ActivationModel.sharedInstance.expireDate = dateFormatter.date(from: newFormate)!.timeIntervalSince1970 * 1000.0.rounded()
    //
    //            print("expireDate",newFormate,ActivationModel.sharedInstance.expireDate)
    //
    //            ActivationModel.sharedInstance.endPeriod = Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!)-1
    //            ActivationModel.sharedInstance.subscribeType = 1
    //            if  ActivationModel.sharedInstance.endPeriod < 0 {
    //                ActivationModel.sharedInstance.endPeriod = 0
    //            }
    //
    //            print("daysBetween expire , start",Date().daysBetween(start: DateStartSubsc, end: dateFormatter.date(from: newFormate)!))
    //
    //            print("\(productIds) is valid until \(String(describing: dateFormatter.date(from: newFormate)))\n\(items)\n",Int(expiryDate.timeIntervalSince1970 * 1000.0 .rounded()))
    //            if !subscribeDirect {
    //                print("subscribeDirect ***")
    //                if getUserInfoProgressHistory().grown == "2"{
    //                    print("Child")
    //
    //                    calculateNormalDietChild()
    //                    SVProgressHUD.dismiss()
    //                }else{
    //                    print("Adult")
    //                    calculateNormalDietAdult()
    //                    SVProgressHUD.dismiss()
    //                }
    //                APiSubscribeUser.apiSubscribeUser().updateUserInfoPayment()
    //                //                return alertWithTitle("Product is purchased", message: "Product is valid until \(newFormate)")
    //            }else{
    //                // genertat
    //
    //                filterList = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfoProgressHistory().id).filter { (item) -> Bool in
    //                    if dateFormatter.date(from: item.date)!.timeIntervalSince1970 >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
    //                        return true
    //                    }
    //                    return false
    //                }
    //
    //                filteringPackge = setupRealm().objects(tblShoppingList.self).filter("userID == %@",getUserInfoProgressHistory().id).filter { (item) -> Bool in
    //                    if  item.date > dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
    //                        return true
    //                    }
    //                    return false
    //                }
    //                ActivationModel.sharedInstance.setData()
    //                SVProgressHUD.show(withStatus: "جاري تهيئة وجباتك")
    //                APiSubscribeUser.apiSubscribeUser().updateUserInfoPayment()
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
    //                    tblPackeg.GenerateMealPlannerFree7Days(date:getDateOnly()) { [self] (err) in
    //                        SVProgressHUD.dismiss {
    //
    //                            try! setupRealm().write{
    //                                print("@9")
    //                                setupRealm().delete(filterList)
    //                            }
    //                            try! setupRealm().write{
    //                                setupRealm().delete(filteringPackge)
    //
    //                            }
    //                            print("yazanhere")
    //                            dismiss(animated: true, completion: nil)
    //                            //                        self.performSegue(withIdentifier: "finishActicationProcces", sender: self) //sub1
    //                            //                        ActivationModel.sharedInstance.dispose()
    //                        }
    //                    }
    //                }
    //                //                return alertWithTitle("Product is purchased", message: "Product is valid until \(newFormate)")
    //            }
    //
    //        case .expired(let expiryDate, let items):
    //            print("\(productIds) is expired since \(expiryDate)\n\(items)\n")
    //        //            return alertWithTitle("Product expired", message: "Product is expired since \(expiryDate)")
    //        case .notPurchased:
    //            print("\(productIds) has never been purchased")
    //        //            return alertWithTitle("Not purchased", message: "This product has never been purchased")
    //        }
    //    }
}


extension AdvViewControllers:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 48
    }
    
    //     func numberOfSections(in collectionView: UICollectionView) -> Int {
    //
    //        return arrFeaturs.count
    //    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeatureCell",for: indexPath) as? FeatureCell else {
            return FeatureCell()
        }
        let item = arrFeaturs[indexPath.row]
        cell.backgroundColor = UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 0.2)// UIColor.init(red: 245/255, green: 251/245, blue: 245/255, alpha: 1)//UIColor.darkGray
        //        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2  {
        //            cell.txtFeature?.textColor = UIColor.white
        
        //        } else {
        cell.txtFeature?.textColor = UIColor.black
        //        }
        cell.txtFeature?.lineBreakMode = .byWordWrapping
        cell.txtFeature?.numberOfLines = 10
        
        if item.contains(".png") {
            
            let attachment = NSTextAttachment()
            
            if #available(iOS 13.0, *) {
                if item == "check.png" {
                    attachment.image = UIImage(named: item)?.withTintColor(UIColor(hexString: "#00CC66"))
                }else{
                    attachment.image = UIImage(named: item)?.withTintColor(.red)
                }
            } else {
                attachment.image = UIImage(named: item)?.withRenderingMode(.alwaysTemplate)
            }
            let attachmentString = NSAttributedString(attachment: attachment)
            let myString = NSMutableAttributedString(string: "")
            myString.append(attachmentString)
            cell.txtFeature?.attributedText = myString
            
        }else{
            cell.txtFeature?.text = item
            
        }
        return cell
        
        
    }
    
    
    
}

class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        
        itemSize = CGSize(width: itemWidth, height: 40)
        
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
}
extension AdvViewControllers {
    var autoRenewableSubscription: RegisteredPurchase {
        switch  appBundleId {
        case "3": return .autoRenewalEvery3Months
        case "1": return .autoRenewableMonthly
        case "12": return .autoRenewableYearly
        default: return .autoRenewableMonthly
        }
    }
}
extension AdvViewControllers:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
            "date": date
        ])
    }
}



extension AdvViewControllers:CustomAlertViewDelegate1 {
    func okButtonTapped1(selectedOption: String, textFieldValue: String) {
            createEvent(key: "31", date: getDateTime())
            subcribeType = 1
            purchase(atomically: autoRenewableIsAtomic)
    }
    
    func cancelButtonTapped1() {
        
    }
    

    
}
