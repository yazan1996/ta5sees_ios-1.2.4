//
//  WWViewController.swift
//  Ta5sees
//
//  Created by Admin on 6/17/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit

class TypeDietController: UIViewController,UIViewControllerTransitioningDelegate {

    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    let array:[String] = ["حمية المحافظة على الوزن","حمية زيادة الوزن","حمية تقليل الوزن"]
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    
    @IBOutlet weak var navigation: NavigationViewController!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        customButtom(btn:btnmaintains)
        customButtom(btn:btnloss)
        customButtom(btn:btnGain)
        customButtom(btn:DietWheat)
        customButtom(btn:LactatingDiet)
        customButtom(btn:DietPatientsKindney)
        customButtom(btn:DietGout)
        customButtom(btn:DietHBlood)
        customButtom(btn:DietHFat)
        inComplete(btn:Diabetics)
        inComplete(btn:DietRenal)
        customButtom(btn:DietPWomen)
        refPlanMaster = -1
        refSubPlanMater = -1
        AgeUser = -1

    }
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var DietWheat: UIButton!
    @IBOutlet weak var LactatingDiet: UIButton!
    @IBOutlet weak var DietPatientsKindney: UIButton!
    @IBOutlet weak var DietGout: UIButton!
    @IBOutlet weak var DietHBlood: UIButton!
    @IBAction func DietHBloodPressure(_ sender: Any) {

    
    }
    @IBOutlet weak var DietHFat: UIButton!
    @IBOutlet weak var Diabetics: UIButton!
    @IBOutlet weak var DietRenal: UIButton!
    @IBOutlet weak var DietPWomen: UIButton!
    @IBOutlet weak var btnloss: UIButton!
    @IBOutlet weak var btnGain: UIButton!
    
    @IBOutlet weak var btnmaintains: UIButton!
    func customButtom(btn:UIButton){
        btn.layer.borderWidth = 3
        btn.layer.borderColor = UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0).cgColor
        btn.layer.cornerRadius = 30
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.font =  UIFont.init(name: "GE Dinar One", size: 18.0)
        btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        effectButton(sender: btn)
    }
    func inComplete(btn:UIButton){
        btn.layer.borderWidth = 3
        btn.layer.borderColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
        btn.layer.cornerRadius = 30
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.font =  UIFont.init(name: "GE Dinar One", size: 18.0)
        btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        effectButton(sender: btn)
        
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if let dis=segue.destination as?  RegesterStepOne{
//
//            if  let Food=sender as? String {
//                dis.ID=Food
//            }
//        }
//    }
    @objc func buttonAction(sender: UIButton!) {
        let btnsendtag: UIButton = sender
        if btnsendtag.tag == 1 || btnsendtag.tag  == 2 || btnsendtag.tag  == 3 || btnsendtag.tag  == 17 || btnsendtag.tag  == 4{
            EnterViewController(btn:btnsendtag)
        }else {
            EnterViewController2(btn:btnsendtag)
        }
    }
    
    func EnterViewController(btn:UIButton){
        let detailView = storyboard!.instantiateViewController(withIdentifier: "regester2") as! RegesterStepOne
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
        refPlanMaster =  btn.tag
        self.performSegue(withIdentifier: "regester2", sender: nil)
    }
    func EnterViewController2(btn:UIButton){
        let detailView = storyboard!.instantiateViewController(withIdentifier: "subTypeDiet") as! SubTypeDietController
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
        refPlanMaster =  btn.tag
        self.performSegue(withIdentifier: "subTypeDiet", sender: nil)
    }
    @IBAction func buDismiss(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
//    func dropCustom(pickertitle:String,btn:UIButton!,placeholder:String){
//        let greenColor =  UIColor(red: 132.0/255.0, green: 255.0/255.0, blue: 185.0/255.0, alpha: 1)
//
//        let greenAppearance = YBTextPickerAppearanceManager.init(
//            pickerTitle         : pickertitle,
//            titleFont           : boldFont,
//            titleTextColor      : .white,
//            titleBackground     : greenColor,
//            searchBarFont       : regularFont,
//            searchBarPlaceholder: "بحث",
//            closeButtonTitle    : "الغاء",
//            closeButtonColor    : .red,
//            closeButtonFont     : regularFont,
//            doneButtonTitle     : "موافق",
//            doneButtonColor     : greenColor,
//            doneButtonFont      : boldFont,
//            checkMarkPosition   : .Right,
//            itemCheckedImage    : UIImage(named:"green_ic_checked"),
//            itemUncheckedImage  : UIImage(named:"green_ic_unchecked"),
//            itemColor           : .black,
//            itemFont            : regularFont
//        )
//
//        let Cusisne = array
//        let picker = YBTextPicker.init(with: Cusisne, appearance: greenAppearance,
//                                       onCompletion: { (selectedIndexes, selectedValues) in
//                                        if selectedValues.count > 0{
//                                            var values = [String]()
//                                            for index in selectedIndexes{
//                                                values.append(Cusisne[index])
//                                            }
//                                            if pickertitle == "الطعام" {
//                                                self.txtcusineneID = values
//                                            }else{
//                                                self.txtallergyID = values
//                                            }
//
//
//
//                                            btn.setTitle(values.joined(separator: ", "), for: .normal)
//
//
//                                        }else{
//                                            btn.setTitle(placeholder, for: .normal)
//                                        }
//        },
//                                       onCancel: {
//                                        print("Cancelled")
//        }
//        )
//
//        if let title = btn.title(for: .normal){
//            if title.contains(","){
//                picker.preSelectedValues = title.components(separatedBy: ", ")
//            }
//        }
//        picker.allowMultipleSelection = true
//
//        picker.show(withAnimation: .Fade)
//    }
}

