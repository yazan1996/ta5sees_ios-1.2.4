//
//  HomeChatViewController.swift
//  Ta5sees
//
//  Created by Admin on 8/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import UIKit
import Firebase
import AudioToolbox
import AVFoundation
import FirebaseDatabase
import Firebase
import FirebaseMessaging
import FirebaseAnalytics
import FirebaseCore
import FirebaseInstanceID
import FirebaseAuth
//import CodableFirebase
import FirebaseFunctions
import SwiftyJSON
class HomeChatViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate,KeyboardHandler {
    var bottomInset: CGFloat = 55.0
    
    func pushTo(viewController: UIViewController, str: String) {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: str, style: .done, target: self, action: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @IBOutlet weak var btnRecoredVoice: UIButton!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer : AVAudioPlayer!
    var isPlayedAudio = false
    var user_id:String!
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @IBOutlet weak var tableView: UITableView!
    let UserName="sendername" // TODO: will update soon
    @IBOutlet weak var txtText: UITextField!
    //    var ref=DatabaseReference.init()
    var ListMessages=Array<Message>()
    var flag = 0
    var count = 0
    var count_item = 0
    var obAudioDelegate:AudioDelegate!
    var lastContentOffset: CGFloat = 0
    lazy var obviewPresnterMessage=viewPresnterMessage(with: self,viewCv: view)
    var userData:tblUserInfo!
    var falgSound = false
    var imageRecored = UILabel()
    lazy var functions = Functions.functions()
    var isOpenController = 0
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imageRecored = UILabel(frame:  CGRect(x: 40,y: 465, width: 200 , height: 40))
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if flag == 0 {
            if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
                if indexPath == lastVisibleIndexPath {
                    if !ListMessages.isEmpty {
                        scrollToBottom()
                        flag = 1
                    }
                }
            }
        }
    }
    func setImageRecord(){
        
        imageRecored.text = "إضغط مطولا للتسجيل"
        imageRecored.backgroundColor = colorBasicApp
        tableView.alpha = 0.4
        imageRecored.alpha = 1
        imageRecored.layer.cornerRadius = 50
        imageRecored.textColor = .white
        imageRecored.textAlignment = .center
        imageRecored.font = UIFont(name: "GE Dinar One", size: 20.0)!
        imageRecored.center = tableView.center
        view.addSubview(imageRecored)
    }
    @objc  func showResetMenu(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            setImageRecord()
            startRecording()
            
        }else if  gestureRecognizer.state == .ended {
            
            finishRecording()
            scrollToBottom()
            tableView.alpha = 1
            imageRecored.removeFromSuperview()
            imageRecored.text = nil
            //            setUpActivityIndicatory(uiView: view)
            print("ended long press")
        }
    }
    func indexPathForPreferredFocusedView(in tableView: UITableView) -> IndexPath? {
        tableView.setNeedsFocusUpdate()
        tableView.updateFocusIfNeeded()
        return  IndexPath(item: ListMessages.count-1, section: 0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        obviewPresnterMessage.getUserData()
        createTopic()
        if (UIApplication.shared.topViewController() as? HomeChatViewController) != nil {
            isOpenController = 1
            print("Messages viewcontroller is visible and open")
            
        } else {
            isOpenController = 0
            print("Messages viewcontroller isnt visible and not open")
        }
        addKeyboardObservers() { [weak self] state in
            guard state! else { return }
            self!.scrollToBottom()
        }
        self.HideKeyboard()
        setupRecoredAudio()
        setUpActivityIndicatory(uiView: view)
        tableView.remembersLastFocusedIndexPath = true
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(showResetMenu(_:)))  //Long function will call when user long press on button.
        longGesture.minimumPressDuration = 0.5
        btnRecoredVoice.addGestureRecognizer(longGesture)
        
        txtText.layer.cornerRadius = 9
        txtText.layer.borderWidth = 1
        txtText.layer.borderColor = colorBasicApp.cgColor
        
        
        //        ref=Database.database().reference()
        tableView.dataSource=self
        tableView.delegate=self
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 250;
        //        changeListner()
        LoadMessages(number:10)
        updateListner()
        setNameUser(name:userData.firstName)
        
    }
    
    func setupRecoredAudio(){
        recordingSession = AVAudioSession.sharedInstance()
        obAudioDelegate = self
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print(" sucusses to Permission!")
                        //                        self.loadRecordingUI()
                    } else {
                        print(" failed to Permission!")
                    }
                }
            }
        } catch {
            print(" failed to Permission!")
            // failed to record!
        }
    }
    func createTopic(){
        var id:String!
        if userData.loginType == "4" {
            id = userData.id.replacingOccurrences(of: ".", with: "_")
        }else{
            id = userData.id
        }
        if id != "0" {
            if (id.firstIndex(of: "+") != nil){
                id = String(id.dropFirst())
            }
            Messaging.messaging().subscribe(toTopic: id!) { error in
                
                print("Subscribed to \(id!) topic")
            }
        }
        
    }
    func setChat(completion: @escaping (Bool) -> Void){
        
        ref.child("chats").child("\(self.user_id!)").setValue(false){
            (error:Error?, ref:DatabaseReference) in
            if let error = error {
                print("Data could not be saved: \(error).")
                completion(false)
            } else {
                print("Data saved successfully!")
                completion(true)
            }
        }
        completion(false)
    }
    @IBAction func buSend(_ sender: Any) {
        //        AddSnap()
        if self.txtText.text!.isEmpty == false || self.txtText.text! != "" {
            
            
            let msg=[
                "seen":"لم يتم رؤيته",
                "receiverId": "cHtH0j3SWFRxrR7KbLWINrl92fU2",
                "senderId":"\(self.user_id!)",
                "textMessage":self.txtText.text!,
                "date": getDateOnlyAsMillSeconde(),
                "voiceUrl":"not Found"
            ] as [String:Any]
            self.obviewPresnterMessage.checkInternet(msg:msg,firstname:self.userData.firstName,id:self.user_id)
            
        }else{
            showToast(message: "يرجى إدخال الرسالة", view: view,place:0)
        }
        
        //ServerValue.timestamp()
    }
    func playSound() {
        guard let url = Bundle.main.url(forResource: "msg_pops", withExtension: "mp3") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            audioPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let player = audioPlayer else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    func LoadMessages(number:Int){
        ref.removeAllObservers()
        self.ListMessages.removeAll()
        self.count += number
        ref.child("messages").observeSingleEvent(of: .value, with: { (snapshot) in
            if !snapshot.hasChild("\(self.user_id!)"){
                self.actInd.stopAnimating()
            }
        })
        ref.child("messages").child("\(self.user_id!)").queryLimited(toLast: 1).observe(.childAdded, with: { (snapshot) in
            
//            if let postDict = snapshot.value as? [String: AnyObject] {
//                if self.falgSound == true  && self.isOpenController == 1{
//                    if postDict["senderId"] as? String != self.user_id && postDict["seen"] as? String != "تمت رؤيته"{
//                        if self.viewIfLoaded?.window != nil {
//                            self.playSound()
//                        }
//
//                    }
//                }
                
                
//            }
        })
        
        
        ref.child("messages").child("\(user_id!)").observe( .childAdded, with: { (snapshot) in
            
            if let postDict = snapshot.value as? [String: AnyObject] {
                
                let message = Message(msgData: postDict,id_msg:snapshot.key,firstName:self.userData.firstName,id_user:self.user_id)
                
                self.ListMessages.append(message)
                
            }else{
                self.tableView.isUserInteractionEnabled = true
                self.tableView.reloadData()
                self.actInd.stopAnimating()
                self.scrollToBottom()
                
            }
            
            
            self.tableView.isUserInteractionEnabled = true
            self.tableView.reloadData()
            self.actInd.stopAnimating()
            self.scrollToBottom()
            
            
        })
        
        
        
        self.falgSound = true
        
    }
    
    func changeListner(){
        let dispatchQueue = DispatchQueue.main
        dispatchQueue.async{
            ref.child("messages").child("\(self.user_id!)").observe(.childAdded) { (snap, res) in
                let i = JSON(snap.value!).dictionaryObject
                let id = snap.key
                print("new msg \(snap)")
                let message = Message(msgData: i! as [String : AnyObject],id_msg:id,firstName:self.userData.firstName,id_user:self.user_id)
                let item = self.ListMessages.filter({$0.id == message.id})
                if item.isEmpty{
                    self.ListMessages.append(message)
                    let indexPath = IndexPath(item: self.ListMessages.count-1, section: 0)
                    self.tableView.insertRows(at: [indexPath], with: .fade)
                    //                self.tableView.reloadData()
                    self.scrollToBottom()
                }
                
                
            }
        }
    }
    func setNameUser(name:String){
        ref.child("messages").child("\(self.user_id!)").observeSingleEvent(of: .value, with: { (snapshot) in
             if snapshot.hasChild("name"){
                 print("true name exist")
             }else{
                 print("false name doesn't exist")
                ref.child("messages").child("\(self.user_id!)").child("name").setValue(name)

             }
         })
    }
    
    func updateListner(){
        let dispatchQueue = DispatchQueue.main
        dispatchQueue.async{
            ref.child("messages").child("\(self.user_id!)").observe(.childChanged) { [self] (snap, res) in
                let i = JSON(snap.value!).dictionaryObject
                let id = snap.key
                print(snap)
                if let row = self.ListMessages.firstIndex(where: {$0.id == id}) {
                    let message = Message(msgData: i! as [String : AnyObject],id_msg:id,firstName:self.userData.firstName,id_user:self.user_id)
                    self.ListMessages[row] = message
                    
                    let indexPath = IndexPath(item: row, section: 0)
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
//                    if (ListMessages.count > 0) {
//                    let pre_last = IndexPath(item: row-1, section: 0)
//                        self.tableView.reloadRows(at: [pre_last], with: .fade)
                        
//                    }
                    //                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
    
    
    @IBAction func BuLoadImages(_ sender: Any) {
        //present(imagePicker, animated: true, completion: nil)
        //startRecording()
    }
    
    
    
    @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
    
    func scrollToBottom(){
        if !ListMessages.isEmpty {
            let indexPath = IndexPath(row: self.ListMessages.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        AddItemListner()
    }
    
    func setUpActivityIndicatory(uiView: UIView) {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0);
        actInd.center = uiView.center
        actInd.hidesWhenStopped = true
        actInd.style =
            UIActivityIndicatorView.Style.gray
        self.tableView.isUserInteractionEnabled = false
        
        uiView.addSubview(actInd)
        actInd.startAnimating()
    }
    
    
}

extension HomeChatViewController:AudioDelegate{
    func playAudio(url:NSURL) {
        //        downloadFileFromURL(url:url)
    }
    
    
}

extension HomeChatViewController:viewDelegteMessages{
    func removeItemAudio(ob: Message!,url:String) {
        if let row = self.ListMessages.firstIndex(where: {$0.senderID == ob.senderID && $0.PostDate == ob.PostDate &&  $0.PostDate == ob.PostDate}) {
            self.ListMessages.remove(at: row)
            let indexPath = IndexPath(item: row, section: 0)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            //           self.tableView.reloadData()
            
        }else{print("nill item")}
        
    }
    
    func removeItem(ob: Message!) {
        
        if let row = self.ListMessages.firstIndex(where: {$0.senderID == ob.senderID && $0.PostDate == ob.PostDate &&  $0.PostDate == ob.PostDate}) {
            self.ListMessages.remove(at: row)
            let indexPath = IndexPath(item: row, section: 0)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            //            self.tableView.reloadData()
            
        }else{print("nill item")}
    }
    
    func setListMessage(obMessage: Message!) {
        self.ListMessages.append(obMessage)
        //        tableView.reloadData()
        
        if let row = self.ListMessages.firstIndex(where: {$0.senderID == obMessage.senderID && $0.PostDate == obMessage.PostDate &&  $0.PostDate == obMessage.PostDate}) {
            let indexPath = IndexPath(item: row, section: 0)
            self.tableView.insertRows(at: [indexPath], with: .fade)
            //            self.tableView.reloadData()
            
        }else{print("nill item")}
        
        
        //        if let row = self.ListMessages.firstIndex(where: {$0.id == obMessage.id}) {
        //            let indexPath = IndexPath(item: row, section: 0)
        //            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        //
        //        }
        
    }
    
    func updateMessage(index:Int){
        print("update index \(index)")
        tableView.reloadData()
    }
    
    func cleareTextxMessage(){
        txtText.text = ""
        self.scrollToBottom()
        
    }
    
    func setUserData(ob:tblUserInfo){
        userData = ob
        if userData.loginType == "4" {
            user_id = userData.id.replacingOccurrences(of: ".", with: "_")
        }else{
            user_id = userData.id
        }
    }
    
    
    
}
extension HomeChatViewController :  UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ListMessages.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let oneMessage=ListMessages[indexPath.row]
        
        
        if oneMessage.text != "not Found" {
            
            
            if oneMessage.senderID != user_id {
                let cell:ChatViewCell=tableView.dequeueReusableCell(withIdentifier: "cellTextSender", for: indexPath) as! ChatViewCell
                
                cell.txtrecive.text = oneMessage.text
                if  self.ListMessages.endIndex-1 == indexPath.row {
                }
                
                cell.createLinke(txtText:cell.txtrecive,oneMessage:oneMessage)
                
                return cell
                
                
            }else{
                let cell:ChatViewCell=tableView.dequeueReusableCell(withIdentifier: "cellTextrecive", for: indexPath) as! ChatViewCell
                if  oneMessage.mark == "لم يتم رؤيته" || self.ListMessages.count - 1 == indexPath.row {
                    cell.checkDielverd.isHidden = false
                    //                     scrollToBottom()
                }else{
                    cell.checkDielverd.isHidden = true
                    //                        scrollToBottom()
                }
                
                //cell.setText(msg: oneMessage)
                if oneMessage.mark == "لم يتم رؤيته"{
                    cell.checkDielverd.image = UIImage(named:"notDeliverd")
                }else {
                    cell.checkDielverd.image = UIImage(named:"dilevrd")
                }
                if oneMessage.flageInternet == "0" && oneMessage.mark == "لم يتم رؤيته"{
                    cell.checkDielverd.image = UIImage(named:"timer")
                }
//                cell.txtText.text = oneMessage.text
                cell.createLinke(txtText:cell.txtText,oneMessage:oneMessage)
                
                
                return cell
            }
        }else if oneMessage.urlImage != "not Found"{
            
            let cell:imageCell=tableView.dequeueReusableCell(withIdentifier: "cellSenderImage", for: indexPath) as! imageCell
            
            cell.delegat = self

            cell.image_url.downloadImage(from: URL(string: oneMessage.urlImage!)!)
            
        
            return cell
            
        }
//        else{
//            if oneMessage.senderID != user_id {
//                let cell:ChatViewCell=tableView.dequeueReusableCell(withIdentifier: "cellSenderAudio", for: indexPath) as! ChatViewCell
//                cell.delegate = self
//                if oneMessage.audio != "not found" {
//                    cell.url = oneMessage.audio
//                }
//                return cell
//            }else{
//
//
//                let cell:ChatViewCell=tableView.dequeueReusableCell(withIdentifier: "cellreciveAudio", for: indexPath) as! ChatViewCell
//
//                if  oneMessage.mark == "لم يتم رؤيته" || self.ListMessages.count - 1 == indexPath.row {
//                    cell.checkDeilverdAudio.isHidden = false
//                }else{
//                    cell.checkDeilverdAudio.isHidden = true
//                }
//                if oneMessage.mark == "لم يتم رؤيته"{
//                    cell.checkDeilverdAudio.image = UIImage(named:"notDeliverd")
//                }else {
//                    cell.checkDeilverdAudio.image = UIImage(named:"dilevrd")
//                }
//                if oneMessage.flageInternet == "0" && oneMessage.mark == "لم يتم رؤيته"{
//                    cell.checkDeilverdAudio.image = UIImage(named:"timer")
//                }
//                cell.delegate = self
//                if  oneMessage.audio != "not found" {
//                    cell.url = oneMessage.audio
//                }
//
//                return cell
//            }
//
//        }
        return ChatViewCell()
    }
}

extension UIApplication {
    func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        switch (base) {
        case let controller as UINavigationController:
            return topViewController(controller.visibleViewController)
        case let controller as UITabBarController:
            return controller.selectedViewController.flatMap { topViewController($0) } ?? base
        default:
            return base?.presentedViewController.flatMap { topViewController($0) } ?? base
        }
    }
}


class MyTapGesture: UITapGestureRecognizer {
    var title = String()
}
