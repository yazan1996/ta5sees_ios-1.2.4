//
//  SubTypeDietController.swift
//  Ta5sees
//
//  Created by Admin on 6/21/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit

class SubTypeDietController: UIViewController ,UIViewControllerTransitioningDelegate{
    
    @IBOutlet weak var gain: ButtonColorRaduis!
    @IBOutlet weak var loss: ButtonColorRaduis!
    @IBOutlet weak var mantains: ButtonColorRaduis!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        animationviewLeft(btn: mantains)
        animationviewLeft(btn: loss)
        animationviewLeft(btn: gain)
    }
    
    @IBAction func btnGain(_ sender: Any) {
        refSubPlanMater = 2
        nextViewController()
    }
    @IBAction func btnMaintains(_ sender: Any) {
        refSubPlanMater = 3
        nextViewController()
    }
    
    @IBAction func btnloss(_ sender: Any) {
        refSubPlanMater = 1
        nextViewController()
    }
    
    @IBAction func buDismiss(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func nextViewController(){
        let detailView = storyboard!.instantiateViewController(withIdentifier: "regester2") as! RegesterStepOne
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
        self.performSegue(withIdentifier: "toregester", sender: nil)
        
    }
    func animationviewLeft(btn:UIButton){
        
        let trans = CATransition()
        trans.type = CATransitionType.moveIn
        trans.subtype = CATransitionSubtype.fromTop
        trans.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        trans.duration = 0.2
        btn.layer.add(trans, forKey: nil)
    }
}
