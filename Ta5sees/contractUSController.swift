//
//  contractUSController.swift
//  Ta5sees
//
//  Created by Admin on 9/28/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD
import SwiftyJSON

class contractUSController: UIViewController,UITextViewDelegate {

    @IBAction func dissmis(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        txtMessage.text = "ملاحظاتك"
        txtMessage.textColor = UIColor.lightGray

        txtMessage.selectedTextRange = txtMessage.textRange(from: txtMessage.beginningOfDocument, to: txtMessage.beginningOfDocument)

    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "ملاحظاتك"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBAction func btnSentNote(_ sender: Any) {
        let header = HTTPHeaders()
        
        if txtEmail!.text!.isEmpty {
            txtEmail.placeholder = "يجب ادخال البريد الالكتروني الخاص بك"
            txtEmail.placeholderColor = .red
        }else if txtMessage!.text!.isEmpty {
            txtMessage.text = "يجب ادخال الملاحظة لاتمام العملية"
            txtMessage.textColor = .red
        }else{
            SVProgressHUD.show()
            AF.request("http://dev.tele-ent.com/TE-WebServices/diet/contactus",method:.post,  parameters:["email":txtEmail!.text!,"userID" :getDataFromSheardPreferanceString(key: "userID"),"message":txtMessage!.text!,"key":"WfJGygCfEu6n5ZhhhucT"],encoding:URLEncoding.default, headers:header).responseJSON{
                    response in
                switch response.result {
                case .success(let value):
                        SVProgressHUD.dismiss()
                        if let recommends = value as? [String: Any] {
                            if recommends["errorCode"] as! Int == 0 {
                                showToast(message: "شكرا لك،تم ارسال الملاحظة", view: self.view,place:0)
                                self.dismiss(animated: true, completion: nil)
                            }else{
                                showToast(message: "نعتذر لم يتم ارسال ملاحظتك", view: self.view,place:0)

                            }
                        }
                    case.failure(let error):
                        print("error \(error)")
                        showToast(message: "نعتذر لم يتم ارسال ملاحظتك", view: self.view,place:0)
                        SVProgressHUD.dismiss()
                        
        
        
                    }
        
                }
                
    }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
          
          view.endEditing(true)
      }
 
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {

            textView.text = "ملاحظاتك"
            textView.textColor = UIColor.lightGray

            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }

        // Else if the text view's placeholder is showing and the
        // length of the replacement string is greater than 0, set
        // the text color to black then set its text to the
        // replacement string
         else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = text
        }

        // For every other case, the text should change with the usual
        // behavior...
        else {
            return true
        }

        // ...otherwise return false since the updates have already
        // been made
        return false
    }
  
}
