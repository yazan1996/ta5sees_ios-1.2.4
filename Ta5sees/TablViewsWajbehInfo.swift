//
//  TablViewsWajbehInfo.swift
//  Ta5sees
//
//  Created by Admin on 2/5/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift


extension ViewController:UITableViewDelegate,UITableViewDataSource  {
   
    func deletBreakfast(index:Int){
        obPresenter.getWajbeh(id: ListEatenBreakfast[index].id, date: lblDate.text!, caloris: Int(ListEatenBreakfast[index].kCal)!, isProposal: ["0","1"]) { (resp, err) in
            if resp as? tblUserWajbehEaten != nil {
                //
                self.ListEatenBreakfast.remove(at: index)
                
                try! setupRealm().write{
                    setupRealm().delete((resp as! tblUserWajbehEaten?)! )
                }
                self.tblBreakfast.reloadData()
                self.DiplayDailyData()
            }
        }
    }
    
    func deletDinner(index:Int){
        obPresenter.getWajbeh(id: ListEatenDinner[index].id, date: lblDate.text!, caloris: Int(ListEatenDinner[index].kCal)!, isProposal: ["0","1"]) { (resp, err) in
            if resp as? tblUserWajbehEaten != nil {
                //
                self.ListEatenDinner.remove(at: index)
                
                try! setupRealm().write{
                    setupRealm().delete((resp as! tblUserWajbehEaten?)! )
                }
                self.tblDinner.reloadData()
                self.DiplayDailyData()
            }
        }
    }
    func deletLunsh(index:Int){
           obPresenter.getWajbeh(id: ListEatenLaunsh[index].id, date: lblDate.text!, caloris: Int(ListEatenLaunsh[index].kCal)!, isProposal: ["0","1"]) { (resp, err) in
               if resp as? tblUserWajbehEaten != nil {
                   //
                   self.ListEatenLaunsh.remove(at: index)
                   
                   try! setupRealm().write{
                       setupRealm().delete((resp as! tblUserWajbehEaten?)! )
                   }
                   self.tblLaunsh.reloadData()
                   self.DiplayDailyData()
               }
           }
        }
    func deletTabsera1(index:Int){
        obPresenter.getWajbeh(id: ListEatenTasbera1[index].id, date: lblDate.text!, caloris: Int(ListEatenTasbera1[index].kCal)!, isProposal: ["0","1"]){ (resp, err) in
            if resp as? tblUserWajbehEaten != nil {
                //
                self.ListEatenTasbera1.remove(at: index)
                
                try! setupRealm().write{
                    setupRealm().delete((resp as! tblUserWajbehEaten?)! )
                }
                self.tblTasber1.reloadData()
                self.DiplayDailyData()
            }
        }
    }
    func deletTabsera2(index:Int){
           obPresenter.getWajbeh(id: ListEatenTasbera2[index].id, date: lblDate.text!, caloris: Int(ListEatenTasbera2[index].kCal)!, isProposal: ["0","1"]) { (resp, err) in
               if resp as? tblUserWajbehEaten != nil {
                   //
                   self.ListEatenTasbera2.remove(at: index)
                   
                   try! setupRealm().write{
                       setupRealm().delete((resp as! tblUserWajbehEaten?)! )
                   }
                   self.tblTasber2.reloadData()
                   self.DiplayDailyData()
               }
           }
        }
    func deletTamreen(index:Int){
        obPresenter.getTamreen(id: ListTamreenBurned[index].id, date: lblDate.text!, caloris: Int(ListTamreenBurned[index].calorisBurn)!) { (resp, err) in
            if resp as? tblUserTamreen != nil {
                //
                self.ListTamreenBurned.remove(at: index)
                
                try! setupRealm().write{
                    setupRealm().delete((resp as! tblUserTamreen?)! )
                }
                self.tblTamreen.reloadData()
                self.DiplayDailyData()
            }
        }
    }
    func getNameItemBreakfast(index:Int,id:String)->Int{
        if id == "1" {
            return  Int(self.ListEatenBreakfast[index].refItemID)!

        }else  if id == "2" {
            return  Int(self.ListEatenLaunsh[index].refItemID)!

        }else  if id == "3" {
            return  Int(self.ListEatenDinner[index].refItemID)!

        }else  if id == "4" {
            return  Int(self.ListEatenTasbera1[index].refItemID)!

        }else  if id == "5" {
            return  Int(self.ListEatenTasbera2[index].refItemID)!

        }else  if id == "6" {
            return  Int(self.ListTamreenBurned[index].refTamreenInfoID)!
        }
        return 0
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            if tableView == tblBreakfast{
                deletBreakfast(index:indexPath.row)
            }else if tableView == tblTasber1{
               deletTabsera1(index:indexPath.row)
            }else if tableView == tblLaunsh{
                deletLunsh(index:indexPath.row)
            }else if tableView == tblTasber2{
              deletTabsera2(index:indexPath.row)
            }else if tableView == tblTamreen{
                deletTamreen(index:indexPath.row)
            }else {
                print("dinner")
                deletDinner(index:indexPath.row)
            }
            
            
        }
    }
   
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
 
        return true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblBreakfast{
            if ListEatenBreakfast.isEmpty{
                tblMealDistribution.getUserBreakfastMealDistribution(brekfast: viewBreakfast)

            }
           
          setLableEaten(lable:breakfastEate,list:ListEatenBreakfast)
            return ListEatenBreakfast.count
        }else if (tableView == tblDinner){
            if ListEatenDinner.isEmpty{
                  tblMealDistribution.getUserDinnerMealDistribution(dinner: viewDinner)
            }
            setLableEaten(lable:dinnerEate,list:ListEatenDinner)
            return ListEatenDinner.count
        }else if tableView == tblLaunsh{
            if ListEatenLaunsh.isEmpty{
                            tblMealDistribution.getUserLunchMealDistribution(luansh: viewLaunch)

            }
            setLableEaten(lable:launshEate,list:ListEatenLaunsh)
            return ListEatenLaunsh.count
        }else if tableView == tblTasber1{
            if ListEatenTasbera1.isEmpty{
                tblMealDistribution.getUserSnack1MealDistribution(tasber1: viewTasber1)

            }
            setLableEaten(lable:snak1Eate,list:ListEatenTasbera1)
            return ListEatenTasbera1.count
        }else if tableView == tblTamreen{
            return ListTamreenBurned.count
        }else{
            if ListEatenTasbera1.isEmpty{
             tblMealDistribution.getUserSnack2MealDistribution(tasber2: viewTasberh2)
            }
            setLableEaten(lable:snak2Eate,list:ListEatenTasbera2)
            return ListEatenTasbera2.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblBreakfast{
          
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            
            cell.idItem = indexPath.row
            cell.view = self
            cell.isWajbeh = ListEatenBreakfast[indexPath.row].isWajbeh
            cell.id_category = "1"
            cell.name_item.text = cell.getNameItem(id: Int(ListEatenBreakfast[indexPath.row].refItemID)!, isWajbeh: ListEatenBreakfast[indexPath.row].isWajbeh)
            cell.kacl_item.text = "\(ListEatenBreakfast[indexPath.row].kCal) سعرة حرارية"
            if cell.name_item.text == "nil" {
                return WajbehInfoCell()
            }
            return cell
        }else if (tableView == tblDinner){
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            cell.idItem = indexPath.row
            cell.view = self
            cell.isWajbeh = ListEatenDinner[indexPath.row].isWajbeh
            cell.id_category = "3"
            cell.name_item.text = cell.getNameItem(id: Int(ListEatenDinner[indexPath.row].refItemID)!, isWajbeh: ListEatenDinner[indexPath.row].isWajbeh)
            cell.kacl_item.text = "\(ListEatenDinner[indexPath.row].kCal) سعرة حرارية"
            if cell.name_item.text == "nil" {
                return WajbehInfoCell()
            }
            return cell
        }else if tableView == tblLaunsh{
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            cell.idItem = indexPath.row
            cell.view = self
            cell.isWajbeh = ListEatenLaunsh[indexPath.row].isWajbeh
            cell.id_category = "2"
            cell.name_item.text = cell.getNameItem(id: Int(ListEatenLaunsh[indexPath.row].refItemID)!, isWajbeh: ListEatenLaunsh[indexPath.row].isWajbeh)
            cell.kacl_item.text = "\(ListEatenLaunsh[indexPath.row].kCal) سعرة حرارية"
            if cell.name_item.text == "nil" {
                return WajbehInfoCell()
            }
            return cell
        }else if tableView == tblTasber1{
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            
            cell.name_item.text = cell.getNameItem(id: Int(ListEatenTasbera1 [indexPath.row].refItemID)!, isWajbeh: ListEatenTasbera1[indexPath.row].isWajbeh)
            cell.kacl_item.text = "\(ListEatenTasbera1[indexPath.row].kCal) سعرة حرارية"
            cell.idItem = indexPath.row
            cell.view = self
            cell.isWajbeh = ListEatenTasbera1[indexPath.row].isWajbeh
            cell.id_category = "4"
            if cell.name_item.text == "nil" {
                return WajbehInfoCell()
            }
            return cell
        }else if tableView == tblTamreen{
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            cell.idItem = indexPath.row
            cell.name_item.text = cell.getNameItem(id: Int(ListTamreenBurned[indexPath.row].refTamreenInfoID)!, isWajbeh: "3")//3-> tamreen
            cell.kacl_item.text = "\(ListTamreenBurned[indexPath.row].calorisBurn) سعرة حرارية"
            cell.view = self
            cell.id_category = "6"
            if cell.name_item.text == "nil" {
                return WajbehInfoCell()
            }
            return cell
               }else {
            let cell:WajbehInfoCell = tableView.dequeueReusableCell(withIdentifier: "breakfastcell", for: indexPath) as! WajbehInfoCell
            cell.idItem = indexPath.row
            cell.name_item.text = cell.getNameItem(id: Int(ListEatenTasbera2[indexPath.row].refItemID)!, isWajbeh: ListEatenTasbera2[indexPath.row].isWajbeh)
            cell.kacl_item.text = "\(ListEatenTasbera2[indexPath.row].kCal) سعرة حرارية"
            cell.view = self
            cell.isWajbeh = ListEatenTasbera2[indexPath.row].isWajbeh
            cell.id_category = "5"
                
                if cell.name_item.text == "nil" {
                    return WajbehInfoCell()
                }
            return cell
        }
    }

//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 93.0
//    }
    
//    func tableView(_ tableView: UITableView,
//                   viewForHeaderInSection section: Int) -> UIView? {
//        let viewHeader = Bundle.main.loadNibNamed("HedarCell", owner: self, options: nil)?.first as! HedarCell
//
//        if tableView == tblBreakfast{
////            if ListEatenBreakfast.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "breakfast")
//            viewHeader.viewContentHeader.tag = 1
//        }else if (tableView == tblDinner){
////            if ListEatenDinner.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "dinner")
//            viewHeader.viewContentHeader.tag = 3
//        }else if tableView == tblLaunsh{
////            if ListEatenLaunsh.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "launch")
//            viewHeader.viewContentHeader.tag = 2
//        }else if tableView == tblTasber1{
////            if ListEatenTasbera1.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "apple")
//            viewHeader.viewContentHeader.tag = 4
//        }else if tableView == tblTamreen{
////            if ListEatenTasbera1.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "tmreen")
//            viewHeader.viewContentHeader.tag = 6
//        }else{
////            if ListEatenTasbera2.isEmpty {
////                viewHeader.lineHeader.isHidden=true
////            }
//            viewHeader.headerImage.image = UIImage(named: "tsbera")
//            viewHeader.viewContentHeader.tag = 5
//        }
//        TapGestureView(view:viewHeader.viewContentHeader)
//        return viewHeader
//    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
          return UITableView.automaticDimension
      }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
         
     }
     
    
    func setLableEaten(lable:UILabel,list:[tblUserWajbehEaten]){
        var sum=0
        if list.isEmpty {
            lable.text = "0"

        }else{
        _ = list.reduce(into:List<tblUserWajbehEaten>()) {
            
            sum = sum + Int($1.kCal)!
            lable.text = "\(sum)"
        }
        }
    }
}



