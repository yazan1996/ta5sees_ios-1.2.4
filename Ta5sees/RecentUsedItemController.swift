//
//  RecentUsedItemController.swift
//  Ta5sees
//
//  Created by Admin on 9/12/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class RecentUsedItemController: UIViewController {

    var obShowNewWajbeh:ShowNewWajbeh?
       var id_items:String!
         var date:String!
         var pg:protocolWajbehInfoTracker?
    var id_category = ""

    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func removeAllItems(_ sender: Any) {
        tblRecentUsed.deleteAllItem { (bool) in
            print(bool)
            self.filterItem.removeAll()
            showToast(message: "تم حذف جميع الوجبات التي شاهدتها", view: self.view,place:0)
            self.tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    var filterItem = tblRecentUsed.getAllDataRecentUsed()
    override func viewDidLoad() {
        super.viewDidLoad()

        obShowNewWajbeh = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        filterItem = tblRecentUsed.getAllDataRecentUsed()
        tableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "DisplayCateItemsController" {
            if let dis=segue.destination as?  DisplayCateItemsController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                    tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_category, date: date, isProposal: "0", completion: { (id,bool) in
                        print("bool \(bool)")
                        dis.idItemsEaten = id
                        if bool == true {
                            dis.isEaten = true
                        }
                        else{
                            dis.isEaten = false
                        }
                    })
                }
            }
        }else if segue.identifier == "DisplayCateItemsSenfController" {
            if let dis=segue.destination as?  DisplayCateItemsSenfController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                    tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_category, date: date, isProposal: "0", completion: { (id,bool) in
                        print("bool \(bool)")
                        dis.idItemsEaten = id
                        if bool == true {
                            dis.isEaten = true
                        }else{
                            dis.isEaten = false
                        }
                    })
                }
            }
        }
        
    }


 

}
extension RecentUsedItemController:ShowNewWajbeh{
    func goToViewWajbeh(idWajbeh: String, flag: String) {
        
        if flag == "wajbeh" || flag == "0" {
            self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
        }else {
            self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
        }
        //        txtSearchOutlet.text = nil
        
    }
    
    
    
}

extension RecentUsedItemController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterItem.isEmpty {
            TableViewHelper.EmptyMessage(message: "لم تقم بمشاهدت اي شىء مؤخرا", viewController: self.tableView)
            return 0
        }
        return filterItem.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RecentUsedCell = tableView.dequeueReusableCell(withIdentifier: "RecentUsedCell", for: indexPath) as! RecentUsedCell
        
        if filterItem[indexPath.row].flag == "0" {
            let item = tblRecentUsed.getPackageDataRecent(id: filterItem[indexPath.row].refItem)
            cell.name.text = item.name
                  cell.category.text =  "وجبة"
                  cell.caloris.text =  "\(item.calories!) سعرة حرارية"

        }else{
            let item = tblRecentUsed.getSenfDataRecent(id: filterItem[indexPath.row].refItem)
            cell.name.text = item.trackerName
                  cell.category.text =  "صنف"
                  cell.caloris.text =  "\(item.wajbehCaloriesTotal!) سعرة حرارية"
        }
        

//        cell.logoImage.image = UIImage(named : "logo")
       
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(filterItem[indexPath.row].refItem)")
        obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filterItem[indexPath.row].refItem,flag:filterItem[indexPath.row].flag)
        if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
