//
//  TextInputViewController.swift
//  Ta5sees
//
//  Created by Admin on 8/24/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import TextFieldEffects
//import PopOverDatePicker
import SVProgressHUD
import Firebase
import FirebaseAnalytics

class TextInputViewController: UIViewController,UITextFieldDelegate, UIViewControllerTransitioningDelegate,UIPopoverPresentationControllerDelegate, UIAdaptivePresentationControllerDelegate {
    
    @IBOutlet weak var stackTarget: UIStackView!
    @IBOutlet weak var stackSeekBar: UIStackView!
    @IBOutlet weak var lblPeriodTarget: UILabel!
    @IBOutlet weak var dietOrderValue: UISlider!
    @IBOutlet weak var txtQuantitiEachWeek: UILabel!
    @IBOutlet weak var txtNumberWeek: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var birthdate: HoshiTextField!
    @IBOutlet weak var target_wieght: HoshiTextField!
    @IBOutlet weak var user_name: HoshiTextField!
    @IBOutlet weak var hieght: HoshiTextField!
    @IBOutlet weak var wieght: HoshiTextField!
    

    var scenarioStep = "0" // 1-> updateDietType else registration
    var incrementKACL:Double = 0.0
    var week:Double = 0.0
    var InfoDict:String!
    var datePicker: UIDatePicker = UIDatePicker()
    var pickerToolbar: UIToolbar?
    var ageChild:Float = 0.0
    let date = Date()
    let color_green = UIColor(red: 123/255, green: 222/255, blue: 179/255, alpha: 1.0)
    let color_Inactive =  UIColor(hex: 0x9CABB3)
    var dietType = getDataFromSheardPreferanceString(key: "dietType")
    lazy var obcustomDatePicker = customDatePicker(with: self)
    
    @IBAction func seekBarAction(_ sender: Any) {
        print(dietOrderValue.value)
        print(getDataFromSheardPreferanceString(key: "txtweight"))
        print(getDataFromSheardPreferanceString(key: "targetValue"))
        if wieght.text == "" {
            customError(textFeild: wieght, color: UIColor.red, placeHolder: "يجب ادخال وزنك")
            return
        }else if target_wieght.text == "" {
            customError(textFeild: target_wieght, color: UIColor.red, placeHolder: "يجب ادخال هدف وزنك")
            
            return
        }else{

            calculateWeek()
  

        if dietOrderValue.value == 0.0 {
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
            txtNumberWeek.text = "(0) اسابيع"
            if dietType == "1" {
                txtQuantitiEachWeek.text = "زيادة : ٠ غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : ٠ غم لكل اسبوع"
                
            }

        }else{
            lblPeriodTarget.text = "قم  بتحديد فترة الوصول الى هدفك"
            lblPeriodTarget.textColor = color_Inactive
            
            txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
            if dietType == "1" {
                txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }else{
                txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }
        }


    }
    }
    func calculateWeek(){
        let progress:Double = round(Double(dietOrderValue.value))
        print(progress)
        let wekcoloreis:Double = (0.2 + (progress * 0.06))

        week =  abs(Double(((FormatterHoshiTextField(str:wieght!).text! as NSString).doubleValue
            - ( FormatterHoshiTextField(str:target_wieght!).text! as NSString).doubleValue)) / wekcoloreis)
        txtNumberWeek.text = "(\(Int(round(week)))) اسابيع"
        incrementKACL = wekcoloreis * 1000.0
        if dietType == "1" {
            txtQuantitiEachWeek.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }else{
            txtQuantitiEachWeek.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }
        if getDataFromSheardPreferanceString(key: "UserAgeID") != "2" {
            if dietType != "3" {
                setDataInSheardPreferance(value:String(Int(round(incrementKACL))),key:"incrementKACL")
                setDataInSheardPreferance(value:String(Int(round(week))),key:"week")
            }
        }

    }
    func delegetGenderType(){
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "NaturalDiet") as! NaturalDiet
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
    }
    @IBAction func btnBack(_ sender: Any) {

            dismiss(animated: true, completion: nil)

      }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("scenarioStep \(scenarioStep)")

        if scenarioStep == "0" {
        if dietType == "3"  || getDataFromSheardPreferanceString(key: "UserAgeID") == "2"{
            let firstView1 =  self.stackSeekBar.arrangedSubviews[0]
            firstView1.isHidden = true
            let firstView2 =  self.stackTarget.arrangedSubviews[1]
            firstView2.isHidden = true
            target_wieght.text = "0.0"
        }
      let preferences = UserDefaults.standard
      if  preferences.object(forKey: "name") == nil {
          return
      }else{
          if getDataFromSheardPreferanceString(key: "loginType") == "2" {
          user_name.text = getDataFromSheardPreferanceString(key: "name")
          }else if getDataFromSheardPreferanceString(key: "loginType") == "3" {
          user_name.text = getDataFromSheardPreferanceString(key: "name")
          }else if getDataFromSheardPreferanceString(key: "loginType") == "4" {
            if getDataFromSheardPreferanceString(key: "name") == "" {
                 user_name.text = getDataFromSheardPreferanceString(key: "name")
            }else{
//                 user_name.text = ""
            }
          }
      }
        }else{
            if getDataFromSheardPreferanceString(key: "copyDietType") == "3"  || getDataFromSheardPreferanceString(key: "UserAgeID") == "2"{
                let firstView1 =  self.stackSeekBar.arrangedSubviews[0]
                firstView1.isHidden = true
                let firstView2 =  self.stackTarget.arrangedSubviews[1]
                firstView2.isHidden = true
            }
        }

    }
//    var nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dietOrderValue.setValue(10.0, animated: true)
        if scenarioStep == "1" {
            dietType = getDataFromSheardPreferanceString(key: "copyDietType")
        }
        
        wieght.clearsOnBeginEditing = true
        target_wieght.clearsOnBeginEditing = true
        user_name.clearsOnBeginEditing = true
        hieght.clearsOnBeginEditing = true
        
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "2" {
            setDataInSheardPreferance(value: "0", key: "targetValue")
            setDataInSheardPreferance(value:"0",key:"incrementKACL")
            setDataInSheardPreferance(value:"0",key:"week")
        }
        if dietType == "1" {
            txtQuantitiEachWeek.text = "زيادة : ٠ غم لكل اسبوع"
            
        }else{
            txtQuantitiEachWeek.text = "تخسيس : ٠ غم لكل اسبوع"
            
        }
        if scenarioStep == "1" {
            if getDataFromSheardPreferanceString(key: "txtweight") != "0"{
                wieght.text = getUserInfoProgressHistory().weight + " كغم"//getDataFromSheardPreferanceString(key: "txtweight") + " كغم"
//                let target = Double(getDataFromSheardPreferanceString(key: "txtweight")) ?? 1.0
//                if dietType == "1"{
//                    target_wieght.text = "\(target + 1.0)" + " كغم"
//                }else if dietType == "2"{
//                    target_wieght.text = "\(target - 1.0)" + " كغم"
//                }
            }
            if getDataFromSheardPreferanceString(key: "name") != "0"{
                user_name.text = getDataFromSheardPreferanceString(key: "name")
            }
               
            
            if getDataFromSheardPreferanceString(key: "txtheight") != "0"{
                hieght.text = getUserInfoProgressHistory().height + " سم"//getDataFromSheardPreferanceString(key: "txtheight")
            }
            if getDataFromSheardPreferanceString(key: "txtdate") != "0"{
                birthdate.text = getUserInfoProgressHistory().birthday//getDataFromSheardPreferanceString(key: "txtdate")
            }
            if dietType != "3" || getDataFromSheardPreferanceString(key: "UserAgeID") != "2" {
                calculateWeek()
            }
        }
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("notificationName"), object: nil) // deleted to until no occur duplicate

//        nc.addObserver(self, selector: #selector(self.TimePickerNotification(_:)), name: Notification.Name("notificationName"), object: nil)
        
        
        user_name.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        hieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        hieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        let showPicker = UITapGestureRecognizer(target: self, action: #selector((self.TimePicker(_:))))
        showPicker.numberOfTapsRequired=1
        birthdate.addGestureRecognizer(showPicker)
        self.HideKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        birthdate.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        if dietType != "3"  || getDataFromSheardPreferanceString(key: "UserAgeID") != "2"{
          
        target_wieght.addTarget(self, action:#selector(textFieldDidBeign), for: UIControl.Event.editingDidBegin)
        target_wieght.addTarget(self, action:#selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        }
       
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notificationName"), object: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Try to find next responder
        if textField.tag == 1 {
            hieght.becomeFirstResponder()
        } else if textField.tag == 2 {
            wieght.becomeFirstResponder()

        }

        return true
    }
    
    func customError(textFeild:HoshiTextField,color:UIColor,placeHolder:String){
        textFeild.isError( numberOfShakes: 2.5, revert: true)
//        textFeild.borderInactiveColor = color_Inactive
//        textFeild.borderActiveColor = color
        textFeild.placeholder = placeHolder
        textFeild.placeholderColor = color
    }
    

       
    @objc func textFieldDidChange(textField: HoshiTextField) {

        let text =  FormatterHoshiTextField(str:textField).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        if text == "" { return }
        if textField.tag == 1 {
            if textField.text == "" {
                customError(textFeild:textField,color:UIColor.red,placeHolder:"يجب ادخال الاسم")
            }else{
                customError(textFeild:textField,color:color_green,placeHolder:"الاسم")
            }
        }
        else if textField.tag == 2 {
            if Double(text)! >= 50.0 && Double(text)! <= 300.0{
                customError(textFeild:textField,color:color_green,placeHolder:"الطول")
                textField.text = text + " سم"
                wieght.becomeFirstResponder()
            }else{
                customError(textFeild:textField,color:UIColor.red,placeHolder:"طولك يجب ان يكون بين ٥٠ و ٣٠٠")
            }
            
        } else if textField.tag == 3 {
        
            target_wieght.text = ""
            if Double(text)! >= 30.0 && Double(text)! <= 299.0{
                customError(textFeild:textField,color:color_green,placeHolder:"الوزن")
                textField.text = text + " كغم"
                if  getDataFromSheardPreferanceString(key: "UserAgeID") != "2" && dietType != "3"{
                    target_wieght.becomeFirstResponder()
                    calculateWeek()
                }else{
                    view.endEditing(true)
                    birthdate.becomeFirstResponder()
                }
            }else{
                customError(textFeild:textField,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }
            
        } else if textField.tag == 4 {
           
            
            let text_wieght = FormatterHoshiTextField(str:wieght!).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            if text_wieght == "" { return}
            if textField.text == "" { return}
            
            if  dietType == "1" {
                if  getDataFromSheardPreferanceString(key: "UserAgeID") != "2"{

                    if Double(textField.text!)! > Double(text_wieght)! && Double(textField.text!)! <= 299.0{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                    if scenarioStep == "0" {
                    birthdate.becomeFirstResponder()
                    }
                }else{
                    
                    customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من  \(Double(text_wieght)!+1.0)و ٢٩٩")
                    
                }
                    dietOrderValue.setValue(10.0, animated: true)
                    calculateWeek()
                }else{
                    view.endEditing(true)
                    if scenarioStep == "0" {
                    birthdate.becomeFirstResponder()
                    }                }
                
            }else if dietType == "2" {
            if  getDataFromSheardPreferanceString(key: "UserAgeID") != "2"{
                if Double(textField.text!)! >= 30.0 && Double(textField.text!)! < Double(text_wieght)!{
                    customError(textFeild:textField,color:color_green,placeHolder:"هدف الوزن")
                    textField.text = text + " كغم"
                    view.endEditing(true)
                    if scenarioStep == "0" {
                    birthdate.becomeFirstResponder()
                    }                }else{
                        customError(textFeild:textField,color:UIColor.red,placeHolder:"الهدف يجب ان يكون من ٣٠  \(Double(text_wieght)!-1.0)و")
                }
                dietOrderValue.setValue(10.0, animated: true)
                calculateWeek()
            }else{
                view.endEditing(true)
                if scenarioStep == "0" {
                birthdate.becomeFirstResponder()
                }
                
            }
            }
            
            
            
            return
        }
    }
    
    
    
    @objc func textFieldDidBeign(textField: HoshiTextField) {
      
        if textField.tag == 5 {
            view.endEditing(true)
//            nc.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: nil)
//            nc.removeObserver(self)

        }else{
            let result =  textField.text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
            textField.text = result
            
        }
    }
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @objc func TimePicker(_ gestureRecognizer: UITapGestureRecognizer) {
        DPPickerManager.shared.showPicker(title: "تاريخ الميلاد", time: "", flag: 0, idTime: "-2", picker: { (picker) in
            picker.date = Date()
            self.customeDate(datePicker:picker)
            picker.datePickerMode = .date
        }) { (date, cancel) in
            if !cancel {
                self.selectDate(date: date!)
            }
        }
    }
    
//    @objc func TimePickerNotification(_ notification: Notification) {
//        DPPickerManager.shared.showPicker(title: "الوقت", time:"", flag: 0, idTime: "-2", picker: { (picker) in
//            picker.date = Date()
//            self.customeDate(datePicker:picker)
//            picker.datePickerMode = .date
//        }) { (date, cancel) in
//            if !cancel {
//                self.selectDate(date: date!)
//            }
//        }
//    }
    func selectDate(date:Date){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        let myStringafd = formatter.string(from: yourDate!)
        ageChild = calclateAge(str: myString)
        birthdate.text = myStringafd
        ageChild = calclateAge(str: formatter.string(from: date))
        print("ageChild \(ageChild)")
    }
    func customeDate(datePicker:UIDatePicker){
        
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "2" {
            datePicker.maximumDate = Calendar.current.date(byAdding:.year, value: -9, to: Date())
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value:  -13, to: Date())
        }else{
            var components = DateComponents()
            components.year = -14
            datePicker.maximumDate = calendar.date(byAdding: components, to: Date())!
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value:  -200, to: Date())
        }
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let val_hieght = FormatterHoshiTextField(str:hieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        let val_wieght = FormatterHoshiTextField(str:wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        var val_target_wieght:String?  = FormatterHoshiTextField(str:target_wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted)
        
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "2" {
            if val_target_wieght == "" || val_target_wieght!.isEmpty{
                val_target_wieght = "0.0"
            }
            if user_name.text == "" {
                customError(textFeild:user_name,color:UIColor.red,placeHolder:"يجب ادخال الاسم")
            }else if val_hieght == ""  {
                customError(textFeild:hieght,color:UIColor.red,placeHolder:"يجب ادخال طولك")
            }else if Int(val_hieght)! < 50  {
                customError(textFeild:hieght,color:UIColor.red,placeHolder:"طولك يجب ان يكون بين ٥٠ و ٣٠٠")
            }else if val_wieght == ""  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
            }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
                customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
            }else if birthdate.text == ""  {
                customError(textFeild:birthdate,color:UIColor.red,placeHolder:"يجب ادخال تاريخ الميلاد")
            }else if calclateAge(str: birthdate.text!) < 2  {
                customError(textFeild:birthdate,color:UIColor.red,placeHolder:"لا يوجد حمية مخصصة لعمر اقل من سنتين")
            }else{
            setDataInSheardPreferance(value: String(round(ageChild)), key: "dateInYears")
             let bmi = ((FormatterHoshiTextField(str:wieght).text! as NSString).doubleValue)/((((FormatterHoshiTextField(str:hieght).text! as NSString).doubleValue)/100.0) * ((( FormatterHoshiTextField(str:hieght).text! as NSString).doubleValue)/100.0))
            setDataInSheardPreferance(value: String(Int(round(bmi))), key: "bmi")
             processDietOrderForTEEChild()
            }
        }
        
        
        
        if val_target_wieght == "" || val_target_wieght!.isEmpty{
            val_target_wieght = "0.0"
        }
        if user_name.text == "" {
            customError(textFeild:user_name,color:UIColor.red,placeHolder:"يجب ادخال الاسم")
        }else if val_hieght == ""  {
            customError(textFeild:hieght,color:UIColor.red,placeHolder:"يجب ادخال طولك")
        }else if Int(val_hieght)! < 50  {
            customError(textFeild:hieght,color:UIColor.red,placeHolder:"طولك يجب ان يكون بين ٥٠ و ٣٠٠")
        }else if val_wieght == ""  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"يجب ادخال وزنك")
        }else if  Double(val_wieght)! < 30.0 || Double(val_wieght)! > 300.0  {
            customError(textFeild:wieght,color:UIColor.red,placeHolder:"وزنك يجب ان يكون بين ٣٠ و ٢٩٩")
        }else if val_target_wieght == "0.0" && dietType != "3"   && getDataFromSheardPreferanceString(key: "UserAgeID") == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ادخال الهدف")
        }else if dietType == "1" && Double(val_target_wieght ?? "0.0")! < 300.0 && Double(val_target_wieght ?? "0.0")! < Double(val_wieght)!  && getDataFromSheardPreferanceString(key: "UserAgeID") == "1"{
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder: "يجب ان يكون الهدف بين \(Int(round(Double(val_wieght)!+1.0))) و ٢٩٩")
        }else if dietType == "2" && Double(val_target_wieght ?? "0.0")! >= 30.0 && Double(val_target_wieght ?? "0.0")! > Double(val_wieght)! && getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            customError(textFeild:target_wieght,color:UIColor.red,placeHolder:"يجب ان يكون الهدف بين ٣٠ و \(Int(round(Double(val_wieght)!-1.0)))")
        }else if birthdate.text == ""  {
            customError(textFeild:birthdate,color:UIColor.red,placeHolder:"يجب ادخال تاريخ الميلاد")
        }else if calclateAge(str: birthdate.text!) < 2  {
            customError(textFeild:birthdate,color:UIColor.red,placeHolder:"لا يوجد حمية مخصصة لعمر اقل من سنتين")
        }else if week == 0.0 && getDataFromSheardPreferanceString(key: "UserAgeID") != "2" && dietType != "3"{
            lblPeriodTarget.text = "يجب تحديد فترة الوصول الى الهدف"
            lblPeriodTarget.textColor = UIColor.red
        }else{

            let bmi = ((FormatterHoshiTextField(str:wieght).text! as NSString).doubleValue)/((((FormatterHoshiTextField(str:hieght).text! as NSString).doubleValue)/100.0) * ((( FormatterHoshiTextField(str:hieght).text! as NSString).doubleValue)/100.0))
     
            setDataInSheardPreferance(value: String(Int(round(bmi))), key: "bmi")
            setDataInSheardPreferance(value: user_name.text!, key: "name")
            setDataInSheardPreferance(value: FormatterHoshiTextField(str:hieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted), key: "txtheight")
            setDataInSheardPreferance(value: birthdate.text!, key: "txtdate")
            setDataInSheardPreferance(value: String(round(ageChild)), key: "dateInYears")
            setDataInSheardPreferance(value: FormatterHoshiTextField(str:wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted), key: "txtweight")
            setDataInSheardPreferance(value: FormatterHoshiTextField(str:target_wieght).text!.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted), key: "targetValue")

            //            self.performSegue(withIdentifier: "typeDiets", sender: nil)
 
            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "FormatNaturalDiet") as! FormatNaturalDiet
            detailView.scenarioStep = scenarioStep
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            if scenarioStep == "1" {
                createEvent(key: "15",date: getDateTime())
            }else{
                if getDataFromSheardPreferanceString(key: "loginType") == "1" {
                    createEvent(key: "54", date: getDateTime())
                }else if getDataFromSheardPreferanceString(key: "loginType") == "2" {
                    createEvent(key: "38", date: getDateTime())
                }else if getDataFromSheardPreferanceString(key: "loginType") == "3" {
                    createEvent(key: "43", date: getDateTime())
                }else if getDataFromSheardPreferanceString(key: "loginType") == "4" {
                    createEvent(key: "48", date: getDateTime())
                }
            }
            self.present(detailView, animated: true, completion: nil)
        }
    }
    
    func processDietOrderForTEEChild(){
           let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
           let bmi = getDataFromSheardPreferanceFloat(key: "bmi")
           var ob = tblCDCGrowthChild()
        print("oldyear \(oldyear)")
           tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: bmi ) { (respose, error) in
               ob = respose as! tblCDCGrowthChild
           }
           
           if ob.value >= 25 && ob.value <= 75 {
               setDataInSheardPreferance(value: "3", key: "dietType")
           }else if ob.value <= 25 {
               setDataInSheardPreferance(value: "1", key: "dietType")
           }else if ob.value > 75 {
               setDataInSheardPreferance(value: "2", key: "dietType")
           }
        
       }
    

}

 
extension TextInputViewController:DatePickerDelegate{
    func cancelBtnClicked() {
        self.view.endEditing(true)
    }
    
    func doneBtnClicked(){
        birthdate.resignFirstResponder()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: datePicker.date)
        let yourDate = formatter.date(from: myString)
        let myStringafd = formatter.string(from: yourDate!)
        ageChild = calclateAge(str: myString)
        birthdate.text = myStringafd
        ageChild = calclateAge(str: formatter.string(from: datePicker.date))
        print("age \(ageChild)")
        self.view.endEditing(true)
    }
}

extension UITextField {
    func isError( numberOfShakes shakes: Float, revert: Bool) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "shadowColor")
        
        animation.duration = 0.4
        if revert { animation.autoreverses = true } else { animation.autoreverses = false }
        self.layer.add(animation, forKey: "")
        
        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        if revert { shake.autoreverses = true  } else { shake.autoreverses = false }
        shake.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y))
        self.layer.add(shake, forKey: "position")
    }
}


extension TextInputViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
