//
//  SliderPopupController.swift
//  Ta5sees
//
//  Created by Admin on 9/22/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD
class SliderPopupController: UIViewController {
    
    var viewAccountPresnter:PresnterAccount?
    var incrementKACL:Float = 0.0
    var week:Float = 0.0
    var userInfo:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!

    var new_target:Double!
    override func viewDidLoad() {
        super.viewDidLoad()
      

//        self.navigationItem.hidesBackButton = true
        
        getInfoUser { [self] (item, error) in
            self.userInfo = item
            if self.new_target == -1.0 {
                self.new_target = Double(item.target)
            }
        }
        
        getInfoHistoryProgressUser { [self] (user, err) in
            self.user_ProgressHistory = user
            if self.new_target == -1.0 {
                self.new_target = Double(user_ProgressHistory.target)
            }
        }
        
        
        if userInfo.grown != "2"{
        let newBackButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(backButtonTapped(sender:)))
        self.navigationItem.rightBarButtonItem = newBackButton
            self.navigationItem.rightBarButtonItem?.tintColor = colorGray
        }
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
               tabBarController?.tabBar.barTintColor = .white
               tabBarController?.tabBar.tintColor = .white
     
        
        let gramsToLose:Float = (Float(user_ProgressHistory.gramsToLose))!

        let i = gramsToLose/1000.0
        let ii = abs((i - 0.2) / 0.06)
        
          print("ii \(ii)")

        seekbarOutlet.setValue(ii, animated: true)
        calculateWeek()

    }
    func calculateWeek(){
        
        week =  abs(Float((Double(user_ProgressHistory.weight)!
            - Double(new_target))) / (0.2 + (seekbarOutlet.value * 0.06)))
        print("week \(week)")
        lblWeek.text  = "(\(Int(round(week)))) اسابيع"
        let wekcoloreis = (0.2 + (seekbarOutlet.value * 0.06))
         incrementKACL = wekcoloreis * 1000
        if user_ProgressHistory.refPlanMasterID == "1" {
            lblPeriod.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }else{
            lblPeriod.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
        }
       

    }
    
    @IBOutlet weak var lblPeriod: UILabel!
    @IBOutlet weak var lblWeek: UILabel!
    @IBOutlet weak var seekbarOutlet: UISlider!
    @IBAction func seekbar(_ sender: Any) {

        
        let wekcoloreis = (0.2 + (seekbarOutlet.value * 0.06))

        week =  abs(Float(((user_ProgressHistory.weight as NSString).doubleValue
            - Double(new_target))) / wekcoloreis)
        
        incrementKACL = wekcoloreis * 1000
        
     
            
            lblWeek.text = "(\(Int(round(week)))) اسابيع"
            if user_ProgressHistory.refPlanMasterID == "1" {
                lblPeriod.text = "زيادة : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }else{
                lblPeriod.text = "تخسيس : \(Int(round(incrementKACL))) غم لكل اسبوع"
                
            }
    

    }
    
    @IBAction func btnSave(_ sender: Any) {
//        CheckInternet.checkIntenet { (bool) in
//            if !bool {
//                SVProgressHUD.dismiss()
//                showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
//                return
//            }
//        }
        print("week \(week) incrementKACL \(incrementKACL) ")
        
        tblUserProgressHistory.updateTargetWeekUser(userID: getUserInfo().id, target: String(new_target), weeksNeeded: String(Int(round(week))), gramsToLose: String(incrementKACL), date: getDateOnly()) {(res, err) in
// tblUserInfo.updateInfoUser(target:String(new_target),weeksNeeded:String(Int(round(week))),gramsToLose:String(incrementKACL)) { (res, err) in
//            if res == "success" {
//                if self.new_target == -1.0 {
//                    showToast(message: "تم تحديث الفترة", view: self.view,place:0)
//
//                }else{
//                    showToast(message: "تم تحديث الهدف و الفترة بنجاح", view: self.view,place:0)
//
//                }
//
                self.viewAccountPresnter?.setAlphaView(flag:true)
                self.dismiss(animated: true, completion: nil)
//            }else if res == "false" {
//                if self.new_target == -1.0 {
//                    showToast(message: "لم يتم تحديث الفترة", view: self.view,place:0)
//
//                }else{
//                    showToast(message: "لم يتم تحديث الهدف و الفترة بنجاح", view: self.view,place:0)
//
//                }
//                self.viewAccountPresnter?.setAlphaView(flag:false)
//                self.dismiss(animated: true, completion: nil)
//            }else{
//                showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
//
//            }
        }
        
       
    }
    @objc func backButtonTapped(sender: UIBarButtonItem) {
        viewAccountPresnter?.setAlphaView(flag: false)
        dismiss(animated: true, completion: nil)
        
    }
}


