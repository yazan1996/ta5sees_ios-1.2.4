//
//  tblUserNotification.swift
//  Ta5sees
//
//  Created by Admin on 10/14/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON

class tblUserNotification:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var title:String = ""
    @objc dynamic var body:String = ""
    @objc dynamic var id_user:String = ""
    @objc dynamic var notificationType:String = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
  
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserNotification.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    static func setNotifcation(name:String){
        let check = setupRealm().objects(tblUserNotification.self).filter("name == %@",name)
        if check.isEmpty {
        let item = tblHistoryNotFoundMeals()
        item.id = item.IncrementaID()
        item.name = name
        item.id_user = getDataFromSheardPreferanceString(key: "userID")
        
        try! setupRealm().write {
            setupRealm().add(item, update: Realm.UpdatePolicy.modified)
        }
        }
    }
    
   
    static func getAllNotify()->[tblUserNotification]{
        
        return Array(setupRealm().objects(tblUserNotification.self))
    }
    
}
