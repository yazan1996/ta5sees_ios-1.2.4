//
//  tblInsulinMedicines.swift
//  Ta5sees
//
//  Created by Admin on 9/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblInsulinMedicines:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription = ""
    @objc dynamic var refInsulinTypeID = 0
    @objc dynamic var refInsulinActingID = 0
    @objc dynamic var reftblInsulinTypeID:tblInsulinTypes?
    @objc dynamic var reftblInsulinActingID:tblInsulinActing?
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    static var array:[tblInsulinMedicines] = []
    static var list:Results<tblInsulinMedicines>!
    static var Desc :[String:String]=[:]
    
    static func getData() ->Array<String>{
        var arrayDisc:[String]=[]
        
        list = (setupRealm().objects(tblInsulinMedicines.self))
        array = Array(list)
        for x in 0..<list.count {
            arrayDisc.append(list[x].discription)
        }
        
        
        return arrayDisc
    }
    
    
    static func getItem(id:Int) ->Int{
        let ob:tblInsulinMedicines = setupRealm().objects(tblInsulinMedicines.self).filter("id == %@",id).first!
        return ob.refInsulinActingID
    }
    
    static func showMediction(completion: @escaping ([Item],Error?) -> Void) {
        var modelAry = [Item]()
        list = (setupRealm().objects(tblInsulinMedicines.self))
        for x in 0..<list.count {
            modelAry.append(Item(id: String(list[x].id), name: list[x].discription))
        }
        completion(modelAry,nil)
        
    }
    

    
    static func getDataID() ->Array<Int>{
        var arrayID:[Int]=[]
        
        list = (setupRealm().objects(tblInsulinMedicines.self))
        array = Array(list)
        for x in 0..<list.count {
            arrayID.append(list[x].id)
        }
        
        return arrayID
    }
  
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        rj.readJson(tableName: "insulin/tblInsulinMedicines") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
                   thread.sync {
            
            if let recommends = JSON(response!).array {
                try! setupRealm().write {
                for item in recommends {
                    let obj = tblInsulinMedicines(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"refInsulinTypeID" : item["refInsulinTypeID"].intValue,"refInsulinActingID" : item["refInsulinActingID"].intValue, "reftblInsulinActingID" : setupRealm().objects(tblInsulinActing.self).filter("id = %@",item["refInsulinActingID"].intValue).first!,"reftblInsulinTypeID" : setupRealm().objects(tblInsulinTypes.self).filter("id = %@",item["refInsulinTypeID"].intValue).first!])
                    print("Save MIDic")

                    setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                    }
                }
                completion(true)
            }
            }
            
        }
    }
    
}
