//
//  tblMealPlanerTime.swift
//  Ta5sees
//
//  Created by Admin on 8/30/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblMealPlanerTime:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var refSenfcat:String = ""
    @objc dynamic var time:String = ""
    @objc dynamic var flag:String = ""
   @objc dynamic var refUserId:String = ""

    
  override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblMealPlanerTime.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    static func sheckFound()->Bool{
        if setupRealm().objects(tblMealPlanerTime.self).filter("refUserId == %@",getDataFromSheardPreferanceString(key: "userID")).first != nil  {
            return true
        }else{
            return false
        }
    }
}
    
