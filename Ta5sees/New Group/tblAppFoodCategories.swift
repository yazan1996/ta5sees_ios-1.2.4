//
//  tblAppFoodCategories.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblAppFoodCategories:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
  
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        rj.readJson(tableName: "tblAppFoodCategories") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let obj = tblAppFoodCategories(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                            setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                    completion(true)
                }
            }
            
        }
    }
}
