//
//  tblItemsFavurite.swift
//  Ta5sees
//
//  Created by Admin on 9/9/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblItemsFavurite:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var refItem:String = ""
    @objc dynamic var flag:String = "" // 0 -> package ? 1 -> senf
    @objc dynamic var refUserId:String = ""
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblItemsFavurite.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func deleteItem(id:String,flag:String){
        do {
            
            let object = setupRealm().objects(tblItemsFavurite.self).filter("refItem == %@ AND refUserId == %@ AND flag == %@",id,getDataFromSheardPreferanceString(key: "userID"),flag)
            
            try setupRealm().write {
                setupRealm().delete(object)
                
            }
        } catch let error as NSError {
            // handle error
            print("error - \(error.localizedDescription)")
        }
    }
    
    static func deleteAllItem(completion: @escaping (Bool) -> Void){
        do {
            
            let object = setupRealm().objects(tblItemsFavurite.self)
            
            try setupRealm().write {
                setupRealm().delete(object)
                completion(true)
            }
        } catch let error as NSError {
            // handle error
            completion(false)
            print("error - \(error.localizedDescription)")
        }
    }
    
    
    static func getPackageDataFavurite(id:String)->tblPackeg{
        return setupRealm().objects(tblPackeg.self).filter("id == %@",Int(id)!).first!
    }
    
    static func getSenfDataFavurite(id:String)->tblSenf{
        return setupRealm().objects(tblSenf.self).filter("id == %@",Int(id)!).first!
    }
    
    
    static func getAllDataFavurite()->[tblItemsFavurite]{
        return Array(setupRealm().objects(tblItemsFavurite.self).filter("refUserId == %@",getDataFromSheardPreferanceString(key: "userID")))
    }
    static func checkItemsFavutite(id:String)-> Bool{
        
        
        let item:tblItemsFavurite! = setupRealm().objects(tblItemsFavurite.self).filter("refItem == %@",id).first
        
        if item == nil{
            return false
        }else{
            return true
        }
        
    }
}


