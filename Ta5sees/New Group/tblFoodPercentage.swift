//
//  tblFoodPercentage.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblFoodPercentage:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var refWajbehInfoID = ""
    @objc dynamic var percentage = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
   
    func readJson(){
        let rj = ReadJSON()
        rj.readJson(tableName: "others/tblFoodPercentage") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblFoodPercentage = tblFoodPercentage()
                        data.id = String(item["id"].intValue)
                        data.refWajbehInfoID = String(item["refWajbehInfoID"].intValue)
                        data.percentage = String(item["percentage"].intValue)
                        
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                }
            }
     
        }
    }
    
    
}
