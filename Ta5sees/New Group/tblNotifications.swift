//
//  tblNotifications.swift
//  Ta5sees
//
//  Created by Admin on 9/21/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblNotifications:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var title:String = ""
    @objc dynamic var body:String = ""
    @objc dynamic var date:String = ""
    @objc dynamic var type_Notfy = ""
    @objc dynamic var refUserID = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
  
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblNotifications.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func removeNotification(id:Int){
         let check = setupRealm().objects(tblNotifications.self).filter("id == %@",id)
        try! setupRealm().write {
              setupRealm().delete(check)
        }
    }
    
    static func setNotification(title:String,body:String,flag:String){
//        let check = setupRealm().objects(tblNotifications.self).filter("refUserID == %@ AND type_Notfy == %@",getDataFromSheardPreferanceString(key: "userID") ,flag)
//        if check.isEmpty {
        let item = tblNotifications()
        item.id = item.IncrementaID()
        item.title = title
        item.body = body
        item.date = getDateTime()
        item.type_Notfy = flag
        item.refUserID = getDataFromSheardPreferanceString(key: "userID")
        
        try! setupRealm().write {
            setupRealm().add(item, update: Realm.UpdatePolicy.modified)
            }
            
//        }else{
//            try! setupRealm().write {
//                check.setValue(title, forKey: "title")
//            }
//            try! setupRealm().write {
//                check.setValue(body, forKey: "body")
//            }
//            try! setupRealm().write {
//                check.setValue(getDateOnly(), forKey: "date")
//            }
//        }
    }
    
    static func deletNotify(id:Int){
        
        let items = setupRealm().objects(tblNotifications.self).filter("id == %@",id)
        try! setupRealm().write{
            setupRealm().delete(items)
        }
    }
    
    static func getAllNotify()->[tblNotifications]{
        
        return Array(setupRealm().objects(tblNotifications.self).sorted(byKeyPath: "id", ascending: false))
    }
    
}
