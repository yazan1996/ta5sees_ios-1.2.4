//
//  tblIBSSymptoms.swift
//  Ta5sees
//
//  Created by Admin on 9/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblIBSSymptoms:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    @objc dynamic var Symptoms: String = ""
    
    
    
    static var array:[tblIBSSymptoms] = []
    static var list:Results<tblIBSSymptoms>!
    static var Desc :[String:String]=[:]
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblIBSSymptoms]()
        rj.readJson(tableName: "others/tblIBSSymptoms") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblIBSSymptoms(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"Symptoms" :  item["Symptoms"].string!])
                        list.append(obj)
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
            
        }
    }
    
    static func getData() ->Array<String>{
        var arrayDisc:[String]=[]
        
        list = (setupRealm().objects(tblIBSSymptoms.self))
        array = Array(list)
        for x in 0..<list.count {
            arrayDisc.append(list[x].Symptoms)
        }
        
        
        return arrayDisc
    }
    
    static func getAllData(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblIBSSymptoms.self),nil)
    }
    
    static func getDataID() ->Array<Int>{
        var arrayID:[Int]=[]
        
        list = (setupRealm().objects(tblIBSSymptoms.self))
        array = Array(list)
        for x in 0..<list.count {
            arrayID.append(list[x].id)
        }
        
        return arrayID
    }
    
}
