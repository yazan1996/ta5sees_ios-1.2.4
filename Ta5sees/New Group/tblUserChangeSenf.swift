//
//  tblUserChangeSenf.swift
//  Ta5sees
//
//  Created by Admin on 9/28/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblUserChangeSenf:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var oldID = "0"
    @objc dynamic var updateID = "0"
    @objc dynamic var date = "0"
    @objc dynamic var idWajbeh = "0"
    @objc dynamic var userID = "0"


    override static func primaryKey() -> String? {
        return "id"
    }
    func IncrementaID() -> Int{
           return (setupRealm().objects(tblUserChangeSenf.self).max(ofProperty: "id") as Int? ?? 0) + 1
       }
    
    static func setChange(oldID:String,updateID:String,w:String,date:String,userID:String)->Bool{
        
        let userHistory = tblUserChangeSenf()
        userHistory.id  =  userHistory.IncrementaID()
        userHistory.oldID = oldID
        userHistory.updateID = updateID
        userHistory.idWajbeh = w
        userHistory.date = date
        userHistory.userID = userID
        
        try! setupRealm().write {
            setupRealm().add(userHistory)
        }
        return true
    }
    
    static func checkData(id:String,idW:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblUserChangeSenf.self).filter("oldID == %@ AND idWajbeh == %@",id,idW).sorted(byKeyPath: "id", ascending: false).first,nil)
       }
}


