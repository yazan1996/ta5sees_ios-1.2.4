//
//  tblMealDistribution.swift
//  Ta5sees
//
//  Created by Admin on 3/5/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblMealDistribution:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var Breakfast = 0
    @objc dynamic var Snack1 = 0
    @objc dynamic var Lunch = 0
    @objc dynamic var Snack2 = 0
    @objc dynamic var Dinner = 0
    @objc dynamic var isBreakfastMain  = 0
    @objc dynamic var isLunchMain = 0
    @objc dynamic var isDinnerMain = 0
    @objc dynamic var BreakfastPercentage = 0
    @objc dynamic var Snack1Percentage = 0
    @objc dynamic var LunchPercentage = 0
    @objc dynamic var Snack2Percentage = 0
    @objc dynamic var DinnerPercentage = 0
    
    override static func primaryKey() -> String? {
           return "id"
       }
    
    static func getItem()->tblMealDistribution{
        print(getUserInfo().mealDistributionId,"####")
        return setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first!
    }
    
    static func getItem(id:Int)->tblMealDistribution{
           return setupRealm().objects(tblMealDistribution.self).filter("id == %@",id).first!
       }
   static func getUserSnack1MealDistribution(tasber1:UIView){
    let ob = getItem()
    
    if ob.Snack1 == 0{
            tasber1.isUserInteractionEnabled = false
            tasber1.alpha = 0.5;
        }
        
    }
    static func getUserLunchMealDistribution(luansh:UIView){
        let ob = getItem()

        if ob.Lunch == 0 {
               luansh.isUserInteractionEnabled = false
               luansh.alpha = 0.5;
           }
        
       }
    
    static func getUserSnack2MealDistribution(tasber2:UIView){
        let ob = getItem()

        if ob.Snack2 == 0{
               tasber2.isUserInteractionEnabled = false
               tasber2.alpha = 0.5;
           }
          
       }
    static func getUserDinnerMealDistribution(dinner:UIView){
        let ob = getItem()

        if ob.Dinner == 0{
               dinner.isUserInteractionEnabled = false
               dinner.alpha = 0.5;
           }
       }
    static func getUserBreakfastMealDistribution(brekfast:UIView){
        let ob = getItem()
        if ob.Breakfast == 0{
         
             brekfast.isUserInteractionEnabled = false
             brekfast.alpha = 0.5;
         }
     }
    
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblMealDistribution]()
        rj.readJson(tableName: "others/tblMealDistribution") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblMealDistribution(value:["id" : item["id"].intValue, "Breakfast": item["Breakfast"].intValue,"Snack1" : item["Snack1"].intValue,"Lunch" :item["Lunch"].intValue,"Snack2" : item["Snack2"].intValue,"Dinner" : item["Dinner"].intValue,"isBreakfastMain" : item["isBreakfastMain"].intValue,"isLunchMain" : item["isLunchMain"].intValue, "isDinnerMain": item["isDinnerMain"].intValue,"BreakfastPercentage" : item["BreakfastPercentage"].intValue,"Snack1Percentage" : item["Snack1Percentage"].intValue,"LunchPercentage" : item["LunchPercentage"].intValue,"Snack2Percentage" : item["Snack2Percentage"].intValue,"DinnerPercentage" : item["DinnerPercentage"].intValue])
                        list.append(obj)
                            
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
            
        }
        
    }
        
        
        func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
            var list = [tblMealDistribution]()
                let thread =  DispatchQueue.global(qos: .userInitiated)
                thread.async {
                    if let recommends = JSON(response).array {
                        for item in recommends {
                            let obj = tblMealDistribution(value:["id" : item["id"].intValue, "Breakfast": item["Breakfast"].intValue,"Snack1" : item["Snack1"].intValue,"Lunch" :item["Lunch"].intValue,"Snack2" : item["Snack2"].intValue,"Dinner" : item["Dinner"].intValue,"isBreakfastMain" : item["isBreakfastMain"].intValue,"isLunchMain" : item["isLunchMain"].intValue, "isDinnerMain": item["isDinnerMain"].intValue,"BreakfastPercentage" : item["BreakfastPercentage"].intValue,"Snack1Percentage" : item["Snack1Percentage"].intValue,"LunchPercentage" : item["LunchPercentage"].intValue,"Snack2Percentage" : item["Snack2Percentage"].intValue,"DinnerPercentage" : item["DinnerPercentage"].intValue])
                            list.append(obj)
                                
                            }
                        }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }
                    completion(true)
                }
                
            
    }
}
