//
//  tblRecentUsed.swift
//  Ta5sees
//
//  Created by Admin on 9/12/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblRecentUsed:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var refItem:String = ""
    @objc dynamic var flag:String = "" // 0 -> package ? 1 -> senf
    @objc dynamic var refUserId:String = ""
    
  override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblRecentUsed.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    static func getPackageDataRecent(id:String)->tblPackeg{
           return setupRealm().objects(tblPackeg.self).filter("id == %@",Int(id)!).first!
       }
    
    static func getSenfDataRecent(id:String)->tblSenf{
              return setupRealm().objects(tblSenf.self).filter("id == %@",Int(id)!).first!
          }
    
    static func processInsert(id:String,flag:String){
        if !checkItemsFavutite(id: id, flag: flag) {
          insertItems(id:id,flag:flag)
        }else{
            insertItems(id:id,flag:flag)
            deleteItem(id:id,flag:flag)

        }
    }
    
    static func insertItems(id:String,flag:String){
        let ob = tblRecentUsed()
               ob.id = ob.IncrementaID()
               ob.refItem = id
               ob.flag = flag
               ob.refUserId = getDataFromSheardPreferanceString(key: "userID")
               try! setupRealm().write {
                   setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
//                   deleteItem(id:id,flag:flag)
               }
    }
    
    static func deleteItem(id:String,flag:String){
        do {
            let object = setupRealm().objects(tblRecentUsed.self).filter("refItem == %@ AND refUserId == %@ AND flag == %@",id,getDataFromSheardPreferanceString(key: "userID"),flag).first
            if setupRealm().isInWriteTransaction == true {
                setupRealm().delete(object!)
            }else {
            try setupRealm().write {
                setupRealm().delete(object!)
            }
            }
        } catch let error as NSError {
            print("error - \(error.localizedDescription)")
        }
    }
    
    
    static func deleteAllItem(completion: @escaping (Bool) -> Void){
           do {

               let object = setupRealm().objects(tblRecentUsed.self)

                     try setupRealm().write {
                             setupRealm().delete(object)
                         completion(true)
                     }
                 } catch let error as NSError {
                     // handle error
                    completion(false)
                     print("error - \(error.localizedDescription)")
                 }
       }
    
    
  
    
    static func getAllDataRecentUsed()->[tblRecentUsed]{
        return Array(setupRealm().objects(tblRecentUsed.self).filter("refUserId == %@",getDataFromSheardPreferanceString(key: "userID")).sorted(byKeyPath: "id", ascending: false))
       }
    static func checkItemsFavutite(id:String,flag:String)-> Bool{
        let item:tblRecentUsed! = setupRealm().objects(tblRecentUsed.self).filter("refItem == %@ AND refUserId == %@ AND flag == %@",id,getDataFromSheardPreferanceString(key: "userID"),flag).first

        if item == nil{
            return false
        }else{
            return true
        }

    }
}
    

