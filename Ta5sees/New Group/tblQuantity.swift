//
//  tblQuantity.swift
//  Ta5sees
//
//  Created by Admin on 8/26/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblQuantity:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var lowRatioFromOneHesa = 0.0
    @objc dynamic var ratioFromOneHesa = 0.0
    @objc dynamic var discription: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblQuantity]()
        print("tblQuantity")
        rj.readJson(tableName: "others/tblQuantity") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblQuantity(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"lowRatioFromOneHesa" : item["lowRatioFromOneHesa"].floatValue,"ratioFromOneHesa" : item["ratioFromOneHesa"].floatValue])
                        list.append(obj)
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
            
        }
    }
    
}
