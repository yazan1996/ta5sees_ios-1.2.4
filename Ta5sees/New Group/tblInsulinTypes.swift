//
//  tblInsulinTypes.swift
//  Ta5sees
//
//  Created by Admin on 9/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblInsulinTypes:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    @objc dynamic var refInsulinActingID = 0
    @objc dynamic var reftblInsulinActing:tblInsulinActing?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
  
    func readJson(completion: @escaping (Bool) -> Void){
            print("    print( self.setUpRealm().objects(tblInsulinTypes.self))11111")
        let rj = ReadJSON()
        rj.readJson(tableName: "insulin/tblInsulinTypes") {(response, Error) in
            let thread = DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let obj = tblInsulinTypes(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"refInsulinActingID" : item["refInsulinActingID"].intValue, "reftblInsulinActing" : setupRealm().objects(tblInsulinActing.self).filter("id = %@",item["refInsulinActingID"].intValue).first!])
                            setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                    completion(true)
                }
            }
            
        }
    }
    
}
