//
//  tblHypertensionRanges.swift
//  Ta5sees
//
//  Created by Admin on 9/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblHypertensionRanges:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription = ""
    @objc dynamic var lowFrom = 0
    @objc dynamic var lowTo = 0
    @objc dynamic var highFrom = 0
    @objc dynamic var highTo = 0
    
    
    
    
    static var array:[tblHypertensionRanges] = []
    static var list:Results<tblHypertensionRanges>!
    static var Desc :[String:String]=[:]
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    static func getAllData(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblHypertensionRanges.self),nil)
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        rj.readJson(tableName: "others/tblHypertensionRanges") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let obj = tblHypertensionRanges(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"lowFrom" : item["lowFrom"].intValue,"lowTo" : item["lowTo"].intValue,"highFrom" : item["highFrom"].intValue,"highTo" : item["highTo"].intValue])
                            setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                    completion(true)
                }
            }
            
        }
    }

}


