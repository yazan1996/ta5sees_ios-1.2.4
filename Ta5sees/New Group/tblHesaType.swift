//
//  tblHesaType.swift
//  Ta5sees
//
//  Created by Admin on 9/20/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblHesaType:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
   
    func readJson(){
        let rj = ReadJSON()
        rj.readJson(tableName: "hesa/tblHesaType") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let obj = tblHesaType(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                            setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                            
                        }
                    }
                }
            }
            
        }
    }}

