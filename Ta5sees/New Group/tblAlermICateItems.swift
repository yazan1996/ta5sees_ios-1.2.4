//
//  tblAlermICateItems.swift
//  Ta5sees
//
//  Created by Admin on 9/16/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON

class tblAlermICateItems:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var cateItem:String = ""
    @objc dynamic var alermHour:Int = -1// 0 -> package ? 1 -> senf
    @objc dynamic var alermMinuet:Int = -1
    @objc dynamic var isActive = "-1" // 0 -> active ? 1 -> not
    @objc dynamic var refUserID = "-1"
    
    override static func primaryKey() -> String? {
        return "id"
    }
  
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblAlermICateItems.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func DeleteAll(){
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem IN %@ AND refUserID == %@",["1","2","3"],getDataFromSheardPreferanceString(key: "userID"))
        try! setupRealm().write{
            setupRealm().delete(item)
        }
    }
    static func DeleteAll1(){
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem IN %@ AND refUserID == %@",["1","2","3","4","5","6","7","8"],getDataFromSheardPreferanceString(key: "userID"))
        try! setupRealm().write{
            setupRealm().delete(item)
        }
    }
    static func checkAlermUser(refUserID:String)-> Bool{
        
        let item:tblAlermICateItems! = setupRealm().objects(tblAlermICateItems.self).filter("refUserID == %@",refUserID).first
        
        if item == nil{
            return false
        }else{
            return true
        }
        
    }
    static  func setalertNotification(hour:Int,min:Int,cateItem:String,isActive:String){
        let ob = tblAlermICateItems()
        ob.id = ob.IncrementaID()
        ob.alermHour = hour
        ob.alermMinuet = min
        ob.cateItem = cateItem
        ob.isActive = isActive
        ob.refUserID = getDataFromSheardPreferanceString(key: "userID")
        try! setupRealm().write {
            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
        }
        
    }
    static func disableAllNotfy(){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).first
        if item == nil {
            return
        }
        try! setupRealm().write {
          item!.isActive = "1"
           
        }
    }
    
    static func updateAlertNotification1(cateItem:String,hour:Int,min:Int,isActive:String){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
        try! setupRealm().write {
            item?.setValue(hour, forKey: "alermHour")
            item?.setValue(min, forKey: "alermMinuet")
            item?.setValue(isActive,forKey: "isActive")
           
        }
    }
    static func updateAlertNotification(cateItem:String,hour:Int,min:Int){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
        try! setupRealm().write {
            item?.setValue(hour, forKey: "alermHour")
            item?.setValue(min, forKey: "alermMinuet")

            if  item!.isActive == "0" {
//                item!.isActive = "1"
                item?.setValue("1",forKey: "isActive")
            }else{
                item?.setValue("0",forKey: "isActive")
//                item!.isActive = "0"
            }
        }
    }
    
    static func updateAlerttimeNotification(cateItem:String,hour:Int,min:Int){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
       
        try! setupRealm().write {
            item?.alermHour = hour
        }
        try! setupRealm().write {
            item?.alermMinuet = min
        }
       
    }
    
    static func updateAlerttimeNotificationActivation(cateItem:String,hour:Int,min:Int){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
        try! setupRealm().write {
            item?.alermHour = hour
        }
        try! setupRealm().write {
            item?.alermMinuet = min
        }
        try! setupRealm().write {
            
            item?.isActive = "0"
        }
        
    }
    static func getNotfyInfo(cateItem:String)->tblAlermICateItems{
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
        return item!
    }

    static func setUserAlarm() {
        
        let alarms = Array(setupRealm().objects(tblAlermICateItems.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")))
        var titleNotfy = ""
        for alarm in alarms {
            if  alarm.isActive == "0" {
                if alarm.cateItem == "1" {
                    titleNotfy =  getTitleNotify(keyRemote:"breakfastRemindersTitles",keyValue:"breakfastTitle",sheardKey:"breakfastTitleCounter")
                }else if alarm.cateItem == "2" {
                    titleNotfy =  getTitleNotify(keyRemote:"launchRemindersTitle",keyValue:"launchTitle",sheardKey:"launchTitleCounter")
                }else if alarm.cateItem == "3" {
                    titleNotfy =  getTitleNotify(keyRemote:"dinnerRemindersTitles",keyValue:"dinnerTitle",sheardKey:"dinnerTitleCounter")
                }else if alarm.cateItem == "4" {
                    titleNotfy =  getTitleNotify(keyRemote:"snack1RemindersTitles",keyValue:"snack1Title",sheardKey:"snack1TitleCounter")
                }else if alarm.cateItem == "5" {
                    titleNotfy =  getTitleNotify(keyRemote:"snack2RemindersTitles",keyValue:"snack2Title",sheardKey:"snack2TitleCounter")
                }else if alarm.cateItem == "6" {
                    titleNotfy =  getTitleNotify(keyRemote:"exerciseRemindersTitles",keyValue:"exerciseTitle",sheardKey:"exerciseTitleCounter")
                }else if alarm.cateItem == "7" {
                    titleNotfy =  getTitleNotify(keyRemote:"waterRemindersTitles",keyValue:"waterTitle",sheardKey:"waterTitleCounter")
                }else if alarm.cateItem == "8" {
                    titleNotfy =  getTitleNotify(keyRemote:"weightRemindersTitles",keyValue:"weightTitle",sheardKey:"weightTitleCounter")
                }
                showNotify(id: alarm.cateItem, body:titleNotfy , hour: alarm.alermHour, minut: alarm.alermMinuet)
            }
        }
        
    }
    
    
    
    static func updateAlertNotificationActive(cateItem:String){
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateItem,getDataFromSheardPreferanceString(key: "userID")).first
        try! setupRealm().write {
            if item!.isActive == "0" {
                item!.isActive =  "1"
            }else{
                item!.isActive = "0"
            }
            
        }
    }
    static func getalertNotificationActive(cateID:String)->String{
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateID,getDataFromSheardPreferanceString(key: "userID")).first
//        print("getalertNotificationActive",item)
        if item != nil {
            
            return "\(item!.isActive)"
            
        }else{
            return "1"
            
        }
        
    }
    
    static func getDataAlertNotification(cateID:String)->tblAlermICateItems{
        print("TTTT \(getDataFromSheardPreferanceString(key: "userID"))  \(cateID)")

        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateID,getUserInfo().id).first!
        
        return item
    }
    static func getalertNotification(cateID:String)->String{
        var isMorning = ""
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateID,getDataFromSheardPreferanceString(key: "userID")).first
        if item != nil {
            var hour = 0
            var minuet = ""
            
            if item!.alermHour > 12 {
                isMorning = "PM"
                hour = item!.alermHour - 12
            }else if item!.alermHour == 12 {
                isMorning = "PM"
                hour =  12
            }else{
                isMorning = "AM"
                hour = item!.alermHour
            }
            if item!.alermMinuet == 0 {
                minuet = "00"
            }else {
                minuet = String(item!.alermMinuet)
            }
            return "\(hour):\(minuet) \(isMorning)"
            
        }else{
            return "حدث خطا"
            
        }
        
    }
    static func getalertNotificationForText(cateID:String)->String{
//        var isMorning = ""
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateID,getDataFromSheardPreferanceString(key: "userID")).first
        if item != nil {
//            var hour = 0
//            var minuet = ""
            
//            if item!.alermHour > 12 {
//                isMorning = "PM"
//                hour = item!.alermHour - 12
//            }else if item!.alermHour == 12 {
//                isMorning = "PM"
//                hour =  12
//            }else{
//                isMorning = "AM"
//                hour = item!.alermHour
//            }
//            if item!.alermMinuet == 0 {
//                minuet = "00"
//            }else {
//                minuet = String(item!.alermMinuet)
//            }
            return "\( item!.alermHour):\( item!.alermMinuet)"
            
        }else{
            return "\(0):\(0)"

        }
        
    }
    static func getalertNotificationAsNumer(cateID:String)->[Int]{
        
        let item = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",cateID,getDataFromSheardPreferanceString(key: "userID")).first
        if item != nil {
//            var hour = 0
//            var minuet = 0
            
//            if item!.alermHour > 12 {
//                hour = item!.alermHour - 12
//            }else{
//                hour = item!.alermHour
//
//            }
//            if item!.alermMinuet == 0 {
//                minuet = 0
//            }else {
//                minuet = item!.alermMinuet
//            }
//            print("original")
            return [item!.alermHour,item!.alermMinuet]
            
        }else{
            print("not original")
            return [0,0]
            
        }
        
    }
    static func getalertNotificationAsHours(id:String)->Int{
                
        let items = setupRealm().objects(tblAlermICateItems.self).filter("cateItem == %@ AND refUserID == %@",id,getDataFromSheardPreferanceString(key: "userID")).first
        if items != nil {
            print(" Hours \(items!.alermHour)")
            return items!.alermHour
            
        }else{
            print("not original")
            return 0
            
        }
        
    }
    static func getalertNotificationAsTime(id:String)->[AlermICateItems]{
        
        var time = ""
        let dateFormatter = DateFormatter()
        var arrTime=[AlermICateItems]()
        
        let items = setupRealm().objects(tblAlermICateItems.self).filter("NOT cateItem IN %@ AND refUserID == %@",[id,"6","7","8","9"],getDataFromSheardPreferanceString(key: "userID"))
        if !items.isEmpty {
            for item in items {
       
                time = "\(item.alermHour):\(item.alermMinuet)"
                print("original \(time)")
                
                dateFormatter.dateFormat = "HH:mm"
            if let date = dateFormatter.date(from:time) {
                print("time ********** \(dateFormatter.string(from: date))")
                arrTime.append(AlermICateItems(id: item.cateItem, time: date.timeIntervalSince1970))
//                arrDate.append(date.timeIntervalSince1970)
            }
            }
            return arrTime
            
        }else{
            print("not original")
            return [AlermICateItems()]
            
        }
        
    }
}


struct AlermICateItems {
    var id:String!
    var time:Double
    
    init(id:String,time:Double) {
        self.id = id
        self.time = time
    }
    init() {
        self.id = "0"
        self.time = 0.0
    }
}
