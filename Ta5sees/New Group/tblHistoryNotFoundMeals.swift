//
//  tblHistoryNotFoundMeals.swift
//  Ta5sees
//
//  Created by Admin on 10/12/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblHistoryNotFoundMeals:Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var name:String = ""
    @objc dynamic var id_user:String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
  
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblHistoryNotFoundMeals.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    static func setMeals(name:String){
        let check = setupRealm().objects(tblHistoryNotFoundMeals.self).filter("name == %@",name)
        if check.isEmpty {
        let item = tblHistoryNotFoundMeals()
        item.id = item.IncrementaID()
        item.name = name
        item.id_user = getDataFromSheardPreferanceString(key: "userID")
        
        try! setupRealm().write {
            setupRealm().add(item, update: Realm.UpdatePolicy.modified)
        }
        }
    }
    
   
    static func getAllNotify()->[tblHistoryNotFoundMeals]{
        
        return Array(setupRealm().objects(tblHistoryNotFoundMeals.self))
    }
    
}
