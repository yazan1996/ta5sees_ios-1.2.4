//
//  tblHesaValues.swift
//  Ta5sees
//
//  Created by Admin on 9/20/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblHesaValues:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblHesaValues]()
        rj.readJson(tableName: "hesa/tblHesaValues") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblHesaValues(value: ["id" : item["id"].intValue, "discription": item["description"].stringValue])
                        
                        list.append(obj)
                        
                    }
                    

                    if setupRealm().isInWriteTransaction == true {
                        
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }else {
                        try! setupRealm().write {
                            
                            
                            setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        }
                            
                        }
                    }
                }
                    completion(true)
                
                
            }
            
        }
    }
