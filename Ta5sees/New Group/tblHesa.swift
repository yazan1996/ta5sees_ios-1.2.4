//
//  tblHesa.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblHesa:Object {
    
    @objc dynamic var id = -1
    @objc dynamic var discription = " "
    @objc dynamic var weight = 0
    @objc dynamic var hesa2 = " "
    @objc dynamic var hesa3 = " "
    @objc dynamic var hesa4 = " "
    @objc dynamic var hesa5 = " "
    @objc dynamic var hesa6 = " "
    
    override static func primaryKey() -> String? {
        return "id"
    }
    

    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblHesa]()
        rj.readJson(tableName: "hesa/tblHesa") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblHesa = tblHesa()
                        data.id = item["id"].intValue
                        data.discription = item["description"].string!
                        data.weight = item["weight"].intValue
                        data.hesa2 = item["hesa2"].string!
                        data.hesa3 = item["hesa3"].string!
                        data.hesa4 = item["hesa4"].string!
                        data.hesa5 = item["hesa5"].string!
                        data.hesa6 = item["hesa6"].string!
                        list.append(data)
                            
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
            
            //            print("response : \(response!)")
            //            print("error : \(Error as Any)")
        }
    }
    
}
