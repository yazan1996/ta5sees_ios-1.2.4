//
//  tblNewWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 7/16/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON

class tblNewWajbeh:Object {
    
    
    @objc dynamic var id:Int = 0
    @objc dynamic var userID:Int = 0
    @objc dynamic var name:String? = ""
    @objc dynamic var quantity:String? = ""
    @objc dynamic var senf:String? = ""
    @objc dynamic var barcode:String? = ""
    @objc dynamic var calories:String? = ""
    @objc dynamic var cho:String? = ""
    @objc dynamic var protien:String? = ""
    @objc dynamic var fat:String? = ""

    convenience init(userID:Int  ,name: String ,quantity:String ,senf:String ,barcode:String ,calories:String ,cho:String ,protien:String ,fat:String) {
        self.init()
        self.id = IncrementaID()
        self.name = name
        self.fat = fat
        self.protien = protien
        self.calories = calories
        self.cho = cho
        self.quantity = quantity
        self.senf = senf
        self.barcode = barcode
        self.userID = userID
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblNewWajbeh.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
}


