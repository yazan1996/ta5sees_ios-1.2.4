//
//  tblFatInfo.swift
//  Ta5sees
//
//  Created by Admin on 9/20/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblFatInfo:Object {
    //
    @objc dynamic var id = 0
    @objc dynamic var name: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    

    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblFatInfo]()
        rj.readJson(tableName: "others/tblFatInfo") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblFatInfo(value: ["id" : item["id"].intValue, "name": item["name"].string!])
                        list.append(obj)
                            
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
            
        }
    }}
