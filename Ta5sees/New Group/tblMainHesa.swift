//
//  tblMainHesa.swift
//  Ta5sees
//
//  Created by Admin on 9/20/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class tblMainHesa:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func getMainHisaSenf(id:Int,completion: @escaping (String) -> Void){
        let item = setupRealm().objects(tblMainHesa.self).filter("id == %@").first
        
        completion(item!.description)
    }
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblMainHesa]()
        rj.readJson(tableName: "hesa/tblMainHesa") {(response, Error) in
//            let thread =  DispatchQueue.global(qos: .userInitiated)
//            thread.async {
                if let recommends = JSON(response!).array {
                        for item in recommends {
                            let obj = tblMainHesa(value: ["id" : item["id"].intValue, "discription": item["description"].string!]) 
                            list.append(obj)
                            
                        }
                    }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    completion(true)
//                }
            }
            
        }
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblMainHesa]()
        if let recommends = JSON(response).array {
            for item in recommends {
                let obj = tblMainHesa(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                list.append(obj)
                
            }
        }
        try! setupRealm().write {
            setupRealm().add(list, update: Realm.UpdatePolicy.modified)
        }
        completion(true)

    }
    
}
