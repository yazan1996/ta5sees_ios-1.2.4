//
//  tblShoppingList.swift
//  Ta5sees
//
//  Created by Admin on 2/18/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblShoppingList:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var userID = ""
    @objc dynamic var senfID = 0
    @objc dynamic var weekID = "" //rename to date
    @objc dynamic var isCheck = 0
    @objc dynamic var quantity = ""
    @objc dynamic var nameSenf = ""
    @objc dynamic var packgeID = 0
    @objc dynamic var date = 0.0

    static let shared = tblShoppingList()
    
    /*
     var latestIndex = self.IncrementaID()
     while true {
         if (setupRealm().object(ofType: tblShoppingList.self, forPrimaryKey:latestIndex) == nil){
             self.id = latestIndex
             self.userID = getUserInfo().id
             self.weekID = date
             self.senfID = senfID
             self.quantity = quantity
             self.isCheck = isCheck
             self.nameSenf = nameSenf
             self.packgeID = packgeID
             self.date = dateDouble
             try! setupRealm().write{
                 setupRealm().add(self,update: .modified)
             }
             break
         }
         latestIndex+=1
     }
     */
    convenience init(id:Int,date:String,quantity:String,senfID:Int,isCheck:Int,nameSenf:String,packgeID:Int,dateDouble:Double) {
        self.init()
        
        self.id = self.IncrementaID()
        self.userID = getUserInfo().id
        self.weekID = date
        self.senfID = senfID
        self.quantity = quantity
        self.isCheck = isCheck
        self.nameSenf = nameSenf
        self.packgeID = packgeID
        self.date = dateDouble
        try? setupRealm().write{
            setupRealm().add(self,update:.modified)
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblShoppingList.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    func IncrementaWeekID() -> Int{
        return (setupRealm().objects(tblShoppingList.self).filter("userID == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!).max(ofProperty: "weekID") as Int? ?? 0) + 1
    }
    static func gteWeekID() -> Int{
        return (setupRealm().objects(tblShoppingList.self).filter("userID == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!).max(ofProperty: "weekID") as Int? ?? 0)
    }
   
    
    
//    static func createNewRow(userID:Int,senfID:Int,idWeek:Int){
//        let isFound = setupRealm().objects(tblShoppingList.self).filter("senfID == %@ AND weekID == %@ AND userID == %@",senfID,gteWeekID(),Int(getDataFromSheardPreferanceString(key: "userID"))!).first
//        if isFound == nil {
//            let ob = tblShoppingList()
//            ob.id = ob.IncrementaID()
//            ob.userID = userID
//            ob.weekID = idWeek
//            ob.senfID = senfID
//            try! setupRealm().write {
//                setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
//            }
//        }
//    }
//
    
    func getAllIngridentInShoppingList(rang:Int,completion:([shoppingModel])->Void){
        var arr = [shoppingModel]()
        var arr2 = [tblShoppingList]()
        var arr3 = [tblShoppingList]()
        var arr1 = [Int]()
        for day in 0...rang {
            let senf:[tblShoppingList] = Array(setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND weekID == %@ AND isCheck == %@",getDataFromSheardPreferanceString(key: "userID"),Date().getDays(value:day),0))//change week to date
            arr2.append(contentsOf: senf)
           
        }
//        print("arr2",arr2)
        var name = arr2.map {$0.nameSenf.trimmingCharacters(in: CharacterSet(charactersIn: " "))}.removeDuplicates()
//        print("name",name)
        if !arr2.isEmpty {
            for x in arr2 {
                
                if name.contains(x.nameSenf) {
                    if let index = name.firstIndex(of: x.nameSenf) {
                        print("Found peaches at index \(index)")
                        name.remove(at: index)
                        arr3.append(x)
                    }
                }else{
                    print("Found peaches")

                }
                
            }
            
        }
        
//        print("arr2",arr2.count,arr3)
        arr1 = arr3.map {$0.senfID}.removeDuplicates()
     
        for i in arr1 {
            
            let senf:[tblShoppingList] = Array(setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND nameSenf == %@ AND isCheck == %@ AND date BETWEEN {%@, %@}",getDataFromSheardPreferanceString(key: "userID"),setupRealm().objects(tblShoppingList.self).filter("senfID == %@",i).first?.nameSenf ?? "",0,arr2.first!.date,arr2.last!.date))//change week to date
            
            if !senf.isEmpty {
                
                arr.append(shoppingModel(userID: getDataFromSheardPreferanceString(key: "userID"), senfID: senf[0].senfID, weekID: senf[0].weekID, isCheck: senf[0].isCheck, quantity: "\(senf.reduce(0, {$0 + Double($1.quantity)!}))", nameSenf: senf[0].nameSenf, packgeID: senf.map{$0.packgeID}, date: [arr2.first!.date,arr2.last!.date]))
            }
        }
        
        completion(arr)
    }
    
    func getAllIngridentInFridge(rang:Int,completion:([shoppingModel])->Void){
            var arr = [shoppingModel]()
            var arr2 = [tblShoppingList]()
            var arr3 = [tblShoppingList]()
            var arr1 = [Int]()
            for day in 0...rang {
                let senf:[tblShoppingList] = Array(setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND weekID == %@ AND isCheck == %@",getDataFromSheardPreferanceString(key: "userID"),Date().getDays(value:day),1))//change week to date
                arr2.append(contentsOf: senf)
               
            }
    //        print("arr2",arr2)
            var name = arr2.map {$0.nameSenf.trimmingCharacters(in: CharacterSet(charactersIn: " "))}.removeDuplicates()
    //        print("name",name)
            if !arr2.isEmpty {
                for x in arr2 {
                    
                    if name.contains(x.nameSenf) {
                        if let index = name.firstIndex(of: x.nameSenf) {
                            print("Found peaches at index \(index)")
                            name.remove(at: index)
                            arr3.append(x)
                        }
                    }else{
                        print("Found peaches")

                    }
                    
                }
                
            }
            
    //        print("arr2",arr2.count,arr3)
            arr1 = arr3.map {$0.senfID}.removeDuplicates()
         
            for i in arr1 {
                
                let senf:[tblShoppingList] = Array(setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND nameSenf == %@ AND isCheck == %@ AND date BETWEEN {%@, %@}",getDataFromSheardPreferanceString(key: "userID"),setupRealm().objects(tblShoppingList.self).filter("senfID == %@",i).first?.nameSenf ?? "",1,arr2.first!.date,arr2.last!.date))//change week to date
                
                if !senf.isEmpty {
                    
                    arr.append(shoppingModel(userID: getDataFromSheardPreferanceString(key: "userID"), senfID: senf[0].senfID, weekID: senf[0].weekID, isCheck: senf[0].isCheck, quantity: "\(senf.reduce(0, {$0 + Double($1.quantity)!}))", nameSenf: senf[0].nameSenf, packgeID: senf.map{$0.packgeID}, date: [arr2.first!.date,arr2.last!.date]))
                }
            }
            
            completion(arr)
        }
    func updateRow(id:Int,value:Int,callBack: (String)->Void){
        let ob = setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND id == %@ ",getDataFromSheardPreferanceString(key: "userID"),id)
      
        try! setupRealm().write {
            ob.setValue(value,forKey: "isCheck")
            callBack("success")
        }
    }
    
    func updateRow1(name:String,date:[Double],value:Int,callBack: (String)->Void){
        let ob = setupRealm().objects(tblShoppingList.self).filter("userID == %@ AND nameSenf == %@ AND date BETWEEN {%@, %@}",getUserInfo().id,name,date[0],date[1])
        try! setupRealm().write {
            ob.setValue(value,forKey: "isCheck")
            callBack("success")

        }
    }
}

class shoppingModel {
    var userID = ""
    var senfID = 0
    var weekID = "" //rename to date
    var isCheck = 0
    var quantity = ""
    var nameSenf = ""
    var packgeID = [Int]()
    var date = [Double]()

    init(userID:String,senfID:Int,weekID:String,isCheck:Int,quantity:String,nameSenf:String,packgeID:[Int],date:[Double]) {
        self.userID = userID
        self.senfID = senfID
        self.weekID = weekID //rename to date
        self.isCheck = isCheck
        self.quantity = quantity
        self.nameSenf = nameSenf
        self.packgeID = packgeID
        self.date = date
    }
}
extension Sequence  {
    func sum<T: AdditiveArithmetic>(_ predicate: (Element) -> T) -> T { reduce(.zero) { $0 + predicate($1) } }
}
