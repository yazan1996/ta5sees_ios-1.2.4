//
//  FavuriteRecentController.swift
//  Ta5sees
//
//  Created by Admin on 9/11/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class FavuriteRecentController: UIViewController{
    var obShowNewWajbeh:ShowNewWajbeh?
    var id_items:String!
    var date:String!
    var pg:protocolWajbehInfoTracker?
    var id_category = ""

    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func dismes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func removeall(_ sender: Any) {
        tblItemsFavurite.deleteAllItem { (bool) in
            print(bool)
            self.filterItem.removeAll()
            self.tableView.reloadData()
            showToast(message: "تم حذف جميع المفضلة", view: self.view,place:0)
        }
    }
    var filterItem = tblItemsFavurite.getAllDataFavurite()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        obShowNewWajbeh = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        filterItem = tblItemsFavurite.getAllDataFavurite()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DisplayCateItemsController" {
            if let dis=segue.destination as?  DisplayCateItemsController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                    tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_category, date: date, isProposal: "0", completion: { (id,bool) in
                        print("bool \(bool)")
                        dis.idItemsEaten = id
                        if bool == true {
                            dis.isEaten = true
                            
                        }
                        else{
                            dis.isEaten = false
                        }
                    })
                }
            }
        }else if segue.identifier == "DisplayCateItemsSenfController" {
            if let dis=segue.destination as?  DisplayCateItemsSenfController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                    tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_category, date: date, isProposal: "0", completion: { (id,bool) in
                        dis.idItemsEaten = id
                        print("bool \(bool)")
                        if bool == true {
                            dis.isEaten = true
                        }else{
                            dis.isEaten = false
                        }
                    })
                }
            }
        }
        
    }
    
    
}
extension FavuriteRecentController:ShowNewWajbeh{
    func goToViewWajbeh(idWajbeh: String, flag: String) {
        
        if flag == "wajbeh" || flag == "0"{
            self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
        }else {
            self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
        }
        //        txtSearchOutlet.text = nil
        
    }
    
    
    
}

extension FavuriteRecentController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterItem.isEmpty {
            TableViewHelper.EmptyMessage(message: "لا يوجد لديك مفضله", viewController: self.tableView)
            return 0
        }
        return filterItem.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FavuriteCell = tableView.dequeueReusableCell(withIdentifier: "FavuriteCell", for: indexPath) as! FavuriteCell
        
        if filterItem[indexPath.row].flag == "0" {
            let item = tblItemsFavurite.getPackageDataFavurite(id: filterItem[indexPath.row].refItem)
            cell.name.text = item.name
            cell.carbo.text =  "\(Int(round(Double(item.carbohydrate!)!))) غم من الكربوهيدات"
            cell.proten.text =  "\(Int(round(Double(item.protein!)!))) غم من البروتين"
            cell.fat.text =  "\(Int(round(Double(item.fat!)!))) غم من الدهون"
            cell.caloris.text =  "\(Int(round(Double(item.calories!)!))) سعرة حرارية"
            
        }else{
            let item = tblItemsFavurite.getSenfDataFavurite(id: filterItem[indexPath.row].refItem)
            cell.name.text = item.trackerName
            cell.carbo.text =  "\(Int(round(Double(item.wajbehCarbTotal!)!))) غم من الكربوهيدات"
            cell.proten.text =  "\(Int(round(Double(item.wajbehProteinTotal!)!))) غم من البروتين"
            cell.fat.text =  "\(Int(round(Double(item.wajbehFatTotal!)!))) غم من الدهون"
            cell.caloris.text =  "\(Int(round(Double(item.wajbehCaloriesTotal!)!))) سعرة حرارية"
            
        }
        
        
        
        
        cell.imageSenf.image = UIImage(named : "logo")
        
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(filterItem[indexPath.row].refItem)")
        //                self.text = filterItem[indexPath.row].iteme_name
        obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filterItem[indexPath.row].refItem,flag:filterItem[indexPath.row].flag)
        if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tblItemsFavurite.deleteItem(id: filterItem[indexPath.row].refItem, flag: filterItem[indexPath.row].flag)
            self.filterItem.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            tableView.reloadData()
        }
    }
}

