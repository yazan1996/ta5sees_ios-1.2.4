//
//  newRegisterStep3.swift
//  Ta5sees
//
//  Created by Admin on 8/19/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD
import FirebaseFirestore
 
class VerificationCode: UIViewController,UIViewControllerTransitioningDelegate,UIAlertViewDelegate {
    
    @IBOutlet weak var textCode: RoundedTextField!
    @IBOutlet weak var lableCode: RoundedTextField!
    var numberPhone:String!
    var seconds = 60
    var timer = Timer()
    var obAPI = APiSubscribeUser()
    var isTimerRunning = false
    let onAPiSubscribeUser = APiSubscribeUser()
    @IBOutlet weak var labelTimer: UILabel!
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    @objc func updateTimer() {
        seconds -= 1
        if seconds >= 0 {
            
            labelTimer.text = timeString(time: TimeInterval(seconds))
        }else{
            reSendCodePin.textColor =  UIColor(red: 141/255, green: 222/255, blue: 187/255, alpha: 1.0)
            reSendCodePin.isEnabled = true
        }
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
        print("PHONE \(numberPhone!)")
        reSendCodePin.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        reSendCodePin.isUserInteractionEnabled = true

    }
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer){
        
        if reSendCodePin.isEnabled == true {
            timer.invalidate()
            sentCodePin(str:getDataFromSheardPreferanceString(key: "mobNum"))
            seconds = 10
            reSendCodePin.textColor =  UIColor(red: 156/255, green: 171/255, blue: 179/255, alpha: 1.0)
            reSendCodePin.isEnabled = false
            timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        }else {
            alert(mes:"يرجى الانتظار بعد ٦٠ ثانيه",selfUI:self)
        }
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        SVProgressHUD.show()
        let pin_orginal = textCode.textField.text!
        var pin = FormatterRoundedTextField(str:textCode).textField.text!
       
        if pin != pin_orginal && pin_orginal.count != pin.count {
            let lingth = pin_orginal.count - pin.count
            var pin_array = Array(pin)
            for x in 0..<lingth {
                pin_array.insert("0", at: x)
            }
            pin = String(pin_array)
            textCode.textField.text = "\(String(pin_array))"
            
        }

        readCodrPin(misdn:pin){ [self] bool in
            if bool {
                
                let syncConc = DispatchQueue(label:"con",attributes:.concurrent)
                
                syncConc.sync {
                    SVProgressHUD.show()
                    tblUserInfo.checkFoundIDUserMSISDN(iduser: numberPhone) { (bool) in
                        if bool == true {
                            if UserDefaults.standard.integer(forKey: "login") == 0 {
                                UserDefaults.standard.set(1, forKey: "login")
                                print("user login app ")
                                setDataInSheardPreferance(value: numberPhone, key: "userID")
                            }
                            setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                            tblAlermICateItems.setUserAlarm()
                            SVProgressHUD.dismiss {
                                onAPiSubscribeUser.updateSomDataUser(id:numberPhone, arr:["deviceType": "2","deviceToken": deviceTokens!,"appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                                self.performSegue(withIdentifier: "toHomeCV", sender: self)
                            }
                        }else {
                            self.checkdataFromFirestore()
                    
                        }
                    }
                }
    
            }else {
                alert(mes:"الرقم المدخل غير صحيح",selfUI:self)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        setBackground()
        setUPTextField()
        self.HideKeyboard()
      
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 3  {
            dismiss(animated: true, completion: nil)
        }
        
    }
    func setUPTextField(){
        if #available(iOS 12.0, *) {
            textCode.textField.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        changePlaceHolder(text: lableCode.textField,str:"كود التفعيل",flag:1)
        lableCode.textField.isEnabled = false
        lableCode.textField.textColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        textCode.textField.setLeftImage(imageName: "greenphone")
        textCode.textField.textAlignment = .center
        textCode.textField.textColor = UIColor(red: 123/255, green: 222/255, blue: 179/255, alpha: 1.0)
        textCode.textField.keyboardType = .numberPad
        
    }
    
    func changePlaceHolder(text:UITextField,str:String,flag:Int){
        if flag == 0 {
            text.attributedPlaceholder = NSAttributedString(string: str,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        }else{
            text.attributedPlaceholder = NSAttributedString(string: str,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          
//          if segue.identifier == "setPasswordCV" {
//              if let dis=segue.destination as? SetPasswordController{
//
//                  if  let number=sender as? String {
//                      dis.numberPhone = number
//                  }
//              }
//          }
      }

    
    
    
    
    func readCodrPin(misdn:String,callBack:@escaping (Bool)->Void){
        
        PhoneAuthVerfy.sheard.readCodPin(numberPhone: misdn) { (flag,err)  in
            SVProgressHUD.dismiss()
            if flag == 0 {
                if err.contains("17029") {
                    alert(mes:"انتهى الوقت المسموح لادخال الرمز" ,selfUI: self)
                }else if err.contains("17030") {
                    alert(mes:" رمز التشغيل غير صحيح" ,selfUI: self)
                }else{
                    alert(mes:"\(err)" ,selfUI: self)
                }
                callBack(false)
                return
            }
            callBack(true)
        }
    }
    
    func sentCodePin(str:String){
        SVProgressHUD.show()
        PhoneAuthVerfy.sheard.sentCodePinForgetPass(numberPhone: str) { (flag,err)  in
            SVProgressHUD.dismiss()
            if flag == 0 {
                alert(mes:"\(err)",selfUI: self)
                //                    "نعتذر لم يتم ارسال طلبك"
                return
            }
            alert(mes: "تم تنفيذ طلبك",selfUI: self)
        }
    }
    
    
    
    
    @IBOutlet weak var reSendCodePin: UILabel!
    
    
    
    
    
    
    
    func HideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    func checkdataFromFirestore(){
        //        dictUser["mobNum"] = numberPhone
        //        dictUser["loginType"] = "1"
        
        let isExist = db.collection("Users").document(numberPhone)
        let progressHistory = AppDelegate.db.collection("UserProgressHistory").document(numberPhone).collection(numberPhone)
        //        if isExist.isEqual(nil) {
        //            obAPI.saveUserFirestore(id:numberPhone)
        //        }
        isExist.getDocument { [self] (document, error) in
            if let document = document, document.exists {
                if  document.data()!.isEmpty{
                    goToGenderCVPage()
                }else{
                    tblUserInfo.saveUserInfoData(doc: document) { (tblUserInfo) in
                        print("save user success")
                        progressHistory.getDocuments { querySnapshot, err in
                            tblUserProgressHistory.saveUserProgressHistoryInfoData(docs: querySnapshot!.documents) { string in
                                if setupRealm().isInWriteTransaction {
                                    setupRealm().cancelWrite()
                                }
                        onAPiSubscribeUser.updateSomDataUser(id:numberPhone, arr:["deviceType": "2","deviceToken": deviceTokens!,"appVersion" : Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String])
                                
                                
                        setDataInSheardPreferance(value: "1", key: "isTutorial-wajbeh")
                        setDataInSheardPreferance(value: "2", key: "isTutorial-tamreen")
                        setDataInSheardPreferance(value: "1", key: "isTutorial-isBeign")
                        setDataInSheardPreferance(value: "2", key: "isTutorial-nutration")
                        setDataInSheardPreferance(value: "1", key: "isTutorial-water")
                        setDataInSheardPreferance(value: "1", key: "isTutorial")
                        //                            SVProgressHUD.dismiss()
                        _ = TryCalaulate(doc:document)
                        
                        if UserDefaults.standard.integer(forKey: "login") == 0 {
                            UserDefaults.standard.set(1, forKey: "login")
                            print("user login app ")
                        }
                        SVProgressHUD.dismiss {
                            setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                            self.performSegue(withIdentifier: "toHomeCV", sender: self)
                        }
                    }
                        }
                        
                    }
                }
            }else {
                goToGenderCVPage()
            }
        }
        
    }
    
    func formatDate(date:String)->String{
        var date_copy = ""
        if date == "" {
            date_copy = getDateOnly()
        }else{
            date_copy = date
        }
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd - MM - yyyy"
        let d = dateFormatter1.date(from: date_copy)
        let newFormate = dateFormatter.string(from: d!)
        return newFormate
    }
    
    func goToGenderCVPage(){
        dispatchGroup.enter()
        setDataInSheardPreferanceInt(value: 0, key: "counterRetryLoad")
        setDataInSheardPreferance(value: "1", key: "finishRegistration")
        setDataInSheardPreferance(value: numberPhone, key: "userID")
        Downloader.loadData_firebase(flag:1) { (bool) in
//            if getDataFromSheardPreferanceString(key:"loadWajbehSenf") == "0" {
            if bool {
                DispatchQueue.main.async {
                    UIApplication.shared.isIdleTimerDisabled = false
                }
//                dispatchGroup.leave()

                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dissmesDialog"), object: self, userInfo:nil)
                
                setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
                 print("loadData_firebase \(bool)")
            }else{
                print("loadData_firebase false")

            }
          
        }
//        obAPI.saveUserFirestore(id:numberPhone)
        SVProgressHUD.dismiss()
        goToGenderCV()
        print("new user")
    }
    func handelOpenPage(id:String){
        SVProgressHUD.dismiss()
        if getDataFromSheardPreferanceString(key: "reRegester-\(id)") == "1"{
             dictUser = getDataFromSheardPreferanceDic(key: "dictUser-\(id)")
            if dictUser["gender"] != nil && dictUser["UserAgeID"] != nil {
                setDataInSheardPreferance(value: dictUser["gender"] as! String, key: "gender")
                setDataInSheardPreferance(value: dictUser["gender"] as! String, key: "UserAgeID")
                self.performSegue(withIdentifier: "typeDiet_page2", sender: self)
            }else{
                goToGenderCV()
//self.performSegue(withIdentifier: "gender_page1", sender: self)
            }
        }else if getDataFromSheardPreferanceString(key: "reRegester-\(id)") == "2"{
             dictUser = getDataFromSheardPreferanceDic(key: "dictUser-\(id)")
            if dictUser["dietType"] != nil {
                setDataInSheardPreferance(value: dictUser["dietType"] as! String, key: "gender")
                self.performSegue(withIdentifier: "personalInfo_page3", sender: self)
            }else{
                self.performSegue(withIdentifier: "typeDiet_page2", sender: self)
            }
        }else if getDataFromSheardPreferanceString(key: "reRegester-\(id)") == "3"{
            dictUser = getDataFromSheardPreferanceDic(key: "dictUser-\(id)")
            self.performSegue(withIdentifier: "personalInfo_page3", sender: self)

        }else if getDataFromSheardPreferanceString(key: "reRegester-\(id)") == "4"{
            dictUser = getDataFromSheardPreferanceDic(key: "dictUser-\(id)")
            self.performSegue(withIdentifier: "lastStep_page4", sender: self)

        }else{
            goToGenderCV()
            
        }
    }
    
    
    func goToGenderCV(){
        setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
        setDataInSheardPreferance(value: "0", key: "isTutorial-tamreen")
        setDataInSheardPreferance(value: "0", key: "isTutorial-nutration")
        setDataInSheardPreferance(value: "0", key: "isTutorial")
        setDataInSheardPreferance(value: "0", key: "isTutorial-water")
        setDataInSheardPreferance(value: "0", key: "isTutorial-isBeign")
        
        setDataInSheardPreferance(value: "1", key: "finishRegistration")
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "GenderType") as! GenderType
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
        print("new user")
    }
}
    

extension UITextField{
    
    func setLeftImage(imageName:String) {
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.image = UIImage(named: imageName)
        
        self.rightView = imageView;
        self.rightViewMode = .always
    }
}
