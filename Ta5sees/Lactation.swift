//
//  Lactation.swift
//  Ta5sees
//
//  Created by Admin on 9/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import RealmSwift

 
class Lactation: UIViewController,UIViewControllerTransitioningDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}
func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return Items[row]
}
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    Items.count
}
func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let item: tblLactationMonths  =  arraylist.filter { $0.discription == Items[row] }.first!
    print(item.id)
    setDataInSheardPreferance(value:String(item.id), key: "LactationMonths")

   
}

    
    @IBOutlet weak var subView: UIView!
    
    var arraylist:[tblLactationMonths] = []
    var resultelist:Results<tblLactationMonths>!
      var Items:[String]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let detailView = storyboard!.instantiateViewController(withIdentifier: "FormatNaturalDiet") as! FormatNaturalDiet
           detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
           detailView.transitioningDelegate = self
           present(detailView, animated: true, completion: nil)
    }
    
    @IBOutlet weak var dataPicker: UIPickerView!
    @IBOutlet weak var titlePage: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setFontText(text:titlePage,size:30)
               titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        tblLactationMonths.getAllData() {(response, Error) in
            self.resultelist = (response) as? Results<tblLactationMonths>
            self.arraylist = Array(self.resultelist)
              self.Items = self.arraylist.map { $0.discription }
            setDataInSheardPreferance(value:"\(self.resultelist[1].id)", key: "LactationMonths")

            }
            dataPicker.selectRow(1, inComponent: 0, animated: true)
        
 
    }
    
  
    

    @IBAction func btnBack(_ sender: Any) {
          dismiss(animated: true, completion: nil)

      }
    
  
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
}
