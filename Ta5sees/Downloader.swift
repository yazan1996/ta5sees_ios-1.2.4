//
//  Downloader.swift
//  Ta5sees
//
//  Created by Admin on 12/3/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import ZIPFoundation
import SwiftyJSON
import Alamofire
import SVProgressHUD
class Downloader {
    
    static var threadGroup = DispatchGroup()
    static var file = FileManager()
    class func load(URL: NSURL,flag:Int,callBack:@escaping (Bool)->Void){
        var documentsUrl: URL {
            return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        }
        
        let file_PackageIngredients = "PackageIngredients.json"
        let file_Package = "Package.json"
        let file_Senf = "Senf.json"
        let file_PackageInfoItems = "PackageInfoItems.json"
        let file_IngredientRules = "IngredientRules.json"
        let file_PlateItems = "PlateItems.json"
        let file_PlatePerPackage = "PlatePerPackage.json"
        let file_PackagePlateUnits = "PackagePlateUnits.json"
        let file_PackageAllergies = "PackageAllergies.json"
        let file_PackageCuisines = "PackageCuisines.json"
        let file_UnwantedFruitsAndVegetablesSenfs = "UnwantedFruitsAndVegetablesSenfs.json"
        let file_IngredientRulesAllergy = "IngredientRulesAllergy.json"
        
        let file_AllergyList = "AllergyList.json"
        let file_CuisineList = "CuisineList.json"
        
        
        let localUrl_PackageIngredients = documentsUrl.appendingPathComponent(file_PackageIngredients)
        let localUrl_Package = documentsUrl.appendingPathComponent(file_Package)
        let localUrl_Senf = documentsUrl.appendingPathComponent(file_Senf)
        let localUrl_PlateItems = documentsUrl.appendingPathComponent(file_PlateItems)
        let localUrl_IngredientRules = documentsUrl.appendingPathComponent(file_IngredientRules)
        let localUrl_PackageInfoItems = documentsUrl.appendingPathComponent(file_PackageInfoItems)
        let localUrl_PlatePerPackage = documentsUrl.appendingPathComponent(file_PlatePerPackage)
        let localUrl_PackagePlateUnits = documentsUrl.appendingPathComponent(file_PackagePlateUnits)
        
        let localUrl_PackageAllergies = documentsUrl.appendingPathComponent(file_PackageAllergies)
        let localUrl_PackageCuisines = documentsUrl.appendingPathComponent(file_PackageCuisines)
        let localUrl_UnwantedFruitsAndVegetablesSenfs = documentsUrl.appendingPathComponent(file_UnwantedFruitsAndVegetablesSenfs)
        let localUrl_IngredientRulesAllergy = documentsUrl.appendingPathComponent(file_IngredientRulesAllergy)
        
        let localUrl_AllergyList = documentsUrl.appendingPathComponent(file_AllergyList)
        let localUrl_CuisineList = documentsUrl.appendingPathComponent(file_CuisineList)
        
        
        let sessionConfig = URLSessionConfiguration.ephemeral
        sessionConfig.timeoutIntervalForRequest = 60.0
        sessionConfig.timeoutIntervalForResource = 60.0
        sessionConfig.requestCachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL as URL)
        request.setValue("dietApp", forHTTPHeaderField: "uname")
        request.setValue("x58PUrTWgKGTcRUK", forHTTPHeaderField: "pwd")
        request.setValue("ios", forHTTPHeaderField: "type")
        request.timeoutInterval = 60.0
        //            if getDataFromSheardPreferanceString(key: "loadWajbehSenf") == "0" {
        
        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") > 4 {
            callBack(false)
        }
        let task = session.downloadTask(with: request) { (url, response,error) in
            let file = FileManager()
            if let tempLocalUrl = url, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Success: \(statusCode)")
                    if statusCode != 200 {
                        let syncConc = DispatchQueue(label:"con0",attributes:.concurrent)
                        
                        syncConc.async {
                            if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 0 {
                                reportDownloadDataError()
                            }
                        }
                        syncConc.async {
                            if getDataFromSheardPreferanceInt(key: "counterRetryLoad") < 4 {
                                if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
                                    SVProgressHUD.show(withStatus: "جاري محاولت تحميل بياناتك")
                                }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                                reTryRequestAPI()
                                setDataInSheardPreferanceInt(value:getDataFromSheardPreferanceInt(key: "counterRetryLoad")+1, key: "counterRetryLoad")
                                callBack(false)
                                return
                            }
                            }
                        }
                        return
                    }
                }
                
                guard let archive = Archive(url: tempLocalUrl, accessMode: .read) else  {
                    print("response .... not found file",getDataFromSheardPreferanceInt(key: "counterRetryLoad"))
                    let syncConc = DispatchQueue(label:"con2",attributes:.concurrent)
                    
                    syncConc.async {
                        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 0 {
                            reportDownloadDataError()
                        }
                        
                    }
                    syncConc.async {
                        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") < 4 {
                            if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
                                SVProgressHUD.show(withStatus: "جاري محاولت تحميل بياناتك")
                            }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                            reTryRequestAPI()
                            setDataInSheardPreferanceInt(value:getDataFromSheardPreferanceInt(key: "counterRetryLoad")+1, key: "counterRetryLoad")
                            callBack(false)
                            return
                        }
                        }
                    }
                    
                    return
                }
                
                
                guard let entry_PackageIngredients = archive["PackageIngredients.json"] else {
                    return
                }
                guard let entry_PlatePerPackage = archive["packagePlates.json"] else {
                    return
                }
                guard let entry_Packages = archive["Packages.json"] else {
                    return
                }
                guard let entry_Senf = archive["Senf.json"] else {
                    return
                }
                guard let entry_PlateItems = archive["PlateItems.json"] else {
                    return
                }
                guard let entry_IngredientRules = archive["IngredientRules.json"] else {
                    return
                }
                guard let entry_PackagePlateUnits = archive["PackagePlateUnits.json"] else {
                    return
                }
                guard let entry_PackageInfoItems = archive["PackageInfoItems.json"] else {
                    return
                }
                guard let entry_IngredientRulesAllergy = archive["IngredientRulesAllergy.json"] else {
                    return
                }
                guard let entry_AllergyList = archive["AllergyList.json"] else {
                    return
                }
                guard let entry_CuisineList = archive["CuisineList.json"] else {
                    return
                }
                guard let entry_UnwantedFruitsAndVegetablesSenfs = archive["UnwantedFruitsAndVegetablesSenfs.json"] else {
                    return
                }
                guard let entry_PackageCuisines = archive["PackageCuisines.json"] else {
                    return
                }
                guard let entry_PackageAllergies = archive["PackageAllergies.json"] else {
                    return
                }
                do {
                    //                    let r = try FileManager.default.copyItem(at: tempLocalUrl, to:localUrl)
                    
                    //                    let decodedData = try JSONDecoder().decode([tblPackeg].self,
                    //                                                               from: fileContent!)
                    if Downloader.checkFile(fileName:file_Senf) {
                        try file.removeItem(at: localUrl_Senf)
                    }
                    if Downloader.checkFile(fileName:file_PlatePerPackage) {
                        try file.removeItem(at: localUrl_PlatePerPackage)
                    }
                    if Downloader.checkFile(fileName:file_Package) {
                        try file.removeItem(at: localUrl_Package)
                    }
                    if Downloader.checkFile(fileName:file_PackageIngredients) {
                        try file.removeItem(at: localUrl_PackageIngredients)
                    }
                    if Downloader.checkFile(fileName:file_PackageInfoItems) {
                        try file.removeItem(at: localUrl_PackageInfoItems)
                    }
                    if Downloader.checkFile(fileName:file_IngredientRules) {
                        try file.removeItem(at: localUrl_IngredientRules)
                    }
                    if Downloader.checkFile(fileName:file_PlateItems) {
                        try file.removeItem(at: localUrl_PlateItems)
                    }
                    if Downloader.checkFile(fileName:file_PackagePlateUnits) {
                        try file.removeItem(at: localUrl_PackagePlateUnits)
                    }
                    
                    if Downloader.checkFile(fileName:file_UnwantedFruitsAndVegetablesSenfs) {
                        try file.removeItem(at: localUrl_UnwantedFruitsAndVegetablesSenfs)
                    }
                    if Downloader.checkFile(fileName:file_PackageCuisines) {
                        try file.removeItem(at: localUrl_PackageCuisines)
                    }
                    if Downloader.checkFile(fileName:file_PackageAllergies) {
                        try file.removeItem(at: localUrl_PackageAllergies)
                    }
                    if Downloader.checkFile(fileName:file_IngredientRulesAllergy) {
                        try file.removeItem(at: localUrl_IngredientRulesAllergy)
                    }
                    
                    if Downloader.checkFile(fileName:file_AllergyList) {
                        try file.removeItem(at: localUrl_AllergyList)
                    }
                    if Downloader.checkFile(fileName:file_CuisineList) {
                        try file.removeItem(at: localUrl_CuisineList)
                    }
                    
                    let data_entry_PackageIngredients = Downloader.extractFile(archive: archive, entry: entry_PackageIngredients, localUrl: localUrl_PackageIngredients)
                    
                    let data_entry_PlateItems = Downloader.extractFile(archive: archive, entry: entry_PlateItems, localUrl: localUrl_PlateItems)
                    
                    let data_entry_IngredientRules = Downloader.extractFile(archive: archive, entry: entry_IngredientRules, localUrl: localUrl_IngredientRules)
                    
                    let data_entry_PackageInfoItems = Downloader.extractFile(archive: archive, entry: entry_PackageInfoItems, localUrl: localUrl_PackageInfoItems)
                    
                    let data_entry_Packages = Downloader.extractFile(archive: archive, entry: entry_Packages, localUrl: localUrl_Package)
                    
                    let data_entry_Senf = Downloader.extractFile(archive: archive, entry: entry_Senf, localUrl: localUrl_Senf)
                    
                    let data_entry_PlatePerPackage = Downloader.extractFile(archive: archive, entry: entry_PlatePerPackage, localUrl: localUrl_PlatePerPackage)
                    
                    let data_entry_PackagePlateUnits = Downloader.extractFile(archive: archive, entry: entry_PackagePlateUnits, localUrl: localUrl_PackagePlateUnits)
                    
                    let data_entry_AllergyList = Downloader.extractFile(archive: archive, entry: entry_AllergyList, localUrl: localUrl_AllergyList)
                    
                    let data_entry_CuisineList = Downloader.extractFile(archive: archive, entry: entry_CuisineList, localUrl: localUrl_CuisineList)
                    
                    
                    
                    let data_entry_UnwantedFruitsAndVegetablesSenfs = Downloader.extractFile(archive: archive, entry: entry_UnwantedFruitsAndVegetablesSenfs, localUrl: localUrl_UnwantedFruitsAndVegetablesSenfs)
                    
                    let data_entry_PackageCuisines = Downloader.extractFile(archive: archive, entry: entry_PackageCuisines, localUrl: localUrl_PackageCuisines)
                    
                    let data_entry_PackageAllergies = Downloader.extractFile(archive: archive, entry: entry_PackageAllergies, localUrl: localUrl_PackageAllergies)
                    
                    let data_entry_IngredientRulesAllergy = Downloader.extractFile(archive: archive, entry: entry_IngredientRulesAllergy, localUrl: localUrl_IngredientRulesAllergy)
                    
                    
                    let syncConc = DispatchQueue(label:"con",attributes:.concurrent)
                    let syncGroup = DispatchGroup()
                    syncGroup.enter()
                    
                    syncConc.sync {
                        Downloader.saveTablesPlateItems(data: data_entry_PlateItems, localUrl: localUrl_PlateItems)
                    }
                    syncConc.sync {
                        Downloader.saveTablesPlatePerPackge(data: data_entry_PlatePerPackage, localUrl: localUrl_PlatePerPackage)
                    }
                    syncConc.sync {
                        Downloader.saveTablesIngredientRules(data: data_entry_IngredientRules, localUrl: localUrl_IngredientRules)
                    }
                    syncConc.sync {
                        Downloader.saveTablesPackageInfoItems(data: data_entry_PackageInfoItems, localUrl: localUrl_PackageInfoItems)
                    }
                    syncConc.sync {
                        Downloader.saveTablesPackge(data: data_entry_Packages, localUrl: localUrl_Package)
                    }
                    syncConc.sync {
                        Downloader.saveTablesPackagePlateUnits(data: data_entry_PackagePlateUnits, localUrl: localUrl_PackagePlateUnits)
                    }
                    syncConc.sync {
                        Downloader.saveTablesSenf(data: data_entry_Senf, localUrl: localUrl_Senf)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesWajbehAllergy(data: data_entry_PackageAllergies, localUrl: localUrl_PackageAllergies)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesIngredientRulesAllergy(data: data_entry_IngredientRulesAllergy, localUrl: localUrl_IngredientRulesAllergy)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesWajbehCuisenes(data: data_entry_PackageCuisines, localUrl: localUrl_PackageCuisines)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesCuisenesType(data: data_entry_CuisineList, localUrl: localUrl_CuisineList)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesAllergyInfo(data: data_entry_AllergyList, localUrl: localUrl_AllergyList)
                    }
                    
                    //AllergyList
                    //CuisineList
                    
                    syncConc.sync {
                        Downloader.saveUnwantedFruitsAndVegetablesSenfs(data: data_entry_UnwantedFruitsAndVegetablesSenfs, localUrl: localUrl_UnwantedFruitsAndVegetablesSenfs)
                    }
                    
                    syncConc.sync {
                        Downloader.saveTablesPackgeIngr(data: data_entry_PackageIngredients, localUrl: localUrl_PackageIngredients, threadGroup: syncGroup)
                    }
                    
                    
                    
                    
                    syncGroup.notify(queue: .main) {
                        setDataInSheardPreferance(value: "1", key: "loadWajbehSenf")
                        
                
                        
                        //                        try? file.removeItem(at: localUrl_Package)
                        //                        try? file.removeItem(at: localUrl_Senf)
                        //                        try? file.removeItem(at: localUrl_PackageIngredients)
                        //                        try? file.removeItem(at: localUrl_PlateItems)
                        //                        try? file.removeItem(at: localUrl_PackageInfoItems)
                        //                        try? file.removeItem(at: localUrl_IngredientRules)
                        //                        try? file.removeItem(at: localUrl_PlatePerPackage)
                        //                        try? file.removeItem(at: localUrl_PackagePlateUnits)
                        
                        if flag == 0 {
                            callBack(true)
                        }
                        
                    }
                    if flag == 1 {
                        callBack(true)
                        
                    }
                } catch (let writeError) {
                    if writeError.asAFError?.responseCode == NSURLErrorTimedOut {
                        print("Request timeout!")
                    }
                    
                    let syncConc = DispatchQueue(label:"con1",attributes:.concurrent)
                    
                    syncConc.async {
                        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 0 {
                            reportDownloadDataError()
                        }
                        
                    }
                    syncConc.async {
                        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") < 4 {
                            if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
                                SVProgressHUD.show(withStatus: "جاري محاولت تحميل بياناتك")
                            }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                            reTryRequestAPI()
                            setDataInSheardPreferanceInt(value:getDataFromSheardPreferanceInt(key: "counterRetryLoad")+1, key: "counterRetryLoad")
                            callBack(false)
                            return
                        }
                        }
                    }
                    print("error writing file : \(writeError)")
                }
            }
            
        }
        task.resume()
    }
    
    
    class func saveTablesWajbehAllergy(data:Data,localUrl:URL){
        let plate = tblWajbehAllergy()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveTablesIngredientRulesAllergy(data:Data,localUrl:URL){
        let plate = tblIngredientRulesAllergy()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveTablesWajbehCuisenes(data:Data,localUrl:URL){
        let plate = tblWajbehCuisenes()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    
    class func saveTablesCuisenesType(data:Data,localUrl:URL){
        let plate = tblCusisneType()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    
    class func saveTablesAllergyInfo(data:Data,localUrl:URL){
        let plate = tblAllergyInfo()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveUnwantedFruitsAndVegetablesSenfs(data:Data,localUrl:URL){
        let plate = tblUnwantedFruitsAndVegetablesSenfs()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    
    class func saveTablesPlateItems(data:Data,localUrl:URL){
        let plate = tblPlateItem()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    
    class func saveTablesPlatePerPackge(data:Data,localUrl:URL){
        let plate = tblPlatePerPackage()
        plate.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveTablesPackageInfoItems(data:Data,localUrl:URL){
        let packageInfoItems = tblPackageInfoItems()
        packageInfoItems.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveTablesIngredientRules(data:Data,localUrl:URL){
        let ingredientRules = tblIngredientRules()
        ingredientRules.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
        
    }
    class func saveTablesPackge(data:Data,localUrl:URL){
        let packges = tblPackeg()
        packges.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    
    class func saveTablesPackgeIngr(data:Data,localUrl:URL,threadGroup:DispatchGroup){
        let packges = tblPackageIngredients()
        packges.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
            threadGroup.leave()
        }
    }
    
    class func saveTablesPackagePlateUnits(data:Data,localUrl:URL){
        let packges = tblPackagePlateUnits()
        packges.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func saveTablesSenf(data:Data,localUrl:URL){
        let packges = tblSenf()
        packges.readArrayJson(response: JSON(data).array!) { (bool) in
            print("bool \(bool)")
            try? file.removeItem(at: localUrl)
        }
    }
    class func extractFile(archive:Archive,entry:Entry,localUrl:URL)->Data{
        var fileContent:Data!
        do {
            let _ = try archive.extract(entry, to: localUrl)
            
            let location = NSString(string:localUrl.path).expandingTildeInPath
            fileContent = try String(contentsOfFile: location as String).data(using: .utf8)
        }catch (let writeError) {
            let syncConc = DispatchQueue(label:"con4",attributes:.concurrent)

            syncConc.async {
                if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 0 {
                    reportDownloadDataError()
                }
                
            }
            syncConc.async {
                if getDataFromSheardPreferanceInt(key: "counterRetryLoad") < 4 {
                    if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
                        SVProgressHUD.show(withStatus: "جاري محاولت تحميل بياناتك")
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [self] in
                        reTryRequestAPI()
                        setDataInSheardPreferanceInt(value:getDataFromSheardPreferanceInt(key: "counterRetryLoad")+1, key: "counterRetryLoad")
                        return
                    }
                }
                return
            }
            print("error writing file \(localUrl) : \(writeError)")
        }
        
        return fileContent
        
    }
    class func checkFile(fileName:String)->Bool{
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                print("FILE AVAILABLE \(fileName)")
                return true
            } else {
                print("FILE NOT AVAILABLE \(fileName)")
                return false
            }
        } else {
            print("FILE PATH NOT AVAILABLE \(fileName)")
            return false
        }
    }
    
    
    
    class func loadData_firebase(flag:Int,callBack:@escaping (Bool)->Void){
        if getDataFromSheardPreferanceString(key: "isDownloadAPI") == "1" {
            print("HERE109")
            callBack(false)
        }
        print("HERE108")
        setDataInSheardPreferance(value: "1", key: "isDownloadAPI")
        DispatchQueue.main.async {
            UIApplication.shared.isIdleTimerDisabled = true
        }
        
        _ = DispatchQueue(label:"swiftlee.concurrent.queue1",attributes:.concurrent)
        
        if getDataFromSheardPreferanceString(key: "loadWajbehSenf") == "0" {
            //"http://dev.tele-ent.com/TE-WebServices/dietdatajson/getdata" old url
            if let URL = NSURL(string:"http://barsha4arab.com/getdata.php" ) {
                Downloader.load(URL: URL,flag:flag) { (bool) in
                    
                    if  getDataFromSheardPreferanceString(key: "isCompletedDataLoad") == "0" &&  getDataFromSheardPreferanceString(key: "isTutorial") == "0" {
                        setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
                        setDataInSheardPreferance(value: "0", key: "isTutorial-tamreen")
                        setDataInSheardPreferance(value: "0", key: "isTutorial-isBeign")
                        setDataInSheardPreferance(value: "0", key: "isTutorial-nutration")
                        setDataInSheardPreferance(value: "0", key: "isTutorial-water")
                        setDataInSheardPreferance(value: "0", key: "isTutorial")
                    }
                    callBack(bool)
                }
            }
        }else{
            callBack(false)
        }
        
    }
    
    class func reTryRequestAPI(){
        dispatchGroup.enter()
        if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 1 {
            print("reTryRequestAPIreTryRequestAPI",getDataFromSheardPreferanceInt(key: "counterRetryLoad"))
            if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
            DispatchQueue.main.async {
                UIApplication.shared.beginIgnoringInteractionEvents()
            }
            }
        }
        Downloader.loadData_firebase(flag:0) { (bool) in
       
           
            if bool {
                if getDataFromSheardPreferanceString(key:"loadWajbehSenf") == "0" {
                  print("loadWajbehSenf == 0")
                    dispatchGroup.leave()
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "dissmesDialog"),object: nil))
                }
                
                setDataInSheardPreferance(value: "1", key: "isDownloadAPI")
                setDataInSheardPreferance(value: "1", key: "isCompletedDataLoad")
                DispatchQueue.main.async {
                    UIApplication.shared.isIdleTimerDisabled = false
                }
                SVProgressHUD.dismiss()
                print("loadData_firebase \(bool)")
                DispatchQueue.main.async {
                UIApplication.shared.endIgnoringInteractionEvents()
                }
                setDataInSheardPreferance(value: "0", key: "isTutorial-wajbeh")
                setDataInSheardPreferance(value: "0", key: "isTutorial-tamreen")
                setDataInSheardPreferance(value: "0", key: "isTutorial-isBeign")
                setDataInSheardPreferance(value: "0", key: "isTutorial-nutration")
                setDataInSheardPreferance(value: "0", key: "isTutorial-water")
                setDataInSheardPreferance(value: "0", key: "isTutorial")
            }else{
                if getDataFromSheardPreferanceInt(key: "counterRetryLoad") == 4 {
                    print("YAZANXXXX")
                    DispatchQueue.main.async {
                        UIApplication.shared.isIdleTimerDisabled = false
                        if getDataFromSheardPreferanceString(key: "finishRegistration") != "1" {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                    ShowTostِError(title: "حصل خطأ", message: "يرجي المحاولة لاحقا")
                    SVProgressHUD.dismiss()
                }
                setDataInSheardPreferance(value: "0", key: "isDownloadAPI")
                print("loadData_firebase false")
            }
            return
            
        }
    }
    
    class func reportDownloadDataError(){
        AF.request("http://dev.tele-ent.com/TE-WebServices/diet/reportDownloadDataError",method:.post,  parameters:["type":"2","key":"WfJGygCfEu6n5ZhhhucT","userID" :getDataFromSheardPreferanceString(key: "userID")],encoding:URLEncoding.default, headers:header).responseJSON{
            response in
            switch response.result {
            case .success(let value):
                print("reportDownloadDataError success")
                if let recommends = value as? [String: Any] {
                    if recommends["errorCode"] as! Int == 0 {
                    }
                }
            case.failure(let error):
                print("reportDownloadDataError error \(error)")
            }
            
        }
    }
}








