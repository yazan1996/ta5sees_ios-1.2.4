//
//  tblExchangeSystem.swift
//  Ta5sees
//
//  Created by Admin on 7/3/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class tblExchangeSystem : Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var date  = ""

    @objc dynamic var totalFateOrigin = 0
    @objc dynamic var totalProteinOrigin  = 0
    @objc dynamic var totalCHOOrigin  = 0
    @objc dynamic var totalEnergyOrigin  = 0
    
    @objc dynamic var totalFate = 0
    @objc dynamic var totalProtein = 0
    @objc dynamic var totalCHO = 0
    @objc dynamic var totalEnergy = 0
    @objc dynamic var subTotal1 = 0
    @objc dynamic var subTotal2 = 0
    @objc dynamic var subTotal3 = 0
    
    @objc dynamic var refMilkInfo = 0
    @objc dynamic var refStarchInfo = 0
    @objc dynamic var refFruitInfo = 0
    @objc dynamic var refVegetablsInfo = 0
    @objc dynamic var refMeatInfo = 0
    @objc dynamic var refFatInfo = 0
    @objc dynamic var refOthersInfo = 0

    
    
    
    
    
    //    func getProtein(refUserID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    //
    //        let realm = try! Realm()
    //        let ob = realm.objects(tblExchangeSystem.self).filter("refUserID == %@",refUserID).first
    //
    //        if ob != nil {
    //            responsEHendler("\(ob!.totalProtein)%","0%" as? Error)
    //
    //        }else{
    //            responsEHendler("0%","0%" as? Error)
    //
    //
    //        }
    //
    //    }
    //
    //    func getFate(refUserID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    //
    //        let realm = try! Realm()
    //        let ob = realm.objects(tblExchangeSystem.self).filter("refUserID == %@",refUserID).first
    //
    //        if ob != nil {
    //            responsEHendler("\(ob!.totalFate)%","0%" as? Error)
    //
    //        }else{
    //            responsEHendler("0%","0%" as? Error)
    //
    //        }
    //
    //
    //    }
    //
    //    func getCHO(refUserID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    //
    //        let realm = try! Realm()
    //        let ob = realm.objects(tblExchangeSystem.self).filter("refUserID == %@",refUserID).first
    //
    //        if ob != nil {
    //            responsEHendler("\(ob!.totalCHO)%","0%" as? Error)
    //
    //
    //        }else{
    //            responsEHendler("0%","0%" as? Error)
    //
    //
    //        }
    //
    //    }
    //
    //
    //    func getEnergy(refUserID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    //
    //        let realm = try! Realm()
    //        let ob = realm.objects(tblExchangeSystem.self).filter("refUserID == %@",refUserID).first
    //
    //        if ob != nil {
    //            responsEHendler(ob!.totalEnergy,nil)
    //
    //        }else{
    //            responsEHendler(0,0 as? Error)
    //
    //        }
    //
    //    }
    //
    //    func getRow(refUserID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
    //
    //        let realm = try! Realm()
    //        let ob = realm.objects(tblExchangeSystem.self).filter("refUserID == %@",refUserID).first
    //
    //        if ob != nil {
    //            responsEHendler(ob,nil)
    //        }else{
    //            responsEHendler(0,0 as? Error)
    //        }
    //    }
    //
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblExchangeSystem.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}



