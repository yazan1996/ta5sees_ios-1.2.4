//
//  tblUserHistory.swift
//  Ta5sees
//
//  Created by Admin on 9/17/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUserHistory:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var Height = ""
    @objc dynamic var Weight = ""
    @objc dynamic var insertDate = ""
   
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func getUserWeight(date:String,weight:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserHistory.self).filter("refUserID == %@ AND insertDate == %@",getDataFromSheardPreferanceString(key: "userID"),date).last
        if ob != nil {
            responsEHendler(ob?.Weight,nil)
        }else{
            let ob = setupRealm().objects(tblUserHistory.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).sorted(byKeyPath: "insertDate", ascending: true).last
            let lastRow = ob
            if lastRow != nil {
                responsEHendler(lastRow?.Weight,nil)
            }else{
                responsEHendler(weight,nil)
            }
        }
    }
    
//     static func getUserWeight(date:String,responsEHendler:@escaping (Any?,Error?)->Void){
//            let realm1 = try! Realm()
//            let ob = realm1.objects(tblUserHistory.self).filter("refUserID == %@ AND insertDate == %@",getDataFromSheardPreferanceString(key: "userID"),date).last
//            if ob != nil {
//                responsEHendler(ob?.Weight,nil)
//            }else{
//                let ob = realm1.objects(tblUserHistory.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).sorted(byKeyPath: "insertDate", ascending: true).last!
//                print("ob \(ob)")
//                let lastRow = ob
//    //                ArrayWeight(list:ob.sorted(byKeyPath: "insertDate", ascending: true).last!,date:date)
//                if !lastRow.isEqual(nil) {
//                    responsEHendler(lastRow.Weight,nil)
//                }else{
//                    responsEHendler("0.0",nil)
//                }
//
//            }
//        }
//
    static func ArrayWeight(list:tblUserHistory,date:String)->tblUserHistory{
        let item:tblUserHistory? = nil
        if !list.isEqual(nil){
            if convertToDate(cdate: list.insertDate) <= convertToDate(cdate: date) {
                return list
                
            }
        }
        return (item)!
    }
    
   static func getLastWeight()->String{
        return setupRealm().objects(tblUserHistory.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).last!.Weight
    }
    static func convertToDate(cdate:String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from:cdate)!
        return date
    }
    static func updateWeight(w:String,date:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserHistory.self).filter("refUserID == %@ AND insertDate == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        if ob.isEmpty {
            print("NIL")
            tblUserHistory.setInsertNewWeight(iduser:getDataFromSheardPreferanceString(key: "userID"), h: "nil",w:w,date:date)
            return
        }
        try! setupRealm().write {
            ob.setValue(w,forKey: "Weight")
//            showToast(message: "تم تحديث الوزن بنجاح", view: view)
            responsEHendler("success",nil)

        }
    }
    
    static func setInsertNewWeight(iduser:String,h:String,w:String,date:String){
        let userHistory = tblUserHistory()
        if h == "nil" {
            getInfoUser(responsEHendler: { (res, err) in
                let ob:tblUserInfo = (res)
                userHistory.Height = ob.height
            })
        }else{
            userHistory.Height = h
        }
        userHistory.id  =  userHistory.IncrementaID()
        userHistory.refUserID = iduser
        userHistory.Weight = w
        userHistory.insertDate = date
        try! setupRealm().write {
            setupRealm().add(userHistory)
        }
        
    }
//    static func setInsertNewWeight(iduser:String,h:String,w:String,date:String){
//        let userHistory = tblUserHistory()
//        userHistory.id  =  userHistory.IncrementaID()
//        userHistory.refUserID = iduser
//        userHistory.Height = h
//        userHistory.Weight = w
//        userHistory.insertDate = date
//        try! realm.write {
//            realm.add(userHistory)
//            ShowTost(title: "تحديث الوزن",message: "تم تحديث الوزن بنجاح")
//        }
//
//    }
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserHistory.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
