//
//  tblAllergyUserItems.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class  tblAllergyUserItems:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var refAllergyInfo = ""
    
    static var arrayCusisneType:[tblAllergyUserItems] = []
    static var listCusisneType:Results<tblAllergyUserItems>!
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblAllergyUserItems.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    static func getDataIDInt(id:String) ->Array<Int>{
                 var arrayID:[Int]=[]

              listCusisneType = (setupRealm().objects(tblAllergyUserItems.self).filter("refUserID == %@",id))
                    arrayCusisneType = Array(listCusisneType)
                    for x in 0..<listCusisneType.count {
                      arrayID.append(Int(listCusisneType[x].refAllergyInfo)!)
                    }
                    return arrayID
                 
              
                 }
    
    static func getDataSelectedProgressHistory() ->String{
          var arrayID:[String]=[]
        let list = Array(setupRealm().objects(tblAllergyInfo.self).filter("id IN %@",getUserInfoProgressHistory().refAllergyInfoID.split(separator: ",")))

          for x in list {

            arrayID.append(x.discription)
          }
          return arrayID.joined(separator:",")
          
          
      }
    static func getDataSelected() ->String{
        var arrayID:[String]=[]
        
        listCusisneType = (setupRealm().objects(tblAllergyUserItems.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")))
        arrayCusisneType = Array(listCusisneType)
        for x in 0..<listCusisneType.count {
            //            let items:String? = setupRealm().objects(tblAllergyInfo.self).filter("id == %@",listCusisneType[x].refAllergyInfo).first?.discription
            guard let items = setupRealm().objects(tblAllergyInfo.self).filter("id == %@",listCusisneType[x].refAllergyInfo).first?.discription else {
                break
            }
            if items.isEmpty == false {
                arrayID.append(items)
            }
        }
        return arrayID.joined(separator:",")
        
        
    }
    
    
    static func updateUserAllergyID(id_User:String,str:String){
           
           let idAlergy:[String]! = str.split(separator: ",").map(String.init)
           
           for item in idAlergy{
       
               let userAllergy:tblAllergyUserItems = tblAllergyUserItems()
               userAllergy.id = userAllergy.IncrementaID()
               userAllergy.refAllergyInfo = String(item)
               userAllergy.refUserID = id_User
               try! setupRealm().write{
                   setupRealm().add(userAllergy, update: Realm.UpdatePolicy.modified)
               
           }
        }
       }
    static func updateUserAllergy(id_User:String,str:String){
        
        let idAlergy:[String]! = str.split(separator: ",").map(String.init)
        try! setupRealm().write{
            
            for item in idAlergy{
                let i = setupRealm().objects(tblAllergyInfo.self).filter("discription == %@",item).first
                if item == "لا شيء" {
                    let list = setupRealm().objects(tblAllergyUserItems.self).filter("refUserID == %@",id_User)
                    setupRealm().delete(list)
                  
                    let userAllergy:tblAllergyUserItems = tblAllergyUserItems()
                    userAllergy.id = userAllergy.IncrementaID()
                    userAllergy.refAllergyInfo = String(i!.id)
                    userAllergy.refUserID = id_User
                    setupRealm().add(userAllergy, update: Realm.UpdatePolicy.modified)
                    
                    
                    break
                }else {
                    if setupRealm().objects(tblAllergyUserItems.self).filter("refAllergyInfo == %@","9").first != nil{
                    if let item =  setupRealm().objects(tblAllergyInfo.self).filter("discription == %@","لا شيء").first{
                        let remove = setupRealm().objects(tblAllergyUserItems.self).filter("refUserID == %@ AND refAllergyInfo == %@",id_User,item.id).first
                        setupRealm().delete(remove!)
                        }
                    }
                   
                    if  setupRealm().objects(tblAllergyUserItems.self).filter("refUserID == %@ AND refAllergyInfo == %@",id_User,i!.id).first == nil {
                        let userAllergy:tblAllergyUserItems = tblAllergyUserItems()
                        userAllergy.id = userAllergy.IncrementaID()
                        userAllergy.refAllergyInfo = String(i!.id)
                        userAllergy.refUserID = id_User
                        setupRealm().add(userAllergy, update: Realm.UpdatePolicy.modified)
                        
                    }
                }
            }
        }
    }
}

