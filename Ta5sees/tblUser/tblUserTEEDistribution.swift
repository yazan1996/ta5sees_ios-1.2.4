//
//  tblUserTEEDistribution.swift
//  Ta5sees
//
//  Created by Admin on 10/16/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//
import Foundation
import RealmSwift
import SwiftyJSON
class tblUserTEEDistribution :Object {
    @objc dynamic var id = 0
    @objc dynamic var userID = ""
    @objc dynamic var Carbo = "0"
    @objc dynamic var Protien = "0"
    @objc dynamic var Fat = "0"
    @objc dynamic var Caloris = "0"
    @objc dynamic var date = "0"
    @objc dynamic var dateAsInt = 0.0

    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserTEEDistribution.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func getDataUserTEE(date:String,responsEHendler:@escaping (Any?,Error?)->Void) {
        let date_type_Int = convertToDate(str:date)
        let ob = setupRealm().objects(tblUserTEEDistribution.self).filter("userID == %@  AND dateAsInt <= %@",getDataFromSheardPreferanceString(key: "userID"),date_type_Int).sorted(byKeyPath: "dateAsInt", ascending: false).first
           if ob != nil {
               responsEHendler(ob,nil)
           }else{
              let ob = setupRealm().objects(tblUserTEEDistribution.self).filter("userID == %@",getDataFromSheardPreferanceString(key: "userID")).sorted(byKeyPath: "dateAsInt", ascending: false).last
            responsEHendler(ob,nil)

        }
    
       }
    
    
    static func convertToDate(str:String)->Double{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from:str)
        let timeInterval = date?.timeIntervalSince1970
//        print("timeInterval \(timeInterval)")
        return  timeInterval ?? Date().timeIntervalSince1970
    }
    //1600951150.689847
    static func setUserTeeDistrbution(userid:String,carbo:String,protien:String,fat:String,caloris:String,date:String,dateAsInt:Double,responsEHendler:@escaping (Any?,Error?)->Void){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        let list = setupRealm().objects(tblUserTEEDistribution.self).filter("userID == %@ AND date == %@",userid,date)
        if list.isEmpty == false{
            if let item = list.first {
                try! setupRealm().write {
                    item.setValue(carbo,forKey: "Carbo")
                }
                try! setupRealm().write {
                    item.setValue(protien,forKey: "Protien")
                }
                try! setupRealm().write {
                    item.setValue(fat,forKey: "Fat")
                }
                try! setupRealm().write {
                    item.setValue(caloris,forKey: "Caloris")
                }
                try! setupRealm().write {
                    item.setValue(dateAsInt,forKey: "dateAsInt")
                }
                responsEHendler(true,nil)

            }
        }else{
            let obj = tblUserTEEDistribution(value: ["id" :tblUserTEEDistribution.IncrementaID(), "userID": userid,"Carbo":carbo ,"Protien":protien,"Fat":fat,"Caloris":caloris,"date":date,"dateAsInt":dateAsInt])
            try! setupRealm().write {
                setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                responsEHendler(true,nil)
            }
        }
    }
    static func getUserTeeDistrbution(refID:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblUserTEEDistribution.self).filter("userID == %@",refID),nil)
    }
    
}
