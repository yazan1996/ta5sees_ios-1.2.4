//
//  tblUserProgressHistory .swift
//  Ta5sees UserProgressHistory

//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON
import SVProgressHUD
import Firebase
import FirebaseFirestore

class tblUserProgressHistory:Object {
    
    
    static var sheard = tblUserProgressHistory()
    @objc dynamic var id = 0
    @objc dynamic var refGenderID = ""
    @objc dynamic var height = ""
    @objc dynamic var weight = ""
    @objc dynamic var birthday = ""
    @objc dynamic var refLayaqaCondID = ""
    @objc dynamic var refPlanMasterID = ""
    @objc dynamic var target = ""
    @objc dynamic var refCuiseseID = ""
    @objc dynamic var gramsToLose = ""
    @objc dynamic var weeksNeeded = ""
    @objc dynamic var refAllergyInfoID = ""
    @objc dynamic var dateInMillis = 0.0
    @objc dynamic var date = ""
    @objc dynamic var refUserID = ""
    
    
    convenience init(refCuiseseID:String,date:String,weeksNeeded:String,gramsToLose:String,dateInMili:Double,refAllergyInfoID:String,refUserID:String,target:String,refPlanMasterID:String,refLayaqaCondID:String,birthday:String,weight:String,height:String,refGenderID:String) {
        self.init()
        if setupRealm().isInWriteTransaction {
            setupRealm().cancelWrite()
        }
        print("datedate",date)
        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        
        let arrInfo = [
            "target" : Double(target)!,
            "weeksNeeded" : Int(weeksNeeded)!,
            "gramsToLose" : Float(gramsToLose)! ,
            "weight" :   Double(weight)!,
            "height" :   Double(height)!,
            "birthday" :  Self.handelDateFormat(date:birthday) ,
            "refCuiseseID" :  refCuiseseID ,
            "refLayaqaCondID" : Int(refLayaqaCondID)!,
            "refAllergyInfoID" : refAllergyInfoID,
            "refPlanMasterID" :Int(refPlanMasterID)!,
            "refGenderID" :Int(refGenderID)!,
            "date" : Self.handelDateFormat(date:date),
            "dateInMillis" :dateInMili,
            "refUserID" : refUserID
        ]  as [String : Any]
        
        
        if ob.isEmpty {
            
            self.id = self.IncrementaID()
            self.refCuiseseID = refCuiseseID
            self.gramsToLose = gramsToLose
            self.weeksNeeded = weeksNeeded
            self.refAllergyInfoID = refAllergyInfoID
            self.dateInMillis = dateInMili
            self.date = date
            self.refUserID = refUserID
            self.target = target
            self.refGenderID = refGenderID
            self.height = height
            self.weight = weight
            self.birthday = birthday
            self.refPlanMasterID = refPlanMasterID
            self.refLayaqaCondID = refLayaqaCondID
            
            try! setupRealm().write{
                setupRealm().add(self, update: Realm.UpdatePolicy.modified)
            }
            
            Self.uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
                if str == "success" {
                    
                }
            }
            
        }else{
            try! setupRealm().write {
                ob.setValue(weight,forKey: "weight")
            }
            try! setupRealm().write {
                ob.setValue(height,forKey: "height")
            }
            
            try! setupRealm().write {
                ob.setValue(birthday,forKey: "birthday")
            }
            try! setupRealm().write {
                ob.setValue(refCuiseseID,forKey: "refCuiseseID")
            }
            try! setupRealm().write {
                ob.setValue(refLayaqaCondID,forKey: "refLayaqaCondID")
            }
            try! setupRealm().write {
                ob.setValue(refAllergyInfoID,forKey: "refAllergyInfoID")
            }
            try! setupRealm().write {
                ob.setValue(refGenderID,forKey: "refGenderID")
            }
            try! setupRealm().write {
                ob.setValue(target,forKey: "target")
            }
            
            try! setupRealm().write {
                ob.setValue(gramsToLose,forKey: "gramsToLose")
            }
            try! setupRealm().write {
                ob.setValue(weeksNeeded,forKey: "weeksNeeded")
            }
            
            try! setupRealm().write {
                ob.setValue(date,forKey: "date")
            }
            
            try! setupRealm().write {
                ob.setValue(dateInMili,forKey: "dateInMillis")
            }
            try! setupRealm().write {
                ob.setValue(refPlanMasterID,forKey: "refPlanMasterID")
            }
            Self.uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
                if str == "success" {
                    
                }
                
                
            }
        }
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserProgressHistory.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func updateInfoUserWeightTarget(target:String,date:String,responsEHendler:@escaping (String,Error?)->Void){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        let list = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        
        
        
        let arrInfo = [
            "target" :  Double(target)! ]  as [String : Any]
        
        
        
        tblUserProgressHistory.checkUploadAllData { bool in
            
            if bool == "false" {
                setupDataUser(parameter:arrInfo, userProgressHistory: list, date: date)
            }
        }
        try! setupRealm().write {
            list.setValue(target,forKey: "target")
        }
        uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
            
            if str == "success" {
                print("Document successfully updated")
             
                responsEHendler("success",nil)
            }else{
                SVProgressHUD.dismiss()
                responsEHendler("error",nil)
            }
          
        }
        
    }
    
    
    static func updateDatathUser(userID:String,refLayaqaCondID:String,target:String,weight:String,weeksNeeded:String,gramsToLose:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",userID,getDateOnly())
        
        
        
        let arrInfo = [
            "refLayaqaCondID" : Int(refLayaqaCondID)!,
            "target" : Double(target )!,
            "weight" :  Double(weight)!,
            "weeksNeeded" : Int(weeksNeeded)!,
            "gramsToLose" :  Float(gramsToLose)!
        ]  as [String : Any]
        
        
        tblUserProgressHistory.checkUploadAllData { bool in
            
            if bool == "false" {
                setupDataUser(parameter:arrInfo, userProgressHistory: ob, date: getDateOnly())
            }
        }
        try! setupRealm().write {
            ob.setValue(refLayaqaCondID,forKey: "refLayaqaCondID")
        }
        try! setupRealm().write {
            ob.setValue(target,forKey: "target")
        }
        try! setupRealm().write {
            ob.setValue(weight,forKey: "weight")
        }
        try! setupRealm().write {
            ob.setValue(weeksNeeded,forKey: "weeksNeeded")
        }
        try! setupRealm().write {
            ob.setValue(gramsToLose,forKey: "gramsToLose")
        }
        uploadDateFireStore(arrInfo: arrInfo, date: getDateOnly()) { str, err in
            let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",userID,getDateOnly())
            
            print("obobob11",ob)
            
            if str == "success" {
                
                responsEHendler("success",nil)
            }else{
                responsEHendler("false",nil)
            }
        }
        
    }
    
    
    static func updateWeightHeightUser(weight:String,height:String,date:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        
        
        var arrInfo:[String : Any]!
        
        if height == "" {
            arrInfo = [
                "weight" :  Double(weight)!
            ]  as [String : Any]
        }else{
            arrInfo = [
                "weight" :  Double(weight)!,
                "height" :  Double(height)!
                
            ]  as [String : Any]
            
        }
        
        tblUserProgressHistory.checkUploadAllData { bool in
            
            if bool == "false" {
                setupDataUser(parameter:arrInfo, userProgressHistory: ob, date: date)
            }
        }
        
        try! setupRealm().write {
            ob.setValue(weight,forKey: "weight")
        }
        if height != "" {
            try! setupRealm().write {
                ob.setValue(height,forKey: "height")
            }
        }
        uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
            
            if str == "success" {
                
                responsEHendler("success",nil)
            }else{
                responsEHendler("false",nil)

            }
        }
    }
    
    
    static func checkUploadAllData(responsEHendler:@escaping (String)->Void){
        let progressHistory = AppDelegate.db.collection("UserProgressHistory").document(getUserInfo().id).collection(getUserInfo().id)
        dateFormatter.dateFormat = "dd - MM - yyyy"
        progressHistory.getDocuments { querySnapshot, err in
            
            _ = querySnapshot?.documents.filter({ d1 in
                if d1.documentID == dateFormatter.string(from: getDateOnlyasDate()) {
                    if d1.data().count >= 13 {
                        responsEHendler("true")
                        return true
                    }
                }
                return false
            })
            responsEHendler("false")
        }
    }
    
    
    
    static func setupDataUser(parameter:[String : Any],userProgressHistory:Results<tblUserProgressHistory>,date:String){
        
        print("datedate",date)
        var arrInfo = [
            "target" : Double(getUserInfo().target)!,
            "weeksNeeded" : Int(getUserInfo().weeksNeeded)!,
            "gramsToLose" : Float(getUserInfo().gramsToLose)! ,
            "weight" :   Double(getUserInfo().weight)!,
            "height" :   Double(getUserInfo().height)!,
            "birthday" :  Self.handelDateFormat(date:getUserInfo().birthday) ,
            "refCuiseseID" :  getUserInfo().refCuiseseID ,
            "refLayaqaCondID" : Int(getUserInfo().refLayaqaCondID)!,
            "refAllergyInfoID" : getUserInfo().refAllergyInfoID,
            "refPlanMasterID" :Int(getUserInfo().refPlanMasterID)!,
            "refGenderID" :Int(getUserInfo().refGenderID)!,
            "date" : Self.handelDateFormat(date:getDateOnly()),
            "dateInMillis" :getDateOnlyasDate().timeIntervalSince1970*1000.0.rounded(),
            "refUserID" : getUserInfo().id
        ]  as [String : Any]
        
        for item in arrInfo {
            _ = parameter.contains(where: { (key: String, value: Any) in
                if key == item.key {
                    arrInfo.removeValue(forKey: item.key)
                    return true
                }
                return false
            })
        }
        
        uploadDateFireStore(arrInfo: arrInfo, date: getDateOnly()) { str, err in}
        
        for item in arrInfo {
            if item.key != "dateInMillis" { //convert all parameter to string
                arrInfo.updateValue("\(item.value)", forKey: item.key )
            }
            if item.key == "date"{ //convert all parameter to string
                arrInfo.updateValue(date, forKey: item.key )
            }else if item.key == "birthday" {
                arrInfo.updateValue(getUserInfo().birthday, forKey: item.key )
            }
        }
        print("arrInfo",arrInfo)
        let combinedDict = arrInfo.merging(parameter) { $1 }
        print("combinedDict",combinedDict)
        saveUserProgressHistoryInfoData(doc:combinedDict)
        
        
    }
    
    static func saveUserProgressHistoryInfoData(doc:[String : Any]){
        
        
        
        let obUser:tblUserProgressHistory = tblUserProgressHistory()
        
        
        if !setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ AND date == %@",getUserInfo().id,getDateOnly()).isEmpty {
            return
        }
        obUser.id  = obUser.IncrementaID()
        
        
        print("doc",doc)
        obUser.refGenderID = doc["refGenderID"] as! String
        obUser.height = String(Int(round(Double("\(doc["height"] ?? "0.0")") ?? 0.0)))
        obUser.weight = String(doc["weight"] as! Double)
        obUser.birthday = doc["birthday"] as? String ?? getUserInfo().birthday
        
        
        obUser.date = doc["date"] as? String ?? getDateOnly()
        obUser.dateInMillis = doc["dateInMillis"] as? Double ?? 0.0
        
        obUser.refUserID = getUserInfo().id
        
        obUser.refLayaqaCondID = doc["refLayaqaCondID"] as? String  ?? "1"
        obUser.refPlanMasterID =  doc["refPlanMasterID"] as? String ?? "1"
        obUser.target =  doc["target"] as? String ?? "0.0"
        obUser.refCuiseseID = doc["refCuiseseID"] as? String ?? "1"
        obUser.gramsToLose =  doc["gramsToLose"] as? String ?? "0"
        obUser.weeksNeeded = doc["weeksNeeded"] as? String ?? "0"
        obUser.refAllergyInfoID = doc["refAllergyInfoID"] as? String ?? "null"
        
        try! setupRealm().write{
            setupRealm().add(obUser, update: Realm.UpdatePolicy.modified)
        }
    }
    static func updateInfoUser(birthday:String,refCuiseseID:String,refLayaqaCondID:String,refAllergyInfoID:String,gender:String,date:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        
        print("ob",ob)
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd - MM - yyyy"
        
        let d = dateFormatter1.date(from: birthday)
        
        
        let newFormate = dateFormatter.string(from: d!)
        
        
        let arrInfo = [
            "birthday" :  newFormate,
            "refCuiseseID" :  refCuiseseID ,
            "refLayaqaCondID" : Int(refLayaqaCondID)!,
            "refAllergyInfoID" : refAllergyInfoID,
            "refGenderID" :Int(gender)!
        ]  as [String : Any]
        
        SVProgressHUD.show(withStatus: "جاري تحديث بياناتك")
        
        tblUserProgressHistory.checkUploadAllData { bool in
            if bool == "false" {
                setupDataUser(parameter:arrInfo, userProgressHistory: ob,date:date)
            }
        }
        
        
        try! setupRealm().write {
            ob.setValue(birthday,forKey: "birthday")
        }
        try! setupRealm().write {
            ob.setValue(refCuiseseID,forKey: "refCuiseseID")
        }
        try! setupRealm().write {
            ob.setValue(refLayaqaCondID,forKey: "refLayaqaCondID")
        }
        try! setupRealm().write {
            ob.setValue(refAllergyInfoID,forKey: "refAllergyInfoID")
        }
        try! setupRealm().write {
            ob.setValue(gender,forKey: "refGenderID")
        }
        uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
//            print("str",str,"err",err)
            if str == "success" {
                SVProgressHUD.dismiss()
                responsEHendler("success",nil)
            }else{
                SVProgressHUD.dismiss()
                responsEHendler("false",nil)
            }
        }
        
    }
    
    static func updateTargetWeekUser(userID:String,target:String,weeksNeeded:String,gramsToLose:String,date:String,responsEHendler:@escaping (String,Error?)->Void){
        
        let ob = setupRealm().objects(tblUserProgressHistory.self).filter("refUserID == %@ && date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        
        let arrInfo = [
            "target" : Double(target)!,
            "weeksNeeded" : Int(weeksNeeded)!,
            "gramsToLose" :  Float(gramsToLose)!
        ]  as [String : Any]
        
        
        tblUserProgressHistory.checkUploadAllData { bool in
            
            if bool == "false" {
                setupDataUser(parameter:arrInfo, userProgressHistory: ob, date: date)
            }
        }
        
        try! setupRealm().write {
            ob.setValue(target,forKey: "target")
        }
        try! setupRealm().write {
            ob.setValue(gramsToLose,forKey: "gramsToLose")
        }
        try! setupRealm().write {
            ob.setValue(weeksNeeded,forKey: "weeksNeeded")
        }
        
        uploadDateFireStore(arrInfo: arrInfo, date: date) { str, err in
            
            if str == "success" {
                
                responsEHendler("success",nil)
                
            }else{
            responsEHendler("false",nil)
            }
        }
    }
    
    static func uploadDateFireStore(arrInfo:[String : Any],date:String,callBack:@escaping (String,Error?)->Void){
        let db = Firestore.firestore()
        
        CheckInternet.checkIntenet { (bool) in
            if !bool {
                SVProgressHUD.dismiss()
                callBack("false",nil)
                return
            }
        }
        
        let newFormate = handelDateFormat(date:date)
        
        db.collection("UserProgressHistory").document(getDataFromSheardPreferanceString(key: "userID")).collection(getDataFromSheardPreferanceString(key: "userID")).document(newFormate).setData(arrInfo,merge: true) { err in
            if let err = err {
                print("Error updating document: UserProgressHistory \(err)")
                callBack("false",nil)
                
            } else {
                print("UserProgressHistory true")
                callBack("success",nil)
                
            }
        }
    }
    
    
    static func handelDateFormat(date:String)->String{
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd - MM - yyyy"
        
        let d = dateFormatter1.date(from: date)
        
        return dateFormatter.string(from: d!)
    }
    
    static func saveUserProgressHistoryInfoData(docs:[DocumentSnapshot],callback:(String)->Void){
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd - MM - yyyy"
        //        var list  = [tblUserProgressHistory]()
        print("docs",docs.count,docs)
        var count = 0
        for doc in docs {
            count+=1
            
            let obUser:tblUserProgressHistory = tblUserProgressHistory()
            
            let d = dateFormatter1.date(from:  doc.get("birthday") as? String ?? dateFormatter1.string(from: Date()))
            
            let newFormate = dateFormatter.string(from: d!)
            
            
            obUser.id  = count
            let date1 = dateFormatter1.date(from:  doc.get("date") as? String ?? dateFormatter1.string(from: Date()))
            
            obUser.refGenderID = String(doc.get("refGenderID") as? Int ?? 0)
            obUser.height = String(Int(round(doc.get("height") as? Double ?? 0.0)))
            obUser.weight = String(doc.get("weight") as? Double ?? 0.0)
            obUser.birthday = newFormate
            
            
            obUser.date = dateFormatter.string(from: date1!)//doc.get("date") as? String ?? "null"
            obUser.dateInMillis = doc.get("dateInMillis") as? Double ?? 0.0
            
            obUser.refUserID = doc.get("id") as? String ?? "null"
            
            obUser.refLayaqaCondID = String(doc.get("refLayaqaCondID") as? Int ?? 0)
            obUser.refPlanMasterID =  String(doc.get("refPlanMasterID") as? Int ?? 0)
            obUser.target =  String(doc.get("target") as? Double ?? 0.0)
            if doc.get("refCuiseseID") == nil {
                print("refCuiseseID == null")
                obUser.refCuiseseID = "-1"//doc.get("refAllergyInfoID") as? String ?? "-1"
            }else{
                obUser.refCuiseseID = doc.get("refCuiseseID") as? String ?? "-1"
            }
            obUser.refCuiseseID = doc.get("refCuiseseID") as? String ?? "-1"
            obUser.gramsToLose = String( doc.get("gramsToLose") as? Double ?? 0)
            obUser.weeksNeeded = String( doc.get("weeksNeeded") as? Int ?? 0)
            
            if doc.get("refAllergyInfoID") == nil {
                print("refAllergyInfoID == null")
                obUser.refAllergyInfoID = "-1"//doc.get("refAllergyInfoID") as? String ?? "-1"
            }else{
                obUser.refAllergyInfoID = doc.get("refAllergyInfoID") as? String ?? "-1"
            }
            
            //            list.append(obUser)
            try! setupRealm().write{
                //                print("docs",list)
                setupRealm().add(obUser, update: Realm.UpdatePolicy.modified)
            }
        }
        callback("success")
        
        
    }
    //    func updateWeightUser(date:String,new_weight:String,view:UIView,completion: @escaping (Bool) -> Void){
    //        var userItem:tblUserInfo!
    //        tblUserHistory.setInsertNewWeight(iduser: getDataFromSheardPreferanceString(key: "userID"), h: "nil",w: new_weight,date: date)
    //            completion(true)
    //
    //        getInfoUser { (user, err) in
    //            userItem = user
    //        }
    //        dateFormatter1.dateFormat = "yyyy-MM-dd"
    //        dateFormatter.dateFormat = "dd - MM - yyyy"
    //
    //        let d = dateFormatter1.date(from: date)
    //
    //        let newFormate = dateFormatter.string(from: d!)
    //        db.collection("UserProgressHistory").document(getDataFromSheardPreferanceString(key: "userID")).collection(getDataFromSheardPreferanceString(key: "userID")).document(newFormate).setData(
    //            [
    //                "date": newFormate,
    //                "height": userItem.height,
    //                "id": "0",
    //                "refUserId": getDataFromSheardPreferanceString(key: "userID"),
    //                "weight": new_weight,
    //
    //
    //        ]){ err in
    //            if err != nil {
    //                showToast(message:"لم يتم تعديل الوزن", view: view,place:0)
    //                print("faliur")
    //                completion(false)
    //            } else {
    //                showToast(message:"تم تعديل الوزن", view: view,place:0)
    //                completion(true)
    //                print("success")
    //
    //            }
    //
    //        }
    //    }
    //    static  func checkFoundIDUserMSISDN (iduser:String,callBack:(Bool)->Void) {
    //            do {
    //               guard setupRealm().objects(tblUserProgressHistory.self).filter("mobNum == %@",iduser).first != nil  else  {
    //                    callBack(false)
    ////                    throw MyError.FoundNil("false")
    //                }
    //
    //                callBack(true)
    ////                throw MyError.FoundNil("true")
    //
    //            } catch let error as NSError {
    //                print("error get user data \(error)")
    //            }
    //
    //
    //        }
}
