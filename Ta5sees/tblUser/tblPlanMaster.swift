//
//  tblPlanMaster.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import UIKit
import Realm
class tblPlanMaster:Object {
    
    //    static var arrayPlanMaster:[tblPlanMaster] = []
    //    static var listPlanMaster:Results<tblPlanMaster>!
    //    static var PlanMasterDes:[String] = []
    //    static var PlanMasterDesc :[String:String]=[:]
    
    @objc dynamic var id:Int = 0
    @objc dynamic var discription = ""
    //    let c = List<tblWajbehRestrictedPlans>()
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static var sheard=tblPlanMaster()
    
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    
    func getSpicalDiets()->Array<tblPlanMaster>{
        return Array(setupRealm().objects(tblPlanMaster.self).filter("id > %@",3))
    }
    func getAllDiets()->Array<tblPlanMaster>{
        return Array(setupRealm().objects(tblPlanMaster.self))
    }
    func getNautralDiets()->Array<tblPlanMaster>{
        return Array(setupRealm().objects(tblPlanMaster.self).filter("id < %@",4))
    }
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblPlanMaster]()
        rj.readJson(tableName: "plan/tblPlanMaster") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblPlanMaster(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                        list.append(obj)
                        
                    }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                    }
                    //                    }
                }
            }
            
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblPlanMaster]()
        let thread =  DispatchQueue.global(qos: .userInitiated)
        thread.async {
            if let recommends = JSON(response).array {
                for item in recommends {
                    
                    let obj = tblPlanMaster(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                    list.append(obj)
                }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                }
                completion(true)
            }
        }
        
    }
    
}



//    static func getData() ->Array<String>{
//        let realm = try! Realm()
//        var arrayDis:[String]=[]
//        listPlanMaster = (realm.objects(tblPlanMaster.self))
//        arrayPlanMaster = Array(listPlanMaster)
//        for x in 0..<listPlanMaster.count {
//            arrayDis.append(listPlanMaster[x].discription)
//        }
//
//        return arrayDis
//    }
//    static func getdataJoin(responsEHendler:@escaping (Any?,Error?)->Void){
//        let realm1 = try! Realm()
//        responsEHendler(realm1.objects(tblPlanMaster.self),nil)
//    }
//    static func getDataID() ->Array<Int>{
//        let realm = try! Realm()
//        var arrayID:[Int]=[]
//
//        listPlanMaster = (realm.objects(tblPlanMaster.self))
//        arrayPlanMaster = Array(listPlanMaster)
//        for x in 0..<listPlanMaster.count {
//            arrayID.append(listPlanMaster[x].id)
//        }

//        return arrayID
//    }
//
//
//    static var arraylist:[tblPlanMaster] = []
//    static var resultelist:Results<tblPlanMaster>!
//    class func generateModelArray() -> [tblPlanMaster]{
//        var modelAry = [tblPlanMaster]()
//        modelAry.removeAll()
//        arraylist.removeAll()
//        tblPlanMaster.getdataJoin() {(response, Error) in
//            resultelist = (response) as? Results<tblPlanMaster>
//            arraylist = Array(resultelist)
//        }
//
//        return arraylist
//    }
//}

