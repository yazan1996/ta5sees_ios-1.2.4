




import Foundation
import RealmSwift
import SwiftyJSON


class tbltoalEnergy :Object {
    
    @objc dynamic var id = 0
    @objc dynamic var BMR = 0
    @objc dynamic var BMI = 0
    @objc dynamic var TEE = 0
    @objc dynamic var TEF = 0
    @objc dynamic var OrderDite = 0

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tbltoalEnergy.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
