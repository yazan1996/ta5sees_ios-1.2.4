//
//  tblTamreenInfo.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class  tblTamreenInfo:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    @objc dynamic var time = ""
    @objc dynamic var totCalburn = ""
    @objc dynamic var imgPath = ""
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    

    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        rj.readJson(tableName: "others/tblTamreenInfo") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                            let data:tblTamreenInfo = tblTamreenInfo()
                            data.id = String(item["id"].intValue)
                            data.discription = item["description"].string!
                            data.time = String(item["time"].intValue)
                            data.imgPath = item["imgPath"].stringValue
                            data.totCalburn = String(item["totCalburn"].intValue)
                                
                                setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                            }
                        }
                    completion(true)
                    }
                }
                
            
        }
    }
    
    static func getAllTamreen(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblTamreenInfo.self),nil)
    }
    static func getMaxCaloris(responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblTamreenInfo.self)
        var max = 0.0
        for x in 0..<ob.count {
            if Double(ob[x].totCalburn)! > max {
                max = Double(ob[x].totCalburn)!
            }
        }
        responsEHendler(max,nil)
        
        
    }
    static var arraylist:[tblTamreenInfo] = []
    static var resultelist:Results<tblTamreenInfo>!
    class func ModelArrayALLData() -> [tblTamreenInfo]{
        var modelAry = [tblTamreenInfo]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblTamreenInfo.getAllTamreen() {(response, Error) in
            resultelist = (response) as? Results<tblTamreenInfo>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
}

