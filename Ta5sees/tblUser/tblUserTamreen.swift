//
//  tblUserTamreen.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class  tblUserTamreen:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refTamreenInfoID = "0"
    @objc dynamic var refUserID = "0"
    @objc dynamic var date = ""
    @objc dynamic var periodTmreen = ""
    @objc dynamic var calorisBurn = ""
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserTamreen.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func setTamreenBurnHistory(iduser:String,idTamreen:String,periodTime:String,date:String,calorisBurn:String,responsEHendler:@escaping (Any?,Error?)->Void){
        
        let ob = tblUserTamreen()
        ob.id = ob.IncrementaID()
        ob.refUserID = iduser
        ob.refTamreenInfoID = idTamreen
        ob.calorisBurn = calorisBurn
        ob.periodTmreen = periodTime
        ob.date = date
        try! setupRealm().write {
            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
            responsEHendler("success",nil)
        }
        responsEHendler("failure",nil)
        
    }
    
    static func getItemsBurned(date:String,responsEHendler:@escaping (Any?,Error?)->Void){
           responsEHendler(setupRealm().objects(tblUserTamreen.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date), nil)
       }
}

