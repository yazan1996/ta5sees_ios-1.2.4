//
//  tblPregancyMonths.swift
//  Ta5sees
//
//  Created by Admin on 9/3/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import  SwiftyJSON
class  tblPregancyMonths:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String = ""
    
    
    static var arrayLayaqa:[tblPregancyMonths] = []
    static var listLayaqa:Results<tblPregancyMonths>!
    static var layaqDesc :[String:String]=[:]
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    static func getData() ->Array<String>{
        var arrayDisc:[String]=[]
        
        listLayaqa = (setupRealm().objects(tblPregancyMonths.self))
        arrayLayaqa = Array(listLayaqa)
        for x in 0..<listLayaqa.count {
            arrayDisc.append(listLayaqa[x].discription)
        }
        
        
        return arrayDisc
    }
    
    
    static func getAllData(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblPregancyMonths.self),nil)
    }
    
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        rj.readJson(tableName: "others/tblPregancyMonths") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                            let obj = tblPregancyMonths(value:["id" : item["id"].intValue, "discription": item["description"].string!])
                               setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
 
                        }
                    }
                    completion(true)
                }
                
            }
        }
    }
    
    
    static func showPragnentMonths(completion: @escaping ([Item],Error?) -> Void){
        var arrayID:[Item]=[]
        
        listLayaqa = (setupRealm().objects(tblPregancyMonths.self))
        arrayLayaqa = Array(listLayaqa)
        for x in 0..<listLayaqa.count {
            arrayID.append(Item(id: String(listLayaqa[x].id),name: listLayaqa[x].discription))
        }
        
        completion(arrayID,nil)
    }
    
    static func getDataID() ->Array<Int>{
        var arrayID:[Int]=[]
        
        listLayaqa = (setupRealm().objects(tblPregancyMonths.self))
        arrayLayaqa = Array(listLayaqa)
        for x in 0..<listLayaqa.count {
            arrayID.append(listLayaqa[x].id)
        }
        
        return arrayID
    }
}

