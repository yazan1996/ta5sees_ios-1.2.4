//
//  tblWajbehInfoKACL.swift
//  Ta5sees
//
//  Created by Admin on 7/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class tblWajbehInfoKACL:Object {
    @objc dynamic var id = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var BreakFastAllowed = 0
    @objc dynamic var BreakFastEate = 0
    @objc dynamic var LaunchAllowed = 0
    @objc dynamic var LaunchEate = 0
    @objc dynamic var DinnerAllowed = 0
    @objc dynamic var DinnerEate = 0
    @objc dynamic var Snak1Allowed = 0
    @objc dynamic var Snak1Eate = 0
    @objc dynamic var Snak2Allowed = 0
    @objc dynamic var Snak2Eate = 0
    @objc dynamic var tamreenAllowed = 0
    @objc dynamic var tamreenBurin = 0

    

    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblExchangeSystem.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
