//
//  tblUserMidicalTest.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class  tblUserMidicalTest:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refMidicalTestID = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var insertDate: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserMidicalTest.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
