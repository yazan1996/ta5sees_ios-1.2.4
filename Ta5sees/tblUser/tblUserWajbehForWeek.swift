//
//  tblUserWajbehForWeek.swift
//  Ta5sees
//
//  Created by Admin on 10/1/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUserWajbehForWeek:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var date = ""
    @objc dynamic var refTblUser:tblWajbeh!
    @objc dynamic var ratio = 0
    @objc dynamic var wajbehInfiItem = "0"
    @objc dynamic var userID = 0
    @objc dynamic var fatInfo = "0"
    @objc dynamic var markEaten = ""
    @objc dynamic var replaceID = 0
    
    static let shared = tblUserWajbehForWeek()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserWajbehForWeek.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    static func getDataMealPlaner(wajbrhInfo:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblUserWajbehForWeek.self).filter("userID == %@ AND wajbehInfiItem == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!,wajbrhInfo),nil)
    }
    
    static func getdataJoin(responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblUserWajbehForWeek.self).filter("userID == %@ AND wajbehInfiItem == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!,"3"),nil)
    }
    
    static var arraylist:[tblUserWajbehForWeek] = []
    static var resultelist:Results<tblUserWajbehForWeek>!
    class func generateModelArray() -> [tblUserWajbehForWeek]{
        var modelAry = [tblWajbeh]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblUserWajbehForWeek.getdataJoin() {(response, Error) in
            resultelist = (response) as? Results<tblUserWajbehForWeek>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
    static func UpdateRow(id:Int,idreplace:Int){
        let obW = setupRealm().objects(tblWajbeh.self).filter("id == %@",idreplace).first
        let ob = setupRealm().objects(tblUserWajbehForWeek.self).filter("id == %@",id)
        try! setupRealm().write {
            ob.setValue(obW, forKey: "refTblUser")
        }
    }
    
    static func UpdateRowMark(id:Int,date:String,marke:String){
        let ob = setupRealm().objects(tblUserWajbehForWeek.self).filter("userID == %@ AND id == %@ AND date == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!,id,date)
        if setupRealm().isInWriteTransaction == true {
            ob.setValue(marke, forKey: "markEaten")
        }else{
            try! setupRealm().write {
                ob.setValue(marke, forKey: "markEaten")
            }
        }
        
    }
    static func checkWajbehDate(date:String,wajbehInfo:String,responsEHendler:@escaping (Any?,Error?)->Void){
    print(date)
        let ob =  setupRealm().objects(tblUserWajbehForWeek.self).filter("userID == %@ AND date == %@ AND wajbehInfiItem == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!,date,wajbehInfo).first
//    print("ob \(ob)")
        if ob != nil {
            responsEHendler("found",nil)
        }else{
            responsEHendler("notfound",nil)
        }
    }
    
    
}

