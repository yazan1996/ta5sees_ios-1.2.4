//
//  tblUser.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUser:Object {
    
    static var sheard = tblUser()
    @objc dynamic var id:Int = 0
    @objc dynamic var firstName = ""
    @objc dynamic var lastName = ""
    @objc dynamic var refGenderID = ""
    @objc dynamic var height = ""
    @objc dynamic var weight = ""
    @objc dynamic var birthday = ""
    @objc dynamic var refLayaqaCondID = ""
    @objc dynamic var reftblGoalinfoID = ""
    @objc dynamic var reftblCounty = ""
    @objc dynamic var mobNum = ""
    @objc dynamic var password = ""
    @objc dynamic var refPaymentTypeID = ""
    @objc dynamic var insertDateTime = ""
    @objc dynamic var refRegistryTypeID = ""
    @objc dynamic var imgPath = ""
    @objc dynamic var refPlanMaster = ""
    @objc dynamic var mealDistrbution = ""
    @objc dynamic var fatInfo = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUser.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    func getdataUser(callBack:(tblUser)->Void){
        callBack(setupRealm().objects(tblUser.self).filter("id == %@",Int(getDataFromSheardPreferanceString(key:"userID"))!).first!)
    }
    
    static  func checkFoundIDUser (iduser:Int,callBack:(Bool)->Void) {
        do {
            guard let user = setupRealm().objects(tblUser.self).filter("id == %@",iduser).first  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            setDataInSheardPreferance(value: user.mealDistrbution, key: "mealDistrbution")
            setDataInSheardPreferance(value: user.fatInfo, key: "fatInfo")
            setDataInSheardPreferance(value: String(user.id), key: "userID")
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    
    static  func checkPasswordUser (iduser:Int,pass:String,callBack:(Bool)->Void) {
          do {
              guard let user = setupRealm().objects(tblUser.self).filter("id == %@ && password == %@",iduser,pass).first  else  {
                  callBack(false)
                  throw MyError.FoundNil("false")
              }
              setDataInSheardPreferance(value: user.mealDistrbution, key: "mealDistrbution")
              setDataInSheardPreferance(value: user.fatInfo, key: "fatInfo")
              setDataInSheardPreferance(value: String(user.id), key: "userID")
              callBack(true)
              throw MyError.FoundNil("true")
              
          } catch let error as NSError {
              print("error get user data \(error)")
          }
          
          
      }
//    static  func checkFoundIDUserdelet (iduser:String) {
//        let realm = try! Realm()
//
//        if (iduser == "nil"){
//            print("not found")
//        }else{
//           try! realm.write {
//
//                realm.delete(setupRealm().objects(tblUser.self).filter("mobNum == %@",iduser))
//            }
//
//        }
//
//    }
    
    static  func checkFoundIDUserMSISDN (iduser:String,callBack:(Bool)->Void) {
         do {
            guard setupRealm().objects(tblUser.self).filter("mobNum == %@",iduser).first != nil  else  {
                 callBack(false)
                 throw MyError.FoundNil("false")
             }
            
             callBack(true)
             throw MyError.FoundNil("true")
             
         } catch let error as NSError {
             print("error get user data \(error)")
         }
         
         
     }
  
}
//enum MyError: Error {
//    case FoundNil(String)
//}
