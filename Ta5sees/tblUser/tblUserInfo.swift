//
//  tblUserInfo.swift
//  Ta5sees
//
//  Created by Admin on 8/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON
import FirebaseFirestore
import Firebase
import SVProgressHUD
class tblUserInfo:Object {
    
    
    static var sheard = tblUserInfo()
    @objc dynamic var id = ""
    @objc dynamic var firstName = ""
    @objc dynamic var facebookID = ""
    @objc dynamic var refGenderID = ""
    @objc dynamic var height = ""
    @objc dynamic var weight = ""
    @objc dynamic var birthday = ""
    @objc dynamic var refLayaqaCondID = ""
    @objc dynamic var reftblCountryID = ""
    @objc dynamic var mobNum = ""
    @objc dynamic var password = ""
    @objc dynamic var insertDateTime = ""
    @objc dynamic var refPlanMasterID = ""
    @objc dynamic var mealDistributionId = ""
    @objc dynamic var target = ""
    @objc dynamic var refCuiseseID = ""
    @objc dynamic var gramsToLose = ""
    @objc dynamic var grown = ""
    @objc dynamic var loginType = ""
    @objc dynamic var weeksNeeded = ""
    @objc dynamic var twitterID = ""
    @objc dynamic var totalCalories = ""
    @objc dynamic var refAllergyInfoID = ""
    @objc dynamic var mealPlan = ""
    @objc dynamic var appleID = ""
    @objc dynamic var endSubMilli:Double = 0.0
    @objc dynamic var startSubMilli:Double = 0.0
    @objc dynamic var subscribeType = -1 // 0->free -- 1->buy
    @objc dynamic var expirDateSubscribtion = "0.0"
    @objc dynamic var subDuration = "0"
    
    @objc dynamic var frute = "-1"
    @objc dynamic var meat = "-1"
    @objc dynamic var vegetabels = "-1"
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    //    func IncrementaID() -> Int{
    //        return (setUpRealm().objects(tblUserInfo.self).max(ofProperty: "id") as Int? ?? 0) + 1
    //    }
    //
    
    
    static func getdataUser(callBack:(tblUserInfo)->Void){
        callBack(setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key:"userID")).first!)
    }
    static func updateTargetWeightUser(totalCalories:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        
        
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "totalCalories" : Double(totalCalories)!
        ]  as [String : Any]
        
        
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(totalCalories,forKey: "totalCalories")
                    responsEHendler("success",nil)
                }
                responsEHendler("success",nil)
                
            }
        }
        
        
        
    }
    
    static func updateMeatFrutVegUser(frute:String,meat:String,vegetabels:String){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        try! setupRealm().write {
            ob.setValue(meat,forKey: "meat")
        }
        try! setupRealm().write {
            ob.setValue(frute,forKey: "frute")
        }
        try! setupRealm().write {
            ob.setValue(vegetabels,forKey: "vegetabels")
        }
        
    }
    
    
    static func updateGrownUser(grown:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        
        
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "grown" : grown
        ]  as [String : Any]
        
        
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(grown,forKey: "grown")
                    responsEHendler("success",nil)
                }
                responsEHendler("success",nil)
                
            }
        }
        
        
        
    }
    //    static func updateGrownUser1(){
    //        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
    //
    //        try! setupRealm().write {
    //            ob.setValue("2005-02-08",forKey: "birthday")
    //            //         birthday = 2019-02-08;
    //            //               ob.setValue("2",forKey: "grown")
    //
    //        }
    //    }
    
    
    static func updateWeightHeigthUser(height:String,weight:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "height" : Double(height)!,
            "weight" :  Double(weight)!
        ]  as [String : Any]
        
        
        SVProgressHUD.show(withStatus: "جاري تحديث بياناتك")
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(height,forKey: "height")
                }
                try! setupRealm().write {
                    ob.setValue(weight,forKey: "weight")
                }
                responsEHendler("success",nil)
                
            }
        }
        
        
    }
    
    static func updateWeightUser(weight:String,callBack:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "weight" :  Double(weight)!
        ]  as [String : Any]
        
        
        
        SVProgressHUD.show(withStatus: "جاري تحديث وزنك")
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                callBack("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(weight,forKey: "weight")
                }
                callBack("success",nil)
                
            }
        }
        
        
    }
    
    
    static func updateDatathUser(userID:String,refLayaqaCondID:String,target:String,weight:String,weeksNeeded:String,gramsToLose:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",userID)
        
        
        let db = Firestore.firestore()
        
        let arrInfo = [
            "refLayaqaCondID" : Int(refLayaqaCondID)!,
            "target" : Double(target )!,
            "weight" :  Double(weight)!,
            "weeksNeeded" : Int(weeksNeeded)!,
            "gramsToLose" :  Float(gramsToLose)!
        ]  as [String : Any]
        
        
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
            } else {
                try! setupRealm().write {
                    ob.setValue(target,forKey: "target")
                }
                try! setupRealm().write {
                    ob.setValue(weight,forKey: "weight")
                }
                try! setupRealm().write {
                    ob.setValue(refLayaqaCondID,forKey: "refLayaqaCondID")
                }
                try! setupRealm().write {
                    ob.setValue(gramsToLose,forKey: "gramsToLose")
                }
                try! setupRealm().write {
                    ob.setValue(weeksNeeded,forKey: "weeksNeeded")
                }
                responsEHendler("success",nil)
                
            }
        }
    }
    
    
    static func EditeInfoCalculateUser(progressHistory:tblUserProgressHistory,responsEHendler:@escaping (String,Error?)->Void){
        
        if progressHistory == nil {
            responsEHendler("false",nil)
        }
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getUserInfo().id).first!,flag:"2", user_ProgressHistory: progressHistory)
        
        try! setupRealm().write {
            ob.setValue(progressHistory.birthday,forKey: "birthday")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.refCuiseseID,forKey: "refCuiseseID")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.refLayaqaCondID,forKey: "refLayaqaCondID")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.refAllergyInfoID,forKey: "refAllergyInfoID")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.refGenderID,forKey: "refGenderID")
        }
        
        try! setupRealm().write {
            ob.setValue(progressHistory.gramsToLose,forKey: "gramsToLose")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.target,forKey: "target")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.refPlanMasterID,forKey: "refPlanMasterID")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.weight,forKey: "weight")
        }
        try! setupRealm().write {
            ob.setValue(progressHistory.height,forKey: "height")
        }
        
        try! setupRealm().write {
            ob.setValue(progressHistory.weeksNeeded,forKey: "weeksNeeded")
        }
        
        
        responsEHendler("success",nil)
        
    }
    
    static func updateNameDatethUser(firstName:String,birthday:String,refCuiseseID:String,refLayaqaCondID:String,refAllergyInfoID:String,gender:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        
        
        
        let db = Firestore.firestore()
        
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        dateFormatter.dateFormat = "dd - MM - yyyy"
        
        let d = dateFormatter1.date(from: birthday)
        
        let newFormate = dateFormatter.string(from: d!)
        
        let arrInfo = [
            "firstName" : firstName,
            "birthday" :  newFormate,
            "refCuiseseID" :  refCuiseseID ,
            "refLayaqaCondID" : Int(refLayaqaCondID)!,
            "refAllergyInfoID" : refAllergyInfoID,
            "refGenderID" :Int(gender)!
        ]  as [String : Any]
        
        
        SVProgressHUD.show(withStatus: "جاري تحديث بياناتك")
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(firstName,forKey: "firstName")
                }
                try! setupRealm().write {
                    ob.setValue(birthday,forKey: "birthday")
                }
                try! setupRealm().write {
                    ob.setValue(refCuiseseID,forKey: "refCuiseseID")
                }
                try! setupRealm().write {
                    ob.setValue(refLayaqaCondID,forKey: "refLayaqaCondID")
                }
                try! setupRealm().write {
                    ob.setValue(refAllergyInfoID,forKey: "refAllergyInfoID")
                }
                try! setupRealm().write {
                    ob.setValue(gender,forKey: "refGenderID")
                }
                
                responsEHendler("success",nil)
            }
        }
    }
    
    static func updateNameUser(firstName:String,responsEHendler:@escaping (String,Error?)->Void){
        let ob = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
        
        let db = Firestore.firestore()
        
        let arrInfo = [
            "firstName" : firstName
        ]  as [String : Any]
        
        
        SVProgressHUD.show(withStatus: "جاري تحديث بياناتك")
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                try! setupRealm().write {
                    ob.setValue(firstName,forKey: "firstName")
                }
                
                responsEHendler("success",nil)
            }
        }
    }
    static  func generteAgainMealPlanner(callBack:@escaping (String,Error?)->Void){
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var first7Days:Int!
        if getUserInfo().subscribeType == -1 {
            callBack("failed",nil)
        }else {
            ActivationModel.sharedInstance.subscribeType = getUserInfo().subscribeType // buy
            
            _ = getUserInfo().startSubMilli
            let endDate = getUserInfo().endSubMilli
            
            var count = 0
            for i in 0...50 {
                print( Date().getDays(value: i))
                if Date().getDaysDate(value:i) > endDate {
                    break
                }
                count+=1
            }
            
            ActivationModel.sharedInstance.startPeriod = 0
            ActivationModel.sharedInstance.endPeriod = count-1
            if ActivationModel.sharedInstance.endPeriod < 0 {
                ActivationModel.sharedInstance.endPeriod = 0
            }
            print("count",count)
            if count >= 7 {
                first7Days = 6
            }else{
                first7Days = count
            }
            
            let queue = DispatchQueue(label: "com.appcoda.myqueue")
            
            //            queue.sync {
            //                callBack("success",nil)
            //            }
            dispatchWorkItem = DispatchWorkItem {
                
                //            queue.async {
                if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                    if first7Days != count {
                        _ =   tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod+7,endPeriod:   ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//ActivationModel.sharedInstance.endPeriod
                        //                        let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
                        //                        try! setupRealm().write{
                        //                                setupRealm().add(newWjbah, update:.modified)
                        
                        //                        }
                    }
                }
                if dispatchWorkItem != nil {
                    print("finish generate and cancel thread ....")
                    callBack("sucess",nil)
                    dispatchWorkItem.cancel()
                }
                ActivationModel.sharedInstance.dispose()
            }
            
            queue.async {
                _ = tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:   ActivationModel.sharedInstance.startPeriod,endPeriod:  first7Days,date:getDateOnly())
                queue.async(execute: dispatchWorkItem)
                //                let newWjbah = r[0] as! [tblWajbatUserMealPlanner]
                //                try! setupRealm().write{
                //                        setupRealm().add(newWjbah, update:.modified)
                //
                //                }
                SVProgressHUD.dismiss {
                    //                    callBack("success",nil)
                }
            }
            //            SVProgressHUD.show(withStatus: "جاري تهيئة و جباتك")
            
            //                tblPackeg.updateMealPlannerFree7Days(date:getDateOnly()) { (err) in
            //                    SVProgressHUD.dismiss {
            //                        ActivationModel.sharedInstance.dispose()
            //                    callBack("success",nil)
            //                    }
            //                }
            
        }
        
        
    }
    
    static  func updateMealPlanner(responsEHendler:@escaping (String,Error?)->Void){
        var filterList = [tblWajbatUserMealPlanner]()
        var filteringPackge = [tblShoppingList]()
        var first7Days:Int!
        var expirDays:Int!
        var days:Int!
        
        print("@3")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if getUserInfo().subscribeType == -1 ||  getUserInfo().subscribeType == 3 ||  getUserInfo().subscribeType == 2 { // غير مشترك
            print("@55")
            responsEHendler("failed",nil)
        }else {
//            if getUserInfo().subscribeType == 1 {
//                ActivationModel.sharedInstance.subscribeType = 1 // buy
//            }else if getUserInfo().subscribeType == 4 {
//                ActivationModel.sharedInstance.subscribeType = 4 // grace period
//
//            }else{
//                ActivationModel.sharedInstance.subscribeType = 0 // free
//            }
            
            ActivationModel.sharedInstance.subscribeType = getUserInfo().subscribeType
            
            filterList = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id).filter { (item) -> Bool in
                if dateFormatter.date(from: item.date)!.timeIntervalSince1970 >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                    return true
                }
                return false
            }
            
            filteringPackge = setupRealm().objects(tblShoppingList.self).filter("userID == %@",getUserInfo().id).filter { (item) -> Bool in
                if  item.date >= dateFormatter.date(from: getDateOnly())!.timeIntervalSince1970 {
                    return true
                }
                return false
            }
            
            print(filterList,"filterList",filterList.map {$0.date}.removeDuplicates().count)
            
            
            let dateUser = Date(timeIntervalSince1970: TimeInterval(getUserInfo().endSubMilli) / 1000)
            let startMili = Date(timeIntervalSince1970: TimeInterval(getUserInfo().startSubMilli) / 1000)
            
            days = Date().daysBetween(start: getDateOnlyasDate(), end: dateUser)
            print("days",days as Any,Date().getDaysDate(value: days),getUserInfo().endSubMilli)
            expirDays = Date().daysBetween(start: getDateOnlyasDate(), end: startMili)
            
            ActivationModel.sharedInstance.startSubMilli = 0
            
            if days == 0 {
                ActivationModel.sharedInstance.startPeriod = 0
                
            }else{
                ActivationModel.sharedInstance.startPeriod = days-(filterList.map {$0.date}.removeDuplicates().count-1)
            }
            
            if  ActivationModel.sharedInstance.startPeriod >= 30 {
                ActivationModel.sharedInstance.startPeriod = 0
            }
            ActivationModel.sharedInstance.endPeriod =  days-1
            ActivationModel.sharedInstance.endSubMilli = days
            
            ActivationModel.sharedInstance.expireDate = Double(getUserInfo().expirDateSubscribtion)!
            
        }
        
        SVProgressHUD.show(withStatus: "جاري تحضير وجباتك")
        
        let queue = DispatchQueue(label: "com.appcoda.myqueue")
        
        dispatchWorkItem = DispatchWorkItem {
            
            //        queue.async {
            if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
                if first7Days != days {
                    
                    _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:first7Days+1,endPeriod: ActivationModel.sharedInstance.endPeriod,date:getDateOnly())//  ActivationModel.sharedInstance.endPeriod
                    //                    try! setupRealm().write{
                    //                        if !filterList.isEmpty{
                    //                            setupRealm().delete(filterList)
                    //                        }
                    //                    }
                    try! setupRealm().write{
                        if !filteringPackge.isEmpty{
                            setupRealm().delete(filteringPackge)
                        }
                    }
                }
            }
            if dispatchWorkItem != nil {
                print("finish generate and cancel thread ....")
                dispatchWorkItem.cancel()
            }
            ActivationModel.sharedInstance.dispose()
        }
        queue.async {
            
            if days >= 7 {
                first7Days = 6
            }else{
                first7Days = days
            }
            _=tblPackeg.GenerateMealPlannerFree7Days11(startPeriod:0,endPeriod:  first7Days,date:getDateOnly())
            SVProgressHUD.dismiss {
                queue.async(execute: dispatchWorkItem)
                
                //                ActivationModel.sharedInstance.dispose()
                responsEHendler("success",nil)
            }
        }
        
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
        //            tblPackeg.updateMealPlannerFree7Days(date:getDateOnly()) { (err) in
        //                SVProgressHUD.dismiss {
        //                    print("@11")
        //                    try! setupRealm().write{
        //                        if !filterList.isEmpty{
        //                            print("@9")
        //                            setupRealm().delete(filterList)
        //                        }
        //                    }
        //                    try! setupRealm().write{
        //                        if !filteringPackge.isEmpty{
        //                            setupRealm().delete(filteringPackge)
        //                        }
        //                    }
        //                    ActivationModel.sharedInstance.dispose()
        //                    responsEHendler("success",nil)
        //                }
        //            }
        //        }
        
    }
    static  func getMealDistrbutionUser(){
        let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
        if item?.isLunchMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "2"
        }else if item?.isDinnerMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "3"
        }else if item?.isBreakfastMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "1"
        }
        if item?.Breakfast == 1 && item?.isBreakfastMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("1")
        }
        if item?.Dinner == 1 && item?.isDinnerMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("3")
        }
        if item?.Lunch == 1 && item?.isLunchMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("2")
        }
        if item?.Snack1 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("4")
        }
        if item?.Snack2 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("5")
        }
        ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refCuiseseID))
        
        ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//.append(contentsOf:Array(arrayLiteral: getUserInfo().refAllergyInfoID))
        
    }
    static func updateInfoUserWeightTarget(target:String,responsEHendler:@escaping (String,Error?)->Void){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "target" :  Double(target)! ]  as [String : Any]
        
        
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                let list = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
                print("Document successfully updated")
                try! setupRealm().write {
                    list.setValue(target,forKey: "target")
                    
                }
                responsEHendler("success",nil)
                
            }
        }
        
        
    }
    static func updateInfoUser(target:String,weeksNeeded:String,gramsToLose:String,responsEHendler:@escaping (String,Error?)->Void){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        let db = Firestore.firestore()
        
        
        
        let arrInfo = [
            "weeksNeeded" : Int(weeksNeeded)!,
            "gramsToLose" :  Float(gramsToLose)!,
            "target" :  Double(target)! ]  as [String : Any]
        
        
        db.collection("Users").document(getDataFromSheardPreferanceString(key: "userID")).updateData(arrInfo) { err in
            if let err = err {
                print("Error updating document: \(err)")
                responsEHendler("false",nil)
                
            } else {
                let list = setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID"))
                print("Document successfully updated")
                try! setupRealm().write {
                    list.setValue(target,forKey: "target")
                    
                }
                try! setupRealm().write {
                    
                    list.setValue(gramsToLose,forKey: "gramsToLose")
                }
                try! setupRealm().write {
                    
                    list.setValue(weeksNeeded,forKey: "weeksNeeded")
                }
                responsEHendler("success",nil)
                
            }
        }
        
        
    }
    
    
    
    
    static  func checkPasswordUser (iduser:String,pass:String,callBack:(Bool)->Void) {
        do {
            guard let user = setupRealm().objects(tblUserInfo.self).filter("id == %@ && password == %@",iduser,pass).first  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            setDataInSheardPreferance(value: user.mealDistributionId, key: "mealDistrbution")
            setDataInSheardPreferance(value: user.id, key: "userID")
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    static  func checkFoundIDUserMSISDN (iduser:String,callBack:(Bool)->Void) {
        do {
            guard setupRealm().objects(tblUserInfo.self).filter("mobNum == %@",iduser).first != nil  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    
    
    
    
    static  func checkFoundIDUserFacebook (iduser:String,callBack:(Bool)->Void) {
        do {
            guard setupRealm().objects(tblUserInfo.self).filter("facebookID == %@",iduser).first != nil  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    
    static  func checkFoundIDUserTwitterID (iduser:String,callBack:(Bool)->Void) {
        do {
            guard setupRealm().objects(tblUserInfo.self).filter("twitterID == %@",iduser).first != nil  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    
    
    static  func checkFoundIDUserAppleID (iduser:String,callBack:(Bool)->Void) {
        do {
            guard setupRealm().objects(tblUserInfo.self).filter("appleID == %@",iduser).first != nil  else  {
                callBack(false)
                throw MyError.FoundNil("false")
            }
            
            callBack(true)
            throw MyError.FoundNil("true")
            
        } catch let error as NSError {
            print("error get user data \(error)")
        }
        
        
    }
    
    static func saveUserInfoData(doc:DocumentSnapshot,callback:(tblUserInfo)->Void){
        let obUser:tblUserInfo = tblUserInfo()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter1.dateFormat = "dd - MM - yyyy"
        
        let d = dateFormatter1.date(from:  doc.get("birthday") as! String)
        
        let newFormate = dateFormatter.string(from: d!)
        setDataInSheardPreferance(value: doc.get("id") as! String , key: "userID")
        let bmi = doc.get("weight") as! Double/(doc.get("height") as! Double/100.0) * ((doc.get("height") as! Double/100.0))
        setDataInSheardPreferance(value:  String(doc.get("refLayaqaCondID") as! Int), key: "refLayaqaCondID")
        setDataInSheardPreferance(value: String(bmi), key: "bmi")
        setDataInSheardPreferance(value: String(doc.get("weight") as! Double), key: "txtweight")
        setDataInSheardPreferance(value: String(Int(round(doc.get("height") as! Double))), key: "txtheight")
        setDataInSheardPreferance(value:  String(doc.get("refGenderID") as! Int), key: "gender")
        setDataInSheardPreferance(value: doc.get("grown") as! String, key: "UserAgeID")
        setDataInSheardPreferance(value:  String(doc.get("refPlanMasterID") as! Int), key: "dietType")
        setDataInSheardPreferance(value: String(doc.get("gramsToLose") as! Double), key: "incrementKACL")
        setDataInSheardPreferance(value: String(doc.get("firstName") as! String), key: "name")
        
        setDataInSheardPreferance(value: doc.get("id") as! String , key: "mobNum") // id user
        
        
        if doc.get("refCuiseseID") as? String ?? "null" != "null"  {
            tblCuisnesUSerItems.updateUserCuisnesID(id_User: doc.get("id") as! String,str: doc.get("refCuiseseID") as! String)
        }
        if doc.get("refAllergyInfoID") as? String ?? "null" != "null"  {
            tblAllergyUserItems.updateUserAllergyID(id_User: doc.get("id") as! String,str: doc.get("refAllergyInfoID") as! String)
        }
        
        
        let ageChild = calclateAge(str: newFormate)
        setDataInSheardPreferance(value: String(round(ageChild)), key: "dateInYears")
        setDataInSheardPreferance(value:newFormate, key: "txtdate")
        obUser.id  = doc.get("id") as! String
        
        print("ididid",doc.get("id") as! String)
        obUser.firstName = doc.get("firstName") as! String
        obUser.facebookID = doc.get("facebookID") as? String ?? "null"
        obUser.refGenderID = String(doc.get("refGenderID") as! Int)
        obUser.height = String(Int(round(doc.get("height") as! Double)))
        obUser.weight = String(doc.get("weight") as! Double)
        obUser.birthday = newFormate
        
        if doc.get("subscribeType") != nil {
            obUser.subscribeType = doc.get("subscribeType") as! Int
        }
        if doc.get("endSubMilli") != nil {
            obUser.endSubMilli = doc.get("endSubMilli") as! Double
            
        }
        if doc.get("startSubMilli") != nil {
            obUser.startSubMilli = doc.get("startSubMilli") as! Double
        }
        
        if doc.get("meats") != nil {
            obUser.meat = doc.get("meats") as! String
        }
        if doc.get("fruits") != nil {
            obUser.frute = doc.get("fruits") as! String
            
        }
        if doc.get("vegetables") != nil {
            obUser.vegetabels = doc.get("vegetables") as! String
        }
        
        if doc.get("expiryDate") != nil {
            obUser.expirDateSubscribtion = "\(doc.get("expiryDate") as! Double)"
        }
        
        if doc.get("subDuration") != nil {
            obUser.subDuration = "\(doc.get("subDuration") as! Int)"
        }
        
        if doc.get("reftblCountryID") != nil {
            obUser.reftblCountryID = "\(doc.get("reftblCountryID") as! Int)"
        }
        
        if doc.get("password") != nil {
            obUser.password = doc.get("password") as? String ?? "null"
        }
        
        obUser.refLayaqaCondID = String(doc.get("refLayaqaCondID") as! Int)
//        obUser.reftblCountryID = doc.get("reftblCountryID")//String( doc.get("reftblCountryID") as! Int)
        obUser.mobNum = doc.get("mobNum") as? String ?? "null"
//        obUser.password = doc.get("password") as? String ?? "null"
        obUser.insertDateTime = doc.get("insertDateTime") as! String
        obUser.refPlanMasterID =  String(doc.get("refPlanMasterID") as! Int)
        obUser.mealDistributionId =  String(doc.get("mealDistributionId") as! Int)
        obUser.target =  String(doc.get("target") as! Double)
        //001236.acdc7413a2ff45b28b812db28119a597.1358 shams
        if doc.get("refCuiseseID") == nil {
            print("refCuiseseID == null")
            obUser.refCuiseseID = "-1"//doc.get("refAllergyInfoID") as? String ?? "-1"
        }else{
        obUser.refCuiseseID = doc.get("refCuiseseID") as? String ?? "-1"
        }
        
//        obUser.refCuiseseID = doc.get("refCuiseseID") as? String ?? "-1"//doc.get("refCuiseseID") as! String
        obUser.gramsToLose = String( doc.get("gramsToLose") as! Double)
        obUser.grown =  doc.get("grown") as! String
        obUser.loginType = String(doc.get("loginType") as! Int)
        obUser.weeksNeeded = String( doc.get("weeksNeeded") as! Int)
        obUser.twitterID =  doc.get("twitterID") as? String ?? "null"
        obUser.totalCalories =  String(doc.get("totalCalories") as! Double)
        
        if doc.get("refAllergyInfoID") == nil {
            print("refAllergyInfoID == null")
            obUser.refAllergyInfoID = "-1"//doc.get("refAllergyInfoID") as? String ?? "-1"
        }else{
            obUser.refAllergyInfoID = doc.get("refAllergyInfoID") as? String ?? "-1"
        }
     
        obUser.appleID =  doc.get("appleID") as? String ?? "null"
        
        
        try! setupRealm().write{
            setupRealm().add(obUser, update: Realm.UpdatePolicy.modified)
        }
        callback(obUser)
        
    }
}
enum MyError: Error {
    case FoundNil(String)
}
