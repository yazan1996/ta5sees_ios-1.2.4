//
//  tblDailyWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblDailyWajbeh:Object {
    
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var totalCalAllowed = 0
    @objc dynamic var totalCalEaten = 0
    @objc dynamic var totalCalBurned = 0
    @objc dynamic var totalLeft = 0
    @objc dynamic var totalCarb = 0
    @objc dynamic var totalFat = 0
    @objc dynamic var totalProtein = 0
    @objc dynamic var date = ""
    
    
    static func IncrementaID() -> Int{
        return (setupRealm().objects(tblDailyWajbeh.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func setUserTrackerDaily(userid:String,carbo:Int,protien:Int,fat:Int,caloris:Int,burned:Int,eaten:Int,left:Int,date:String,responsEHendler:@escaping (Any?,Error?)->Void){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        let list = setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        if !list.isEmpty {
            if let item = list.first {
              
            try! setupRealm().write {
                item.setValue(carbo,forKey: "totalCarb")
                }
                try! setupRealm().write {

                item.setValue(protien,forKey: "totalProtein")
                }
                try! setupRealm().write {

                item.setValue(fat,forKey: "totalFat")
                }
                try! setupRealm().write {

                item.setValue(abs(caloris-item.totalCalEaten),forKey: "totalCalAllowed")
                }
                try! setupRealm().write {
                item.setValue(date,forKey: "date")
                }

                responsEHendler(true,nil)
                }
            
            
        }else{
            let obj = tblDailyWajbeh(value: ["id" :tblDailyWajbeh.IncrementaID(), "refUserID": userid,"totalCarb":carbo ,"totalProtein":protien,"totalFat":fat,"totalCalAllowed":caloris,"totalCalBurned":burned,"totalLeft":left,"totalCalEaten":eaten,"date":date])
            try! setupRealm().write {
                setupRealm().add(obj, update: Realm.UpdatePolicy.modified)
                responsEHendler(true,nil)
            }
        }
      
        responsEHendler(false,nil)
    }
    
    static func getDailyData(date:String,responsEHendler:@escaping (Any?,Error?)->Void){

        let ob = setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date).last
        if ob != nil {
            responsEHendler(ob,nil)
        }else{
            
            responsEHendler("false",nil)
        }
        
    }
    
    static func updateDataDaily(date:String,calorisBurned:Int,flag:String,compltion:@escaping (Any?,Error?)->Void){
        print("add To tblEaten 1 \(calorisBurned)")

        let ob = setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        if ob.isEmpty {
            print("NIL")
            setNewRow(date: date, calorisBurned: calorisBurned, flag: flag) { (res, err) in
                print("add row Daily --  response : \(res as? String ?? "update value")")
                compltion("true",nil)

            }
            compltion("true",nil)

            return
        }
        
        if setupRealm().isInWriteTransaction == true {

            if flag == "burned" {
                ob.setValue(ob[0].totalCalBurned+calorisBurned,forKey: "totalCalBurned")
//                ob.setValue((calorisBurned+ob[0].totalCalAllowed),forKey: "totalCalAllowed")
            }else if flag == "UnMark"{
                ob.setValue(ob[0].totalCalEaten-calorisBurned,forKey: "totalCalEaten")
                ob.setValue((ob[0].totalCalAllowed+calorisBurned),forKey: "totalCalAllowed")
            }else if flag == "UnBurned"{
                ob.setValue(ob[0].totalCalBurned-calorisBurned,forKey: "totalCalBurned")
            }else{
                ob.setValue(ob[0].totalCalEaten+calorisBurned,forKey: "totalCalEaten")
                ob.setValue((ob[0].totalCalAllowed-calorisBurned),forKey: "totalCalAllowed")
            }
            compltion("true",nil)
             return
        }else{
            try! setupRealm().write {
                if flag == "burned" {
                    ob.setValue(ob[0].totalCalBurned+calorisBurned,forKey: "totalCalBurned")
//                    ob.setValue((calorisBurned+ob[0].totalCalAllowed),forKey: "totalCalAllowed")
                }else if flag == "UnMark"{
                    ob.setValue(ob[0].totalCalEaten-calorisBurned,forKey: "totalCalEaten")
                    ob.setValue((ob[0].totalCalAllowed+calorisBurned),forKey: "totalCalAllowed")
                }else if flag == "UnBurned"{
                     ob.setValue(ob[0].totalCalBurned-calorisBurned,forKey: "totalCalBurned")
                }else{
                    ob.setValue(ob[0].totalCalEaten+calorisBurned,forKey: "totalCalEaten")
                    ob.setValue((ob[0].totalCalAllowed-calorisBurned),forKey: "totalCalAllowed")
                }
                compltion("true",nil)
                 return
            }
        }
    }
    
    
    static func setNewRow(date:String,calorisBurned:Int,flag:String,responsEHendler:@escaping (Any?,Error?)->Void){
        var ob:tblUserTEEDistribution!
        tblUserTEEDistribution.getDataUserTEE(date: date) { (res, err) in
            ob = res as? tblUserTEEDistribution
        }
        let dailyWajbeh = tblDailyWajbeh()
        dailyWajbeh.id = dailyWajbeh.IncrementaID()
        dailyWajbeh.refUserID = getDataFromSheardPreferanceString(key: "userID")
        if flag == "burned"{
//            dailyWajbeh.totalCalAllowed = Int(ob.Caloris)! + calorisBurned
            dailyWajbeh.totalCalAllowed = Int(ob.Caloris)!

            dailyWajbeh.totalCalBurned = calorisBurned
            dailyWajbeh.totalCalEaten = 0
        }else{
            dailyWajbeh.totalCalAllowed = Int(ob.Caloris)! - calorisBurned
            dailyWajbeh.totalCalEaten = calorisBurned
            dailyWajbeh.totalCalBurned = 0
            
        }
        dailyWajbeh.totalLeft = 0
        dailyWajbeh.totalCarb = Int(ob.Carbo)!
        dailyWajbeh.totalFat = Int(ob.Fat)!
        dailyWajbeh.totalProtein = Int(ob.Protien)!
        dailyWajbeh.date = date
        if setupRealm().isInWriteTransaction == true {
            setupRealm().add(dailyWajbeh, update: Realm.UpdatePolicy.modified)
            responsEHendler("true",nil)
        }else{
            try! setupRealm().write {
                setupRealm().add(dailyWajbeh, update: Realm.UpdatePolicy.modified)
                responsEHendler("true",nil)
            }
        }
    }
    
    
    
    
//    func getTotalCalEaten(date:String,responsEHendler:@escaping (Any?,Error?)->Void){
//
//        let realm = try! Realm()
//        let ob = realm.objects(tblDailyWajbeh.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date).first
//
//        if ob != nil {
//            responsEHendler("\(ob!.totalCalEaten)\nتم اكل","٠\nتم اكل" as? Error)
//        }else{
//            responsEHendler("٠\nتم اكل","٠\nتم اكل" as? Error)
//        }
//
//    }
//
//    func getTotalCalBurned(date:String,responsEHendler:@escaping (Any?,Error?)->Void){
//
//        let realm = try! Realm()
//        let ob = realm.objects(tblDailyWajbeh.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date).first
//
//        if ob != nil {
//            responsEHendler("\(ob!.totalCalBurned)\nتم حرق","٠\nتم حرق" as? Error)
//
//
//        }else{
//            responsEHendler("٠\nتم حرق","٠\nتم حرق" as? Error)
//
//
//        }
//
//    }
//
    
    
//    static func getProtein(responsEHendler:@escaping (Int,Error?)->Void){
//
//        let ob = Array(setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")))
//
//        let totalFat =  ob.map({$0.totalProtein}).reduce(0) { $0 + $1 }
//        print("totalFat \(totalFat)")
//        if totalFat>0 {
//            responsEHendler(totalFat,"0" as? Error)
//        }else{
//            responsEHendler(0,"0" as? Error)
//        }
//    }
//
//    static func getFate(responsEHendler:@escaping (Int,Error?)->Void){
//
//        let ob = Array(setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")))
//
//        let totalFat =  ob.map({$0.totalFat}).reduce(0) { $0 + $1 }
//        print("totalFat \(totalFat)")
//        if totalFat>0 {
//            responsEHendler(totalFat,"0" as? Error)
//        }else{
//            responsEHendler(0,"0" as? Error)
//        }
//
//    }
//
//    static func getCHO(responsEHendler:@escaping (Int,Error?)->Void){
//
//        let ob = Array(setupRealm().objects(tblDailyWajbeh.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")))
//        var sum = 0
//        let totalFat =  ob.map({$0.totalCarb})
//        for i in totalFat {
//            sum+=i
//        }
//        print("totalFat \(sum)")
//        if sum>0 {
//            responsEHendler(sum,"0" as? Error)
//        }else{
//            responsEHendler(0,"0" as? Error)
//        }
//
//    }

//
//    static func getEnergy(responsEHendler:@escaping (Any?,Error?)->Void){
//
//        let realm = try! Realm()
//        let ob = realm.objects(tblDailyWajbeh.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).first
//
//        if ob != nil {
//            responsEHendler(ob!.totalCalAllowed,nil)
//
//        }else{
//            responsEHendler(0,0 as? Error)
//
//        }
//
//    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblDailyWajbeh.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}

