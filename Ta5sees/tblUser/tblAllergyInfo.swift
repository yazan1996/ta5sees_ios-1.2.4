//
//  tblAllergyInfo.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class  tblAllergyInfo:Object {
    static var arrayAllergyInfo:[tblAllergyInfo] = []
    static var listAllergyInfo:Results<tblAllergyInfo>!
    static var AllergyInfoDesc :[String:String]=[:]
    static var txtallergyID = [String]()
    
    
    
    
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func getIDtblAllergyInfoSelected( string:String)-> String{
        let idAlergy:[String]! = string.split(separator: ",").map(String.init)

        txtallergyID.removeAll()
        for x in idAlergy {
            txtallergyID.append(setupRealm().objects(tblAllergyInfo.self).filter("discription contains '\(x)'").first!.id)
        }
        
        return txtallergyID.joined(separator:",")
    }
    
    
    func readArrayJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblAllergyInfo]()
        let thread =  DispatchQueue.global(qos: .utility)
        thread.async {
            if let recommends = JSON(response).array {

                for item in recommends {
                    
                    if item.isEmpty == false{
                    let obj = tblAllergyInfo(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                        list.append(obj)
                        }
                    
                }
            }
            try! setupRealm().write {
                let allData =  setupRealm().objects(tblAllergyInfo.self)
                if !allData.isEmpty {
                    setupRealm().delete(allData)
                }
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                completion(true)
            }
        }
    }
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblAllergyInfo]()

        rj.readJson(tableName: "others/tblAllergyInfo") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        if item.isEmpty == false{
                        let obj = tblAllergyInfo(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                            list.append(obj)
                            }
                        }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                    }
                }
                
            }
        }
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblAllergyInfo]()

            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
                        if item.isEmpty == false{
                        let obj = tblAllergyInfo(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                            list.append(obj)
                            }
                        }
                    try! setupRealm().write {
                        let allData =  setupRealm().objects(tblAllergyInfo.self)
                        if !allData.isEmpty {
                            setupRealm().delete(allData)
                        }
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }
                    completion(true)
                }
                
            }
        }
    
    static func showAllergy(completion: @escaping ([Item],Error?) -> Void) {
                
        var indexNoThing:Int!
        listAllergyInfo = (setupRealm().objects(tblAllergyInfo.self))
        arrayAllergyInfo = Array(listAllergyInfo)
        var item1:Item!
        var item2:Item!
        var modelAry = [Item]()
        for x in 0..<listAllergyInfo.count {
            if listAllergyInfo[x].discription == "لا شيء" {
            indexNoThing = x
            item1 = Item(id: listAllergyInfo[x].id, name: listAllergyInfo[x].discription)
            }
            modelAry.append(Item(id: listAllergyInfo[x].id, name: listAllergyInfo[x].discription))
        }
        if  modelAry[0].name != "لا شيء" {
            item2 = modelAry[0]
            modelAry[0] = item1
            modelAry[indexNoThing] = item2
        }

        completion(modelAry, nil)
    }
    
    
    
    static func getData() ->Array<String>{
        
        listAllergyInfo = (setupRealm().objects(tblAllergyInfo.self))
        arrayAllergyInfo = Array(listAllergyInfo)
        for x in 0..<listAllergyInfo.count {
            AllergyInfoDesc[listAllergyInfo[x].id]=listAllergyInfo[x].discription
        }
//        var array:[String]=[]
//        for name in AllergyInfoDesc.values {
//            array.append(name.description)
//        }
        let array:[String] = AllergyInfoDesc.values.map { (item) in
            
            return item.description
        }
        return array
    }
    
    static func getDataID() ->Array<String>{
        var arrayID:[String]=[]
        
        listAllergyInfo = (setupRealm().objects(tblAllergyInfo.self))
        arrayAllergyInfo = Array(listAllergyInfo)
        for x in 0..<listAllergyInfo.count {
            arrayID.append(listAllergyInfo[x].id)
        }
        
        return arrayID
    }
    
}
