//
//  tblCuisnesUSerItems.swift
//  Ta5sees
//
//  Created by Admin on 4/24/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblCuisnesUSerItems:Object {

    @objc dynamic var id = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var refCuisenenID = ""
    
    static var arrayCusisneType:[tblCuisnesUSerItems] = []
    static var listCusisneType:Results<tblCuisnesUSerItems>!

    override static func primaryKey() -> String? {
        return "id"
    }

    func IncrementaID() -> Int{
        return (setupRealm().objects(tblCuisnesUSerItems.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    static func getDataIDInt(id:String) ->Array<Int>{
              var arrayID:[Int]=[]

           listCusisneType = (setupRealm().objects(tblCuisnesUSerItems.self).filter("refUserID == %@",id))
                 arrayCusisneType = Array(listCusisneType)
                 for x in 0..<listCusisneType.count {
                   arrayID.append(Int(listCusisneType[x].refCuisenenID)!)
                 }
                 return arrayID
              
           
              }
    
    static func getDataSelectedProgressHistory() ->String{
          var arrayID:[String]=[]
        let list = Array(setupRealm().objects(tblCusisneType.self).filter("id IN %@",getUserInfoProgressHistory().refCuiseseID.split(separator: ",")))

          for x in list {

            arrayID.append(x.discription)
          }
        arrayID = arrayID.removeDuplicates()
          return arrayID.joined(separator:",")
          
          
      }
    static func getDataSelected() ->String{
        var arrayID:[String]=[]
        listCusisneType = (setupRealm().objects(tblCuisnesUSerItems.self).filter("refUserID == %@",getUserInfo().id))
        
        arrayCusisneType = Array(listCusisneType)
        for x in listCusisneType {
            //            let items:String? = setupRealm().objects(tblCusisneType.self).filter("id == %@",x.refCuisenenID).first?.discription
            guard let item = setupRealm().objects(tblCusisneType.self).filter("id == %@",x.refCuisenenID).first?.discription else {
                break
            }
            if !item.isEmpty {
                arrayID.append(item)
            }
        }
        arrayID = arrayID.removeDuplicates()
        return arrayID.joined(separator:",")
        
        
    }
     static func updateUserCuisnesID(id_User:String,str:String){
        let usersCuisnesUSerItems = setupRealm().objects(tblCuisnesUSerItems.self).filter("refUserID == %@",getUserInfo().id)
        if !usersCuisnesUSerItems.isEmpty {
            try! setupRealm().write{
                setupRealm().delete(usersCuisnesUSerItems)
            }
        }
          let idAlergy:[String]! = str.split(separator: ",").map(String.init)
          
          for item in idAlergy{
       
              let userAllergy:tblCuisnesUSerItems = tblCuisnesUSerItems()
              userAllergy.id = userAllergy.IncrementaID()
              userAllergy.refCuisenenID = String(item)
              userAllergy.refUserID = id_User
              try! setupRealm().write{
                  setupRealm().add(userAllergy, update: Realm.UpdatePolicy.modified)
              }
          
       }
      }
    static func updateUserCuisnes(id_User:String,str:String){
        
        let idAlergy:[String]! = str.split(separator: ",").map(String.init)
        
        for item in idAlergy{
         let i = setupRealm().objects(tblCusisneType.self).filter("discription contains '\(item)'").first
         if  setupRealm().objects(tblCuisnesUSerItems.self).filter("refUserID == %@ AND refCuisenenID == %@",id_User,i!.id).first == nil {
            let userAllergy:tblCuisnesUSerItems = tblCuisnesUSerItems()
            userAllergy.id = userAllergy.IncrementaID()
            userAllergy.refCuisenenID = String(i!.id)
            userAllergy.refUserID = id_User
            try! setupRealm().write{
                setupRealm().add(userAllergy, update: Realm.UpdatePolicy.modified)
            }
        }
     }
    }
}

