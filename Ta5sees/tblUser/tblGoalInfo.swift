//
//  tblGoalInfo.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift

class  tblGoalInfo:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
 
    func IncrementaID() -> Int{
        let realm = try! Realm()
        return (realm.objects(tblGoalInfo.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
