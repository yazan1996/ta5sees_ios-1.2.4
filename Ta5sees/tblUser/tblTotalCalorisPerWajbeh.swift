//
//  tblTotalCalorisPerWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 7/2/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON


class tblTotalCalorisPerWajbeh :Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var refWajbehInfo = 0
    @objc dynamic var totalColoreis = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblTotalCalorisPerWajbeh.self).max(ofProperty: "id") as Int? ?? 0) + 1
}
}
