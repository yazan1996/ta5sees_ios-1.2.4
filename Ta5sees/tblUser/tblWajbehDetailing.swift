//
//  tblWajbehDetailing.swift
//  Ta5sees
//
//  Created by Admin on 6/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblWajbehDetailing:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var quantity = 0
    @objc dynamic var refwajbeh = 0
    @objc dynamic var date = ""
    @objc dynamic var refWajbehInfoID = 0
    @objc dynamic var refwajbehCategoryID = 0

    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblWajbehDetailing.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    
//    func dede(refWajbehInfo:Int,responsEHendler:@escaping (Any?,Any?)->Void){
//        let realm = try! Realm()
//        
//        let obWajbehDetailing = realm.objects(tblWajbehDetailing.self).filter("refWajbehInfoID == %@ AND refwajbehCategoryID == %@",1,1).first
//        let obWajbeh = realm.objects(tblWajbeh.self).filter("id == %@",2).first
//        if obWajbeh != nil && obWajbehDetailing != nil {
//            print(obWajbehDetailing?.quantity as Any )
//            print(obWajbehDetailing?.refwajbeh as Any)
//            responsEHendler(obWajbehDetailing?.quantity,obWajbeh?.discription)
//        }else {
//            responsEHendler("","")
//        }
//    }
    func getLaunchWajbeh(refWajbehInfoID:Int,refCategoryID:Int,responsEHendler:@escaping (Any?,Any?)->Void){
        

        
//        let obWajbeh = realm.objects(tblWajbeh.self).filter("id == %@",refwajbeh).filter("ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ AND ANY reftblWajbehCategories.refCategoryID IN %@ ",[refWajbehInfoID],[refCategoryID]).first
//
//        let ob = realm.objects(tblHesa.self).filter("refHesaTypeID == %@",obWajbeh!.).first
//
//
//
//        print(obWajbeh!)
//
        
        
        
        
//        if obWajbeh  != nil {
//            let refH:Int = Int((obWajbeh!.refRenalHesaID)!)!
//            let string = tblWajbehDetailing.quantityServing(q:ob!.quantity,id:refH)
//            responsEHendler(obWajbeh!.discription,string)
//        }
        //        }else {
        //          responsEHendler("---","---")
        //        }
    }
    
    static func quantityServing(q:Int,id:Int)-> String{
        let obHesa = setupRealm().objects(tblHesa.self).filter("id == %@",id as Any).randomElement()
        switch q {
        case 1:
            return obHesa!.discription
        case 2:
            return obHesa!.hesa2
        case 3:
            return obHesa!.hesa3
        case 4:
            return obHesa!.hesa4
        case 5:
            return obHesa!.hesa5
        default:
            return obHesa!.hesa6
            
            
        }
    }
    
    static func UpdateID(){
        let ob = setupRealm().objects(tblWajbehDetailing.self).filter("refUserID == %@",-3)
        try! setupRealm().write {
            ob.setValue(ValuerefUserID, forKey : "refUserID")
            ValuerefUserID = -3
        }
    }
    
}
