//
//  tblExchangeSystemCategoryInfo.swift
//  Ta5sees
//
//  Created by Admin on 4/12/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblExchangeSystemCategoryInfo : Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var serve = 0
    @objc dynamic var proten = 0
    @objc dynamic var fat = 0
    @objc dynamic var carbo = 0
    @objc dynamic var energy = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int {
        return (setupRealm().objects(tblExchangeSystemCategoryInfo.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
}
