//
//  tblNeededMidicalTests.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class  tblNeededMidicalTests:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var discription = ""
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblNeededMidicalTests]()
        rj.readJson(tableName: "others/tblNeededMidicalTests") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblNeededMidicalTests(value: ["id" : item["id"].intValue, "discription": item["description"].string!])
                        list.append(obj)
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            }
        }
    }
}

