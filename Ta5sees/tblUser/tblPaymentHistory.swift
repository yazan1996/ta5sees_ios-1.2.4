//
//  tblPaymentHistory.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 20/05/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblPaymentHistory:Object {
    
    
    @objc dynamic var id:Int = 0
    @objc dynamic var amount = ""
    @objc dynamic var datetime = ""
    @objc dynamic var descriptionResponse = ""
    @objc dynamic var iosOrginalDateTime = ""
    @objc dynamic var iosOrginalDescription = ""
    @objc dynamic var iosOrginalTransactionID = ""
    @objc dynamic var isFirstTimeSubscription = ""
    @objc dynamic var status = ""
    @objc dynamic var subscriptionDuration = ""
    @objc dynamic var transactionID = ""
    @objc dynamic var product_id = ""
    @objc dynamic var refUserID = ""
    @objc dynamic var PurchaseToken = "null"//for android

    
    convenience init(amount:String,datetime:String,descriptionResponse:String,iosOrginalDateTime:String,iosOrginalDescription:String,iosOrginalTransactionID:String,isFirstTimeSubscription:String,status:String,subscriptionDuration:String,transactionID:String,product_id:String) {
        self.init()
        self.id = self.IncrementaID()
        self.amount = amount
        self.datetime = datetime
        self.descriptionResponse = descriptionResponse
        self.iosOrginalDateTime = iosOrginalDateTime
        self.iosOrginalDescription = iosOrginalDescription
        self.iosOrginalTransactionID = iosOrginalTransactionID
        self.isFirstTimeSubscription = isFirstTimeSubscription
        self.subscriptionDuration = subscriptionDuration
        self.transactionID = transactionID
        self.status = status
        self.PurchaseToken = "null"
        self.product_id = product_id
        self.refUserID = getUserInfo().id
        try! setupRealm().write{
            setupRealm().add(self)
        }
    }
    convenience init(obj:[[String:Any]]) {
        self.init()
        self.id = self.IncrementaID()
        self.amount = obj[0]["amount"] as! String
        self.datetime = obj[0]["datetime"] as! String
        self.descriptionResponse = obj[0]["description"] as! String
        self.iosOrginalDateTime = obj[0]["iosOrginalDateTime"] as! String
        self.iosOrginalDescription = obj[0]["iosOrginalDescription"] as! String
        self.iosOrginalTransactionID = obj[0]["iosOrginalTransactionID"] as! String
        self.isFirstTimeSubscription = obj[0]["isFirstTimeSubscription"] as! String
        self.subscriptionDuration = obj[0]["subscriptionDuration"] as! String
        self.transactionID = obj[0]["transactionID"] as! String
        self.status = obj[0]["status"] as! String
        self.product_id = obj[0]["productId"] as! String
        self.PurchaseToken = "null"
        self.refUserID = getUserInfo().id
        try! setupRealm().write{
            setupRealm().add(self)
        }
    }
   

    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblPaymentHistory.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    

}

