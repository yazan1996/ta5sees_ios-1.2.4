//
//  tblUserDailyFood.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblUserDailyFood:Object {
    
    
    @objc dynamic var id = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var refWajbehInfoID = 0
    @objc dynamic var refDailyWajbeh = 0
    @objc dynamic var totalCalBurned = 0
    @objc dynamic var carb = 0
    @objc dynamic var fat = 0
    @objc dynamic var protein = 0
    @objc dynamic var totalCal = 0

    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserDailyFood.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}


