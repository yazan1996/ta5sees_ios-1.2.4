//
//  tblUserWajbehEaten.swift
//  Ta5sees
//
//  Created by Admin on 1/27/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUserWajbehEaten :Object {
    
    @objc dynamic var id = 0
    @objc dynamic var userID = "0"
    @objc dynamic var refItemID = "0"
    @objc dynamic var date = "0"
    @objc dynamic var wajbehInfiItem = "0"
    @objc dynamic var kCal = "0"
    @objc dynamic var isWajbeh = "0" // 0 ?? 1-> senf
    @objc dynamic var ratio = "0"
    @objc dynamic var isProposal:String! // 1 ?? 0-> not Proposal
    // ratio
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
     func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserWajbehEaten.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func deletItem(itemid:Int,date:String,wajbehInfo:String,completion: @escaping (Bool) -> Void) {
           print("deletItem  \(setupRealm().objects(tblUserWajbehEaten.self))")
        let item = setupRealm().objects(tblUserWajbehEaten.self).filter("refItemID == %@ AND userID == %@ AND date == %@ AND wajbehInfiItem == %@ AND isProposal == %@",String(itemid),getDataFromSheardPreferanceString(key: "userID"),date,wajbehInfo,"1").first
//        print("deletItem \(item) \(realm.objects(tblUserWajbehEaten.self))")
        try! setupRealm().write {
            setupRealm().delete(item!)
            print("deletItem  \(setupRealm().objects(tblUserWajbehEaten.self))")
             setupRealm().refresh()
            completion(true)
        }
    }
    
    static func checkFoundItemIsNotProposal(id:Int,wajbehInfo:String,date:String,isProposal:String,completion: @escaping (Int,Bool) -> Void) {
        let item:tblUserWajbehEaten?  = setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND date == %@ AND wajbehInfiItem == %@ AND isProposal == %@ AND refItemID == %@",getDataFromSheardPreferanceString(key: "userID"),date,wajbehInfo,isProposal,String(id)).first
        
        if item != nil  {
            completion(item!.id,  true )
        }else{
            completion(-1,  false )
        }
    }
    
    static func checkFoundItem(id:Int,wajbehInfo:String,date:String,completion: @escaping (Bool) -> Void) {
          let item = setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND date == %@ AND wajbehInfiItem == %@ AND isProposal == %@ AND refItemID == %@",getDataFromSheardPreferanceString(key: "userID"),date,wajbehInfo,"1",String(id)).first
          
          completion( item != nil ? true : false)
      }
    
    func addItem(ob:tblUserWajbehEaten, completion: @escaping (Bool,Error?) -> Void) {
        
            if setupRealm().isInWriteTransaction == true {
                setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
                completion(true,nil)
            }else {
                try! setupRealm().write {
                    setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
                    setupRealm().refresh()
                    completion(true,nil)
                }
            }
            completion(false,nil)
        }

    
    static func getProteinFatCarbo(date:String,responsEHendler:@escaping ([Int],Error?)->Void){
        
        var protien = 0
        var fat = 0
        var carbo = 0
        let obWajbat = Array(setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND date == %@ AND isWajbeh == %@",getDataFromSheardPreferanceString(key: "userID"),date,"0"))
        
        let obSenf = Array(setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND date == %@ AND isWajbeh == %@",getDataFromSheardPreferanceString(key: "userID"),date,"1"))
        
        let asnaf =  obSenf.map({$0.refItemID})
        let wajbat =  obWajbat.map({$0.refItemID})
        print("wajbat id \(wajbat) snaf id \(asnaf)")
        
        for item_id in wajbat {
            let item:tblPackeg? = setupRealm().objects(tblPackeg.self).filter("id == %@",Int(item_id) ?? 0).first
            if item != NSNull(){
            protien+=Int(round(Double(item?.protein ?? "0.0")!))
            fat+=Int(round(Double(item?.fat ?? "0.0")!))
            carbo+=Int(round(Double(item?.carbohydrate ?? "0.0")!))
            }
        }
        for item_id in asnaf {
            let item:tblSenf? = setupRealm().objects(tblSenf.self).filter("id == %@",Int(item_id) ?? 0).first
            if item != NSNull(){
                protien+=Int(round(Double(item?.wajbehProteinTotal ?? "0.0")!))
            fat+=Int(round(Double(item?.wajbehFatTotal ?? "0.0")!))
            carbo+=Int(round(Double(item?.wajbehCarbTotal ?? "0.0")!))
            }
        }
            responsEHendler([protien,fat,carbo],"0" as? Error)

    }

         
       
    
    
    
    static func getItemsEaten(date:String,wajbehInfo:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND date == %@ AND wajbehInfiItem == %@",getDataFromSheardPreferanceString(key: "userID"),date,wajbehInfo), nil)
    }
}




/*
 //
 //  AddEditViewController.swift
 //  Ta5sees
 //
 //  Created by Admin on 5/29/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //


 protocol ShowNewWajbeh {
     func goToViewWajbeh(idWajbeh:String,flag:String)
 }

 import UIKit
 import SwiftIconFont

 @available(iOS 11.0, *)
 class AddEditCategoryController: UIViewController ,UISearchBarDelegate {
     
     
     var refWInfo:Int!
     var refWCAtegory:Int!
     var arrPassData:[Int] = []
     var obShowNewWajbeh:ShowNewWajbeh?
     var id_items:String!
     var date:String!
     var pg:protocolWajbehInfoTracker?
     var filteredDataPackges: [tblPackeg]!
     var filtered_all_Data =  [SearchItem]()
     var filtered_all_Data_copy =  [SearchItem]()

     let listPackges =  tblPackeg.searchModelArray()
     let listSenf =  tblSenf.searchModelArray()
     var frameCell:CGRect!
     var list_all_data =  [SearchItem]()

     let tableView = UITableView()
     var safeArea: UILayoutGuide!

     @IBOutlet weak var titlePage: UILabel!
     @IBAction func buDismiss(_ sender: Any) {
         dismiss(animated: true, completion: nil)
     }
     
     @IBAction func favourites(_ sender: Any) {
         self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")
     }
     @IBAction func addANDEdite(_ sender: Any) {
 //        self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
         showToast(message: "غير متاحة حاليا", view: view,place:0)
     }
     @IBAction func recentlyUsed(_ sender: Any) {
         self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
     }
     
     @IBOutlet weak var  searchBar: UISearchBar!
     
     
     @IBOutlet weak var lblTestSearch: UISearchBar!
     
     override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(true)
         if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") == "0" {
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) { [self] in
             self.showTutorial(flag: 1)
             self.perform(#selector(setAutText),with:0,afterDelay: 1)
         }

         }
     }
     
     func showTutorial(flag:Int) {
         var tutorials: [KJTutorial] = []
         if flag == 1 {
             // tut1
             let focusRect1 = searchBar.frame
             let message1 = "ابحث عن وجبة"
             let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
             let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect1.maxY + 30, width: 72, height: 72)
             let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
             let tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame)
 //            tut1.isArrowHidden = true
              tutorials = [tut1]
         }else if flag == 2 {
             let focusRect2 = tableView.convert(frameCell, to: self.view)
             let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
             let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect2.maxY + 12, width: 72, height: 72)
             let message2 = "اختر وجبة"
             let message1Center1 = CGPoint(x: view.bounds.width/2, y: focusRect2.maxY + 100)
             let tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message2, textPosition: message1Center1, icon: icon3, iconFrame: icon3Frame)
             self.tutorialVC.addEditWajbehLastStep = true
              tutorials = [tut2]
         }

      // let tutorials = [tut1, tut2, tut3, tut4]
         self.tutorialVC.tutorials = tutorials
         self.tutorialVC.showInViewController(self)
         self.tutorialVC.addEditWajbeh = self
     }
     
     lazy var tutorialVC: KJOverlayTutorialViewController = {
       return KJOverlayTutorialViewController()
     }()
     //    @IBAction func btndelet(_ sender: Any) {
     //        let ob = realm.objects(tblWajbehDetailing.self).filter("id == %@",1)
     //        try! realm.write {
     //            realm.delete(ob)
     //        }
     //    }
     //    @IBAction func edite(_ sender: Any) {
     //        self.performSegue(withIdentifier: "mainmenus", sender: [1,3])
     //
     //    }
     //    @IBOutlet weak var search: UISearchBar!
     
     override func viewDidLoad() {
         super.viewDidLoad()
         //        txtSearchOutlet.obShowNewWajbeh = self
         obShowNewWajbeh = self
         tableView.dataSource = self
         tableView.delegate = self
         
         searchBar.delegate = self
         setTilePage(id:id_items)
         
 //        let syncConc = DispatchQueue(label:"swiftlee.concurrent.queue1",attributes:.concurrent)
 //        syncConc.async {
             self.list_all_data.append(contentsOf: self.listPackges)
 //        }
 //        syncConc.async {
             self.list_all_data.append(contentsOf:self.listSenf)
 //        }
         
     }
     
     func textFieldDidChange(textField: UITextField) {
         //your code
     }
     
     
     @objc func setAutText(sender: Any){
         let word = "بانكيك"
         var index = sender as! Int

         let arr = Array(word)
         if index < arr.count {
         UIView.animate(withDuration: 0.4, animations: { [self] in
             searchBar.textField?.text!.append(arr[index])
             index = index+1
         }, completion: { [self]
             (value: Bool) in
             self.searchBar(self.searchBar, textDidChange: searchBar.text!)
             self.perform(#selector(setAutText),with:index,afterDelay: 0)
             
         })
         }else{
             showTutorial(flag:2)
         }
     }

     
     func setTilePage(id:String){
         
         switch id {
         case "1":
             titlePage.text! = "إضافة فطور"
             break
         case "2":
             titlePage.text! = "إضافة غداء"
             break
         case "3":
             titlePage.text! = "إضافة عشاء"
             break
         case "4":
             titlePage.text! = "إضافة تصبيرة ١"
             break
         case "5":
             titlePage.text! = "إضافة تصبيرة ٢"
             break
         default:
             print("not found !!")
         }
     }
     
     func toutorialViewWajbeh(){
         self.performSegue(withIdentifier: "DisplayCateItemsController", sender: "256")
     }
     func getUserInWithThe( userIn:String)->String {
         var polyUserIn = userIn;
         if userIn.count >= 3{
             if (userIn.hasPrefix("ال")) {
                 polyUserIn = String(userIn.dropFirst(2))
             }else{
                 polyUserIn = "ال\(userIn)";
             }
         }
             
         
         return polyUserIn;
     }
     func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
         let components = searchKey.components(separatedBy: "،")
         for item in components {

             if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
             return true
             }
         
            }

         return false
     }
     

     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
     }
     @objc func saveItemHistory(){
         if filtered_all_Data.isEmpty {
             tblHistoryNotFoundMeals.setMeals(name:lblTestSearch.text!)
         }
         
     }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         self.perform(#selector(saveItemHistory),with:nil,afterDelay: 2)
         if searchText.isEmpty == true {
             tableView.isHidden = true
             tableView.removeFromSuperview()
         }else{
             let syncConc = DispatchQueue(label:"swiftlee.concurrent.queue1",attributes:.concurrent)
 //            autoreleasepool {
             setupTableView()
             syncConc.sync(flags: .barrier) { [self] in
                     safeArea = view.layoutMarginsGuide
                     filtered_all_Data = []
                     self.filtered_all_Data  = searchText.isEmpty ? self.list_all_data : self.list_all_data.filter { (item: SearchItem) -> Bool in
                         
                         let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
                         
                         if range.contains(" ") {
                             
                             let input =  searchText.components(separatedBy: " ")
                             
                             let counter = 0
                             
 //                            for text in input {
 //                                if self.confirmSearchKey(userINp:text,searchKey:item.iteme_search,nameMel:item.iteme_name) {
 //                                    counter+=1
 //                                }
 //                            }
                             
                             if counter == input.count {
                                 return true
                             }else{
                                 for index in input{
                                     if self.confirmSearchKey(userINp:index,searchKey:item.iteme_search,nameMel:item.iteme_name){
                                         return true
                                         
                                     }
                                 }
                             }
                         }else{
                             if self.confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.iteme_search,nameMel:item.iteme_name) {
                                 return true
                                 
                             }
                         }
                         
                         return false
                     }
                     
                 }
 //            }
             syncConc.sync(flags: .barrier) {
                 self.sotrList(searchText: searchText, filtered_all_Data: self.filtered_all_Data)
             }
             //            DispatchQueue.global().sync {
             //
             //            }
         }
     }
     
  
     
     func matchesKey(constraint:String,searchKey:String)->Bool{
 //        if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
 //            return true
 //        }

         return false
     }
     
     
     
     func setupTableView() {
         view.addSubview(tableView)
         tableView.translatesAutoresizingMaskIntoConstraints = false
         tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
         tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
         tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
         tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
     }
     
     override func loadView() {
         super.loadView()
         
         if #available(iOS 13.0, *) {
         searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
         searchBar.searchTextField.textAlignment = .right
         }
         self.tableView.isHidden = true
         
     }
     
     
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         searchBar.resignFirstResponder()
         
     }
     
     
     
 }




 extension AddEditCategoryController:ShowNewWajbeh{
     func goToViewWajbeh(idWajbeh: String, flag: String) {
         
         if flag == "wajbeh"{
             self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
         }else {
             self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
         }
         //        txtSearchOutlet.text = nil
         
     }
     
     
     
 }


 extension AddEditCategoryController: UITableViewDelegate, UITableViewDataSource {
     
     
     //////////////////////////////////////////////////////////////////////////////
     // Table View related methods
     //////////////////////////////////////////////////////////////////////////////
     
     
     // MARK: TableView creation and updating
     
     // Create SearchTableview
     
     
     
     // MARK: TableViewDataSource methods
     public func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
     
     public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return filtered_all_Data.count
     }
     
     // MARK: TableViewDelegate methods
     
     //Adding rows in the tableview with the data from dataList
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
         filtered_all_Data_copy = []
         let filter = filtered_all_Data_copy.filter { (item: SearchItem) -> Bool in
             if item == filtered_all_Data[indexPath.row] {
                return false
             }
             return true
         }
             
         if filter.isEmpty {
             
         cell.nameSanf.text = filtered_all_Data[indexPath.row].getStringText()
         cell.caloris.text =  filtered_all_Data[indexPath.row].getStringTextcaloris()
         cell.catrgory.text =  filtered_all_Data[indexPath.row].getStringTextCate()
         
 //        cell.imageSenf.image = UIImage(named : "logo")
         SetImage(laImage:cell.imageSenf)
         filtered_all_Data_copy.append(filtered_all_Data[indexPath.row])
             if indexPath.row == 1 {
                 frameCell = cell.frame
             }
         }
         return cell
     }
     
     func SetImage(laImage:UIImageView){
         laImage.layer.borderWidth = 1
         laImage.layer.masksToBounds = false
         laImage.layer.borderColor = colorGray.cgColor
         laImage.layer.cornerRadius = laImage.frame.height/2
         laImage.clipsToBounds = true
         
     }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("selected row \(filtered_all_Data[indexPath.row].item_id)")
         obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filtered_all_Data[indexPath.row].item_id,flag:filtered_all_Data[indexPath.row].item_flag)
         tableView.isHidden = true
         
     }
     
     
     
 }



 extension AddEditCategoryController {
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "FavuriteRecentController" {
               if let dis=segue.destination as?  FavuriteRecentController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.id_category = id_items
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }else   if segue.identifier == "DisplayCateItemsController" {
               if let dis=segue.destination as?  DisplayCateItemsController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                         print("bool \(bool)")
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                           }
                           else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "DisplayCateItemsSenfController" {
               if let dis=segue.destination as?  DisplayCateItemsSenfController{
                   if  let data=sender as? String {
                       dis.idPackge = Int(data)
                       dis.id_items = id_items
                       dis.date = date
                       dis.pg = pg
                       if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                           dis.isFavrite = true
                       }else{
                           dis.isFavrite = false
                       }
                       
                       tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                         print("bool \(bool)")
                           dis.idItemsEaten = id
                           if bool == true {
                               dis.isEaten = true
                           }else{
                               dis.isEaten = false
                           }
                       })
                   }
               }
           }else if segue.identifier == "RecentUsedItemController" {
               if let dis=segue.destination as?  RecentUsedItemController{
                   if  sender != nil  {
                       dis.id_items = "-1"
                       dis.id_category = id_items
                       dis.date = date
                       dis.pg = pg
                   }
               }
           }
           
       }
       
 }

 extension AddEditCategoryController:filterListner {
     
     func sotrList(searchText:String,filtered_all_Data:[SearchItem]) {
         var sortedResultSenf = [SearchItem]()
         var lissPrioritys = [SearchItem]()

         for resultsSenf in self.filtered_all_Data {
             let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()

             if range.contains(" ") {

                 let constraintKeys =  searchText.components(separatedBy: " ")

                 var counter = 0
 //                let thread =  DispatchQueue.global(qos: .background)
 //                thread.sync {
                     for text in constraintKeys {
                         if self.confirmSearchKey(userINp:text,searchKey:resultsSenf.iteme_search,nameMel:resultsSenf.iteme_name) {
                             counter+=1
                         }

                     }
 //                }
                 if counter == constraintKeys.count {
                     sortedResultSenf.append(resultsSenf)
                 }else{

                     for index in constraintKeys{
                         if self.matchesKey(constraint: index, searchKey: resultsSenf.iteme_search) == true {
                             sortedResultSenf.append(resultsSenf)

                         }else {
                             lissPrioritys.append(resultsSenf)
                         }
                     }
                 }
             }else if self.matchesKey(constraint: searchText, searchKey: resultsSenf.iteme_search) == true {
                 sortedResultSenf.append(resultsSenf)


             }else{
                 lissPrioritys.append(resultsSenf)
             }


         }
         if lissPrioritys.count > 0 {
             sortedResultSenf.append(contentsOf: lissPrioritys)
         }
         self.filtered_all_Data = []
         self.filtered_all_Data = sortedResultSenf
 //        self.filtered_all_Data = self.filtered_all_Data.removeDuplicates()
         
         
 //            DispatchQueue.main.async {
                 self.tableView.reloadData()
                 self.tableView.isHidden = false
 //            }
             
     }
     
     
 }

 /*
  //
  //  TestSearch.swift
  //  Ta5sees
  //
  //  Created by Admin on 9/10/20.
  //  Copyright © 2020 Telecom enterprise. All rights reserved.
  //
  
  import UIKit
  
  class TestSearch: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return resultsList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
  cell.textLabel?.text = resultsList[indexPath.row].getStringText()
  return cell
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  var filteredDataPackges: [tblPackeg]!
  var resultsList : [SearchItem] = [SearchItem]()
  var resultsListLiss : [SearchItem] = [SearchItem]()
  
  let listPackges =  tblPackeg.searchModelArray()
  
  override func viewDidLoad() {
  super.viewDidLoad()
  tableView.dataSource = self
  searchBar.delegate = self
  filteredDataPackges = listPackges
  print("RRRRRRR\("DFSDASDBMNMF ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  }
  
  func getUserInWithThe( userIn:String)->String {
  var polyUserIn = userIn;
  if userIn.count >= 3{
  if (userIn.hasPrefix("ال")) {
  polyUserIn = String(userIn.dropFirst(2))
  }else{
  polyUserIn = "ال\(userIn)";
  }
  }
  return polyUserIn;
  }
  func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
  let components = searchKey.components(separatedBy: "،")
  for item in components {
  if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
  return true
  }
  }
  return false
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
  var lissPriority = [tblPackeg]()
  resultsList.removeAll()
  filteredDataPackges = searchText.isEmpty ? listPackges : listPackges.filter { (item: tblPackeg) -> Bool in
  
  ////            let whitespace = NSCharacterSet.whitespaces
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  //.self.rangeOfCharacter(from: whitespace)
  
  if range.contains(" ") {
  let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  for text in input {
  if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
  counter+=1
  }
  
  }
  if counter == input.count {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  for index in input.reversed(){
  if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }else{
  lissPriority.append(item)
  resultsListLiss.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  
  }
  }
  }
  }else{
  if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
  resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
  return true
  }
  }
  
  
  //
  
  
  
  return false
  
  
  }
  
  
  if lissPriority.count > 0 {
  filteredDataPackges.append(contentsOf: lissPriority)
  filteredDataPackges = filteredDataPackges.removeDuplicates()
  lissPriority.removeAll()
  }
  
  if resultsListLiss.count > 0 {
  resultsList.append(contentsOf: resultsListLiss)
  resultsList = resultsList.removeDuplicates()
  resultsListLiss.removeAll()
  }
  //        var sortedResult = [tblPackeg]();
  var sortedResult = [SearchItem]();
  if sortedResult.count > 0 {
  sortedResult.removeAll()
  }
  
  for results in resultsList {
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  
  if range.contains(" ") {
  let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  
  for text in constraintKeys {
  if confirmSearchKey(userINp:text,searchKey:results.iteme_search,nameMel:results.iteme_name) {
  counter+=1
  }
  
  }
  if counter == constraintKeys.count {
  sortedResult.append(results)
  //                    sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else{
  for index in constraintKeys.reversed(){
  if matchesKey(constraint: index, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //                              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else {
  //                            lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  }
  }
  }else if matchesKey(constraint: searchText, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  
  }else{
  //                lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  
  
  }
  if resultsListLiss.count > 0 {
  sortedResult.append(contentsOf: resultsListLiss)
  }
  //        if lissPriority.count > 0 {
  //            sortedResult.append(contentsOf: lissPriority)
  //        }
  resultsList.removeAll()
  resultsList = sortedResult
  
  tableView.reloadData()
  }
  
  
  
  func matchesKey(constraint:String,searchKey:String)->Bool{
  if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
  return true
  }
  
  return false
  }
  
  
  
  func binarySearch( arr:[String],firstIndex:Int, lastIndex:Int, key:String)->Int
  {
  if (lastIndex >= firstIndex) {
  let mid = firstIndex + (lastIndex - firstIndex) / 2;
  
  //arr[mid].compare(key, options: NSString.CompareOptions.caseInsensitive).rawValue)
  if arr[mid] == key {
  return mid;
  
  }
  
  if arr[mid] > key {
  return binarySearch(arr: arr, firstIndex: firstIndex, lastIndex: mid - 1, key: key)
  }
  
  return binarySearch(arr: arr, firstIndex: mid + 1, lastIndex: lastIndex, key: key)
  }
  
  
  return -1;
  }
  
  
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  self.searchBar.showsCancelButton = true
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
  searchBar.showsCancelButton = false
  searchBar.text = ""
  searchBar.resignFirstResponder()
  }
  }
  
  struct SenfPackage {
  var tilie = ""
  var name = ""
  var id = ""
  var isWajbeh = ""
  var searchKey = ""
  //    init(tilie:String,) {
  //        statements
  //    }
  }
  
  
  //   let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                print(input)
  //                for text in input {
  //                    if text != "" {
  //                        return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                    }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                          print(input)
  //                for text in input {
  //                    if text != "" {
  //                   return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  
  /*
  filteredDataPackges.removeAll()
  for item in listPackges {
  let components = item.searchKey!.components(separatedBy: "،")
  //            print(components.joined(separator: " "))
  
  let whitespace = NSCharacterSet.whitespaces
  let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  let input =  searchText.lowercased().components(separatedBy: " ")
  if range != nil {
  
  for text in input {
  if   components.joined(separator: "").lowercased().contains(text.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: text, isCaseSensitive: false) == true {
  filteredDataPackges.append(item)
  tableView.reloadData()
  
  }
  }
  }else{
  if   components.joined(separator: " ").lowercased().contains(searchText.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false) == true {
  filteredDataPackges.append(item)
  tableView.reloadData()
  
  }
  }
  }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  //            return false
  //
  //
  //        }
  
  }
  
  */
  
  
  
  
  */
 protocol filterListner {
     func sotrList(searchText:String,filtered_all_Data:[SearchItem])
 }
 extension  UISearchBar {

     var textField : UITextField? {
         if #available(iOS 13.0, *) {
             return self.searchTextField
         } else {
             // Fallback on earlier versions
             return value(forKey: "_searchField") as? UITextField
         }
     }
 }
 extension AddEditCategoryController: UISearchResultsUpdating {
   func updateSearchResults(for searchController: UISearchController) {
     if #available(iOS 13.0, *) {
         searchController.showsSearchResultsController = true
     } else {
         // Fallback on earlier versions
     }
   }
 }

 */
