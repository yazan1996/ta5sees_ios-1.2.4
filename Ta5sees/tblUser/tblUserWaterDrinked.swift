//
//  tblUserWaterDrinked.swift
//  Ta5sees
//
//  Created by Admin on 9/17/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblUserWaterDrinked:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var amountDrinked = 0
    @objc dynamic var amountDefualt = 0
    @objc dynamic var date = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func setWaterDrink(iduser:String,amountDrinked:Int,amountDefualt:Int){

        let isFound = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID"))
        if isFound.isEmpty {
        for x in  0..<amountDefualt {
        let ob = tblUserWaterDrinked()
        ob.id = ob.IncrementaID()
        ob.refUserID = iduser
        ob.amountDrinked = amountDrinked
        ob.amountDefualt = amountDefualt
        ob.date = getCurrentDate(flag:x)
        try! setupRealm().write {
            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
        }
        }
        }
        
    }
    static func removeForUser(){
        let arr = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),getDateOnly()).first
        try! setupRealm().write {
            arr!.setValue(0,forKey: "amountDrinked")
        }
    }
    
    static func checkFound()->Bool{
        if setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@",getDataFromSheardPreferanceString(key: "userID")).first != nil{
            return true
        }else{
            return false
        }
    }
    static func createNewRow(userID:String,date:String,drinke:Int,defualt:Int){
            let ob = tblUserWaterDrinked()
            ob.id = ob.IncrementaID()
            ob.refUserID = userID
            ob.amountDrinked = drinke
            ob.amountDefualt = defualt
            ob.date = date
            try! setupRealm().write {
                setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
            }
    }
    static func saveWaterDrink(x:Int,date:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        if ob.isEmpty {
            print("NIL")
            tblUserWaterDrinked.createNewRow(userID:getDataFromSheardPreferanceString(key: "userID"),date:date,drinke:x,defualt:7)
            return
        }
        try! setupRealm().write {
            ob.setValue(x,forKey: "amountDrinked")
            responsEHendler("success",nil)
        }
    }
    //AND date == %@
    static func saveWaterGoal(x:Int,date:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        let ob1 = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date).first
        if ob.isEmpty {
            print("NIL")
            tblUserWaterDrinked.createNewRow(userID:getDataFromSheardPreferanceString(key: "userID"),date:date,drinke:0,defualt:x)
            return
        }
        try! setupRealm().write {
            
            if ob1!.amountDrinked < x {
                ob.setValue(x,forKey: "amountDefualt")
                responsEHendler("success ",nil)

            }else{
                responsEHendler("falier",nil)

            }
        }
    }
    
    static func getWaterDrink(date:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserWaterDrinked.self).filter("refUserID == %@ AND date == %@",getDataFromSheardPreferanceString(key: "userID"),date)
        if !ob.isEmpty{
            responsEHendler(ob.last,nil)
        }else{
            responsEHendler(nil,nil)
        }
    }
    
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserWaterDrinked.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
