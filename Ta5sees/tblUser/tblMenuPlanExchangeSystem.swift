//
//  tblMenuPlanExchangeSystem.swift
//  Ta5sees
//
//  Created by Admin on 7/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class tblMenuPlanExchangeSystem :Object {
    @objc dynamic var id = 0
    @objc dynamic var refPlanMasterID = 0
    @objc dynamic var refGoalInfoID = 0 // delet
    @objc dynamic var milk = 0
    @objc dynamic var fruit = 0
    @objc dynamic var vegetables = 0
    @objc dynamic var isChild = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblMenuPlanExchangeSystem.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func getMenuPlanExchangeSystem(refID:Int,isChild:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblMenuPlanExchangeSystem.self).filter("refPlanMasterID == %@ AND isChild == %@",refID,isChild).first,nil)
    }
   
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblMenuPlanExchangeSystem]()
        rj.readJson(tableName: "plan/tblMenuPlanExchangeSystem") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                            let data:tblMenuPlanExchangeSystem = tblMenuPlanExchangeSystem()
                            data.id = item["id"].intValue
                            data.refPlanMasterID = item["refPlanMasterID"].intValue
                            data.isChild = item["isChild"].intValue
                            data.milk = item["milk"].intValue
                            data.fruit = item["fruit"].intValue
                            data.vegetables = item["vegetables"].intValue
                        list.append(data)
                               
                                
                            }
                        }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    completion(true)
                }
            }
            
        }
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblMenuPlanExchangeSystem]()
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
                            let data:tblMenuPlanExchangeSystem = tblMenuPlanExchangeSystem()
                            data.id = item["id"].intValue
                            data.refPlanMasterID = item["refPlanMasterID"].intValue
                            data.isChild = item["isChild"].intValue
                            data.milk = item["milk"].intValue
                            data.fruit = item["fruit"].intValue
                            data.vegetables = item["vegetables"].intValue
                        list.append(data)
                               
                                
                            }
                        }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                }
                completion(true)

            }
            
        }
    
}

