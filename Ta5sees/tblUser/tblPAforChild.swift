//
//  tblPAforChild.swift
//  Ta5sees
//
//  Created by Admin on 7/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class tblPAforChild :Object {
    @objc dynamic var id = 0
    @objc dynamic var refLayaqaConditionID = 0
    @objc dynamic var male = 0
    @objc dynamic var female = 0
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblPAforChild.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}
