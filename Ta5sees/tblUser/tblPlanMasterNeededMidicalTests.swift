//
//  tblPlanMaterNeededMidicalTests.swift
//  Ta5sees
//
//  Created by Admin on 9/3/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import  SwiftyJSON
class  tblPlanMasterNeededMidicalTests:Object {
    
    @objc dynamic var id = 0
    @objc dynamic var refPlanMasterID = 0
    @objc dynamic var refMidicalTestID = 0
//    @objc dynamic var neededMidicalTests:tblNeededMidicalTests?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
//    static func getDisc(id:Int)->String{
//
//        let NeededMidicalTests =  setupRealm().objects(tblPlanMasterNeededMidicalTests.self).filter("refPlanMasterID == %@",id).first!
//        return NeededMidicalTests.neededMidicalTests!.discription
//
//    }
  
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblPlanMasterNeededMidicalTests]()
        print("tblPlanMasterNeededMidicalTests")
        rj.readJson(tableName: "plan/tblPlanMasterNeededMidicalTests") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                    
                        let obj = tblPlanMasterNeededMidicalTests(value:["id" : item["id"].intValue, "refMidicalTestID": item["refMidicalTestID"].intValue,"refPlanMasterID": item["refPlanMasterID"].intValue])
                        list.append(obj)
                        }
                    }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
                
            }
        }
    }
}
