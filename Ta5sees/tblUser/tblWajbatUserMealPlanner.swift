//
//  wajbatUserMealPlanner.swift
//  Ta5sees
//
//  Created by Admin on 3/11/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbatUserMealPlanner:Object {
    
    
    @objc dynamic var id:Int = 0
    @objc dynamic var refUserID = ""
    @objc dynamic var packgeName = ""
    @objc dynamic var date = ""
    @objc dynamic var refPackgeID = ""
    @objc dynamic var packagesCalories = ""
    @objc dynamic var packagesWeights = ""
    @objc dynamic var refPackgeCateItme = ""
    @objc dynamic var refOrderID = 0

    convenience init(packgeName:String,date:String,refPackgeID:String,packagesCalories:String,packagesWeights:String,refPackgeCateItme:String,refOrderID:Int,countID:Int) {
        self.init()
        let oldItem = setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ AND date == %@ AND refPackgeCateItme == %@",getUserInfo().id,date,refPackgeCateItme)
        if oldItem.isEmpty {
        self.id = self.IncrementaID()//countID
        self.refUserID = getUserInfo().id
        self.date = date
        self.packgeName = packgeName
        self.refPackgeID = refPackgeID
        self.packagesCalories = packagesCalories
        self.packagesWeights = packagesWeights
        self.refPackgeCateItme = refPackgeCateItme
        self.refOrderID = refOrderID
        try! setupRealm().write{
            setupRealm().add(self, update: Realm.UpdatePolicy.modified)
        }
        }else{
            try! setupRealm().write{
            oldItem.setValue(date,forKey: "date")
            }
            try! setupRealm().write{
            oldItem.setValue(refPackgeID,forKey: "refPackgeID")
            }
            try! setupRealm().write{
            oldItem.setValue(packagesCalories,forKey: "packagesCalories")
            }
            try! setupRealm().write{
            oldItem.setValue(packagesWeights,forKey: "packagesWeights")
            }
            try! setupRealm().write{
            oldItem.setValue(refPackgeCateItme,forKey: "refPackgeCateItme")
            }
            try! setupRealm().write{
            oldItem.setValue(packgeName,forKey: "packgeName")
            }
        }
        
    }
    
    convenience init(packgeName:String,date:String,refPackgeID:String,packagesCalories:String,packagesWeights:String,refPackgeCateItme:String,refOrderID:Int) {
        self.init()
        self.id = self.IncrementaID()
        self.refUserID = getUserInfo().id
        self.date = date
        self.packgeName = packgeName
        self.refPackgeID = refPackgeID
        self.packagesCalories = packagesCalories
        self.packagesWeights = packagesWeights
        self.refPackgeCateItme = refPackgeCateItme
        self.refOrderID = refOrderID
        try! setupRealm().write{
            setupRealm().add(self)
        }
    }
    static func getAllData(date:String,responsEHendler:@escaping ([tblWajbatUserMealPlanner])->Void){
        
        responsEHendler(Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ AND date == %@",getUserInfo().id,date)))
    }
    static func getAllDataWajbehMealPlanner(date:String,refPackgeCateItme:String,responsEHendler:@escaping ([tblWajbatUserMealPlanner])->Void){
        print(getUserInfo().id,"date",date,"refPackgeCateItme",refPackgeCateItme)
        responsEHendler(Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ AND date == %@ AND refPackgeCateItme == %@",getUserInfo().id,date,refPackgeCateItme)))
    }
    
    static func getAllData1(responsEHendler:@escaping ([String])->Void){
        
        let arrDate = Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@",getUserInfo().id))
        
        let arr:[String] = arrDate.map { (item) in
            return item.refPackgeID
        }
        responsEHendler(arr)

    }
    
    static func getAllDate3(date:String,flag:Int,responsEHendler:@escaping ([String])->Void){
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        print("datedatedate",date)
        if flag == 1{
            var arrDate = Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ ",getUserInfo().id))
            
            arrDate = arrDate.filter({ item in
                if dateFormatter.date(from:date)!.timeIntervalSince1970*1000.0.rounded() < dateFormatter.date(from: item.date)!.timeIntervalSince1970*1000.0.rounded(){
                    return true
                }
                return false
            })
            
            print("arrDate",arrDate)
            let arr:[String] = arrDate.map { (item) in
                return item.date
            }.removeDuplicates()
            
            print(">",arr.sorted(by: {$0 < $1}).removeDuplicates())
            responsEHendler(arr.sorted(by: {$0 < $1}).removeDuplicates())
            
        }else{
            
            var arrDate = Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ ",getUserInfo().id))
            
            arrDate = arrDate.filter({ item in
                if dateFormatter.date(from:date)!.timeIntervalSince1970*1000.0.rounded() > dateFormatter.date(from: item.date)!.timeIntervalSince1970*1000.0.rounded(){
                    return true
                }
                return false
            })
            
            print("arrDate",arrDate)
            let arr:[String] = arrDate.map { (item) in
                return item.date
            }.removeDuplicates()
            
            
            print("<",arr.sorted(by: {$0 < $1}).removeDuplicates())
            responsEHendler(arr.sorted(by: {$0 < $1}).removeDuplicates())
            
        }
        
    }
    
    static func getAllDate(responsEHendler:@escaping ([String])->Void){
        
        let arrDate = Array(setupRealm().objects(tblWajbatUserMealPlanner.self).filter("refUserID == %@ ",getUserInfo().id))
        
        let arr:[String] = arrDate.map { (item) in
            return item.date
        }
        responsEHendler(arr.sorted(by: {$0 < $1}).removeDuplicates())
    }
    

    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblWajbatUserMealPlanner.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
//    static func generateWajbeh(date:String,refPackgeID:String,packagesCalories:String,packagesWeights:String,refPackgeCateItme:String,responsEHendler:@escaping (Any?,Error?)->Void){
//        var item = tblWajbatUserMealPlanner()
//        responsEHendler(nil,nil)
//    }

}
