//
//  tblUserDailyFoodItem.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class tblUserDailyFoodItem:Object {
    
    
    @objc dynamic var id = 0
    @objc dynamic var refWajbehD = 0
    @objc dynamic var refUserID = 0
    @objc dynamic var quantity = 0
    @objc dynamic var carb = 0
    @objc dynamic var fat = 0
    @objc dynamic var protein = 0
    @objc dynamic var totalCarb = 0
    @objc dynamic var refWajbehInfo = 0
    @objc dynamic var refWajbehCategoryID = 0
    @objc dynamic var Date = "0"
    @objc dynamic var flag = -1


    
    
    
    
    func dede(refWajbehInfo:Int,refWajbehCatID:Int,responsEHendler:@escaping (Any?,Any?)->Void){
        
        let obWajbehDetailing = setupRealm().objects(tblUserDailyFoodItem.self).filter("refWajbehInfo == %@ AND refWajbehCategoryID == %@",refWajbehInfo,refWajbehCatID).first
        if obWajbehDetailing != nil {
            let obWajbeh = setupRealm().objects(tblWajbeh.self).filter("id == %@",obWajbehDetailing!.refWajbehD).first
            if obWajbeh != nil {
                responsEHendler(obWajbehDetailing,obWajbeh)
            }else {
                responsEHendler(nil,nil)
            }
        }else{
            responsEHendler(nil,nil)
        }
      
    }
    
    func listWajbehInfo(refWCate:Int,responsEHendler:@escaping (Any?,Any?)->Void){
        
        let obWajbehDetailing = setupRealm().objects(tblUserDailyFoodItem.self).filter("refWajbehInfo == %@",refWCate)
            if obWajbehDetailing.count >= 0 {
                responsEHendler(obWajbehDetailing,nil)
            }else {
                responsEHendler(nil,nil)
            }
       
        
    }
    
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblUserDailyFoodItem.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
}

