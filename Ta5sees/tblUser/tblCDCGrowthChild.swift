//
//  tblCDCGrowhChild.swift
//  Ta5sees
//
//  Created by Admin on 8/26/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class tblCDCGrowthChild :Object {
    @objc dynamic var id = -1
    @objc dynamic var age = -1
    @objc dynamic var bmi:Float = -1.0
    @objc dynamic var value = -1
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblCDCGrowthChild.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    static func getBMIChild(age:Int,bmi:Float,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblCDCGrowthChild.self).filter("age <= %@ && bmi <= %@",age,bmi).last,nil)
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblCDCGrowthChild]()
        rj.readJson(tableName: "others/tblCDCGrowthChild") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                            let data:tblCDCGrowthChild = tblCDCGrowthChild()
                            data.id = item["id"].intValue
                            data.age = item["age"].intValue
                            data.bmi = item["BMI"].floatValue
                            data.value = item["Value"].intValue
                        list.append(data)
                                
                            }
                        }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    completion(true)
                    }
                
                
            }
        }
        
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblCDCGrowthChild]()
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
                            let data:tblCDCGrowthChild = tblCDCGrowthChild()
                            data.id = item["id"].intValue
                            data.age = item["age"].intValue
                            data.bmi = item["BMI"].floatValue
                            data.value = item["Value"].intValue
                        list.append(data)
                                
                            }
                        }
                try! setupRealm().write {
                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }
                completion(true)

                
            }
        }
        
    
}
