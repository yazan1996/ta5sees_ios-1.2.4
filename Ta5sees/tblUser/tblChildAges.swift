//
//  tblChildAges.swift
//  Ta5sees
//  Created by Admin on 9/26/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblChildAges:Object {
    
    @objc dynamic var id:Int = 0
    @objc dynamic var discription = ""
    @objc dynamic var startFrom = 0
    @objc dynamic var endTo = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    static func range1StartFrom(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.startFrom)
    }
    static func range1EndTo(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.endTo)
    }
    static func range2StartFrom(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.startFrom)
    }
    static func range2EndTo(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.endTo)
    }
    static func range3StartFrom(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.startFrom)
    }
    static func range3EndTo(id:Int) ->Float{
        let ob:tblChildAges = setupRealm().objects(tblChildAges.self).filter("id == %@",id).first!
        return Float(ob.endTo)
    }
    
  
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblChildAges]()
        rj.readJson(tableName: "others/tblchildAges") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
                           thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends{
                            let obj = tblChildAges(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"startFrom" : item["startFrom"].intValue,"endTo" : item["endTo"].intValue])
                        list.append(obj)
                            
                            }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                        }
                }
            }
        }
    }
    
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
          var list = [tblChildAges]()
              let thread =  DispatchQueue.global(qos: .userInitiated)
                             thread.async {
                  if let recommends = JSON(response).array {
                      for item in recommends{
                              let obj = tblChildAges(value: ["id" : item["id"].intValue, "discription": item["description"].string!,"startFrom" : item["startFrom"].intValue,"endTo" : item["endTo"].intValue])
                          list.append(obj)
                              
                              }
                      try! setupRealm().write {
                          setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                          }
                    completion(true)
                  }
              }
          }
      }

