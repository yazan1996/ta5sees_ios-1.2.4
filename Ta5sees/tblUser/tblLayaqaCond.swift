//
//  tblLayaqaCond.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
import Realm

class tblLayaqaCond:Object {
    static var arrayLayaqa:[tblLayaqaCond] = []
    static var listLayaqa:Results<tblLayaqaCond>!
    static var layaqDesc :[String:String]=[:]
    
    
    var arrayLayaqa1:[tblLayaqaCond] = []
    var listLayaqa1:Results<tblLayaqaCond>!
    var layaqDesc1 :[String:String]=[:]
    
    
    static var sheardLayaqaCond = tblLayaqaCond()
    
    @objc dynamic var id = 0
    @objc dynamic var discription = ""
    @objc dynamic var PAadult = 0.0
    @objc dynamic var PAchildmale =  0.0
    @objc dynamic var PAchildfemale =  0.0
    override static func primaryKey() -> String? {
        return "id"
    }
  
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblLayaqaCond]()
        rj.readJson(tableName: "others/tblLayaqaCond") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                            let obj = tblLayaqaCond(value:["id":item["id"].intValue,"PAchildmale" : item["PAchildmale"].double!,"PAadult" : item["PAadult"].double!,"PAchildfemale" : item["PAchildfemale"].double!, "discription": item["description"].string!])
                        list.append(obj)
                            }
                        }
                try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                    }
                
  
            }
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
        var list = [tblLayaqaCond]()
        
        let thread =  DispatchQueue.global(qos: .userInitiated)
        thread.async {
            if let recommends = JSON(response).array {
                for item in recommends {
                    let obj = tblLayaqaCond(value:["id":item["id"].intValue,"PAchildmale" : item["PAchildmale"].double!,"PAadult" : item["PAadult"].double!,"PAchildfemale" : item["PAchildfemale"].double!, "discription": item["description"].string!])
                    list.append(obj)
                }
            }
            try! setupRealm().write {
                setupRealm().add(list, update: Realm.UpdatePolicy.modified)
            }
            completion(true)

            
        }
    }
    
    static func showLayaqaCondition(completion: @escaping ([Item],Error?) -> Void) {
           
           
           listLayaqa = (setupRealm().objects(tblLayaqaCond.self))
           arrayLayaqa = Array(listLayaqa)
           var modelAry = [Item]()
           for x in 0..<listLayaqa.count {
               modelAry.append(Item(id: String(listLayaqa[x].id), name: listLayaqa[x].discription))
           }
           
           completion(modelAry, nil)
       }
       
    
    static func getData() ->Array<String>{
        var arrayDisc:[String]=[]
        
        listLayaqa = (setupRealm().objects(tblLayaqaCond.self))
        arrayLayaqa = Array(listLayaqa)
        for x in 0..<listLayaqa.count {
            arrayDisc.append(listLayaqa[x].discription)
        }
        
        
        return arrayDisc
    }
    
    
    func getData2(callBack:(Array<String>?) -> Void ) {
        var arrayDisc:[String]=[]
        
        listLayaqa1 = (setupRealm().objects(tblLayaqaCond.self))
        arrayLayaqa1 = Array(listLayaqa1)
        for x in 0..<listLayaqa1.count {
            arrayDisc.append(listLayaqa1[x].discription)
        }
        
        if arrayDisc.isEmpty {
            callBack(nil)
        }else{
            callBack(arrayDisc)
        }
        
    }
    
    static func getRate(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblLayaqaCond.self).filter("id == %@",refID).first,nil)
    }
    static func getItem(refID:Int)->String{
        return(setupRealm().objects(tblLayaqaCond.self).filter("id == %@",refID).first!.discription)
       }
    
    static func getDataID() ->Array<Int>{
        var arrayID:[Int]=[]
        
        listLayaqa = (setupRealm().objects(tblLayaqaCond.self))
        arrayLayaqa = Array(listLayaqa)
        for x in 0..<listLayaqa.count {
            arrayID.append(listLayaqa[x].id)
        }
        
        return arrayID
    }
    
    func getDataID2(callBack:(Array<Int>?) -> Void ) {
        var arrayID:[Int]=[]
        
        listLayaqa1 = (setupRealm().objects(tblLayaqaCond.self))
        arrayLayaqa1 = Array(listLayaqa1)
        for x in 0..<listLayaqa1.count {
            arrayID.append(listLayaqa1[x].id)
        }
        
        if arrayID.isEmpty {
            callBack(nil)
        }else{
            callBack(arrayID)
        }
    }
    
    
    //    func IncrementaID() -> Int{
    //        let realm = try! Realm()
    //        return (realm.objects(tblLayaqaCond.self).max(ofProperty: "id") as Int? ?? 0) + 1
    //    }
}


class tblLayaqaCondJSON:Codable{
    var id:Int
    var discription:String
    var PAadult:Double
    var PAchildmale:Double
    var PAchildfemale:Double
}
