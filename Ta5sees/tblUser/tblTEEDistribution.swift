//
//  tblTEEDistribution.swift
//  Ta5sees
//
//  Created by Admin on 7/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
class tblTEEDistribution :Object {
    @objc dynamic var id = 0
    @objc dynamic var refPlanMasterID = 0
    @objc dynamic var refGoalInfoID = 0 // delet
    @objc dynamic var Carbo = 0
    @objc dynamic var Protien = 0
    @objc dynamic var Fat = 0
    @objc dynamic var isChild = -1
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func IncrementaID() -> Int{
        return (setupRealm().objects(tblTEEDistribution.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    func getTeeDistrbution(refID:Int,isChild:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblTEEDistribution.self).filter("refPlanMasterID == %@ AND isChild == %@",refID,isChild).first,nil)
    }
  
    
    func readJson(completion: @escaping (Bool) -> Void){
        let rj = ReadJSON()
        var list = [tblTEEDistribution]()
        rj.readJson(tableName: "others/tblTEEDistribution") {(response, Error) in
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                            let data:tblTEEDistribution = tblTEEDistribution()
                            data.id = item["id"].intValue
                            data.refPlanMasterID = item["refPlanMasterID"].intValue
                            data.refGoalInfoID = item["refGoalInfoID"].intValue
                            data.Carbo = item["Carb"].intValue
                            data.Protien = item["Protein"].intValue
                            data.Fat = item["Fat"].intValue
                            data.isChild = item["isChild"].intValue
                        list.append(data)
                            }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        completion(true)
                    }
                }
                
            }
        }
    }
    
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        var list = [tblTEEDistribution]()
            let thread =  DispatchQueue.global(qos: .userInitiated)
            thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
                            let data:tblTEEDistribution = tblTEEDistribution()
                            data.id = item["id"].intValue
                            data.refPlanMasterID = item["refPlanMasterID"].intValue
                            data.refGoalInfoID = item["refGoalInfoID"].intValue
                            data.Carbo = item["Carb"].intValue
                            data.Protien = item["Protein"].intValue
                            data.Fat = item["Fat"].intValue
                            data.isChild = item["isChild"].intValue
                        list.append(data)
                            }
                    try! setupRealm().write {
                        setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                    }
                    completion(true)

                }
                
            }
        }
    }

