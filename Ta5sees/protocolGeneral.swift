//
//  protocolDate.swift
//  Ta5sees
//
//  Created by Admin on 5/16/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
protocol protocolGeneral {
    func setdate(m:String,d:Int,y:Int)
    func saveAmountWaterDrink(x:Int)
    func saveAmountWaterGoal(x:Int)
    func refreshWater(flag:Bool)
    func TrackerWater(date: String)
    func TrackerWeight(date: String)
    func TrackerTamreen()

       

}

protocol protocolWajbehInfoTracker {
    func refreshListItemTasbera1(itemTasbera2:tblUserWajbehEaten!)
    func refreshListItemTasbera2(itemTasbera2:tblUserWajbehEaten!)
    func refreshListItemBreakfast(itemTasbera2:tblUserWajbehEaten!)
    func refreshListItemDinner(itemTasbera2:tblUserWajbehEaten!)
    func refreshListItemLaunsh(itemTasbera2:tblUserWajbehEaten!)
    func refreshListItemTamreen(itemTamreen: tblUserTamreen!)
    func refrechData()
}
