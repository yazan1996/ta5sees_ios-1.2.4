//
//   PhoneAuthVerfy.swift
//  Ta5sees
//
//  Created by Admin on 3/17/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase


class PhoneAuthVerfy {
    
    
    static var sheard = PhoneAuthVerfy()
    
    
    func sentCodePinForgetPass(numberPhone:String,completion: @escaping (Int,String) -> Void){
//        showIndicator()
        PhoneAuthProvider.provider().verifyPhoneNumber(numberPhone, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                completion(0,"\(error!)")
            } else {
                let defaults = UserDefaults.standard
                defaults.set(verificationID, forKey: "authVID")
                completion(1,"")
            }
        }
   
        
    }
    
    
  
    func readCodPin(numberPhone:String,completion: @escaping (Int,String) -> Void){
        Auth.auth().signIn(with: getAuthPinCod(numberPhone:numberPhone)) { (user, error) in
            if error != nil {
                
                completion(0,"\(error!)")
                return
            }
            
            
            completion(1,"")
            
        }
        
        
       
    }
    
    func getAuthPinCod(numberPhone:String)->PhoneAuthCredential{
        let defaults = UserDefaults.standard
        return PhoneAuthProvider.provider().credential(withVerificationID:
            defaults.string(forKey: "authVID")!, verificationCode: numberPhone)
    }
}
