//
//  CalculationServing.swift
//  Ta5sees
//
//  Created by Admin on 4/13/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
class CalculationServing {
    var servmilk:Int?
    var servfruit:Int?
    var servVgtsbels:Int?
    var FBG = 0.0
    var fat:Int?
    var pro:Int?
    var carb:Int?
    var energy:Int?
    var dietType:String
    var otherspr = 0
    var othersfa = 0
    var othersen = 0
    var othercho = 0
    var reftotalenergy = 0
    var servothers = 0
    var totalPro = 0
    var totalCarbo = 0
    var totalFate = 0
    var milkprtein = 0
    var milkfat = 0
    var milkcho = 0
    var milkenergy = 0
    var others = 0.0
    var totalServing = 0
    var starch = 0
    var meat = 0
    var fats = 0
    var meatProtien = 0
    var meatFat = 0
    var meatenergy = 0
    var TDD1700 = 0.0
    var tblSenfSubCat:tblWajbehSenfCatSub = tblWajbehSenfCatSub()
    var exchangeSystem:tblExchangeSystem = tblExchangeSystem()

    init(malik:Int?
        , fruit:Int, vg:Int, fat:Int, pro:Int, carb:Int, energy:Int,dietType:String, FBG:Float) {
        self.servmilk = malik
        self.servfruit = fruit
        self.servVgtsbels = vg
        self.fat = fat
        self.pro = pro
        self.carb = carb
        self.energy = energy
        self.FBG = Double(FBG)
        self.dietType = dietType
            
        
        switch dietType {
        case "7":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
        case "3":
            milkprtein = 8 ;milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        case "2":
            milkprtein = 8 ;milkfat = 8 ;milkcho = 12 ;milkenergy = 160
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        case "1":
            milkprtein = 8;milkfat = 1 ;milkcho = 12; milkenergy = 100
            meatProtien = 7; meatFat = 2; meatenergy = 45
            break
        case "6":
            milkprtein = 8;milkfat = 1 ;milkcho = 12; milkenergy = 100
            meatProtien = 7; meatFat = 2; meatenergy = 45
            break
        case "8":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        case "19":
            milkprtein = 8 ;milkfat = 8 ;milkcho = 12 ;milkenergy = 160
            meatProtien = 7; meatFat = 8; meatenergy = 100
            break
        case "4":
            milkprtein = 8 ;milkfat = 8 ;milkcho = 12 ;milkenergy = 160
            meatProtien = 7; meatFat = 8; meatenergy = 100
            break
            
        case "33":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
            
        case "20":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        case "21":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        case "22":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
            
        case "11":
            milkprtein = 8; milkfat = 5 ;milkcho = 12 ;milkenergy = 120
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        default:
            milkprtein = 8 ;milkfat = 1 ;milkcho = 12 ;milkenergy = 100
            meatProtien = 7; meatFat = 5; meatenergy = 75
            break
        }
        
        
        
        proccess()
    }
    
    
    func proccess() {
        
        let milkpr = servmilk! * milkprtein
        let mailkfat = servmilk! * milkfat
        let malikchr = servmilk! * milkcho
        let maliken = servmilk! * milkenergy
        
    
        exchangeSystem.refMilkInfo = savetblExchangeSystemCategoryInfo(name:"milk",pr:milkpr,fat:mailkfat,charbo:malikchr,en:maliken,servfruit:servmilk!)
        
        print(milkpr)
        print(mailkfat)
        print(malikchr)
        print(maliken)
        
        let fruitpr = 0
        let fruitfa = 0
        let fruitch = servfruit! * 15
        let fruiten = servfruit! * 60
        
        exchangeSystem.refFruitInfo = savetblExchangeSystemCategoryInfo(name:"fruite",pr:fruitpr,fat:fruitfa,charbo:fruitch,en:fruiten,servfruit:servfruit!)
        
        
        print(fruitpr)
        print(fruitfa)
        print(fruitch)
        print(fruiten)
        
        let vgp = servVgtsbels! * 2
        let vgfa = 0
        let vgch = servVgtsbels! * 5
        let vgen = servVgtsbels! * 25
        
        
        exchangeSystem.refVegetablsInfo =  savetblExchangeSystemCategoryInfo(name:"vegitabels",pr:vgp,fat:vgfa,charbo:0,en:vgen,servfruit:servVgtsbels!)
        
        print(vgp)
        print(vgfa)
        print(vgch)
        print(vgen)
        
        let sub1  = carb! - (malikchr+vgch+fruitch)
        print(sub1)
        exchangeSystem.subTotal1 = sub1
        
        starch = Int(round(Double(sub1)/15.0))
        if starch > 11 {
            starch = 11
        }
        
        let starchpr = starch * 3
        let starchfa = starch * 1
        let starchch = starch * 15
        let starchen = starch * 80
        
        exchangeSystem.refStarchInfo =  savetblExchangeSystemCategoryInfo(name:"starch",pr:starchpr,fat:starchfa,charbo:starchch,en:starchen,servfruit:starch)
        
        totalCarbo = (malikchr+vgch+fruitch+starchch + othercho)
        print("totalCarbo : \(totalCarbo)")
        
        if dietType != "20" || dietType != "7" || dietType != "6" || dietType != "33" {
            others = Double(carb!) - Double(totalCarbo)
            servothers = Int(round(others / 15.0))
            print("servothers : \(servothers)")
            othercho = servothers * 15
            othersen = servothers * 80
            othersfa = servothers * 1
            otherspr = servothers * 3
            
            print("othercho : \(othercho)")
            totalCarbo = (malikchr+vgch+fruitch+starchch + othercho)
            
         
            exchangeSystem.refOthersInfo =  savetblExchangeSystemCategoryInfo(name:"others",pr:otherspr,fat:othersfa,charbo:othercho,en:othersen,servfruit:servothers)
        }
        
        
        let sub2 = pro! - (milkpr + fruitpr + vgp + starchpr + otherspr)
        print("\(milkpr) \(fruitpr) \(vgp) \(starchpr) \(otherspr) \(sub2)")
        exchangeSystem.subTotal2 = sub2
        
        meat = Int(round(Double(sub2) / 7.0))
        
        let meatpr = meat * meatProtien
        let  meatfa = meat * meatFat
        let  meaten = meat * meatenergy
        
       
        exchangeSystem.refMeatInfo =   savetblExchangeSystemCategoryInfo(name:"meat",pr:meatpr,fat:meatfa,charbo:0,en:meaten,servfruit:meat)
        
        totalPro = (milkpr + fruitpr + vgp + starchpr) + meatpr + otherspr
        print("\(milkpr) \(fruitpr) \(vgp) \(starchpr) \(meatpr) \(otherspr)")
        
        
        let sub3 = fat! - (mailkfat+fruitfa+vgfa+meatfa+starchfa+othersfa)
        exchangeSystem.subTotal3 = sub3
        print("sub3 : \(sub3) \(mailkfat) \(fruitfa) \(vgfa) \(meatfa) \(starchfa) \(fat!) \(meat)")
        fats = Int(round(Double(sub3) / 5.0))
        let fatsfa = fats * 5
        let fatsen = fats * 45
        
     
        
        exchangeSystem.refFatInfo =  savetblExchangeSystemCategoryInfo(name:"fat",pr:0,fat:fatsfa,charbo:0,en:fatsen,servfruit:fats)
        
        
        totalFate = (mailkfat+fruitfa+vgfa+meatfa+starchfa) + fatsfa + othersfa
        print("\(mailkfat) \(fruitfa) \(vgfa) \(meatfa) \(starchfa)")
        totalServing = (servmilk! + servfruit! + servVgtsbels!)
        totalServing += (starch + meat + fats + servothers)
        reftotalenergy = fruiten+vgen+maliken+othersen+meaten+starchen+fatsen
        
        print("\(servmilk!) \(servfruit!) \(servVgtsbels!) \(starch) \(meat) \(fats) \(servothers)")
        print("TP \(totalPro)")
        print("TEN \(reftotalenergy)")
        print(" TCA \(totalCarbo)")
        print(" TFa \(totalFate) ")
        print("p \(pro!) ")
        print(" f \(fat!)  ")
        print(" c \(carb!)  ")
        print(" en \(energy!) ")
       exchangeSystem.id =  exchangeSystem.IncrementaID()
        exchangeSystem.totalCHO = totalCarbo
        exchangeSystem.totalProtein = totalPro
       exchangeSystem.totalEnergy = reftotalenergy
        exchangeSystem.totalFate = totalFate
        
        exchangeSystem.totalCHOOrigin = carb!
        exchangeSystem.totalProteinOrigin = pro!
        exchangeSystem.totalEnergyOrigin = energy!
        exchangeSystem.totalFateOrigin = fat!
        exchangeSystem.id = exchangeSystem.IncrementaID()
        exchangeSystem.date = getDateOnly()
        
        saveExchangeSystem(ob:exchangeSystem)
        setDataInSheardPreferance(value: String("\(pro))"), key: "pro")
        setDataInSheardPreferance(value: String("\(fat))"), key: "fat")
        setDataInSheardPreferance(value:  String("\(carb))"), key: "carb")
        setDataInSheardPreferance(value: String("\(servmilk))"), key: "servmilk")
        setDataInSheardPreferance(value:  String("\(servfruit))"), key: "servfruit")
        setDataInSheardPreferance(value:  String("\(servVgtsbels))"), key: "servVgtsbels")
        setDataInSheardPreferance(value: String("\(starch))"), key: "starch")
        setDataInSheardPreferance(value:  String("\(meat))"), key: "meat")
        setDataInSheardPreferance(value: String("\(fats))"), key: "fats")
        setDataInSheardPreferance(value: String(servothers), key: "servothers")
        setDataInSheardPreferance(value: String(totalServing), key: "totalServing")
        print("tee1 \(reftotalenergy)")

        
        
        if getDataFromSheardPreferanceString(key: "dietType") == "20" {
            let SUMCC = Double((Double((servmilk! + servfruit! + starch)) * 15.0)) / Double(getDataFromSheardPreferanceString(key: "TDD500"))!
            let longInsulin = SUMCC + Double(getDataFromSheardPreferanceString(key: "insolin2"))!
            let rule = FBG - 120
            let s = 1700/longInsulin
            let s1 = s/rule
            let ss = SUMCC + s1
            TDD1700 = Double(getDataFromSheardPreferanceString(key: "insolin2"))!+ss
            setDataInSheardPreferance(value: String(TDD1700), key: "TDD1700")
            print("TDD1700\(TDD1700)")
        }
        
        sheardPreferanceWrite(fat: Float(totalFate),pro: Float(totalPro),carbo: Float(totalCarbo),energy: Float(reftotalenergy))
        
        distrbutionServing(totalServ:totalServing)
    }
    
    func distrbutionServing(totalServ:Int){
        
        let breakfastSer = Int( round((0.2) * Double(totalServ)))
        let snak1Ser = Int(round((0.1) * Double(totalServ)))
        let snak2Ser = Int(round((0.1) * Double(totalServ)))
        let launchSer = Int(round((0.4) * Double(totalServ)))
        let dinnerSer = Int(round((0.2) * Double(totalServ)))
        var breakfast = 0
        var snak1 = 0
        var snak2 = 0
        var launch = 0
        var dinner = 0
        var servMilkBreakfast = 0; var servMilkLaunch = 0; var servMilkDinner = 0;
        var servFriutBreakfast = 0; let servFriutLaunch = 0; var servFriutDinner = 0; var servFriutSnack1 = 0; var servFriutSnack2 = 0;
        var servVegetabeleBreakfast = 0; var servVegetabeleLaunch = 0; var servVegetabeleDinner = 0
        var servStarchBreakfast = 0; var servStarchLaunch = 0; var servStarchDinner = 0
        var servMeatBreakfast = 0; var servMeatLaunch = 0; var servMeatDinner = 0
        var servFatBreakfast = 0; var servFatLaunch = 0 ; var servFatDinner = 0;var servFatSnack1 = 0;var servFatSnack2 = 0
        let servOtherBreakfast = 0; let servOtherLaunch = 0; let servOtherDinner = 0;var servOtherSnack1 = 0;var servOtherSnack2 = 0
        
        if servmilk == 3 {
            breakfast += 1
            servMilkBreakfast += 1
            launch += 1
            servMilkLaunch += 1
            dinner += 1
            servMilkDinner += 1
            
        }else{
            breakfast += 1
            servMilkBreakfast += 1
            launch += 1
            servMilkLaunch += 1
        }
        
        if servfruit == 4 {
            breakfast += 1
            servFriutBreakfast += 1
            snak1 += 1
            servFriutSnack1 += 1
            snak2 += 1
            servFriutSnack2 += 1
            
            dinner += 1
            servFriutDinner += 1
            
        }else{
            breakfast += 1
            servFriutBreakfast += 1
            dinner += 1
            servFriutDinner += 1
            snak1 += 1
            servFriutSnack1 += 1
            
        }
        
        if servVgtsbels == 4 {
            breakfast += 1
            servVegetabeleBreakfast += 1
            launch += 2
            servVegetabeleLaunch += 2
            dinner += 1
            servVegetabeleDinner += 1
            
        }
        
        let bs = Int(round(0.25 * Double(starch)))
        breakfast += bs
        servStarchBreakfast += bs
        let bm = Int(round(0.25 * Double(meat)))
        breakfast += bm
        servMeatBreakfast += bm
        
        let ds = Int(round(0.25 * Double(starch)))
        dinner += ds
        servStarchDinner += ds
        
        let dm = Int(round(0.25 * Double(meat)))
        dinner += dm
        servMeatDinner += dm
        
        let ls = Int(round(0.5 * Double(starch)))
        launch += ls
        servStarchLaunch += ls
        
        let lm = Int(round(0.5 * Double(meat)))
        launch += lm
        servMeatLaunch += lm
        
        let so1 = Int(round(Double(servothers)/2.0))
        snak1 += so1
        servOtherSnack1 += so1
        
        let so2 = Int(round(Double(servothers)/2.0))
        snak2 += so2
        servOtherSnack2 += so2
        
        var copyFats = fats
        if breakfast < breakfastSer {
            if copyFats >= (breakfastSer-breakfast){
                copyFats = copyFats-(breakfastSer-breakfast)
                let num = breakfastSer-breakfast
                breakfast += num
                servFatBreakfast += num
                print("1")
                
                print(copyFats)
            }
        }
        
        if launch < launchSer {
            if copyFats >= (launchSer-launch){
                copyFats = copyFats-(launchSer-launch)
                let num = launchSer-launch
                launch += num
                servFatLaunch += num
                
                print(copyFats)
                print("11")
                
            }
        }
        if dinner < dinnerSer {
            if copyFats >= (dinnerSer-dinner){
                copyFats = copyFats-(dinnerSer-dinner)
                let num = dinnerSer-dinner
                dinner += num
                servFatDinner += num
                
                print(copyFats)
                print("22")
                
            }
        }
        
        
        if copyFats > 0 {
            print(Int(Double(copyFats)/2.0))
            snak1 += Int(Double(copyFats)/2.0)
            servFatSnack1 +=  Int(Double(copyFats)/2.0)
            snak2 += Int(Double(copyFats)/2.0)
            servFatSnack2 += Int(Double(copyFats)/2.0)
            
            
        }
        
        
        print("serving")
        print("totoser\(totalServ)")
        print("launch\(launchSer)  \(launch)  \(servMeatLaunch) \(servStarchLaunch) \(servVegetabeleLaunch) \(servMilkLaunch) \(servFriutLaunch) \(servOtherLaunch) \(servFatLaunch)")
        print("dinner\(dinnerSer)  \(dinner) \(servMeatDinner) \(servStarchDinner) \(servVegetabeleDinner) \(servMilkDinner) \(servFriutDinner) \(servOtherDinner) \(servFatDinner)")
        print("break\(breakfastSer)  \(breakfast) \(servMeatBreakfast) \(servStarchBreakfast) \(servVegetabeleBreakfast) \(servMilkBreakfast) \(servFriutBreakfast) \(servOtherBreakfast) \(servFatBreakfast)")
        print("s1\(snak1Ser)  \(snak1) \(servFatSnack1) \(servOtherSnack1)  \(servFriutSnack1)")
        print("s2\(snak2Ser)  \(snak2)  \(servFatSnack2) \(servOtherSnack2) \(servFriutSnack2)")
        DispatchQueue.main.async {
            
            
            
            
            
            /*
             let realm1 = try! Realm()
             let listQuery1 =  realm1.objects(tblWajbeh.self).filter("NONE  reftblWajbehRestrictedPlans.refPlanID IN %@ AND NONE reftblWajbehAllergy.refAllergyInfoID IN %@ AND ANY  reftblWajbehInfoItems.refWajbehInfoID IN %@ AND ANY reftblWajbehCuisenes.refCuiseneID IN %@ AND isContainOtherCHO == %@ AND isContainMilk == %@ AND isContainFriut ==  %@ AND isContainVegetables == %@",[5],[6,0,0],[2],[1,2,3],"1","1","1","1")
             
             
             if listQuery1.count > 0{
             for x in  0..<listQuery1.count  {
             let caloriesMedium = Int(listQuery1[x].wajbehTotalCaloriesMedium!)
             let caloriesHigh = Int(listQuery1[x].wajbehTotalCaloriesHigh!)
             let caloriesLow = Int(listQuery1[x].wajbehTotalCaloriesLow!)
             
             let persentig = 400*Int(listQuery1[x].weight!)!
             print(persentig/caloriesMedium!)
             print(persentig/caloriesHigh!)
             print(persentig/caloriesLow!)
             print("_____________")
             
             }
             }else{
             print(listQuery1)
             }
             
             */
            
            
            //versin 1
            //                let listQuery1 =  realm.objects(tblWajbeh.self).filter("NONE  reftblWajbehRestrictedPlans.refPlanID IN %@ AND NONE reftblWajbehAllergy.refAllergyInfoID IN %@ AND ANY  reftblWajbehInfoItems.refWajbehInfoID IN %@ AND ANY reftblWajbehCuisenes.refCuiseneID IN %@ AND isContainFat == %@ AND isContainStarch == %@ AND isContainMeat == %@ AND isContainOtherCHO == %@ AND isContainMilk == %@ AND isContainFriut ==  %@ AND isContainVegetables == %@ ",[5],[6,0,0],[2],[1,2,3],"1","1","1","1","1","1","1")
            //
            //
            //                    if listQuery1.count > 7 {
            //                        for _ in  0..<7 {
            //                            print(listQuery1.randomElement()!)
            //                        }
            //                    }else{
            //                        print(listQuery1.randomElement()!)
            //                    }
            //
            
            
            
            //                tblWajbeh.getWajbehDetailing(milk: servMilkBreakfast, refPlanID: refPlanMaster, refCategoryID: 1, refWajbehInfoID: 1, friut: servFriutBreakfast, vegetabels: servVegetabeleBreakfast, meat: servMeatBreakfast, fat: servMeatBreakfast, starch: servStarchBreakfast, others: servOtherBreakfast, totalserv: totalServ, responsEHendler: { (response, error) in
            //                    if response != nil {
            //                        print("BREAKFAST \(response!)")
            //    //                    self.setDataTblWajbehDetailing(quantity: Int(Double(copyFats)/2.0), wajbeh: response as! tblWajbeh)
            //                    }
            //
            //
            //                })
            
        }
        
        
        
    }
    func savetblExchangeSystemCategoryInfo(name:String,pr:Int,fat:Int,charbo:Int,en:Int,servfruit:Int)->Int{
        let obfruite=tblExchangeSystemCategoryInfo()
        obfruite.id =  obfruite.IncrementaID()
        obfruite.name = name
        obfruite.proten = pro!
        obfruite.fat = fat
        obfruite.carbo = charbo
        obfruite.energy = en
        obfruite.serve = servfruit
        saveData(ob:obfruite)
        return  obfruite.id
    }
    func saveData(ob:tblExchangeSystemCategoryInfo){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        try! setupRealm().write {
            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
        }
        
        
    }
    
    func saveExchangeSystem(ob:tblExchangeSystem){
        if setupRealm().isInWriteTransaction == true {
            setupRealm().cancelWrite()
        }
        idUserExchangeSystem = ob.id
        try! setupRealm().write {
            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
        }
        
    }
    //
    //    func setDataTblWajbehDetailing(quantity:Int,wajbehID:[Int]){
    //        let realm = try! Realm()
    //        let ob = tblWajbehDetailing()
    //        ob.id = ob.IncrementaID()
    //        ob.refUserID = -1
    //        ob.quantity = quantity
    //        ob.refwajbeh = 0
    //
    //        try! realm.write{
    //            realm.add(ob, update: Realm.UpdatePolicy.modified)
    //        }
    //    }
    //
    //    func setColerisWajbehInfo(energy:Int){
    //        let realm = try! Realm()
    //        let ob = tblTotalCalorisPerWajbeh()
    //        ob.id = ob.IncrementaID()
    //        ob.refUserID = -1
    //        ob.refWajbehInfo = 0
    //        ob.totalColoreis = Int(round(Double(energy) * 0.40))
    //
    //        try! realm.write{
    //            realm.add(ob, update: Realm.UpdatePolicy.modified)
    //        }
    //    }
    //
}
