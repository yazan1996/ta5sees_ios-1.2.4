//
//  MealDistrbutionVC.swift
//  Ta5sees
//
//  Created by Admin on 3/18/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class MealDistrbutionVC: UIViewController ,UITableViewDelegate,UITableViewDataSource ,UIViewControllerTransitioningDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return ModelMEalDistrbution.sharedManager.list.count
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            let cellBtnCell = tableView.dequeueReusableCell(withIdentifier: "BtnCell", for: indexPath) as! BtnCell
            cellBtnCell.btn.addTarget(self, action: #selector(clicked(_:)), for: .touchUpInside)
            
            return cellBtnCell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealsDistrbution", for: indexPath) as! MealDistbutionCell
        
        cell.index = indexPath
        cell.delegate = self
        cell.cell = cell
        cell.view = self
        cell.id_Notification = ModelMEalDistrbution.sharedManager.list[indexPath.row].id
        cell.timeFood = ModelMEalDistrbution.sharedManager.list[indexPath.row].time_food_date_str
        if  ModelMEalDistrbution.sharedManager.list[indexPath.row].selected { cell.setColor(color1:colorBasicApp,color2:.black,imageSmall:"clockWajbeh",imageBig: "clockmealDistrbution",isEnable:true,imgCateg: "breakfast")
        }else{
            cell.setColor(color1:colorGray,color2:colorGray,imageSmall:"clockDisable",imageBig: "clockWajbeh",isEnable:false,imgCateg: "breakFastDisable")
        }
        cell.setMainWajbeh(flag:ModelMEalDistrbution.sharedManager.list[indexPath.row].checkAsMain)
        
        cell.wajbehName.text = ModelMEalDistrbution.sharedManager.list[indexPath.row].name
        cell.setCustomTimr(time: ModelMEalDistrbution.sharedManager.list[indexPath.row].time_food)
        
        cell.imgWajbeh.image =  UIImage(named:ModelMEalDistrbution.sharedManager.list[indexPath.row].image)
        
        return cell
        
        
    }
    
    
    
    @objc func clicked(_ sender:UIButton){
        
        print("arr_mealDe",ActivationModel.sharedInstance.arr_mealDes)
 
        let obMD:tblMealDistribution = setupRealm().objects(tblMealDistribution.self).filter("Breakfast == %@ AND Snack1 == %@ AND Lunch == %@ AND Snack2 == %@ AND Dinner == %@ AND isBreakfastMain == %@ AND isLunchMain == %@ AND isDinnerMain == %@", NSNumber(value:ActivationModel.sharedInstance.arr_mealDes[0].selected),NSNumber(value:ActivationModel.sharedInstance.arr_mealDes[1].selected),NSNumber(value:ActivationModel.sharedInstance.arr_mealDes[2].selected),NSNumber(value:ActivationModel.sharedInstance.arr_mealDes[3].selected),NSNumber(value:ActivationModel.sharedInstance.arr_mealDes[4].selected),NSNumber(value:!ActivationModel.sharedInstance.arr_mealDes[0].checkAsMain),NSNumber(value:!ActivationModel.sharedInstance.arr_mealDes[2].checkAsMain),NSNumber(value:!ActivationModel.sharedInstance.arr_mealDes[4].checkAsMain)).first!
        ActivationModel.sharedInstance.TeeDistribution = obMD.id
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "DietTypeVCViewController") as! DietTypeVCViewController
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        createEvent(key: "23",date: getDateTime())
        self.present(detailView, animated: true, completion: nil)
        
        ActivationModel.sharedInstance.refMealDistrbution = ActivationModel.sharedInstance.arr_mealDes
        
        print("refMealDistrbution",Array(ActivationModel.sharedInstance.refMealDistrbution))
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
    @IBOutlet weak var tblview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // setupCellConfiguration()
        tblview.delegate=self
        tblview.dataSource=self
    }
    
    //    @IBOutlet weak var btnNextOutlet: UIBarButtonItem!
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    @IBAction func dissmes(_ sender: Any) {

        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNext(_ sender: Any) {}
    
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateTable(){
        DispatchQueue.main.async {
            self.tblview.reloadData()
        }
    }
    
    
    func alertDialog(mes:String){
        let alert = UIAlertController(title: "تنبيه", message: mes, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "إخفاء", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let viewHeader = Bundle.main.loadNibNamed("HeaderMealDistbutionCell", owner: self, options: nil)?.first as! HeaderMealDistbutionCell
            
            return viewHeader
        }
        return UIView()
    }
    
    func updateCell(cell:MealDistbutionCell){
        if let indexPath = tblview.indexPath(for: cell) {
            DispatchQueue.main.async {
                self.tblview.reloadRows(at: [indexPath], with: .fade)
                
            }
        }
    }
    
    
    
}


extension MealDistrbutionVC:updateList {
    func updateWajbeh(index:IndexPath,flag:Bool) {
        //        ModelMEalDistrbution.sharedManager.list[index.row].selected = flag
        for (indexItem,_) in  ModelMEalDistrbution.sharedManager.list.enumerated() {
            if index.row == indexItem {
                if ModelMEalDistrbution.sharedManager.list[indexItem].checkAsMain != false {
                    ModelMEalDistrbution.sharedManager.list[indexItem].selected = flag
                }
            }
        }
        
        updateTable()
    }
    
    
    func updateImageWajbeh(index:IndexPath,img:String) {
        ModelMEalDistrbution.sharedManager.list[index.row].image = img
        updateTable()
    }
    
    func updateMain(index:IndexPath,flag:Bool,cell:MealDistbutionCell) {
        ModelMEalDistrbution.sharedManager.list[index.row].checkAsMain = flag
        for (indexItem,_) in  ModelMEalDistrbution.sharedManager.list.enumerated() {
            if index.row == indexItem {
                ModelMEalDistrbution.sharedManager.list[indexItem].checkAsMain = flag
                if ModelMEalDistrbution.sharedManager.list[indexItem].selected == false {
                    ModelMEalDistrbution.sharedManager.list[indexItem].selected = true
                }
            }else{
                ModelMEalDistrbution.sharedManager.list[indexItem].checkAsMain = true
            }
        }
        updateTable()
    }
    
    
    func updateTime(index:IndexPath,str:String,cell:MealDistbutionCell,textDate:String) {
        print(textDate,"strstrstr",str)
        ModelMEalDistrbution.sharedManager.list[index.row].time_food = str
        ModelMEalDistrbution.sharedManager.list[index.row].time_food_date_str = textDate
        updateTable()
    }
}

extension MealDistrbutionVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
