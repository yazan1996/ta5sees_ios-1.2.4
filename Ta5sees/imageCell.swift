//
//  imageCell.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 23/08/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit

class imageCell: UITableViewCell {
    
    @IBOutlet weak var image_url: UIImageView!
    @IBOutlet weak var checkDielverd: UIImageView!

    var delegat:HomeChatViewController!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        //image_url.translatesAutoresizingMaskIntoConstraints = false
        // Initialization code

        let p = UIProgressView()
        p.progress = 1000.0
        image_url.insertSubview(p, at: 0)
        image_url.isUserInteractionEnabled = true
        image_url.adjustsImageSizeForAccessibilityContentSizeCategory = true
        image_url.translatesAutoresizingMaskIntoConstraints = false
        image_url.layer.borderWidth = 1
        image_url.layer.borderColor = colorGray.cgColor
        let tapLong = UILongPressGestureRecognizer(target: self, action: #selector((self.Tapped(_:))))
        tapLong.minimumPressDuration = 0.2
        tapLong.delaysTouchesEnded = true
        
        image_url.addGestureRecognizer(tapLong)
    
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func removeImageWindow(){
        for subview in delegat.view.subviews {
            if subview.tag == 500 {
                subview.removeFromSuperview()
            }
        }
    }

    @objc func Tapped(_ gestureRecognizer: UITapGestureRecognizer){
        let imageview = UIImageView(frame: CGRect(x:0,y:0,width:delegat.view.frame.width-50,height:delegat.view.frame.height-200))
        if gestureRecognizer.state == UIGestureRecognizer.State.ended  {
            removeImageWindow()
            return
        }
      
        imageview.image = image_url.image
        imageview.layer.borderWidth = 2
        imageview.layer.borderColor =  colorGray.cgColor
        imageview.tag = 500
        imageview.center = delegat.view.center
        delegat.view.addSubview(imageview)
    }
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

}

var imagCahce = NSCache<AnyObject,AnyObject>()

extension UIImageView {
    
    func downloadImage(from url: URL) {
        print("Download Started")
        
        DispatchQueue.global(qos: .userInteractive).async {
            if let i = imagCahce.object(forKey: url.absoluteString as NSString) as? UIImage {
                DispatchQueue.main.async { [self] in
                    self.image = i
                }
                return
            }
         
                // Create data from url (You can handle exeption with try-catch)
                guard let data = try? Data(contentsOf: url) else {
                    return
                }

                // Create image from data
                guard let image = UIImage(data: data) else {
                    return
                }

                // Perform on UI thread
            DispatchQueue.main.async { [self] in
                imagCahce.setObject(image, forKey: url.absoluteString as NSString)
                    self.image = image
                    /* Do some stuff with your imageView */
                }
            }
        
    }
 
}
extension UIImage {

    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }

    func resize(scaledToWidth desiredWidth: CGFloat) -> UIImage {
        let oldWidth = size.width
        let scaleFactor = desiredWidth / oldWidth

        let newHeight = size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        let newSize = CGSize(width: newWidth, height: newHeight)

        return resize(targetSize: newSize)
    }

    func resize(scaledToHeight desiredHeight: CGFloat) -> UIImage {
        let scaleFactor = desiredHeight / size.height
        let newWidth = size.width * scaleFactor
        let newSize = CGSize(width: newWidth, height: desiredHeight)

        return resize(targetSize: newSize)
    }

}
