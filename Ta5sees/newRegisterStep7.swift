    
import UIKit
import fluid_slider

class newRegisterStep7: UIViewController,UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var stackFemaleDiet: UIStackView!
   
    @IBOutlet weak var stackViewSeekBar: UIStackView!
    
    @IBOutlet weak var seekbarview: UIView!
   
    @IBOutlet weak var v8: viewColorsWithRaduis!
    @IBOutlet weak var v7: viewColorsWithRaduis!
    @IBOutlet weak var v6: viewColorsWithRaduis!
    @IBOutlet weak var v5: viewColorsWithRaduis!
    @IBOutlet weak var v4: viewColorsWithRaduis!
    @IBOutlet weak var v3: viewColorsWithRaduis!
    @IBOutlet weak var v2: viewColorsWithRaduis!

    @IBOutlet weak var v1: viewColorsWithRaduis!
    let slider = Slider()
    let w =  getDataFromSheardPreferanceFloat(key: "txtweight")
    var pregnant:UILabel!
    var Lactating:UILabel!
    var pregnantImg:UIImageView!
    var LactatingImg:UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var subview: UIView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setBackground()
        setupLable()
        setImage()
        HideSeekbar(flag: true)
        let firstView3 = stackFemaleDiet.arrangedSubviews[1]
        firstView3.isHidden = true
        if "1" == "1" {
            Lactating.isHidden = true
            pregnant.isHidden = true
            LactatingImg.isHidden = true
            pregnantImg.isHidden = true
            let firstView3 = stackFemaleDiet.arrangedSubviews[0]
            firstView3.isHidden = true
        }
        slider.attributedTextForFraction = { fraction in
            let formatter = NumberFormatter()
            formatter.maximumIntegerDigits = 3
            formatter.maximumFractionDigits = 0
            let string = formatter.string(from: (fraction * 100) as NSNumber) ?? ""
            
            setDataInSheardPreferance(value:string,key:"targetValue")
            
            return NSAttributedString(string: string)
        }
        slider.setMinimumLabelAttributedText(NSAttributedString(string: ""))
        slider.setMaximumLabelAttributedText(NSAttributedString(string: ""))
        slider.fraction = 0.5
        slider.shadowOffset = CGSize(width: 0, height: 10)
        slider.shadowBlur = 5
        slider.frame.size.height = 40
        slider.frame.size.width = seekbarview.frame.size.width
        
        slider.shadowColor = UIColor(white: 0, alpha: 0.1)
        slider.contentViewColor = UIColor(red: 156/255.0, green: 171/255.0, blue: 179/255.0, alpha: 0.5)
        slider.valueViewColor = UIColor(red: 141, green: 222, blue: 187)
        seekbarview.addSubview(slider)
        
    }
    func imgBackground (imgname:String) {
        let myLayer = CALayer()
        let myImage = UIImage(named: imgname)?.cgImage
        myLayer.frame = CGRect(x: -587, y: -384,  width: 1535, height: 681)
        myLayer.contents = myImage
        subview.layer.addSublayer(myLayer)
        subview.layer.insertSublayer(myLayer, at: 0)
    }
    func setBackground(){
        imgBackground(imgname: "registerbackground")
    }
    
    
    func setImage(){
        
        let Diabetes = UIImageView(frame: CGRect(x: 256, y: 363, width: 72.42, height: 72.43))
        Diabetes.image = UIImage(named: "diabetes")
        Diabetes.tag = 20
        btnNextOutlet.becomeFirstResponder()
        Diabetes.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        Diabetes.isUserInteractionEnabled = true
        self.subview.addSubview(Diabetes)
        
        let Pressure = UIImageView(frame: CGRect(x: 148.25, y: 363.58, width: 64.5, height: 68.42))
        Pressure.image = UIImage(named: "pressure")
        Pressure.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        Pressure.isUserInteractionEnabled = true
        Pressure.tag = 7
        btnNextOutlet.becomeFirstResponder()
        self.subview.addSubview(Pressure)
        
        let IBS = UIImageView(frame: CGRect(x: 44.06, y: 376, width: 47.61, height: 52.25))
        IBS.image = UIImage(named:"ibs")
        IBS.tag = 33
        IBS.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        IBS.isUserInteractionEnabled = true
        self.subview.addSubview(IBS)
        
        let Kidney = UIImageView(frame: CGRect(x: 259.39, y: 498.53, width: 65.21, height: 48.71))
        Kidney.image = UIImage(named: "renal")
        Kidney.tag = 9
        Kidney.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        Kidney.isUserInteractionEnabled = true
        self.subview.addSubview(Kidney)
        
        let cholestrol = UIImageView(frame: CGRect(x: 152, y: 496.68, width: 56 , height: 51.52))
        cholestrol.image = UIImage(named: "cholestrol")
        cholestrol.tag = 6
        cholestrol.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        cholestrol.isUserInteractionEnabled = true
        self.subview.addSubview(cholestrol)
        
        let gout = UIImageView(frame: CGRect(x: 36, y: 500.9, width: 63.44  , height: 39.44))
        gout.image = UIImage(named: "gout")
        gout.tag = 8
        btnNextOutlet.becomeFirstResponder()
        gout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        gout.isUserInteractionEnabled = true
        self.subview.addSubview(gout)
        
         pregnantImg = UIImageView(frame: CGRect(x: 203, y: 618, width: 65.91, height: 48.1))
        pregnantImg.image = UIImage(named: "pregnant")
        pregnantImg.tag = 4
        pregnantImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        pregnantImg.isUserInteractionEnabled = true
        self.subview.addSubview(pregnantImg)
        
         LactatingImg = UIImageView(frame: CGRect(x: 109, y: 608, width: 30.51, height: 64))
        LactatingImg.image = UIImage(named: "lactating")
        LactatingImg.tag = 19
        LactatingImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        LactatingImg.isUserInteractionEnabled = true
        self.subview.addSubview(LactatingImg)
        
        let image = UIImage(named: "imgback") as UIImage?
        let imageBack = UIButton(frame: CGRect(x: 320, y: 24, width: 32, height: 32))
        imageBack.setImage(image, for: .normal)
        imageBack.tag = 77
        imageBack.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(imageBack)
    }
    

    @IBOutlet weak var btnNextOutlet: ButtonColorRaduis!
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 6  && getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0 {
            selectDiet(v1:v5,v2:v2,v3:v3,
                       v4:v4,v5:v1,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:false)
        }else if tappedImageView.tag == 6  && getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0 {
            selectDiet(v1:v5,v2:v2,v3:v3,
                       v4:v4,v5:v1,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 4 {
            selectDiet(v1:v8,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v1)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 7 && getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0 {
            selectDiet(v1:v2,v2:v1,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:false)
        }else if tappedImageView.tag == 7 && getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0 {
            selectDiet(v1:v2,v2:v1,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 8  && getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0  {
            selectDiet(v1:v4,v2:v2,v3:v3,
                       v4:v1,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:false)
        }else if tappedImageView.tag == 8  && getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0  {
            selectDiet(v1:v4,v2:v2,v3:v3,
                       v4:v1,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 9 {
            
            selectDiet(v1:v6,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v1,
                       v7:v7,v8:v8)
            
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 19  {
            selectDiet(v1:v7,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v1,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 20   {
            selectDiet(v1:v3,v2:v2,v3:v1,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 3 {
            selectDiet(v1:v1,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }else if tappedImageView.tag == 33 && getDataFromSheardPreferanceFloat(key: "dateInYears") > 18.0  {
            selectDiet(v1:v1,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:false)
        }
        else if tappedImageView.tag == 33 && getDataFromSheardPreferanceFloat(key: "dateInYears") <= 18.0  {
            selectDiet(v1:v1,v2:v2,v3:v3,
                       v4:v4,v5:v5,v6:v6,
                       v7:v7,v8:v8)
            HideSeekbar(flag:true)
        }
        setDataInSheardPreferance(value:String(tappedImageView.tag),key:"dietType")
    }
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 77 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    func HideSeekbar(flag:Bool){
        let firstView3 = stackFemaleDiet.arrangedSubviews[1]
        firstView3.isHidden = flag
//        let firstView1 = stackViewSeekBar.arrangedSubviews[1]
//        firstView1.isHidden = flag
    }
 
    func setupLable(){
        let Diabetes = UILabel(frame: CGRect(x: 254, y: 432, width: 76, height: 16))
        Diabetes.textAlignment = .center
        Diabetes.text = "السكري"
        Diabetes.textColor = UIColor(red: 141, green: 222, blue: 187)
        Diabetes.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(Diabetes)
        
        
        let Pressure = UILabel(frame: CGRect(x: 142, y: 432, width: 76, height: 16))
        Pressure.textAlignment = .center
        Pressure.text = "الضغط"
        Pressure.textColor = UIColor(red: 141, green: 222, blue: 187)
        Pressure.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(Pressure)
        
        let IBS = UILabel(frame: CGRect(x: 30, y: 430, width: 88, height: 16))
        IBS.textAlignment = .center
        IBS.text = "القولون العصبي"
        IBS.textColor = UIColor(red: 141, green: 222, blue: 187)
        IBS.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(IBS)
        
        
        let Kidney = UILabel(frame: CGRect(x: 254, y: 551, width: 76, height: 16))
        Kidney.textAlignment = .center
        Kidney.text = "الكلى"
        Kidney.textColor = UIColor(red: 141, green: 222, blue: 187)
        Kidney.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(Kidney)
        
        
        let cholestrol = UILabel(frame: CGRect(x: 142, y: 551, width: 76, height: 16))
        cholestrol.textAlignment = .center
        cholestrol.text = "كولسترول"
        cholestrol.textColor = UIColor(red: 141, green: 222, blue: 187)
        cholestrol.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(cholestrol)
        
       let gout = UILabel(frame: CGRect(x: 30, y: 551, width: 76, height: 16))
        gout.textAlignment = .center
        gout.text = "نقرس"
        gout.textColor = UIColor(red: 141, green: 222, blue: 187)
        gout.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(gout)
        
        pregnant = UILabel(frame: CGRect(x: 198, y: 669, width: 76, height: 16))
        pregnant.textAlignment = .center
        pregnant.text = "حامل"
        pregnant.textColor = UIColor(red: 141, green: 222, blue: 187)
        pregnant.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(pregnant)
        
        Lactating = UILabel(frame: CGRect(x: 86, y: 672, width: 76, height: 16))
        Lactating.textAlignment = .center
        Lactating.text = "مرضع"
        Lactating.textColor = UIColor(red: 141, green: 222, blue: 187)
        Lactating.font =  UIFont(name: "GE Dinar One", size: 14.0)!
        self.subview.addSubview(Lactating)
        
        
    }
    func selectDiet(v1:viewColorsWithRaduis,v2:viewColorsWithRaduis,v3:viewColorsWithRaduis,
                    v4:viewColorsWithRaduis,v5:viewColorsWithRaduis,v6:viewColorsWithRaduis,
                    v7:viewColorsWithRaduis,v8:viewColorsWithRaduis){
        v1.backgroundColor = UIColor(red: 156, green: 171, blue: 179)
        v3.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v2.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v4.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v5.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v6.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v7.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        v8.backgroundColor = UIColor(red: 255,green: 255,blue: 255)
        
    }
    
    
    func BMIAdult(){
        if getDataFromSheardPreferanceFloat(key: "bmi") <= 18.5 && getDataFromSheardPreferanceString(key: "subDiet") != "gain"{
             showAlert(str:"نقترح عليك ان تقوم بزيادة وزنك")
        }else if getDataFromSheardPreferanceFloat(key: "bmi") > 18.5 &&  getDataFromSheardPreferanceFloat(key: "BMI") <= 24.9  && getDataFromSheardPreferanceString(key: "subDiet") != "maintain"{
             showAlert(str:"نقترح عليك ان تقوم بالمحافظة على وزنك")
        }else if getDataFromSheardPreferanceFloat(key: "bmi") >= 25.0 &&  getDataFromSheardPreferanceFloat(key: "BMI") <= 29.9 && getDataFromSheardPreferanceString(key: "subDiet") != "loss"{
              showAlert(str:"نقترح عليك ان تقوم بتخسيس وزنك")
        }else if getDataFromSheardPreferanceFloat(key: "bmi") >= 30.0 &&  getDataFromSheardPreferanceFloat(key: "BMI") <= 39.9 && getDataFromSheardPreferanceString(key: "subDiet") != "loss"{
               showAlert(str:"نقترح عليك ان تقوم بتخسيس وزنك")
        }else if getDataFromSheardPreferanceFloat(key: "bmi") <= 40.0 && getDataFromSheardPreferanceString(key: "subDiet") != "loss"{
               showAlert(str:"نقترح عليك ان تقوم بتخسيس وزنك")
        }
    }
    
    func showAlert(str:String){
          let alert = UIAlertController(title: "نصيحة", message: "", preferredStyle: .alert)
             
                   
                   let done = UIAlertAction(title: "تخطي", style: .default, handler: { action in
                     if getDataFromSheardPreferanceString(key: "dietType") == "20"{
                        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "regesterStep10") as! regesterStep10
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "step13", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                               
                        }else {
                        if getDataFromSheardPreferanceString(key: "dietType") == "7"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Heypertention") as! Heypertention
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "Heypertention", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }else if getDataFromSheardPreferanceString(key: "dietType") == "6"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Cholestrole") as! Cholestrole
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "Cholestrole", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }else if getDataFromSheardPreferanceString(key: "dietType") == "8"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "IBS") as! IBS
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "IBS", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }else if getDataFromSheardPreferanceString(key: "dietType") == "33"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Gout") as! Gout
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "Gout", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }else if getDataFromSheardPreferanceString(key: "dietType") == "19"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Lactation") as! Lactation
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "Lactation", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }else if getDataFromSheardPreferanceString(key: "dietType") == "4"{
                            let detailView = self.storyboard!.instantiateViewController(withIdentifier: "Pregnant") as! Pregnant
                               detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                               detailView.transitioningDelegate = self
                               self.present(detailView, animated: true, completion: nil)
                               self.performSegue(withIdentifier: "Pregnant", sender: nil)
                               self.view.snapshotView(afterScreenUpdates: true)
                        }
                           }
                   })
                   done.setValue(UIColor.black, forKey: "titleTextColor")
                   
                   alert.addAction(done)
                   
                   self.present(alert, animated: true)
       }
    @IBAction func btnNext(_ sender: Any) {
 
   
        let num = Float(getDataFromSheardPreferanceString(key: "targetValue"))!
        if num > w {
            setDataInSheardPreferance(value:"gain",key:"subDiet")
        }else  if num < w {
            setDataInSheardPreferance(value:"loss",key:"subDiet")
        }else{
            setDataInSheardPreferance(value:"maintain",key:"subDiet")
        }
        BMIAdult()
      
    }

}
