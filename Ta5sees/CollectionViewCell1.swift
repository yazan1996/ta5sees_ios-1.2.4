//
//  CollectionViewCell1.swift
//  Ta5sees
//
//  Created by Admin on 10/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit


protocol ButtonClicable {
    func tap(idW:Int)
}


class changeWajbehCell: UICollectionViewCell {
    
    var delegate:ButtonClicable?
    var index:IndexPath?
    
    @IBOutlet weak var btnChangeWajbehBreakFast: UIButton!
    @IBOutlet weak var imagBreakFast: UIImageView!
    @IBOutlet weak var txtdateBreakFast: UILabel!
    @IBOutlet weak var btnChangeDinner: UIButton!
    @IBOutlet weak var imgDiner: UIImageView!
    @IBOutlet weak var txtDateDinner: UILabel!
    @IBOutlet weak var btnChangeTasbera2: UIButton!
    @IBOutlet weak var imgTasbera: UIImageView!
    @IBOutlet weak var txtDateTassbera2: UILabel!
    @IBOutlet weak var btnChangeWajbehLaunsh: UIButton!
    @IBOutlet weak var imgWajbehLaunsh: UIImageView!
    @IBOutlet weak var txtDateLaunsh: UILabel!
    @IBOutlet weak var btnChangeWajbeh: UIButton!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var imageWajbeh: UIImageView!

    @IBAction func btnChangeDinner(_ sender: Any) {
        delegate?.tap(idW: index!.row)
    }
    
    @IBAction func btnChangeBreakFast(_ sender: Any) {
        delegate?.tap(idW: index!.row)
    }
    @IBAction func btnChangeTasbera2(_ sender: Any) {
        delegate?.tap(idW: index!.row)
    }
    
    @IBAction func btnChangeLaunsh(_ sender: Any) {
        delegate?.tap(idW: index!.row)
    }
    
    @IBAction func btnChangTasber1(_ sender: Any) {
        delegate?.tap(idW: index!.row)
    }
    
    
    func SetImage(url:String,laImage:UIImageView){
        laImage.layer.borderWidth = 1
        laImage.layer.masksToBounds = false
        laImage.layer.borderColor = UIColor.black.cgColor
        laImage.layer.cornerRadius = laImage.frame.height/2
        laImage.clipsToBounds = true
        //paralel process
        DispatchQueue.global().async {
            
            
            do{
                let AppURL=URL(string:url)
                let data = try Data(contentsOf: AppURL!)
                
                // access to UI
                DispatchQueue.main.sync {
                    laImage.image = UIImage(data: data)
                }
            }
            catch {
                print("cannot load from server")
            }
            
        }
    }
    
    
    
}
