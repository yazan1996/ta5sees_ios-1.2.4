//
//  RecentUsedController.swift
//  Ta5sees
//
//  Created by Admin on 9/12/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class RecentUsedController: UIViewController {
    var obShowNewWajbeh:ShowNewWajbeh?
    var id_items:String!
      var date:String!
      var pg:protocolWajbehInfoTracker?
   
    @IBAction func dismes(_ sender: Any) {
          dismiss(animated: true, completion: nil)
    }
 
    @IBOutlet weak var tableview: UITableView!
   
    @IBAction func removeall(_ sender: Any) {
        tblItemsFavurite.deleteAllItem { (bool) in
                   print(bool)
                   self.filterItem.removeAll()
            showToast(message: "تم حذف الجميع", view: self.view,place:0)
                   self.tableview.reloadData()
          
               }
    }
    var filterItem = tblItemsFavurite.getAllDataFavurite()
    override func viewDidLoad() {
        super.viewDidLoad()

        obShowNewWajbeh = self
        tableview.delegate = self
        tableview.dataSource = self
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = UITableView.automaticDimension

    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "DisplayCateItemsController" {
            if let dis=segue.destination as?  DisplayCateItemsController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                }
            }
        }else if segue.identifier == "DisplayCateItemsSenfController" {
            if let dis=segue.destination as?  DisplayCateItemsSenfController{
                if  let data=sender as? String {
                    dis.idPackge = Int(data)
                    dis.id_items = id_items
                    dis.date = date
                    dis.pg = pg
                    if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                        dis.isFavrite = true
                    }else{
                        dis.isFavrite = false
                    }
                }
            }
        }
        
    }
    

}
extension RecentUsedController:ShowNewWajbeh{
    func goToViewWajbeh(idWajbeh: String, flag: String) {
        
        if flag == "wajbeh" || flag == "0" {
            self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
        }else {
            self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
        }
        //        txtSearchOutlet.text = nil
        
    }
    
    
    
}

extension RecentUsedController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterItem.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RecentUsedCell = tableView.dequeueReusableCell(withIdentifier: "RecentUsedCell", for: indexPath) as! RecentUsedCell


        let item = tblItemsFavurite.getPackageDataFavurite(id: filterItem[indexPath.row].refItem)
        cell.name.text = item.name
        cell.category.text =  "\(item.protein!)"
        cell.caloris.text =  "\(item.calories!) سعرة حرارية"

        cell.logoImage.image = UIImage(named : "logo")
       
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(filterItem[indexPath.row].refItem)")
//                self.text = filterItem[indexPath.row].iteme_name
                obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filterItem[indexPath.row].refItem,flag:filterItem[indexPath.row].flag)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}
