//
//  SourcesAppController.swift
//  Ta5sees
//
//  Created by Admin on 12/20/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD

 
class SourcesAppController: UIViewController {

    @IBOutlet weak var text1: UITextView!
    @IBOutlet weak var text2: UITextView!
    lazy var  obAPiSubscribeUser = APiSubscribeUser(with: self)
    var flagNotfy = false
    override func viewDidLoad() {
        super.viewDidLoad()
        UITextView.appearance().linkTextAttributes = [ .foregroundColor: UIColor.blue ]
        text1.font = UIFont(name: "GE Dinar One", size: 17)
        text2.text = "Advanced Nutrition and Human Metabolism, Sixth Edition\nSareen S. Gropper (Auburn University) and Jack L. Smith (University of Delaware)\n\nKrause's Food and Nutrition Care Process, 14'th Edition\nL.Kathleen Mahan and  Janice L. Raymond\n\nUS Department of Agriculture\nhttps://www.usda.gov\n\nBritish Nutrition Foundation\nhttps://www.nutrition.org.uk"
        createHiperLinkLable(text:"https://www.usda.gov" ,name: "https://www.usda.gov")
        createHiperLinkLable(text: "https://www.nutrition.org.uk",name: "https://www.nutrition.org.uk")
        
    }
    
    @IBAction func btnNext2(_ sender: Any) {
        sntDataToFirebase()
    }
    @IBAction func btnNext(_ sender: Any) {
        sntDataToFirebase()
        }
    @IBAction func dismess(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func sntDataToFirebase(){
        self.obAPiSubscribeUser.subscribeUser(mealDitr:11,flag: 0) { [self] (bool) in
            if bool {
                setUpNotification(flagNotfy:flagNotfy)
                setDataInSheardPreferance(value: getDateOnly(), key: "lastUpdataSyncData")
                setDataInSheardPreferance(value: "0", key: "finishRegistration")
                //
                //                let appDelegate = UIApplication.shared.delegate as!AppDelegate
                //                appDelegate.window = UIWindow(frame:UIScreen.main.bounds)
                //                let mainStoryboard =  UIStoryboard(name: "Main", bundle: nil)
                //                let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain") as! UITabBarController
                //                tabbarVC.selectedViewController = tabbarVC.viewControllers![0]
                //
                //                appDelegate.window?.rootViewController = tabbarVC
                //                appDelegate.window?.makeKeyAndVisible()
                
                self.performSegue(withIdentifier: "SourcesAppControllerToHome", sender: self) //sub1
            }else{
                alert(mes: "لم يتم تسجيل حسابك ،حاول لاحقا", selfUI: self)
            }
        }
    }
    func createHiperLinkLable(text:String,name:String){
        text2.backgroundColor = UIColor.clear
//        text2.text = name
           
        text2.dataDetectorTypes = .link
        text2.isEditable = false
        text2.isSelectable = true
        text2.isScrollEnabled = false
        text2.translatesAutoresizingMaskIntoConstraints = true
        text2.sizeToFit()
        let attrtbued = NSAttributedString.makeHyperlink(for: text, in: text2.text, as: name)
        
        text2.attributedText = attrtbued

//        text2.font = UIFont(name: "GE Dinar One", size: 17)

    }

}
 
extension SourcesAppController:ApiResponseDaelegat{
    func showIndecator() {
        
        SVProgressHUD.show()
    }
    
    func HideIndicator() {
        SVProgressHUD.dismiss()
    }
    
    func getResponseSuccuss() {
        
    }
    
    func getResponseFalueir(meeage:String) {
        let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getResponseMissigData() {
        
    }
    
    
    
    
}
