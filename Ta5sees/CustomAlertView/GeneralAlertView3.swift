//
//  GeneralAlertView3.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 09/08/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit

class GeneralAlertView3: UIViewController,UIGestureRecognizerDelegate {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var stackBtn: UIStackView!
    var delegate: CustomAlertViewDelegate1?

    @IBOutlet weak var txt: UILabel!

    var appBundelID:String!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)

    var text = ""

    
    var isFlagController = "0" // 0-> viewCotroller else AlarmSitingController
    var pd:UIViewController?
    func add(stringList: [String],
             font: UIFont,
             bullet: String = "\u{2022}",
             indentation: CGFloat = 20,
             lineSpacing: CGFloat = 2,
             paragraphSpacing: CGFloat = 12,
             textColor: UIColor = .gray,
             bulletColor: UIColor = .red) -> NSAttributedString {

        let textAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: textColor]
        let bulletAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: bulletColor]

        let paragraphStyle = NSMutableParagraphStyle()
        let nonOptions = [NSTextTab.OptionKey: Any]()
        paragraphStyle.tabStops = [
            NSTextTab(textAlignment: .left, location: indentation, options: nonOptions)]
        paragraphStyle.defaultTabInterval = indentation
        //paragraphStyle.firstLineHeadIndent = 0
        //paragraphStyle.headIndent = 20
        //paragraphStyle.tailIndent = 1
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacing = paragraphSpacing
        paragraphStyle.headIndent = indentation

        let bulletList = NSMutableAttributedString()
        for string in stringList {
            let formattedString = "\(bullet)\t\(string)\n"
            let attributedString = NSMutableAttributedString(string: formattedString)

            attributedString.addAttributes(
                [NSAttributedString.Key.paragraphStyle : paragraphStyle],
                range: NSMakeRange(0, attributedString.length))

            attributedString.addAttributes(
                textAttributes,
                range: NSMakeRange(0, attributedString.length))

            let string:NSString = NSString(string: formattedString)
            let rangeForBullet:NSRange = string.range(of: bullet)
            attributedString.addAttributes(bulletAttributes, range: rangeForBullet)
            bulletList.append(attributedString)
        }

        return bulletList
    }

    var arrayString:[String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txt.textColor = UIColor.white
        txt.numberOfLines = 0

      
        txt.attributedText = add(stringList: arrayString, font: txt.font, bullet: "")//""

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
   
        btn4.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: 0)
        btn3.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: 0)

        
    }
    
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    func dissmesController(){
        pd?.view.isUserInteractionEnabled = true
        if isFlagController == "0" {
        pd?.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func onTap(sender: UITapGestureRecognizer) {
        self.view.window?.removeGestureRecognizer(sender)
        dissmesController()
    }
    
    var tap: UITapGestureRecognizer!
    override func viewDidAppear(_ animated: Bool) {

        tap = UITapGestureRecognizer(target: self, action: #selector(onTap(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.cancelsTouchesInView = true
        tap.delegate = self
        self.view.window?.addGestureRecognizer(tap)
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let location = touch.location(in: self.view)

        if self.view.point(inside: location, with: nil)  {
        
            return false
        }else {
            return true
        }
    }
    @IBAction func btn_click_1(_ sender: Any) {
//        delegate?.okButtonTapped(selectedOption: "dropdown.text!", textFieldValue: "\(flag)")
//        self.view.window?.removeGestureRecognizer(tap)
//        dissmesController()
//        self.dismiss(animated: true, completion: nil)
    }
   

    @IBAction func btn_click_2(_ sender: Any) {
//        delegate?.okButtonTapped(selectedOption: "dropdown.text!", textFieldValue: "\(flag)")
//        self.view.window?.removeGestureRecognizer(tap)
//        dissmesController()
//        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_click_3(_ sender: Any) {
        delegate?.okButtonTapped1(selectedOption: "dropdown.text!", textFieldValue: appBundelID)
        self.view.window?.removeGestureRecognizer(tap)
        dissmesController()
        self.dismiss(animated: true, completion: nil)
//        delegate?.okButtonTapped(selectedOption: "dropdown.text!", textFieldValue: "\(flag)")
//        self.view.window?.removeGestureRecognizer(tap)
//        dissmesController()
//        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btn_click_4(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.view.window?.removeGestureRecognizer(tap)
        dissmesController()
        delegate?.cancelButtonTapped1()
    }
}
