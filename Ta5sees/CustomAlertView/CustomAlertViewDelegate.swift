//
//  CustomAlertViewDelegate.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

protocol CustomAlertViewDelegate: AnyObject {
    func okButtonTapped(selectedOption: String, textFieldValue: String)
    func cancelButtonTapped()
}


protocol CustomAlertViewDelegate1: AnyObject {
    func okButtonTapped1(selectedOption: String, textFieldValue: String)
    func cancelButtonTapped1()
}
