//
//  GeneralAlertView2.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 08/08/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation


import UIKit
import iOSDropDown
class GeneralAlertView2: UIViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var stackBtn: UIStackView!

    var isFlagController = "0" // 0-> viewCotroller else AlarmSitingController
    var pd:UIViewController?

    var titltMessage = ""
    var noteMessage = ""
    var cancelBtnMessage = ""
    var okBtnMessage = ""
    var flag = 0 // 1->major ... 2->notmajor ...
    
    var delegate: CustomAlertViewDelegate?
    var selectedOption = "First"
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    func dissmesController(){
        pd?.view.isUserInteractionEnabled = true
        if isFlagController == "0" {
        pd?.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        dismiss(animated: true, completion: nil)
    }
    @objc private func onTap(sender: UITapGestureRecognizer) {
        self.view.window?.removeGestureRecognizer(sender)
        dissmesController()
    }
    var tap: UITapGestureRecognizer!
    override func viewDidAppear(_ animated: Bool) {

        tap = UITapGestureRecognizer(target: self, action: #selector(onTap(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.cancelsTouchesInView = true
        tap.delegate = self
        self.view.window?.addGestureRecognizer(tap)
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let location = touch.location(in: self.view)

        if self.view.point(inside: location, with: nil)  {
        
            return false
        }else {
            return true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageLabel.text = titltMessage
        note.text = noteMessage
        okButton.setTitle(okBtnMessage, for: .normal)
        cancelButton.setTitle(cancelBtnMessage, for: .normal)
        okButton.titleLabel?.textAlignment = .center
        cancelButton.titleLabel?.textAlignment = .center

        if flag == 1 {
            stackBtn.arrangedSubviews[0].isHidden = true
        }
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap(gesture:)))
//        view.addGestureRecognizer(tapGesture)
//        window?.superview?.addGestureRecognizer(tapGesture)
        setupButton(btn:okButton)
        setupButton(btn:cancelButton)
       
    }
    @objc func didTap(gesture: UIGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        if flag != 1 {
            cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: cancelButton.frame.width)
            cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1, widithBtn: 0)
        }
        if flag != 1 {
        okButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: okButton.frame.size.width)
        }else{
            okButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: okButton.frame.size.width+cancelButton.frame.size.width)

        }
    }
    func setupButton(btn:UIButton){
        btn.titleLabel?.numberOfLines = 10
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.titleLabel?.textAlignment = .center
    }
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.view.window?.removeGestureRecognizer(tap)
        dissmesController()
        delegate?.cancelButtonTapped()
    }
    
    @IBAction func onTapOkButton(_ sender: Any) {
        delegate?.okButtonTapped(selectedOption: "dropdown.text!", textFieldValue: "\(flag)")
        self.view.window?.removeGestureRecognizer(tap)
        dissmesController()
        self.dismiss(animated: true, completion: nil)
    }
    
}
