//
//  GeneralAlertView.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 29/06/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//


import UIKit
import iOSDropDown
class GeneralAlertView: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var inputsContainerHeightAnchor: NSLayoutConstraint!
    @IBOutlet weak var inputsContainerwieghtAnchor: NSLayoutConstraint!

    var text:String!
    var delegate: CustomAlertViewDelegate?
    var selectedOption = "First"
    var remainingDays:Int!
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputsContainerHeightAnchor.constant = 500
        inputsContainerwieghtAnchor.constant = 500


        messageLabel.text = text
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap(gesture:)))
        view.addGestureRecognizer(tapGesture)
        setupButton(btn:cancelButton)
    }
    @objc func didTap(gesture: UIGestureRecognizer) {
        print("You clicked on Monthly")
        self.dismiss(animated: true, completion: nil)
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1, widithBtn: cancelButton.frame.width)
        cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1, widithBtn: 0)
    }
    func setupButton(btn:UIButton){
        btn.titleLabel?.numberOfLines = 10
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.titleLabel?.textAlignment = .center
    }
    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onTapCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.cancelButtonTapped()
    }
    
    
    @IBAction func onTapSegmentedControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            print("First option")
            selectedOption = "First"
            break
        case 1:
            print("Second option")
            selectedOption = "Second"
            break
        default:
            break
        }
    }
}
