
//
//  Heypertention.swift
//  Ta5sees
//
//  Created by Admin on 9/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import iOSDropDown
import TextFieldEffects
//import DLRadioButton
import RealmSwift
 
class Heypertention: UIViewController,UIViewControllerTransitioningDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Items[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        Items.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item: tblHypertensionRanges  =  arraylist.filter { Items[row].contains(String($0.lowFrom)) &&  Items[row].contains(String($0.lowTo)) && Items[row].contains(String($0.highTo)) && Items[row].contains(String($0.highFrom)) }.first!
        print(item.id)
        setDataInSheardPreferance(value:String(item.id), key: nameMidicalTietType)
        
        
    }
    
    var arraylist:[tblHypertensionRanges] = []
    var resultelist:Results<tblHypertensionRanges>!
    var refIDSelectList:String!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontText(text:titlePage,size:30)
        titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        
        
    }
    
    var nameMidicalTietType:String!
    @IBOutlet weak var subView: UIView!
    var refIDiet = getDataFromSheardPreferanceString(key: "dietType")
    var Items:[String]!
    
    
    
    @IBOutlet weak var dataPicker: UIPickerView!
    @IBOutlet weak var titlePage: UILabel!
    
    @IBAction func btnNext(_ sender: Any) {
        let detailView = storyboard!.instantiateViewController(withIdentifier: "FormatNaturalDiet") as! FormatNaturalDiet
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        nameMidicalTietType = "Bloodpressure"

        tblHypertensionRanges.getAllData() {(response, Error) in
            self.resultelist = (response) as? Results<tblHypertensionRanges>
            self.arraylist = Array(self.resultelist)
            
            for i in 0..<self.arraylist.count {
                if i == 1 {
                    setDataInSheardPreferance(value:"\(self.arraylist[i].id)", key: self.nameMidicalTietType)

                }
                if self.arraylist[i].id == 3 {
                    if self.Items == nil {
                        self.Items=["اكبر من \(self.arraylist[i].lowFrom),اكبر من \(self.arraylist[i].highFrom)"]
                    }else {
                        self.Items.append("اكبر من \(self.arraylist[i].lowFrom),اكبر من \(self.arraylist[i].highFrom)")
                    }
                }else{
                    if self.Items == nil {
                        self.Items=["\(self.arraylist[i].highTo)-\(self.arraylist[i].highFrom),\(self.arraylist[i].lowTo)-\(self.arraylist[i].lowFrom)"]
                    }else {
                        self.Items.append("\(self.arraylist[i].highTo)-\(self.arraylist[i].highFrom),\(self.arraylist[i].lowTo)-\(self.arraylist[i].lowFrom)")
                        
                    }
                }
            }
        }
        dataPicker.selectRow(1, inComponent: 0, animated: true)
        
        
        
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
}




