//
//  CuisineVC.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 25/03/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class CuisineVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UIViewControllerTransitioningDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return items.count
        } else {
            return 1
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 1 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BTNColletionCell",for: indexPath) as? BTNColletionCell else {
                return BTNColletionCell()
            }
            cell.btn.addTarget(self, action: #selector(clicked(_:)), for: .touchUpInside)
            cell.btnNoThing.addTarget(self, action: #selector(clickedNoThing(_:)), for: .touchUpInside)
            
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DietsVCCell",for: indexPath) as? DietsVCCell else {
            return formateSelectedCell()
        }
        let item = section[indexPath.section].items[indexPath.row]
        
        
        if section[indexPath.section].isMultiple {
            
            //For multiple selection
            if item.selected {
                cell.layer.borderWidth = 3
                cell.layer.borderColor = UIColor.gray.cgColor
                cell.layer.cornerRadius = 9
            }else {
                cell.layer.borderWidth = 0
            }
        }
        
        //        if item.id == ActivationModel.sharedInstance.refCusine {
        //            cell.layer.borderWidth = 2
        //            cell.layer.borderColor = UIColor.gray.cgColor
        //            cell.layer.cornerRadius = 9
        //            item.selected = true
        //        }
        if item.id == "9"{
            cell.txt.text = section[indexPath.section].items[indexPath.row].name
        }else{
            cell.txt.text = "حمية من بيتك (\(section[indexPath.section].items[indexPath.row].name))"
        }
        cell.txt.textAlignment = .center
        cell.txt.font = UIFont(name: "GE Dinar One",size: 20)
        cell.txt.layer.cornerRadius = 9
        cell.txt.backgroundColor = UIColor.white.withAlphaComponent(0)
        setGradientBackgroundLable(txt: cell.txt,delegate:self)
        
        return cell
        
        
    }
    @objc func clicked(_ sender:UIButton){
        
        let refItem:[Item] = items.filter { (item) -> Bool in
            if item.selected {
                return true
            }
            return false
        }
        let refIDs:[String] = refItem.map { (item) in
            item.id
        }
        ActivationModel.sharedInstance.xtraCusine.append(contentsOf: refIDs)
        ActivationModel.sharedInstance.xtraCusine.append(ActivationModel.sharedInstance.refCusine)
//        ActivationModel.sharedInstance.refCusine = ""
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "ContainerVC") as! ContainerVC
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        createEvent(key: "27",date: getDateTime())
        self.present(detailView, animated: true, completion: nil)
    }
    
    @objc func clickedNoThing(_ sender:UIButton){
        
        ActivationModel.sharedInstance.xtraCusine.append(ActivationModel.sharedInstance.refCusine)
//        ActivationModel.sharedInstance.refCusine = ""
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "ContainerVC") as! ContainerVC
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        self.present(detailView, animated: true, completion: nil)
    }
    var items = [Item]()
    var section = [Section]()
    var obselectionDelegate:selectionDelegate!
    var optionsSelected = ""
    var flag = "14" // flag 1 >> cusine / flag 2 >> allergy
    var index:Int!
    var seletAll = 0
    lazy var obviewMultiSelectPresnter = viewMultiSelectPresnter(with: self)
    @IBOutlet weak var subViewBacground: UIView!

    @IBOutlet weak var titlePage: UILabel!
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        obviewMultiSelectPresnter.getDataWithID(id:ActivationModel.sharedInstance.refCusine)
        setUpCollectionView()
        let cusine = setupRealm().objects(tblCusisneType.self).filter("id == %@",ActivationModel.sharedInstance.refCusine).first
        titlePage.text = "لقد اخترت مطبخ \(cusine?.discription ?? "") لنظامك الغذائي \n يمكنك تشكيل نظامك الغذائي مع مطبخ اخر (اضغط للاختيار)"
        titlePage.numberOfLines = 4
//        let filtered = items.map{$0.name}.intersection(with:optionsSelected.split(separator: ",").map { String($0) })
        
        // items.filter { filtered.map { String($0) }.contains($0.name)}
//        _ = items.filter { (Item) -> Bool in
//            Item.selected = filtered.map { String($0) }.contains(Item.name)
//            return true
//        }
        
        
//        let imageName = "logo"
//        let image = UIImage(named: imageName)
//        let imageView = UIImageView(image: image!)
//        imageView.contentMode = .center
//        
//        let txtTitle = UILabel()
//        txtTitle.text = "نوع المطبخ"
//        txtTitle.font =  UIFont(name: "GE Dinar One", size: 20.0)
//        txtTitle.frame = CGRect(x: 0, y: 0, width: subViewBacground.frame.width-32, height: 40)
//        txtTitle.textColor = .white
//        txtTitle.textAlignment = .center
//        let stack = UIStackView()
//        stack.frame = CGRect(x: 0, y: 0, width: subViewBacground.frame.width, height: subViewBacground.frame.height*0.80)
//
//
//        stack.addArrangedSubview(imageView)
//        stack.addArrangedSubview(txtTitle)
//
//        stack.axis = .vertical
//        stack.alignment = .center
//        stack.distribution = .fill
//        stack.spacing = 24
//        subViewBacground.addSubview(stack)
//
//   
//        stack.center.x = view.center.x
//        stack.center.y = subViewBacground.center.y+15
    }
    
    
    func setUpCollectionView() {
        /// 1
        
        
        /// 2
        tblview.delegate = self
        tblview.dataSource = self
        
        /// 3
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        /// 4
        layout.minimumLineSpacing = 8
        /// 5
        layout.minimumInteritemSpacing = 4
        
        /// 6
        tblview.setCollectionViewLayout(layout, animated: true)
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        
        
    }
    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setUNSeletd(falg:Bool){
        for item in items {
            item.selected = falg
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if section[indexPath.section].isMultiple {
            let item = section[indexPath.section].items[indexPath.row]
//            if item.id == ActivationModel.sharedInstance.refCusine {
//                ActivationModel.sharedInstance.refCusine = ""
//            }
            //For multiple selection
            item.selected = !item.selected
            
            section[indexPath.section].items[indexPath.row] = item
            tblview.reloadItems(at: [indexPath])
        }else {
            //For multiple selection
            let items = section[indexPath.section].items
            
            if let selectedItemIndex = items.indices.first(where: { items[$0].selected }) {
                section[indexPath.section].items[selectedItemIndex].selected = false
                if selectedItemIndex != indexPath.row {
                    section[indexPath.section].items[indexPath.row].selected = true
                }
            }
            else {
                section[indexPath.section].items[indexPath.row].selected = true
            }
            tblview.reloadItems(at: [indexPath])
        }
        
        
    }
    
    
    
    @IBOutlet weak var tblview: UICollectionView!
}

extension CuisineVC:viewMultiSelectDelegate {
    
    func getDataSection(section: [Section]) {
        self.section = section
    }
    
    func getDataArray(list: [Item]) {
        items = list
 
        if items.contains(where: {$0.selected == true}) {
            seletAll = 0
        } else {
            seletAll = 1

        }
    }
    
    
}

extension CuisineVC: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            /// 2
            return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
        }
        return UIEdgeInsets(top:12.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
    
    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            /// 4
            let lay = collectionViewLayout as! UICollectionViewFlowLayout
            /// 5
            let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
            /// 6
            return CGSize(width: widthPerItem - 8, height: 100)
        }
        return CGSize(width: tblview.frame.width-32, height: 44)
    }
}
extension CuisineVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
