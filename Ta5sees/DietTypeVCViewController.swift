//
//  DietTypeVCViewController.swift
//  Ta5sees
//
//  Created by Admin on 3/21/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import FLAnimatedImage
import Firebase
import FirebaseAnalytics

class DietTypeVCViewController: UIViewController,UIViewControllerTransitioningDelegate {
 
   
  
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var currentDiet: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        currentDiet.text = "الحمية الحالية : \(setTextTitlePage(id:getUserInfoProgressHistory().refPlanMasterID))"
        setUpView()

        if  ActivationModel.sharedInstance.refCusine == "8" {
            stackView.arrangedSubviews[0].isHidden = true
        }

    }
    
    
    @IBAction func btnBack(_ sender: Any) {
            dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
    }
  
  
    @IBOutlet weak var gainDiet: viewColorsWithRaduis!
    @IBOutlet weak var lossDiet: viewColorsWithRaduis!
    @IBOutlet weak var maintainDiet: viewColorsWithRaduis!
    @IBOutlet weak var txtGain: UILabel!
    @IBOutlet weak var txtLoss: UILabel!

    @IBOutlet weak var txtMaintain: UILabel!
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    func setUpView(){
        
        maintainDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        txtMaintain.text = "تثبيت الوزن"
        maintainDiet.isUserInteractionEnabled = true
        
        lossDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        txtLoss.text = "تخسيس الوزن"
        lossDiet.isUserInteractionEnabled = true
        
        gainDiet.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        txtGain.text = "زيادة الوزن"
        gainDiet.isUserInteractionEnabled = true
        
    }
    
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        print("@@",tappedImageView.tag)
        ActivationModel.sharedInstance.dietType = "\(tappedImageView.tag)"
        print(tappedImageView.tag)
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "UpdateInfoUser") as! UpdateInfoUserVC
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        createEvent(key: "25",date: getDateTime())
        self.present(detailView, animated: true, completion: nil)

    }
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
}
extension DietTypeVCViewController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
