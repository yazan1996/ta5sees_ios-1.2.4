//
//  progresBarHome.swift
//  Ta5sees
//
//  Created by Admin on 1/23/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class progresBarHome {
    
    var view:UIView!
    var contentView:UIView!

    var progresBar:CircularProgressBar!
    init(view:UIView,contentView:UIView) {
        self.view = view
        self.contentView = contentView
//        let xPosition = contentView.frame.midX
//         let yPosition = contentView.frame.midY
        let position =  CGPoint(x: contentView.frame.size.width / 2.0, y: contentView.frame.size.height / 2.0)
        progresBar = CircularProgressBar(radius: 80, position: position, innerTrackColor:  defaultInnerColor, outerTrackColor: .white, lineWidth: 2)
        contentView.layer.addSublayer(progresBar)


    }
    
    func setProgressValue(totalCalorisDay:Int ,totalCalorisEate:Int){
       

        progresBar.progress = CGFloat(totalCalorisDay-totalCalorisEate)
        progresBar.progressEate = CGFloat(totalCalorisEate)
        if totalCalorisEate > totalCalorisDay {
            progresBar.str = "زيادة"
        }else{
            progresBar.str = "متبقي"
        }
    }


}

