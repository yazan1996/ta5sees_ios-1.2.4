//
//  Cholestrole.swift
//  Ta5sees
//
//  Created by Admin on 9/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit

 
class Cholestrole: UIViewController,UIViewControllerTransitioningDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @IBOutlet weak var titlePage: UILabel!

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return numbersCholestrole[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numbersCholestrole.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        setDataInSheardPreferance(value:numbersCholestrole[row], key: "Cholesterol")
    }
    
    var nameMidicalTietType:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontText(text:titlePage,size:30)
        titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setDataInSheardPreferance(value:"\(numbersCholestrole[200])", key:  "Cholesterol")
        piker.selectRow(200, inComponent: 0, animated: true)
        
        
    }
    
    @IBOutlet weak var piker: UIPickerView!
    @IBOutlet weak var subView: UIView!
    
   
    @IBAction func btnNext(_ sender: Any) {
        let detailView = storyboard!.instantiateViewController(withIdentifier: "FormatNaturalDiet") as! FormatNaturalDiet
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            detailView.transitioningDelegate = self
            present(detailView, animated: true, completion: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
          dismiss(animated: true, completion: nil)

      }
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
}
