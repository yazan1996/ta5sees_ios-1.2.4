//
//  LogoAnimationView.swift
//  AnimatedGifLaunchScreen-Example
//
//  Created by Amer Hukic on 13/09/2018.
//  Copyright © 2018 Amer Hukic. All rights reserved.
//

import UIKit
import SwiftyGif

class LogoAnimationView: UIView {
    
    let logoGifImageView = try! UIImageView(gifImage: UIImage(gifName: "LogoAnimation.gif"), loopCount: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
  
    private func commonInit() {
            logoGifImageView.frame.size.width = self.frame.size.width
              logoGifImageView.frame.size.height = self.frame.size.height
              addSubview(logoGifImageView)

              logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
              logoGifImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
              logoGifImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
              logoGifImageView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width).isActive = true
              logoGifImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.height).isActive = true
        
        
    }
}
