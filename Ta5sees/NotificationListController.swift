//
//  NotificationListController.swift
//  Ta5sees
//
//  Created by Admin on 9/21/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD


class NotificationListController: UIViewController,UIViewControllerTransitioningDelegate ,UIPopoverPresentationControllerDelegate,UITabBarControllerDelegate{
    
    var obprotocolWajbehInfoTracker:protocolWajbehInfoTracker?
    var id_arr = ["1","2","3","4","5"]
    @IBOutlet weak var tblview: UITableView!
    var listNotfy:[tblNotifications] = tblNotifications.getAllNotify()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obprotocolWajbehInfoTracker = self
        
        
    }
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
        
    }
    
    @IBAction func dissims(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBOutlet weak var viewContanir: UIView!
    func viewPopupTarget(date:String,index:Int) {
        
        
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "PopuPControllerWieght") as! PopuPControllerWieght
        popoverContent.date = date
        popoverContent.indexItem = index
        popoverContent.presnter = self
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
        popover!.delegate = self
        popover!.sourceView = self.viewContanir
        popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)
        
        
        
        self.present(nav, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addWajbehSenfManual" {
            if let dis=segue.destination as?  SearchItems{
                if  let data=sender as? [String] {
                    dis.id_items = data[0]
                    dis.date = convertDate(data1:data[1])
                    dis.pg = self
                }
            }
        }
        else if segue.identifier == "TamreenTypeListController" {
            if let dis=segue.destination as?  TamreenTypeListController{
                if  let data=sender as? String {
                    dis.date = convertDate(data1:data)
                    dis.refprotocolGeneralTamreen = self
                }
            }
        }
    }
    
    func convertDate(data1:String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let str:String = data1
        let date = dateFormatter.date(from: str)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date)
    }
    
    func hanelOpenAppwithNotif(id:String,date:String){
        
        //        let mainStoryboard:UIStoryboard=UIStoryboard(name:"Main", bundle:nil)
        //        let yourVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain")
        //        setDataInSheardPreferance(value: id, key: "id_alarm")
        //        setDataInSheardPreferance(value: convertDate(data1:date), key: "dateNotfy")
        //        self.present(yourVC, animated: true, completion: nil)
        
//        let appDelegate = UIApplication.shared.delegate as!AppDelegate
//        appDelegate.window = UIWindow(frame:UIScreen.main.bounds)
        let mainStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarMain") as! UITabBarController
        if let vcs = tabbarVC.viewControllers,
           let nc = vcs.first as? UINavigationController,
           let pendingOverVC = nc.topViewController as? ViewController {
            pendingOverVC.id_alarm = id
            pendingOverVC.dateNotfy = convertDate(data1:date)
        }
        
        window?.rootViewController = tabbarVC
        window?.makeKeyAndVisible()
        
    }
}


extension NotificationListController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listNotfy.count > 0 {
            return listNotfy.count
        } else {
            TableViewHelper.EmptyMessage(message: "لايوجد إشعارات متاحة", viewController: tblview)
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NotificationCell =
            tblview.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.title.text = listNotfy[indexPath.row].title
        cell.body.text = listNotfy[indexPath.row].body
        cell.date.text = listNotfy[indexPath.row].date
        
        cell.logo_image.image = resizeImage(image: UIImage(named:cell.setImageIcon(id: listNotfy[indexPath.row].type_Notfy))!, targetSize: CGSize(width: 40.0, height: 40.0))
        cell.delegat = self
        cell.index = indexPath.row
        //        SetImage(laImage: cell.logo_image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = listNotfy[indexPath.row]
        if item.type_Notfy == "8" || item.type_Notfy == "7" {
            hanelOpenAppwithNotif(id:item.type_Notfy, date: item.date)
        }else if item.type_Notfy == "6"{
            self.performSegue(withIdentifier: "TamreenTypeListController", sender: item.date)
        }else if id_arr.contains(item.type_Notfy) {
            self.performSegue(withIdentifier: "addWajbehSenfManual", sender: [item.type_Notfy,item.date])
        }
        tblNotifications.deletNotify(id: listNotfy[indexPath.row].id)
        listNotfy = tblNotifications.getAllNotify()
        tblview.reloadData()
    }
    
    //    func SetImage(laImage:UIImageView){
    //        laImage.layer.masksToBounds = false
    //        laImage.layer.cornerRadius = 30
    //        laImage.clipsToBounds = true
    //
    //    }
    
    
    
}

 
extension NotificationListController :prisnterDataNotification{
    func removeItem(index: Int) {
        if let row = self.listNotfy.firstIndex(where: {$0.id == listNotfy[index].id }) {
            tblNotifications.removeNotification(id: listNotfy[index].id)
            self.listNotfy.remove(at: row)
            let indexPath = IndexPath(item: row, section: 0)
            self.tblview.deleteRows(at: [indexPath], with: .fade)
            showToast(message: "تم تعديل الوزن", view: self.view,place:0)
            SVProgressHUD.dismiss()
        }
    }
    
    func viewPopup(index:Int) {
        print(listNotfy[index].date)
        viewPopupTarget(date: listNotfy[index].date, index: index)
    }
    
    
}
 
extension NotificationListController:protocolWajbehInfoTracker{
    
    
    func refreshListItemTasbera2(itemTasbera2: tblUserWajbehEaten!) {
        print("#1")
        tblview.reloadData()
    }
    
    func refreshListItemTasbera1(itemTasbera2: tblUserWajbehEaten!) {
        print("#2")
        tblview.reloadData()
    }
    
    func refreshListItemBreakfast(itemTasbera2: tblUserWajbehEaten!) {
        print("#3")
        tblview.reloadData()
    }
    
    func refreshListItemDinner(itemTasbera2: tblUserWajbehEaten!) {
        print("#4")
        tblview.reloadData()
    }
    
    func refreshListItemLaunsh(itemTasbera2: tblUserWajbehEaten!) {
        print("#5")
        tblview.reloadData()
    }
    
    func refreshListItemTamreen(itemTamreen: tblUserTamreen!) {
        print("#6")
        tblview.reloadData()
    }
    func refrechData(){
        tblview.reloadData()
    }
    
    func setUPViewEnable(view:UIView){
        view.isUserInteractionEnabled = true
        view.alpha = 1
    }
    
    func setUPViewDisable(view:UIView){
        view.isUserInteractionEnabled = false
        view.alpha = 0.5
    }
    
}

 
extension NotificationListController: protocolGeneral{
    func setdate(m: String, d: Int, y: Int) {
        tblview.reloadData()
    }
    
    func saveAmountWaterDrink(x: Int) {
        tblview.reloadData()
    }
    
    func saveAmountWaterGoal(x: Int) {
        tblview.reloadData()
        
    }
    
    func refreshWater(flag: Bool) {
        tblview.reloadData()
    }
    
    func TrackerWater(date: String) {
        tblview.reloadData()
    }
    
    func TrackerWeight(date: String) {
        tblview.reloadData()
    }
    
    func TrackerTamreen() {
        tblview.reloadData()
    }
    
    
}
