//
//  FridgeController.swift
//  Ta5sees
//
//  Created by Admin on 3/16/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
import RxCocoa

class FridgeController: UITableViewController {
    
    var x:[Int]! = []
    
    let disposeBag = DisposeBag()
    var items : Observable<[shoppingModel]>!
    @IBOutlet var tbl: UITableView!
    lazy var obShoppingListPresnter = ShoppingListPresnter(with: self)
    var initialDataAry=[shoppingModel]()
    var dataAry = [shoppingModel]()
    let textLabel = UILabel()
    //    var nameItem:String!
    
    var count = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl.register(FridgeHeaderCell.self,
               forHeaderFooterViewReuseIdentifier: "sectionHeader")
        
        tbl.rx.modelSelected(shoppingModel.self).subscribe(onNext: {
            item in //selec all row
            if let selectedRowIndexPath = self.tbl.indexPathForSelectedRow {
                self.tbl.deselectRow(at: selectedRowIndexPath, animated: true)
            }
        }).disposed(by: disposeBag)

        
        tbl.rx.itemSelected.subscribe(onNext : {
            [weak self] indexPath in
            if (self?.tbl.cellForRow(at: indexPath) as? TableShoppingViewCell) != nil {
                
                if let cell = self?.tbl.cellForRow(at: indexPath) as? TableShoppingViewCell {
                    
                    let item: shoppingModel = try! self!.tbl.rx.model(at: indexPath)
                    if item.isCheck == 0 {
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:1) { (response) in
                            item.isCheck = 1
                            cell.btn.setImage(UIImage(named: "green_ic_checked"), for: .normal)
                        }
                    }else{
                        tblShoppingList.shared.updateRow1(name:item.nameSenf, date: item.date,value:0) { (response) in
                            item.isCheck = 0
                            cell.btn.setImage(UIImage(named: "green_ic_unchecked"), for: .normal)
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

       
        
        obShoppingListPresnter.getAllIngridintForPurchased(rang:7)
        items = Observable.just(dataAry)
        bindData()
  
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
    }
    
    private func bindData() {
        tbl.dataSource = nil
        items.bind(to: tbl.rx.items(cellIdentifier: "cellShopping")) { (row, newitem, cell) in
            if let cellToUse = cell as? TableShoppingViewCell {
                cellToUse.item = newitem
            }
        }.disposed(by: disposeBag)
    }
    
  
    
    private func prependData(dataToPrepend : [shoppingModel]) {
        let newObserver = Observable.just(dataToPrepend)
        items = Observable.combineLatest(items, newObserver) {
            $1+$0
        }
        bindData()
    }
    
    private func appendData(dataToAppend : [shoppingModel]) {
        let newObserver = Observable.just(dataToAppend)
        items = Observable.combineLatest(items, newObserver) {
            $0+$1
        }
        bindData()
    }
    

    
    
    static var indexID:Int!
    
    
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if initialDataAry.count > 0 {
            return initialDataAry.count
        } else {
            TableViewHelper.EmptyMessage(message: "تأتيكم قريبا", viewController: tbl)
            return 0
        }
    }
      
  
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableShoppingViewCell = tableView.dequeueReusableCell(withIdentifier: "cellShopping", for: indexPath) as! TableShoppingViewCell

        
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
    }
    
    func reloadData(count : Int) {
        if self.count != count{
            obShoppingListPresnter.getAllIngridintForPurchased(rang:count)
            items = Observable.just(dataAry)
            bindData()
        }
    }
    
    override func tableView(_ tableView: UITableView,
            viewForHeaderInSection section: Int) -> UIView? {
       let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                   "sectionHeader") as! FridgeHeaderCell
       view.title.text = "فلتر"
       view.viewCV = self
       view.image.image = UIImage(named: "filter-menu")

       return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
}
extension FridgeController:ShoppingListView {
    func setListIngridint(list: [shoppingModel]) {
        dataAry.removeAll()
        print("list \(list)")
        dataAry = list
    }
}
