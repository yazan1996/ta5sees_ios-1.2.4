//
//  SplashViewController.swift
//  Ta5sees
//
//  Created by Admin on 4/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import SwiftyGif

class SplashViewController: UIViewController {
    let logoAnimationView = LogoAnimationView()


    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
        logoAnimationView.logoGifImageView.delegate = self

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoAnimationView.logoGifImageView.startAnimatingGif()
    }
 

}
extension SplashViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        logoAnimationView.isHidden = true
    }
}
