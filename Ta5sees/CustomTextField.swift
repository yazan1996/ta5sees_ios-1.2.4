//
//  CustomSearchTextField.swift
//  CustomSearchField
//
//  Created by Emrick Sinitambirivoutin on 19/02/2019.
//  Copyright © 2019 Emrick Sinitambirivoutin. All rights reserved.
//

import UIKit
import Foundation
//import RealmSwift


 
class CustomTextField: UITextField{
    
    var dataList : [tblPackeg] = [tblPackeg]()
    var dataListSenf : [tblSenf] = [tblSenf]()
    var resultsList : [SearchItem] = [SearchItem]()
    var tableView: UITableView?
    var obShowNewWajbeh:ShowNewWajbeh?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    lazy var obGetListSearchItems = GetListSearchItems(with: self)
    // Connecting the new element to the parent view
    open override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        tableView?.removeFromSuperview()
        
    }
    
    override open func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        self.addTarget(self, action: #selector(CustomTextField.textFieldDidChange), for: .editingChanged)
        self.addTarget(self, action: #selector(CustomTextField.textFieldDidBeginEditing), for: .editingDidBegin)
        self.addTarget(self, action: #selector(CustomTextField.textFieldDidEndEditing), for: .editingDidEnd)
        self.addTarget(self, action: #selector(CustomTextField.textFieldDidEndEditingOnExit), for: .editingDidEndOnExit)
    }
    
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        buildSearchTableView()
        
    }
    
    
    //////////////////////////////////////////////////////////////////////////////
    // Text Field related methods
    //////////////////////////////////////////////////////////////////////////////
    
    @objc open func textFieldDidChange(){
        //        print("Text changed ...\(wajbehInfoID)")
        self.self.filter()
        print("Reload 2")
        
        self.updateSearchTableView()
        
        self.tableView?.isHidden = false
    }
    
    @objc open func textFieldDidBeginEditing() {
        print("Begin Editing")
    }
    
    @objc open func textFieldDidEndEditing() {
        print("End editing")
        
    }
    
    @objc open func textFieldDidEndEditingOnExit() {
        print("End on Exit")
    }
    
    //////////////////////////////////////////////////////////////////////////////
    // Data Handling methods
    //////////////////////////////////////////////////////////////////////////////
    
    
    // MARK: CoreData manipulation methods
    
    // Don't need this function in this case
    func saveItems() {
        print("Saving items")
        do {
            try context.save()
        } catch {
            print("Error while saving items: \(error)")
        }
    }
    
//    func loadWajbeh() {
//        print("loading items")
//        var resultelist:Results<tblPackeg>!
//        tblPackeg.getAllWajbeh() {(response, Error) in
//            resultelist = (response) as? Results<tblPackeg>
//            self.dataList = Array(resultelist)
//
//        }
//
//    }
//
//
//
//    func loadSenf() {
//        print("loading items")
//
//
//        var resultelistSenf:Results<tblSenf>!
//        tblSenf.getAllSenf() {(response, Error) in
//            resultelistSenf = (response) as? Results<tblSenf>
//            self.dataListSenf.append(contentsOf: Array(resultelistSenf))
//        }
//    }
    
    
    
    // MARK: Filtering methods
    
    fileprivate func filter() {
        //        dataList = tblWajbeh.searchModelArray(wajbehInfo: "1").filter({ (mod) -> Bool in
        //            return mod.discription!.lowercased().contains(text!.lowercased())
        //        })
        //        obGetListSearchItems.getListWajbehItems(wajbehInfoID: "1", text: (text?.lowercased())!) { (error) in
        //
        //        }
        DispatchQueue.main.async {
            
            self.obGetListSearchItems.getListWajbehItems(wajbehInfoID: "1", text: (self.text?.lowercased())!)
        }
        DispatchQueue.main.async {
            
            self.obGetListSearchItems.getListSenfItems(text: (self.text?.lowercased())!)
        }
        //        dataListSenf = tblSenf.searchModelArray().filter({ (mod) -> Bool in
        //            return mod.discription!.lowercased().contains(text!.lowercased())
        //        })
        
        //        self.loadWajbeh()
        //
        //        self.loadSenf()
        
        
        resultsList = []
 
        
        
        DispatchQueue.main.async {
            
            
            for i in 0 ..< self.dataList.count {
                
                let item = SearchItem(iteme_name: self.dataList[i].name!,item_id: String(self.dataList[i].id), item_flag: "wajbeh",caolris: self.dataList[i].calories!,iteme_search: self.dataList[i].searchKey!)
               
              
              
                let NameFilterRange = (item.iteme_search as NSString).range(of: self.text!, options: .caseInsensitive)

//                let IDFilterRange = (item.item_id as NSString).range(of: self.text!, options: .caseInsensitive)

                

                if NameFilterRange.location != NSNotFound {
                    item.attributedItemName = NSMutableAttributedString(string: item.iteme_search)
//                    item.attributedItemID = NSMutableAttributedString(string: item.item_id)

                    item.attributedItemName!.setAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: NameFilterRange)
////                    if IDFilterRange.location != NSNotFound {
////                        item.attributedItemID!.setAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: IDFilterRange)
////                    }
                    self.resultsList.append(item)

                }
//
                
            }
            self.tableView!.reloadData()
            
        }
        
        DispatchQueue.main.async {
            
            for i in 0 ..< self.dataListSenf.count {
                
                let item = SearchItem(iteme_name: self.dataListSenf[i].trackerName,item_id: String(self.dataListSenf[i].id), item_flag: "senf", caolris: self.dataListSenf[i].wajbehCaloriesTotal!,iteme_search: self.dataListSenf[i].searchKeyword)
                
                let NameFilterRange = (item.iteme_search as NSString).range(of: self.text!, options: .caseInsensitive)
//                let IDFilterRange = (item.item_id as NSString).range(of: self.text!, options: .caseInsensitive)
                
                if NameFilterRange.location != NSNotFound {
                    item.attributedItemName = NSMutableAttributedString(string: item.iteme_search)
//                    item.attributedItemID = NSMutableAttributedString(string: item.item_id)
                    
                    item.attributedItemName!.setAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: NameFilterRange)
//                    if IDFilterRange.location != NSNotFound {
//                        item.attributedItemID!.setAttributes([.font: UIFont.boldSystemFont(ofSize: 17)], range: IDFilterRange)
//                    }
                    self.resultsList.append(item)
                    
                    
                }
            }
            self.tableView?.reloadData()
            
        }
    }
    
    
}

 
extension CustomTextField: UITableViewDelegate, UITableViewDataSource {
    
    
    //////////////////////////////////////////////////////////////////////////////
    // Table View related methods
    //////////////////////////////////////////////////////////////////////////////
    
    
    // MARK: TableView creation and updating
    
    // Create SearchTableview
    func buildSearchTableView() {
        
        if let tableView = tableView {
            tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CustomSearchTextFieldCell")
            tableView.delegate = self
            tableView.dataSource = self
            self.window?.addSubview(tableView)
            
        } else {
            //addData()
            print("tableView created")
            tableView = UITableView(frame: CGRect.zero)
        }
        
        updateSearchTableView()
    }
    
    // Updating SearchtableView
    func updateSearchTableView() {
        
        if let tableView = tableView {
            DispatchQueue.main.async {
                self.superview?.bringSubviewToFront(tableView)
                var tableHeight: CGFloat = 0
                tableHeight = tableView.contentSize.height
                
                // Set a bottom margin of 10p
                if tableHeight < tableView.contentSize.height {
                    tableHeight -= 10
                }
                
                // Set tableView frame
                var tableViewFrame = CGRect(x: 0, y: 0, width: self.frame.size.width - 4, height: tableHeight)
                tableViewFrame.origin = self.convert(tableViewFrame.origin, to: nil)
                tableViewFrame.origin.x += 2
                tableViewFrame.origin.y += self.frame.size.height + 2
                UIView.animate(withDuration: 0.2, animations: { [weak self] in
                    self?.tableView?.frame = tableViewFrame
                })
                
                //Setting tableView style
                tableView.layer.masksToBounds = true
                tableView.separatorInset = UIEdgeInsets.zero
                tableView.layer.cornerRadius = 5.0
                tableView.separatorColor = UIColor.lightGray
                tableView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
                
                if self.isFirstResponder {
                    self.superview?.bringSubviewToFront(self)
                }
                
            }
        }
        tableView!.reloadData()
        
    }
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(resultsList.count)
        return resultsList.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
        //            .dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as UITableViewCell
        
        //let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as UITableViewCell
        cell.nameSanf.text = resultsList[indexPath.row].getStringText()
        cell.caloris.text =  resultsList[indexPath.row].getStringTextcaloris()
        cell.catrgory.text =  resultsList[indexPath.row].getStringTextCate()

        cell.imageSenf.image = UIImage(named : "logo")
        SetImage(laImage:cell.imageSenf)
//        cell.backgroundColor = UIColor(red: 98/255, green: 230/255, blue: 155/255, alpha: 1.0)
        //        cell.textLabel!.semanticContentAttribute = .forceRightToLeft
        //        cell.textLabel!.textColor = UIColor.white
        //        cell.textLabel?.attributedText = resultsList[indexPath.row].getFormatedText()
        return cell
    }
    func SetImage(laImage:UIImageView){
        laImage.layer.borderWidth = 1
        laImage.layer.masksToBounds = false
        laImage.layer.borderColor = UIColor.black.cgColor
        laImage.layer.cornerRadius = laImage.frame.height/2
        laImage.clipsToBounds = true
        
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(resultsList[indexPath.row].item_id)")
        self.text = resultsList[indexPath.row].iteme_name
        obShowNewWajbeh?.goToViewWajbeh(idWajbeh: resultsList[indexPath.row].item_id,flag:resultsList[indexPath.row].item_flag)
        tableView.isHidden = true
        self.endEditing(true)
    }
    
    
    
}
 
extension CustomTextField:SearchViewResulte{
    func setArraySenf(dataList: [tblSenf]) {
        self.dataListSenf = dataList
    }
    
    func setArrayWajbeh(dataList: [tblPackeg]) {
      
       if  self.dataList.isEmpty == true {
              self.dataList = dataList
       }else{
        self.dataList.append(contentsOf: dataList)
        }
    }
    
    
}


/*
 //
 //  AddEditViewController.swift
 //  Ta5sees
 //
 //  Created by Admin on 5/29/1398 AP.
 //  Copyright © 1398 Telecom enterprise. All rights reserved.
 //


 protocol ShowNewWajbeh {
     func goToViewWajbeh(idWajbeh:String,flag:String)
 }

 import UIKit
 import RealmSwift
 class AddEditCategoryController: UIViewController ,UISearchBarDelegate {
     
     
     var refWInfo:Int!
     var refWCAtegory:Int!
     var arrPassData:[Int] = []
     let realm = try! Realm()
     var obShowNewWajbeh:ShowNewWajbeh?
     var id_items:String!
     var date:String!
     var pg:protocolWajbehInfoTracker?
     var filteredData: [tblPackeg]!
     var filteredDataSenf: [tblSenf]!
     
     let list =  tblPackeg.searchModelArray()
     let listSenf =  tblSenf.searchModelArray()
     var lissPrioritySenf = [tblSenf]()
     var filterItem = [SearchItem]()
     let tableView = UITableView()
     var safeArea: UILayoutGuide!

     @IBOutlet weak var titlePage: UILabel!
     @IBAction func buDismiss(_ sender: Any) {
         dismiss(animated: true, completion: nil)
     }
     
     @IBAction func favourites(_ sender: Any) {
         self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")

     }
     @IBAction func addANDEdite(_ sender: Any) {
         self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
         
     }
     @IBAction func recentlyUsed(_ sender: Any) {
 //
     self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
     }
     
     @IBOutlet weak var  searchBar: UISearchBar!
     
     
     
     //    @IBAction func btndelet(_ sender: Any) {
     //        let ob = realm.objects(tblWajbehDetailing.self).filter("id == %@",1)
     //        try! realm.write {
     //            realm.delete(ob)
     //        }
     //    }
     //    @IBAction func edite(_ sender: Any) {
     //        self.performSegue(withIdentifier: "mainmenus", sender: [1,3])
     //
     //    }
     //    @IBOutlet weak var search: UISearchBar!
     override func viewDidLoad() {
         super.viewDidLoad()
         //        txtSearchOutlet.obShowNewWajbeh = self
         obShowNewWajbeh = self
              tableView.dataSource = self
                 tableView.delegate = self
         
         searchBar.delegate = self
         setTilePage(id:id_items)
 //        setTilePage(id:"1")
         
     }
     
     
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "FavuriteRecentController" {
             if let dis=segue.destination as?  FavuriteRecentController{
                 if  sender != nil  {
                     dis.id_items = "-1"
                     dis.date = date
                     dis.pg = pg
                 }
             }
         }else   if segue.identifier == "DisplayCateItemsController" {
             if let dis=segue.destination as?  DisplayCateItemsController{
                 if  let data=sender as? String {
                     dis.idPackge = Int(data)
                     dis.id_items = id_items
                     dis.date = date
                     dis.pg = pg
                     if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                         dis.isFavrite = true
                     }else{
                         dis.isFavrite = false
                     }
                     tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                         dis.idItemsEaten = id
                         if bool == true {
                             dis.isEaten = true
                             
                         }
                         else{
                         dis.isEaten = false
                         }
                     })
                 }
             }
         }else if segue.identifier == "DisplayCateItemsSenfController" {
             if let dis=segue.destination as?  DisplayCateItemsSenfController{
                 if  let data=sender as? String {
                     dis.idPackge = Int(data)
                     dis.id_items = id_items
                     dis.date = date
                     dis.pg = pg
                     if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                         dis.isFavrite = true
                     }else{
                         dis.isFavrite = false
                     }
                     
                      tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, completion: { (id,bool) in
                           dis.idItemsEaten = id
                         if bool == true {
                             dis.isEaten = true
                         }else{
                         dis.isEaten = false
                         }
                         })
                 }
             }
         }else if segue.identifier == "RecentUsedItemController" {
              if let dis=segue.destination as?  RecentUsedItemController{
                         if  sender != nil  {
                             dis.id_items = "-1"
                             dis.date = date
                             dis.pg = pg
                         }
                     }
         }
         
     }
     
     
     func setTilePage(id:String){

         switch id {
         case "1":
             titlePage.text! = "إضافة فطور"
             break
         case "2":
             titlePage.text! = "إضافة غداء"
             break
         case "3":
             titlePage.text! = "إضافة عشاء"
             break
         case "4":
             titlePage.text! = "إضافة تصبيرة ١"
             break
         case "5":
             titlePage.text! = "إضافة تصبيرة ٢"
             break
         default:
             print("not found !!")
         }
     }
     
     
     func getUserInWithThe( userIn:String)->String {
         var polyUserIn = userIn;
         if userIn.count >= 3{
             if (userIn.hasPrefix("ال")) {
                 polyUserIn = String(userIn.dropFirst(2))
             }else{
                 polyUserIn = "ال\(userIn)";
             }
         }
         return polyUserIn;
     }
     func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
         let components = searchKey.components(separatedBy: "،")
         for item in components {
             if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
                 return true
             }
         }
         return false
     }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
 //        buildSearchTableView()
 //        tableView?.removeFromSuperview()
         
     }
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         if searchText.isEmpty == true {
             tableView.isHidden = true
             tableView.removeFromSuperview()
         }else{
         setupTableView()
               safeArea = view.layoutMarginsGuide
         var lissPrioritySenf = [tblSenf]()
         filterItem.removeAll()
         filteredDataSenf = searchText.isEmpty ? listSenf : listSenf.filter { (item: tblSenf) -> Bool in
             
             ////            let whitespace = NSCharacterSet.whitespaces
             let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
             //.self.rangeOfCharacter(from: whitespace)
             
             if range.contains(" ") {
                 let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                 var counter = 0
                 for text in input {
                     if confirmSearchKey(userINp:text,searchKey:item.searchKeyword,nameMel:item.trackerName) {
                         counter+=1
                     }
                     
                 }
                 if counter == input.count {
                     return true
                 }else{
                     for index in input.reversed(){
                         if confirmSearchKey(userINp:index,searchKey:item.searchKeyword,nameMel:item.trackerName) {
                             
                             return true
                         }else{
                             lissPrioritySenf.append(item)
                             
                         }
                     }
                 }
             }else{
                 if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKeyword,nameMel:item.trackerName) {
                     return true
                 }
             }
             
             
             //
             
             
             
             return false
             
             
         }
         
         if lissPrioritySenf.count > 0 {
             filteredDataSenf.append(contentsOf: lissPrioritySenf)
             filteredDataSenf = filteredDataSenf.removeDuplicates()
             lissPrioritySenf.removeAll()
         }
         for item in filteredDataSenf {
             filterItem.append(SearchItem(iteme_name: item.trackerName,item_id: String(item.id), item_flag: "senf", caolris: item.wajbehCaloriesTotal!,iteme_search: item.searchKeyword))
         }
         
         
         
         
         var lissPriority = [tblPackeg]()
         
         filteredData = searchText.isEmpty ? list : list.filter { (item: tblPackeg) -> Bool in
             
             ////            let whitespace = NSCharacterSet.whitespaces
             let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
             //.self.rangeOfCharacter(from: whitespace)
             
             if range.contains(" ") {
                 let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                 var counter = 0
                 for text in input {
                     if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
                         counter+=1
                     }
                     
                 }
                 if counter == input.count {
                     return true
                 }else{
                     for index in input.reversed(){
                         if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
                             
                             return true
                         }else{
                             lissPriority.append(item)
                             
                         }
                     }
                 }
             }else{
                 if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
                     return true
                 }
             }
             
             
             //
             
             
             
             return false
             
             
         }
         
         if lissPriority.count > 0 {
             filteredData.append(contentsOf: lissPriority)
             filteredData = filteredData.removeDuplicates()
             lissPriority.removeAll()
         }
         
         for item in filteredData {
             filterItem.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!))
         }
         
         
         var sortedResultSenf = [SearchItem]()
         var lissPrioritys = [SearchItem]()
         
         for resultsSenf in filterItem {
             let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
             
             if range.contains(" ") {
                 let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
                 var counter = 0
                 
                 for text in constraintKeys {
                     if confirmSearchKey(userINp:text,searchKey:resultsSenf.iteme_search,nameMel:resultsSenf.iteme_name) {
                         counter+=1
                     }
                     
                 }
                 if counter == constraintKeys.count {
                     sortedResultSenf.append(resultsSenf)
                 }else{
                     for index in constraintKeys.reversed(){
                         if matchesKey(constraint: index, searchKey: resultsSenf.iteme_search) == true {
                             sortedResultSenf.append(resultsSenf)
                             
                         }else {
                             lissPrioritys.append(resultsSenf)
                         }
                     }
                 }
             }else if matchesKey(constraint: searchText, searchKey: resultsSenf.iteme_search) == true {
                 sortedResultSenf.append(resultsSenf)
                 
                 
             }else{
                 lissPrioritys.append(resultsSenf)
             }
             
             
         }
         if lissPrioritys.count > 0 {
             sortedResultSenf.append(contentsOf: lissPrioritys)
         }
         filterItem = sortedResultSenf
         //
         /////////////
         
         
         tableView.reloadData()
         self.tableView.isHidden = false

                  }
     }
     
     
     
     func matchesKey(constraint:String,searchKey:String)->Bool{
         if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
             return true
         }
         
         return false
     }
     
     
     
     func setupTableView() {
         view.addSubview(tableView)
         tableView.translatesAutoresizingMaskIntoConstraints = false
       tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
       tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
       tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
       tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
         
 //         tableView.register(UITableViewCell.self, forCellReuseIdentifier: "customCellSearch")
     }
     
     override func loadView() {
       super.loadView()
     
         searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
         searchBar.searchTextField.textAlignment = .right
         self.tableView.isHidden = true

     }

     
 //    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
 //
 //        let style = NSMutableParagraphStyle()
 //                  style.alignment = NSTextAlignment.right
 //        self.searchBar.searchTextField.textAlignment = .right
 //        self.searchBar.searchTextField.attributedPlaceholder =  NSAttributedString(string: "ابحث هنا", attributes: [NSAttributedString.Key.paragraphStyle:style])
 //
 //
 //    }
     
     
 //    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
 //        searchBar.showsCancelButton = false
 //        searchBar.text = ""
 //        searchBar.resignFirstResponder()
 //        tableView.isHidden = true
 //        tableView.removeFromSuperview()
 //    }
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
                 searchBar.resignFirstResponder()

     }
     
     
     
 }




 extension AddEditCategoryController:ShowNewWajbeh{
     func goToViewWajbeh(idWajbeh: String, flag: String) {
         
         if flag == "wajbeh"{
             self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
         }else {
             self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
         }
         //        txtSearchOutlet.text = nil
         
     }
     
     
     
 }


 extension AddEditCategoryController: UITableViewDelegate, UITableViewDataSource {
     
     
     //////////////////////////////////////////////////////////////////////////////
     // Table View related methods
     //////////////////////////////////////////////////////////////////////////////
     
     
     // MARK: TableView creation and updating
     
     // Create SearchTableview
     
     
     
     // MARK: TableViewDataSource methods
     public func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }
     
     public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return filterItem.count
     }
     
     // MARK: TableViewDelegate methods
     
     //Adding rows in the tableview with the data from dataList
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
         //            .dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as UITableViewCell
         
         //let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSearchTextFieldCell", for: indexPath) as UITableViewCell
 //        setFont(lbl: cell.nameSanf,size: 17)
 //        setFont(lbl: cell.caloris,size: 14)
 //        setFont(lbl: cell.catrgory,size: 12)

         cell.nameSanf.text = filterItem[indexPath.row].getStringText()
         cell.caloris.text =  filterItem[indexPath.row].getStringTextcaloris()
         cell.catrgory.text =  filterItem[indexPath.row].getStringTextCate()
         
         cell.imageSenf.image = UIImage(named : "logo")
         SetImage(laImage:cell.imageSenf)
         //        cell.backgroundColor = UIColor(red: 98/255, green: 230/255, blue: 155/255, alpha: 1.0)
         //        cell.textLabel!.semanticContentAttribute = .forceRightToLeft
         //        cell.textLabel!.textColor = UIColor.white
         //        cell.textLabel?.attributedText = resultsList[indexPath.row].getFormatedText()
         return cell
     }
 //    func setFont(lbl:UILabel,size:CGFloat){
 //        lbl.font = UIFont(name: "GE Dinar One",size:size)
 //        lbl.lineBreakMode = .byWordWrapping
 //        lbl.numberOfLines = 10
 //    }
     func SetImage(laImage:UIImageView){
         laImage.layer.borderWidth = 1
         laImage.layer.masksToBounds = false
         laImage.layer.borderColor = colorGray.cgColor
         laImage.layer.cornerRadius = laImage.frame.height/2
         laImage.clipsToBounds = true
         
     }
     public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("selected row \(filterItem[indexPath.row].item_id)")
 //                self.text = filterItem[indexPath.row].iteme_name
                 obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filterItem[indexPath.row].item_id,flag:filterItem[indexPath.row].item_flag)
         tableView.isHidden = true
         
     }
     
     
     
 }


 /*
  //
  //  TestSearch.swift
  //  Ta5sees
  //
  //  Created by Admin on 9/10/20.
  //  Copyright © 2020 Telecom enterprise. All rights reserved.
  //
  
  import UIKit
  
  class TestSearch: UIViewController , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  return resultsList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
  cell.textLabel?.text = resultsList[indexPath.row].getStringText()
  return cell
  }
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var tableView: UITableView!
  var filteredData: [tblPackeg]!
  var resultsList : [SearchItem] = [SearchItem]()
  var resultsListLiss : [SearchItem] = [SearchItem]()
  
  let list =  tblPackeg.searchModelArray()
  
  override func viewDidLoad() {
  super.viewDidLoad()
  tableView.dataSource = self
  searchBar.delegate = self
  filteredData = list
  print("RRRRRRR\("DFSDASDBMNMF ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("ASDF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).rawValue)")
  print("RRRRRRR\("ASDF".compare("DF", options: NSString.CompareOptions.caseInsensitive).hashValue)")
  }
  
  func getUserInWithThe( userIn:String)->String {
  var polyUserIn = userIn;
  if userIn.count >= 3{
  if (userIn.hasPrefix("ال")) {
  polyUserIn = String(userIn.dropFirst(2))
  }else{
  polyUserIn = "ال\(userIn)";
  }
  }
  return polyUserIn;
  }
  func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
 let components = searchKey.components(separatedBy: "،")
 for item in components {
 if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
 return true
 }
 }
 return false
 }
 
 func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
 var lissPriority = [tblPackeg]()
 resultsList.removeAll()
 filteredData = searchText.isEmpty ? list : list.filter { (item: tblPackeg) -> Bool in
 
 ////            let whitespace = NSCharacterSet.whitespaces
 let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
 //.self.rangeOfCharacter(from: whitespace)
 
 if range.contains(" ") {
 let input =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
 var counter = 0
 for text in input {
 if confirmSearchKey(userINp:text,searchKey:item.searchKey!,nameMel:item.name!) {
 counter+=1
 }
 
 }
 if counter == input.count {
 resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
 return true
 }else{
 for index in input.reversed(){
 if confirmSearchKey(userINp:index,searchKey:item.searchKey!,nameMel:item.name!) {
 resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
 return true
 }else{
 lissPriority.append(item)
 resultsListLiss.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
 
 }
 }
 }
 }else{
 if confirmSearchKey(userINp:searchText.trimmingCharacters(in: .whitespaces).lowercased(),searchKey:item.searchKey!,nameMel:item.name!) {
 resultsList.append(SearchItem(iteme_name: item.name!,item_id: String(item.id), item_flag: "wajbeh",caolris: item.calories!,iteme_search: item.searchKey!) )
 return true
 }
 }
 
  
  //
  
  
  
  return false
  
  
  }
  
  
  if lissPriority.count > 0 {
  filteredData.append(contentsOf: lissPriority)
  filteredData = filteredData.removeDuplicates()
  lissPriority.removeAll()
  }
  
  if resultsListLiss.count > 0 {
  resultsList.append(contentsOf: resultsListLiss)
  resultsList = resultsList.removeDuplicates()
  resultsListLiss.removeAll()
  }
  //        var sortedResult = [tblPackeg]();
  var sortedResult = [SearchItem]();
  if sortedResult.count > 0 {
  sortedResult.removeAll()
  }
  
  for results in resultsList {
  let range = searchText.trimmingCharacters(in: .whitespaces).lowercased()
  
  if range.contains(" ") {
  let  constraintKeys =  searchText.trimmingCharacters(in: .whitespaces).lowercased().components(separatedBy: " ")
  var counter = 0
  
  for text in constraintKeys {
  if confirmSearchKey(userINp:text,searchKey:results.iteme_search,nameMel:results.iteme_name) {
  counter+=1
  }
  
  }
  if counter == constraintKeys.count {
  sortedResult.append(results)
  //                    sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else{
  for index in constraintKeys.reversed(){
  if matchesKey(constraint: index, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //                              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }else {
  //                            lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  }
  }
  }else if matchesKey(constraint: searchText, searchKey: results.iteme_search) == true {
  sortedResult.append(results)
  //              sortedResult.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  
  }else{
  //                lissPriority.append(results)
  resultsListLiss.append(SearchItem(iteme_name: results.iteme_name,item_id: String(results.item_id), item_flag: "wajbeh",caolris: results.caolris,iteme_search: results.iteme_search) )
  }
  
  
  }
  if resultsListLiss.count > 0 {
  sortedResult.append(contentsOf: resultsListLiss)
  }
  //        if lissPriority.count > 0 {
  //            sortedResult.append(contentsOf: lissPriority)
  //        }
  resultsList.removeAll()
  resultsList = sortedResult
  
  tableView.reloadData()
  }
  
  
  
  func matchesKey(constraint:String,searchKey:String)->Bool{
  if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
  return true
  }
  
  return false
  }
  
  
  
  func binarySearch( arr:[String],firstIndex:Int, lastIndex:Int, key:String)->Int
  {
  if (lastIndex >= firstIndex) {
  let mid = firstIndex + (lastIndex - firstIndex) / 2;
  
  //arr[mid].compare(key, options: NSString.CompareOptions.caseInsensitive).rawValue)
  if arr[mid] == key {
  return mid;
  
  }
  
  if arr[mid] > key {
  return binarySearch(arr: arr, firstIndex: firstIndex, lastIndex: mid - 1, key: key)
  }
  
  return binarySearch(arr: arr, firstIndex: mid + 1, lastIndex: lastIndex, key: key)
  }
  
  
  return -1;
  }
  
  
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
  self.searchBar.showsCancelButton = true
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
  searchBar.showsCancelButton = false
  searchBar.text = ""
  searchBar.resignFirstResponder()
  }
  }
  
  struct SenfPackage {
  var tilie = ""
  var name = ""
  var id = ""
  var isWajbeh = ""
  var searchKey = ""
  //    init(tilie:String,) {
  //        statements
  //    }
  }
  
  
  //   let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                print(input)
  //                for text in input {
  //                    if text != "" {
  //                        return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                    }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let components = item.searchKey!.components(separatedBy: "،")
  //            //            print(components.joined(separator: " "))
  //
  //            let whitespace = NSCharacterSet.whitespaces
  //            let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  //
  //            if range != nil {
  //                let input =  searchText.lowercased().components(separatedBy: " ")
  //                          print(input)
  //                for text in input {
  //                    if text != "" {
  //                   return   components.joined(separator: "").lowercased().contains(text.lowercased())
  //                }
  //                }
  //            }else{
  //                return  components.joined(separator: " ").lowercased().contains(searchText.lowercased())
  //            }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  
  /*
  filteredData.removeAll()
  for item in list {
  let components = item.searchKey!.components(separatedBy: "،")
  //            print(components.joined(separator: " "))
  
  let whitespace = NSCharacterSet.whitespaces
  let range = searchText.lowercased().self.rangeOfCharacter(from: whitespace)
  let input =  searchText.lowercased().components(separatedBy: " ")
  if range != nil {
  
  for text in input {
  if   components.joined(separator: "").lowercased().contains(text.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: text, isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: text, isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }else{
  if   components.joined(separator: " ").lowercased().contains(searchText.lowercased()) || components.joined(separator: " ").lowercased().containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || components.joined(separator: " ").lowercased().hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false) == true {
  filteredData.append(item)
  tableView.reloadData()
  
  }
  }
  }
  
  //            let r = components.filter { (str) -> Bool in
  //                print(str)
  //              return str.containsSubString(theSubString:searchText.lowercased(), isCaseSensitive: false) || str.hasPrefixCheck(prefix: searchText.lowercased(), isCaseSensitive: false) || str.hasSuffixCheck(suffix: searchText.lowercased(), isCaseSensitive: false)
  ////                 str.hasPrefix(searchText.lowercased())
  //            }
  
  
  //                components.joined(separator: " ").lowercased().starts(with:  searchText.lowercased())
  //                    || components.joined(separator: " ").lowercased().contains(searchText.lowercased()) ||
  //                    components.joined(separator: " ").lowercased().range(of: searchText.lowercased(), options: NSString.CompareOptions.caseInsensitive) != nil
  
  //components.forEach { item in  item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil; return true }
  //                components.joined(separator: " ").range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
  //
  //                ||
  
  //            return false
  //
  //
  //        }
  
  }
  
  */
  
  
  
  
  */

 */
