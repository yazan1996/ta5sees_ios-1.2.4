//
//  MultiSeletetionView.swift
//  Ta5sees
//
//  Created by Admin on 3/8/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

class MultiSeletetionView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    lazy var obviewMultiSelectPresnter = viewMultiSelectPresnter(with: self)
    var items = [Item]()
    var section = [Section]()
    var title_Navigation:String!
    var obselectionDelegate:selectionDelegate!
    var optionsSelected:String!
    var flag:String! // flag 1 >> cusine / flag 2 >> allergy
    var index:Int!
    var seletAll = 0
    @IBOutlet weak var navigation: NavigationViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        obviewMultiSelectPresnter.getData(flag:flag)
        navigation.topItem!.title = title_Navigation
        tblview.register(MyCustomHeader.self,
              forHeaderFooterViewReuseIdentifier: "sectionHeader")
        let filtered = items.map{$0.name}.intersection(with:optionsSelected.split(separator: ",").map { String($0) })
        
       // items.filter { filtered.map { String($0) }.contains($0.name)}
        _ = items.filter { (Item) -> Bool in
            Item.selected = filtered.map { String($0) }.contains(Item.name)
            return true
        }
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if flag == "1" {
        return 40
        }
        return 0
    }
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
       
        
        if flag == "1" {
            
            let viewHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                "sectionHeader") as! MyCustomHeader
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            viewHeader.addGestureRecognizer(tapRecognizer)
            viewHeader.backgroundColor = .white
            viewHeader.backgroundView?.backgroundColor = .white
            viewHeader.tintColor = .white
            if seletAll == 1 {
                viewHeader.title.text = "تحديد الكل"
            }else{
                viewHeader.title.text = "إلغاء تحديد الكل"
            }
            return viewHeader
        }
        return nil
    }
    
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer)
    {
        if seletAll == 1 {
            showToast(message: "تم تحديد الكل", view: view,place:0)
            setUNSeletd(falg:true)
            seletAll = 0
        }else{
            showToast(message: "تم إالغاء تحديد الكل", view: view,place:0)
            setUNSeletd(falg:false)
            seletAll = 1
        }
        tblview.reloadData()
    }
    func setUNSeletd(falg:Bool){
        for item in items {
            item.selected = falg
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "multiSelect")!
        let item = section[indexPath.section].items[indexPath.row]
        if section[indexPath.section].isMultiple {
            if item.name == "لا شيء" {
                index = indexPath.row
            }else{
                
            }
            //For multiple selection
            if item.selected {
                cell.accessoryType = .checkmark
            }
            else {
                cell.accessoryType = .none
            }
        }
        else {
            //For single selection
            if item.selected {
                cell.accessoryType = .checkmark
            }
            else {
                cell.accessoryType = .none
            }
        }
        cell.textLabel?.text = section[indexPath.section].items[indexPath.row].name
        cell.textLabel?.textAlignment = .right
        cell.textLabel?.font = UIFont(name: "GE Dinar One",size: 17)
        cell.textLabel?.textColor = colorGray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if section[indexPath.section].isMultiple {
            let item = section[indexPath.section].items[indexPath.row]
            if item.name == "لا شيء" && item.selected == false {

                index = indexPath.row
                _ = items.map { (item_) -> Void in
                    if items[index].name == item_.name {
                        item_.selected = true
                    }else{
                        item_.selected = false
                    }
                }
                tblview.reloadData()
            } else if item.name != "لا شيء" && item.selected == false {
                if index != nil {
                items[index].selected = false
                }
                item.selected = true
                tblview.reloadData()
            }else{
                //For multiple selection
                item.selected = !item.selected
                
            }
            section[indexPath.section].items[indexPath.row] = item
            tblview.reloadRows(at: [indexPath], with: .automatic)
        }
        else {
            //For multiple selection
            let items = section[indexPath.section].items
            
            if let selectedItemIndex = items.indices.first(where: { items[$0].selected }) {
                section[indexPath.section].items[selectedItemIndex].selected = false
                if selectedItemIndex != indexPath.row {
                    section[indexPath.section].items[indexPath.row].selected = true
                }
            }
            else {
                section[indexPath.section].items[indexPath.row].selected = true
            }
            tblview.reloadSections([indexPath.section], with: .automatic)
        }
    }
    
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBAction func btn_Save(_ sender: Any) {
        let str = items.filter {$0.selected == true}
        if flag == "1" {
            obselectionDelegate.setSeletedCusine(str:str.map{$0.name}.joined(separator: ","),strID: str.map{$0.id}.joined(separator: ","))
        }else if flag == "2"{
            obselectionDelegate.setSeletedallaegy(str:str.map{$0.name}.joined(separator: ","), strID: str.map{$0.id}.joined(separator: ","))
        }else if flag == "3" {
            obselectionDelegate.setSeletedLayaqa(str:str.map{$0.name}.joined(separator: ","), strID: str.map{$0.id}.joined(separator: ","))
        }else if flag == "4"{
            obselectionDelegate.setDiabetsType(str:str.map{$0.name}.joined(separator: ","), typeDiabites: str.map{$0.id}.joined(separator: ","))
        }else if flag == "5"{
            obselectionDelegate.setMidctionDiabetsTypeOptionOne(str:str.map{$0.name}.joined(separator: ","), type: str.map{$0.id}.joined(separator: ","))
        }else if flag == "6"{
            obselectionDelegate.setMidctionDiabetsTypeOptionTwo(str:str.map{$0.name}.joined(separator: ","), type: str.map{$0.id}.joined(separator: ","))
        }else if flag == "7"{
            obselectionDelegate.setPragnentMonths(str:str.map{$0.name}.joined(separator: ","), id: str.map{$0.id}.joined(separator: ","))
        }
        
        closePage()
    }
    @IBAction func btn_close(_ sender: Any) {
        closePage()
    }
    
    func closePage(){
        dismiss(animated: true, completion: nil)
    }
}


extension MultiSeletetionView:viewMultiSelectDelegate {
    
    func getDataSection(section: [Section]) {
        self.section = section
    }
    
    func getDataArray(list: [Item]) {
        items = list
      if items.contains(where: {$0.selected == true}) {
         // it exists, do something
        NSLog("here")
        seletAll = 0
      } else {
        seletAll = 1
        NSLog("here2")
         //item could not be found
      }
    }
    
    
}




extension Collection where Element: Equatable {
    
    func intersection(with filter: [Element]) -> [Element] {
        return self.filter { element in filter.contains(element) }
    }
    
}



class MyCustomHeader: UITableViewHeaderFooterView {
    let title = UILabel()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureContents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureContents() {
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont(name: "GE Dinar One", size: 18)!
        title.backgroundColor = .white
        contentView.addSubview(title)

        // Center the image vertically and place it near the leading
        // edge of the view. Constrain its width and height to 50 points.
        NSLayoutConstraint.activate([
          
        
            // Center the label vertically, and use it to fill the remaining
            // space in the header view.
            title.heightAnchor.constraint(equalToConstant: 40),
//            title.leadingAnchor.constraint(equalTo: contentView.trailingAnchor,
//                   constant:16),
            title.trailingAnchor.constraint(equalTo:
                   contentView.layoutMarginsGuide.trailingAnchor),
            title.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}
