//
//  viewMultiSelectDelegate.swift
//  Ta5sees
//
//  Created by Admin on 3/8/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class viewMultiSelectPresnter {
    
    var view:viewMultiSelectDelegate!
    
    
    var TypeDiabitsList: [Item] = {
        let typ1 = Item(id: "1",
                        name: "سكري نوع اول")
        let typ2 = Item(id: "2",
                        name: "سكري نوع ثاني")
        let typ3 = Item(id: "3",
                        name: "سكري حوامل")
        
        
        return [typ1,
                typ2,
                typ3
        ]
    }()
    
    init(with view:viewMultiSelectDelegate){
        self.view = view
    }
    
    func getDataWithID(id:String){
        getCusine3(id: id)
    }
    func getData(flag:String){
        if flag == "1" {
            getCusine2()
        }else if flag == "2"{
            getAllergy()
        }else if flag == "3" {
            getLayaqaCondition()
        }else if flag == "4" {
            getTypeDiabites()
        }else if flag == "5"  || flag == "6" {
            getTypeMedictionDiabets()
        }else if flag == "7" {
            getPregnantMonths()
        }else if flag == "8" {
            getMeats()
        }else if flag == "9" {
            getVegetables()
        }else if flag == "10" {
            getFruits()
        }else if flag == "12" {
            getCusineWithoutKito()
        }else if flag == "13" {
            getCusine()
        }
    }
    
    func getMeats(){
        var arr = [Item]()
        //        arr.append(Item(id: "-1", name: "لا شيء"))
        arr.append(Item(id: "16", name: "اسماك"))
        arr.append(Item(id: "14", name: "لحوم حمراء"))
        arr.append(Item(id: "15", name: "دجاج"))
        
        self.view.getDataArray(list: arr )
        self.view.getDataSection(section:[Section(isMultiple: true,items: arr)])
    }
    func getVegetables(){
        
        var arr = [Item]()
        //        arr.append(Item(id: "-1", name: "لا شيء"))
        arr.append(Item(id: "10", name:"بروكلي"))
        arr.append(Item(id: "11", name:"زهرة"))
        arr.append(Item(id: "9", name:"باذنجان"))
        arr.append(Item(id: "13", name: "فطر"))
        arr.append(Item(id: "12", name: "شمندر"))
        
        self.view.getDataArray(list: arr )
        self.view.getDataSection(section:[Section(isMultiple: true,items: arr)])
    }
    func getFruits(){
        
        var arr = [Item]()
        //        arr.append(Item(id: "-1", name: "لا شيء"))
        arr.append(Item(id: "8", name:"موز"))
        arr.append(Item(id: "4", name:"توت"))
        arr.append(Item(id: "7", name:"مشمش"))
        arr.append(Item(id: "3", name: "تفاح"))
        arr.append(Item(id: "2", name: "بطيخ"))
        arr.append(Item(id: "1", name: "اناناس"))
        arr.append(Item(id: "5", name: "فراولة"))
        arr.append(Item(id: "6", name: "كيوي"))
        
        
        self.view.getDataArray(list: arr )
        self.view.getDataSection(section:[Section(isMultiple: true,items: arr)])
    }
    func getCusine(){
        tblCusisneType.showCusiene { (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: true,items: items)])
            }
        }
    }
    func getCusine2(){
        tblCusisneType.showCusiene2 { (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: true,items: items)])
            }
        }
    }
    
    func getCusine3(id:String){
        tblCusisneType.showCusiene3(id:id) { (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: true,items: items)])
            }
        }
    }
    
    func getCusineWithoutKito(){
        tblCusisneType.showCusiene1 { (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: true,items: items)])
            }
        }
    }
    func getAllergy(){
        tblAllergyInfo.showAllergy { (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: true,items: items)])
                
            }
        }
    }
    
    
    func getLayaqaCondition(){
        tblLayaqaCond.showLayaqaCondition{ (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: false,items: items)])
                
            }
        }
    }
    
    func getTypeDiabites(){
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            TypeDiabitsList.removeLast()
        }
        self.view.getDataArray(list: TypeDiabitsList )
        self.view.getDataSection(section:[Section(isMultiple: false,items: TypeDiabitsList)])
        
    }
    func getTypeMedictionDiabets(){
        tblInsulinMedicines.showMediction{ (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: false,items: items)])
                
            }
        }
    }
    func getPregnantMonths(){
        tblPregancyMonths.showPragnentMonths{ (items, error) in
            if !items.isEmpty {
                self.view.getDataArray(list: items )
                self.view.getDataSection(section:[Section(isMultiple: false,items: items)])
                
            }
        }
        
    }
}





protocol viewMultiSelectDelegate {
    func getDataArray(list:[Item])
    func getDataSection(section:[Section])
    
}




class Section {
    var isMultiple: Bool
    var items: [Item]
    
    init(isMultiple: Bool, items: [Item]) {
        self.isMultiple = isMultiple
        self.items = items
    }
}

class Item  {
    var id: String
    var name: String
    var selected: Bool = false
    
    
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(id: String, name: String, selected:Bool) {
        self.id = id
        self.name = name
        self.selected = selected
    }
}
