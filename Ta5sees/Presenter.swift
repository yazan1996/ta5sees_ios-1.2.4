//
//  Presenter.swift
//  Ta5sees
//
//  Created by Admin on 2/9/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift

class Presenter {
    
       var view:ItemsViewList
       var ListEatenBreakfast:[tblUserWajbehEaten]!
       var ListEatenLaunsh:[tblUserWajbehEaten]!
       var ListEatenDinner:[tblUserWajbehEaten]!
       var ListEatenTasbera2:[tblUserWajbehEaten]!
       var ListEatenTasbera1:[tblUserWajbehEaten]!
       var ListTamreen:[tblUserTamreen]!
       var resultelist:Results<tblUserWajbehEaten>!
       var resultelistTamreen:Results<tblUserTamreen>!


       init(with view: ItemsViewList) {
              self.view = view
        }
        
    func getWajbeh(id:Int,date:String,caloris:Int,isProposal:[String],responsEHendler:@escaping (Any?,Error?)->Void){
        let totalCaloris:Int!
        let ob = setupRealm().objects(tblUserWajbehEaten.self).filter("userID == %@ AND id == %@ AND isProposal IN %@",getDataFromSheardPreferanceString(key: "userID"), id,isProposal).first
        if caloris == -1 {
            totalCaloris = Int(ob!.kCal)!
        }else{
            totalCaloris = caloris
        }
       updateDataDaily(date: date, calorisBurned: totalCaloris, flag:"UnMark", mark: "nonSeleted") { (res, error) in
        }
        if (ob?.isEqual(nil))!{
            responsEHendler(nil,nil)
        }else{
            responsEHendler(ob,nil)
        }
    }
    
    func getTamreen(id:Int,date:String,caloris:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        let ob = setupRealm().objects(tblUserTamreen.self).filter("refUserID == %@ AND id == %@",getDataFromSheardPreferanceString(key: "userID"), id).first
       updateDataDaily(date: date, calorisBurned: caloris, flag:"UnBurned", mark: "nonSeleted") { (res, error) in
        }
        if (ob?.isEqual(nil))!{
            responsEHendler(nil,nil)
        }else{
            responsEHendler(ob,nil)
        }
    }
       
       func loadItemBreakfast(date:String){
            tblUserWajbehEaten.getItemsEaten(date: date, wajbehInfo: "1") { (response, error) in
                print(response!)
                //            if !self.ListEatenBreakfast.isEmpty{
                //                self.ListEatenBreakfast.removeAll()
                //            }
                self.resultelist = response as? Results<tblUserWajbehEaten>
                self.ListEatenBreakfast = Array(self.resultelist)
                self.view.setBreakfdast(ListEatenBreakfast: self.ListEatenBreakfast)

            }
    }
            
            func loadItemLuasnsh(date:String){

            tblUserWajbehEaten.getItemsEaten(date: date, wajbehInfo: "2") { (response, error) in
                print(response!)
                self.resultelist = response as? Results<tblUserWajbehEaten>
                self.ListEatenLaunsh = Array(self.resultelist)
                self.view.setLuansh(ListEatenLaunsh: self.ListEatenLaunsh)
            }
           }
            func loadItemDinner(date:String){

            tblUserWajbehEaten.getItemsEaten(date: date, wajbehInfo: "3") { (response, error) in
                print(response!)
     
                self.resultelist = response as? Results<tblUserWajbehEaten>
                self.ListEatenDinner = Array(self.resultelist)
                self.view.setDinner(ListEatenDinner: self.ListEatenDinner)
               }
            }
            func loadItemTasbera1(date:String){

            tblUserWajbehEaten.getItemsEaten(date:date, wajbehInfo: "4") { (response, error) in
                print(response!)
    
                self.resultelist = response as? Results<tblUserWajbehEaten>
                self.ListEatenTasbera1 = Array(self.resultelist)
                self.view.setTasber1(ListEatenTasbera1: self.ListEatenTasbera1)
            }
           }
           func loadItemTasber2(date:String){

            tblUserWajbehEaten.getItemsEaten(date: date, wajbehInfo: "5") { (response, error) in
                print(response!)
       
                self.resultelist = response as? Results<tblUserWajbehEaten>
                self.ListEatenTasbera2 = Array(self.resultelist)
                self.view.setTasber2(ListEatenTasbera2: self.ListEatenTasbera2)
            }
             
   }
    
      func loadItemTamreen(date:String){

             tblUserTamreen.getItemsBurned(date: date) { (response, error) in
                 print(response!)
        
                self.resultelistTamreen = response as? Results<tblUserTamreen>
                 self.ListTamreen = Array(self.resultelistTamreen)
                 self.view.setTamreen(ListTamreen:  self.ListTamreen)
             }
              
    }
    
    func updateDataDaily(date : String,calorisBurned:Int,flag:String,mark:String, completion: @escaping (Any?,Error?) -> Void) {
         tblDailyWajbeh.updateDataDaily(date:date, calorisBurned: calorisBurned, flag:flag)
         { (res, err) in
             if res as? String == "true"{
                 print("Save Tamreen Burned : \(res!)")
                 completion(res as? String,nil)
             }else{
                 print("Save Tamreen Burned : \(res!)")
                 completion(nil,nil)
             }
         }
     }
        
}

protocol ItemsViewList {
 
    func setBreakfdast(ListEatenBreakfast:[tblUserWajbehEaten]!)
    func setLuansh(ListEatenLaunsh:[tblUserWajbehEaten]!)
    func setDinner(ListEatenDinner:[tblUserWajbehEaten]!)
    func setTasber1(ListEatenTasbera1:[tblUserWajbehEaten]!)
    func setTasber2(ListEatenTasbera2:[tblUserWajbehEaten]!)
    func setTamreen(ListTamreen:[tblUserTamreen]!)
}
protocol ItemsAsNil {
}
