//
//  customDatePicker.swift
//  Ta5sees
//
//  Created by Admin on 3/10/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import UIKit

class customDatePicker {
    var view:DatePickerDelegate!
    
    init(with view:DatePickerDelegate) {
        self.view=view
        
    }
    
    func createUIToolBar(pickerToolbar:UIToolbar) {
        pickerToolbar.autoresizingMask = .flexibleHeight
        //customize the toolbar
        pickerToolbar.barStyle = .default
        pickerToolbar.barTintColor = colorGray
        pickerToolbar.backgroundColor = .white
        pickerToolbar.isTranslucent = false
        
        //add buttons
        let cancelButton = UIBarButtonItem(title: "إلغاء",style: .done, target: self, action:
            #selector(cancelBtnClicked(_:)))
        cancelButton.tintColor = UIColor.white
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تحديد",style: .done, target: self, action:
            #selector(doneBtnClicked(_:)))
        doneButton.tintColor = UIColor.white
        
        
        //add the items to the toolbar
        pickerToolbar.items = [cancelButton,flexSpace,doneButton]
        
    }
    
    
   @objc func cancelBtnClicked(_ button: UIBarButtonItem?) { // name this properly!
        view.cancelBtnClicked()
   }
    
    @objc func doneBtnClicked(_ button: UIBarButtonItem?) { // name this properly!
        view.doneBtnClicked()
    }
    
    func handleTap(){}
    
}



protocol DatePickerDelegate {
    func cancelBtnClicked()
    func doneBtnClicked()
}
