//
//  TryCalaulate.swift
//  Ta5sees
//
//  Created by Admin on 8/31/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseCore
import FirebaseFirestore
import Firebase
import Realm
import RealmSwift
import UIKit
import SwiftyJSON

class TryCalaulate {
    
    var tee:Float!
    var idFetnessRate = Int(getDataFromSheardPreferanceFloat(key: "refLayaqaCondID"))
    var energyTotal = 0
    let ageChild = getDataFromSheardPreferanceFloat(key: "dateInYears")
    let weight = getDataFromSheardPreferanceString(key: "txtweight")
    let gender = getDataFromSheardPreferanceString(key: "gender")
    let height = getDataFromSheardPreferanceString(key: "txtheight")
    let incrementKACL = Float(getDataFromSheardPreferanceString(key: "incrementKACL"))
    var document:DocumentSnapshot!
    let bmi:Float = getDataFromSheardPreferanceFloat(key: "bmi")

    init(doc:DocumentSnapshot) {
        document = doc
        
      
        if document.get("grown") as! String == "2" {
            calculateNormalDietChild()
            
            print("Child")
        }else{
            calculateNormalDietAdult()
            print("Adult")
        }
        
        
        var id = getUserInfo().id
        if getUserInfo().loginType == "4" {
          id = id.replacingOccurrences(of: ".", with: "_")
        }
        ref.child("UserInfoUpdate").child(id).observe(DataEventType.value, with: { (snapshot) in
          // Get user value
          let value = snapshot.value as? NSDictionary
          let latestDate = value?["latestDate"] as? String ?? "0"
            setDataInSheardPreferance(value: latestDate, key: "latestDateOpenApp")

          // ...
          }) { (error) in
            print(error.localizedDescription)
        }
        
//        tblAlermICateItems.setalertNotification(hour:9,min:0,cateItem:"1", isActive: "1") // 0 active // 1 not active
//        tblAlermICateItems.setalertNotification(hour:16,min:0,cateItem:"2", isActive: "1")
//        tblAlermICateItems.setalertNotification(hour:21,min:0,cateItem:"3", isActive: "1")
//        tblAlermICateItems.setalertNotification(hour:12,min:0,cateItem:"4", isActive: "1")
//        tblAlermICateItems.setalertNotification(hour:19,min:0,cateItem:"5", isActive: "1")
//        tblAlermICateItems.setalertNotification(hour:19,min:30,cateItem:"6", isActive: "1")//tamreen
//        tblAlermICateItems.setalertNotification(hour:15,min:0,cateItem:"7", isActive: "1")//water
//        tblAlermICateItems.setalertNotification(hour:20,min:0,cateItem:"8", isActive: "0")//weight
        
        
//        setUpNotification(flagNotfy: false)

    }
    
    func calculateNormalDietAdult(){
        var bmr:Float
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            
            bmr = Float(24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }  else{
            bmr = Float(0.9 * 24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }
        
        let pa:Float = Float(getRatePA(id:idFetnessRate)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        
        energyTotal = processDietOrderForTEEAdulte(tee:tee, incrementKACL: incrementKACL!)
        
        
        setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
        setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
        setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(getDataFromSheardPreferanceString(key: "dietType"))!)
        
    }
    
    func calculateNormalDietChild(){
     
        if ageChild >= 1.0 && ageChild <= 3.0{
            print("****0")
            let sub1 = (89.0 * (weight as NSString).floatValue - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= 4.0 && ageChild <= 8.0 && gender == "1" {
            print("****1")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= 4.0 && ageChild <= 8.0 && ageChild <= tblChildAges.range2EndTo(id:2)  && gender == "2" {
            print("****2")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= 9  && ageChild <= 16 && gender == "1" {
            print("****3")
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
            
        }else if ageChild >= 9  && ageChild <= 16 && ageChild <= tblChildAges.range3EndTo(id:3)  && gender == "2"{
            print("****4")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }else {
            print("****5")
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
        }
        
        
        energyTotal = processDietOrderForTEEChild(tee:tee)
        
        
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        getTeeDistribution(energy:energyTotal)
        getservMenuPlan(id:Int(getDataFromSheardPreferanceString(key: "dietType"))!)
        
        
        
    }
    
    
    
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let age = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let sum1 = v2 * age
        let sum2 = (v3 * (weight as NSString).floatValue)
        let sum5 = (((height as NSString).floatValue/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(getRatePA(id:idFetnessRate)) * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        print(sum4)
        return sum4
    }
    
    
    
    
    func processDietOrderForTEEAdulte(tee:Float,incrementKACL:Float)->Int{
        if getDataFromSheardPreferanceString(key: "dietType") == "2"  {
            return Int(round(tee - incrementKACL))
        }else if getDataFromSheardPreferanceString(key: "dietType") == "1"   {
            return Int(round(tee + incrementKACL))
        }else if getDataFromSheardPreferanceString(key: "dietType") == "3"   {
            return Int(round(tee))
        }
        //        else if getDataFromSheardPreferanceString(key: "subDiet") == "loss" {
        //            print(Int(round(tee - incrementKACL)))
        //            return Int(round(tee - incrementKACL))
        //        }else if getDataFromSheardPreferanceString(key: "subDiet") == "gain" {
        //            return Int(round(tee + incrementKACL))
        //
        //        }else if getDataFromSheardPreferanceString(key: "subDiet") == "maintain" {
        //            return Int(round(tee))
        //
        //        }
        return 1
    }
    
    
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = getDataFromSheardPreferanceFloat(key: "dateInYears")
        let bmi = getDataFromSheardPreferanceFloat(key: "bmi")
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: Int(oldyear), bmi: bmi ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value <= 25 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
        //        else if getDataFromSheardPreferanceString(key: "subDiet") == "loss" {
        //            return Int(round(tee - 500))
        //        }else if getDataFromSheardPreferanceString(key: "subDiet") == "gain" {
        //            return Int(round(tee + 500))
        //        }else if getDataFromSheardPreferanceString(key: "subDiet") == "maintain" {
        //            return Int(round(tee))
        //        }
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = String(Int(getDataFromSheardPreferanceFloat(key: "gender")))
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  {
            return Float(ob.PAadult)
        } else{
            if gender == "1"  {
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
        }
    }
    
    
    func getservMenuPlan(id:Int){
        
        var ob = tblMenuPlanExchangeSystem()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }
        
        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        
//        _ = CalculationServing(malik: m, fruit: f, vg: v, fat: Int(getDataFromSheardPreferanceString(key: "fat"))!, pro: Int(getDataFromSheardPreferanceString(key: "pro"))!, carb: Int(getDataFromSheardPreferanceString(key: "carbo"))!, energy: energy,dietType:getDataFromSheardPreferanceString(key: "dietType"), FBG: getDataFromSheardPreferanceFloat(key: "FBG"))
        
        tblUserWaterDrinked.setWaterDrink(iduser: document.get("id") as! String,amountDrinked: 0,amountDefualt: 8)
        let pro = String(Int(round(getDataFromSheardPreferanceFloat(key: "newpro"))))
        let fat = String(Int(round(getDataFromSheardPreferanceFloat(key: "newfat"))))
        let carb = String(Int(round(getDataFromSheardPreferanceFloat(key: "newcarbo"))))
        let energy = String(Int(round(getDataFromSheardPreferanceFloat(key: "newenergy"))))
        
      
        
        tblUserTEEDistribution.setUserTeeDistrbution(userid: document.get("id") as! String, carbo: carb, protien: pro, fat: fat, caloris: energy, date: getDateOnly(), dateAsInt: getDateOnlyAsIntType()) { (resp,err) in
            print("tblUserTEEDistribution  save : \(resp!)")
        }
        
        
        tblDailyWajbeh.setUserTrackerDaily(userid: document.get("id") as! String, carbo:Int(carb)!,protien: Int(pro)! , fat: Int(fat)!, caloris:Int(energy)! ,burned: 0,eaten: 0, left:0,date:getDateOnly()) { (resp,err) in
            print("tblDailyWajbeh  save : \(resp!)")
        }
    
        tblUserHistory.setInsertNewWeight(iduser: document.get("id") as! String,h:String(document.get("height") as! Double),w: String(document.get("weight") as! Double),date: getDateOnly())
        
//        self.saveExchangeSystem(id:document.get("id") as! String)
        
        
        
        self.saveMealPlanerTime(iduser: document.get("id") as! String, arr:document.get("mealPlan") as! [Any])
        
        if document.get("paymentHistory") != nil {
        self.savePaymentHestory(iduser: document.get("id") as! String, arr:document.get("paymentHistory") as! [Any])
        }
        
        setDataInSheardPreferance(value: "true", key: "completeRegister")

    }
    func saveExchangeSystem(id:String){
        let item = setupRealm().objects(tblExchangeSystem.self).filter("id = %@", idUserExchangeSystem!).first!
        try! setupRealm().write {
            item.setValue(id, forKeyPath: "refUserID")
        }
    }
    
    func savePaymentHestory(iduser: String,arr:[Any]){
        
      
        if let recommends = JSON(document.get("paymentHistory")!).array {
            if recommends.isEmpty {
                return
            }
            for index in recommends {
               
                let item:tblPaymentHistory = tblPaymentHistory()
                item.id = item.IncrementaID()
                item.amount = index["amount"].string ?? "0.0"
                item.datetime = index["datetime"].string ?? "0.0"
                item.descriptionResponse = index["description"].string ?? "null"
                item.iosOrginalDateTime = index["iosOrginalDateTime"].string ?? "0.0"
                item.iosOrginalDescription = index["iosOrginalDescription"].string ?? "0.0"
                item.iosOrginalTransactionID = index["iosOrginalTransactionID"].string ?? "0.0"
                item.isFirstTimeSubscription = index["isFirstTimeSubscription"].string ?? "0.0"
                item.status = index["status"].string!
                item.subscriptionDuration = index["subscriptionDuration"].string ?? "0.0"
                item.transactionID = index["transactionID"].string ?? "0.0"
                item.product_id = index["productId"].string ?? "0.0"
                item.refUserID = iduser
                
                try! setupRealm().write{
                    setupRealm().add(item, update: Realm.UpdatePolicy.modified)
                }
            }
            
        }
        
        
        
        
        
        //
        
    }
    
    func saveMealPlanerTime(iduser: String,arr:[Any]){
        
        tblAlermICateItems.setalertNotification(hour:19,min:30,cateItem:"6", isActive: "1")
        tblAlermICateItems.setalertNotification(hour:15,min:0,cateItem:"7", isActive: "1")
        tblAlermICateItems.setalertNotification(hour:20,min:0,cateItem:"8", isActive: "1")
        
        if let recommends = JSON(document.get("mealPlan")!).array {
            for item in recommends {
                print( item["id"])
                print( item["time"] )
                
                let meal:tblMealPlanerTime = tblMealPlanerTime()
                meal.id = meal.IncrementaID()
                meal.refSenfcat = String(item["id"].intValue)
                meal.refUserId = iduser
                meal.time = item["time"].string!
                if item["selectionId"].exists() {
                    meal.flag = String(item["selectionId"].intValue)
//                if String(item["selectionId"].intValue) == "2" {
//                    meal.flag = "1"
//                } else{
//                    meal.flag = "0"
//                }
                }else{
                    meal.flag = "0"
                }
                try! setupRealm().write{
                    setupRealm().add(meal, update: Realm.UpdatePolicy.modified)
            
                }
                
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm a"
                
                let date = dateFormatter.date(from: meal.time)
     
                let hour = calendar.component(.hour, from: date ?? Date())
                let minute = calendar.component(.minute, from: date ??  Date())
            
             
               
                tblAlermICateItems.setalertNotification(hour:hour ,min:minute ,cateItem:meal.refSenfcat, isActive: "1")
            }
            
        }
        

        
        //
        
    }
    func getTeeDistribution(energy:Int){
        var ref = Int(getDataFromSheardPreferanceFloat(key: "dietType"))
                
        if getDataFromSheardPreferanceString(key: "cusinene") == "8" {
            ref = 8
        }
        var ob = tblTEEDistribution()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            ob.getTeeDistrbution(refID: ref,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{
            ob.getTeeDistrbution(refID: ref,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
    }
    
    
    
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)
//        setDataInSheardPreferance(value: "CC", key: String(c/15))
//        setDataInSheardPreferance(value: "KCALTo1CC", key: String(energy/(c/15)))
        
        
        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        sheardPreferanceWrite(fat: Float(fat),pro: Float((pro)),carbo: Float(carbo),energy: Float(energy))
    }
}
