//
//  TableViewController.swift
//  SearchBar
//
//  Created by Shinkangsan on 12/20/16.
//  Copyright © 2016 Sheldon. All rights reserved.
//

import UIKit



class ChangeSenfFromWajbh: UIViewController,UITableViewDelegate,
UITableViewDataSource{
   
    
    @IBOutlet var tbl: UITableView!
    var titleSenf:String!
    var idSenfCat:String!
    var idSefCatSub:String!

    var initialDataAry:[Model]!
    var dataAry:[Model]!
    var x:Int!
    var updateID:String!
    var oldID:String!
    var idWajbeh:String!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
     
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tilteNavigation.text = titleSenf
        if idSefCatSub == "0" {
            initialDataAry = Model.generateModelArray(str: idSenfCat)
            dataAry = Model.generateModelArray(str: idSenfCat)
        }else{
            initialDataAry = Model.generateModelArray2(str: idSenfCat,str2: idSefCatSub)
            dataAry = Model.generateModelArray2(str: idSenfCat,str2: idSefCatSub)
        }
    }
    func showAlert(str:String){
           let alert = UIAlertController(title: "خطأ", message: str, preferredStyle: UIAlertController.Style.alert)
              alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
              self.present(alert, animated: true, completion: nil)
       }
    
    @IBAction func btnChange(_ sender: Any) {

        let date = Date()
              let formatter = DateFormatter()
              formatter.dateFormat = "yyyy-MM-dd"
              let result = formatter.string(from: date)

        if updateID != nil {
            if  tblUserChangeSenf.setChange(oldID: oldID, updateID: updateID, w: idWajbeh, date: result, userID: "100") == true {
//                navigationController?.popViewController(animated: true)
//                    self.performSegue(withIdentifier: "showWajbeh", sender: nil)
                       dismiss(animated: true, completion: nil)
                
                   }else{
                       print("error ########")
                   }
        }else {
            showAlert(str:"يجب تحديد الصنف")
        }
       
    }
    
    @IBAction func buDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var tilteNavigation: UILabel!
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ChangeSenfFromWajbhCell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ChangeSenfFromWajbhCell
        
        let model = dataAry[indexPath.row]
       
        if x == indexPath.row {
            //green_ic_checked
            cell.btn.setImage(UIImage(named: "green_ic_checked"), for: UIControl.State.normal)
        }else{
            cell.btn.setImage(UIImage(named: "green_ic_unchecked"), for: UIControl.State.normal)
            
        }
        cell.nameLabel.text = model.nameItem
      
        
        return cell
    }
    
    //add delegate method for pushing to new detail controller
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        x = indexPath.row
          updateID = String(dataAry[indexPath.row].id)
        //check(index:indexPath.row)
        self.tbl.reloadData()
        
        
    }
    
    
    
}
