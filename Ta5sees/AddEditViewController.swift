//
//  AddEditViewController.swift
//  Ta5sees
//
//  Created by Admin on 5/29/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


protocol ShowNewWajbeh {
    func goToViewWajbeh(idWajbeh:String,flag:String)
}

import UIKit
import SwiftIconFont
import Firebase
import FirebaseAnalytics

@available(iOS 11.0, *)
class SearchItems: UIViewController ,UISearchBarDelegate,UIViewControllerTransitioningDelegate, CustomAlertViewDelegate {
    func okButtonTapped(selectedOption: String, textFieldValue: String) {
    }
    
    func cancelButtonTapped() {
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
    
    
    var flagEvent = true
    var refWInfo:Int!
    var refWCAtegory:Int!
    var arrPassData:[Int] = []
    var obShowNewWajbeh:ShowNewWajbeh?
    var id_items:String!
    var date:String!
    var delegateMain:ViewController!
    var pg:protocolWajbehInfoTracker?
    var filtered_all_Data =  [SearchItem]()
    var reversedNames =  [SearchItem]()
    var frameCell:CGRect!
    var list_all_data =  [SearchItem]()
    let syncConc = DispatchQueue(label:"swiftlee.concurrent.queue1",attributes:.concurrent)
    let tableView = UITableView()
    var safeArea: UILayoutGuide!
    internal var _arrWajbat = [tblWajbatUserMealPlanner]()
    @IBOutlet weak var stackItem: UIStackView!

    @IBOutlet weak var tblMealPlanner: SelfSizedTableView!
    @IBOutlet weak var titlePage: UILabel!
    @IBAction func buDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func favourites(_ sender: Any) {
        self.performSegue(withIdentifier: "FavuriteRecentController", sender: "data")
    }
    @IBAction func addANDEdite(_ sender: Any) {
//        self.performSegue(withIdentifier: "addeditewahjbh", sender: arrPassData)
        showToast(message: "غير متاحة حاليا", view: view,place:0)
    }
    @IBAction func recentlyUsed(_ sender: Any) {
        self.performSegue(withIdentifier: "RecentUsedItemController", sender: "data")
    }
    
    @IBOutlet weak var  searchBar: UISearchBar!

    
    @IBOutlet weak var lblTestSearch: UISearchBar!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if getDataFromSheardPreferanceString(key: "isTutorial-wajbeh") == "0" {
            view.superview?.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.20) { [self] in
            self.showTutorial(flag: 1)
            self.perform(#selector(setAutText),with:0,afterDelay: 0.2)
        }

        }
    }
  
    func disableController(){
        view.isUserInteractionEnabled = false
    }
    func enableController(){
        view.isUserInteractionEnabled = true
    }
    func showTutorial(flag:Int) {
        let tutorialVC = KJOverlayTutorialViewController(views:self)
        var tutorials: [KJTutorial] = []
        if flag == 1 {
            // tut1
            let focusRect1 = searchBar.frame
            let message1 = "ابحث عن وجبة"
            let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
            let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect1.maxY + 30, width: 72, height: 72)
            let message1Center = CGPoint(x: view.bounds.width/2, y: focusRect1.maxY + 24)
            let tut1 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect1, text: message1, textPosition: message1Center, icon: icon3, iconFrame: icon3Frame)
            tutorialVC.addEditWajbehFirstStep = true
            tutorialVC.addEditWajbehLastStep = false
            tutorials = [tut1]
        }else if flag == 2 {
            if !filtered_all_Data.isEmpty{
                let focusRect2 = tableView.convert(frameCell, to: self.view)
                let icon3 = UIImage(from: .fontAwesome, code: "handoup", textColor:.white, backgroundColor:.clear, size: CGSize(width: 72, height: 72))
                let icon3Frame = CGRect(x: self.view.bounds.width/2-72/2, y: focusRect2.maxY + 12, width: 72, height: 72)
                let message2 = "اختر وجبة"
                let message1Center1 = CGPoint(x: view.bounds.width/2, y: focusRect2.maxY + 100)
                let tut2 = KJTutorial.textWithIconTutorial(focusRectangle: focusRect2, text: message2, textPosition: message1Center1, icon: icon3, iconFrame: icon3Frame)
                tutorialVC.addEditWajbehLastStep = true
                tutorialVC.addEditWajbehFirstStep = false
                tutorials = [tut2]
            }
        }
        
        // let tutorials = [tut1, tut2, tut3, tut4]
        tutorialVC.tutorials = tutorials
        tutorialVC.addEditWajbeh = self
        tutorialVC.showInViewController(self)
    }
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
   
   
    override func viewDidLoad() {
        super.viewDidLoad()

        obShowNewWajbeh = self
        tableView.dataSource = self
        tableView.delegate = self
        
        searchBar.delegate = self
        setTilePage(id:id_items)
        tblMealPlanner.maxHeight = 205
        
        self.list_all_data.append(contentsOf: tblPackeg.searchModelArray())
        
        self.list_all_data.append(contentsOf:tblSenf.searchModelArray())
        

        tblWajbatUserMealPlanner.getAllDataWajbehMealPlanner(date: date,refPackgeCateItme:id_items) { [self] (arrWajbatUserMealPlanner) in
            _arrWajbat.append(contentsOf: arrWajbatUserMealPlanner)
            tblMealPlanner.reloadData()
    }
    }
    
    func textFieldDidChange(textField: UITextField) {
        //your code
    }
    
    
    @objc func setAutText(sender: Any){
        let word = "بانكيك"
        var index = sender as! Int

        let arr = Array(word)
        if index < arr.count {
        UIView.animate(withDuration: 0.2, animations: { [self] in
            searchBar.textField?.text!.append(arr[index])
            index = index+1
        }, completion: { [self]
            (value: Bool) in
            self.searchBar(self.searchBar, textDidChange: searchBar.text!)
            self.perform(#selector(setAutText),with:index,afterDelay: 0.2)
            
        })
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
                showTutorial(flag:2)
            }
        }
    }

    
    func setTilePage(id:String){
        
        switch id {
        case "1":
            titlePage.text! = "إضافة فطور"
            break
        case "2":
            titlePage.text! = "إضافة غداء"
            break
        case "3":
            titlePage.text! = "إضافة عشاء"
            break
        case "4":
            titlePage.text! = "إضافة تصبيرة صباحي"
            break
        case "5":
            titlePage.text! = "إضافة تصبيرة مسائي"
            break
        default:
            print("not found !!")
        }
    }
    
    func toutorialViewWajbeh(){
        self.performSegue(withIdentifier: "DisplayCateItemsController", sender: "256")
    }
    func getUserInWithThe( userIn:String)->String {
        var polyUserIn = userIn;
        if userIn.count >= 3{
            if (userIn.hasPrefix("ال")) {
                polyUserIn = String(userIn.dropFirst(2))
            }else{
                polyUserIn = "ال\(userIn)";
            }
        }
            
        
        return polyUserIn;
    }
    func confirmSearchKey(userINp:String,searchKey:String,nameMel:String)->Bool{
        let components = searchKey.components(separatedBy: "،")
        for item in components {

            if item.elementsEqual(userINp) ||  item.hasPrefix(userINp) ||  item.hasPrefix(getUserInWithThe(userIn: userINp)) {
            return true
            }
        
           }

        return false
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !_arrWajbat.isEmpty {
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
            UIView.animate(withDuration: 1) { [self] in
                tblMealPlanner.reloadRows(at: [indexPathForFirstRow], with: .fade)
            }
        }
    }
    
    @objc func saveItemHistory(){
        if filtered_all_Data.isEmpty {
            tblHistoryNotFoundMeals.setMeals(name:lblTestSearch.text!)
        }
    }
    let serialQueue = DispatchQueue(label: "swiftlee.serial.queue")

    func showAlert(){
        
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "GeneralAlertView") as! GeneralAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = self
        customAlert.text = "النسخة المجانية تتيح لك مراجعة ٥ اصناف باليوم"
        self.present(customAlert, animated: true, completion: nil)
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar(self.searchBar, textDidChange: searchBar.text!)
        searchBar.resignFirstResponder()
    }
  
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if id_items == "1"{
        flagEvent = true
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if flagEvent {
                createEvent(key: "2",date: getDateTime())
                flagEvent = false
        }
        DispatchQueue.main.async { [self] in
            self.perform(#selector(saveItemHistory),with:nil,afterDelay: 2)
        }
        if searchText.isEmpty == true {
            tableView.isHidden = true
            tableView.removeFromSuperview()
        }else{
            setupTableView()
            let serachWord = searchText.trimmingCharacters(in: .whitespaces).components(separatedBy: " ")
            for item in serachWord{
                serialQueue.sync { [self] in
            filtered_all_Data = list_all_data.filter { ($0.iteme_search.lowercased()).contains(item.lowercased())}
            }
                serialQueue.sync { [self] in
            sotrList(searchText:serachWord)
            }
//            self.tableView.isHidden = false
//            tableView.reloadData()
            }

        }
           
    }
    
 
    
    func matchesKey(constraint:String,searchKey:String)->Bool{
//        if searchKey.compare(constraint, options: NSString.CompareOptions.caseInsensitive).rawValue >= 0 {
//            return true
//        }

        return false
    }
    
    
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
    }
    
    override func loadView() {
        super.loadView()
        tblMealPlanner.register(UINib(nibName: "MealPlannerCell", bundle: nil), forCellReuseIdentifier: "MealPlannerCell")

        if #available(iOS 13.0, *) {
        searchBar.searchTextField.font = UIFont(name: "GE Dinar One",size:20)
        searchBar.searchTextField.textAlignment = .right
        }
        self.tableView.isHidden = true
      
    }
}




extension SearchItems:ShowNewWajbeh{
    func goToViewWajbeh(idWajbeh: String, flag: String) {
        
        if getSearchLimit(key: "\(getDateOnly())-\(id_items!)-\(getUserInfo().id)") > 6{
            showAlert()
            return
        }
        createEvent(key: "3", date: getDateTime())
        
        if flag == "wajbeh"{
            self.performSegue(withIdentifier: "DisplayCateItemsController", sender: idWajbeh)
        }else {
            self.performSegue(withIdentifier: "DisplayCateItemsSenfController", sender: idWajbeh)
        }
        //        txtSearchOutlet.text = nil
        
    }
    
    
    
}


extension SearchItems: UITableViewDelegate, UITableViewDataSource {
    
    
    //////////////////////////////////////////////////////////////////////////////
    // Table View related methods
    //////////////////////////////////////////////////////////////////////////////
    
    
    // MARK: TableView creation and updating
    
    // Create SearchTableview
    
    
    
    // MARK: TableViewDataSource methods
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblMealPlanner {
            if _arrWajbat.count > 0 {
                return 1//_arrWajbat.count
            } else {
                stackItem.arrangedSubviews[0].isHidden = true
                return 0
            }
        }
        return filtered_all_Data.count
    }
    
    // MARK: TableViewDelegate methods
    
    //Adding rows in the tableview with the data from dataList
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("customCellSearch", owner: self, options: nil)?.first as! customCellSearch
        var plateID = [String]()
        var restrectedAllergy = [String]()
        
        if tableView == tblMealPlanner {
            let item = _arrWajbat[indexPath.row]
            tblPackageIngredients.getPlatID(refID: item.refPackgeID) { (arrPlateID) in
                plateID = arrPlateID.removeDuplicates()
            }
            for id in plateID {
                tblPackageIngredients.getIngPlate(refIDPKG: item.refPackgeID, refIDPL: id) { (arr,arr1) in
                    restrectedAllergy.append(contentsOf: arr1)
                }
            }
            let cell = Bundle.main.loadNibNamed("MealPlannerCell", owner: self, options: nil)?.first as! MealPlannerCell
            if _arrWajbat.count > 0 {
                let attributedString = NSMutableAttributedString()
                let style = NSMutableParagraphStyle()
                let attributedStringTextAttachment = NSTextAttachment()
                style.alignment = NSTextAlignment.right
                if !restrectedAllergy.isEmpty {
                    if #available(iOS 13.0, *) {
                        attributedStringTextAttachment.image = UIImage(named: "warning.png")?.withTintColor(.red)
                    } else {
                        // Fallback on earlier versions
                    }
                attributedString.append(NSAttributedString(attachment: attributedStringTextAttachment))
                }
                attributedString.append(NSMutableAttributedString.init(string: " \( item.packgeName)"))
                
                cell.lblName.attributedText = attributedString //item.packgeName
                cell.lblCaloris.text = " \(item.packagesCalories) سعرة "
                cell.lblGram.text = " \(item.packagesWeights) غم "
                cell.view = self
                cell.item = item
                cell.index = indexPath.row
                if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
                    cell.isFavrite = true
                    cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
                }else{
                    cell.isFavrite = false
                    cell.imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
                    cell.imgFav.alpha = 0.2
                }
                cell.lbltet.text = setTilePage(id:tblPackeg.getIDCategory(id: Int(item.refPackgeID)!,id_Cate:Int(item.refPackgeCateItme)!))
            }
            return cell
        }
        
        
        cell.nameSanf.text = filtered_all_Data[indexPath.row].getStringText()
        cell.caloris.text =  filtered_all_Data[indexPath.row].getStringTextcaloris()
        cell.catrgory.text =  filtered_all_Data[indexPath.row].getStringTextCate()
        
        //        cell.imageSenf.image = UIImage(named : "logo")
        SetImage(laImage:cell.imageSenf)
        if indexPath.row == 1 {
            frameCell = cell.frame
        }
        
        return cell
    }
    func DisplayWajbh(index:Int){
        let item = _arrWajbat[index]
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "showWajbehMealPlanner") as! showWajbehMealPlanner
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        detailView.date = item.date
        detailView.asMealPlanner = true
        detailView.idPackge = Int(item.refPackgeID)
        detailView.id_items = id_items
        detailView.movefromHome = true
        detailView.delegateMain = delegateMain
        detailView.flg_category_mealPlanner = Int(item.refPackgeCateItme)!
        detailView.pg = pg
        if tblItemsFavurite.checkItemsFavutite(id:item.refPackgeID) == true {
            detailView.isFavrite = true
        }else{
            detailView.isFavrite = false
        }
        detailView.itemMealPlanner = item
        tblUserWajbehEaten.checkFoundItemIsNotProposal(id:Int(item.refPackgeID)!, wajbehInfo:item.refPackgeCateItme, date: item.date, isProposal: "1", completion: { (id,bool) in
          print("bool \(bool)")
            detailView.idItemsEaten = id
            if bool == true {
                detailView.isEaten = true
            }
            else{
                detailView.isEaten = false
            }
        })
        if id_items == "1" {
            createEvent(key: "33", date: getDateTime())
        }else if id_items == "2" {
            createEvent(key: "71", date: getDateTime())
        }else if id_items == "3" {
            createEvent(key: "73", date: getDateTime())
        }else if id_items == "4" {
            createEvent(key: "75", date: getDateTime())
        }else if id_items == "5" {
            createEvent(key: "77", date: getDateTime())
        }
        present(detailView, animated: true, completion: nil)
    }
    func setTilePage(id:Int)->String{
        
        switch id {
        case 1:
            return "فطور"
        case 2:
            return "غداء"
        case 3:
            return "عشاء"
        case 4:
            return "تصبيرة صباحي"
        case 5:
            return "تصبيرة مسائي"
        default:
            print("not found !!")
        }
        return ""
    }
    
    func SetImage(laImage:UIImageView){
        laImage.layer.borderWidth = 1
        laImage.layer.masksToBounds = false
        laImage.layer.borderColor = colorGray.cgColor
        laImage.layer.cornerRadius = laImage.frame.height/2
        laImage.clipsToBounds = true
        
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblMealPlanner {
            let indexPathForFirstRow = IndexPath(row: indexPath.row, section: 0)
            let currentCell = tableView.cellForRow(at: indexPathForFirstRow)! as! MealPlannerCell
            UIView.animate(withDuration: 0.1, delay: 0.01, options: .allowAnimatedContent) {
               
                currentCell.contentView.backgroundColor = .gray.withAlphaComponent(0)
            } completion: {  bool in
                currentCell.contentView.backgroundColor = UIColor(hexString: "#F2F2F7")
            }
            DisplayWajbh(index: indexPath.row)

        }else {
        print("selected row \(filtered_all_Data[indexPath.row].item_id)")
        obShowNewWajbeh?.goToViewWajbeh(idWajbeh: filtered_all_Data[indexPath.row].item_id,flag:filtered_all_Data[indexPath.row].item_flag)
        tableView.isHidden = true
        }
    }
    
    
    
}



extension SearchItems {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "FavuriteRecentController" {
              if let dis=segue.destination as?  FavuriteRecentController{
                  if  sender != nil  {
                      dis.id_items = "-1"
                      dis.id_category = id_items
                      dis.date = date
                      dis.pg = pg
                  }
              }
          }else   if segue.identifier == "DisplayCateItemsController" {
              if let dis=segue.destination as?  DisplayCateItemsController{
                  if  let data=sender as? String {
                      dis.idPackge = Int(data)
                      dis.id_items = id_items
                      dis.date = date
                      dis.pg = pg
                      if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                          dis.isFavrite = true
                      }else{
                          dis.isFavrite = false
                      }
                      tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, isProposal: "0", completion: { (id,bool) in
                        print("bool \(bool)")
                          dis.idItemsEaten = id
                          if bool == true {
                              dis.isEaten = true
                          }
                          else{
                              dis.isEaten = false
                          }
                      })
                  }
              }
          }else if segue.identifier == "DisplayCateItemsSenfController" {
              if let dis=segue.destination as?  DisplayCateItemsSenfController{
                  if  let data=sender as? String {
                      dis.idPackge = Int(data)
                      dis.id_items = id_items
                      dis.date = date
                      dis.pg = pg
                      if tblItemsFavurite.checkItemsFavutite(id:data) == true {
                          dis.isFavrite = true
                      }else{
                          dis.isFavrite = false
                      }
                      
                      tblUserWajbehEaten.checkFoundItemIsNotProposal(id: Int(data)!, wajbehInfo:id_items, date: date, isProposal: "0", completion: { (id,bool) in
                        print("bool \(bool)")
                          dis.idItemsEaten = id
                          if bool == true {
                              dis.isEaten = true
                          }else{
                              dis.isEaten = false
                          }
                      })
                  }
              }
          }else if segue.identifier == "RecentUsedItemController" {
              if let dis=segue.destination as?  RecentUsedItemController{
                  if  sender != nil  {
                      dis.id_items = "-1"
                      dis.id_category = id_items
                      dis.date = date
                      dis.pg = pg
                  }
              }
          }
          
      }
      
}

extension SearchItems:filterListner {
    
    func sotrList(searchText:[String]) {
        var sortedResultSenf = [SearchItem]()
        

        for resultsSenf in searchText {
            sortedResultSenf.append(contentsOf: filtered_all_Data.filter { ($0.iteme_search.lowercased()).contains(resultsSenf.lowercased()) }
            )
        }
        
        DispatchQueue.main.async { [self] in
            self.filtered_all_Data = uniq(source: sortedResultSenf)
            self.tableView.reloadData()
            self.tableView.isHidden = false
        }
        
    }
}

protocol filterListner {
    func sotrList(searchText:[String])
}
extension  UISearchBar {

    var textField : UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            // Fallback on earlier versions
            return value(forKey: "_searchField") as? UITextField
        }
    }
}
extension SearchItems: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    if #available(iOS 13.0, *) {
        searchController.showsSearchResultsController = true
    } else {
        // Fallback on earlier versions
    }
  }
}


import UIKit
import Realm
import RealmSwift
class CustomCell1: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnsh.layer.cornerRadius = btnsh.bounds.height*0.5
        btnre.layer.cornerRadius = btnre.bounds.height*0.5
        imgFav.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(btnFavurite(_:))))
        imgFav.tintColor = .red
        //          Initialization code
    }
    var view:GLPresentViewController!
    var index:Int!
    var isFavrite = false
    var item:tblWajbatUserMealPlanner!
    @IBOutlet weak var lblCaloris: UILabel!
    
    @IBOutlet weak var lblGram: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnsh: UIButton!
    @IBOutlet weak var btnre: UIButton!
    @IBOutlet weak var lbltet: UILabel!
    @IBOutlet weak var imgFav: UIImageView!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        //          Configure the view for the selected state
    }
   
    @IBAction func btnReplace(_ sender: Any) {
        tblPackeg.replaceWajbeh(cateInfo:Int(item.refPackgeCateItme)!, old_item_id:item.id) { [self] (item) in
            let indexPath = IndexPath(row: index, section: 0)

            view.tblView.reloadRows(at:[indexPath], with: UITableView.RowAnimation.top)
        }
    }
    @IBAction func btnShow(_ sender: Any) {
        view.DisplayWajbh(index : index)
    }
    @objc func btnFavurite(_ gestureRecognizer: UITapGestureRecognizer) {
        if isFavrite == false {
            let obFavurite = tblItemsFavurite()
            obFavurite.id = obFavurite.IncrementaID()
            obFavurite.refUserId = getDataFromSheardPreferanceString(key: "userID")
            obFavurite.flag = "0"
            obFavurite.refItem = String(item.refPackgeID)
            
            try! setupRealm().write {
                setupRealm().add(obFavurite, update: Realm.UpdatePolicy.modified)
                imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
                imgFav.alpha = 1
                showToast(message: "تم إضافة الوجبة الى المفضلة", view: view.view,place:0)
            }
            isFavrite = true
        }else{
            tblItemsFavurite.deleteItem(id: String(item.refPackgeID),flag:"0")
            imgFav.setImage(UIImage(named : "Favourites")!.withRenderingMode(.alwaysTemplate))
            imgFav.alpha = 0.1
            showToast(message: "تم حذف الوجبة المفضلة", view: view.view,place:0)
            isFavrite = false
        }
    }
    public func getIngrPlate(idPackge:String,id_Cate:Int){
        var protein:Float = 0.0
        var fat:Float = 0.0
        var carbohydrate:Float = 0.0
        var caloris:Float = 0.0
        var grams:Float = 0.0
        var target:Float = 0.0
        var plateID:[String]!
        var total_caloris = 0.0
        var total_caloris1 = 0.0
        
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            plateID = arrPlateID.removeDuplicates()
        }
        
     
        target = calculateBasedCalorisAllowdUser(idWajbeh:id_Cate)
        for id in plateID {
            let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
            caloris = target
                let percentge = Float(caloris) * nutrationPlate!.Percentage

            tblPackageIngredients.getIngsPlat(packgID: idPackge, platID: id) { (arrIngredients) in
                let resulteNutrtion = getIngPlate(arr:arrIngredients, id_waj:idPackge,plateId: id, date: "nil", saveShoppingList: false)
                total_caloris1 += Double(percentge)
                total_caloris += Double(resulteNutrtion[0])
                let valKCal:Float = percentge * resulteNutrtion[4]

                grams += valKCal/Float(resulteNutrtion[0])
          
                
                
                protein += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[3]),val3:Float(resulteNutrtion[0]))
                
                fat += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[1]),val3:Float(resulteNutrtion[0]))
                
                carbohydrate += calclatNuitrationFacts(val1:percentge,val2:Float(resulteNutrtion[2]),val3:Float(resulteNutrtion[0]))
                print("********* \(total_caloris1) - protien \(protein) - cho\(carbohydrate) - fat\(fat) rm \(grams)")

            }
            
          
        }
        
        print("total_caloris \(total_caloris1) - protien \(protein) - cho\(carbohydrate) - fat\(fat) rm \(grams)")

        
        lblCaloris.text = " \(Int(round(total_caloris1))) سعرة "
        lblGram.text = " \(String(Int(round(grams)))) غم "
        
    }
    
    
}


extension SearchItems:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
