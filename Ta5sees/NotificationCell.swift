//
//  NotificationCell.swift
//  Ta5sees
//
//  Created by Admin on 9/21/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit

 
class NotificationCell: UITableViewCell {
    @IBOutlet weak var logo_image: UIImageView!
    
    @IBOutlet weak var contanir: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var title: UILabel!
    var delegat:NotificationListController!
    var index:Int!
    
    
    func setImageIcon(id:String)->String{
        switch id {
        case "1":
            return "breakfast.png"
        case "2":
            return "launch.png"
        case "3":
            return "dinner.png"
        case "4":
            return "apple.png"
        case "5":
            return "tsbera.png"
        case "6":
            return "tmreen.png"
        case "7":
            return "full_small.png"
        case "8":
            return "logo"
        default:
            return "logo"
        }
    }
    func viewPopup(){
        delegat.viewPopup(index: index)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
//        addGestureRecognizer(tapGesture)
        //tapGesture.delegate = ViewController()
        
    }
    
   @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        delegat?.viewPopup(index:index)
    }
    
    
    
}


protocol prisnterDataNotification {
    func viewPopup(index:Int)
    func removeItem(index:Int)
}
