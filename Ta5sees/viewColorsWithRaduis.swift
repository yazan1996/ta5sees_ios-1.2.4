//
//  ViewColors.swift
//  Ta5sees
//
//  Created by Admin on 4/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import UIKit

@IBDesignable class viewColorsWithRaduis: UIView {
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 25.0 {
           didSet {
            if cornerRadius != CGFloat(0) {
                layer.cornerRadius = cornerRadius
            }else{
                layer.cornerRadius = bounds.size.height * 0.5
            }
           }
       }
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    @IBInspectable var isSoon: Bool = false {
           didSet {
               updateView()
           }
       }
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        if (isSoon ) {

        let myLayer = CALayer()
        let myImage = UIImage(named: "soon_label")?.cgImage
        myLayer.frame = CGRect(x: 0, y: -30, width: self.frame.size.width+15, height: self.frame.size.height+50)
        myLayer.contents = myImage
        self.layer.addSublayer(myLayer)
        }
        
        layer.cornerRadius = cornerRadius
        layer.borderColor = UIColor(red: 141/255, green: 222/255, blue: 187/255, alpha: 1.0).cgColor
        layer.borderWidth = 1

        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
}
