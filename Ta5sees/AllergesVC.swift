//
//  AllergesVC.swift
//  Ta5sees
//
//  Created by Admin on 3/21/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class AllergesVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIViewControllerTransitioningDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return items.count
        return items.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multiSelect1",for: indexPath) as? formateSelectedCell else {
            return formateSelectedCell()
        }
        
        let item = section[indexPath.section].items[indexPath.row]
        print("items.count",item.name,item.id)
        if section[indexPath.section].isMultiple {
            if item.name == "لا شيء" {
                index = indexPath.row
            }else{

            }
            //For multiple selection
            if item.selected {
//                cell.accessoryType = .checkmark
                cell.img.setImage(UIImage(named: "checked_checkbox")!.withRenderingMode(.alwaysTemplate))
            }
            else {
                cell.img.setImage(UIImage(named: "unchecked_checkbox")!.withRenderingMode(.alwaysTemplate))
//                cell.accessoryType = .none
            }
        }
        else {
            //For single selection
            if item.selected {
//                cell.accessoryType = .checkmark
                cell.img.setImage(UIImage(named: "checked_checkbox")!.withRenderingMode(.alwaysTemplate))
            }
            else {
                cell.img.setImage(UIImage(named: "unchecked_checkbox")!.withRenderingMode(.alwaysTemplate))
//                cell.accessoryType = .none
            }
        }
        cell.txt.text = section[indexPath.section].items[indexPath.row].name
        cell.txt.textAlignment = .right
        cell.txt.font = UIFont(name: "GE Dinar One",size: 16)
        cell.txt.textColor = colorGray
        cell.txt.numberOfLines = 10
        cell.txt.lineBreakMode = .byWordWrapping
        return cell
    }
    
    
    
    var items = [Item]()
    var section = [Section]()
    var obselectionDelegate:selectionDelegate!
    var optionsSelected = ""
    var flag = "2" // flag 1 >> cusine / flag 2 >> allergy
    var index:Int!
    var seletAll = 0
    lazy var obviewMultiSelectPresnter = viewMultiSelectPresnter(with: self)
    @IBAction func backToHome(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        obviewMultiSelectPresnter.getData(flag:flag)
        setUpCollectionView()

        let filtered = items.map{$0.name}.intersection(with:optionsSelected.split(separator: ",").map { String($0) })
        
       // items.filter { filtered.map { String($0) }.contains($0.name)}
        _ = items.filter { (Item) -> Bool in
            Item.selected = filtered.map { String($0) }.contains(Item.name)
            return true
        }
        
        let imageName = "logo"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.contentMode = .center
        
        let txtTitle = UILabel()
        txtTitle.text = "الحساسيات"
        txtTitle.font =  UIFont(name: "GE Dinar One", size: 20.0)
        txtTitle.frame = CGRect(x: 0, y: 0, width: subViewBacground.frame.width-32, height: 40)
        txtTitle.textColor = .white
        txtTitle.textAlignment = .center
        let stack = UIStackView()
        stack.frame = CGRect(x: 0, y: 0, width: subViewBacground.frame.width, height: subViewBacground.frame.height*0.80)


        stack.addArrangedSubview(imageView)
        stack.addArrangedSubview(txtTitle)

        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fill
        stack.spacing = 24
        subViewBacground.addSubview(stack)

   
        stack.center.x = view.center.x
        stack.center.y = subViewBacground.center.y+15

        
        

    }
    private func showAlert(WithMessage message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func btnNoThing(_ sender: Any) {
        ActivationModel.sharedInstance.refAllergys = ["-1"]
        nextPage()
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if  ActivationModel.sharedInstance.refAllergys.isEmpty {
            ActivationModel.sharedInstance.refAllergys = ["-1"]
        }
        nextPage()
    }
    
    func nextPage(){
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "MealDistrbutionVC") as! MealDistrbutionVC
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        createEvent(key: "24",date: getDateTime())
        self.present(detailView, animated: true, completion: nil)
    }
        
        func setUpCollectionView() {
             /// 1
            

             /// 2
            tblview.delegate = self
            tblview.dataSource = self
            tblview.semanticContentAttribute = .forceRightToLeft

             /// 3
             let layout = UICollectionViewFlowLayout()
             layout.scrollDirection = .vertical
             /// 4
             layout.minimumLineSpacing = 8
             /// 5
             layout.minimumInteritemSpacing = 4

             /// 6
            tblview
                   .setCollectionViewLayout(layout, animated: true)
           }
        
        
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    
  
 
 
    
    func setUNSeletd(falg:Bool){
        for item in items {
            item.selected = falg
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if section[indexPath.section].isMultiple {
            let item = section[indexPath.section].items[indexPath.row]
            if item.name == "لا شيء" && item.selected == false {

                index = indexPath.row
                _ = items.map { (item_) -> Void in
                    if items[index].name == item_.name {
                        item_.selected = true
                    }else{
                        item_.selected = false
                    }
                }
                tblview.reloadData()
            } else if item.name != "لا شيء" && item.selected == false {
                if index != nil {
                items[index].selected = false
                }
                item.selected = true
                tblview.reloadData()
            }else{
                //For multiple selection
                item.selected = !item.selected
                
            }
            section[indexPath.section].items[indexPath.row] = item
            tblview.reloadItems(at: [indexPath])
//            tblview.reloadRows(at: [indexPath], with: .automatic)
        }
        else {
            //For multiple selection
            let items = section[indexPath.section].items
            
            if let selectedItemIndex = items.indices.first(where: { items[$0].selected }) {
                section[indexPath.section].items[selectedItemIndex].selected = false
                if selectedItemIndex != indexPath.row {
                    section[indexPath.section].items[indexPath.row].selected = true
                }
            }
            else {
                section[indexPath.section].items[indexPath.row].selected = true
            }
            tblview.reloadItems(at: [indexPath])
//            tblview.reloadSections([indexPath.section], with: .automatic)
        }
        
        let refItem:[Item] = items.filter { (item) -> Bool in
            if item.selected {
                return true
            }
            return false
        }
        let refIDs:[String] = refItem.map { (item) in
            item.id
        }
        ActivationModel.sharedInstance.refAllergys = refIDs
    }
 
    
    @IBOutlet weak var subViewBacground: UIView!

    @IBOutlet weak var tblview: UICollectionView!
}

extension AllergesVC:viewMultiSelectDelegate {
    
    func getDataSection(section: [Section]) {
        self.section = section
    }
    
    func getDataArray(list: [Item]) {
        items = list
      if items.contains(where: {$0.selected == true}) {
         // it exists, do something
        NSLog("here")
        seletAll = 0
      } else {
        seletAll = 1
        NSLog("here2")
         //item could not be found
      }
    }
    
    
}

extension AllergesVC: UICollectionViewDelegateFlowLayout {
    /// 1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        /// 2
        return UIEdgeInsets(top: 1.0, left: 8.0, bottom: 1.0, right: 8.0)
    }
    
    /// 3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        /// 4
        let lay = collectionViewLayout as! UICollectionViewFlowLayout
        /// 5
        let widthPerItem = collectionView.frame.width / 2 - lay.minimumInteritemSpacing
        /// 6
        return CGSize(width: widthPerItem - 8, height: 45)
    }
}

extension CGFloat {
    static func randomValue() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

class CurveView:UIView {

    var once = true

    override func layoutSubviews() {
        super.layoutSubviews()

        if once {
        


            let bb = UIBezierPath()

            bb.move(to: CGPoint(x: 0, y: self.frame.height))

            // the offset here is 40 you can play with it to increase / decrease the curve height

            bb.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height), controlPoint: CGPoint(x: self.frame.width / 2 , y: self.frame.height + 20 ))

            bb.close()

            let l = CAShapeLayer()

            l.path = bb.cgPath

            l.fillColor =  UIColor(hexString:"#B6FBD3").cgColor//self.backgroundColor!.cgColor

            self.layer.insertSublayer(l,at:0)

            
            let gradient = CAGradientLayer()

            gradient.frame = self.bounds
            gradient.colors = [UIColor(hexString:"#7BDEB3").cgColor,UIColor(hexString:"#7BDEB3").cgColor,UIColor(hexString:"#B6FBD3").cgColor]

            self.layer.insertSublayer(gradient, at: 0)
            
            once = false
        }

    }

}

extension AllergesVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
