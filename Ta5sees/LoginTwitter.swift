//
//  LoginTwitter.swift
//  Ta5sees
//
//  Created by Admin on 4/14/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import TwitterKit
import FirebaseAuth
import Firebase

class LoginTwitter {
    
    var twitterSession:TWTRSession?
    var nameTwitter:String!
    var usernameTwitter:String!
    var delege:Any!
    var view:viewTwitterLogin!
    var flag:Int!
    var delegte:UIViewController!
    init(with view:viewTwitterLogin,delege:Any,flag:Int,delegte:UIViewController){
        self.view=view
        self.delege=delege
        self.flag=flag
        self.delegte = delegte
    }
    
    func LoginTwitter(callBack:@escaping (Bool)->Void){
        TWTRTwitter.sharedInstance().logIn(with: delegte) { session, error in
            if (session != nil) { // Log in succeeded
                self.twitterSession = session
                  self.view.showProgress()
                self.sighnTwitter()
                callBack(true)
            } else {
                self.view.HideProgress()
//                self.view.errorConnectionTwitter(str: "\(error)")//"لم تتم العملية بنجاح"
                callBack(false)
            }
        }
    }
    
    func sighnTwitter(){
        
        let credential = TwitterAuthProvider.credential(withToken: twitterSession!.authToken, secret: twitterSession!.authTokenSecret)
        
        print("twitterSession!.authToken \(twitterSession!.authToken)")
        
        Auth.auth().signIn(with : credential ) { (user, err) in
            if err != nil {
                self.view.HideProgress()
                self.view.errorConnectionTwitter(str: "\(String(describing: err))")//"لم تتم العملية بنجاح"
                return
            }
//            if getDataFromSheardPreferanceString(key: "loadWajbehSenf") == "0" {
//            DispatchQueue.global(qos: .utility).async {
//                LoadData.FetchWajbehSenf()
//            }
//            }
            print("successfully twitter")
            self.fetchUserTwitter()
        }
    }
    
    func fetchUserTwitter(){
        let client = TWTRAPIClient.withCurrentUser()
            
        client.loadUser(withID: twitterSession!.userID) { (user, err) in
            if err != nil {
                self.view.HideProgress()
                self.view.errorConnectionTwitter(str: "\(String(describing: err))") //"لم تتم العملية بنجاح")
                return
            }
            
            guard let user = user else {return}
            self.nameTwitter = user.name
            self.usernameTwitter = self.twitterSession?.userName
           
//            if self.flag == 0 { // signup
//                self.saveData(name: self.usernameTwitter,id_Twitter: user.userID)
//
//            }else {
                //signin
                self.view.getIDUseerTwitter(id: user.userID,name: self.usernameTwitter,loginType: "3")
//            }
        }
    }
    
    
    func saveData(name:String,id_Twitter:String){
        setDataInSheardPreferance(value: name, key: "name")
        setDataInSheardPreferance(value: id_Twitter, key: "mobNum")
        setDataInSheardPreferance(value: "3", key: "loginType")
//        APILogin.sheard.checkUserTwitterIDFound(idTwitter:id_Twitter) { (flag) in
//            if flag == 4 {
//                self.view.HideProgress()
//                self.view.successConnectionTwitter()
//            }else if flag == 1 {
//                self.view.errorConnectionTwitter(str: "المستخدم مشترك مسبقا")
//            }
//        }
        
    }
}

protocol viewTwitterLogin {
    func errorConnectionTwitter(str:String)
    func successConnectionTwitter()
    func showProgress()
    func HideProgress()
    func getIDUseerTwitter(id:String,name:String,loginType:String)
    
}


/*
 
 */
