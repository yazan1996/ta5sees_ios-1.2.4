//
//  Model.swift
//  SearchBar
//
//  Created by Shinkangsan on 12/20/16.
//  Copyright © 2016 Sheldon. All rights reserved.
//

import UIKit
import SwiftyJSON
import Foundation
import RealmSwift

class Model: NSObject {
    let ob = tblSenf()
    var nameItem:String = ""
    var id:Int = -1
    static var arraylist:[tblSenf] = []
    static var resultelist:Results<tblSenf>!
    static var listDectionary :[Int:String]=[:]
    var txtallergyID = [String]()
    init(name:String,id:Int) {
        self.nameItem = name
        self.id = id
    }
   
   
    
    class func generateModelArray(str:String) -> [Model]{
        var modelAry = [Model]()
        modelAry.removeAll()
        listDectionary.removeAll()
        arraylist.removeAll()
        tblSenf.getdataJoin(refID:Int(str)!) {(response, Error) in
            resultelist = (response) as? Results<tblSenf>
            arraylist = Array(resultelist)
            for x in 0..<resultelist.count {
               
                listDectionary[resultelist[x].id]=resultelist[x].discription
            }
            for (key, value) in listDectionary {
                modelAry.append(Model(name: value,id: key))
            }
        }
    
        return modelAry
    }
    
    
    class func generateModelArray2(str:String,str2:String) -> [Model]{
          var modelAry = [Model]()
          modelAry.removeAll()
          listDectionary.removeAll()
          arraylist.removeAll()
        tblSenf.getdataJoin2(refID:Int(str)!,refID2: Int(str2)!) {(response, Error) in
              resultelist = (response) as? Results<tblSenf>
              arraylist = Array(resultelist)
              for x in 0..<resultelist.count {
                 
                  listDectionary[resultelist[x].id]=resultelist[x].discription
              }
              for (key, value) in listDectionary {
                  modelAry.append(Model(name: value,id: key))
              }
          }
      
          return modelAry
      }
}
