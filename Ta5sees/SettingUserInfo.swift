//
//  Offers.swift
//  NavigationDrawer
//
//  Created by Sowrirajan Sugumaran on 05/10/17.
//  Copyright © 2017 Sowrirajan Sugumaran. All rights reserved.
//

import UIKit

class SettingUserInfo: UIViewController {

    var listItem = ["معلومات شخصية","قياسات الجسم","التنبيهات","تواصل معنا","المصادر"]
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBOutlet weak var tblview: UITableView!

    @IBAction func dissmes(_ sender: Any) {
          dismiss(animated: true, completion: nil)
      }


}
extension SettingUserInfo :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listItem.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SettingUserInfoCell =
            tblview.dequeueReusableCell(withIdentifier: "SettingUserInfoCell", for: indexPath) as! SettingUserInfoCell
        cell.title.text = listItem[indexPath.row]
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedRowIndexPath = self.tblview.indexPathForSelectedRow {
            self.tblview.deselectRow(at: selectedRowIndexPath, animated: true)
        }
        let x = indexPath.row
        
        if x == 0{
            self.performSegue(withIdentifier: "PersonalInformationController", sender: nil)
            
        }else if (x == 1){
            self.performSegue(withIdentifier: "BodyMasurmentController", sender: nil)
            
        }else if x == 2{
            self.performSegue(withIdentifier: "AlarmsSittengController", sender: nil)
            
        }else if x == 3{
            self.performSegue(withIdentifier: "contractUSController", sender: nil)
            
        }else {
            self.performSegue(withIdentifier: "about", sender: nil)
            
        }
        
       
    }
    
    



}




