//
//  AccountController.swift
//  Ta5sees
//
//  Created by Admin on 9/20/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import FirebaseAuth
import FirebaseAnalytics

class AccountController: UIViewController,UIViewControllerTransitioningDelegate,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var lblVirsion: UILabel!
    @IBOutlet weak var type_subsc: UILabel!
    @IBOutlet weak var date_subsc: UILabel!
    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var viewSetting: UIView!

    @IBOutlet weak var btnLogOut: btnTwitter!

    
    @IBOutlet weak var viewContanir: CapsuleView!

    @IBOutlet weak var viewFavurite: UIView!
    @IBOutlet weak var viewNotfy: UIView!
    @IBOutlet weak var stackSetting: UIStackView!
    @IBOutlet weak var stackNotfy: UIStackView!
    @IBOutlet weak var imageuser: UIImageView!
    var userInfo:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!

    lazy var obpresnterAccount = PresnterAccount(with:self)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if getUserInfo().subscribeType == 1 || getUserInfo().subscribeType == 4 {
            btnChangeDietTypeOut.isHidden = true
        }
        if getUserInfo().subscribeType == 0 {
            if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() < Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                btnChangeDietTypeOut.isHidden = true
            }
        }
        lblVirsion.text = "إصدار \(String(describing: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String))"
        
        getInfoUser { (item, error) in
                   self.userInfo = item
               }
        
        getInfoHistoryProgressUser { [self] (user, err) in
            self.user_ProgressHistory = user
        }
        
        
        
      
        if user_ProgressHistory.refPlanMasterID == "3" && userInfo.grown == "1"  {
            stackUpdats.isHidden = true
        }else if userInfo.grown == "2"  {
            viewContanir.isHidden = true
        }

        setTargetWeight(target:user_ProgressHistory.target)
        SetImage(laImage:imageuser)
        SetStack(laImage:viewSetting)
        SetStack(laImage:viewNotfy)
        SetStack(laImage:viewFavurite)
   
//        let buttonWidth = btnLogOutOutlet.frame.width
//        let spacing: CGFloat = 8.0 / 2
//        let imageWidth = btnLogOutOutlet.imageView!.frame.width

        
//        btnLogOutOutlet.imageEdgeInsets = UIEdgeInsets(top: 7, left: buttonWidth + spacing - imageWidth - 16, bottom: 7, right: -spacing)
//        btnLogOutOutlet.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageWidth/2, bottom: 0, right: imageWidth/2)
//        btnLogOutOutlet.contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        
        setGradientBackgroundButton(btn:btnLogOut, delegate: self)

    
    }
    func setTargetWeight(target:String){
        if Double(target)!.rounded(.up) == Double(target)!.rounded(.down){
            lblTarget.text = "هدف الوزن : \(Int(Double(target)!.rounded(.down))) كغم"
        }else{
            lblTarget.text = "هدف الوزن : \(target) كغم"

        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getInfoUser { (user,err) in
                 if err == nil {
                    var subType = ""
                    var expire = ""
                    if getDateOnlyasDate().timeIntervalSince1970 * 1000.0.rounded() > Double(getUserInfo().expirDateSubscribtion) ?? 0.0 {
                        expire = "(منتهي الصلاحية)"
                    }
                     self.user_name.text = user.firstName
                    let hestoryPayment = setupRealm().objects(tblPaymentHistory.self).filter("refUserID == %@",getUserInfo().id).last
                    if hestoryPayment?.product_id == "1" {
                        subType = "شهري"
                    }else if hestoryPayment?.product_id == "3" {
                        subType = "٣ شهور"
                    }else if hestoryPayment?.product_id == "12" {
                        subType = "سنوي"
                    }
                     self.date_subsc.text = "مشترك منذ \(user.insertDateTime)"
                    if user.subscribeType == 1 || getUserInfo().subscribeType == 4 || getUserInfo().subscribeType == 2 || getUserInfo().subscribeType == 3 {
                        self.type_subsc.text = "نوع الاشتراك : \(subType) \(expire)"
                    }else if user.subscribeType == 0 {
                        self.type_subsc.text = "نوع الاشتراك : اشتراك مجاني \(expire)"
                    }else{
                        self.type_subsc.text = "نوع الاشتراك : غير مشترك"
                    }
                 }else{
                     alert(mes: "نعتذر لا يمكن الوصول لبياناتك", selfUI: self)
                 }
             }
    }
    @IBOutlet weak var stackUpdats: UIStackView!
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
        return false
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
    @IBOutlet weak var btnChangeDietTypeOut: ButtonColorRaduis!
    @IBAction func btnChangeDietType(_ sender: Any) {
        createEvent(key: "13",date: getDateTime())
        let detailView = storyboard!.instantiateViewController(withIdentifier: "NaturalDiet") as! NaturalDiet
            detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        detailView.scenarioStep = "1"
        present(detailView, animated: true, completion: nil)
//        self.performSegue(withIdentifier: "updateDietType", sender: "1")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "updateDietType" {
            
              if let dis=segue.destination as?  NaturalDiet{
                if  let data=sender as? String {
                      dis.scenarioStep = data
                  }
              }
          }
    }
    @IBOutlet weak var lblTarget: UILabel!
    
    @IBAction func btnLogOut(_ sender: Any) {
        let detailView = storyboard!.instantiateViewController(withIdentifier: "firstPage") as! FirstPageController
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

        appDel.window?.rootViewController = detailView
        
        UserDefaults.standard.set(0, forKey: "login")

        let center = UNUserNotificationCenter.current()//remove alarm
        center.removeAllPendingNotificationRequests()
        
        var id = getDataFromSheardPreferanceString(key: "userID")//remove topic
        if userInfo.loginType == "1" {
            if (id.firstIndex(of: "+") != nil){
                id = String(id.dropFirst())
            }
        }else if userInfo.loginType == "4"{
            id = user_ProgressHistory.refUserID.replacingOccurrences(of: ".", with: "_")
        }
      
        Messaging.messaging().unsubscribe(fromTopic: id) { error in
            
            print("unSubscribe to \(id) topic")
        }
        try? Auth.auth().signOut()
        createEvent(key: "17", date: getDateTime())
//        present(detailView, animated: true, completion: nil)
       
    }
    @IBAction func btnUpdatePeriod(_ sender: Any) {
        if userInfo.grown == "2" {
            alert(mes: "نعتذر غير متاحه لعمرك او حميتك", selfUI: self)
        }else if user_ProgressHistory.refPlanMasterID == "3"  {
            alert(mes: "نعتذر غير متاحه لعمرك او حميتك", selfUI: self)
        }else{
            viewPopuSlider(new_target: "-1.0")
                       view.alpha = 0.5
            
        }
    }
    @IBAction func btnUpdateTarhet(_ sender: Any) {
        if userInfo.grown == "2" {
            alert(mes: "نعتذر غير متاحه لعمرك او حميتك", selfUI: self)
        }else if user_ProgressHistory.refPlanMasterID == "3"  {
            alert(mes: "نعتذر غير متاحه لعمرك او حميتك", selfUI: self)
        }else{
            viewPopupTarget()
            view.alpha = 0.5
            
        }
    
    }
    @IBAction func btnSetting(_ sender: Any) {
        self.performSegue(withIdentifier: "SettingUserInfo", sender: self)
    }
    @IBAction func btnFavurit(_ sender: Any) {
        self.performSegue(withIdentifier: "favurit", sender: self)
}
    @IBAction func btnNotfy(_ sender: Any) {
        self.performSegue(withIdentifier: "NotificationListController", sender: self)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
        
    }

    
    func SetImage(laImage:UIImageView){
        laImage.layer.masksToBounds = false
        laImage.layer.cornerRadius = laImage.frame.height/2
        laImage.clipsToBounds = true
        
    }
    
    func SetStack(laImage:UIView){
        laImage.layer.borderWidth = 0.5
        laImage.layer.masksToBounds = false
        laImage.layer.borderColor = colorGray.cgColor
        laImage.layer.cornerRadius = 5
        laImage.alpha = 0.5
        laImage.clipsToBounds = true
        
    }
    
     func viewPopupTarget() {
        
        let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "PopuPController") as! PopuPController
        popoverContent.viewAccountPresnter = obpresnterAccount
        let nav = UINavigationController(rootViewController: popoverContent)
        nav.modalPresentationStyle = UIModalPresentationStyle.popover
        let popover = nav.popoverPresentationController
        popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
        popover!.delegate = self
        popover!.sourceView = self.view
        popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)
        popoverContent.popoverPresentationController?.passthroughViews?.removeAll()
        popover?.permittedArrowDirections = []
        
        self.present(nav, animated: true, completion: nil)

    }
  
    
    func viewPopuSlider(new_target:String) {

      
          let popoverContent = self.storyboard?.instantiateViewController(withIdentifier: "SliderPopupController") as! SliderPopupController
           popoverContent.new_target = Double(new_target)
          popoverContent.viewAccountPresnter = obpresnterAccount
          let nav = UINavigationController(rootViewController: popoverContent)
          nav.modalPresentationStyle = .popover
          let popover = nav.popoverPresentationController
          popoverContent.preferredContentSize = CGSize(width: UIScreen.main.bounds.width,height: 250)
        popoverContent.popoverPresentationController?.passthroughViews?.removeAll()
          popover!.delegate = self
          popover!.permittedArrowDirections = .up
          popover!.sourceView = self.view
        popover?.permittedArrowDirections = []
          popover!.sourceRect = CGRect(x: 100,y: 100,width: 0,height: 0)

          
          self.present(nav, animated: true, completion: nil)

      }
}



extension AccountController:viewAccount{
    func setNewTarget(val: String) {
       
        setTargetWeight(target:val)
        obpresnterAccount.showDialog()
        
        tblUserProgressHistory.updateInfoUserWeightTarget(target: val,date: getDateOnly()) { [self] (res, err) in
//            if res == "success" {
//                _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,ob:obpresnterAccount)
//                tblUserInfo.getMealDistrbutionUser()
//                tblUserInfo.updateMealPlanner { (s, e) in
                    obpresnterAccount.hidDialog()
//                    UIApplication.shared.endIgnoringInteractionEvents()
                    viewPopuSlider(new_target:val)
//                }
                
                
//            }
        }
        
    }
    
    func setAlpha(flag:Bool) {
        if flag == true {
//            obpresnterAccount.showDialog()
//            UIApplication.shared.beginIgnoringInteractionEvents()
//            _ = UpdateCalulater(item: setupRealm().objects(tblUserInfo.self).filter("id == %@",getDataFromSheardPreferanceString(key: "userID")).first!,ob:obpresnterAccount)
//            tblUserInfo.getMealDistrbutionUser()
//            tblUserInfo.updateMealPlanner { (s, e) in
                self.view.alpha = 1
//                UIApplication.shared.endIgnoringInteractionEvents()
//            }
        }else{
             view.alpha = 1
        }
    }

    
}
extension AccountController:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
