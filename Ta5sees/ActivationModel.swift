//
//  ActivationModel.swift
//  Ta5sees
//
//  Created by TelecomEnterprise on 25/03/2021.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import Foundation
class ActivationModel{
    internal init(refCusine: String = "", refWajbehMain: String = "", refWajbehNotMain: [String] = [String](), refTasbera: [String] = [String](), refMealDistrbution: [ModelMEalDistrbution] = [ModelMEalDistrbution](), arr_mealDes: [ModelMEalDistrbution] = [ModelMEalDistrbution](), refmealPlaners: [[String : Any]] = [[String:Any]](), dietType: String = "", BMI: Double = 0.0, weight: String = "", target_weight: String = "", refLayaqa: String = "", incrementKACL: String = "", week: String = "", ageChild: String = "", xtraCusine: [String] = [String](), refMeat: [String] = [String](), refVegetables: [String] = [String](), refFruit: [String] = [String](), TeeDistribution: Int = 0, endSubMilli: Int = 0, startSubMilli: Int = 0, subscribeType: Int = 0, expireDate: Double = 0.0, subDuration: Int = 0, startPeriod: Int = 0, endPeriod: Int = 0, removeCountItem: Int = 0,refAllergys:[String] = [String]()) {
        self.refCusine = refCusine
        self.refWajbehMain = refWajbehMain
        self.refWajbehNotMain = refWajbehNotMain
        self.refTasbera = refTasbera
        self.refMealDistrbution = refMealDistrbution
        self.arr_mealDes = arr_mealDes
        self.refmealPlaners = refmealPlaners
        self.dietType = dietType
        self.BMI = BMI
        self.weight = weight
        self.target_weight = target_weight
        self.refLayaqa = refLayaqa
        self.incrementKACL = incrementKACL
        self.week = week
        self.ageChild = ageChild
        self.refAllergys = refAllergys
        self.xtraCusine = xtraCusine
        self.refMeat = refMeat
        self.refVegetables = refVegetables
        self.refFruit = refFruit
        self.TeeDistribution = TeeDistribution
        self.endSubMilli = endSubMilli
        self.startSubMilli = startSubMilli
        self.subscribeType = subscribeType
        self.expireDate = expireDate
        self.subDuration = subDuration
        self.startPeriod = startPeriod
        self.endPeriod = endPeriod
        self.removeCountItem = removeCountItem
    }
    
    func setData() {
        self.refCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }.first!//Array(arrayLiteral: getUserInfo().refCuiseseID)[0]
        
        //        self.dietType = getUserInfo().refPlanMasterID
        //        self.BMI = 0
        //        self.weight = getUserInfo().weight
        //        self.target_weight = getUserInfo().target
        //        self.refLayaqa = getUserInfo().refLayaqaCondID
        //        self.incrementKACL = getUserInfo().gramsToLose
        //        self.ageChild = String(Int(round(calclateAge(str: getUserInfo().birthday))))
        self.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//Array(arrayLiteral: getUserInfo().refCuiseseID)
        self.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//Array(arrayLiteral: getUserInfo().refAllergyInfoID)
        //        self.TeeDistribution = Int(getUserInfo().mealDistributionId)!
        //        self.expireDate = Double(getUserInfo().expirDateSubscribtion)!
        //        self.subDuration = Int(getUserInfo().subDuration)!
        
        //        self.removeCountItem = 0
        
        getMealDistrbutionUser()
    }
    
    var refCusine = ""
    var refWajbehMain:String = ""
    var refWajbehNotMain = [String]()
    var refTasbera = [String]()
    var refMealDistrbution = [ModelMEalDistrbution]()
    var arr_mealDes = [ModelMEalDistrbution]()
    var refmealPlaners = [[String:Any]]()
    var dietType:String = ""
    var BMI:Double = 0.0
    var weight:String = ""
    var target_weight:String = ""
    var refLayaqa:String = ""
    
    var incrementKACL:String = ""
    var week:String = ""
    var ageChild:String = ""
    var xtraCusine = [String]()
    var refAllergys = [String]()
    var refMeat = [String]()
    var refVegetables = [String]()
    var refFruit = [String]()
    var TeeDistribution = 0
    
    
    var endSubMilli = 0
    var startSubMilli = 0
    var subscribeType = 0
    var expireDate = 0.0
    var subDuration = 0
    
    var startPeriod = 0
    var endPeriod = 0
    var removeCountItem = 0
    
    
    //    func deletItem(){
    //        ActivationModel.shared.refCusine = ""
    //        ActivationModel.shared.refWajbehMain = ""
    //        ActivationModel.shared.refWajbehNotMain = [String]()
    //        ActivationModel.shared.refTasbera = [String]()
    //        ActivationModel.shared.refMealDistrbution = [ModelMEalDistrbution]()
    //        ActivationModel.shared.arr_mealDes = [ModelMEalDistrbution]()
    //        ActivationModel.shared.refmealPlaners = [[String:Any]]()
    //        ActivationModel.shared.dietType = ""
    //        ActivationModel.shared.BMI = 0.0
    //        ActivationModel.shared.weight = ""
    //        ActivationModel.shared.target_weight = ""
    //        ActivationModel.shared.refLayaqa = ""
    //
    //        ActivationModel.shared.incrementKACL = ""
    //        ActivationModel.shared.week = ""
    //        ActivationModel.shared.ageChild = ""
    //        ActivationModel.shared.xtraCusine = [String]()
    //        ActivationModel.shared.refMeat = [String]()
    //        ActivationModel.shared.refVegetables = [String]()
    //        ActivationModel.shared.refFruit = [String]()
    //        ActivationModel.shared.TeeDistribution = 0
    //    }
    struct Static  {
        
        static var instance: ActivationModel?
    }
    
    class var sharedInstance: ActivationModel
    {
        if Static.instance == nil
        {
            Static.instance = ActivationModel()
        }
        
        return Static.instance!
    }
    
    func dispose()
    {
        ActivationModel.Static.instance = nil
        print("Disposed Singleton instance")
    }
    
    func saySomething()
    {
        print("Hi")
    }
    
    
    func getMealDistrbutionUser(){
        
        let item = setupRealm().objects(tblMealDistribution.self).filter("id == %@",Int(getUserInfo().mealDistributionId)!).first
        if item?.isLunchMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "2"
        }else if item?.isDinnerMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "3"
        }else if item?.isBreakfastMain == 1 {
            ActivationModel.sharedInstance.refWajbehMain = "1"
        }
        if item?.Breakfast == 1 && item?.isBreakfastMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("1")
        }
        if item?.Dinner == 1 && item?.isDinnerMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("3")
        }
        if item?.Lunch == 1 && item?.isLunchMain == 0 {
            ActivationModel.sharedInstance.refWajbehNotMain.append("2")
        }
        if item?.Snack1 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("4")
        }
        if item?.Snack2 == 1 {
            ActivationModel.sharedInstance.refTasbera.append("5")
        }
        ActivationModel.sharedInstance.subscribeType =  getUserInfo().subscribeType
        
        ActivationModel.sharedInstance.startSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Date().getDaysDate(value:  ActivationModel.sharedInstance.startSubMilli) / 1000.0)))
        
        //        ActivationModel.sharedInstance.endSubMilli =
        ActivationModel.sharedInstance.endSubMilli = Date().daysBetween(start: getDateOnlyasDate(), end:  Date(timeIntervalSince1970: TimeInterval(Date().getDaysDate(value:  ActivationModel.sharedInstance.endSubMilli) / 1000.0)))
        
        ActivationModel.sharedInstance.xtraCusine = getUserInfo().refCuiseseID.split(separator: ",").map { String($0) }//Array(arrayLiteral: getUserInfo().refCuiseseID)
        ActivationModel.sharedInstance.refAllergys = getUserInfo().refAllergyInfoID.split(separator: ",").map { String($0) }//Array(arrayLiteral: getUserInfo().refAllergyInfoID)
        
        ActivationModel.sharedInstance.refMeat = getUserInfo().meat.split(separator: ",").map { String($0) }
        ActivationModel.sharedInstance.refFruit = getUserInfo().frute.split(separator: ",").map { String($0) }
        ActivationModel.sharedInstance.refVegetables = getUserInfo().vegetabels.split(separator: ",").map { String($0) }
        
        
        
        print("hereyazan",ActivationModel.sharedInstance.refWajbehNotMain,ActivationModel.sharedInstance.refWajbehMain,ActivationModel.sharedInstance.refTasbera,ActivationModel.sharedInstance.startSubMilli,ActivationModel.sharedInstance.endSubMilli,ActivationModel.sharedInstance.refVegetables,ActivationModel.sharedInstance.refFruit,ActivationModel.sharedInstance.refMeat)
    }
}
