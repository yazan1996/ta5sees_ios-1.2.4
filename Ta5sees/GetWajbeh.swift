//
//  GetWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 1/28/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift

class WajbehModel {
    var wajbehItem:tblUserWajbehForWeek!
    var arraylist:[tblWajbehIngredietns] = []
    var resultelist:Results<tblWajbehIngredietns>!
    var resultelistW:Results<tblUserWajbehForWeek>!
    var responseMarkEaten:String!
    var arraylistW:[tblUserWajbehForWeek] = []
    
    func getWajbehDaily(wajbehInfo : String,date:String, completion: @escaping (tblUserWajbehForWeek?,Error?) -> Void) {
        wajbehItem = setupRealm().objects(tblUserWajbehForWeek.self).filter("userID == %@ AND date == %@ AND wajbehInfiItem == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!,date,wajbehInfo).first
        
        completion(wajbehItem,nil)
    }
    
    func getWajbehDailyIngridnts(refID : Int,fatInfo:Int, completion: @escaping ([tblWajbehIngredietns],Error?) -> Void) {
        tblWajbehIngredietns.getdataToutorial(refID:refID,fatInfo:fatInfo) { (response, error) in
            self.resultelist = (response) as? Results<tblWajbehIngredietns>
            self.arraylist = Array(self.resultelist)
        }
        completion(arraylist,nil)
        
    }
    
    
    func getWajbehDailyInfoItems(infoID : String, completion: @escaping ([tblUserWajbehForWeek],Error?) -> Void) {
        
        tblUserWajbehForWeek.getDataMealPlaner(wajbrhInfo:infoID) { (response, error) in // should be set id user
            self.resultelistW = (response) as? Results<tblUserWajbehForWeek>
            self.arraylistW = Array(self.resultelistW)
            
        }
        completion(arraylistW,nil)
    }
    
    
    //    func updateDataDaily(id:Int ,date : String,calorisBurned:Int,flag:String,mark:String, completion: @escaping (Any?,Error?) -> Void) {
    //        tblDailyWajbeh.updateDataDaily(date:date, calorisBurned: calorisBurned, flag:flag)
    //        { (res, err) in
    //            if res as? String == "true"{
    //                self.responseMarkEaten = res as? String
    //                print("Save Tamreen Burned : \(res!)")
    //                tblUserWajbehForWeek.UpdateRowMark(id:id, date: date, marke:mark)
    //                completion(self.responseMarkEaten,nil)
    //            }else{
    //                self.responseMarkEaten = res as? String
    //                print("Save Tamreen Burned : \(res!)")
    //                completion(nil,nil)
    //            }
    //        }
    //    }
    
    
}
class GetWajbeh {
    var obWajbehModel = WajbehModel()
    var view:setWajbehDailyView
    
    init(with view: setWajbehDailyView) {
        self.view = view
    }
    
    
    public func getWajbeh(wajbehInfo : String,date:String, completion: @escaping (Error?) -> Void) {
        
        self.obWajbehModel.getWajbehDaily(wajbehInfo: wajbehInfo,date:date, completion: { (tblUserWajbehForWeek,error) in
            if error == nil {
                if tblUserWajbehForWeek == nil {
                    self.view.showAlertMessage(message: "لا يوجد لديك وجبات في هذااليوم")
                }else{
                    self.view.setWajbeh(ob: tblUserWajbehForWeek!)
                }
            }
            completion(error)
        })
    }
    
    
    public func getWajbehIngredints(refID: Int,fatInfo:Int, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyIngridnts(refID: refID,fatInfo:fatInfo, completion: { (tblWajbehIngredietns,error) in
            if error == nil {
                self.view.setWajbehIngridents(obList:tblWajbehIngredietns)
            }
            completion(error)
        })
    }
    
    
    
    
    public func getListWajbehInfoItemsBreakfast(infoID: String, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyInfoItems(infoID: infoID) { (tblUserWajbehForWeek, error) in
            if error == nil {
                self.view.setListWajbehInfobreakFast(obList:tblUserWajbehForWeek)
            }
            completion(error)
        }
    }
    
    public func getListWajbehInfoItemsTasber1(infoID: String, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyInfoItems(infoID: infoID) { (tblUserWajbehForWeek, error) in
            if error == nil {
                self.view.setListWajbehInfoTasber1(obList:tblUserWajbehForWeek)
            }
            completion(error)
        }
    }
    
    public func getListWajbehInfoItemsLaunsh(infoID: String, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyInfoItems(infoID: infoID) { (tblUserWajbehForWeek, error) in
            if error == nil {
                self.view.setListWajbehInfoLaunsh(obList:tblUserWajbehForWeek)
            }
            completion(error)
        }
    }
    public func getListWajbehInfoItemsTasber2(infoID: String, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyInfoItems(infoID: infoID) { (tblUserWajbehForWeek, error) in
            if error == nil {
                self.view.setListWajbehInfoTasbera2(obList:tblUserWajbehForWeek)
            }
            completion(error)
        }
    }
    public func getListWajbehInfoItemsDinner(infoID: String, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyInfoItems(infoID: infoID) { (tblUserWajbehForWeek, error) in
            if error == nil {
                self.view.setListWajbehInfoDinner(obList:tblUserWajbehForWeek)
            }
            completion(error)
        }
    }
    
    
    //    public func setMarkEaten(wajbeh:tblUserWajbehForWeek,id:Int ,date:String, calorisBurned:Int, completion: @escaping (Error?) -> Void) {
    //        self.obWajbehModel.updateDataDaily(id: id, date: date, calorisBurned: calorisBurned, flag: "eaten", mark: "selected") { (res, error) in
    //            if res != nil {
    //
    //                self.obUserWajbehEaten.id =  self.obUserWajbehEaten.IncrementaID()
    ////                self.view.setImageMarkEaten(flag:res as! String)
    //                self.obUserWajbehEaten.date = date
    //                self.obUserWajbehEaten.userID = getDataFromSheardPreferanceString(key: "userID")
    //                self.obUserWajbehEaten.refItemID = String( wajbeh.refTblUser.id)
    //                 self.obUserWajbehEaten.isWajbeh = "0"
    //                self.obUserWajbehEaten.ratio = String(wajbeh.ratio)
    //                self.obUserWajbehEaten.kCal = wajbeh.refTblUser.wajbehTotalCaloriesHigh!
    //                self.obUserWajbehEaten.wajbehInfiItem = wajbeh.wajbehInfiItem
    //                self.obUserWajbehEaten.addItem(ob:self.obUserWajbehEaten ) { (bool, error) in
    //                    print("DONE")
    //                }
    //
    //            }
    //            completion(error)
    //        }
    //    }
    //
    
    func deleteWajbehEaten(wajbeh:tblUserWajbehForWeek,date:String,wajbehInfo:String){
        print("Deleteee")
        
        tblUserWajbehEaten.deletItem(itemid: wajbeh.refTblUser.id,date:wajbeh.date,wajbehInfo:wajbeh.wajbehInfiItem) { (bool) in
            if bool == true {
                tblDailyWajbeh.updateDataDaily(date:date, calorisBurned: Int(wajbeh.refTblUser!.wajbehTotalCaloriesHigh!)!, flag:"UnMark")
                { (res, err) in
                    if res as? String == "true"{
                        print("Save delet Burned : \(res!)")
                        self.view.setImageMarkEaten(bool:false)
                        return
                    }else{
                        print("not delet Burned : \(res!)")
                        return
                    }
                }
            }
        }
        
    }
    func addToEaten(wajbeh:tblUserWajbehForWeek,date:String){
        print("ADDD")
        let obUserWajbehEaten = tblUserWajbehEaten()
        obUserWajbehEaten.id =  obUserWajbehEaten.IncrementaID()
        obUserWajbehEaten.date = date
        obUserWajbehEaten.userID = getDataFromSheardPreferanceString(key: "userID")
        obUserWajbehEaten.refItemID = String( wajbeh.refTblUser.id)
        obUserWajbehEaten.isWajbeh = "0"
        obUserWajbehEaten.isProposal = "1"
        obUserWajbehEaten.ratio = String(wajbeh.ratio)
        obUserWajbehEaten.kCal = wajbeh.refTblUser.wajbehTotalCaloriesHigh!
        obUserWajbehEaten.wajbehInfiItem = wajbeh.wajbehInfiItem
        obUserWajbehEaten.addItem(ob:obUserWajbehEaten ) { (bool, error) in
            if bool == true {
                print("add To tblEaten 2 \(obUserWajbehEaten.kCal)")
                tblDailyWajbeh.updateDataDaily(date: obUserWajbehEaten.date, calorisBurned: Int(obUserWajbehEaten.kCal)!, flag:"eaten"){ (res, err) in
                    if res as? String == "true"{
                        print("Save Burned : \(res!)")
                        self.view.setImageMarkEaten(bool:true)
                    }else{
                        print("not Save Burned : \(res!)")
                    }
                }
            }
        }
        
    }
    func checkItemEaten(wajbeh:tblUserWajbehForWeek,wajbehInfo:String,date:String,isAdd:String){
        tblUserWajbehEaten.checkFoundItem(id: wajbeh.refTblUser.id, wajbehInfo: wajbehInfo, date: date) { (bool) in
            if isAdd == "true" {
                bool == false ? self.addToEaten(wajbeh:wajbeh,date:date) : self.deleteWajbehEaten(wajbeh: wajbeh, date: date, wajbehInfo: wajbehInfo)
            }else{
                self.view.setImageMarkEaten(bool:bool)
            }
        }
    }
    //    public func setUnMarkEaten(wajbeh:tblUserWajbehForWeek,id:Int ,date:String, calorisBurned:Int,completion: @escaping (Error?) -> Void) {
    //        self.obWajbehModel.updateDataDaily(id: id, date: date,calorisBurned: calorisBurned,flag:"UnMark", mark: "nonSeleted") { (res, error) in
    //            if res != nil {
    ////                self.view.setImageMarkEaten(flag:res as! String)
    //            }
    //            completion(error)
    //        }
    //    }
    //
}


class ADDManualWajbeh {
    
    var obWajbehModel = WajbehModel()
    var view:setWajbehDailyView
    
    init(with view: setWajbehDailyView) {
        self.view = view
    }
    
    func calculateManualQuantityWeightWajbeh(ob:tblPackeg,unit:Int,quatnity:Int) {
        DispatchQueue.main.async {
            self.view.setDateWajbeh(wahbeh:setupRealm().objects(tblPackeg.self).filter("id == %@",ob.id).first!,quatnity:quatnity,unit:unit)
        }
        
        //        DispatchQueue.main.async {
        //            self.getManualWajbehIngredints(refID: ob.id, fatInfo: Int(getDataFromSheardPreferanceString(key: "fatInfo"))!, ratio: quatnity) { (error) in
        //            }
        //        }
        
        
        
    }
    
    public func getNewWjbeh(idPackge:Int){
        let ob:tblPackeg = setupRealm().objects(tblPackeg.self).filter("id == %@",idPackge).first!
        print("Waj\(ob)")
        view.setDateWajbeh(wahbeh:ob,quatnity:1, unit: Int(round(Double(ob.weightPerServing!)!)))
    }
    
    public func getManualWajbehIngredints(refID: Int,fatInfo:Int,ratio:Int, completion: @escaping (Error?) -> Void) {
        self.obWajbehModel.getWajbehDailyIngridnts(refID: refID,fatInfo:fatInfo, completion: { (tblWajbehIngredietns,error) in
            if error == nil {
                self.view.setIngridentWithRatio(wahbeh:tblWajbehIngredietns,ratio:ratio)
            }
            completion(error)
        })
    }
}



class ADDManualPackege {
    
    var obWajbehModel = WajbehModel()
    var viewPackage:setPackageDailyView
    var plateID:[String]!
    var arrayPlates=[PlateArray]()
    var arrOrigin=[PlateArray]()
    var arrReloadPerc=[PlateArray]()
    var ArrayPlateUnit = [PlateArrayUnit]()
    init(with view: setPackageDailyView) {
        self.viewPackage = view
    }
    func calculateManualQuantityWeightWajbeh(ob:tblPackeg,unit:Int,quatnity:Int,arr:[PlateArray],defualtWeight:String) {
        //        DispatchQueue.main.async {
        self.viewPackage.setDataWajbeh(wahbeh:ob,quatnity:quatnity,unit:unit)
        
        if unit == 100 || unit == 1 {
            //            getIngrPlate2(idPackge:String(ob.id),quatnity:quatnity, unit: unit,defualtWeight:defualtWeight)
            
        }else{
            getIngrPlate(idPackge:String(ob.id),quatnity:quatnity, unit: unit, recipe: "")
        }
        //        reloadPieChart(arr:arr,unit:unit,quatnity:quatnity)
        
        //        }
    }
    
    
    func newCalculatePlatePerUint(id_package:String,listUnitEdite:[UnitEdite],asMealPlanner:Bool,flg_Cate:Int){
        
        if asMealPlanner {
            NewCalculatePerPlateUnitMealPlanner(idPackge:id_package,quatnity:1, listUnitEdite:listUnitEdite, id_Cate: flg_Cate )
        }else{
            NewCalculatePerPlateUnit(idPackge:id_package,quatnity:1, listUnitEdite:listUnitEdite )
            
        }
    }
    //    func reloadPieChart(arr:[PlateArray],unit:Int,quatnity:Int) {
    //        arrReloadPerc.removeAll()
    //        for id in arr {
    //            self.arrReloadPerc.append(PlateArray(plateMain:id.plateMain, percentge:(id.percentge*Double(unit)*Double(quatnity))/100.0, Ingr: id.Ingr))
    //        }
    //
    //        viewPackage.setReloadPercentage(arr:arrReloadPerc)
    //    }
    
    public func getNewWjbeh(idPackge:Int,asMealPlanner:Bool,flg_Cate:Int){
        let ob:tblPackeg? = setupRealm().objects(tblPackeg.self).filter("id == %@",idPackge).first ?? tblPackeg()
        viewPackage.setDataWajbeh(wahbeh:ob!,quatnity:1, unit: Int(round(Double(ob?.weightPerServing ?? "0")!)))
        print("ass \(flg_Cate)")
        if asMealPlanner {
            getIngrPlateMealPlanner(idPackge:String(idPackge), id_Cate: flg_Cate, recipe: ob?.recipe ?? "0")
        }else{
            getIngrPlate(idPackge:String(idPackge),quatnity:1, unit: Int(round(Double(ob?.weightPerServing ?? "0")!)), recipe: ob?.recipe ?? "0")
            
        }
        viewPackage.setOtherData(val1: ob?.preparingTime ?? "0", val2: ob?.recipeYield ?? "0", val3: ob?.totalWeight ?? "0")
    }
    
    public func getIngrPlateMealPlanner(idPackge:String,id_Cate:Int,recipe:String){
        var protein:Float = 0.0
        var fat:Float = 0.0
        var carbohydrate:Float = 0.0
        var grams:Float = 0.0
        var target:Float = 0.0
        var plateID:[String]!
        var total_caloris:Float = 0.0
        var total_caloris1:Float = 0.0
        var arrInfo = [Int]()
        var ids_Ingr = [String]()
        var restrectedAllergy = [String]()
        ArrayPlateUnit.removeAll()
        arrayPlates.removeAll()
        restrectedAllergy.append("\n")
        restrectedAllergy.append("ملحوظة:")
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            plateID = arrPlateID.removeDuplicates()
        }
        
        
        target = calculateBasedCalorisAllowdUser(idWajbeh:id_Cate)
        for id in plateID {
            tblPackageIngredients.getIngPlate(refIDPKG: idPackge, refIDPL: id) { (arr,arr1) in
                ids_Ingr = arr
                restrectedAllergy.append(contentsOf: arr1)
            }
            let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
            
            let percentge:Float = target * nutrationPlate!.Percentage
            tblPackageIngredients.getIngsPlat(packgID: idPackge, platID: id) { (arrIngredients) in
                print(idPackge,id,"arrIngredients",arrIngredients)
                let resulteNutrtion = getIngPlate(arr:arrIngredients, id_waj:idPackge,plateId: id, date: "nil", saveShoppingList: false)
                
                
                total_caloris1 += 0.0//Double(percentge)
                total_caloris += 0.0//Double(resulteNutrtion[0])
                let valKCal:Float = percentge * resulteNutrtion[4]
                
                print("valKCal",valKCal,percentge,resulteNutrtion)
                grams += 0.0//valKCal/Float(resulteNutrtion[0])
                //                print(id_Cate,"new tot gram \(valKCal/resulteNutrtion[0])," ,valKCal,percentge,target,nutrationPlate)
                protein += 0//0// calclatNuitrationFacts2(val1:percentge,val2:Float(resulteNutrtion[3]),val3:Float(resulteNutrtion[0]))
                
                fat += 0// calclatNuitrationFacts2(val1:percentge,val2:Float(resulteNutrtion[1]),val3:Float(resulteNutrtion[0]))
                
                carbohydrate += 0// calclatNuitrationFacts2(val1:percentge,val2:Float(resulteNutrtion[2]),val3:Float(resulteNutrtion[0]))
                
                for id in id_Ing_rep{
                    if ids_Ingr.contains(String(id)) {
                        if let index = ids_Ingr.firstIndex(of:"\(id)") {
                            ids_Ingr.remove(at:index)
                        }
                    }
                }
                self.arrayPlates.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id) ?? 0), percentge:Double((valKCal/Float(resulteNutrtion[0]))), Ingr: ids_Ingr))
                ArrayPlateUnit.append(PlateArrayUnit(plate_name: tblPlateItem.getPlatName(refID: Int(id)!), plate_id: Int(id)!, units: tblPackagePlateUnits.getUnits(PlateID: Int(id)!,PackageID: Int(idPackge)!),valGrams:valKCal/resulteNutrtion[0],package_id:Int(idPackge)!))
                listUnitEdite.append(UnitEdite(val_gram:0.0/*Int(valKCal/Float(resulteNutrtion[0]))*/, plate_id:id))
                self.arrOrigin.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id)!), percentge:round(Double((valKCal/resulteNutrtion[0]))), Ingr: ids_Ingr))
            }
            id_Ing_rep.removeAll()
            
        }
        
        //        print("total_caloris \(total_caloris) - protien \(protien) - cho\(cho) - fat\(fat)")
        
        arrInfo.append(Int(round(protein)))
        arrInfo.append(Int(round(fat)))
        arrInfo.append(Int(round(carbohydrate)))
        
        restrectedAllergy.append("\n")
        print("restrectedAllergy",restrectedAllergy.count)
        if restrectedAllergy.count < 4 {
            restrectedAllergy.removeAll()
            viewPackage.setRecipe(recipe: "\(recipe)", strRestrc: "")
        }else{
            viewPackage.setRecipe(recipe: "\(recipe) \(restrectedAllergy.joined(separator: "\n"))\n", strRestrc: restrectedAllergy.joined(separator: "\n"))
        }
        
        viewPackage.setIngrANDPate(plates:arrayPlates,origin:arrOrigin,weight:"0"/*String(Int(round(grams/1.0)))*/,arrInfo:arrInfo,quatnity:1, unit: 1,caloris:0/* Int(round(total_caloris1))*/,ArrayPlateUnit:ArrayPlateUnit)//caloris
        viewPackage.setDefualtWeight(weight: "\(String(Int(round(grams))))")
    }
    
    public func getIngrPlate(idPackge:String,quatnity:Int, unit: Int,recipe:String){
        arrayPlates.removeAll()
        listUnitEdite.removeAll()
        var kcal:Float = 0.0
        var grams:Float = 0.0
        var protien:Int = 0
        var fat:Int = 0
        var cho:Int = 0
        var caloris = 0
        var arrInfo = [Int]()
        var restrectedAllergy = [String]()
        ArrayPlateUnit.removeAll()
        restrectedAllergy.append("\n")
        restrectedAllergy.append("ملحوظة:")
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            self.plateID = arrPlateID.removeDuplicates()
        }
        
        let wajbeh = setupRealm().objects(tblPackeg.self).filter("id == %@",Int(idPackge)!).first!
        for id in self.plateID {
            
            tblPackageIngredients.getIngPlate(refIDPKG: idPackge, refIDPL: id) { (arr,arr1) in
                restrectedAllergy.append(contentsOf: arr1)
                
                let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
                caloris = Int(round(Double(wajbeh.calories!) ?? 1.0))
                let percentge = Float((caloris*quatnity)) * nutrationPlate!.Percentage
                
                
                print("percentge  \(percentge)")
                
                let valKCal:Float = percentge * Float(nutrationPlate!.weight)
                
                
                protien = 0//protien+calclatNuitrationFacts(val1:percentge,val2:Float(nutrationPlate!.Protein),val3:Float(nutrationPlate!.Calories))
                fat = 0//fat+calclatNuitrationFacts(val1:percentge,val2:Float(nutrationPlate!.Fat),val3:Float(nutrationPlate!.Calories))
                
                cho = 0//cho+calclatNuitrationFacts(val1:percentge,val2:Float(nutrationPlate!.Carbohydrate),val3:Float(nutrationPlate!.Calories))
                print("info \(nutrationPlate!.Calories) - \(nutrationPlate!.weight) - \(nutrationPlate!.Percentage)")
                
                print("plate gram \(valKCal/Float(nutrationPlate!.Calories))")
                let gramPerPlat = valKCal/Float(nutrationPlate!.Calories)
                grams = grams + gramPerPlat
                kcal = kcal+percentge
                self.arrayPlates.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id) ?? 0), percentge:Double((valKCal/Float(nutrationPlate!.Calories))), Ingr: arr))
                
                ArrayPlateUnit.append(PlateArrayUnit(plate_name: tblPlateItem.getPlatName(refID: Int(id)!), plate_id: Int(id)!, units: tblPackagePlateUnits.getUnits(PlateID: Int(id)!,PackageID: Int(idPackge)!),valGrams:gramPerPlat,package_id:Int(idPackge)!))
                listUnitEdite.append(UnitEdite(val_gram: 0.0/*Float(gramPerPlat)*/, plate_id:id))
                self.arrOrigin.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id)!), percentge:Double((valKCal/Float(nutrationPlate!.Calories))), Ingr: arr))
            }
            //        }
        }
        
        print("Float(kcal) \(Float(kcal))  - \(grams)")
        print("protien \(protien) -fat \(fat) -cho \(cho)")
        arrInfo.append(protien)
        arrInfo.append(fat)
        arrInfo.append(cho)
        restrectedAllergy.append("\n")
        
        print("restrectedAllergy",restrectedAllergy.count)
        if restrectedAllergy.count < 4 {
            restrectedAllergy.removeAll()
            viewPackage.setRecipe(recipe: "\(recipe)", strRestrc: "")
        }else{
            viewPackage.setRecipe(recipe: "\(recipe) \(restrectedAllergy.joined(separator: "\n"))\n", strRestrc: restrectedAllergy.joined(separator: "\n"))
        }
        
        
        viewPackage.setIngrANDPate(plates:arrayPlates,origin:arrOrigin,weight:"0"/*String(Int(round(grams/Float(quatnity))))*/,arrInfo:arrInfo,quatnity:quatnity, unit: unit,caloris:0/*Int(round(kcal))*quatnity*/,ArrayPlateUnit:ArrayPlateUnit)//caloris
        viewPackage.setDefualtWeight(weight: String(Int(round(grams/Float(quatnity)))))
        
        
    }
    
    public func NewCalculatePerPlateUnitMealPlanner(idPackge:String,quatnity:Int,listUnitEdite:[UnitEdite],id_Cate:Int){
        arrayPlates.removeAll()
        var total_caloris:Float = 0.0
        var grams:Float = 0.0
        var protien:Float = 0.0
        var fat:Float = 0.0
        var cho:Float = 0.0
        var arrInfo = [Int]()
        var count = 0
        var ids_Ingr = [String]()
        ArrayPlateUnit.removeAll()
        
        var curentWeigthAfterEdite:Float = 0.0
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            self.plateID = arrPlateID.removeDuplicates()
        }
        let wajbeh = setupRealm().objects(tblPackeg.self).filter("id == %@",Int(idPackge)!).first!
        
        for id in self.plateID {
            tblPackageIngredients.getIngPlate(refIDPKG: idPackge, refIDPL: id) { (arr,arr1) in
                ids_Ingr = arr
            }
            
            tblPackageIngredients.getIngsPlat(packgID: idPackge, platID: id) { (arrIngredients) in
                let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
                
                let resulteNutrtion = getIngPlate(arr:arrIngredients, id_waj:idPackge,plateId: id, date: "nil", saveShoppingList: false)
                var valKCal:Float!
                
                if listUnitEdite[count].plate_id == id {
                    valKCal = resulteNutrtion[0] * Float(listUnitEdite[count].val_gram)
                    
                }else{
                    let caloris1:Float = Float(wajbeh.calories!) ?? 1.0
                    let percentge:Float = caloris1 * nutrationPlate!.Percentage
                    let valKCal1:Float = percentge * resulteNutrtion[4]
                    let gramPerPlat:Float = valKCal1/resulteNutrtion[0]
                    
                    print("gramPerPlat1 \(gramPerPlat)")
                    valKCal = resulteNutrtion[0] * gramPerPlat
                }
                
                if valKCal.isNaN || valKCal.isInfinite {
                    valKCal = 1
                }
                var val:Float = valKCal/resulteNutrtion[4]
                if val.isNaN || val.isInfinite {
                    val = 1
                }
                total_caloris += val
                print("valKCal \(val) - total_caloris \(total_caloris)")
                
                
                let gramPerPlat:Float = valKCal/resulteNutrtion[0]
                grams = grams + gramPerPlat
                if listUnitEdite[count].plate_id == id {
                    curentWeigthAfterEdite = listUnitEdite[count].val_gram
                }else{
                    let caloris1:Float = Float(wajbeh.calories!)!
                    let percentge:Float = caloris1*Float(quatnity) * nutrationPlate!.Percentage
                    let valKCal1:Float = percentge * resulteNutrtion[4]
                    let gramPerPlat:Float = valKCal1/resulteNutrtion[0]
                    
                    curentWeigthAfterEdite = gramPerPlat
                }
                protien += calclatNuitrationFacts2(val1:resulteNutrtion[3],val2:curentWeigthAfterEdite,val3:resulteNutrtion[4])
                fat += calclatNuitrationFacts2(val1:resulteNutrtion[1],val2:curentWeigthAfterEdite,val3:resulteNutrtion[4])
                
                cho += calclatNuitrationFacts2(val1:resulteNutrtion[2],val2:curentWeigthAfterEdite,val3:resulteNutrtion[4])
                
                
                if protien.isNaN || protien.isInfinite {
                    protien = 1
                }
                
                if cho.isNaN || cho.isInfinite {
                    cho = 1
                }
                
                if fat.isNaN || fat.isInfinite {
                    fat = 1
                }
                for id in id_Ing_rep{
                    if ids_Ingr.contains(String(id)) {
                        if let index = ids_Ingr.firstIndex(of:"\(id)") {
                            ids_Ingr.remove(at:index)
                        }
                    }
                }
                
                self.arrayPlates.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id) ?? 0), percentge:Double((valKCal/resulteNutrtion[0])), Ingr: ids_Ingr))
                
                self.arrOrigin.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id)!), percentge:Double((valKCal/resulteNutrtion[0])), Ingr: ids_Ingr))
            }
            
            
            
            
            count+=1
            id_Ing_rep.removeAll()
        }
        
        
        print("total_caloris \(total_caloris) - protien \(protien) - cho\(cho) - fat\(fat) - weight \(String(Int(round(grams/Float(quatnity))))) - grams \(grams)")
        
        arrInfo.append(Int(round(protien)))
        arrInfo.append(Int(round(fat)))
        arrInfo.append(Int(round(cho)))
        viewPackage.setIngrANDPate(plates:arrayPlates,origin:arrOrigin,weight:String(Int(round(grams/Float(quatnity)))),arrInfo:arrInfo,quatnity:quatnity, unit: 1,caloris:Int(round(total_caloris)),ArrayPlateUnit:ArrayPlateUnit)
        viewPackage.setDefualtWeight(weight: String(Int(round(grams/Float(quatnity)))))
        
        
    }
    public func NewCalculatePerPlateUnit(idPackge:String,quatnity:Int,listUnitEdite:[UnitEdite]){
        arrayPlates.removeAll()
        
        print("listUnitEdite \(listUnitEdite.count)")
        var caloris:Float = 0.0
        var grams:Float = 0.0
        var protien:Int = 0
        var fat:Int = 0
        var cho:Int = 0
        var arrInfo = [Int]()
        var count = 0
        var curentWeigthAfterEdite = 0
        var ids_Ingr = [String]()
        ArrayPlateUnit.removeAll()
        
        tblPackageIngredients.getPlatID(refID: idPackge) { (arrPlateID) in
            self.plateID = arrPlateID.removeDuplicates()
        }
        let wajbeh = setupRealm().objects(tblPackeg.self).filter("id == %@",Int(idPackge)!).first!
        for id in self.plateID {
            
            tblPackageIngredients.getIngPlate(refIDPKG: idPackge, refIDPL: id) { (arr,arr1) in
                let nutrationPlate = setupRealm().objects(tblPlatePerPackage.self).filter("refPlateID == %@ && refPackageID == %@",Int(id)! ,Int(idPackge)!).first
                
                ids_Ingr = arr
                var valKCal:Float!
                
                if listUnitEdite[count].plate_id == id {
                    valKCal = Float(nutrationPlate!.Calories) * Float(listUnitEdite[count].val_gram)
                    print("gramPerPlat \(listUnitEdite[count].val_gram)")
                    
                }else{
                    let caloris1 = Float(wajbeh.calories!) ?? 1.0
                    let percentge = caloris1 * nutrationPlate!.Percentage
                    let valKCal1:Float = percentge * Float(nutrationPlate!.weight)
                    let gramPerPlat = valKCal1/Float(nutrationPlate!.Calories)
                    
                    print("gramPerPlat \(gramPerPlat)")
                    valKCal = Float(nutrationPlate!.Calories) * gramPerPlat
                }
                
                if valKCal.isNaN || valKCal.isInfinite {
                    valKCal = 1
                }
                
             
                var val = (valKCal/Float(nutrationPlate!.weight))
                
                if val.isNaN || val.isInfinite {
                    val = 1
                }
                caloris = caloris+val
                
                print("info \(nutrationPlate!.Calories) - \(listUnitEdite[0].val_gram) - \(nutrationPlate!.Percentage)")
                
                print("plate gram \(valKCal/Float(nutrationPlate!.Calories))")
                let gramPerPlat = valKCal/Float(nutrationPlate!.Calories)
                grams = grams + gramPerPlat
                if listUnitEdite[count].plate_id == id {
                    curentWeigthAfterEdite = Int(listUnitEdite[count].val_gram)
                }else{
                    let caloris1 = Int(round(Double(wajbeh.calories!) ?? 1.0))
                    let percentge = Float((caloris1*quatnity)) * nutrationPlate!.Percentage
                    let valKCal1:Float = percentge * Float(nutrationPlate!.weight)
                    let gramPerPlat = valKCal1/Float(nutrationPlate!.Calories)
                    
                    curentWeigthAfterEdite = Int(round(gramPerPlat))
                }
                
                protien = protien+calclatNuitrationFacts(val1:Float(nutrationPlate!.Protein),val2:Float(curentWeigthAfterEdite),val3:Float(nutrationPlate!.weight))
                fat = fat+calclatNuitrationFacts(val1:Float(nutrationPlate!.Fat),val2:Float(curentWeigthAfterEdite),val3:Float(nutrationPlate!.weight))
                
                cho = cho+calclatNuitrationFacts(val1:Float(nutrationPlate!.Carbohydrate),val2:Float(curentWeigthAfterEdite),val3:Float(nutrationPlate!.weight))
                
           
                for id in ids_Ingr{
                    if ids_Ingr.contains(String(id)) {
                        if let index = ids_Ingr.firstIndex(of:"\(id)") {
                            ids_Ingr.remove(at:index)
                        }
                    }
                }
                
                self.arrayPlates.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id) ?? 0), percentge:Double((valKCal/Float(nutrationPlate!.Calories))), Ingr: ids_Ingr))
                
                self.arrOrigin.append(PlateArray(plateMain: tblPlateItem.getPlatName(refID: Int(id)!), percentge:Double((valKCal/Float(nutrationPlate!.Calories))), Ingr: ids_Ingr))
            }
            count+=1
            ids_Ingr.removeAll()
        }
        
        print("protien \(protien) -fat \(fat) -cho \(cho) - caloris \(caloris)")
        arrInfo.append(protien)
        arrInfo.append(fat)
        arrInfo.append(cho)
        
        viewPackage.setIngrANDPate(plates:arrayPlates,origin:arrOrigin,weight:String(Int(round(grams/Float(quatnity)))),arrInfo:arrInfo,quatnity:quatnity, unit: 1,caloris:Int(round(caloris)),ArrayPlateUnit:ArrayPlateUnit)
        viewPackage.setDefualtWeight(weight: String(Int(round(grams/Float(quatnity)))))
        
        
    }
    
    
    
    
    
    
    func calclatNuitrationFacts(val1:Float,val2:Float,val3:Float)->Int{
        let valGram:Float = val1 * Float(val2)
        return Int(round(valGram/val3))
        
    }
    
    
}



class ADDManualSenfItem{
    
    var viewSenf:setSenfDailyView
    
    init(with view: setSenfDailyView) {
        self.viewSenf = view
    }
    func calculateManualQuantityWeightWajbeh(ob:tblSenf,unit:Int,quatnity:Int) {
        //        DispatchQueue.main.async {
        self.viewSenf.setDataSenf(wahbeh:ob,quatnity:quatnity,unit:unit)
        //        }
    }
    
    public func getNewSenf(idPackge:Int){
        let ob:tblSenf = setupRealm().objects(tblSenf.self).filter("id == %@",idPackge).first!
        viewSenf.setDataSenf(wahbeh:ob,quatnity:1, unit: Int(round(Double(ob.servingTypeWeight)!)))
        print("ob.discription1 \(ob)")
        getMainHesa(refMainHesa:Int(ob.HesaUnit) ?? 0, unit: String(round(Double(ob.servingTypeWeight)!)))
    }
    public func getMainHesa(refMainHesa:Int,unit:String){
        if refMainHesa != 0 {
            let ob:tblMainHesa? = setupRealm().objects(tblMainHesa.self).filter("id == %@",refMainHesa).first
            viewSenf.setMainHESA(item:ob!.discription, unit: unit)
        }else{
            viewSenf.setMainHESA(item:"غم", unit: unit)
        }
    }
    public func getMainHesa(refMainHesa:Int)->String{
        let ob:tblMainHesa = setupRealm().objects(tblMainHesa.self).filter("id == %@",refMainHesa).first!
        return ob.discription
    }
}


class ADDManualSenf { // unused
    
    var view:setManualSenf
    
    init(with view: setManualSenf) {
        self.view = view
    }
    public func getNewSenf(idPackge:Int,quantity:Double){
        view.setSenf(ob:setupRealm().objects(tblSenf.self).filter("id == %@",idPackge).first!,quantity:quantity)
    }
    
    
}

protocol setWajbehDailyView:AnyObject {
    func setWajbeh(ob:tblUserWajbehForWeek)
    func setWajbehIngridents(obList:[tblWajbehIngredietns])
    func setListWajbehInfobreakFast(obList:[tblUserWajbehForWeek])
    func setListWajbehInfoTasber1(obList:[tblUserWajbehForWeek])
    func setListWajbehInfoLaunsh(obList:[tblUserWajbehForWeek])
    func setListWajbehInfoTasbera2(obList:[tblUserWajbehForWeek])
    func setListWajbehInfoDinner(obList:[tblUserWajbehForWeek])
    func setImageMarkEaten(bool:Bool)
    func showAlertMessage(message:String)
    func setDateWajbeh(wahbeh:tblPackeg,quatnity:Int,unit:Int)
    func setIngridentWithRatio(wahbeh:[tblWajbehIngredietns],ratio:Int)
    
}

protocol setPackageDailyView:AnyObject {
    func setDataWajbeh(wahbeh:tblPackeg,quatnity:Int,unit:Int)
    func setIngrANDPate(plates:[PlateArray],origin:[PlateArray],weight:String,arrInfo:[Int],quatnity:Int, unit: Int,caloris:Int,ArrayPlateUnit: [PlateArrayUnit])
    func setRecipe(recipe:String,strRestrc:String)
    func setOtherData(val1:String,val2:String,val3:String)
    func setDefualtWeight(weight:String)
    
}

protocol setSenfDailyView:AnyObject {
    func setDataSenf(wahbeh:tblSenf,quatnity:Int,unit:Int)
    func setMainHESA(item:String,unit:String)
}
protocol setManualSenf:AnyObject {
    func setSenf(ob:tblSenf,quantity:Double)
    
}

