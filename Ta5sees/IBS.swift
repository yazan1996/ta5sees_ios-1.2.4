//
//  IBS.swift
//  Ta5sees
//
//  Created by Admin on 9/12/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import UIKit
import iOSDropDown
import TextFieldEffects
//import DLRadioButton
import RealmSwift
 
class IBS: UIViewController,UIViewControllerTransitioningDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Items[row]
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        Items.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item: tblIBSSymptoms  =  arraylist.filter { $0.discription == Items[row] }.first!
        print(item.id)
        setDataInSheardPreferance(value:String(item.id), key: nameMidicalTietType)
       
    }
    @IBAction func btnNext(_ sender: Any) {
        let detailView = storyboard!.instantiateViewController(withIdentifier: "FormatNaturalDiet") as! FormatNaturalDiet
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        present(detailView, animated: true, completion: nil)
    }
    @IBOutlet weak var datePicker: UIPickerView!
    var arraylist:[tblIBSSymptoms] = []
     var resultelist:Results<tblIBSSymptoms>!
    var refIDSelectList:String!
    var Items:[String]!
    

      
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
}
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontText(text:titlePage,size:30)
        titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        
        
    }
    
    var nameMidicalTietType:String!
    @IBOutlet weak var subView: UIView!
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        nameMidicalTietType = "ColonSyndrome"

        tblIBSSymptoms.getAllData() {(response, Error) in
            self.resultelist = (response) as? Results<tblIBSSymptoms>
            self.arraylist = Array(self.resultelist)
            self.Items = self.arraylist.map { $0.discription }
       
           setDataInSheardPreferance(value:"\(self.arraylist[2].id)", key: self.nameMidicalTietType)

                           
        }
        datePicker.selectRow(2, inComponent: 0, animated: true)


    }
    
    @IBOutlet weak var titlePage: UILabel!

 
    
    

    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
}



