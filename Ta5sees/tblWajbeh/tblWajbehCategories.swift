//
//  tblWajbehCategories.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehCategories:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refWajbehID = -2
    @objc dynamic var refCategoryID = -2
    let lines = LinkingObjects(fromType: tblWajbeh.self, property: "reftblWajbehCategories")
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
            let rj = ReadJSON()
            
            rj.readJson(tableName: "wajbeh/tblWajbehCategories") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblWajbehCategories = tblWajbehCategories()
                        data.id = item["id"].intValue
                        data.refWajbehID = item["refWajbehID"].intValue
                        data.refCategoryID = item["refCategoryID"].intValue
                        
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                         
                        }
                    }
                    completion(true)
                }
                }
        
        }
    }
    
    
}
