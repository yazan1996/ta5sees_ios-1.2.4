//
//  tblWajbeh.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//


import Foundation
import RealmSwift
import SwiftyJSON
import Realm
class tblWajbeh:Object {
    
    
    @objc dynamic var id:Int = -4
    @objc dynamic var discription:String? = ""
    @objc dynamic var wajbehProteinTotal:String? = ""
    @objc dynamic var wajbehFatTotal:String? = ""
    @objc dynamic var wajbehCarbTotal:String? = ""
    @objc dynamic var wajbehTotalCaloriesLow:String? = ""
    @objc dynamic var wajbehTotalCaloriesMedium:String? = ""
    @objc dynamic var wajbehTotalCaloriesHigh:String? = ""
    @objc dynamic var imgPath:String? = ""
    @objc dynamic var refSourceId:String? = ""
    @objc dynamic var source:String? = ""
    @objc dynamic var weight:String? = ""
    @objc dynamic var preparingTime:String? = ""
    @objc dynamic var recipeYield:String? = ""
    @objc dynamic var recipeInstruction:String? = ""
    @objc dynamic var isContainMilk:String? = ""
    @objc dynamic var isContainFriut:String? = ""
    @objc dynamic var isContainVegetables:String? = ""
    @objc dynamic var isContainOtherCHO:String? = ""
    @objc dynamic var isContainStarch:String? = ""
    @objc dynamic var isContainMeat:String? = ""
    @objc dynamic var isContainFat:String? = ""
    
    
    var reftblWajbehCuisenes = List<tblWajbehCuisenes>()
    var refWajbehIngredietns = List<tblWajbehIngredietns>()
    var reftblWajbehAllergy = List<tblWajbehAllergy>()
    var reftblWajbehSenfCatSub = List<tblWajbehSenfCatSub>()
    var reftblWajbehCategories = List<tblWajbehCategories>()
    var reftblWajbehRestrictedPlans =  List<tblWajbehRestrictedPlans>()
    var reftblWajbehInfoItems = List<tblWajbehInfoItems>()
    
    static var arraylistWajbeh:[tblWajbeh] = []
    static var resultelistWajbeh:Results<tblWajbeh>!
    
    
    static var lastWeekShoppintlist = getLastWeekIDShoppingList()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    static func KeyName() -> String
    {
        return primaryKey()!
    }
    
    static func getWajbehDetailing(milk:Int,refPlanID :Int,refCategoryID :Int,refWajbehInfoID:Int,friut:Int,vegetabels :Int,meat :Int,fat:Int,starch:Int,others :Int,totalserv :Int,responsEHendler:@escaping (Any?,Error?)->Void){
        var resultID = [Int]()
        
        
        
        
        let listQuery =  setupRealm().objects(tblWajbeh.self).filter("NONE reftblWajbehRestrictedPlans.refPlanID IN  %@ AND ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ AND ANY reftblWajbehCategories.refCategoryID IN %@ AND %@ <= numberOfMilkHesa AND %@ <= numberOfFatHesa AND %@ <= numberOfFriutHesa AND %@ <= numberOfStarchHesa AND %@ <= numberOfOtherCHOHesa AND %@ <= numberOfVegetablesHesa AND %@ <= numberOfMeatHesa ",[refPlanID],[refWajbehInfoID],[refCategoryID],milk,fat,friut,starch,others,vegetabels,meat).sorted(byKeyPath: "totalNumberOfHesa", ascending: false)
        ///limte 7
        for i in 0..<7 {
            resultID.append(listQuery[i].id)
        }
        
        responsEHendler(resultID,nil)
    }
    
    static func getdataJoin(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbeh.self),nil)
        //        responsEHendler(setupRealm()1.objects(tblWajbeh.self).filter("ANY reftblWajbehSenfCatSub.refSenfCatID IN %@",[refID]),nil)
    }
    
    
    static func getAllDataPerUser(idCusene:String,wajbehInfo:String,responsEHendler:@escaping (Any?,Error?)->Void){
        let userInfo = setupRealm().objects(tblUser.self).filter("id == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!).first
        
        tblWajbeh.getData(refID: Int(userInfo!.refPlanMaster)!,wajbehInfo:wajbehInfo,idCusene: idCusene) { (res,err) in
            responsEHendler(res,nil)
            
            
        }
    }
    
    static func getDataWeek(userID:Int,planMaster:Int,wajbehInfo:Int,fatInfo:String,persntge:Double,responsEHendler:@escaping (Any?,Error?)->Void){
        resultelistWajbeh = setupRealm().objects(tblWajbeh.self).filter("NONE reftblWajbehRestrictedPlans.refPlanID IN %@ AND ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ AND isContainStarch == %@ AND isContainMeat == %@ AND isContainFat == %@",[userID],[wajbehInfo],"1","1","1")
        arraylistWajbeh = Array(resultelistWajbeh)
        tblWajbeh.setDataINRealm(listQuery1: arraylistWajbeh, idUser: userID, wajbehInfo: wajbehInfo,fatInfo:fatInfo, persntge:persntge)
        responsEHendler(arraylistWajbeh,nil)
    }
    static func getDataWeekHypertensionANDCholesterolANDDiabetesANDIBS(userID:Int,planMaster:Int,wajbehInfo:Int,fatInfo:String,persntge:Double,responsEHendler:@escaping (Any?,Error?)->Void){
        
        resultelistWajbeh = setupRealm().objects(tblWajbeh.self).filter("NONE reftblWajbehRestrictedPlans.refPlanID IN %@ AND ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ AND isContainStarch == %@ AND isContainMeat == %@ AND isContainFat == %@ AND isContainOtherCHO == %@",[userID],[wajbehInfo],"1","1","1","0")
        arraylistWajbeh = Array(resultelistWajbeh)
        
        
        if planMaster == 6 {
            tblWajbeh.setDataINRealmHypertension(listQuery1: arraylistWajbeh, idUser: userID, wajbehInfo: wajbehInfo,fatInfo:fatInfo, persntge: persntge)
        }else  if planMaster == 8 {
            tblWajbeh.setDataINRealmCholesterol(listQuery1: arraylistWajbeh, idUser: userID, wajbehInfo: wajbehInfo,fatInfo:fatInfo, persntge: persntge)
        }else {
            tblWajbeh.setDataINRealm(listQuery1: arraylistWajbeh, idUser: userID, wajbehInfo: wajbehInfo,fatInfo:fatInfo, persntge: persntge)
            
        }
        
        
        responsEHendler(arraylistWajbeh,nil)
        
        
    }
    
    
    
    static func setDataINRealm(listQuery1:[tblWajbeh],idUser:Int,wajbehInfo:Int,fatInfo:String,persntge:Double){
        if listQuery1.count > 0{
            DispatchQueue.main.async {
                for x in  0..<listQuery1.count {
                    if x == 7 {
                        break
                    }
                    let caloriesMedium = Int(listQuery1[x].wajbehTotalCaloriesMedium!)
                    let caloriesHigh = Int(listQuery1[x].wajbehTotalCaloriesHigh!)
                    let caloriesLow = Int(listQuery1[x].wajbehTotalCaloriesLow!)
                    
                    let  energyTotal = Int(Double(getDataFromSheardPreferanceString(key: "newenergy"))!)
                    let valueRate:Int!
                    
                    valueRate = Int(round(Double(energyTotal) * persntge))
                    
                    let persentig = valueRate*Int(listQuery1[x].weight!)!
                    let ratio1 = persentig / caloriesMedium!
                    let ratio = ratio1 / Int(listQuery1[x].weight!)!
                    let obUserWajbehWeek:tblUserWajbehForWeek = tblUserWajbehForWeek()
                    
                    print(persentig/caloriesMedium!)
                    print(persentig/caloriesHigh!)
                    print(persentig/caloriesLow!)
                    obUserWajbehWeek.id = obUserWajbehWeek.IncrementaID()
                    obUserWajbehWeek.refTblUser = listQuery1[x]
                    obUserWajbehWeek.ratio = ratio
                    obUserWajbehWeek.date = getCurrentDate(flag:x)
                    obUserWajbehWeek.wajbehInfiItem = String(wajbehInfo)
                    obUserWajbehWeek.replaceID = obUserWajbehWeek.id
                    obUserWajbehWeek.userID = idUser
                    obUserWajbehWeek.fatInfo = fatInfo  // should be save in setupRealm()
                    obUserWajbehWeek.markEaten = "notSelected"
                    print("ratio : \(ratio) \(ratio1) \(persentig) \(energyTotal) \(valueRate!) ")
                    print("______\(x)_______")
                    try! setupRealm().write{
                        setupRealm().add(obUserWajbehWeek, update: Realm.UpdatePolicy.modified)
                    }
                    createShoppingList(ob:listQuery1[x],idUser:idUser, weekId: lastWeekShoppintlist)
                }
            }
            
        }
    }
    
    static func setDataINRealmHypertension(listQuery1:[tblWajbeh],idUser:Int,wajbehInfo:Int,fatInfo:String,persntge:Double){
        if listQuery1.count > 0{
            for x in  0..<listQuery1.count {
                if x == 7 {
                    break
                }
                var sum = 0
                if getDataFromSheardPreferanceString(key: "dietType") == "6" {
                    sum = getSumSodum(list:listQuery1[x])
                }
                if sum <= getBloodpressure(){
                    
                    let caloriesMedium = Int(listQuery1[x].wajbehTotalCaloriesMedium!)
                    let caloriesHigh = Int(listQuery1[x].wajbehTotalCaloriesHigh!)
                    let caloriesLow = Int(listQuery1[x].wajbehTotalCaloriesLow!)
                    
                    let  energyTotal = Int(Double(getDataFromSheardPreferanceString(key: "energyTotal"))!)
                    let valueRate:Int!
                    valueRate = Int(round(Double(energyTotal) * persntge))
                    
                    let persentig = valueRate*Int(listQuery1[x].weight!)!
                    let ratio1 = persentig / caloriesMedium!
                    let ratio = ratio1 / Int(listQuery1[x].weight!)!
                    let obUserWajbehWeek:tblUserWajbehForWeek = tblUserWajbehForWeek()
                    
                    print(persentig/caloriesMedium!)
                    print(persentig/caloriesHigh!)
                    print(persentig/caloriesLow!)
                    obUserWajbehWeek.id = obUserWajbehWeek.IncrementaID()
                    obUserWajbehWeek.refTblUser = listQuery1[x]
                    obUserWajbehWeek.ratio = ratio
                    obUserWajbehWeek.date = getCurrentDate(flag:x)
                    obUserWajbehWeek.wajbehInfiItem = String(wajbehInfo)
                    obUserWajbehWeek.replaceID = obUserWajbehWeek.id
                    obUserWajbehWeek.userID = idUser
                    obUserWajbehWeek.fatInfo = getDataFromSheardPreferanceString(key: "fatInfo") // should be save in setupRealm()
                    
                    print("ratio : \(ratio) \(ratio1) \(persentig) \(energyTotal) \(valueRate!) ")
                    print("______\(x)_______")
                    try! setupRealm().write{
                        setupRealm().add(obUserWajbehWeek, update: Realm.UpdatePolicy.modified)
                    }
                    createShoppingList(ob:listQuery1[x],idUser:idUser, weekId: lastWeekShoppintlist)
                }
                
            }
        }
    }
    
    
    static func setDataINRealmCholesterol(listQuery1:[tblWajbeh],idUser:Int,wajbehInfo:Int,fatInfo:String,persntge:Double){
        
        if listQuery1.count > 0{
            for x in  0..<listQuery1.count {
                if x == 7 {
                    break
                }
                var sum = 0
                if getDataFromSheardPreferanceString(key: "dietType") == "8" {
                    sum = getSumCholestrol(list:listQuery1[x])
                }
                if sum < 100{
                    
                    let caloriesMedium = Int(listQuery1[x].wajbehTotalCaloriesMedium!)
                    let caloriesHigh = Int(listQuery1[x].wajbehTotalCaloriesHigh!)
                    let caloriesLow = Int(listQuery1[x].wajbehTotalCaloriesLow!)
                    
                    let energyTotal = Int(Double(getDataFromSheardPreferanceString(key: "energyTotal"))!)
                    let valueRate:Int!
                    valueRate = Int(round(Double(energyTotal) * persntge))
                    
                    let persentig = valueRate*Int(listQuery1[x].weight!)!
                    let ratio1 = persentig / caloriesMedium!
                    let ratio = ratio1 / Int(listQuery1[x].weight!)!
                    let obUserWajbehWeek:tblUserWajbehForWeek = tblUserWajbehForWeek()
                    
                    print(persentig/caloriesMedium!)
                    print(persentig/caloriesHigh!)
                    print(persentig/caloriesLow!)
                    obUserWajbehWeek.id = obUserWajbehWeek.IncrementaID()
                    obUserWajbehWeek.refTblUser = listQuery1[x]
                    obUserWajbehWeek.ratio = ratio
                    obUserWajbehWeek.date = getCurrentDate(flag:x)
                    obUserWajbehWeek.wajbehInfiItem = String(wajbehInfo)
                    obUserWajbehWeek.replaceID = obUserWajbehWeek.id
                    obUserWajbehWeek.userID = idUser
                    obUserWajbehWeek.fatInfo = getDataFromSheardPreferanceString(key: "fatInfo") // should be save in setupRealm()
                    
                    print("ratio : \(ratio) \(ratio1) \(persentig) \(energyTotal) \(valueRate!) ")
                    print("______\(x)_______")
                    try! setupRealm().write{
                        setupRealm().add(obUserWajbehWeek, update: Realm.UpdatePolicy.modified)
                    }
                    createShoppingList(ob:listQuery1[x],idUser:idUser, weekId: 3)
                    
                }
                
            }
        }
    }
    static func getLastWeekIDShoppingList()->Int{
        return (setupRealm().objects(tblShoppingList.self).filter("userID == %@",Int(getDataFromSheardPreferanceString(key: "userID"))!).max(ofProperty: "weekID") as Int? ?? 0) + 1
    }
    
    static func createShoppingList(ob:tblWajbeh,idUser:Int,weekId:Int){
        
        if !ob.isEqual(nil) {
            
            DispatchQueue.main.async {
                
                for _ in 0..<ob.refWajbehIngredietns.count {
//                    tblShoppingList.createNewRow(userID:idUser,senfID:ob.refWajbehIngredietns[index].reftbltblSenf!.id,idWeek:weekId)
                    
                }
            }
            
        }
    }
    static func getBloodpressure()->Int{
        if getDataFromSheardPreferanceString(key: "Bloodpressure") == "1" {
            return 2500
            
        }else if getDataFromSheardPreferanceString(key: "Bloodpressure") == "2" {
            return 1250
            
        }else if getDataFromSheardPreferanceString(key: "Bloodpressure") == "3" {
            return 600
            
        }
        return 0
    }
    static func getSumCholestrol(list:tblWajbeh)->Int{
        var sum = 0
        for xx in  0..<list.refWajbehIngredietns.count {
            sum += Int(list.refWajbehIngredietns[xx].reftbltblSenf!.Cholestrol!)!
        }
        return sum
    }
    
    static func getSumSodum(list:tblWajbeh)->Int{
        var sum = 0
        for xx in  0..<list.refWajbehIngredietns.count {
            sum += Int(list.refWajbehIngredietns[xx].reftbltblSenf!.Sodium!)!
        }
        return sum
    }
    
    
    static func getCurrentDate(flag:Int)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result:String!
        let today = Date()
        if flag == 0 {
            result = formatter.string(from: today)
        }else{
            let nextDate = Calendar.current.date(byAdding: .day, value: flag, to: today)
            result = formatter.string(from: nextDate!)
            
        }
        return result
    }
    
    static func getdataToturial(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbeh.self).filter("id == %@",refID).first,nil)
    }
    static func getAllWajbeh(wajbehInfo:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbeh.self).filter("ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ ",[Int(wajbehInfo)!]),nil)
    }
    func getDailyWajbeh(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbeh.self).filter("id == %@",refID).first,nil)
    }
    
    static func getData(refID:Int,wajbehInfo:String,idCusene:String,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbeh.self).filter("NONE reftblWajbehRestrictedPlans.refPlanID IN %@ AND ANY reftblWajbehInfoItems.refWajbehInfoID IN %@ AND NONE reftblWajbehAllergy.refAllergyInfoID IN %@ AND ANY reftblWajbehCuisenes.refCuiseneID IN %@ ",[refID],[Int(wajbehInfo)],tblAllergyUserItems.getDataIDInt(id: getDataFromSheardPreferanceString(key: "userID")),[Int(idCusene)]),nil)
    }
    
   
    
    func readJson(completion: @escaping (Bool) -> Void){
       
        
        let rj = ReadJSON()
        rj.readJson(tableName: "wajbeh/tblWajbeh") {(response, Error) in
          
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblWajbeh = tblWajbeh()
                        data.id = item["id"].intValue
                        data.discription = item["description"].stringValue
                        data.wajbehTotalCaloriesLow = String(item["wajbehTotalCaloriesLow"].intValue)
                        data.wajbehTotalCaloriesHigh = String(item["wajbehTotalCaloriesHigh"].intValue)
                        data.wajbehTotalCaloriesMedium = String(item["wajbehTotalCaloriesMedium"].intValue)
                        data.isContainFat = String(item["isContainFat"].intValue)
                        data.isContainMeat = String(item["isContainMeat"].intValue)
                        data.isContainMilk = String(item["isContainMilk"].intValue)
                        data.isContainFriut = String(item["isContainFriut"].intValue)
                        
                        data.wajbehFatTotal = String(item["wajbehFatTotal"].intValue)
                        data.wajbehCarbTotal = String(item["wajbehCarbTotal"].intValue)
                        data.wajbehProteinTotal = String(item["wajbehProteinTotal"].intValue)
                        data.isContainStarch = String(item["isContainStarch"].intValue)
                        data.refSourceId = String(item["refSourceId"].intValue)
                        data.source = String(item["source"].intValue)
                        
                        data.preparingTime = String(item["preparingTime"].intValue)
                        data.weight = String(item["weight"].intValue)
                        data.imgPath = String(item["imgPath"].string ?? "null")
                        data.recipeYield = String(item["recipeYield"].intValue)
                        data.recipeInstruction = item["recipeInstruction"].string
                        
                        data.isContainOtherCHO = String(item["isContainOtherCHO"].intValue)
                        data.isContainVegetables! = String(item["isContainVegetables"].intValue)
                        
                        
                        
                        
                        
                        let itemSub:Results<tblWajbehSenfCatSub> = setupRealm().objects(tblWajbehSenfCatSub.self).filter("refWajbehID == %@",item["id"].intValue)
                        
                        data.reftblWajbehSenfCatSub = itemSub.reduce(List<tblWajbehSenfCatSub>()) { (list, element) -> List<tblWajbehSenfCatSub> in
                            list.append(element)
                            
                            return list
                        }
                       
                        let itemSubrefRestr:Results<tblWajbehRestrictedPlans> = setupRealm().objects(tblWajbehRestrictedPlans.self).filter("refWajbehID == %@",item["id"].intValue)
                        data.reftblWajbehRestrictedPlans = itemSubrefRestr.reduce(List<tblWajbehRestrictedPlans>()) { (list, element) -> List<tblWajbehRestrictedPlans> in
                            list.append(element)
                            
                            return list
                        }
                       
                        
                        
                        let results: Results<tblWajbehCategories> = setupRealm().objects(tblWajbehCategories.self).filter("refWajbehID == %@",item["id"].intValue)
                        data.reftblWajbehCategories = results.reduce(List<tblWajbehCategories>()) { (list, element) -> List<tblWajbehCategories> in
                            list.append(element)
                            
                            return list
                        }
                        
                        
                        
                        let itemSubrefInfoItems: Results<tblWajbehInfoItems> = setupRealm().objects(tblWajbehInfoItems.self).filter("refWajbehID == %@",item["id"].intValue)
                        data.reftblWajbehInfoItems = itemSubrefInfoItems.reduce(List<tblWajbehInfoItems>()) { (list, element) -> List<tblWajbehInfoItems> in
                            list.append(element)
                            
                            return list
                        }
                       
                        
                        let itemAllergy:Results<tblWajbehAllergy> = setupRealm().objects(tblWajbehAllergy.self).filter("refWajbehID == %@",item["id"].intValue)
                        data.reftblWajbehAllergy = itemAllergy.reduce(List<tblWajbehAllergy>()) { (list, element) -> List<tblWajbehAllergy> in
                            list.append(element)
                            
                            return list
                        }
                       
                        let itemCusinene:Results<tblWajbehCuisenes> = setupRealm().objects(tblWajbehCuisenes.self).filter("refWajbehID  == %@",item["id"].intValue)
                        data.reftblWajbehCuisenes = itemCusinene.reduce(List<tblWajbehCuisenes>()) { (list, element) -> List<tblWajbehCuisenes> in
                            list.append(element)
                            
                            return list
                        }
                        
                       
                        let ingredietns:Results<tblWajbehIngredietns> = setupRealm().objects(tblWajbehIngredietns.self).filter("refWajbehID == %@",item["id"].intValue)
                        data.refWajbehIngredietns = ingredietns.reduce(List<tblWajbehIngredietns>()) { (list, element) -> List<tblWajbehIngredietns> in
                            list.append(element)
                            
                            return list
                        }
                     
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                            
                            
                        }
                    }
                    completion(true)
                }
                
            
        }
    }
    
    
    
    static var arraylist:[tblWajbeh] = []
    static var resultelist:Results<tblWajbeh>!
    class func generateModelArray() -> [tblWajbeh]{
        var modelAry = [tblWajbeh]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblWajbeh.getdataJoin(refID:1) {(response, Error) in
            resultelist = (response) as? Results<tblWajbeh>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
    
    class func ModelArrayALLData(idCusene:String,wajbehInfo:String) -> [tblWajbeh]{
        var modelAry = [tblWajbeh]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblWajbeh.getAllDataPerUser(idCusene: idCusene,wajbehInfo: wajbehInfo) {(response, Error) in
            resultelist = (response) as? Results<tblWajbeh>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
    
    class func searchModelArray(wajbehInfo:String) -> [tblWajbeh] {
        var modelAry = [tblWajbeh]()
        modelAry.removeAll()
        arraylist.removeAll()
        tblWajbeh.getAllWajbeh(wajbehInfo:wajbehInfo) {(response, Error) in
            resultelist = (response) as? Results<tblWajbeh>
            arraylist = Array(resultelist)
        }
        
        return arraylist
    }
    
}


