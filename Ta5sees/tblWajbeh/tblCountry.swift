//
//  tblCountry.swift
//  Ta5sees
//
//  Created by Admin on 4/9/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON
class  tblCountry:Object {
    static var arraytblCountry:[tblCountry] = []
    static var listtblCountry:Results<tblCountry>!
    static var tblCountryDesc :[String:String]=[:]
    @objc dynamic var id = ""
    @objc dynamic var discription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
        
                    let rj = ReadJSON()
            var list = [tblCountry]()
            rj.readJson(tableName: "others/tblCountry") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let obj = tblCountry(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                        list.append(obj)
                        }
                    }
                        try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                
            }
                }
        }
    }
    func readFileJson(response:[JSON],completion: @escaping (Bool) -> Void){
        
            var list = [tblCountry]()
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response).array {
                    for item in recommends {
                        let obj = tblCountry(value: ["id" : String(item["id"].intValue), "discription": item["description"].string!])
                        list.append(obj)
                        }
                    }
                        try! setupRealm().write {
                            setupRealm().add(list, update: Realm.UpdatePolicy.modified)
                        }
                        completion(true)

                }
        }
    
    static func getData() ->Array<String>{
        var arrayDisc:[String]=[]
        
        listtblCountry = (setupRealm().objects(tblCountry.self))
        arraytblCountry = Array(listtblCountry)
        for x in 0..<listtblCountry.count {
            arrayDisc.append(listtblCountry[x].discription)
        }
        
        
        return arrayDisc
    }
    static func getDataID() ->Array<Int>{
        var arrayID:[Int]=[]
        
        listtblCountry = (setupRealm().objects(tblCountry.self))
        arraytblCountry = Array(listtblCountry)
        for x in 0..<listtblCountry.count {
            arrayID.append(Int(listtblCountry[x].id)!)
        }
        
        return arrayID
    }
    
}
