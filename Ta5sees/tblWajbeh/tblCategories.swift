//
//  tblCategories.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblCategories:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var refWajbehID = ""
    @objc dynamic var discription = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
  
    func readJson(completion: @escaping (Bool) -> Void){
        
            let rj = ReadJSON()
        var list = [tblCategories]()

            rj.readJson(tableName: "others/tblCategories") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblCategories = tblCategories()
                        data.id = String(item["id"].intValue)
                        data.refWajbehID = String(item["refWajbehID"].intValue)
                        data.discription = item["description"].string!
                        list.append(data)
                          
                        }
                    }
                        try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
            
                }
        }
    }
    
}

