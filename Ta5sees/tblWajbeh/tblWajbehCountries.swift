//
//  tblWajbehCountries.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehCountries:Object {
    
    @objc dynamic var id = -1
    @objc dynamic var refWajbehID = -1
    @objc dynamic var refCountryID = -1
    
    override static func primaryKey() -> String? {
        return "id"
    }
 
    func readJson(completion: @escaping (Bool) -> Void){
       
                    let rj = ReadJSON()
        var list = [tblWajbehCountries]()
            rj.readJson(tableName: "wajbeh/tblWajbehCountries") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.async {
                if let recommends = JSON(response!).array {
                    for item in recommends {
                        let data:tblWajbehCountries = tblWajbehCountries()
                        data.id = item["id"].intValue
                        data.refWajbehID = item["refWajbehID"].intValue
                        
                        data.refCountryID = item["refCountryID"].intValue
                        
                        list.append(data)
                  
                        }
                    }
                        try! setupRealm().write {

                    setupRealm().add(list, update: Realm.UpdatePolicy.modified)

                    completion(true)
                }
                }
        }
    }
    
    
}
