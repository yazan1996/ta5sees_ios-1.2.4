//
//  tblWajbehIngredietns.swift
//  Ta5sees
//
//  Created by Admin on 6/24/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehIngredietns:Object {
    
    @objc dynamic var id = -2
    @objc dynamic var refSenfCatID = -2
    @objc dynamic var refSenfID = 0
    @objc dynamic var refFatInfoId = 0
    @objc dynamic var refWajbehID = 0
    @objc dynamic var reftbltblSenf:tblSenf?
    @objc dynamic var isFixIngredietns = 0
    @objc dynamic var refHesaValuesID = 0
    //    @objc dynamic var refTblHesaValuesID:tblHesaValues?
    @objc dynamic var refTblMainHesa:tblMainHesa?
    
    static let shared = tblWajbehIngredietns()
    
    //    let lines = LinkingObjects(fromType: tblWajbeh.self, property: "reftblWajbehInfoItems")
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    static func getdataToutorial(refID:Int,fatInfo:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbehIngredietns.self).filter("refWajbehID = %@ AND reftbltblSenf.id == refSenfID AND refFatInfoId IN %@",refID,[1,fatInfo]),nil)
    }
    
    static func getIngredietnsNewWajbeh(refID:Int,responsEHendler:@escaping (Any?,Error?)->Void){
        responsEHendler(setupRealm().objects(tblWajbehIngredietns.self).filter("refWajbehID = %@ AND reftbltblSenf.id == refSenfID",refID),nil)
    }
    
    func getIngredietnsForWajbeh(refID:Int,callBack:([tblWajbehIngredietns])->Void){
        let list = setupRealm().objects(tblWajbehIngredietns.self).filter("refWajbehID = %@ AND reftbltblSenf.id == refSenfID",refID)
        callBack(Array(list))
    }
    
    func readJson(completion: @escaping (Bool) -> Void){

  
                    let rj = ReadJSON()
            rj.readJson(tableName: "wajbeh/tblWajbehIngredients") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                            thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let ob = tblWajbehIngredietns()
                        ob.id = item["id"].intValue
                        ob.refFatInfoId = item["refFatInfoID"].intValue
                        ob.refSenfID = item["refSenfID"].intValue
                        ob.refWajbehID = item["refWajbehID"].intValue
                        ob.refSenfCatID = item["refSenfCatID"].intValue
                        ob.isFixIngredietns = item["isFixIngredient"].intValue
                        ob.refHesaValuesID = item["refHesaValueID"].intValue
                        ob.reftbltblSenf = setupRealm().objects(tblSenf.self).filter("id = %@",item["refSenfID"].intValue).first
                        
                        //                    ob.refTblHesaValuesID = realm.objects(tblHesaValues.self).filter("id = %@",item["refHesaValueID"].intValue).first
                        
                        ob.refTblMainHesa = setupRealm().objects(tblMainHesa.self).filter("id = %@",item["reftbltblSenf.refMainHesaID"].intValue).first
                        
                            setupRealm().add(ob, update: Realm.UpdatePolicy.modified)
                         
                        }
                    }
                    completion(true)

                }
            }
        }
    }
}


