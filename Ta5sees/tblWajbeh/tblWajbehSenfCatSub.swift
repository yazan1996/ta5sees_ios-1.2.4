//
//  tblWajbehSenfCatSub.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehSenfCatSub:Object {
    @objc dynamic var id:Int = -4
    @objc dynamic var refWajbehID:Int = -4
    @objc dynamic var refSenfCatSubID:Int = -4
    @objc dynamic var refSenfCatID:Int = -4
    let lines = LinkingObjects(fromType: tblWajbeh.self, property: "reftblWajbehSenfCatSub")
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(completion: @escaping (Bool) -> Void){
     
            
            let rj = ReadJSON()
            
            rj.readJson(tableName: "wajbeh/tblWajbehSenfCatSub") {(response, Error) in
                let thread =  DispatchQueue.global(qos: .userInitiated)
                     thread.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblWajbehSenfCatSub = tblWajbehSenfCatSub()
                        data.id = item["id"].intValue
                        data.refWajbehID = item["refWajbehID"].intValue
                        data.refSenfCatSubID = item["refSenfCatSubID"].intValue
                        data.refSenfCatID = item["refSenfCatID"].intValue
                        
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                        
                        }
                    }
                    completion(true)
                }
             
            }
        }
    }
    
    
    
    //    func getdata()-> [String] {
    //        let realm = try! Realm()
    //        var array:[String] = []
    //        let objects = realm.objects(tblWajbehSenfCatSub.self).filter("refSenfCatID = '4'").randomElement()
    //        let idQuery = realm.objects(tblWajbeh.self).filter("id = '\(objects!.refWajbehID)'").randomElement()
    //
    //        array[0] = idQuery!.wajbehCaloriesTotal!
    //        array[1] = idQuery!.wajbehProteinTotal!
    //        array[2] = idQuery!.wajbehFatTotal!
    //        array[3] = idQuery!.wajbehCaloriesTotal!
    //
    //        return array
    ////        for i in 0..<1 {
    ////            let object = objects[i]
    ////               print(object)
    ////            // ...
    ////        }
    //
    //    }
    
}
