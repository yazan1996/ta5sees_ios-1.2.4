//
//  tblWajbehSefItems.swift
//  Ta5sees
//
//  Created by Admin on 4/23/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import SwiftyJSON

class tblWajbehSefItems:Object {
    
    @objc dynamic var id = ""
    @objc dynamic var refWajbehID = ""
    @objc dynamic var refSenfItemsID = ""
    @objc dynamic var quantity = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func readJson(){
       
            
            let rj = ReadJSON()
            rj.readJson(tableName: "tblWajbehSefItems") {(response, Error) in
                let d = DispatchQueue.global(qos: .userInitiated)
                           d.sync {
                if let recommends = JSON(response!).array {
                    try! setupRealm().write {
                    for item in recommends {
                        let data:tblWajbehSefItems = tblWajbehSefItems()
                        data.id = String(item["id"].intValue)
                        data.quantity = item["quantity"].string!
                        data.refWajbehID = String(item["refWajbehID"].intValue)
                        data.refSenfItemsID = String(item["refSenfItemsID"].intValue)
                            setupRealm().add(data, update: Realm.UpdatePolicy.modified)
                        }
                        
                    }
                }
         
            }
        }
    }
}

