//
//  PopuPControllerWieght.swift
//  Ta5sees
//
//  Created by Admin on 10/14/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//

import UIKit
import SVProgressHUD

class PopuPControllerWieght: UIViewController,UINavigationBarDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var firstNumber: UILabel!
    @IBOutlet weak var lastNumber: UILabel!
    var date:String!
    @IBAction func btnpdateWieght(_ sender: Any) {
//        CheckInternet.checkIntenet { (bool) in
//            if !bool {
//                SVProgressHUD.dismiss()
//                showToast(message: "لا يوجد اتصال بالانترنت", view: self.view, place: 0)
//                return
//            }
//        }
        updateWeightUser(date: date!,new_weight: "\(firstNumber.text!).\(lastNumber.text! )",view:view) { (bool) in
            showToast(message:"تم تحديث الوزن", view: self.view,place:0)
            print(bool)
        }
        tblUserProgressHistory.updateWeightHeightUser(weight:  "\(firstNumber.text!).\(lastNumber.text! )", height: "", date: getDateOnly()) {  (str, err) in
            if str == "success" {
               
            }else if str == "false" {
                SVProgressHUD.dismiss()
//                showToast(message:"لم يتم تحديث الوزن", view: self.view,place:0)
            }
        }
//        SVProgressHUD.show()
//        updateWeightUser(date: date, new_weight: "\(firstNumber.text!).\(lastNumber.text! )",view:view) { (bool) in
//            if bool {
//                self.presnter.removeItem(index: self.indexItem)
//                self.dismiss(animated: true, completion: nil)
//            }else{
//                alert(mes: "حدث خطا،يرجى المحاولة مجددا", selfUI: self)
//            }
//        }
    }
    var plusFlag = 0
    var MinusFlag = 0
    var indexItem = 0
    var viewAccountPresnter:PresnterAccount?
    var currentWeight:String!
    var weight:Double!
    var refPlan:String!
    var presnter:prisnterDataNotification!
    
    var userInfo:tblUserInfo!
    var user_ProgressHistory:tblUserProgressHistory!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        navigationController?.navigationBar.barTintColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        tabBarController?.tabBar.barTintColor = .white
        tabBarController?.tabBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = UIColor.green
        
        //        self.navigationItem.hidesBackButton = true
        
        getInfoUser { [self] (user, err) in
            self.userInfo = user
            tblUserHistory.getUserWeight(date: date, weight: user.weight) { (response, error) in
                self.currentWeight = (response as? String)!
                if  (self.currentWeight.firstIndex(of: ".") != nil) {
                    self.currentWeight = String(format: "%.1f", CGFloat(Double(self.currentWeight)!))
                    print("%%%")
                }
            }
        }
        
        getInfoHistoryProgressUser { (user, err) in
            self.user_ProgressHistory = user
        }
        
        if userInfo.grown != "2" {
            let newBackButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(backButtonTapped(sender:)))
            
            self.navigationItem.rightBarButtonItem = newBackButton
            self.navigationItem.rightBarButtonItem?.tintColor = colorGray
        }
        setUpLabel()
        firstNumber.font = firstNumber.font.withSize(30)
        lastNumber.font = firstNumber.font.withSize(40)
        firstNumber.alpha = 0.7
        lastNumber.alpha = 1
        plusFlag = 2
        MinusFlag = 2
    
        
        refPlan = user_ProgressHistory!.refPlanMasterID
        weight = Double(user_ProgressHistory.weight)
        if let index = self.currentWeight.firstIndex(of: ".") {
            firstNumber.text = "\( self.currentWeight.prefix(upTo: index))"
            let x = self.currentWeight.suffix(from: index)
            if x.firstIndex(of: ".") != nil {
                lastNumber.text = "\(x.dropFirst(1))"
            }else{
                lastNumber.text = "0"
            }
        }else{
            firstNumber.text = "\(self.currentWeight!)"
            lastNumber.text = "0"
            
            
        }
    }
    
    func checkPalnMaster(valu:Double)->Bool{
        if refPlan == "1" {
            if valu <= weight{
                return false
            }
        }else if refPlan == "2"{
            if valu >= weight{
                return false
            }
        }
        return true
    }
    @IBAction func btnMinus(_ sender: Any) {
        if MinusFlag == 1 {
            animationviewBottom(dataView: firstNumber)
            
            var num = Int(firstNumber.text!)
            let x = Double("\(num!-1).\(lastNumber.text!)")!
            if !checkPalnMaster(valu: x) {
                return
            }else{
                num!-=1
            }
            if num! < 0 {
                return
            }
            firstNumber.text = "\(num!)"
        }else if MinusFlag == 2{
            animationviewBottom(dataView: lastNumber)
            var num = Int(lastNumber.text!)
            if num!-1 < 0 {
                num = 10
                firstNumber.text! =  "\(Int(firstNumber.text!)!-1)"
                animationviewBottom(dataView: firstNumber)
            }
            let x = Double("\(firstNumber.text!).\(num!-1)")!
            if !checkPalnMaster(valu: x) {
                return
            }else{
                if num!-1 < 0 {
                    num = 9
                    firstNumber.text! =  "\(Int(firstNumber.text!)!-1)"
                    animationviewBottom(dataView: firstNumber)
                }else{
                    num!-=1
                    
                }
            }
            if num! < 0 {
                num = 9
                firstNumber.text! =  "\(Int(firstNumber.text!)!-1)"
                animationviewBottom(dataView: firstNumber)
            }
            lastNumber.text = "\(num!)"
        }
    }
    @IBAction func btnPlus(_ sender: Any) {
        if plusFlag == 1 {
            animationviewTop(dataView: firstNumber)
            var num = Int(firstNumber.text!)
            let x = Double("\(num!+1).\(lastNumber.text!)")!
            if !checkPalnMaster(valu: x) {
                return
            }else{
                num!+=1
            }
            if num! < 0 {
                return
            }
            firstNumber.text = "\(num!)"
        }else if plusFlag == 2{
            animationviewTop(dataView: lastNumber)
            var num = Int(lastNumber.text!)
            let x = Double("\(firstNumber.text!).\(num!+1)")!
            if !checkPalnMaster(valu: x) {
                return
            }
            if num! > 9 {
                num = 0
                animationviewTop(dataView: firstNumber)
                firstNumber.text! =  "\(Int(firstNumber.text!)!+1)"
            }
            lastNumber.text = "\(num!)"
        }
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func setUpLabel() -> Void {
        
        firstNumber.isUserInteractionEnabled = true
        lastNumber.isUserInteractionEnabled = true
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        firstNumber.addGestureRecognizer(tap1)
        lastNumber.addGestureRecognizer(tap2)
    }
    //updateWeightUser(date:String,new_weight:String)
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let tapped = sender.view!
        if tapped.tag == 1 {
            firstNumber.font = firstNumber.font.withSize(40)
            lastNumber.font = firstNumber.font.withSize(30)
            firstNumber.alpha = 1
            lastNumber.alpha = 0.7
            plusFlag = 1
            MinusFlag = 1
        }else if tapped.tag == 2{
            firstNumber.font = firstNumber.font.withSize(30)
            lastNumber.font = firstNumber.font.withSize(40)
            firstNumber.alpha = 0.7
            lastNumber.alpha = 1
            plusFlag = 2
            MinusFlag = 2
        }else{
            print("tap not found")
            
        }
    }
}
