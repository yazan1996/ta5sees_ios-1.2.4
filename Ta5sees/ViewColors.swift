//
//  ViewColors.swift
//  Ta5sees
//
//  Created by Admin on 4/11/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//



import UIKit

@IBDesignable class ViewColors: UIView {
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    func updateView() {
        let layer = self.layer as! CAGradientLayer
       
        layer.colors = [UIColor(red: 98/255, green: 230/255, blue: 155/255, alpha: 1.0) ,UIColor(red: 94/255, green: 204/255, blue: 156/255, alpha: 1.0) ].map {$0.cgColor}
        if (isHorizontal) {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint (x: 1, y: 0.5)
        } else {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint (x: 0.5, y: 1)
        }
    }
}
