//
//  GetListSearchItems.swift
//  Ta5sees
//
//  Created by Admin on 1/30/20.
//  Copyright © 2020 Telecom enterprise. All rights reserved.
//


import Foundation

class SearchModel {
    
    var item_name:String!
    var item_id:String!
    var dataListWajbeh : [tblPackeg] = [tblPackeg]()
    var dataListSenf : [tblSenf] = [tblSenf]()
    var i = 0
    
    func getListWajbehDB(wajbehInfoID:String,text:String, completion: @escaping ([tblPackeg],Error?) -> Void) {
        DispatchQueue.main.async {

            let whitespace = NSCharacterSet.whitespaces
            let range = text.self.rangeOfCharacter(from: whitespace)

            if range != nil {
//                print("whitespace  found")
//
//                let input =  text.components(separatedBy: " ")
//
//                    self.dataListWajbeh = tblPackeg.searchModelArray().filter({ (mod) -> Bool in
//                        for i in 0 ..< input.count {
//                            return mod.searchKey!.lowercased().contains(input[i])
//                        
//                        }
//                        return false
//                    })
//                

        print("Reload")
            }else {
//                self.dataListWajbeh = tblPackeg.searchModelArray().filter({ (mod) -> Bool in
////                    let components = mod.searchKey!.components(separatedBy: "،")
//
//                    return mod.searchKey!.lowercased().contains("\(text.lowercased().trimmingCharacters(in: .whitespaces))")
//
//
////                    return components.contains(where: { !text.contains($0) })
//
//
//
//
//
//
//                })
                print("whitespace not found")
            }
            
            
            
        }
        print("count \(self.dataListWajbeh.count)")
        completion(self.dataListWajbeh,nil)
        
    }
    
    func getListSenfDB(text:String, completion: @escaping ([tblSenf],Error?) -> Void) {
//        DispatchQueue.main.async {
//            self.dataListSenf = tblSenf.searchModelArray().filter({ (mod) -> Bool in
//                return mod.searchKeyword.lowercased().contains(text.lowercased())
//            })
//
//
//        }
        completion(self.dataListSenf,nil)
        
    }
    
}


class GetListSearchItems {
    
    var obSearchModel = SearchModel()
    var view:SearchViewResulte
    
    init(with view: SearchViewResulte) {
        self.view = view
    }
    
    
    public func getListWajbehItems(wajbehInfoID: String, text: String) {
        self.obSearchModel.getListWajbehDB(wajbehInfoID: wajbehInfoID, text: text, completion: { (tblPackeg,error) in
            if error == nil {
                self.view.setArrayWajbeh(dataList: tblPackeg)
            }else{
                print("##############")
                
            }
        })
    }
    
    
    public func getListSenfItems(text: String) {
        self.obSearchModel.getListSenfDB(text: text, completion: { (tblSenf,error) in
            if error == nil {
                self.view.setArraySenf(dataList: tblSenf)
            }else{
                print("##############")
                
            }
        })
    }
    
    
}


protocol SearchViewResulte:AnyObject {
    func setArrayWajbeh(dataList : [tblPackeg])
    func setArraySenf(dataList : [tblSenf])
    
}
