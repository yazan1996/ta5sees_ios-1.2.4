//
//  newRegesterStep9.swift
//  Ta5sees
//
//  Created by Admin on 8/27/1398 AP.
//  Copyright © 1398 Telecom enterprise. All rights reserved.
//

import Foundation

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON
import SwiftyCodeView
import Firebase
import FirebaseAuth

class FormatDiabetsDiet2: UIViewController ,UIViewControllerTransitioningDelegate {
    
    let header = HTTPHeaders()
    var idFetnessRate:Int!
    var week:Float = 0.0
    var energyTotal = 0
    var tee:Float!
    let ageChild = getDataFromSheardPreferanceFloat(key: "dateInYears")
    let weight = getDataFromSheardPreferanceFloat(key: "txtweight")
    let gender = getDataFromSheardPreferanceString(key: "gender")
    let height = getDataFromSheardPreferanceFloat(key: "txtheight")
    let headers = HTTPHeaders()
    //    lazy var  obAPiSubscribeUser = APiSubscribeUser(with: self)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
//        setBackground()
        
        
    }
    
    
    @IBOutlet weak var titlePage: UILabel!
    @IBOutlet weak var imageDiet: UIImageView!
    @IBOutlet weak var lblLayaqaCondition: UILabel!
    @IBOutlet weak var lblAlergy: UILabel!
    @IBOutlet weak var lblCusine: UILabel!
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        let tappedImageView = gestureRecognizer.view!
        if tappedImageView.tag == 1 {
            
            self.performSegue(withIdentifier: "selectioncusine2", sender: ["title": "انواع المطابخ", "options": lblCusine.text,"flag":"1"])
        }else if tappedImageView.tag == 2{
            self.performSegue(withIdentifier: "selectioncusine2", sender: ["title": "انواع الحساسيات", "options": lblAlergy.text,"flag":"2"])
        }else{
            self.performSegue(withIdentifier: "selectioncusine2", sender: ["title": "اختر معدل  اللياقة", "options": lblLayaqaCondition.text,"flag":"3"])
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //  flag 1 >>> cusin
        // flag 2 >>> alleagy
        // flag 3 >>> layaCondition
        if let dis=segue.destination as? MultiSeletetionView {
            if  let dictionary=sender as? [String:String] {
                dis.title_Navigation = dictionary["title"]
                dis.obselectionDelegate = self
                dis.optionsSelected = dictionary["options"]
                dis.flag = dictionary["flag"]
            }
        }
    }
    
    func clicableLabel(lbl:UILabel,tag:Int){
        lbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        lbl.tag = tag
        lbl.isUserInteractionEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontText(text:titlePage,size:30)
        titlePage.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
        imageDiet.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
        clicableLabel(lbl:lblCusine,tag:1)
        clicableLabel(lbl:lblAlergy,tag:2)
        clicableLabel(lbl:lblLayaqaCondition,tag:3)
        
        
    }
    
    func showAlert(str:String){
        let alert = UIAlertController(title: "خطأ", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if lblLayaqaCondition.text! == "اختر معدل اللياقة" {
            showAlert(str:"يجب اختيار معدل اللياقة")
        }else if lblAlergy.text! == "أختر حساسية الطعام-اختيار متعدد"{
            showAlert(str:"يجب اختيار نوع الحساسية")
        }else {
            if getDataFromSheardPreferanceString(key: "UserAgeID") == "2" {
                calculateNormalDietChildDiabites()
            }else{
                calculateNormalDietAdultDiabites()
            }
        }
        
    }
    
    
    
    
    
    
    
    
    @objc func buttonAction(sender: UIButton!) {
        if sender.tag == 4 {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func CalulateTEEChild(v1:Float,v2:Float,v3:Float,v4:Float,v5:Float)-> Float{
        let sum1 = v2 * getDataFromSheardPreferanceFloat(key: "dateInYears")
        let sum2 = (v3 * weight)
        let sum5 = ((height/100) * v4)
        let sum6 = sum5 + sum2
        let sum3 = Float(getRatePA(id:idFetnessRate)) * sum6
        let sum4 = v1 - sum1 + sum3 + v5
        return sum4
    }
    
    
    func processDietOrderForTEEDiabetes(tee:Float)->Int{
        let bmi = getDataFromSheardPreferanceFloat(key: "bmi")
        if bmi <= 18.5  {
            return Int(round(tee + 500))
        }else if bmi > 18.5 && bmi <= 24.9  {
            return Int(round(tee))
        }else if bmi >= 25.0 && bmi <= 29.9  {
            return Int(round(tee - 500))
        }else if bmi >= 30.0 && bmi <= 39.9{
            return Int(round(tee - 500))
        }else if bmi >= 40.0 {
            return Int(round(tee - 500))
            
        }
        return 1
    }
    
    
    
    func processDietOrderForTEEChild(tee:Float)->Int{
        let oldyear = Int(getDataFromSheardPreferanceFloat(key: "dateInYears"))
        let bmi = Float(getDataFromSheardPreferanceString(key: "bmi"))!
        var ob = tblCDCGrowthChild()
        tblCDCGrowthChild.getBMIChild(age: oldyear, bmi: bmi ) { (respose, error) in
            ob = respose as! tblCDCGrowthChild
        }
        
        if ob.value >= 25 && ob.value <= 75 {
            return Int(round(tee))
        }else if ob.value >= 5 && ob.value <= 25   {
            return Int(round(tee + 500))
        }else if ob.value < 5 {
            return Int(round(tee + 500))
        }else if ob.value > 75 {
            return Int(round(tee - 500))
        }
        return 0
    }
    
    func getRatePA(id:Int)->Float{
        let gender = String(Int(getDataFromSheardPreferanceFloat(key: "gender")))
        
        var ob = tblLayaqaCond()
        tblLayaqaCond.getRate(refID: id) { (response, error) in
            ob = response as! tblLayaqaCond
        }
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1"  {
            return Float(ob.PAadult)
        }else{
            if gender == "1"  {
                return Float(ob.PAchildmale)
            }else {
                return Float(ob.PAchildfemale)
            }
            
            
            /*
             if ageChild >= 1.0  && ageChild <= 3.0  {
             return Float(ob.PAadult)
             }else if ageChild >= 4.0  && ageChild <= 8.0  && gender == "1" {
             return Float(ob.PAchildmale)
             }else if ageChild >= 4.0  && ageChild <= 8.0  && gender == "2" {
             return Float(ob.PAchildfemale)
             }else if ageChild >= 9.0  && ageChild <= 18.0  && gender == "1" {
             return Float(ob.PAchildmale)
             }else if ageChild >= 9.0  && ageChild <= 18.0  && gender == "2"{
             return Float(ob.PAchildfemale)
             }
             */
        }
    }
    
    
    func getservMenuPlan(id:Int){
        
        var ob = tblMenuPlanExchangeSystem()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID: id,isChild:0) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
            
        }else{
            tblMenuPlanExchangeSystem.getMenuPlanExchangeSystem(refID:id,isChild:1) { (response, error) in
                ob = response as! tblMenuPlanExchangeSystem
            }
        }
        
        calculateServingGeneral(m:ob.milk,f:ob.fruit,v:ob.vegetables, energy: energyTotal)
    }
    
    func calculateServingGeneral(m:Int,f:Int,v:Int,energy:Int){
        _ = CalculationServing( malik: m, fruit: f, vg: v, fat: Int(getDataFromSheardPreferanceString(key: "fat"))!, pro: Int(getDataFromSheardPreferanceString(key: "pro"))!, carb: Int(getDataFromSheardPreferanceString(key: "carbo"))!, energy: energy,dietType:getDataFromSheardPreferanceString(key: "dietType"), FBG: Float(getDataFromSheardPreferanceFloat(key: "FBG")))
        
        ObjectMealDistrbution.id = ObjectMealDistrbution.filterMealDistrbution().id
        ObjectMealDistrbution.meals = ObjectMealDistrbution.filterMealDistrbution()
        
        //        self.obAPiSubscribeUser.subscribeUser { (bool) in
        //            if bool {
        //                    self.obAPiSubscribeUser.filterMealDistrbution()
        self.performSegue(withIdentifier: "goToToturial", sender: self)
        
        //            }
        //        }
        
        
        
    }
    
    func getTeeDistribution(energy:Int,str:String){
        var ref = Int(str)
                
        if getDataFromSheardPreferanceString(key: "cusinene") == "8" {
            ref = 8
        }
        var ob = tblTEEDistribution()
        if getDataFromSheardPreferanceString(key: "UserAgeID") == "1" {
            ob.getTeeDistrbution(refID: ref!,isChild:0) { (res, err) in
                ob = res as! tblTEEDistribution
                
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }else{
            ob.getTeeDistrbution(refID: ref!,isChild:1) { (res, err) in
                let ob = res as! tblTEEDistribution
                self.TeeDistribution(f:Float(ob.Fat),c:Float(ob.Carbo),p:Float(ob.Protien),energy:Float(energy))
            }
        }
        
    }
    
    
    
    
    func TeeDistribution(f:Float,c:Float,p:Float,energy:Float){
        let fat = (((f/100) * round(energy))/9)
        let carbo = (((c/100) *  round(energy))/4)
        let pro = (((p/100) *  round(energy))/4)
        setDataInSheardPreferance(value: "CC", key: String(c/15))
        setDataInSheardPreferance(value: "KCALTo1CC", key: String(energy/(c/15)))
        
        
        
        setDataInSheardPreferance(value:String(Int(round(fat))),key:"fat")
        setDataInSheardPreferance(value:String(Int(round(carbo))),key:"carbo")
        setDataInSheardPreferance(value:String(Int(round(pro))),key:"pro")
        
    }
    
    
    func calculateNormalDietChildDiabites(){
        if ageChild >= tblChildAges.range1StartFrom(id:1) && ageChild <= tblChildAges.range1EndTo(id:1){
            let sub1 = (89.0 * weight  - 100.0)
            tee = sub1 + 20.0 // tee -> eer
        }else if ageChild >= tblChildAges.range2StartFrom(id:2) && ageChild <= tblChildAges.range2EndTo(id:2) && gender == "1" {
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:20)
        }else if ageChild >= tblChildAges.range2StartFrom(id:2) && ageChild <= tblChildAges.range2EndTo(id:2)  && gender == "2" {
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:20)
        }else if ageChild >= tblChildAges.range3StartFrom(id:3)  && ageChild <= tblChildAges.range3EndTo(id:3) && gender == "1" {
            tee = CalulateTEEChild(v1:88.5,v2:61.9,v3:26.7,v4:903,v5:25)
        }else if ageChild >= tblChildAges.range3StartFrom(id:3)  && ageChild <= tblChildAges.range3EndTo(id:3)  && gender == "2"{
            tee = CalulateTEEChild(v1:135.3,v2:30.8,v3:10,v4:934,v5:25)
            
        }
        
        print("tee : \(tee ?? 00.0) \(ageChild)")
        setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
        
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            //            if getDataFromSheardPreferanceString(key: "typeDiabites") == "1" {
            
            if tblInsulinMedicines.getItem(id:Int(getDataFromSheardPreferanceString(key: "typeMedication"))!) == 1 {
                energyTotal = processDietOrderForTEEChild(tee: tee)
                //500 bmi
                
                let insolin1 = getDataFromSheardPreferanceFloat(key: "insolin1")
                let insoli2 = getDataFromSheardPreferanceFloat(key: "insolin2")
                setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
                setDataInSheardPreferance(value:String(500.0/(insolin1+insoli2)), key: "TDD500")
                getTeeDistribution(energy:energyTotal,str:"20")
                getservMenuPlan(id:20)
                
                
            }else {
                setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
                energyTotal = processDietOrderForTEEChild(tee:tee)
                getTeeDistribution(energy:energyTotal,str:"3")
                getservMenuPlan(id:3)
                
                
            }
            //            }
        }else {
            //            if getDataFromSheardPreferanceString(key: "typeDiabites") == "1" {
            
            if tblInsulinMedicines.getItem(id:Int(getDataFromSheardPreferanceString(key: "typeMedication"))!) == 1 {
                energyTotal = processDietOrderForTEEChild(tee: tee)
                
                
                let insolin1 = getDataFromSheardPreferanceFloat(key: "insolin1")
                let insoli2 = getDataFromSheardPreferanceFloat(key: "insolin2")
                setDataInSheardPreferance(value:String(500.0/(insolin1+insoli2)), key: "TDD500")
                setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
                getTeeDistribution(energy:energyTotal,str:"20")
                getservMenuPlan(id:20)
                
                
            }else {
                energyTotal = processDietOrderForTEEChild(tee:tee)
                setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
                getTeeDistribution(energy:energyTotal,str:"3")
                getservMenuPlan(id:3)
                
            }
            //            }
            //            else if getDataFromSheardPreferanceString(key: "typeDiabites") == "3"  {
            //                //based on mobnth
            //                if getDataFromSheardPreferanceString(key: "PregancyMonth") == "3" {
            //                    energyTotal = Int(tee + 452)
            //                }else if getDataFromSheardPreferanceString(key: "PregancyMonth") == "2" {
            //                    energyTotal = Int(tee + 350)
            //                }else if getDataFromSheardPreferanceString(key: "PregancyMonth") == "1" {
            //                    energyTotal = Int(tee)
            //                }
            //                setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
            //                getTeeDistribution(energy:energyTotal,str:"3")
            //                getservMenuPlan(id:3)
            //
            //            }
            
        }
        
    }
    
    
    
    func calculateNormalDietAdultDiabites(){
        var bmr:Float
        if getDataFromSheardPreferanceString(key: "gender") == "1" {
            
            bmr = Float(24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }  else{
            bmr = Float(0.9 * 24.0 * (getDataFromSheardPreferanceString(key: "txtweight") as NSString).doubleValue)
        }
        
        let pa:Float = Float(getRatePA(id:idFetnessRate)) * bmr
        let tef = (0.1 * (bmr + pa))
        tee = tef + pa + bmr
        
        
        if getDataFromSheardPreferanceString(key: "gender") == "1"{
            if getDataFromSheardPreferanceString(key: "typeDiabites") == "1" {
                
                if tblInsulinMedicines.getItem(id:Int(getDataFromSheardPreferanceString(key: "typeMedication"))!) == 1 {
                    energyTotal = processDietOrderForTEEDiabetes(tee: tee)
                    //500 bmi
                    
                    let insolin1 = getDataFromSheardPreferanceFloat(key: "insolin1")
                    let insoli2 =  getDataFromSheardPreferanceFloat(key: "insolin2")
                    setDataInSheardPreferance(value:String(500.0/(insolin1+insoli2)), key: "TDD500")
                    getTeeDistribution(energy:energyTotal,str:"20")
                    getservMenuPlan(id:20)
                    
                    
                }else {
                    energyTotal = processDietOrderForTEEDiabetes(tee:tee)//500 bmi
                    getTeeDistribution(energy:energyTotal,str:"3")
                    getservMenuPlan(id:21)
                    
                    
                }
            }else {//500 bmi
                energyTotal = processDietOrderForTEEDiabetes(tee:tee)
                getTeeDistribution(energy:energyTotal,str:"3")
                getservMenuPlan(id:21)
                
            }
        }else {
            if getDataFromSheardPreferanceString(key: "typeDiabites") == "1" {
                
                if tblInsulinMedicines.getItem(id:Int(getDataFromSheardPreferanceString(key: "typeMedication"))!) == 1 {
                    energyTotal = processDietOrderForTEEDiabetes(tee: tee)
                    
                    let insolin1 = getDataFromSheardPreferanceFloat(key: "insolin1")
                    let insoli2 = getDataFromSheardPreferanceFloat(key: "insolin2")
                    setDataInSheardPreferance(value:String(500.0/(insolin1+insoli2)), key: "TDD500")
                    getTeeDistribution(energy:energyTotal,str:"20")
                    getservMenuPlan(id:20)
                    
                    
                }else {
                    energyTotal = processDietOrderForTEEDiabetes(tee: tee)
                    
                    getTeeDistribution(energy:energyTotal,str:"3")
                    getservMenuPlan(id:21)
                    
                }
            }else if getDataFromSheardPreferanceString(key: "typeDiabites") == "2" {
                energyTotal = processDietOrderForTEEDiabetes(tee:tee)
                getTeeDistribution(energy:energyTotal,str:"3")
                getservMenuPlan(id:21)
                
            }else if getDataFromSheardPreferanceString(key: "typeDiabites") == "3"{
                //based on mobnth
                if getDataFromSheardPreferanceString(key: "PregancyMonth") == "3"{
                    energyTotal = Int(tee + 452)
                }else if getDataFromSheardPreferanceString(key: "PregancyMonth") == "2" {
                    energyTotal = Int(tee + 350)
                }else if getDataFromSheardPreferanceString(key: "PregancyMonth") == "1" {
                    energyTotal = Int(round(tee))
                    
                }
                getTeeDistribution(energy:energyTotal,str:"19")
                getservMenuPlan(id:22)
                
                
            }
            
            
            
            setDataInSheardPreferance(value:String(Int(round(bmr))),key:"bmr")
            setDataInSheardPreferance(value:String(Int(round(pa))),key:"pa")
            setDataInSheardPreferance(value:String(Int(round(tef))),key:"tef")
            setDataInSheardPreferance(value:String(Int(round(tee))),key:"tee")
            setDataInSheardPreferance(value:String(energyTotal),key:"energyTotal")
        }
        
    }
    
//
//    func imgBackground (imgname:String) {
//        let Diabetes = UIImageView(frame: CGRect(x: 224, y: 68, width: 100, height: 100))
//        Diabetes.image = UIImage(named: setImageTitlePage(id:getDataFromSheardPreferanceString(key: "dietType")))
//        view.addSubview(Diabetes)
//
//        let spicialDiet = UILabel(frame: CGRect(x: 8, y: 13, width: 224, height: 96))
//        spicialDiet.textAlignment = .center
//        spicialDiet.text = setTextTitlePage(id:getDataFromSheardPreferanceString(key: "dietType"))
//        spicialDiet.textColor = UIColor(red: 255, green: 255, blue: 255)
//        spicialDiet.font =  UIFont(name: "GE Dinar One", size: 25.0)!
//        view.addSubview(spicialDiet)
//    }
//    func setBackground(){
//        imgBackground(imgname: "curve")
//    }
    @IBAction func btnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func alert(mess:String){
        let alert = UIAlertController(title: "خطأ", message: mess, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

extension FormatDiabetsDiet2:selectionDelegate{
    func setPragnentMonths(str: String, id: String) {
        
    }
    func setMidctionDiabetsTypeOptionTwo(str: String, type: String) {
        
    }
    
    func setDiabetsType(str: String, typeDiabites: String) {
        
    }
    
    func setMidctionDiabetsTypeOptionOne(str: String, type: String) {
        
    }
    
    func setSeletedCusine(str:String,strID:String){
        
        if !str.isEmpty {
            lblCusine.text = str
            saveCusine(strId:strID)
        }else{
            lblCusine.text = "اختر نوع المطبخ-اختيار متعدد"
            saveAllergy(strId:strID)
        }
    }
    func setSeletedallaegy(str:String,strID:String){
        if !str.isEmpty {
            lblAlergy.text = str
            saveCusine(strId:strID)
        }else{
            lblAlergy.text = "أختر حساسية الطعام-اختيار متعدد"
            saveAllergy(strId:strID)
        }
    }
    
    func setSeletedLayaqa(str:String,strID:String){
        if !str.isEmpty {
            lblLayaqaCondition.text = str
            saveLayaqa(strId:strID)
            idFetnessRate = Int(strID)
        }else{
            lblLayaqaCondition.text = "اختر معدل اللياقة"
            saveLayaqa(strId:strID)
            idFetnessRate = 0
        }
    }
    
    
}

//extension FormatDiabetsDiet2:ApiResponseDaelegat{
//  func showIndecator() {
//
//      ANLoader.showLoading(" يرجى الانتظار قليلا ....", disableUI: true)
//      ANLoader.showLoading()
//  }
//
//  func HideIndicator() {
//      ANLoader.hide()
//  }
//
//  func getResponseSuccuss() {
//
//  }
//
//  func getResponseFalueir(meeage:String) {
//      let alert = UIAlertController(title: "خطأ", message: meeage, preferredStyle: UIAlertController.Style.alert)
//      alert.addAction(UIAlertAction(title: "موافق", style: UIAlertAction.Style.default, handler: nil))
//      self.present(alert, animated: true, completion: nil)
//
//  }
//
//  func getResponseMissigData() {
//
//  }
//
//
//
//}
