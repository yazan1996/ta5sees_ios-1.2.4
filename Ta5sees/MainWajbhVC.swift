//
//  MainWajbhVC.swift
//  Ta5sees
//
//  Created by Admin on 3/17/21.
//  Copyright © 2021 Telecom enterprise. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics

class MainWajbhVC: UIViewController,UIViewControllerTransitioningDelegate {

    //MARK: - IBOutlets
   
    @IBOutlet var btnNextOut:UIButton!
    @IBOutlet var lblText:UIView!
    var items = [ARSelectModel]()

    @IBAction func btnNext(_ sender: Any) {
        
        let refID:[ARSelectModel] = items.filter { (item) -> Bool in
            if item.isSelected {
                return true
            }
            return false
        }
        if refID.isEmpty {
            showAlert(WithMessage: "يجب اختيار وجبة رئيسية")
            return
        }
        ActivationModel.sharedInstance.refWajbehMain = refID.first!.id
        let detailView = self.storyboard!.instantiateViewController(withIdentifier: "WajbehNotMainVC") as! WajbehNotMainVC
        detailView.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        detailView.transitioningDelegate = self
        createEvent(key: "20",date: getDateTime())
        self.present(detailView, animated: true, completion: nil)
//        self.performSegue(withIdentifier: "WajbehNotMainVC", sender: nil)
    }
    @IBAction func dissmes(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func backToHome(_ sender: Any) {
        dismiss(animated: true, completion: nil)

    }
    //MARK: - Declared Variables
    let selectionMessage = "Tag selection type not supported horizontal scroll direction"
    
    let musics = [["1","فطور",false],["2","غداء",true],["3","عشاء",false]]

    fileprivate var selectionView: ARSelectionView?
    var alignment: ARSelectionAlignment = ARSelectionAlignment.left {
        willSet {
            if newValue != self.alignment {
                DispatchQueue.main.async {
                    self.selectionView?.alignment = newValue
                }
            }
        }
    }

    var currentSelectionType: ARSelectionType? {
        willSet {
            if newValue != self.currentSelectionType {
                self.navigationItem.leftBarButtonItem?.isEnabled = newValue != .tags
                DispatchQueue.main.async {
                    if newValue == ARSelectionType.tags {
                        var designDefaults = ARCellDesignDefaults()
                        designDefaults.defaultCellBGColor = UIColor.lightGray.withAlphaComponent(0.3)
                        designDefaults.selectedTitleColor = .white
                        designDefaults.selectedCellBGColor = .black
                        designDefaults.selectedButtonColor = .white
                        designDefaults.rowHeight = 40
                        designDefaults.cornerRadius = 5
                        self.selectionView?.cellDesignDefaults = designDefaults
                        self.selectionView?.options = ARCollectionLayoutDefaults(sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),lineSpacing: 10, interitemSpacing: 10, scrollDirection: .vertical)
                    }
                    else {
                        self.selectionView?.items.forEach {$0.isSelected = false}
                        self.selectionView?.cellDesignDefaults = ARCellDesignDefaults()
                        self.selectionView?.options = ARCollectionLayoutDefaults(scrollDirection: self.scrollDirection == .vertical ? .vertical: .horizontal)
                    }
                    self.selectionView?.selectionType = newValue
                }
            }
        }
    }

    var scrollDirection = UICollectionView.ScrollDirection.vertical {
        willSet {
            if newValue != self.scrollDirection {
                DispatchQueue.main.async {
                    if self.currentSelectionType == .tags {
                        self.selectionView?.options = ARCollectionLayoutDefaults(sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),lineSpacing: 10, interitemSpacing: 10, scrollDirection: .vertical)
                    }
                    else {
                        self.selectionView?.options = ARCollectionLayoutDefaults(scrollDirection: newValue)
                    }
                }
            }
        }
    }

    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Selection"
        self.addSelectionView()
        self.currentSelectionType = .radio
        self.setDummyData()
    }

    //MARK: - Design Layout
    fileprivate func addSelectionView() {

        self.selectionView = ARSelectionView(frame: CGRect.zero)
        self.selectionView?.delegate = self
//        self.selectionView?.maxSelectCount = 5 //set as per need
        self.view.addSubview(self.selectionView!)

        self.selectionView?.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.selectionView!.topAnchor.constraint(equalTo: lblText.bottomAnchor,constant: 16),
            self.selectionView!.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.selectionView!.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.selectionView!.bottomAnchor.constraint(equalTo: btnNextOut.topAnchor),
        ])
        self.alignment = .right
        self.view.layoutIfNeeded()
    }

    //MARK: - Dummy Data
    func setDummyData() {

        for music in musics {
            items.append(ARSelectModel(title: music[1] as! String,id:music[0] as! String, isSelected: music[2] as! Bool))
        }

        let chunkeditems = items.chunked(into: Int((self.selectionView?.frame.height)! / (self.selectionView?.cellDesignDefaults.rowHeight)!))
        for insa in chunkeditems {
            let maxHeight = (insa.map { $0.width }.max() ?? width/2) + ARSelectableCell.CELL_EXTRA_SPACE
            insa.forEach {$0.width = maxHeight }
        }

        DispatchQueue.main.async { [self] in
            self.selectionView?.items = items
        }
    }

    //MARK: - Show Selection Alert
    private func showAlert(WithMessage message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}



//MARK: - ARSelectionView Delegate
extension MainWajbhVC: ARSelectionViewDelegate {

    func selectionMaxLimitReached(_ selectionView: ARSelectionView) {

        self.showAlert(WithMessage: "You can select maximum \(selectionView.maxSelectCount ?? 0)")
    }
}


extension MainWajbhVC:EventPresnter{
    func createEvent(key: String, date: String) {
        Analytics.logEvent("ta5sees_\(key)", parameters: [
          "date": date
        ])
    }
}
